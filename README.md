# README #

Lattice Search Engine was primarily intended for indexing and search in lattices (output of speech recognition systems), but several other tools were included to it over the time.

Use the **LSE_2.x** dir (1.x is old and 3.x was an attempt to refactor 2.x, but it was never completed)

## Compilation ##

Use makefiles included in subdirs.

For example to compile the kws_scorer:

	cd LSE_2.x/kws_scorer
	make dist

## Usage ##

* kws_scorer: wrapper script can be found in LSE_2.x/kws_scorer/scripts

### Contact ###

michal.fapso@gmail.com
