/*********************************************************************************
 *
 *	Application for converting HTK Standard Lattice File (SLF) to binary file
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include "latbinfile.h"
#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
//#include "hypothesis.h"
#include "lexicon.h"
#include "confmeasure.h"

#include "dot_writer.h"

#define TIMER_START	dbgTimer.start();
#define TIMER_END(msg)	dbgTimer.end();	DBG_FORCE("TIMER["<<msg<<"]: "<<dbgTimer.val());

using namespace std;
using namespace lse;

static const string ext_dot = ".dot";
static const string ext_htk = ".lat";

Timer dbgTimer;

//--------------------------------------------------------------------------------

#define DBG(str) DBG_FORCE(str)
//#define DBG(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes filenames as parameters.";
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID

	//application parameters
	char* p_dot_out = NULL;
	char* p_fwbw_out = NULL;

	float p_wi_penalty = 0.0;
	float p_lmscale = 1;
	float p_amscale = 1;
	float p_amscale_kwd = 0.0;
	float p_lmscale_kwd = 0.0;
	float p_posterior_scale = 1.0f;

	bool p_viterbi_baumwelsh = false;
	float p_logAdd_treshold = 7;
	bool p_cut_time_interval = false; 
	float p_cut_t_start = 0;
	float p_cut_t_end = 0;
	bool p_fill_time = false;

	float p_posterior_mapping_power_base = 1.04;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-dot-out") == 0) {
			j++;
			p_dot_out = argv[j];
			
		} else if (strcmp(argv[j], "-fwbw-out") == 0) {
			j++;
			p_fwbw_out = argv[j];
			
		} else if (strcmp(argv[j], "-fill-time") == 0) {
			p_fill_time = true;

		} else if (strcmp(argv[j], "-wi-penalty") == 0) {
			j++;
			p_wi_penalty = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-lmscale") == 0) {
			j++;
			p_lmscale = atof(argv[j]);
			DBG_FORCE("lmscale: "<<p_lmscale);

		} else if (strcmp(argv[j], "-amscale") == 0) {
			j++;
			p_amscale = atof(argv[j]);
			DBG_FORCE("amscale: "<<p_amscale);

		} else if (strcmp(argv[j], "-viterbi-fwbw") == 0) {
			p_viterbi_baumwelsh = false;

		} else if (strcmp(argv[j], "-baumwelsh-fwbw") == 0) {
			p_viterbi_baumwelsh = true;

		} else if (strcmp(argv[j], "-cut-time-interval") == 0) {
			p_cut_time_interval = true; 
			j++;
			p_cut_t_start = atof(argv[j]);
			j++;
			p_cut_t_end = atof(argv[j]);
			
		} else if (strcmp(argv[j], "-posterior-scale") == 0) {
			j++;
			p_posterior_scale = atof(argv[j]);

		} else if (strcmp(argv[j], "-posterior-mapping-power-base") == 0) {
			j++;
			p_posterior_mapping_power_base = atof(argv[j]);

		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		j++;
	}

	string filename = argv[j];

	DBG_FORCE("Processing file: "<<filename);
	if (!file_exists(filename.c_str()))
	{
		CERR("ERROR: file does not exist: "<<filename);
		EXIT();
	}
	
	string latname = filename;

	Lattice lat(&lexicon);
	lat.amscale = p_amscale;
	lat.lmscale = p_lmscale;

	//==================================================
	// READ HTK LATTICE
	//==================================================
	DBG( "reading...");
	if(lat.loadFromHTKFile(filename) != 0) {
		CERR("Error occured while reading lattice file " << filename);
		exit (1);
	}
	DBG( "done\t");

	//==================================================
	// COMPUTE LINKS LIKELIHOOD (POSTERIORS)
	//==================================================
	DBG("compute links likelihood...");
	LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
	vit.computeFwViterbi();
	vit.computeBwViterbi();
	vit.computeLinksLikelihood();
	if (p_fwbw_out) 
		vit.writeViterbiFwBw(p_fwbw_out);
	DBG("done   ");

	//==================================================
	// DOT OUTPUT
	//==================================================
	if (p_dot_out) {
		DBG("dot-export...");
		DotWriter dotwr(&lat);
		dotwr.SetFillTimeAxis(p_fill_time);
		dotwr.SetPosteriorMappingPowerBase(p_posterior_mapping_power_base);
		dotwr.Output(p_dot_out);
		DBG("done\t");
	}
	
	DBG( endl << "***** OPERATION COMPLETED *****" << endl);
	return (0);
}


