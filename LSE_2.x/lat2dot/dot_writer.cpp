#include <cmath>
#include "dot_writer.h"

using namespace std;
using namespace lse;

void DotWriter::Output(const std::string filename) 
{
	if (filename == "-") {
		Output(cout);
	} else {
		ofstream out(filename.c_str());
		if (out.fail()) {
			CERR("ERROR: Can not open file '"<<filename<<"' for writing!");
			EXIT();
		}
		Output(out);
		out.close();
	}
}

//void DotWriter::saveToDotFile(const string filename, bool printNodeNum, bool printLinkData, bool fillTimeAxis, bool showBestPath, float setTimeSamplingRate) {
void DotWriter::Output(ostream &out) {

	// write header
	out << "digraph LATTICE {" << endl 
		<< "rankdir = LR" << endl
		//		<< "size = \"100000,100000\"" << endl
		<< "label = \"\"" << endl
		<< "center = 1" << endl
		<< "nodesep = \"0.250000\"" << endl
		<< "ranksep = \"0.400000\"" << endl
		<< "orientation = Portrait" << endl;


	// create and fill set of all nodes times
	typedef map<float, int> float2intMap; // set of all nodes times
	typedef map<int, std::string> int2stringMap; // set of all nodes times

	float2intMap time_axis;
	int2stringMap frame2nodes;

	// fill frame2nodes mapping frame to list of node IDs
	for(Lattice::Nodes::iterator i=mpLat->nodes.begin(); i!=mpLat->nodes.end(); ++i) {
		std::ostringstream pom;
		pom << (*i)->id;
		int frame = (int)((*i)->t * mFramesPerSecond);
//		DBG_FORCE("I="<<(*i)->id<<" t="<<);
		frame2nodes[frame] += " "+pom.str();
	}

	// DOT: rank nodes with frames
	out << endl;
	for (int2stringMap::iterator i=frame2nodes.begin(); i!=frame2nodes.end(); ++i) {
		out << "{rank=same; T" << (i->first) << (i->second) << ";}" << endl;
	}
	out << endl;

	// Get best path
	map<ID_t, bool> best_path_links;
	if (mShowBestPath) {
		vector<ID_t> best_path;
		mpLat->GetBestPath(&best_path);
		for (vector<ID_t>::iterator i=best_path.begin(); i!=best_path.end(); i++) {
			best_path_links[*i] = true;
		}
	}

	// DOT: main graph
	mpLat->updateFwdBckLinks(Lattice::forward);
	for (Lattice::Nodes::iterator i=mpLat->nodes.begin(); i!=mpLat->nodes.end(); ++i) {
		// int color = (int)(exp((i->second).bestLikelihood) * 255);
		// DOT: main graph - NODES
		Lattice::Node* p_node = *i;
		if (p_node->W == "!NULL") {
			out << p_node->id << " [label = \"\", shape=ellipse, style=bold, fontsize=14, width=0.5, height=0.5, color=dodgerblue]" << endl;
		} else {
			out << p_node->id << " [label = \"" << p_node->W << "\", shape = ellipse, style = bold, fontsize = 14]" << endl;
		}

		// DOT: main graph - LINKS
		std::string color="color=black";
		Lattice::FwdLinks::iterator iFwdLinks = mpLat->fwdLinks.find(p_node->id);

		// Output links
		if (iFwdLinks != mpLat->fwdLinks.end()) {
			for (vector<ID_t>::iterator j=(iFwdLinks->second).begin(); j!=(iFwdLinks->second).end(); ++j) {
				Lattice::Link* p_link = mpLat->links[*j];
				if (mShowBestPath) {
					if (best_path_links.find(p_link->id) != best_path_links.end()) {
						color="color=red, fontcolor=red, penwidth=3";
					} else {
						color="color=\""+ Posterior2HSV(p_link->confidence_noScaleOnKeyword) +"\"";
						color+=",fontcolor=\""+ Posterior2HSV(p_link->confidence_noScaleOnKeyword) +"\"";
						//ostringstream oss_color;
						//oss_color << "color=\""<< Posterior2HSV(p_link->confidence_noScaleOnKeyword) <<"\"";
						//color = oss_color.str();
					}
					color+=",weight="+Posterior2Weight(p_link->confidence_noScaleOnKeyword);
				}

				int link_length = (int)((mpLat->nodes[p_link->E]->t - mpLat->nodes[p_link->S]->t) * mFramesPerSecond);
				out << "  " << p_link->S << " -> " << p_link->E << " [label = \"P:"<<p_link->confidence_noScaleOnKeyword<<" (A:" << p_link->a << "/L:" << p_link->l << ") ["<<link_length<<"]\", " << color << "]" << endl;
			}
		}
	}

	// DOT: time axe subgraph
	out << endl;
	out << "subgraph time_axe {" << endl;
	std::ostringstream time_axe_connections;
	time_axe_connections << "Tstart";
	int first_frame = frame2nodes.begin()->first;
	int last_frame = frame2nodes.rbegin()->first;
	int previous_frame=first_frame;
	for(int2stringMap::iterator i=frame2nodes.begin(); i!=frame2nodes.end(); ++i) {
		int frame = i->first;
		if (last_frame < frame){
			last_frame = frame;
		}
		if (first_frame > frame){
			first_frame = frame;
		}

		// fill time axe with frames which are not associated with any node
		if (mFillTimeAxis) {
			if (previous_frame < (frame - 1)) {
				for (int aux_frame=previous_frame+1; aux_frame<frame; aux_frame++) {
					out << " T" << aux_frame << " [label=\"" << aux_frame << "\" shape=house, fontsize=10, color=lightblue]" << endl;
					time_axe_connections << " -> T" << aux_frame;
				}
			}
		}

		out << " T" << frame << " [label=\"" << frame << "\" shape=house, fontsize=10, color=lightblue]" << endl;
		if (mFillTimeAxis) {
			time_axe_connections << " -> T" << frame;
		} else {
			out << " T" << previous_frame << " -> T" << frame << " [label=\"" << frame - previous_frame << "\"]" << endl;
		}
		previous_frame = frame;
	}
	if (mFillTimeAxis) {
		out << time_axe_connections.str() << endl;
	}
	out << "}" << endl; // end of time axe subgraph

	out << "}" << endl; // end of whole graph
}

std::string DotWriter::Posterior2Weight(float p)
{
	ostringstream oss;

	float val = pow(mPosteriorMappingPowerBase, p)*100;

	oss << val;
	return oss.str();
}

std::string DotWriter::Posterior2HSV(float p) 
{
	ostringstream oss;

//	float poly[] = { 4.0523e-08, 4.7297e-06, 1.7330e-04, 2.0341e-03, 2.5381e-02, 9.9786e-01 };
//	float poly[] = { -4.7782e-08, -6.4741e-06, -2.9648e-04, -4.3709e-03, 3.1371e-02, 9.9668e-01 };
/*
	float val = 
	poly[0] * pow(p,5) +
	poly[1] * pow(p,4) +
	poly[2] * pow(p,3) +
	poly[3] * pow(p,2) +
	poly[4] *     p    +
	poly[5];
*/
	float val = pow(mPosteriorMappingPowerBase, p);
	//float hue = 0.66+val*0.33;

	//oss << hue << " 1.0 "<< val;
	oss << val << " 1.0 1.0";
	return oss.str();
}

