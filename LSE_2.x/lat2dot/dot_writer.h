#ifndef DOT_WRITER_H
#define DOT_WRITER_H

#include <map>
#include "lat.h"
#include "latviterbifwbw.h"

namespace lse {
class DotWriter {
	public:
		DotWriter(Lattice *pLat) : 
			mpLat(pLat), 
			mEndNodeId(-1),
			mStartNodeId(-1),
			mFramesPerSecond(100),
			mShowBestPath(true),
			mFillTimeAxis(true),
			mPosteriorMappingPowerBase(1.04)
		{
			mEndNodeId = mpLat->getEndNode();
			mStartNodeId = mpLat->firstNode()->id;
		}
		void SetShowBestPath(bool show=true) {mShowBestPath = show;}
		void SetFillTimeAxis(bool fill=true) {mFillTimeAxis = fill;}
		void SetPosteriorMappingPowerBase(float base) {mPosteriorMappingPowerBase = base;}
		void Output(const std::string filename);
		void Output(std::ostream &out);
	private:
		Lattice *mpLat;
		ID_t mEndNodeId;
		ID_t mStartNodeId;

		float mFramesPerSecond;
		bool mShowBestPath;
		bool mFillTimeAxis;
		float mPosteriorMappingPowerBase;
		std::string Posterior2HSV(float p);
		std::string Posterior2Weight(float p);
};
}

#endif
