#ifndef _WARNING_H_
#define _WARNING_H_

#include <iostream>

#define WARNING(msg) std::cerr << "\033[1;31m" << "WARNING: " << msg << "\033[0m\n";

#endif
