#ifndef SORTED_MAP_KEYS_H
#define SORTED_MAP_KEYS_H

template <class TMap>
class SortedMapKeys {
	protected:
		typedef typename TMap::mapped_type ValType;
		typedef typename TMap::key_type KeyType;
		typedef std::vector<KeyType> Keys;
		Keys mKeys;
	public:
		SortedMapKeys(const TMap& map) 
		{
			mKeys.reserve(map.size());
			for (typename TMap::const_iterator it = map.begin(); it != map.end(); it++) {
				mKeys.push_back(it->first);
			}
			sort(mKeys.begin(), mKeys.end());
		}
		typedef typename Keys::const_iterator const_iterator;
		inline const_iterator begin() const { return mKeys.begin(); }
		inline const_iterator end()   const { return mKeys.end(); }
};

#endif
