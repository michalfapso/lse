/*********************************************************************************
 *
 *	Application for converting HTK Standard Lattice File (SLF) to binary file
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include "lat.h"

using namespace std;
using namespace lse;

//--------------------------------------------------------------------------------

#define DBG(str) DBG_FORCE(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Usage: " << argv[0] << " -htk-out out_sorted.lat in_notsorted.lat" << endl;
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer indexer;

	//application parameters
	char* p_htk_out = NULL;
	bool p_check_sorting = false;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-htk-out") == 0) {
			j++;
			p_htk_out = argv[j];
			
		} else if (strcmp(argv[j], "-check-if-sorting-is-necessary") == 0) {
			p_check_sorting = true;
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		j++;
	}

	string filename = argv[j];

	DBG_FORCE("Processing file: "<<filename);
	if (!file_exists(filename.c_str()))
	{
		CERR("ERROR: file does not exist: "<<filename);
		EXIT();
	}
	
	string latname = filename;

	Lattice lat(&lexicon);

	//==================================================
	// READ HTK LATTICE
	//==================================================
	DBG( "reading...");
	if(lat.loadFromHTKFile(filename) != 0) {
		CERR("Error occured while reading lattice file " << filename);
		exit (1);
	}
	DBG( "done\t");

	//==================================================
	// SORT LATTICE
	//==================================================
	bool do_sort = true;
	if (p_check_sorting) {
		do_sort = false;
		for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
			if ((*l)->S > (*l)->E) {
				CERR("WARNING: Lattice is not sorted -> sorting...");
				do_sort = true;
				break;
			}
		}
	}

	if (do_sort) {
		lat.sortLattice();
	}
	
	//==================================================
	// HTK OUTPUT
	//==================================================
	if (p_htk_out) {
		DBG("htk-export (" << p_htk_out << ")...");
		lat.saveToHTKFile(p_htk_out);
		DBG("done\t");
	}

	return 0;
}


