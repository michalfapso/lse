#ifndef FST_WRITER_H
#define FST_WRITER_H

#include <map>
#include <sstream>
#include "lat.h"
#include "latviterbifwbw.h"

namespace lse {
class FstWriter {
	public:
		FstWriter(Lattice *pLat, LatViterbiFwBw *pFwbw) : 
			mpLat(pLat), 
			mpFwbw(pFwbw),
			mEndNodeId(-1),
			mStartNodeId(-1),
			mAddTimeLinks(false),
			mAddNormalizingFirstLink(false)
		{
			mEndNodeId = mpLat->getEndNode();
			mStartNodeId = mpLat->firstNode()->id;
		}
		void Output(const std::string filename);
		void Output(std::ostream &out);
		void SetAddTimeLinks(bool add) { mAddTimeLinks = add; }
		void SetAddNormalizingFirstLink(bool add) { mAddNormalizingFirstLink = add; }
		void SetFirstLinkLabels (const std::string& label) { SplitLabels(label, &mFirstLinkLabels); }
		void SetLastLinkLabels  (const std::string& label) { SplitLabels(label, &mLastLinkLabels); }
	private:
		static const char LABEL_SEPARATOR = ':';
		Lattice *mpLat;
		LatViterbiFwBw *mpFwbw;
		ID_t mEndNodeId;
		ID_t mStartNodeId;
		bool mAddTimeLinks;
		bool mAddNormalizingFirstLink;

		typedef std::vector<std::string> Labels;
		Labels mFirstLinkLabels;
		Labels mLastLinkLabels;

		double Posterior2FstWeight(double p) {
			return p == 0 ? 0 : -p;
		}

		const std::string LatWord2FstLabel(const std::string &word) {
			return word == "!NULL" || word == "<s>" || word == "</s>" ? "<eps>" : word;
		}

		void SplitLabels(const std::string& str, Labels *pLabels) {
			std::istringstream iss(str);
			while (iss) {
				std::string sub;
				iss >> sub;
				if (!sub.empty()) {
					pLabels->push_back(sub);
				}
			}
		}

		std::string GetILabel(const std::string& str) {
			size_t pos = str.find_first_of(LABEL_SEPARATOR);
			if (pos == std::string::npos) {
				CERR("ERROR: Label has to be in the format input_label:output_label");
				EXIT();
			}
			return str.substr(0, pos);
		}

		std::string GetOLabel(const std::string& str) {
			size_t pos = str.find_first_of(LABEL_SEPARATOR);
			if (pos == std::string::npos) {
				CERR("ERROR: Label has to be in the format input_label:output_label");
				EXIT();
			}
			return str.substr(pos+1, str.length()-pos+1);
		}
};
}

#endif
