/*********************************************************************************
 *
 *	Application for converting HTK Standard Lattice File (SLF) to binary file
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include "latbinfile.h"
#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
//#include "hypothesis.h"
#include "lexicon.h"
#include "confmeasure.h"

#include "fst_writer.h"

using namespace std;
using namespace lse;

static const string ext_fst = ".fst";
static const string ext_htk = ".lat";

Timer dbgTimer;

//--------------------------------------------------------------------------------

#ifndef DBG
	#define DBG(str) DBG_FORCE(str)
#endif
//#define DBG(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes filenames as parameters.";
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID

	//application parameters
	char* p_fst_out = NULL;
	char* p_fwbw_out = NULL;
	bool  p_sort_lattice = false;
	bool  p_add_time_links = false;
	bool  p_add_normalizing_first_link = false;
	char* p_first_link_labels = NULL;
	char* p_last_link_labels = NULL;

	float p_wi_penalty = 0.0;
	float p_lmscale = 1.0;
	float p_amscale = 1.0;
	float p_amscale_kwd = 0.0;
	float p_lmscale_kwd = 0.0;
	float p_posterior_scale = 1.0f;

	char *p_lat_out = NULL;

	bool  p_viterbi_baumwelsh = false;
	float p_logAdd_treshold = 7;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-fst-out") == 0) {
			j++;
			p_fst_out = argv[j];
			
		} else if (strcmp(argv[j], "-fwbw-out") == 0) {
			j++;
			p_fwbw_out = argv[j];
			
		} else if (strcmp(argv[j], "-sort-lattice") == 0) {
			p_sort_lattice = true;
			
		} else if (strcmp(argv[j], "-add-time-links") == 0) {
			p_add_time_links = true;
			
		} else if (strcmp(argv[j], "-add-normalizing-first-link") == 0) {
			p_add_normalizing_first_link = true;
			
		} else if (strcmp(argv[j], "-first-link-labels") == 0) {
			j++;
			p_first_link_labels = argv[j];
			
		} else if (strcmp(argv[j], "-last-link-labels") == 0) {
			j++;
			p_last_link_labels = argv[j];
			
		} else if (strcmp(argv[j], "-wi-penalty") == 0) {
			j++;
			p_wi_penalty = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-lmscale") == 0) {
			j++;
			p_lmscale = atof(argv[j]);
			DBG_FORCE("lmscale: "<<p_lmscale);

		} else if (strcmp(argv[j], "-amscale") == 0) {
			j++;
			p_amscale = atof(argv[j]);
			DBG_FORCE("amscale: "<<p_amscale);

		} else if (strcmp(argv[j], "-viterbi-fwbw") == 0) {
			p_viterbi_baumwelsh = false;

		} else if (strcmp(argv[j], "-baumwelsh-fwbw") == 0) {
			p_viterbi_baumwelsh = true;

		} else if (strcmp(argv[j], "-posterior-scale") == 0) {
			j++;
			p_posterior_scale = atof(argv[j]);

		} else if (strcmp(argv[j], "-lat-out") == 0) {
			j++;
			p_lat_out = argv[j];
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		j++;
	}

	string filename = argv[j];

	DBG_FORCE("Processing file: "<<filename);
	if (!file_exists(filename.c_str()))
	{
		CERR("ERROR: file does not exist: "<<filename);
		EXIT();
	}
	
	Lattice lat(&lexicon);
	lat.amscale = p_amscale;
	lat.lmscale = p_lmscale;

	//==================================================
	// READ HTK LATTICE
	//==================================================
	DBG( "reading...");
	if(lat.loadFromHTKFile(filename) != 0) {
		CERR("Error occured while reading lattice file " << filename);
		exit (1);
	}
	DBG( "done\t");

	//==================================================
	// CHECK WHETHER THE LATTICE IS SORTED TOPOLOGICALY
	//==================================================
	bool sorted = true;
	for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
		if ((*l)->S > (*l)->E) {
			sorted = false;
			break;
		}
	}
	if (!sorted)
	{
		if (!p_sort_lattice) CERR("WARNING: Lattice '"<<filename<<"' is not sorted! ...sorting lattice");
		lat.sortLattice();
		if (!p_sort_lattice) CERR("sorting lattice...done");
	}

	//==================================================
	// COMPUTE LINKS LIKELIHOOD (POSTERIORS)
	//==================================================
	DBG("compute links likelihood...");
	LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
	vit.computeFwViterbi();
	vit.computeBwViterbi();
	vit.computeLinksLikelihood();
	if (p_fwbw_out) 
		vit.writeViterbiFwBw(p_fwbw_out);

	cout << "total_likelihood: " << lat.nodes[0]->beta << endl;

	//for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
	//	cout << (*l)->S << " -> " << (*l)->E << " = " << (*l)->confidence << endl;
	//}
	DBG("done   ");

	//==================================================
	// HTK LATTICE OUTPUT
	//==================================================
	if (p_lat_out) {
		lat.saveToHTKFile(p_lat_out, true); // COPY OF LATTICE
	}

	//==================================================
	// FST OUTPUT
	//==================================================
	if (p_fst_out) {
		DBG("fst-export...");
		FstWriter fstwr(&lat, &vit);
		fstwr.SetAddTimeLinks(p_add_time_links);
		fstwr.SetAddNormalizingFirstLink(p_add_normalizing_first_link);
		if (p_first_link_labels) fstwr.SetFirstLinkLabels(p_first_link_labels);
		if (p_last_link_labels)  fstwr.SetLastLinkLabels(p_last_link_labels);
		fstwr.Output(p_fst_out);
		DBG("done\t");
	}
	
	DBG( endl << "***** OPERATION COMPLETED *****" << endl);
	return (0);
}


