#include <cmath>
#include "fst_writer.h"

using namespace std;
using namespace lse;

//void FstWriter::saveToFstFile(const string filename, bool printNodeNum, bool printLinkData, bool fillTimeAxis, bool showBestPath, float setTimeSamplingRate) {
void FstWriter::Output(const std::string filename) 
{
	if (filename == "-") {
		Output(cout);
	} else {
		ofstream out(filename.c_str());
		if (out.fail()) {
			CERR("ERROR: Can not open file '"<<filename<<"' for writing!");
			EXIT();
		}
		Output(out);
		out.close();
	}
}

void FstWriter::Output(ostream &out) 
{
	// Weight of the First link is equal to minus full weight of the whole lattice (full forward likelihood).
	// Therefore the weight of each full path in the lattice will give a log posterior probability.
	if (mAddNormalizingFirstLink && mFirstLinkLabels.empty()) {
		mFirstLinkLabels.push_back("<eps>:<eps>");
	}
	CERR("Full forward probability: "<<fixed<<setprecision(10)<<mpFwbw->bwds[0]);
	// Add prefix links
	for (size_t i=0; i<mFirstLinkLabels.size(); i++) {
		out << i << " "
			<< i + 1 << " " 
			<< GetILabel(mFirstLinkLabels[i]) << " "
			<< GetOLabel(mFirstLinkLabels[i]) << " "
			<< fixed << setprecision(10) << (i == 0 && mAddNormalizingFirstLink ? Posterior2FstWeight(- mpFwbw->bwds[0]) : 0)
			<< endl; 
	}

	int prefix_links = mFirstLinkLabels.size(); // additional nodes at the beginning
	int suffix_links = mLastLinkLabels.size(); // additional nodes at the end

	// Add time links
	if (mAddTimeLinks) {
		assert(mEndNodeId == (int)mpLat->nodes.size()-1);

		// Add links from beginning to each node with forward probability (from beginning to that node)
		// and from each node to the end with backward probability (from that node to the end)
		// But skip the first and the last node.
		for (int i=0; i<(int)mpLat->nodes.size()-1; i++) {
			Lattice::Node* n = mpLat->nodes[i];
			out << prefix_links << " "
				<< prefix_links + 1 + n->id << " " // +1 is there because time links going from the start are like another prefix link
				<< fixed << setprecision(2) << "t=" << n->t << " "
				<< fixed << setprecision(2) << "t=" << n->t << " " 
				<< fixed << setprecision(10) << Posterior2FstWeight(mpFwbw->fwds[n->id])
				<< endl;

			out << prefix_links + 1 + n->id << " " // +1 is there because time links going from the start are like another prefix link
				<< prefix_links + 1 + mEndNodeId << " " 
				<< fixed << setprecision(2) << "t=" << n->t << " "
				<< fixed << setprecision(2) << "t=" << n->t << " " 
				<< fixed << setprecision(10) << Posterior2FstWeight(mpFwbw->bwds[n->id])
				<< endl;

//			DBG_FORCE(mpFwbw->fwds[n->id] << " + " << mpFwbw->bwds[n->id] << " - " << mpFwbw->bwds[0] << " = " << mpFwbw->fwds[n->id] + mpFwbw->bwds[n->id] - mpFwbw->bwds[0]);
		}
		prefix_links++;
	}

	// Add lattice links
	for (Lattice::Links::iterator iLink=mpLat->links.begin(); iLink!=mpLat->links.end(); ++iLink) {
		// int color = (int)(exp((i->second).bestLikelihood) * 255);
		// FST: main graph - NODES
		Lattice::Link* l = *iLink;

		// If time links are added, the beginning and last links are discarded / replaced by them
		if (mAddTimeLinks && (l->E == mEndNodeId)) {
			continue;
		}

		const string w = LatWord2FstLabel(mpLat->nodes[l->E]->W);
		float wipen = mpLat->lexicon->CanAddWiPen(mpLat->nodes[l->E]->W_id) ? mpFwbw->wipen : 0;
		float lik;
//		if (mAddTimeLinks && (l->S == 0)) {
//			lik = 0; // because this likelihood is already inlcuded in the time link coming from the start to this FST link
//		} else {
			lik = (l->a * mpFwbw->amscale + l->l * mpFwbw->lmscale + wipen) * mpFwbw->posteriorscale;
//		}
		out << prefix_links + l->S << " " 
			<< prefix_links + l->E << " " 
			<< w << " " 
			<< w << " " 
			<< fixed << setprecision(10) << Posterior2FstWeight(lik)
			<< endl;
//		out << "DEBUG: " << l->a << " * " << mpFwbw->amscale <<" + "<<l->l<<" * "<<mpFwbw->lmscale<<" + "<<wipen<<" = "<<(l->a * mpFwbw->amscale + l->l * mpFwbw->lmscale + wipen) << endl;
	}

	// Add suffix links
	if (!mLastLinkLabels.empty()) {
		out << prefix_links + mEndNodeId << " "
			<< prefix_links + mpLat->nodes.size() << " "
			<< GetILabel(mLastLinkLabels[0]) << " "
			<< GetOLabel(mLastLinkLabels[0]) << " "
			<< "0.0"
			<< endl;

		for (size_t i=1; i<mLastLinkLabels.size(); i++) {
			out << prefix_links + mpLat->nodes.size() + i-1 << " "
				<< prefix_links + mpLat->nodes.size() + i << " "
				<< GetILabel(mLastLinkLabels[i]) << " "
				<< GetOLabel(mLastLinkLabels[i]) << " "
				<< "0.0"
				<< endl;
		}
	}

	// The last node:
	out << prefix_links + mpLat->nodes.size() - 1 + suffix_links << endl;
}

