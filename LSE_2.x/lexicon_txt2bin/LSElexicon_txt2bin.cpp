#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/lattypes.h"
#include "lexicon.h"

using namespace std;
using namespace lse;

string p_lexicon_in = "";
string p_lexicon_out = "";

int main(int argc, char * argv[]) {
	if (argc != 3) {
		cout << argv[0] << " [txt-lexicon-in] [bin-lexicon-out]" << endl;
		return 1;
	}

	p_lexicon_in = argv[1];
	p_lexicon_out = argv[2];
	Lexicon lexicon(Lexicon::readwrite);
	
	ifstream in(p_lexicon_in.c_str());
	if (!in.good()) {
		return (2);
	}
	
	ofstream out(p_lexicon_out.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	while (!in.eof()) {
		ID_t id;
		string W;
		in >> id;
		in >> W;

		/*
		// remove all backslashes
		string::size_type pos = 0;
		while( (pos = W.find("\\", pos)) != string::npos )
		{
			W.erase(pos, 1);
		}
		*/

		lexicon.writeStruct(out, id, W);
//		cout << id << "\t" << W << endl;
	}

	out.close();
	in.close();

	if (lexicon.loadFromFile(p_lexicon_out) != 0)
		cerr << "Lexicon input file " << p_lexicon_out << " not found ... using empty lexicon" << endl;

	lexicon.createIndexFile(p_lexicon_out);
	return 0;
} // main()

