/*********************************************************************************
 *
 *	Application for sorting HTK Standard Lattice Files (SLF)
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include "latbinfile.h"
#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
//#include "hypothesis.h"
#include "lexicon.h"



using namespace std;
using namespace lse;

//--------------------------------------------------------------------------------

#define DBG_MAIN(str) if (p_debug_level > 1) cout << str << flush
//#define DBG_MAIN(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cerr << "Application takes filenames as parameters.";
		cerr << "Usage: " << argv[0] << " \\" << endl
			 << "       -lattice-dir-out /mnt/matylda/exp/sorted_lattices \\" << endl
			 << "       -latlist-in unsortedlattices.list" << endl;
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer indexer;

	//application parameters
	bool p_write_htk = true;
	bool p_sort_lattice = true;
	string p_lattice_dir_out = ".";
	string p_latlist_in_file = "";
	bool p_htk_posteriors_output = false;
	bool p_check_node_sorting = false;
	int p_debug_level = 0;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-latlist-in") == 0) {
			j++;			
			p_latlist_in_file = argv[j];

		} else if (strcmp(argv[j], "-lattice-dir-out") == 0) {
			j++;
			p_lattice_dir_out = argv[j];
			
		} else if (strcmp(argv[j], "-check-node-sorting") == 0) {
			p_check_node_sorting = true;
			
		} else if (strcmp(argv[j], "-htk-posteriors-output") == 0) {
			p_htk_posteriors_output = true;
			
		} else if (strcmp(argv[j], "-debug-level") == 0) {
			j++;
			p_debug_level = atoi(argv[j]);
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		
		
		j++;
	}


	DBG_MAIN( endl);
	
	string filename;
	
	//==================================================
	// OPEN LATTICE LIST FILE
	//==================================================
	ifstream in_latlist;
	if (p_latlist_in_file != "") {
		in_latlist.open(p_latlist_in_file.c_str());
		if (in_latlist.fail()) {
			cerr << "Error: latlist open failed ... " << p_latlist_in_file << endl;
			exit(1);
		}
	}
	
	//==================================================
	// MAIN LOOP - FOR EACH LATTICE FILE
	//==================================================
	DBG_MAIN("Processing files...");
	while ((p_latlist_in_file != "") ? (getline(in_latlist, filename)) : (j < argc)) { 
		// jump over application parameters
		if (p_latlist_in_file != "") 
			DBG_MAIN("Latlist record: "<<filename<<endl);
		
		if (p_latlist_in_file == "") {
			if (argv[j][0] == '-') {
				cerr << "Parameter " << argv[j] << " ignored (try --help)" << endl;
				continue;
			}
			filename = argv[j];
		}
		DBG_MAIN("filename: "<<filename<<endl<<flush);

		//==================================================
		// PROCESS LATTICE FILENAME
		//==================================================
		string latname = filename;
		// erase file extension
		string::size_type strPos = latname.rfind ('/');
		if (strPos != string::npos) {
			string::size_type pos2 = latname.find ('.', strPos);
			latname.erase(pos2);
		}
		// erase path (only filename without extension remains)
		string latname_nopath = latname;
		strPos = latname_nopath.rfind('/');
		latname_nopath.erase(0, strPos+1);

		DBG_MAIN( "latname_nopath: " << latname_nopath << endl);

		//==================================================
		// READ HTK LATTICE
		//==================================================
		DBG_MAIN( "reading htk lattice..." );

		Lattice lat(lexicon, indexer);

		LatTime latStartTime = 0;
		int p_lat_time_multiplier = 1;
		bool p_only_add_words_to_lexicon = false;
		bool p_input_SRI_lattices = false;
		
		if(lat.loadFromHTKFile(filename, latStartTime, p_lat_time_multiplier, p_only_add_words_to_lexicon, p_input_SRI_lattices) != 0) {
			cerr << "Error occured while reading lattice file " << filename << endl << flush;
			exit (1);
		}
		DBG_MAIN( "done" << endl);

		//==================================================
		// SORT LATTICE
		//==================================================
		if (p_sort_lattice)
		{
			DBG_MAIN("sorting lattice...");
			lat.sortLattice();
			DBG_MAIN("done" << endl);
		}
		
		//==================================================
		// CHECK NODE SORTING
		//==================================================
		if (p_check_node_sorting) {
			DBG_MAIN("check node sorting...");
			for (Lattice::LinkMap::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
				if ((l->second).S > (l->second).E) {
					cerr << "Error in \"" << filename << "\" ...linkID:"<<l->first<<" "<<(l->second).S<<" -> "<<(l->second).E << endl;
//					exit(1);
				}
			}
			DBG_MAIN("done" << endl);
		}
		
		//==================================================
		// HTK OUTPUT
		//==================================================
		if (p_write_htk) {
			DBG_MAIN( "htk-export (" << p_lattice_dir_out + "/" + latname_nopath << ")..." << flush);
			lat.saveToHTKFile(p_lattice_dir_out + "/" + latname_nopath + ".lat.gz", p_htk_posteriors_output, false); // COPY OF LATTICE		
			DBG_MAIN( "done" << endl);
		}

		DBG_MAIN( endl << endl); // insert empty line between lattices info output
		
		if (p_latlist_in_file == "") j++;

	} // while (j < argc)


	//==================================================
	// AFTER PROCESSING LATTICES
	//==================================================

	
	if (p_latlist_in_file != "") in_latlist.close();
	
	DBG_MAIN( endl << "***** OPERATION COMPLETED *****" << endl);
	return (0);
}


