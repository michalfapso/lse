#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/common.h"
#include "../liblse/latindexer.h"
#include "../liblse/lattypes.h"
#include "../liblse/timer.h"

using namespace std;
using namespace lse;

string p_fwd_idx_list = "";
string p_idx_out = "";
string p_merged_idx_out = "";

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes forward index filenames as parameters." << endl;
		return 1;
	}

	int j=1;
	while (j<argc) {

		DBG_FORCE("argv["<<j<<"]:" << argv[j]);
			
		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;

		} else if (strcmp(argv[j], "-fwd-idx-list") == 0) {
			j++;
			p_fwd_idx_list = argv[j];
			DBG_FORCE("-fwd-idx-list: "<< p_fwd_idx_list);

		} else if (strcmp(argv[j], "-idx-out") == 0) {
			j++;
			p_idx_out = argv[j];
			DBG_FORCE("-idx-out: "<< p_idx_out);
		
		} else if (strcmp(argv[j], "-merged-idx-out") == 0) {
			j++;
			p_merged_idx_out = argv[j];
			DBG_FORCE("-merged-idx-out: "<< p_merged_idx_out);
		}
		j++;
	} // for each param


	Timer sortTimer;
	sortTimer.start();
	float sortingTime = 0.0;

	FwdIndex fwdIndex;

	int firstfile_idx = j;
	unsigned long long completesize = 0;
	
	//==================================================
	// GETTING FILES' SIZE
	//==================================================
	
	ifstream idx_list;
	if (p_fwd_idx_list != "")
	{
		idx_list.open(p_fwd_idx_list.c_str());
	}

	string filename;
	while ( p_fwd_idx_list!="" ? (!idx_list.eof()) : (j<argc) ) 
	{
		if (p_fwd_idx_list!="") 
		{
			getline(idx_list, filename);
		} 
		else 
		{
			filename = argv[j];
		}
		if (filename.length() == 0) {
			continue;
		}
		unsigned long long filesize_tmp = file_size(filename.c_str());
		cout << "file_size(" << filename << ") = " << filesize_tmp << endl;
		if (filesize_tmp > 0)
		{
			completesize += filesize_tmp;
		}
		j++;
	}

	cout << "Allocating memory: " << completesize << " bytes...";
	fwdIndex.initArray(completesize);
	cout << "done" << endl;

	//==================================================
	// ADDING FWD INDEX FILES TO MEMORY
	//==================================================
	if (p_fwd_idx_list != "")
	{
		idx_list.close();
		idx_list.open(p_fwd_idx_list.c_str());
	}
	j = firstfile_idx;
	DBG_FORCE("idx_list.eof():"<<idx_list.eof());
	while ( p_fwd_idx_list!="" ? (!idx_list.eof()) : (j<argc) ) 
	{
		if (p_fwd_idx_list!="") 
		{
			getline(idx_list, filename);
		}
		else 
		{
			filename = argv[j];
		}
		DBG("adding fwd index: " << filename);
		if (filename == "")
		{
			CERR("WARNING: ignoring empty filename");
		}
		else if (!file_exists(filename.c_str())) 
		{
			CERR("WARNING: file "<<filename<<" does not exist");
		}
		else
		{
			fwdIndex.appendFile(filename);
		}
		j++;
	}
	DBG("forward indices loaded");

	sortTimer.end();
	sortingTime += sortTimer.val();	

	if (p_merged_idx_out == "")
	{
		p_merged_idx_out = p_idx_out+".notsorted";
	}
	DBG("fwdIndex.save("<<p_merged_idx_out<<");");
	fwdIndex.save(p_merged_idx_out);
	DBG("...saved");

	if (p_idx_out != "")
	{
		//==================================================
		// SORT FWD INDEX
		//==================================================
		sortTimer.start();
		DBG_FORCE("fwdIndex.sortarray();");
		fwdIndex.sortarray();
		
		//==================================================
		// SAVE SORTED INDEX TO FILE
		//==================================================
		DBG_FORCE("fwdIndex.save(p_idx_out);");
		fwdIndex.save(p_idx_out);
		sortTimer.end();
		sortingTime += sortTimer.val();
		
		ofstream log((p_idx_out+".log").c_str());
		log << "sorting_time: " << sortingTime << endl;
		log.close();
	}

/*	ofstream out(p_idx_out.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	for(int i=0; i<arraysize; i++) {
		out.write(reinterpret_cast<const char *>(&(fwdidx[i])), sizeof(LatIndexer::Record));
	}
	out.close();
*/
	DBG_FORCE("main() return 0");
	return 0;
} // main()

