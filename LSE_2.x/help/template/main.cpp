/*********************************************************************************
 *
 *	A basic skeleton (structure) of application processing lattices
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
//#include "latbinfile.h"
//#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
//#include "strindexer.h"
//#include "latindexer.h"
//#include "indexer.h"
//#include "hypothesis.h"
#include "lexicon.h"



using namespace std;
using namespace lse;

//--------------------------------------------------------------------------------

#define DBG_MAIN(str) if (p_debug_level > 1) cerr << str << flush
//#define DBG_MAIN(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes filenames as parameters.";
		return 1;
	}
		
	Lexicon lexicon; // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer indexer;

	//application parameters
	string p_lattice_dir_out = ".";
	string p_latlist_in_file = "";

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
			
		} else if (strcmp(argv[j], "-latlist-in") == 0) {
			j++;			
			p_latlist_in_file = argv[j];

		} else if (strcmp(argv[j], "-lattice-dir-out") == 0) {
			j++;
			p_lattice_dir_out = argv[j];
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		
		j++;
	}

	
	//==================================================
	// OPEN LATTICE LIST FILE
	//==================================================
	ifstream in_latlist;
	if (p_latlist_in_file != "") {
		in_latlist.open(p_latlist_in_file.c_str());
/*		if (!in_latlist.good()) 
			DBG_FORCE("!GOOD");
		if (in_latlist.bad()) 
			DBG_FORCE("BAD");*/
		if (in_latlist.fail()) {
			cerr << "Error: latlist open failed ... " << p_latlist_in_file << endl;
			exit(1);
		}
	}
	
	// if there are lattices to process and start-time-field is not set, then exit with error
	if ((p_latlist_in_file != "" || j < argc) && (p_lat_filename_start_time_field == 0) && !p_only_add_words_to_lexicon) {
		cerr << "ERROR: set the parameter -lat-filename-start-time-field [NUMBER]";
		exit(1);
	}

	//==================================================
	// MAIN LOOP - FOR EACH LATTICE FILE
	//==================================================
	DBG_MAIN("Processing files...");
	while ((p_latlist_in_file != "") ? (getline(in_latlist, filename)) : (j < argc)) { 
		// jump over application parameters
		if (p_latlist_in_file != "") 
			DBG_MAIN("Latlist record: "<<filename<<endl);
		
		if (p_latlist_in_file == "") {
			if (argv[j][0] == '-') {
				cerr << "Parameter " << argv[j] << " ignored (try --help)" << endl;
				continue;
			}
			filename = argv[j];
		}
		DBG_MAIN("filename: "<<filename<<endl<<flush);

		//==================================================
		// PROCESS LATTICE FILENAME
		//==================================================
		string latname = filename;
		// erase file extension
		string::size_type strPos = latname.rfind ('/');
		if (strPos != string::npos) {
			string::size_type pos2 = latname.find ('.', strPos);
			latname.erase(pos2);
		}
		// erase path (only filename without extension remains)
		string latname_nopath = latname;
		strPos = latname_nopath.rfind('/');
		latname_nopath.erase(0, strPos+1);

		DBG_MAIN( "latname_nopath: " << latname_nopath << endl);


		//==================================================
		// GET LATTICE'S START TIME
		//==================================================
		LatTime latStartTime = 0;
		if (p_read_time_from_filename || p_concatlattice_out!="") {
			string sStartTime = latname_nopath;
			string::size_type pos_1st = 0;
			for (int field_idx=1; field_idx < p_lat_filename_start_time_field; field_idx++) {
				pos_1st = sStartTime.find (p_lat_filename_delimiter, pos_1st+1); // find first _ in latname
				DBG("pos_1st:"<<pos_1st);
			}
			if (pos_1st != string::npos) {
				string::size_type pos_2nd = latname_nopath.find (p_lat_filename_delimiter, pos_1st+1); // find second _ in latname
				sStartTime = sStartTime.substr(pos_1st+1, pos_2nd - pos_1st - 1);
				latStartTime = atof(sStartTime.c_str()) / 100.0;
				DBG_MAIN("Getting start time from filename...sStartTime:"<<sStartTime<<" latStartTime:"<<latStartTime<<endl);
			} else {
				cerr << "Error in lattice's name: Expecting _ in \"" << latname_nopath << "\" ... setting start time to 0.0" << endl << flush;
			}
		}

		if (p_debug_level > 0) cout << "Processing file: " << filename << endl << flush;

		Lattice lat(lexicon, indexer);

		//==================================================
		// READ HTK LATTICE
		//==================================================
		DBG_MAIN( "reading..." << flush);

		if(lat.loadFromHTKFile(filename, latStartTime, p_lat_time_multiplier, p_only_add_words_to_lexicon, p_input_SRI_lattices) != 0) {
			cerr << "Error occured while reading lattice file " << filename << endl << flush;
			exit (1);
		}
		if (p_only_add_words_to_lexicon) {
			if (p_latlist_in_file == "") j++; // go to the next file (when parsing filenames as command-line parameters)
			continue;
		}
		DBG_MAIN( "done\t" << flush);

		//==================================================
		// HTK OUTPUT
		//==================================================
		if (p_write_htk) {
			DBG_MAIN( "htk-export (" << p_lattice_dir_out + "/" + latname_nopath + ext_copy << ")..." << flush);
			lat.saveToHTKFile(p_lattice_dir_out + "/" + latname_nopath + ext_copy, p_htk_posteriors_output, true); // COPY OF LATTICE		
			DBG_MAIN( "done\t" << flush);
		}


		DBG_MAIN( endl << endl); // insert empty line between lattices info output
		
		if (p_latlist_in_file == "") j++;

	} // while (j < argc)


	//==================================================
	// AFTER PROCESSING LATTICES
	//==================================================

	
	if (p_latlist_in_file != "") in_latlist.close();
	
	DBG_MAIN( endl << "***** OPERATION COMPLETED *****" << endl);
	return (0);
}


