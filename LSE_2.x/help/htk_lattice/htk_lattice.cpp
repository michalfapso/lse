#include "lat.h"
#include "lexicon.h"

using namespace lse; // Lattice Search Engine

int main() {
	Lexicon lexicon; // stores records of type: wordID -> word  and  word -> wordID
	Lattice lat(&lexicon);
	lat.loadFromHTKFile("htk-test-lattice.lat");
	lat.saveToHTKFile("htk-test-lattice.out.lat"); // COPY OF LATTICE		
}

