#ifndef HYPOTHESES_H
#define HYPOTHESES_H

#include "mlf.h"
#include "container_interface.h"
#include "dbg.h"

class Hypothesis;

class ReferenceMlfRecord : public mlf::MlfRecordWithFilename {
	protected:
		typedef mlf::MlfRecordWithFilename Base;
		unsigned int mHitsCount;
		Hypothesis* mpHypHit;
	public:
		ReferenceMlfRecord() :
			Base(),
			mHitsCount(0),
			mpHypHit(NULL)
		{}
		virtual void Reset() {
			Base::Reset();
			ResetHitsCount();
			ResetHypHit();
		}
		virtual inline void ResetHitsCount() { mHitsCount = 0; }
		virtual inline void ResetHypHit() { mpHypHit = NULL; }
		virtual inline void IncrementHitsCount() { mHitsCount++; }
		virtual inline unsigned int GetHitsCount() const { return mHitsCount; }
		virtual inline Hypothesis* GetHypHit() const { return mpHypHit; }
		virtual inline void SetHypHit(Hypothesis* pHyp) { mpHypHit = pHyp; }
		friend std::ostream& operator<<(std::ostream& oss, const ReferenceMlfRecord& rec) {
			return oss << *static_cast<const Base*>(&rec) << " hits="<<rec.GetHitsCount();// << " hyp_hit="<<*rec.GetHypHit();
		}
};

class Hypothesis : public mlf::MlfRecordWithFilename {
	protected:
		typedef mlf::MlfRecordWithFilename Base;
		bool mIsHit;
		bool mIsUniqueHit;
		ReferenceMlfRecord* mpRefHit;
		
	public:
		Hypothesis() :
			Base(),
			mIsHit(false),
			mIsUniqueHit(false),
			mpRefHit(NULL)
		{}

		friend std::ostream& operator<<(std::ostream& oss, const Hypothesis& hyp) {
			return oss << *static_cast<const Base*>(&hyp);
		}

		// IsHit()
		typedef bool (Hypothesis::* IsHitFunPtr)(const mlf::MlfRecord* recRef) const;
		static std::string IsHitFunPtr2String(Hypothesis::IsHitFunPtr ptr);
		static IsHitFunPtr String2IsHitFunPtr(std::string str);
		static std::string IsHitFunGetStringNames();
		bool IsHit_point_middle_of_reference(const mlf::MlfRecord* recRef) const;
		bool IsHit_point_middle_of_result   (const mlf::MlfRecord* recRef) const;
		bool IsHit_point_middle_of_both     (const mlf::MlfRecord* recRef) const;
		bool IsHit_half_sec_around_reference(const mlf::MlfRecord* recRef) const;

		inline bool IsHit() const { return mIsHit; }
		inline bool IsUniqueHit() const { return mIsUniqueHit; }
		inline ReferenceMlfRecord* GetRefHit() { return mpRefHit; }

		void DetermineIsHit(
				const mlf::MlfRecords<ReferenceMlfRecord>* recsRef, 
				IsHitFunPtr isHitFunPtr = &Hypothesis::IsHit_point_middle_of_reference) 
		{
			mIsHit = false;
			mIsUniqueHit = false;
			if (!recsRef) { return; }
			for (mlf::MlfRecords<ReferenceMlfRecord>::const_iterator it = recsRef->begin(); it != recsRef->end(); it++)
			{
				if (GetFilename() == (*it)->GetFilename()) {
					if ((this->*isHitFunPtr)(*it)) {
						mIsHit = true;
						mIsUniqueHit = (*it)->GetHitsCount() == 0;
						// If there is already a hit for that reference
						if ((*it)->GetHypHit()) {
							if ((*it)->GetHypHit()->GetScore() < this->GetScore()) {
								(*it)->GetHypHit()->mIsUniqueHit = false;
								this->mIsUniqueHit = true;
								(*it)->SetHypHit(this);
							}
						} else {
							(*it)->SetHypHit(this);
						}
						mpRefHit = *it;
						mpRefHit->IncrementHitsCount();
						return;
					}
				}
			}
		}

		static bool CmpScores(const Hypothesis* a, const Hypothesis* b) {
			return a->GetScore() > b->GetScore();
		}
};

/*
class Hypotheses : public ContainerInterface< std::vector<Hypothesis*> >
{
	public:
		Hypotheses() : mIsSortedByScore(false) {}
		~Hypotheses() {
			for (iterator it = mContainer.begin(); it != mContainer.end(); it++) {
				delete *it;
			}
		}

		inline void Add(Hypothesis* hyp) { mContainer.push_back(hyp); mIsSortedByScore = false;}

		void Fill(const mlf::MlfRecords* recsRef, const mlf::MlfRecords* recsRes);
		void SortByScore() { 
			sort(begin(), end(), Hypothesis::CmpScores); 
			mIsSortedByScore = true;
		}
		inline bool IsSortedByScore() const { return mIsSortedByScore; }
		friend std::ostream& operator<<(std::ostream& oss, const Hypotheses& hyps)
		{
			for (const_iterator it = hyps.begin(); it != hyps.end(); it++) {
				oss << **it << std::endl;
			}
			return oss;
		}
	protected:
		bool mIsSortedByScore;
};

class Words2Hypotheses : public MapInterface< boost::unordered_map<std::string, Hypotheses*> >
{
	public:
		void Fill(const mlf::Mlf& mlfRef, const mlf::Mlf& mlfRes);
		void Fill(const mlf::File* fRef, const mlf::File* fRes);
		friend std::ostream& operator<<(std::ostream& oss, const Words2Hypotheses& w2h)
		{
			for (const_iterator it = w2h.begin(); it != w2h.end(); it++) {
//				oss << "  Word: \"" << it->first << "\"" << std::endl
				oss << *it->second;
			}
			return oss;
		}
};

class Paths2Words2Hypotheses : public MapInterface< boost::unordered_map<std::string, Words2Hypotheses*> >
{
	public:
		void Fill(const mlf::Mlf& mlfRef, const mlf::Mlf& mlfRes);
		friend std::ostream& operator<<(std::ostream& oss, const Paths2Words2Hypotheses& p2w2h)
		{
			for (const_iterator it = p2w2h.begin(); it != p2w2h.end(); it++) {
				oss << "File: \"" << it->first << "\"" << std::endl
				    << *it->second;
			}
			return oss;
		}
};
*/

#endif
