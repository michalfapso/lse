#ifndef MLF_WITH_HIT_INFO_H
#define MLF_WITH_HIT_INFO_H

#include "mlf.h"
#include "hypotheses.h"

namespace mlf {

class MlfWithHitInfo_Hypothesis : public Hypothesis
{
	public:
		friend std::ostream& operator<< (std::ostream& oss, const MlfWithHitInfo_Hypothesis& r) {
			oss << *static_cast<const Hypothesis*>(&r) << " " << r.IsHit();
			return oss;
		}
};

class MlfWithHitInfo
{
	public:
		static void Save(Mlf<Hypothesis>& mlf, const std::string& output_filename) {
			Mlf<MlfWithHitInfo_Hypothesis> *pMlfOut = (Mlf<MlfWithHitInfo_Hypothesis>*)&mlf;
			pMlfOut->Save(output_filename);
		}
};

} // namespace mlf

#endif
