#ifndef TERMLIST_H
#define TERMLIST_H

//#include <boost/unordered_set.hpp>
#include <unordered_set>
#include "container_interface.h"

class Termlist : public ContainerInterface< std::unordered_set<std::string> > {
	public:
		Termlist() {}
		Termlist(const std::string& filename) { Load(filename); }
		inline bool Contains(const std::string word) const {return mContainer.find(word) != mContainer.end();}
		void Add(const std::string& term) {
			mContainer.insert(term);
		}
		void Load(const std::string& filename) {
			std::ifstream f(filename.c_str());
			if ( !f.good() ) {
				throw std::runtime_error("Error opening file '"+filename+"'");
			}
			while (!f.eof()) {
				std::string term;
				f >> term;
				if (term.length() > 0) {
					Add(term);
				}
			}
			f.close();
		}
};

#endif

