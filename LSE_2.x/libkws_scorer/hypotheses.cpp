#include "hypotheses.h"

using namespace std;
using namespace mlf;

//--------------------------------------------------
// Hypothesis
//--------------------------------------------------

// IsHit {{{

string Hypothesis::IsHitFunPtr2String(Hypothesis::IsHitFunPtr ptr) {
	return 
		ptr == &Hypothesis::IsHit_point_middle_of_reference ? "MID_REF_IN_RES"      :
		ptr == &Hypothesis::IsHit_point_middle_of_result    ? "MID_RES_IN_REF"      :
		ptr == &Hypothesis::IsHit_point_middle_of_both      ? "MID_BOTH"            :
		ptr == &Hypothesis::IsHit_half_sec_around_reference ? "HALF_SEC_AROUND_REF" :
		throw runtime_error("ERROR: IsHitFunPtr2String(): unknown function pointer");
}

Hypothesis::IsHitFunPtr Hypothesis::String2IsHitFunPtr(string str) {
	return 
		str == "MID_REF_IN_RES"      ? &Hypothesis::IsHit_point_middle_of_reference :
		str == "MID_RES_IN_REF"      ? &Hypothesis::IsHit_point_middle_of_result    :
		str == "MID_BOTH"            ? &Hypothesis::IsHit_point_middle_of_both      :
		str == "HALF_SEC_AROUND_REF" ? &Hypothesis::IsHit_half_sec_around_reference :
		throw runtime_error("ERROR: String2IsHitFunPtr(): unknown name '"+str+"'. Supported names are MID_REF_IN_RES, MID_RES_IN_REF, MID_BOTH, HALF_SEC_AROUND_REF");
}

std::string Hypothesis::IsHitFunGetStringNames() {
	return "MID_REF_IN_RES | MID_RES_IN_REF | MID_BOTH | HALF_SEC_AROUND_REF";
}

bool Hypothesis::IsHit_point_middle_of_reference(const mlf::MlfRecord* recRef) const
{
	assert(recRef && recRef->GetWord() == GetWord());
	unsigned long mid = recRef->GetStartTime() + (recRef->GetEndTime() - recRef->GetStartTime()) / 2;
	return this->GetStartTime() <= mid && mid <= this->GetEndTime();
}

bool Hypothesis::IsHit_point_middle_of_result(const mlf::MlfRecord* recRef) const
{
	assert(recRef && recRef->GetWord() == GetWord());
	unsigned long mid = this->GetStartTime() + (this->GetEndTime() - this->GetStartTime()) / 2;
	return recRef->GetStartTime() <= mid && mid <= recRef->GetEndTime();
}

bool Hypothesis::IsHit_point_middle_of_both(const mlf::MlfRecord* recRef) const
{
	assert(recRef && recRef->GetWord() == GetWord());
	return IsHit_point_middle_of_reference(recRef) &&
	       IsHit_point_middle_of_result   (recRef);
}

bool Hypothesis::IsHit_half_sec_around_reference(const mlf::MlfRecord* recRef) const
{
	assert(recRef && recRef->GetWord() == GetWord());
	return recRef->GetStartTime() - 50 <= this->GetEndTime() 
		&& recRef->GetEndTime()   + 50 >= this->GetStartTime();
}

// }}}

/*
//--------------------------------------------------
// Paths2Words2Hypotheses
//--------------------------------------------------
void Paths2Words2Hypotheses::Fill(const mlf::Mlf& mlfRef, const mlf::Mlf& mlfRes)
{
	for (Mlf::const_iterator it = mlfRes.begin(); it != mlfRes.end(); it++) {
		const string& path = it->first;
		const File* f_res = it->second;
		const File* f_ref = mlfRef.GetFile(path);
		if (!f_ref) {
			cerr << "Warning: file '"<<path<<"' not found in the reference MLF" << endl;
		}

		Words2Hypotheses* w2h = this->Find(path);
		if (!w2h) {
			w2h = new Words2Hypotheses;
			mContainer.insert(make_pair(path, w2h));
		}
		w2h->Fill(f_ref, f_res);
	}
}

//--------------------------------------------------
// Words2Hypotheses
//--------------------------------------------------
void Words2Hypotheses::Fill(const mlf::Mlf& mlfRef, const mlf::Mlf& mlfRes)
{
	for (Mlf::const_iterator it = mlfRes.begin(); it != mlfRes.end(); it++) {
		const string& path = it->first;
		const File* f_res = it->second;
		const File* f_ref = mlfRef.GetFile(path);
		if (!f_ref) {
			cerr << "Warning: file '"<<path<<"' not found in the reference MLF" << endl;
		}
		Fill(f_ref, f_res);
	}
}

void Words2Hypotheses::Fill(const mlf::File* fRef, const mlf::File* fRes)
{
	for (Word2Records::const_iterator it = fRes->GetWord2Records().begin(); it != fRes->GetWord2Records().end(); it++) {
		const std::string& word = it->first;
		const Records* recs_ref = fRef ? fRef->GetRecords(word) : NULL;
		const Records* recs_res = it->second;
		if (!recs_ref) {
//			cerr << "Warning: word '"<<word<<"' not found in file '"<<fRes->GetPath()<<"' in the reference MLF" << endl;
		}

		Hypotheses* hyps = Find(word);
		if (!hyps) {
			hyps = new Hypotheses;
			mContainer.insert(make_pair(word, hyps));
		}
		hyps->Fill(recs_ref, recs_res);
	}
}

//--------------------------------------------------
// Hypotheses
//--------------------------------------------------
void Hypotheses::Fill(const mlf::Records* recsRef, const mlf::Records* recsRes)
{
	for (Records::const_iterator it = recsRes->begin(); it != recsRes->end(); it++) {
		const MlfRecord* rec_res = *it;

		Hypothesis* hyp = new Hypothesis(rec_res);
		hyp->AnalyzeAndFill(recsRef);
		Add(hyp);
	}
}
*/
