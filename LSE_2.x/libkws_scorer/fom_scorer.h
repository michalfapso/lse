#ifndef _FOM_SCORER_H_
#define _FOM_SCORER_H_

#include "foreach.h"
#include "exception.h"
#include "mlf.h"
#include "hypotheses.h"
#include "termlist.h"
#include "fom.h"
#include "warning.h"

class FomScorer {
	public:
		typedef ReferenceMlfRecord                RecRef;
		typedef Hypothesis                        RecRes;

		typedef mlf::MlfRecords<RecRef>           RecsRef;
		typedef mlf::MlfRecords<RecRes>           RecsRes;

		typedef mlf::MlfWord2Records<RecRef>      W2R_Ref;
		typedef mlf::MlfWord2Records<RecRes>      W2R_Res;

		typedef mlf::MlfPath2Records<RecRef>      P2R_Ref;
		typedef mlf::MlfPath2Records<RecRes>      P2R_Res;

		typedef mlf::MlfWord2Path2Records<RecRef> W2P2R_Ref;
		typedef mlf::MlfWord2Path2Records<RecRes> W2P2R_Res;

		static unsigned int GetOccCount(const W2R_Ref& w2rRef, const Termlist& termlist) 
		{
			unsigned int occs = 0;
			foreach (const std::string& term, termlist)
			{
				RecsRef* recs_ref = w2rRef.Find(term);
				if (recs_ref) {
					occs += recs_ref->size();
				}
			}
			return occs;
		}

		static Fom ComputeFom(
				W2R_Ref&        w2rRef, 
				W2R_Res&        w2rRes,
				const Termlist& termlist,
				float           durationHours)
		{
			unsigned int ref_occs_count_all_terms = GetOccCount(w2rRef, termlist);
			Fom fom_overall(durationHours, ref_occs_count_all_terms);

			// process the results mlf
			foreach (const std::string& term, termlist)
			{
				//if (term != "AGAINST") { continue; }
				RecsRes* recs_res = w2rRes.Find(term);
				RecsRef* recs_ref = w2rRef.Find(term);
				if (!recs_res) WARNING("Term '"+term+"' is in termlist, but it was not found in results");
				if (!recs_ref) WARNING("Term '"+term+"' is in termlist, but it was not found in reference");

				Fom fom(durationHours, ref_occs_count_all_terms);
				fom.FillFromRecords(recs_ref, recs_res);
				fom.ComputeFom();
				fom.PrintFom(term, std::cout);

				fom_overall.Add(fom);
			}
			fom_overall.ComputeFom();
			fom_overall.PrintFom("Overall", std::cout);
			return fom_overall;
		}

		static Fom ComputeFomPooled(
				W2R_Ref&        w2rRef, 
				W2R_Res&        w2rRes,
				const Termlist& termlist,
				float           durationHours)
		{
			std::cerr << "ERROR: ComputeFomPooled() is not implemented yet!" << std::endl;
			exit(1);

			unsigned int ref_occs_count_all_terms = GetOccCount(w2rRef, termlist);
			Fom fom_overall(durationHours, ref_occs_count_all_terms);

			// process the results mlf
			foreach (const std::string& term, termlist)
			{
				//if (term != "AGAINST") { continue; }
				RecsRes* recs_res = w2rRes.Find(term);
				RecsRef* recs_ref = w2rRef.Find(term);
				if (!recs_res) WARNING("Term '"+term+"' is in termlist, but it was not found in results");
				if (!recs_ref) WARNING("Term '"+term+"' is in termlist, but it was not found in reference");

				Fom fom(durationHours, ref_occs_count_all_terms);
				fom.FillFromRecords(recs_ref, recs_res);
				fom.ComputeFom();
				fom.PrintFom(term, std::cout);

				fom_overall.Add(fom);
			}
			fom_overall.ComputeFom();
			fom_overall.PrintFom("Overall", std::cout);
			return fom_overall;
		}

		static Fom ComputeFomPerFile(
				W2R_Ref&        w2rRef,
				W2R_Res&        w2rRes,
				const Termlist& termlist,
				float           durationHours)
		{
			unsigned int ref_occs_count_all_terms = GetOccCount(w2rRef, termlist);
			// Convert "word->hypotheses" map into "word->path->hypotheses" map
			W2P2R_Ref w2p2r_ref(w2rRef);
			W2P2R_Res w2p2r_res(w2rRes);

			Fom fom_overall(durationHours, ref_occs_count_all_terms);
			foreach (const std::string& term, termlist)
			{
				// if (term != "TALKING") continue;
				P2R_Res* p2r_res = w2p2r_res.Find(term);
				P2R_Ref* p2r_ref = w2p2r_ref.Find(term);
				if (!p2r_res) { WARNING("Term '"+term+"' is in termlist, but it was not found in results"); }
				if (!p2r_ref) { WARNING("Term '"+term+"' is in termlist, but it was not found in reference"); continue; }

				Fom fom_term(durationHours, ref_occs_count_all_terms);

				//std::cerr << "p2r_res:" << (void*)p2r_res << std::endl;
				foreach_pair (const std::string& path, RecsRef* recs_ref, *p2r_ref) {
					//std::cerr << "path:" << path << " recs_res:" << (void*)recs_res << std::endl;
					RecsRes* recs_res = p2r_res ? p2r_res->Find(path) : NULL;
					//if (!recs_res) { continue; }

					Fom fom_term_file(durationHours, ref_occs_count_all_terms);
					fom_term_file.FillFromRecords(recs_ref, recs_res);
					fom_term_file.ComputeFom();
					fom_term_file.PrintFom(term+" "+path, std::cout);

					fom_term.Add(fom_term_file);
				}

				//RecsRef* recs_ref_term_all = w2rRef.Find(term);
				//if (recs_ref_term_all && fom_term.GetRefOccCount() != recs_ref_term_all->size()) {
					// This may happen also when for some utterance, a query is in reference, but not in results, so it is not a fatal error.
				//}
				
				fom_term.ComputeFom();
				fom_term.PrintFom(term+" per_file", std::cout);

				fom_overall.Add(fom_term);
			}
			fom_overall.ComputeFom();
			fom_overall.PrintFom("Overall per_file", std::cout);
			return fom_overall;
		}
};

#endif

