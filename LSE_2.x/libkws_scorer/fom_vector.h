#ifndef _FOM_VECTOR_H_
#define _FOM_VECTOR_H_

#include <vector>
#include "container_interface.h"

class FomVector : public ContainerInterface< std::vector<unsigned int> >
{
	public:
		FomVector() {}
		FomVector(size_t size) { mContainer.resize(size); }
		inline unsigned int Get(unsigned int idx) const {
			assert(idx < mContainer.size());
			return mContainer[idx];
		}
		inline void Set(unsigned int idx, unsigned int val) {
			assert(idx < mContainer.size());
			mContainer[idx] = val;
		}
		void Add(const FomVector& v) {
			for (unsigned int i=0; i<v.size(); i++) {
				mContainer[i] += v.Get(i);
			}
		}
		inline void resize(unsigned int size) { mContainer.resize(size); }
		friend std::ostream& operator<<(std::ostream& oss, const FomVector& v) {
			for (unsigned int i=0; i<v.size(); i++) {
				oss << v.mContainer[i] << " ";
			}
			return oss;
		}
};

#endif
