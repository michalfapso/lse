#ifndef _DETERMINE_HITS_H_
#define _DETERMINE_HITS_H_

#include "mlf.h"
#include "hypotheses.h"
#include "foreach.h"

class DetermineHits {
	public:
		typedef ReferenceMlfRecord                 RecRef;
		typedef Hypothesis                         RecRes;

		typedef mlf::MlfRecords<RecRef>            RecsRef;
		typedef mlf::MlfRecords<RecRes>            RecsRes;

		typedef mlf::MlfWord2Records<RecRef>       W2R_Ref;
		typedef mlf::MlfWord2Records<RecRes>       W2R_Res;

		static void Process(
				W2R_Ref& w2rRef,
				W2R_Res& w2rRes,
				RecRes::IsHitFunPtr isHitFunPtr)
		{
			// For each word
			foreach_pair (const std::string& word, RecsRes* recs_res, w2rRes)
			{
				assert(recs_res);
				RecsRef* recs_ref = w2rRef.Find(word);
				Process(recs_ref, recs_res, isHitFunPtr);
			}
		}

		static void Process(
				RecsRef* recsRef,
				RecsRes* recsRes,
				RecRes::IsHitFunPtr isHitFunPtr)
		{
			// Reset reference hits counts
			if (recsRef) {
				foreach (RecRef* rec, *recsRef) {
					rec->ResetHitsCount();
					rec->ResetHypHit();
				}
			}
			if (!recsRef || !recsRes) { return; }
			// Determine each hypothesis as HIT or FA
			foreach (RecRes* hyp, *recsRes) {
				hyp->DetermineIsHit(recsRef, isHitFunPtr);
			}
		}

		static void CheckMultipleReferenceHits(W2R_Ref& w2rRef) 
		{
			//for (W2R_Ref::iterator it = w2rRef.begin(); it != w2rRef.end(); it++) {
			foreach_pair (const std::string& word, RecsRef* recs, w2rRef) {
				assert(recs);
				foreach (RecRef* rec, *recs) {
					assert(rec);
					if (rec->GetHitsCount() >= 1) {
						if (rec->GetHitsCount() > 1) {
							DBG("MULTIPLE HITS: word="<<word<<" "<<*rec);
						}
					}
				}
			}
		}
};

#endif

