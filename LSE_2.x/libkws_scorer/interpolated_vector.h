#ifndef INTERPOLATED_VECTOR_H
#define INTERPOLATED_VECTOR_H

#include <boost/static_assert.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_floating_point.hpp>

#include "container_interface.h"
#include "dbg.h"

template < std::vector<typename T> >
class InterpolatedVector
{
	protected:
		typedef std::vector<T> Vec;
		BOOST_STATIC_ASSERT(is_integral<T>::value || is_floating_point<T>::value);
		const Vec& mrVec;
	public:
		InterpolatedVector(const Vec& vec) : mrVec(vec) {}
		float Interpolate(float x)
		{
			assert(x > 0);
			int idx_lower = floor(x);
			int idx_higher = ceil(x);
			float val_lower = mrVec[idx_lower];
			float val_higher = mrVec[idx_higher];
			float res = val_lower + (val_higher - val_lower) / 2;
			DBG("Interpolate() lower:"<<idx_lower<<" higher:"<<idx_higher<<" res:"<<res);
			return res;
		}
};

#endif
