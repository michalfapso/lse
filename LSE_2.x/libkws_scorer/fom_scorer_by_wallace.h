#ifndef _FOM_SCORER_BY_WALLACE_H_
#define _FOM_SCORER_BY_WALLACE_H_

#include "fom.h"
#include "fom_scorer.h"
#include "foreach.h"
#include "warning.h"

class FomScorerByWallace : public FomScorer
{
	public:
		float ComputeFomWallaceMy(
				W2R_Ref&        w2rRef, 
				W2R_Res&        w2rRes,
				const Termlist& termlist,
				float           durationHours)
		{
			// process the results mlf
			float A = (float)10 * termlist.size() * durationHours;
			float fom = (float)1/A;
			DBG("A = "<<A);
			DBG("fom = "<<fom);

			unsigned int fom_N = Fom::GetFomN(durationHours);
			foreach (const std::string& word, termlist)
			{
				if (word != "ACTUALLY") { continue; }
				RecsRes* recs_res = w2rRes.Find(word);
				RecsRef* recs_ref = w2rRef.Find(word);
				if (!recs_res) THROW("ERROR: Word '"+word+"' is in termlist, but it was not found in results");
				if (!recs_ref) THROW("ERROR: Reference for word '"+word+"' not found!");
				recs_res->SortByScore();
		//		DBG("Word: "<<word);

				//float fom_word = (float)1/(10*durationHours);
				unsigned int fa_count = 0;
				// For each false alarm
				foreach (Hypothesis* rec_fa, *recs_res) {
					if (rec_fa->IsHit()) { continue; } // skip hits
					if (fa_count++ > fom_N) { break; }
		//			DBG("  FA: "<<*rec_fa);
					// For each hit
					foreach (Hypothesis* rec_hit, *recs_res) {
						if (!rec_hit->IsHit()) { continue; } // skip false alarms
						if (rec_hit->GetScore() < rec_fa->GetScore()) { break; }
		//				DBG("Hit: "<<*rec_hit);

						float h_e_k = (float)1 / (termlist.size() * recs_ref->size());
						fom *= h_e_k;
		//				DBG("h_e_k = "<<h_e_k);
		//				DBG("fom = "<<fom);

						unsigned int fa_highscore_count = 0;

						if (rec_fa->GetScore() > rec_hit->GetScore()) {
		//					DBG("  high");
							fa_highscore_count ++;
						}
					}
					//DBG("fa_highscore_count = "<<fa_highscore_count);
					//DBG("A - fa_highscore_count = " << (A - fa_highscore_count));
					//fom *= max(0.0f, A - fa_highscore_count);
					DBG("fom = "<<fom);
				}
			}
			DBG("TOTAL FOM Wallace My: "<<fom);
		//	ComputeFomPerFile(w2rRef, w2rRes, termlist, durationHours);
			return fom;
		}

		float ComputeFomWallace(
				W2R_Ref&        w2rRef, 
				W2R_Res&        w2rRes,
				const Termlist& termlist,
				float           durationHours)
		{
			// process the results mlf
			float A = (float)10 * termlist.size() * durationHours;
			float fom = (float)1/A;
			DBG("A = "<<A);
			DBG("fom = "<<fom);

		//	for (MlfWord2Records<Hypothesis>::iterator it = w2rRes.begin(); it != w2rRes.end(); it++)
			foreach (const std::string& word, termlist)
			{
				if (word != "ACTUALLY") { continue; }
				RecsRes* recs_res = w2rRes.Find(word);
				RecsRef* recs_ref = w2rRef.Find(word);
				if (!recs_res) THROW("ERROR: Word '"+word+"' is in termlist, but it was not found in results");
				if (!recs_ref) THROW("ERROR: Reference for word '"+word+"' not found!");
				recs_res->SortByScore();
				//DBG("Word: "<<word);

				// For each hit
				foreach (Hypothesis* rec_hit, *recs_res) {
					if (!rec_hit->IsHit()) { continue; } // skip false alarms
					//DBG("Hit: "<<*rec_hit);

					float h_e_k = (float)1 / (termlist.size() * recs_ref->size());
					fom *= h_e_k;
					//DBG("h_e_k = "<<h_e_k);
					//DBG("fom = "<<fom);

					unsigned int fa_highscore_count = 0;
					// For each false alarm
					foreach (Hypothesis* rec_fa, *recs_res) {
						if (rec_fa->IsHit()) { continue; } // skip hits
		//				DBG("  FA: "<<*rec_fa);

						if (rec_fa->GetScore() > rec_hit->GetScore()) {
		//					DBG("  high");
							fa_highscore_count ++;
						}
					}
					//DBG("fa_highscore_count = "<<fa_highscore_count);
					//DBG("A - fa_highscore_count = " << (A - fa_highscore_count));
					fom *= std::max(0.0f, A - fa_highscore_count);
					//DBG("fom = "<<fom);
				}
			}
			DBG("TOTAL FOM Wallace: "<<fom);
		//	ComputeFomPerFile(w2rRef, w2rRes, termlist, durationHours);
			return fom;
		}
};
#endif
