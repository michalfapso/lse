#ifndef _FOM_H_
#define _FOM_H_

#include "online_average.h"
#include "fom_vector.h"
#include "foreach.h"

class Fom {
	public:
		Fom(float durationHours, unsigned int refOccCountAllTerms) : 
			mFom(0),
			mFomOracle(0),
			mDurationHours(durationHours),
			mN(GetFomN(durationHours)),
			mHitsCount(0), 
			mFACount(0), 
			mRefOccCount(0),
			mRefOccCountAllTerms(refOccCountAllTerms),
			mFomVector(mN+1)
		{}
	
		template <class TRecRef, class TRecRes>
		void FillFromRecords(
				const mlf::MlfRecords<TRecRef>* recsRef, 
				      mlf::MlfRecords<TRecRes>* recsRes)
		{
			assert(recsRef);
			mRefOccCount = recsRef->size();
			
			if (recsRes) {
				recsRes->SortByScore();
				foreach (TRecRes* hyp, *recsRes) {
					//DBG("hyp: "<<*hyp<<" "<<(hyp->IsHit() ? "HIT" : "FA"));
					if (hyp->IsHit()) {
						if (hyp->IsUniqueHit()) {
							mHitsCount ++;
						}
					} else {
						if (mFomVector.size() > mFACount) {
							mFomVector.Set(mFACount, mHitsCount);
						}
						mFACount ++;
					}
				}
			}
			// Fill the rest of the false alarms array with total hits count
			for (unsigned int i=mFACount; i<mFomVector.size(); i++) {
				mFomVector.Set(i, mHitsCount);
			}
			// Recompute false alarms enough ratio
			mFAEnoughRatio.Reset(); 
			mFAEnoughRatio.Add(mFACount < mN ? (float)mFACount/mN : 1.0f);
		}

		void ComputeFom()
		{
			assert(mFomVector.size() == mN+1);

			mFom = 0;
			mFomOracle = 0;
			for (unsigned int i=1; i<=mN+1; i++) {
				float acc = 0;
				float acc_oracle = 0;
				if (mRefOccCount > 0) {
					acc        = (float)mFomVector.Get(i-1) / mRefOccCount * 100;
					acc_oracle = (float)mHitsCount          / mRefOccCount * 100;
				}
				if (i == mN+1) {
					acc        *= (float)10*mDurationHours - mN;
					acc_oracle *= (float)10*mDurationHours - mN;
				}
				mFom       += acc;
				mFomOracle += acc_oracle;
			}
			mFom       *= (float)1/(10*mDurationHours);
			mFomOracle *= (float)1/(10*mDurationHours);
		}

		inline float        GetFom()           const { return mFom; }
		inline float        GetFomOracle()     const { return mFomOracle; }
		inline unsigned int GetHitsCount()     const { return mHitsCount; }
		inline unsigned int GetFACount()       const { return mFACount; }
		inline unsigned int GetRefOccCount()   const { return mRefOccCount; }
		inline float        GetFAEnoughRatio() const { return mFAEnoughRatio.GetValue(); }
		inline const FomVector& GetFomVector() const { return mFomVector; }

		void Add(const Fom& from) {
			mHitsCount       += from.mHitsCount;
			mFACount         += from.mFACount;
			mRefOccCount     += from.mRefOccCount;
			mFAEnoughRatio.Add( from.mFAEnoughRatio.GetValue());
			mFomVector.Add(     from.mFomVector);
		}

		void PrintFom(const std::string& word, std::ostream& oss) {
			using namespace std;
			oss << setw(23) << word <<": " 
				<< setw(8) << mHitsCount <<" "
				<< setw(8) << mFACount <<" "
				<< setw(8) << mRefOccCount <<" "
				<< setw(8) << setprecision(2) << fixed << mFom << " "
				<< setw(8) << setprecision(2) << fixed << (mFom * mRefOccCount / mRefOccCountAllTerms) << " "
				<< setw(8) << setprecision(2) << fixed << mFomOracle << " "
				<< setw(8) << setprecision(2) << fixed << (mFomOracle * mRefOccCount / mRefOccCountAllTerms) << " "
				<< setw(8) << setprecision(2) << fixed << mFAEnoughRatio.GetValue()
				<< endl;
		}

		static inline unsigned int GetFomN(float durationHours) {
			return ceil((float)10*durationHours - 0.5);
		}

	protected:
		float        mFom;
		float        mFomOracle;
		float        mDurationHours;
		unsigned int mN;
		unsigned int mHitsCount;
		unsigned int mFACount;
		unsigned int mRefOccCount;
		unsigned int mRefOccCountAllTerms;
		OnlineAverage<float> mFAEnoughRatio;
		FomVector mFomVector;
};

#endif
