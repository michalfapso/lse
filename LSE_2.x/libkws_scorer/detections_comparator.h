#ifndef _DETECTIONS_COMPARATOR_H_
#define _DETECTIONS_COMPARATOR_H_

class DetectionsComparator {
	public:
		typedef ReferenceMlfRecord                 RecRef;
		typedef Hypothesis                         RecRes;

		typedef mlf::MlfRecords<RecRef>            RecsRef;
		typedef mlf::MlfRecords<RecRes>            RecsRes;

		typedef mlf::MlfWord2Records<RecRef>       W2R_Ref;
		typedef mlf::MlfWord2Records<RecRes>       W2R_Res;

		static void Compare(
				W2R_Ref& w2rRef,
				W2R_Res& w2rRes1,
				W2R_Res& w2rRes2,
				RecRes::IsHitFunPtr isHitFunPtr)
		{
			using namespace std;
			// For each word
			foreach_pair (const std::string& word, RecsRef* recs_ref, w2rRef)
			{
				assert(recs_ref);
				cout << "Word: "<<word << endl;
				RecsRes* recs_res1 = w2rRes1.Find(word);
				RecsRes* recs_res2 = w2rRes2.Find(word);
				Compare(recs_ref, recs_res1, recs_res2, isHitFunPtr);
			}
		}

		static void Compare(
				RecsRef* recsRef,
				RecsRes* recsRes1,
				RecsRes* recsRes2,
				RecRes::IsHitFunPtr isHitFunPtr)
		{
			using namespace std;

			if (!recsRef || (!recsRes1 && !recsRes2) ) { return; }
			// Determine each hypothesis as HIT or FA
			foreach (RecRef* ref, *recsRef) {
				cout << "  ref: "<<*ref << endl;
				foreach(RecRes* hyp, *recsRes1) {
					if (hyp->GetRefHit() == ref) {
						cout << "    res1: "<<*hyp << endl;
					}
				}
				foreach(RecRes* hyp, *recsRes2) {
					if (hyp->GetRefHit() == ref) {
						cout << "    res2: "<<*hyp << endl;
					}
				}
			}
		}

};

#endif
