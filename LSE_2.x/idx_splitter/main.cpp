#include <iostream>
#include <fstream>
#include "../indexer/latindexer.h"

using namespace std;
using namespace lse;

int main(int argc, char *argv[]) {

/*	
	ifstream in("_SEARCH.IDX", ios::binary);
	if (!in.good()) {
			return (2);
	}

	
	ofstream out1("_SEARCH.IDX.1", ios::binary);
	ofstream out2("_SEARCH.IDX.2", ios::binary);
	ofstream out3("_SEARCH.IDX.3", ios::binary);
	ofstream out4("_SEARCH.IDX.4", ios::binary);
	
	
	
	LatIndexer::Record latRec;

	// read all nodes
	while(!in.eof()) {
			in.read(reinterpret_cast<char *>(&latRec), sizeof(latRec));
			switch (latRec.meetingID) {
				case 1:
				case 2:
				case 3:
				case 4:
					out1.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));
					break;
				case 5:
				case 6:
				case 7:
				case 8:
					out2.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));
					break;
				case 9:
				case 10:
				case 11:
				case 12:
					out3.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));
					break;
				case 13:
				case 14:
				case 15:
				case 16:
					out4.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));
					break;
			}
			// add record to container
	}

	in.close();
	out1.close();
	out2.close();
	out3.close();
	out4.close();
*/
	ifstream in;
	ofstream out1;
	ofstream out2;
	ofstream out3;
	ofstream out4;
	in.open("_MEETING.IDX", ios::binary);
	out1.open("_MEETING.IDX.1", ios::binary);
	out2.open("_MEETING.IDX.2", ios::binary);
	out3.open("_MEETING.IDX.3", ios::binary);
	out4.open("_MEETING.IDX.4", ios::binary);
	int meetingID;
	string meeting;
	char terminator = 0;

	while(!in.eof()) {
		in.read(reinterpret_cast<char*>(&meetingID), sizeof(meetingID));
		cout << meetingID << endl;
		
		meeting.clear();
		char c;
		in.get(c);
		
		while(c != 0) {				
			meeting += c;
			in.get(c);
		}
		cout << meeting << endl;

		switch (meetingID) {
			case 1:
			case 2:
			case 3:
			case 4:
				out1.write(reinterpret_cast<const char *>(&meetingID), sizeof(meetingID));
				out1 << meeting;
				out1.write(reinterpret_cast<const char *>(&terminator), sizeof(terminator)); // string terminator
				break;
			case 5:
			case 6:
			case 7:
			case 8:
				out2.write(reinterpret_cast<const char *>(&meetingID), sizeof(meetingID));
				out2 << meeting;
				out2.write(reinterpret_cast<const char *>(&terminator), sizeof(terminator)); // string terminator
				break;
			case 9:
			case 10:
			case 11:
			case 12:
				out3.write(reinterpret_cast<const char *>(&meetingID), sizeof(meetingID));
				out3 << meeting;
				out3.write(reinterpret_cast<const char *>(&terminator), sizeof(terminator)); // string terminator
				break;
			case 13:
			case 14:
			case 15:
			case 16:
				out4.write(reinterpret_cast<const char *>(&meetingID), sizeof(meetingID));
				out4 << meeting;
				out4.write(reinterpret_cast<const char *>(&terminator), sizeof(terminator)); // string terminator
				break;
		}
		
	}
	in.close();
	out1.close();
	out2.close();
	out3.close();
	out4.close();
	return 0;
}
