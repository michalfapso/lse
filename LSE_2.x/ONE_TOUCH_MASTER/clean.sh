#!/bin/bash

if [ `pwd | grep ONE_TOUCH_MASTER` ]; then
	exit
fi

rm -r idx lattices.htk.lvcsr lattices.system.lvcsr lexicon lists scripts tmp sge
