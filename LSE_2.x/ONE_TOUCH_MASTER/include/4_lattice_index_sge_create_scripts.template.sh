function template_lattice_index_sge_create_scripts()
{

local LVCSR=$1
local PHN=$2
local ROOT_DIR=$3
local LATDIR_LVCSR=$4
local LATDIR_PHN=$5

for type in lvcsr phn; do
	if [ $type == "lvcsr" ]; then
		if [ $LVCSR != "YES" ]; then
			continue
		fi
		local type_extension=$HTK_LATTICES_LVCSR_EXTENSION
		local type_latdir=$LATDIR_LVCSR
	fi
	if [ $type == "phn" ]; then
		if [ $PHN != "YES" ]; then
			continue
		fi
		local type_extension=$HTK_LATTICES_PHN_EXTENSION
		local type_latdir=$LATDIR_PHN
	fi

	cat << EOFF > $ROOT_DIR/scripts/4_lattice_index_sge_create_scripts_$type.sh
#!/bin/bash

	if [ -e $ROOT_DIR/sge/lattice_indexing_$type ]; then
		rm $ROOT_DIR/sge/lattice_indexing_$type
	fi
	mkdir $ROOT_DIR/sge/lattice_indexing_$type
	echo -n > $ROOT_DIR/sge/lattice_indexing_$type.scriptlist

	for i in \`cat $ROOT_DIR/lists/$type_latdir.rel.latlist\`; do
		script_name=\`basename "\$i" | sed 's/$type_extension//'\`.sh
#		echo "\$script_name";
		script=$ROOT_DIR/sge/lattice_indexing_$type/\$script_name
		cat << EOF > \$script
#!/bin/bash
$ROOT_DIR/scripts/5_lattice_index_$type.sh $ROOT_DIR/$type_latdir/\$i
EOF
		chmod a+x \$script

		echo \$script >> $ROOT_DIR/sge/lattice_indexing_$type.scriptlist
	done

EOFF

done

}
