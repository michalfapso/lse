function template_search()
{

local ROOT_DIR=$1
local SEARCHER_ARGS="$2"
local SEARCH_SRV=$3

cat << EOF > $ROOT_DIR/scripts/8_search.sh
#!/bin/bash

export query=\$1
args="$SEARCHER_ARGS"

eval $SEARCH_SRV \$args; exit

EOF

}

