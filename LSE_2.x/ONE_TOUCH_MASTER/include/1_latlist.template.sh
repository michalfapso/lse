function template_latlist()
{

local LVCSR=$1
local PHN=$2
local ROOT_DIR=$3
local LATDIR_LVCSR=$4
local LATDIR_PHN=$5

if [ $LVCSR == "YES" ]; then
cat << EOF > $ROOT_DIR/scripts/1_latlists_lvcsr.sh
#!/bin/bash

# directory listing -> .latlist
find $ROOT_DIR/$LATDIR_LVCSR -name '*.lat*' -follow \
	| tee $ROOT_DIR/lists/$LATDIR_LVCSR.latlist \
	| sed 's/^.*\///' \
	> $ROOT_DIR/lists/$LATDIR_LVCSR.rel.latlist

EOF
fi

if [ $PHN == "YES" ]; then
cat << EOF > $ROOT_DIR/scripts/1_latlists_phn.sh
#!/bin/bash

# directory listing -> .latlist
find $ROOT_DIR/$LATDIR_PHN -name '*.lat*' -follow \
	| tee $ROOT_DIR/lists/$LATDIR_PHN.latlist \
	| sed 's/^.*\///' \
	> $ROOT_DIR/lists/$LATDIR_PHN.rel.latlist

EOF
fi

}
