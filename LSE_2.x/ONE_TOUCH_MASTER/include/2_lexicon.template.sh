function template_lexicon()
{

local LVCSR=$1
local PHN=$2
local ROOT_DIR=$3
local SCRIPTS_DIR=$4
local DICTIONARY_LOCAL_LVCSR=$5
local DICTIONARY_LOCAL_PHN=$6

if [ $LVCSR == "YES" ]; then
cat << EOF > $ROOT_DIR/scripts/2_lexicon_lvcsr.sh
#!/bin/bash

$SCRIPTS_DIR/dct2LSElexicon.sh $DICTIONARY_LOCAL_LVCSR $LEXICON_TXT2BIN
EOF
fi


if [ $PHN == "YES" ]; then
local DICTIONARY_PHN_BASE=`basename $DICTIONARY_LOCAL_PHN`
cat << EOF > $ROOT_DIR/scripts/2_lexicon_phn.sh
#!/bin/bash

#$SCRIPTS_DIR/dct2phonemelist.pl $DICTIONARY_LOCAL_PHN > $DICTIONARY_LOCAL_PHN.phonemes

$SCRIPTS_DIR/phonemelist2ngrams.pl 3 $DICTIONARY_LOCAL_PHN > $DICTIONARY_LOCAL_PHN.ngrams

# create a !NULL word
echo "!NULL" >  $DICTIONARY_LOCAL_PHN.words
echo "<sil>" >> $DICTIONARY_LOCAL_PHN.words
echo "<s>" >> $DICTIONARY_LOCAL_PHN.words
echo "</s>" >> $DICTIONARY_LOCAL_PHN.words

# add all phonemes
cat $DICTIONARY_LOCAL_PHN >> $DICTIONARY_LOCAL_PHN.words

# add all triphones
cat $DICTIONARY_LOCAL_PHN.ngrams >> $DICTIONARY_LOCAL_PHN.words

# associate ID for each word
cat $DICTIONARY_LOCAL_PHN.words | nl -n ln > $DICTIONARY_LOCAL_PHN.lexicon.IDX.txt

# convert lexicon from txt format to binary format
LSElexicon_txt2bin $DICTIONARY_LOCAL_PHN.lexicon.IDX.txt $DICTIONARY_LOCAL_PHN.lexicon.IDX

# convert binary lexicon back to txt format (just for fun ;o)
# LSEindexer -lexicon-in $DATA_ROOT/$LEXICON_TXT.IDX -print-lexicon > $DATA_ROOT/$LEXICON_TXT.IDX.txt

EOF
fi

}
