function template_lattice_index()
{

local TYPE=$1
local ROOT_DIR=$2
local INDEXER=$3
local INDEXER_ARGS=$4

cat << EOF > $ROOT_DIR/scripts/5_lattice_index_$TYPE.sh
#!/bin/bash

LATTICE=\$1
if [ \$1 == "-latlist-in" ]; then
	LATTICE=\$2
fi

LATTICE_BASE=\`basename \$LATTICE .lat.gz\`

cmd="$INDEXER \
$INDEXER_ARGS \
\$@"

#echo "cmd: \$cmd" >> $ROOT_DIR/LOG/\$LATTICE_BASE.log

\$cmd
EOF


}
