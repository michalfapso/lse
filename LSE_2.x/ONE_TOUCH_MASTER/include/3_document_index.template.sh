function template_document_index()
{

local LVCSR=$1
local PHN=$2
local ROOT_DIR=$3

if [ $LVCSR == "YES" ]; then
	local lvcsr_latlist_concat="cat $ROOT_DIR/lists/$LATDIR_LVCSR.rel.latlist >> $ROOT_DIR/tmp/latlist.concat"
fi

if [ $PHN == "YES" ]; then
	local phn_latlist_concat="cat $ROOT_DIR/lists/$LATDIR_PHN.rel.latlist >> $ROOT_DIR/tmp/latlist.concat"
fi

cat << EOF > $ROOT_DIR/scripts/3_document_index.sh
#!/bin/bash

echo -n > $ROOT_DIR/tmp/latlist.$$.concat
$lvcsr_latlist_concat
$phn_latlist_concat

cat $ROOT_DIR/tmp/latlist.concat | sed 's/\..*//' | sort | uniq > $ROOT_DIR/tmp/latlist.merged

cat $ROOT_DIR/tmp/latlist.merged | sed 's/$/.binlat\t0/' | nl -nln > $ROOT_DIR/idx/_DOCUMENTS.IDX

rm $ROOT_DIR/tmp/latlist.concat
rm $ROOT_DIR/tmp/latlist.merged
EOF

}
