function template_search_sge_create_scripts()
{

local ROOT_DIR=$1
local TERMLIST=$2
local LANGUAGE=$3

cat << EOF > $ROOT_DIR/scripts/7_search_sge_create_scripts.pl
#!/usr/bin/perl

\$termlist = "$TERMLIST";
\$subdir = "$ROOT_DIR/search";
\$LANGUAGE = "$3";

\$runserverScript = "$ROOT_DIR/scripts/8_search.sh";
\$SGE_script_dir = "$ROOT_DIR/sge/search";
\$results_dir = "$ROOT_DIR/search/results";
\$LOG_dir = "\$results_dir/LOG";


system ("mkdir -p $ROOT_DIR/sge/search");
open (SCRIPT_LIST, "> $ROOT_DIR/sge/search.scriptlist");

system ("mkdir -p \$results_dir");
system ("mkdir -p \$LOG_dir");

open (TLIST,\$termlist);

while (\$line = <TLIST>)
{
#	print "\n\n";
	chomp(\$line);
	(\$termid,\$termtext) = \$line =~ /<term\s+.*termid="([^\"]*)">.*<termtext>(.*)<\/termtext>/;
	if (\$termid eq "") 
	{
		next;
	}
#	print STDERR "\$termid\n";
	\$termtext = uc(\$termtext); # upper-case

	\$cmd = \$runserverScript." \"&RES=1000000 &EM=true &FNC=0 &BNC=0 &ID=\$termid \\\\\\\\\\\\\\"\$termtext\\\\\\\\\\\\\\"\"";
	
	if (\$LOG_dir ne '') {
		\$termtext_ = \$termtext;
		\$termtext =~ s/\ /_/g;
		\$termtext =~ s/'/\\\\'/g;
		\$cmd .= " 2>\$LOG_dir/\${termid}_\${termtext}.log";
	} else {
		\$cmd .= ' 2>/dev/null';
	}
	\$cmd .= " >\$results_dir/\$termid.stdlist.xml";

#	print STDERR "\$SGE_script_dir/\$termid.sh\n";
	print SCRIPT_LIST "\$SGE_script_dir/\$termid.sh\n";
	open (SCRIPT_OUT, "> \$SGE_script_dir/\$termid.sh");
	print SCRIPT_OUT "#!/bin/bash\n\n";
	print SCRIPT_OUT "\$cmd\n";
	close (SCRIPT_OUT);
	system ("chmod a+x \$SGE_script_dir/\$termid.sh");
	next;

	open (RES,"\$cmd |");
	while (\$res = <RES>)
	{
		print \$res;
	}
}

close(SCRIPT_LIST);

EOF


}
