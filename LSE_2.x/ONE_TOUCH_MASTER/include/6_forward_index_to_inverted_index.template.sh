function template_forward_index_to_inverted_index()
{

local TYPE=$1
local ROOT_DIR=$2
local SEARCHIDX2TXT=$3
local SORT_BUFFER_SIZE=$4
local SEARCHIDXTXT_ADDPOINTERS=$5
local SEARCHIDXTXT2BIN=$6
local INDEXER=$7

local IDX_DIR=$ROOT_DIR/idx/$TYPE

cat << EOF > $ROOT_DIR/scripts/6_forward_index_to_inverted_index_$TYPE.sh
#!/bin/bash

echo -n "Generating forward indices list..."
find $IDX_DIR/fwd -name '_FWDSEARCH*.IDX' >$IDX_DIR/fwd.list
echo "done"


echo -n "Generating concatenated txt forward indices..."
echo -n "" > $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt
for i in \`cat $IDX_DIR/fwd.list\`; do
	$SEARCHIDX2TXT \$i >> $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt
done
echo "done"


echo -n "Sorting by confidence (buffer=$SORT_BUFFER_SIZE)...";
sort -S $SORT_BUFFER_SIZE -k2,2g -k3,3rg $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt > $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf
echo "done"


echo -n "Adding pointers to the index sorted by confidence...";
$SEARCHIDXTXT_ADDPOINTERS $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf > $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf.pointers
echo "done"


echo -n "Sorting by time (buffer=$SORT_BUFFER_SIZE)...";
sort -S $SORT_BUFFER_SIZE -k2,2g -k1,1g -k5,5g $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf.pointers > $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf.pointers.sort_time
echo "done"


echo -n "Converting a txt index to binary index..."
$SEARCHIDXTXT2BIN $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf $IDX_DIR/_CONCAT_FWDSEARCH.IDX.txt.sort_conf.pointers.sort_time $IDX_DIR/_INVSEARCH.IDX
echo "done"

echo -n "Generating index for search table which contains pointers from lexicon.wordID to search index file"
$INDEXER -generate-wordid-search-index $IDX_DIR/_INVSEARCH.IDX
echo "...done"
EOF

}
