#!/bin/bash

dct_filename=$1
LEXICON_TXT2BIN=$2

if [ $LEXICON_TXT2BIN == "" ]; then
	LEXICON_TXT2BIN="LSElexicon_txt2bin"
fi

echo "!NULL" >  $dct_filename.lexicon.words.txt;
echo "<sil>" >> $dct_filename.lexicon.words.txt;
echo "<s>" >> $dct_filename.lexicon.words.txt;
echo "</s>" >> $dct_filename.lexicon.words.txt;

cat $dct_filename | sed 's/\s.*//' | sed 's/\\//g' | sed 's/"//g' | sort | uniq >> $dct_filename.lexicon.words.txt

cat $dct_filename.lexicon.words.txt | sort | uniq | nl -n ln > $dct_filename.lexicon.txt

$LEXICON_TXT2BIN $dct_filename.lexicon.txt $dct_filename.lexicon.IDX
