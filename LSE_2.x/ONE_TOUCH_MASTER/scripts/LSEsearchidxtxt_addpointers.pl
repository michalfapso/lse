#!/usr/bin/perl

$searchidxtxt_filename_in = $ARGV[0];
if (@ARGV < 1)
{
	print STDERR "ERROR: arguments needed\n";
	exit;
}

$i=0;
$wordID_pred=-1;
open(IDXTXT,$searchidxtxt_filename_in)
	or die "cannot open file";
while(<IDXTXT>)
{
	chomp;
	($meetingID, $wordID, $conf, $startTime, $endTime) = $_ =~ /^([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)/;
	
	if ($wordID_pred != $wordID)
	{
		$i=0;
		$wordID_pred = $wordID;
	}

	print "$meetingID\t$wordID\t$conf\t$startTime\t$endTime\t$i\n";
	$i++;
}

