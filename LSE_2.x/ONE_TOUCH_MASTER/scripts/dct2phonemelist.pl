#!/usr/bin/perl

if (!@ARGV)
{
	print "Usage: $0 DICTIONARY_FILENAME > PHONEME_LIST_FILENAME\n";
	exit;
}

$dct_filename = $ARGV[0];

%phonemes_count = ();

open(DCT,$dct_filename);
while(<DCT>)
{
	chomp;
	($phonemes_str) = $_ =~ /^[^\s]+\s+(.+)$/;
#	@phonemes = split(/ /,$phonemes_str);
	foreach $phoneme (split(/ /,$phonemes_str))
	{
#		print "$phoneme ";
		$phoneme =~ s/\s+//g;
		if ($phoneme !~ /^\s*$/)
		{
			$phonemes_count{$phoneme}++;
		}
	}
}

foreach $phoneme (sort keys %phonemes_count)
{
	print "$phoneme\n";
}
