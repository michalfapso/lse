#!/usr/bin/perl

if (!@ARGV)
{
	print "Usage: $0 NGRAM_LENGTH PHONEME_LIST_FILENAME > TRIGRAM_LIST_FILENAME\n";
	exit;
}

$n = $ARGV[0];
$phonemelist_filename = $ARGV[1];

@phonemes = ();
open(PHNS,$phonemelist_filename);
while (<PHNS>)
{
	chomp;
	push (@phonemes, $_);
}


sub GenerateNgrams
{
	my $phonemes_array = shift;
	my $ngram = shift;
	my $length = shift;
	my $maxlen = shift;

	if ($length == $maxlen)
	{
		print "$ngram\n";
	}
	else
	{
		foreach $phn (@$phonemes_array)
		{
			GenerateNgrams($phonemes_array, ($ngram eq "" ? "" : "$ngram\_").$phn, $length+1, $maxlen);
		}
	}
}


GenerateNgrams(\@phonemes, "", 0, $n);
