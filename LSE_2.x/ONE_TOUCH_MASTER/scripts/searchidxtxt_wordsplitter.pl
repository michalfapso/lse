#!/usr/bin/perl

$searchidxtxt_in_filename = $ARGV[0];
$split_directory_out = $ARGV[1];


open (IDXTXT_IN,$searchidxtxt_in_filename);
$searchidxtxt_out_filename = "";
$wordID_pred = -1;
while (<IDXTXT_IN>)
{
	($wordID) = $_ =~ /^\d+\s+(\d+)/;
	if ($wordID != $wordID_pred or not $searchidxtxt_out_filename)
	{
		if ($searchidxtxt_out_filename)
		{
			close(IDXTXT_OUT);
		}
		$searchidxtxt_out_filename = "$split_directory_out/wordID_$wordID.txt";
		print "$searchidxtxt_out_filename\n";
		open (IDXTXT_OUT,"> $searchidxtxt_out_filename");
		$wordID_pred = $wordID;
	}
	print IDXTXT_OUT $_;
}

