#!/bin/bash

if [ `pwd | grep ONE_TOUCH_MASTER` ]; then
	echo "This script should not be run within the folder ONE_TOUCH_MASTER"
	exit
fi

source ./CONFIG
source $INCLUDE_DIR/1_latlist.template.sh
source $INCLUDE_DIR/2_lexicon.template.sh
source $INCLUDE_DIR/3_document_index.template.sh
source $INCLUDE_DIR/4_lattice_index_sge_create_scripts.template.sh
source $INCLUDE_DIR/5_lattice_index.template.sh
source $INCLUDE_DIR/6_forward_index_to_inverted_index.template.sh
source $INCLUDE_DIR/7_search_sge_create_scripts.template.sh
source $INCLUDE_DIR/8_search.template.sh

function check_directories()
{
	for dir in $@; do
		if [ -d $dir ]; then
			echo "ERROR: Directory already exists... please remove it first. ($dir)" > /dev/stderr;
			exit;
		fi
	done
}

if [ "$DICTIONARY_LVCSR" != "" ]; then
	DICTIONARY_LOCAL_LVCSR=$ROOT_DIR/lexicon/lvcsr/`basename $DICTIONARY_LVCSR`
fi

if [ "$DICTIONARY_PHN" != "" ]; then
	DICTIONARY_LOCAL_PHN=$ROOT_DIR/lexicon/phn/`basename $DICTIONARY_PHN`
fi

if [ "$1" != "--only-create-scripts" ]; then
	directories="";
	directories="$directories $ROOT_DIR/lattices.htk";
	directories="$directories $ROOT_DIR/lattices.system";
	directories="$directories $ROOT_DIR/idx";
	directories="$directories $ROOT_DIR/scripts";
	directories="$directories $ROOT_DIR/tmp";
	directories="$directories $ROOT_DIR/lexicon";
	directories="$directories $ROOT_DIR/lists";
	directories="$directories $ROOT_DIR/sge";
	directories="$directories $ROOT_DIR/search";

	check_directories $directories;

	mkdir $ROOT_DIR/idx
	mkdir $ROOT_DIR/scripts
	mkdir $ROOT_DIR/tmp
	mkdir $ROOT_DIR/lexicon
	mkdir $ROOT_DIR/lists
	mkdir $ROOT_DIR/sge
	mkdir $ROOT_DIR/search

	if [ $LVCSR == "YES" ]; then
		if [ "$HTK_LATTICES_LVCSR" != "$ROOT_DIR/lattices.htk.lvcsr" ]; then
			ln -s $HTK_LATTICES_LVCSR $ROOT_DIR/lattices.htk.lvcsr
		fi
		mkdir $ROOT_DIR/lattices.system.lvcsr
		mkdir $ROOT_DIR/lexicon/lvcsr
		mkdir $ROOT_DIR/idx/lvcsr
		mkdir $ROOT_DIR/idx/lvcsr/fwd

		if [ $DICTIONARY_COPY == "YES" ]; then cp $DICTIONARY_LVCSR $DICTIONARY_LOCAL_LVCSR;
		else ln -s $DICTIONARY_LVCSR $DICTIONARY_LOCAL_LVCSR; fi
	fi
	if [ $PHN == "YES" ]; then
		if [ "$HTK_LATTICES_PHN" != "$ROOT_DIR/lattices.htk.phn" ]; then
			ln -s $HTK_LATTICES_PHN $ROOT_DIR/lattices.htk.phn
		fi
		mkdir $ROOT_DIR/lattices.system.phn
		mkdir $ROOT_DIR/lexicon/phn
		mkdir $ROOT_DIR/idx/phn
		mkdir $ROOT_DIR/idx/phn/fwd

		if [ $DICTIONARY_COPY == "YES" ]; then cp $DICTIONARY_PHN $DICTIONARY_LOCAL_PHN;
		else ln -s $DICTIONARY_PHN $DICTIONARY_LOCAL_PHN; fi
	fi
fi


##################################################
# LISTS 
# 
# OUTPUT: 1_latlists_lvcsr.sh, 1_latlists_phn.sh
##################################################
LATDIR_LVCSR=lattices.htk.lvcsr
LATDIR_PHN=lattices.htk.phn

template_latlist \
	$LVCSR \
	$PHN \
	$ROOT_DIR \
	$LATDIR_LVCSR \
	$LATDIR_PHN

##################################################
# LEXICON
#
# OUTPUT: 2_lexicon_lvcsr.sh, 2_lexicon_phn.sh
##################################################

template_lexicon \
	$LVCSR \
	$PHN \
	$ROOT_DIR \
	$SCRIPTS_DIR \
	$DICTIONARY_LOCAL_LVCSR \
	$DICTIONARY_LOCAL_PHN


##################################################
# DOCUMENT (LATTICE) INDEX
#
# OUTPUT: 3_document_index.sh
##################################################

template_document_index \
	$LVCSR \
	$PHN \
	$ROOT_DIR




##################################################
# LATTICE INDEXING - CREATE SGE SCRIPTS
#
# OUTPUT: 4_lattice_index_sge_create_scripts_lvcsr.sh 4_lattice_index_sge_create_scripts_phn.sh
##################################################

template_lattice_index_sge_create_scripts \
	$LVCSR \
	$PHN \
	$ROOT_DIR \
	$LATDIR_LVCSR \
	$LATDIR_PHN





##################################################
# LATTICE INDEXING
#
# OUTPUT: 5_lattice_index_lvcsr.sh 5_lattice_index_phn.sh
##################################################

if [ $LVCSR == "YES" ]; then
	template_lattice_index \
		lvcsr \
		$ROOT_DIR \
		$INDEXER \
		"$INDEXER_LVCSR_ARGS"
fi

if [ $PHN == "YES" ]; then
	template_lattice_index \
		phn \
		$ROOT_DIR \
		$INDEXER \
		"$INDEXER_PHN_ARGS"
fi


##################################################
# FORWARD INDEX => INVERTED INDEX
#
# OUTPUT: 6_forward_index_to_inverted_index_lvcsr.sh 6_forward_index_to_inverted_index_phn.sh
##################################################
for i in lvcsr phn; do
	
	if [ $i == "lvcsr" -a $LVCSR != "YES" ]; then
		continue;
	fi
	if [ $i == "phn" -a $PHN != "YES" ]; then
		continue;
	fi
	template_forward_index_to_inverted_index \
		$i \
		$ROOT_DIR \
		$SEARCHIDX2TXT \
		$SORT_BUFFER_SIZE \
		$SEARCHIDXTXT_ADDPOINTERS \
		$SEARCHIDXTXT2BIN \
		$INDEXER
done


##################################################
# SEARCH - CREATE SGE SCRIPTS
#
# OUTPUT: 7_search_sge_create_scripts.pl
##################################################

template_search_sge_create_scripts \
	$ROOT_DIR \
	$TERMLIST \
	$LANGUAGE

##################################################
# SEARCH
#
# OUTPUT: 8_search.sh
##################################################


if [ $LVCSR == "YES" ]; then
	SEARCHER_ARGS="$SEARCHER_ARGS $SEARCHER_LVCSR_ARGS"
fi
if [ $PHN == "YES" ]; then
	SEARCHER_ARGS="$SEARCHER_ARGS $SEARCHER_PHN_ARGS"
fi

template_search \
	$ROOT_DIR \
	"$SEARCHER_ARGS" \
	$SEARCH_SRV




chmod a+x $ROOT_DIR/scripts/*.sh $ROOT_DIR/scripts/*.pl
