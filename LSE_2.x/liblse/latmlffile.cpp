#include "latmlffile.h"

#define SIL_LIKELIHOOD	-100000

using namespace std;
using namespace lse;

/*
void LatMlfFile::WordClusters::sort() {
	
	for (AllClusters::iterator i=allClusters.begin(); i!=allClusters.end(); ++i) {
		std::sort(i->begin(), i->end());
	}
}


void LatMlfFile::WordClusters::add(LatMlfFile::Node node) {

//	DBG_FORCE("add()");
	// let's find the set for given node
	for (AllClusters::iterator iCluster=allClusters.begin(); iCluster!=allClusters.end(); ++iCluster) {
		// compare with all nodes in set
		if (iCluster->W_id == node.W_id) {
			for (vector<LatMlfFile::Node>::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
				//DBG_FORCE("if ("<<node.t<<" >= "<<iWord->tStart<<" && "<<node.t<<" <= "<<iWord->t<<")");
				if (node.t >= iWord->tStart && node.t <= iWord->t) {				//node's end time is in iWord
					iCluster->push_back(node);
					return;
				} 
				//DBG_FORCE("if ("<<node.tStart<<" >= "<<iWord->tStart<<" && "<<node.tStart<<" <= "<<iWord->t<<")");
				if (node.tStart >= iWord->tStart && node.tStart <= iWord->t) { 		//node's start time is in iWord
					iCluster->push_back(node);
					return;
				}

				if (node.tStart <= iWord->tStart && node.t >= iWord->t) {			//iWord is in node
					iCluster->push_back(node);
					return;
				}
			}
		}
	}
	// if no matching set found, create a new one
//	DBG_FORCE("Creating a new Cluster...");
	WordClusters::Cluster v;
	v.push_back(node);
	v.W_id = node.W_id;
	allClusters.push_back(v);
//	DBG_FORCE("node: tStart:"<< node.tStart << "\t" << (Lattice::Node)node);
//	DBG_FORCE("done");
//	cin.get();
}



void LatMlfFile::WordClusters::print() {
	DBG("allClusters.size(): "<< allClusters.size());
	for (AllClusters::iterator iCluster=allClusters.begin(); iCluster!=allClusters.end(); ++iCluster) {
		cout << "==================================================" << endl << flush;
		DBG("  wClusters.size(): "<<iCluster->size());
		for (WordClusters::Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
			cout << "overlapped_likelihood:" << iWord->overlapped_likelihood << " likelihood:" << iWord->likelihood << " tStart:" << iWord->tStart << "\t" << *iWord << " " << endl << flush;
		}
	}
}
*/

/**
  @brief Load list of keywords from given file

  @param filename

  @return 
*/
int LatMlfFile::loadKeywordList(string filename) {
	ifstream in(filename.c_str());
	if (!in.good()) {
		cerr << "Error: opening file " << filename << endl;
		return (2);
	}
	string line;
	
	while(getline(in, line)) {
		kwdList.push_back(lat->lexicon->getValID(line, false));
//		DBG_FORCE("kwdList.push_back: " << line << " -> " << lat->lexicon->getValID(line, false));// << " -> " << lat->lexicon->getValID(line));
	}

	
	in.close();
	return 0;
}
	


/**
  @brief Add the word with given ID to MLF?

  @param wordID

  @return True - if no keyword list file is set or word with given ID appears
  		  in keyword file
*/
bool LatMlfFile::validWord(ID_t wordID) {
//	DBG("validWord()");
	if (kwdList.size() == 0) 
		return true;
	for (vector<ID_t>::iterator i=kwdList.begin(); i!=kwdList.end(); ++i) {
		if (*i == wordID) {
			return true;
		}
	}
	return false;
}



/**
  @brief Get min time of all nodes having links to given nodeID

  @param nodeID

  @return Node's start time
*/
LatTime LatMlfFile::getMinStartTime(ID_t nodeID) {

	bool minTime_isset = false;
	LatTime minTime = INFINITY; // set min time to last node's time

//	DBG("getMinStartTime()");
	for (vector<ID_t>::iterator i=lat->bckLinks[nodeID].begin(); i!=lat->bckLinks[nodeID].end(); ++i) {

		LatTime curTime = lat->nodes[lat->links[*i]->S]->t;
		if (minTime_isset) {
			if (minTime > curTime )
				minTime = curTime;
		} else {
			minTime = curTime;
			minTime_isset = true;
		}
	}
//	DBG("getMinStartTime() end");
	return (minTime_isset) ? (minTime) : (0);
	
}



unsigned long ToHTKTime(double t) {
	return ((int)(floor(t*100))) * 100000;
}

/**
  @brief Save lattice to Master Label File. If list of keywords is set, then
  		 add only these keywords to MLF

  @param filename MLF name
  @param latName Lattice's name

  @return 
*/
int LatMlfFile::save(const string filename, const string latName) {

/*	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("test:");
	DBG_FORCE("valid(LITTLE):"<<validWord("LITTLE"));
	DBG_FORCE("valid(THOSE):"<<validWord("THOSE"));
	DBG_FORCE("valid(FIRST):"<<validWord("FIRST"));
	DBG_FORCE("--------------------------------------------------");
*/	
	
	ifstream test(filename.c_str());
	if (test.fail()) {
		ofstream mkfile(filename.c_str());
		mkfile << "#!MLF!#" << endl;
		mkfile.close();
	}
/*
	for (unsigned int i=0; i<vit->NN; i++) {
		DBG("bwds["<<i<<"]: "<<vit->bwds[i]);
	}
*/
//	lat->updateFwdBckLinks(); // not needed - executed in lat->loadFromHTKFile()
	
//	DBG_FORCE("lat->nodes.size():" << lat->nodes.size());
	ofstream out(filename.c_str(), ofstream::app);
	out <<"\""<<latName<<".lab\""<<endl;

	WordClusters::Node tmpNode;
//	wClusters.print();
	for (Lattice::Nodes::iterator i=lat->nodes.begin(); i!=lat->nodes.end(); ++i) {
		if ((*i)->W_id == 0) 
			(*i)->W_id = lat->lexicon->getValID((*i)->W);
				
		if (validWord((*i)->W_id)) {
			tmpNode = **i;
			wClusters.add(tmpNode);
		}
	}

	// compute overlapped likelihood for each word in each cluster
	if (compute_overlapped_words_likelihood) {
		for (WordClusters::AllClusters::iterator iCluster=wClusters.allClusters.begin(); iCluster!=wClusters.allClusters.end(); ++iCluster) {
			for (WordClusters::Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
				iWord->overlapped_likelihood = iWord->likelihood;		// word's own likelihood
				for (WordClusters::Cluster::iterator iWordOverlapped=iCluster->begin(); iWordOverlapped!=iCluster->end(); ++iWordOverlapped) {
					LatTime start = iWordOverlapped->tStart;
					LatTime end = iWordOverlapped->t;
					if (start < iWord->tStart && end > iWord->t)
						iWord->overlapped_likelihood = logAdd(iWord->overlapped_likelihood, iWordOverlapped->likelihood);
				}
//				iWord->overlapped_likelihood -= 
			}
		}
	}
//	DBG_FORCE("sorting...");
	wClusters.sort();
//	DBG_FORCE("printing...");
//	wClusters.print();
//	DBG_FORCE("done");
//	cin.get();


	

	// remove unnecessary words
/*	
	if (this->likelihood_treshold >= 0) {
		for (WordClusters::AllClusters::iterator iCluster=wClusters.allClusters.begin(); iCluster!=wClusters.allClusters.end(); ++iCluster) {
			int allCount = iCluster->size();
			int okCount = allCount;
			
			double bestLikelihood;
			bool first = true;
			for (WordClusters::Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
				
				// break cycle if treshold reached
				if (first) {
					first = false;
					bestLikelihood = iWord->likelihood;
				} else if (iWord->likelihood * (1 + this->likelihood_treshold) < bestLikelihood) {
					DBG("iCluster->erase: " << *iWord);
					iCluster->erase(iWord); 
					okCount--;
				}
				DBG_FORCE("first="<<first<<" ... "<< iWord->likelihood << " * " << (1 + this->likelihood_treshold) << " = " << iWord->likelihood * (1 + this->likelihood_treshold) <<  " < " << bestLikelihood);
			}
			cout << "added words ("<<okCount<<"/"<<allCount<<")" << endl;
		}
	}
*/	
/*
    // get min and max time of each word set
	bool found = false;
	LatTime lastInsertedTime = 0;
		
	for (WordClusters::AllClusters::iterator iCluster=wClusters.allClusters.begin(); iCluster!=wClusters.allClusters.end(); ++iCluster) {
		int wordCounter = 0;
		LatTime tStart = (iCluster->begin())->tStart;
		LatTime tEnd;
		double max_likelihood = (iCluster->begin())->likelihood; // likelihood of first word in list
		
		for (WordClusters::Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
			if (wordCounter++ > 1)
				break;

			if (iWord->tStart < tStart)
				tStart = iWord->tStart;
			if (iWord->t > tEnd)
				tEnd = iWord->t;
			if (max_likelihood < iWord->likelihood)
				max_likelihood = iWord->likelihood;
		}
		
		if (found == false) {
			out << 0 << "\t" << ToHTKTime(tStart)<< "\t<sil>\t"<< SIL_LIKELIHOOD << endl;
			found = true;
		}
		out << ToHTKTime(tStart) << "\t" << ToHTKTime(tEnd) << "\t" << lat->lexicon->getVal(iCluster->W_id) << "\t" << max_likelihood << endl; //lat->lexicon->getVal((i->second).W_id) << endl;
		lastInsertedTime = tEnd;
	}
*/

    // only first word in set
	bool found = false;
	LatTime lastInsertedTime = 0;

	for (WordClusters::AllClusters::iterator iCluster=wClusters.allClusters.begin(); iCluster!=wClusters.allClusters.end(); ++iCluster) {
		// only the first word in each cluster
		WordClusters::Cluster::iterator iWord = iCluster->begin();
		if (iWord != iCluster->end()) {
			if (found == false) {
				out << 0 << "\t" << ToHTKTime(iWord->tStart)<< "\t<sil>\t"<< SIL_LIKELIHOOD<< endl;
				found = true;
			}
			out << ToHTKTime(iWord->tStart) << "\t" << ToHTKTime(iWord->t) << "\t" << lat->lexicon->getVal(iCluster->W_id) << "\t";
			if (compute_overlapped_words_likelihood)
				out << iWord->overlapped_likelihood;
			else
				out << iWord->bestLikelihood;
			out << endl; //lat->lexicon->getVal((i->second).W_id) << endl;
			lastInsertedTime = iWord->t;
		}
	}
	
	Lattice::Nodes::reverse_iterator iLast = lat->nodes.rbegin();
	out << ToHTKTime(lastInsertedTime) << "\t" << ToHTKTime((*iLast)->t) << "\t<sil>\t"<< SIL_LIKELIHOOD<< endl;
	out << "." << endl;
	
	out.close();
	DBG("MLF OUTPUT COMPLETED");
	return 0;
}
