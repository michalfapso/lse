#include <unistd.h>
#include <sys/stat.h>
#include "common.h"
#include "lattypes.h"
#include <iostream>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstring>
#include <cstdio>
#include <cctype>
#include <errno.h>
#include <sstream>

using namespace std;

bool file_exists(const char *filename, Rights rights) {
	int mode = F_OK; // default == just check whether the file exists
	switch (rights) {
		case r: mode = R_OK; break;
		case w: mode = W_OK; break;
		case x: mode = X_OK; break;
		case none: mode = F_OK; break;
	}
	return access(filename, mode) == 0; // return true if rights are OK / file exists
}


long file_size(const char *filename) {
	struct stat results;
    if (stat(filename, &results) == 0) {
		return results.st_size;
	} else {
		return -1;
	}
}



char* itoa(int input, char *str, int radix /*not implemented yet - using 10*/){
//    static char buffer[16];
    snprintf(str,sizeof(str),"%d",input);
	cout << "itoa("<<input<<", "<<str<<")" << endl;
    return str;
}

string upcase(string s) {
	// uppercase all characters
	for(string::iterator ch = s.begin(); ch != s.end(); ++ch) {
		*ch = toupper(*ch);
	}
	return s;
}

string lowcase(string s) {
	// lowercase all characters
	for(string::iterator ch = s.begin(); ch != s.end(); ++ch) {
		*ch = tolower(*ch);
	}
	return s;
}

string nonAsciiToOctal(string s) {
	string res;
	char buf[5];
	for(string::iterator ch = s.begin(); ch != s.end(); ++ch) {
		if ((unsigned int)*ch > 127) {
			sprintf(buf,"\\%o",(int)*ch);
			res += buf;
		} else {
			res += *ch;
		}
	}
	return res;
}

// used in sockets server implementation
size_t strlcpy(char *dst, const char *src, size_t siz)
{
	char *d = dst;
	const char *s = src;
	size_t n = siz;

	if (n != 0 && --n != 0) {
		do {
			if ((*d++ = *s++) == 0)
				break;
		} while (--n != 0);
	}

	if (n == 0) {
		if (siz != 0)
			*d = '\0';
		while (*s++)
			;
	}

	return(s - src - 1);
}

// remove the newline character(s) from the end of the given string str
char* chomp(char *str, int len) 
{
	if (len == -1) 	{
		len = strlen(str);
	}
	while (str[len-1] == 0x0D || str[len-1] == 0x0A)
	{
		len--;
		str[len] = 0x00; // replace newline with string-terminator
	}
	return str;
}


void string_split(const std::string& str, const std::string& delimiters, std::vector<std::string>& tokens )
{
     // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
	// if there is no delimiter in the given string, add the given string as the only token
	if (tokens.size() == 0)
	{
		tokens.push_back(str);
	}
}

bool is_overlapping(double t1Start, double t1End, double t2Start, double t2End)
{
	return !(t1End <= t2Start || t1Start >= t2End);
/*
	return (t1Start < t2Start && t2Start < t1End) || 
		   (t1Start < t2End   && t2End   < t1End) ||
		   (t1Start < t2Start && t2End   < t1End);
*/
}

std::string CheckBackslash( std::string InputStr )
{
  unsigned int i=0;
  while(i<InputStr.npos){
    i=InputStr.find("\\",i);
    if(i<InputStr.npos){
      InputStr.insert(i,"\\");
      i=i+2;
    }
  }//while
  return InputStr;
}

std::string DeleteBackslash( std::string InputStr )
{
  unsigned int i=0;
  while(i<InputStr.npos){
    i=InputStr.find("\\",i);
    if(i<InputStr.npos){
      InputStr.erase(i,1);
    }
  }//while
  return InputStr;
}

void phn2ngram(const string src, vector<string>* dest, int n)
{
	typedef std::list<string> HistContainer;
	
	HistContainer	hist;
	
	// parse phonemes
	vector<string> phonemes_str;
	string_split(src, " ", phonemes_str);
//	for (QueryKwdList::const_iterator iKwd = src->begin(); iKwd != src->end(); ++iKwd)
	for (vector<string>::iterator i=phonemes_str.begin(); i!=phonemes_str.end(); ++i)
	{
		hist.push_back(*i);

		if ((int)hist.size() == n)
		{
			string ngram = "";
			for (HistContainer::iterator iHistRec=hist.begin(); iHistRec!=hist.end(); ++iHistRec)
			{
				ngram += (ngram=="" ? "" : "_") + *iHistRec;
			}
			dest->push_back(ngram);
			hist.pop_front();
		}
	}
}

string trim_path(const string &fullpath, const string &dataDirPrefix, const string &ext)
{
	DBG("trim_path(\""<<fullpath<<"\")");
	int begin = 0;
	int end = fullpath.length();

	if (dataDirPrefix != "")
	{
		if (fullpath.find( dataDirPrefix, 0 ) == 0)
		{
			begin = dataDirPrefix.length();
		}
	}

	while (fullpath[begin] == PATH_SLASH)
	{
		begin++;
	}

	if (ext != "")
	{
		if (fullpath.find( dataDirPrefix, 0 ) == fullpath.length() - ext.length())
		{
			end = fullpath.length() - ext.length();
		}
	}

	DBG("trim_path(\""<<fullpath<<"\")...done..."<<fullpath.substr(begin, end - begin));
	return fullpath.substr(begin, end - begin);
}

string trim_file_extension(const string &fullpath)
{
	//DBG("trim_file_extension(\""<<fullpath<<"\")");
	//DBG("PATH_SLASH:"<<PATH_SLASH);
	unsigned int slash_pos = fullpath.rfind( PATH_SLASH );
	if (slash_pos == string::npos)
	{
		slash_pos = 0;
	}
	//DBG("slash_pos:"<<slash_pos);

	unsigned int dot_pos = fullpath.find( ".", slash_pos );
	if ( dot_pos == string::npos )
	{
		dot_pos = fullpath.length();
	}
	//DBG("dot_pos:"<<dot_pos);

	//DBG("trim_file_extension(\""<<fullpath<<"\")...done..."<<fullpath.substr(0, dot_pos));
	return fullpath.substr(0, dot_pos);
}


std::string trim_prefix(const std::string &instr, const std::string &Prefix)
{
  DBG("trim_prefix(\""<<instr<<"\")");
  int begin = 0;
  int end = instr.length();

  begin = instr.find( Prefix, 0 );
  if( begin == (int)string::npos ) {
    begin=0;
  }else{
    begin++;
  }  
    
  //std::cout << instr << " " << instr.substr(begin, end - begin) << std::endl;

  return instr.substr(begin, end - begin);
}


std::string trim_outer_spaces(std::string str)
{
	while(str[0] == ' ' || str[0] == '\t') { str.erase(0,1); }
	while(str[str.length()-1] == ' ' || str[str.length()-1] == '\t' || str[str.length()-1] == 0x0a || str[str.length()-1] == 0x0d) { str.erase(str.length()-1, 1); }
	return str;
}

std::string string2hex(const std::string &str)
{
	ostringstream res;
	for(int i=0; i<(int)str.length(); i++)
	{
		res << "0x" << hex << (int)str[i] << "("<<str[i]<<") ";
	}
	return res.str();
}

