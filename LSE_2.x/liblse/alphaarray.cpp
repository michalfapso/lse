#include "latbinfile.h"

using namespace std;
using namespace lse;

void LatBinFile::AlphaLastArray::free()
{
	if (this->alphaArray != NULL) {
		delete[] this->alphaArray;
	}
}

int LatBinFile::AlphaLastArray::load(const std::string &filename)
{
	DBG("AlphaLastArray::load("<<filename<<")");
	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
		return (LAT_ERROR_IO);
	}

	if ((arraySize = file_size(filename.c_str())) == -1) {
		return (LAT_ERROR_IO);
	};
	DBG("  filesize:"<<arraySize);
	DBG("  filesize / sizeof(LatBinFile::AlphaLastRecord) = " << arraySize << " / " << sizeof(LatBinFile::AlphaLastRecord));
	DBG("  sizeof(nodeID):"<<sizeof(ID_t)<<" sizeof(alphaLast)"<<sizeof(TLatViterbiLikelihood));
	arraySize /= sizeof(ID_t) + sizeof(TLatViterbiLikelihood);
	DBG("  arraySize:"<<arraySize);

	if (alphaArray != NULL) {
		delete[] alphaArray;
	}
	
	alphaArray = new LatBinFile::AlphaLastRecord[this->arraySize];

	// read nodes from file to array ending with end node
	LatBinFile::AlphaLastRecord alRec;
	for (int i=0; i<this->arraySize; i++) {
		alRec.readFrom(in);
		alphaArray[i] = alRec;
		DBG("  read... nodeID:"<<alRec.nodeID<<" alphaLast:"<<alRec.alphaLast);
	}
	lastFoundIndex = 0;
	in.close();
	DBG("AlphaLastArray::load()...done");
	return 0;
}

void LatBinFile::AlphaLastArray::print()
{
	DBG("LatBinFile::AlphaLastArray::print()");
	for (int i=0; i<this->arraySize; i++)
	{
		cout << "nodeID:"<<alphaArray[i].nodeID << " alphaLast:"<<alphaArray[i].alphaLast << endl;
	}
	DBG("LatBinFile::AlphaLastArray::print()...done");
}

TLatViterbiLikelihood LatBinFile::AlphaLastArray::getAlphaLast(ID_t nodeID)
{
	// binary search (records are sorted by nodeID)
	DBG("AlphaLastArray::getAlphaLast("<<nodeID<<")");
	
	// can we use the value returned last time?
	bool get_new_alpha_last = false;
	if (lastFoundIndex == 0)
	{
		get_new_alpha_last = true;
	}
	else if (lastFoundIndex > 0)
	{	
//		DBG("alphaArray[lastFoundIndex].nodeID ... alphaArray["<<lastFoundIndex<<"].nodeID ... "<<alphaArray[lastFoundIndex].nodeID);
//		DBG("alphaArray[lastFoundIndex-1].nodeID ... alphaArray["<<lastFoundIndex-1<<"].nodeID ... "<<alphaArray[lastFoundIndex-1].nodeID);
		if (alphaArray[lastFoundIndex].nodeID < nodeID || alphaArray[lastFoundIndex-1].nodeID >= nodeID)
		{
			get_new_alpha_last = true;
		}
	}

	if (get_new_alpha_last)
	{
		// if not, we have to search for the new one
		for (int i=0; i<this->arraySize; i++) 
		{			
			if (alphaArray[i].nodeID >= nodeID) 
			{
				lastFoundIndex = i;
				break;				
			}
		}
	}

	DBG("  lastFoundIndex:"<<lastFoundIndex);
	if (lastFoundIndex > 0)	if (! (alphaArray[lastFoundIndex].nodeID >= nodeID && alphaArray[lastFoundIndex-1].nodeID < nodeID)) {
		cerr << "AlphaLastArray::getAlphaLast() ... ERROR: wrong value found" << endl;
		exit(1);
	}
	
	return alphaArray[lastFoundIndex].alphaLast;
	
/*	
	int left = 0;
	int right = this->arraySize - 1;
	int lastNodeID = 0;

	enum LastNodeCmp {
		not_set,
		greater,
		less,
	} lastNodeCmp = not_set;
	int middle_pred = -1;
	int middle = -1;
//	while (left!=middle && right!=middle) {
	while(1) {
		middle = (left + right) / 2;
		if (middle == middle_pred) break;
		DBG("  left:"<<left<<" right:"<<right<<" middle:"<<middle<<" val:"<<this->alphaArray[middle].nodeID<<" lastNodeID:"<<lastNodeID<<" lastNodeCmp:"<<lastNodeCmp);

		if (nodeID > this->alphaArray[middle].nodeID) {
			left = middle;
			lastNodeCmp = greater;
		} else if (nodeID < this->alphaArray[middle].nodeID) {
			right = middle;
			lastNodeCmp = less;
		} else {
			DBG("AlphaLastArray::getAlphaLast()...done value successfuly found:"<<alphaArray[middle].alphaLast);
			break;
		}

		lastNodeID = this->alphaArray[middle].nodeID;
		middle_pred = middle;
	}
	DBG("  left:"<<left<<" right:"<<right<<" middle:"<<middle<<" val:"<<this->alphaArray[middle].nodeID<<" lastNodeID:"<<lastNodeID<<" lastNodeCmp:"<<lastNodeCmp);
	if (nodeID < alphaArray[0].nodeID) {
		DBG("AlphaLastArray::getAlphaLast()...done value successfuly found:"<<alphaArray[0].alphaLast);
		return alphaArray[0].alphaLast;
	}

	if (middle > 0) if (! (alphaArray[middle].nodeID >= nodeID && alphaArray[middle-1].nodeID < nodeID)) {
		cerr << "AlphaLastArray::getAlphaLast() ... ERROR: wrong value found" << endl;
		exit(1);
	}
	if (middle < 0) {
		cerr << "AlphaLastArray::getAlphaLast() ... ERROR: value not found" << endl;
		exit(1);
	}
	return alphaArray[middle].alphaLast;
*/


/*
	while (left <= right) {
		int middle = (left + right) / 2;
		DBG("  left:"<<left<<" right:"<<right<<" middle:"<<middle<<" val:"<<this->alphaArray[middle].nodeID<<" lastNodeID:"<<lastNodeID<<" lastNodeCmp:"<<lastNodeCmp);

		if (nodeID > this->alphaArray[middle].nodeID) {
			if (lastNodeCmp == less) {
				DBG("AlphaLastArray::getAlphaLast()...done value successfuly found:"<<alphaArray[middle+1].alphaLast);
				return alphaArray[middle+1].alphaLast;
			}
			left = middle + 1;
			lastNodeCmp = greater;
		} else if (nodeID < this->alphaArray[middle].nodeID) {
			if (lastNodeCmp == greater) {
				DBG("AlphaLastArray::getAlphaLast()...done value successfuly found:"<<alphaArray[middle].alphaLast);
				return alphaArray[middle].alphaLast;
			}
			right = middle - 1;
			lastNodeCmp = less;
		} else {
			DBG("AlphaLastArray::getAlphaLast()...done value successfuly found:"<<alphaArray[middle].alphaLast);
			return alphaArray[middle].alphaLast;
		}

		lastNodeID = this->alphaArray[middle].nodeID;
		middle_pred = middle;
	}
*/
}

