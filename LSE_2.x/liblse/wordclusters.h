#ifndef WORDCLUSTERS_H
#define WORDCLUSTERS_H

#include <vector>
#include "lattypes.h"
#include "lat.h"
#include "latviterbifwbw.h"
#include <math.h>

namespace lse {


	
	
//	typedef std::vector< LatMlfFile::Node > Cluster;
	
	
	
class WordClusters {

	public:

		class Node : public Lattice::Node {
		public:
			double likelihood;				// word's own likelihood
			double overlapped_likelihood;	// word's own likelihood plus aliquote likelihood of overlapping words
			
			Node& operator=(const Lattice::Node& r) {
				// Handle self-assignment:
	//			if(this == &right) return *this;
				id = r.id;
				tStart = r.tStart;
				t = r.t;
				W_id = r.W_id;
				W = r.W;
				v = r.v;
				p = r.p;
				bestLikelihood = r.bestLikelihood;
				bestLinkID = r.bestLinkID;
				return *this;
			}
			
			friend bool operator<(const WordClusters::Node& l, const WordClusters::Node& r) {
				if (l.overlapped_likelihood == r.overlapped_likelihood) {
					return l.bestLikelihood > r.bestLikelihood;
				} else
					return l.overlapped_likelihood > r.overlapped_likelihood; // move higher values to the front of the container
			}
		};

		class Cluster : public std::vector< WordClusters::Node > {
			public:	ID_t W_id; // word ID
		};
		
		typedef std::vector< Cluster > AllClusters;
		AllClusters allClusters;
		
		void add(WordClusters::Node node);
		void print();
		void sort();
};

	
} // namespace lse



#endif
