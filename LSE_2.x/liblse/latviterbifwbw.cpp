// Viterbi - precomputation of FW BW probas
#include "latviterbifwbw.h"
#include <cmath>

using namespace std;
using namespace lse;
/*
double lse::logAdd(double a, double b, float treshold) {
//	return (a>b) ? a : b;
	if (a < b) {
		double tmp = a;
		a = b;
		b = tmp;
	}
	double diff = b - a;
	if (-diff > treshold) {
		return a;
	}
	return log(1.0 + exp(diff)) + a;
//	return log(exp(a) + exp(b));
}
*/

//--------------------------------------------------------------------------------
// constructor - has to alloc mem for all...
LatViterbiFwBw::LatViterbiFwBw (Lattice *inlat, float wip, float p_amscale, float p_lmscale, float p_posteriorscale, float p_amscale_kwd, float p_lmscale_kwd, bool p_baum_welsh, float p_logAdd_treshold) {
	unsigned int ii;

	this->amscale = p_amscale;
	this->lmscale = p_lmscale;
	this->posteriorscale = p_posteriorscale;
	this->amscale_kwd = p_amscale_kwd;
	this->lmscale_kwd = p_lmscale_kwd;
	this->baum_welsh = p_baum_welsh;
	this->logAdd_treshold = p_logAdd_treshold;

	// set the lattice to my own
	this -> lat = inlat;
	// get the no of nodes.
	NN = lat->nodes.size();
	// and allocate for all possible vars.
	fwds = new double[NN];
	bwds = new double[NN];
	fwlens = new int[NN];
	bwlens = new int[NN];
	// set all probas to -INF
	for (ii=0; ii<NN; ii++) {
		fwds[ii] = bwds[ii] = -INF;
		fwlens[ii] = bwlens[ii] = -1;
	}

	// set the WI proba.
	wipen = wip;
//	cout << "allocated for " << NN << " nodes"<< endl;
}


//--------------------------------------------------------------------------------
// destructor
LatViterbiFwBw::~LatViterbiFwBw () {
	delete [] fwds;
	delete [] bwds;
	delete [] fwlens;
	delete [] bwlens;
//	cout << "freed..." << endl;

}


//--------------------------------------------------------------------------------
// doing the job - FW
void LatViterbiFwBw::computeFwViterbi() {
//	cout << "at the moment nothing... " << endl;

	ID_t nodeid, S, E;     // starting and ending node id.
	unsigned int ii,jj;
	vector <ID_t> flinks;  // forward links
	double cfwd,nfwd;      // current and new forward Viterbi proba
	int    cfwlen,nfwlen;  // current and new length of path.
	float  a,l;	       // acoustic and LM probas.


// hoping that the nodes are sorted in time - otherwise we're fucked up :-)
	// get the 1st node and the last ...
	nodeid = lat->lastNode()->id;
//	cout <<"FORWARD VITERBI ... ";
	DBGFWBW("got last node id=" << nodeid << " might be a nonsense"<<endl);
	nodeid = lat->firstNode()->id;
	DBGFWBW("got 1st node id=" << nodeid << endl);
	// set the proba to zero, length to zero
	fwds[nodeid] = 0.0;
	fwlens[nodeid] = 0;
	for (ii=0; ii<NN-1; ii++) {

		// get the current fwd proba and path  length
		cfwd = fwds[ii];
		cfwlen = fwlens[ii];
		//check the node...
		DBGFWBW("=== ii=" << ii << " node id=" << lat->nodes[ii]->id);
		DBGFWBW(" t=" << lat->nodes[ii]->t << " W="<<lat->nodes[ii]->W <<endl);
		// get the fwd links
		flinks = lat->fwdLinks[ii];
		// go through them, each time:
		for (jj=0; jj < flinks.size(); jj++) {
			// - get where we are going from and to + probas
			// cout << lat->links[jj]<<endl;
			S=lat->links[flinks[jj]]->S;
			E=lat->links[flinks[jj]]->E;
			a=lat->links[flinks[jj]]->a;
			l=lat->links[flinks[jj]]->l;
			DBGFWBW("S E a l: " << S << " " <<E<<" "<<a<<" "<<l<<endl);
			
//			DBG_FORCE("nodesQ.push(): "<<E);
//			nodesQ.push(E);
			
			// new proba and new path length
			// add acoustic, language model and penalty
			nfwd = cfwd + (a*amscale + l*lmscale)*posteriorscale;
			if (lat->nodes[E]->W != "!NULL")
				nfwd += wipen*posteriorscale;
//			if (lat->lexicon->CanAddWiPen(lat->nodes[E].W_id))
			nfwlen = cfwlen + 1;
			// OK, if better than proba at the target, push
			// it there...
			if (baum_welsh) {
				fwds[E] = logAdd(fwds[E], nfwd, logAdd_treshold);
			} else if (nfwd > fwds[E]) {
				DBGFWBW("updating node "<< E<< " from fwd "<<fwds[E]<<" len "<<fwlens[E]);
				DBGFWBW(" to fwd " << nfwd << " len " << nfwlen<<endl);
				fwds[E] = nfwd;
				fwlens[E] = nfwlen;
			}
		}
	}
	// at this point, should get the final Viterbi proba at node NN
//	cout<<"LAST NODE: Viterbi at "<<NN-1;
	ii = NN-1;
	DBGFWBW("*** =" << ii << " node id=" << lat->nodes[ii]->id);
	DBGFWBW(" t=" << lat->nodes[ii]->t << " W="<<lat->nodes[ii]->W <<endl);

	// write fwds into nodes
	for (Lattice::Nodes::iterator n=lat->nodes.begin(); n!=lat->nodes.end(); ++n) {
		(*n)->alpha = fwds[(*n)->id];
	}
	
	for (unsigned int i=0; i<NN; i++) DBGFWBW("fwds["<<i<<"]: "<< setprecision(13) << fwds[i]);
	
//	cout << " fwd "<<fwds[NN-1]<<" len "<<fwlens[NN-1]<<endl;

}

//--------------------------------------------------------------------------------
// doing the job - BW
void LatViterbiFwBw::computeBwViterbi() {
//	cout << "at the moment nothing... " << endl;

	ID_t nodeid, S, E;     // starting and ending node id.
	unsigned int ii,jj;
	vector <ID_t> blinks;  // forward links
	double cbwd,nbwd;      // current and new backward Viterbi proba
	int    cbwlen,nbwlen;  // current and new length of path.
	float  a,l;	       // acoustic and LM probas.
	
	
// hoping that the nodes are sorted in time - otherwise we're fucked up :-)
	// get the 1st node and the last ...
	nodeid = lat->lastNode()->id;
//	cout << "BACKWARD VITERBI ... ";
	DBGFWBW("got last node id=" << nodeid << " might be a nonsense"<<endl);
	nodeid = lat->firstNode()->id;
	DBGFWBW("got 1st node id=" << nodeid << endl);
	// set the proba to zero, length to zero
	bwds[NN-1] = 0.0;
	bwlens[NN-1] = 0;
	for (ii=NN-1; ii>0; ii--) {  // 0 will be handled separately ...

		// get the current fwd proba and path  length
		cbwd = bwds[ii];
		cbwlen = bwlens[ii];
		//check the node...
		DBGFWBW("=== ii=" << ii << " node id=" << lat->nodes[ii]->id);
		DBGFWBW(" t=" << lat->nodes[ii]->t << " W="<<lat->nodes[ii]->W <<endl);
		// get the bwd links
		blinks = lat->bckLinks[ii];
		// go through them, each time:
//		DBG("--------------------------------------------------------------------------------");
//		DBG("node ID: " << ii);
		for (jj=0; jj < blinks.size(); jj++) {
			// - get where we are going from and to + probas
			// cout << lat->links[jj]<<endl;
			S=lat->links[blinks[jj]]->S;
			E=lat->links[blinks[jj]]->E;
			a=lat->links[blinks[jj]]->a;
			l=lat->links[blinks[jj]]->l;
			DBGFWBW("S E a l: " << S << " " <<E<<" "<<a<<" "<<l<<endl);
			// new proba and new path length
			// add acoustic, language model and penalty
			nbwd = cbwd + (a*amscale + l*lmscale)*posteriorscale;
			if (lat->nodes[E]->W != "!NULL")
				nbwd += wipen*posteriorscale;
			nbwlen = cbwlen + 1;
//			DBG("nbwd: "<<nbwd<<" = "<<cbwd<<" + "<<a<<" + "<<l<<" + "<<wipen);
			// OK, if better than proba at the target, push
			// it there...

			if (baum_welsh) {
				bwds[S] = logAdd(bwds[S], nbwd, logAdd_treshold);
			} else if (nbwd > bwds[S]) { // this must be START !
//				DBG("nbwd: "<<nbwd<<" = "<<cbwd<<" + "<<a<<" + "<<l<<" + "<<wipen);
//				DBG("*********** nbwd:" << nbwd << " bwds["<<S<<"]:" << bwds[S]);
				DBGFWBW("updating node "<< S<< " from bwd "<<bwds[S]<<" len "<<bwlens[S]);
				DBGFWBW(" to fwd " << nbwd << " len " << nbwlen<<endl);
				bwds[S] = nbwd;
				bwlens[S] = nbwlen;
			}
		}
	}
	// at this point, should get the final Viterbi proba at node NN
//	cout<<"LAST NODE: Viterbi at "<<0;
	ii = 0;
	DBGFWBW("*** =" << ii << " node id=" << lat->nodes[ii]->id);
	DBGFWBW(" t=" << lat->nodes[ii]->t << " W="<<lat->nodes[ii]->W <<endl);
//	cout << " bwd "<<bwds[ii]<<" len "<<bwlens[ii]<<endl;

//	for (unsigned int i=0; i<NN; i++) DBG_FORCE("bwds["<<i<<"]: "<<bwds[i]);
	
	// write fwds into nodes
	for (Lattice::Nodes::iterator n=lat->nodes.begin(); n!=lat->nodes.end(); ++n) {
		(*n)->beta = bwds[(*n)->id];
	}
}


//--------------------------------------------------------------------------------
// print it out to file;
void LatViterbiFwBw::writeViterbiFwBw(const string filename) {
	DBG_FORCE("writeViterbiFwBw("<<filename<<")");
//	cout << "when ready, this will print ..." << endl;
	ofstream out(filename.c_str());
	// first thing - out the WI penalty and no of nodes
	out << wipen<<"\t"<<NN<<endl;
	// just go node by node and print ... ID, fwd, fwdlen, bwd, bwdlen
	unsigned int ii;

	for (ii=0; ii<NN; ii++) {
		out <<setprecision(13) << ii<<"\t"<<fwds[ii]<<"\t"<<fwlens[ii]<<"\t"<<bwds[ii]<<"\t"<<bwlens[ii]<<endl;
	}
	out.close();
	DBG_FORCE("writeViterbiFwBw()...done");
}
//--------------------------------------------------------------------------------
// load it from file;
int LatViterbiFwBw::loadViterbiFwBw(const string filename) {
//	cout << "when ready, this will print ..." << endl;
	// this will be a classical reading - no iostreams ...
	FILE *fp;
	unsigned int myNN;
	float mywipen;
	unsigned int ii;
	// values to be read
	double fwd,bwd;
	unsigned int fwlen,bwlen,iir;

	fp = fopen(filename.c_str(),"r");
	if (fp == NULL) {
		cout << "can't open ViterbiFwBw file"<<filename<<endl;
		return(1);
	}
	// check no of nodes and wi proba - if don't match, return nonzero
	fscanf(fp,"%f",&mywipen);
	fscanf(fp,"%d",&myNN);
	if (mywipen != wipen) {
		cout << "read wi penalty "<<mywipen<<" not match with set "<<wipen;
		return(2);
	}
	if (myNN != NN) {
		cout << "read no of nodes "<<myNN<<" not match with "<<NN;
		return(3);
	}
	// here, everything should be OK, and all should be allocated.
	for (ii=0; ii<NN; ii++) {
		if (fscanf(fp,"%d",&iir) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		if (iir != ii) {
			cout <<"strange index of node"; return(5);
		}
		if (fscanf(fp,"%lf",&fwd) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		fwds[ii] = fwd;
		if (fscanf(fp,"%d",&fwlen) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		fwlens[ii] = fwlen;
		if (fscanf(fp,"%lf",&bwd) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		bwds[ii] = bwd;
		if (fscanf(fp,"%d",&bwlen) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		bwlens[ii] = bwlen;
	}
	fclose(fp);
	return(0);
}


TLatViterbiLikelihood LatViterbiFwBw::getLinkConfidence(lse::ID_t linkID, bool scaleOnKeyword) 
{
//	ID_t S = lat->links[linkID].S;
//	ID_t E = lat->links[linkID].E;

	Lattice::Link* p_link = lat->links[linkID];
	double whole_lattice_posterior = fwds[NN-1];

	double alpha = fwds[ p_link->S ];
	double beta = bwds[ p_link->E ];

	LatTime time_length = lat->nodes[p_link->E]->t - lat->nodes[p_link->S]->t;

	double kwd_scale =
		(
			p_link->a*amscale*amscale_kwd 
			+ p_link->l*lmscale*lmscale_kwd
		) / (time_length > 0 ? time_length : 1);

	double link_wipen = (lat->lexicon->CanAddWiPen(lat->nodes[p_link->E]->W_id) ? wipen : 0);

/*
    // IT IS NOT NEEDED TO EXPAND THE NULL NODE BECAUSE THE POSTERIOR CAN BE CORRECTLY COMPUTED FROM THE LINK GOING FOM NULL NODE
    //
	// if the start node of the given link is a !NULL node, expand all it's predecessors
	if (lat->nodes[p_link->S]->W == "!NULL")
	{
		vector<ID_t>* v_bck = &(lat->bckLinks[p_link->S]);
		double conf_best = -INF;
		// for each link comming to the !NULL node
		for (vector<ID_t>::iterator iBckLinkId=v_bck->begin(); iBckLinkId!=v_bck->end(); iBckLinkId++)
		{
			Lattice::Link* p_bck_link = lat->links[*iBckLinkId];

			time_length = lat->nodes[p_bck_link->E]->t - lat->nodes[p_bck_link->S]->t; // length of the incoming link

			double link_link_wipen = link_wipen + lat->lexicon->CanAddWiPen(lat->nodes[p_bck_link->E]->W_id) ? wipen : 0; // word insertion penalty for a !NULL link (should be zero)
			
			kwd_scale +=
				(
					p_bck_link->a * amscale * amscale_kwd // acoustic likelihood with scaling on keyword
					+ p_bck_link->l * lmscale * lmscale_kwd // language likelihood with scaling on keyword
				) / (time_length > 0 ? time_length : 1);

			alpha = fwds[ p_bck_link->S ];

			double conf = 
				alpha // alpha
				+ (scaleOnKeyword ? kwd_scale : 0)
				+ p_bck_link->a * amscale // acoustic likelihood
				+ p_bck_link->l * lmscale // language likelihood
				+ link_link_wipen // insertion penalty
				+ p_link->a * amscale // acoustic likelihood
				+ p_link->l * lmscale // language likelihood
				+ beta
				- whole_lattice_posterior; // normalize by the best path

			if (conf_best < conf)
			{
				conf_best = conf;
				if (lat->nodes[p_link->E]->bestLikelihood < conf_best && scaleOnKeyword)
				{
					lat->nodes[p_link->E]->bestLikelihood = conf_best;
					lat->nodes[p_link->E]->bestLinkID = p_link->id; // id of node's ingoing link with best confidence
					lat->nodes[p_link->E]->tStart = lat->nodes[p_bck_link->S]->t; // node's start time = end time of link's start node
				}
			}
			if (lat->nodes[p_link->E]->tStartBoundary > lat->nodes[p_bck_link->S]->t)
			{
				lat->nodes[p_link->E]->tStartBoundary = lat->nodes[p_bck_link->S]->t;
			}
		}
		return conf_best;
	}
	else // if the start node is not !NULL
*/
	{
		if (lat->nodes[p_link->E]->tStartBoundary > lat->nodes[p_link->S]->t)
		{
			lat->nodes[p_link->E]->tStartBoundary = lat->nodes[p_link->S]->t;
		}

		double conf =
			alpha // alpha
			+ (scaleOnKeyword ? kwd_scale : 0)
			+ (link_wipen // insertion penalty
			   + p_link->a * amscale // acoustic likelihood
			   + p_link->l * lmscale // language likelihood
			  ) * posteriorscale
			+ beta
			- whole_lattice_posterior; // normalize by the best path
		
		return conf;
	}
}


/**
  @brief Compute each link's confidence and set Lattice::Node's parameters:
         bestLikelihood, bestLinkID, tStart
*/
void LatViterbiFwBw::computeLinksLikelihood(LatTime startTime) {
	// values for first node in lattice have to be set manually
	Lattice::Node* firstNode = *lat->nodes.begin();
	lat->nodes[firstNode->id]->tStart = startTime;
	lat->nodes[firstNode->id]->tStartBoundary = 0;
	lat->nodes[firstNode->id]->bestLikelihood = 0; // log(1) = 0
	lat->nodes[firstNode->id]->bestLinkID = -1;
	lat->nodes[firstNode->id]->bestBckLinkID = -1;
	TLatViterbiLikelihood arr_bestLikelihood_scaleOnKeyword[lat->nodes.size()]; // array of best likelihoods with scaling on keyword
	TLatViterbiLikelihood arr_bestFwdLikelihood[lat->nodes.size()];				// array of best outgoing link's confidence
	TLatViterbiLikelihood arr_bestBckLikelihood[lat->nodes.size()];				// array of best ingoing link's confidence

	for (unsigned int i=0; i<lat->nodes.size(); i++) {
		arr_bestLikelihood_scaleOnKeyword[i] = -INF;
		arr_bestBckLikelihood[i] = -INF;
		arr_bestFwdLikelihood[i] = -INF;
		lat->nodes[i]->bestLikelihood = -INF;
	}
		 
	// now compute all confidences
	for (Lattice::Links::iterator l=lat->links.begin(); l!=lat->links.end(); ++l) 
	{
		(*l)->likelihood					= (*l)->a*amscale + (*l)->l*lmscale;
//		DBG_FORCE("link #"<<(*l)->id<<" likelihood:"<<(*l)->likelihood);
		(*l)->confidence					= getLinkConfidence((*l)->id, true);
		(*l)->confidence_noScaleOnKeyword	= getLinkConfidence((*l)->id, false);
		
		// set the best confidence for link's startNode 
		if (arr_bestFwdLikelihood[(*l)->S] < (*l)->confidence) {
			arr_bestFwdLikelihood[(*l)->S] = (*l)->confidence; // set the best forward confidence
			lat->nodes[(*l)->S]->bestFwdLinkID = (*l)->id; // set the best forward-link ID
		}
		// set the best confidence for link's endNode 
		if (lat->nodes[(*l)->E]->bestLikelihood < (*l)->confidence) 
		{
			lat->nodes[(*l)->E]->bestLikelihood = (*l)->confidence;
			lat->nodes[(*l)->E]->bestBckLinkID = (*l)->id;
			lat->nodes[(*l)->E]->bestLinkID = (*l)->id;					// id of node's ingoing link with best confidence
			lat->nodes[(*l)->E]->tStart = lat->nodes[(*l)->S]->t;		// node's start time = end time of link's start node
		}
	}

/*
	// get start time boundary for each node
	for (Lattice::Nodes::iterator iNode = lat->nodes.begin(); iNode != lat->nodes.end(); iNode++)
	{
		Lattice::Node *p_node = *iNode;
		vector<ID_t>* v = &(lat->bckLinks[p_node->id]);
		for (vector<ID_t>::iterator iLinkId = v->begin(); iLinkId != v->end(); iLinkId++)
		{
			Lattice::Link* p_link = lat->links[*iBckLinkId];
			Lattice::Node* p_s_node = lat->nodes[p_link->S];
			// if the previous node is !NULL
			if (lat->nodes[p_link->S]->W == "!NULL")
			{
				vector<ID_t>* v_bck = &(lat->bckLinks[p_link->S]);
				for (vector<ID_t>::iterator iBckLinkId = v_bck->begin(); iBckLinkId != v_bck->end(); iBckLinkId++)
				{
					Lattice::Link* p_bck_link = lat->links[*iBckLinkId];
					Lattice::Node* p_s_s_node = lat->nodes[p_bck_link->S];
					if (p_node->tStartBoundary > p_s_s_node->t)
					{
						p_node->tStartBoundary = p_s_s_node->t;
					}
				}
			}
			else
			{
				if (p_node->tStartBoundary > p_s_node->t)
				{
					p_node->tStartBoundary = p_s_node->t;
				}
			}
		}
	}
*/
}


