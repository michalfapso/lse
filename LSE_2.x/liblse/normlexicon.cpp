#include "normlexicon.h"
#include "normlexiconfile.h"

using namespace std;
using namespace lse;

NormLexicon::NormLexicon(Lexicon * pWordLexicon, std::string normLexiconFilename)
{
	mPhnNorm = 0;
	mFrmNorm = 0;
	mGlobNorm = 0;

	if (pWordLexicon != NULL)
	{
		SetWordLexicon(pWordLexicon);
	}
	else
	{
		mpWordLexicon = NULL;
	}

	if (normLexiconFilename != "")
	{
		LoadFromNormLexiconFile(normLexiconFilename);
	}
	else
	{
		mpNormArray = NULL;
		mNormArraySize = 0;
	}
}

bool NormLexicon::IsActive()
{
	DBG_FORCE("NormLexicon::mpNormArray = "<<mpNormArray);
	return mpNormArray != NULL;
}

void NormLexicon::SetWordLexicon(Lexicon * pWordLexicon)
{
	mpWordLexicon = pWordLexicon;
}

void NormLexicon::LoadFromNormLexiconFile(string filename)
{
	DBG_FORCE("NormLexicon::LoadFromNormLexiconFile("<<filename<<")");
	NormLexiconFile normFile(filename);
	NormLexiconFileRec normRec;

	mNormArraySize = mpWordLexicon->size();
	mpNormArray = new float[mNormArraySize];

	while(normFile.Next(&normRec))
	{
		if (normRec.mWord == NORM_LABEL_PHN)
		{
			mPhnNorm = normRec.mNormConst;
		}
		else if (normRec.mWord == NORM_LABEL_FRM)
		{
			mFrmNorm = normRec.mNormConst;
		}
		else if (normRec.mWord == NORM_LABEL_GLOB)
		{
			mGlobNorm = normRec.mNormConst;
		}
		else if (normRec.mWord == NORM_LABEL_GLOB_OOV)
		{
			mGlobOovNorm = normRec.mNormConst;
		}
		else
		{
			ID_t word_id = mpWordLexicon->getValID(normRec.mWord);
			mpNormArray[word_id - 1] = normRec.mNormConst;
		}
//		DBG_FORCE("normRec:"<<normRec.mWord<<" "<<normRec.mNormConst);
	}
}


float NormLexicon::operator[] (const int wordId)
{
	return mpNormArray[wordId-1];
}

float NormLexicon::GetWordNormConst(const std::string &word)
{
	return mpNormArray[mpWordLexicon->getValID(word) - 1]; // -1 because of word id starts from 1
}
