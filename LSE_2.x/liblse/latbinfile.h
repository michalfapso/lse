#ifndef LATBINFILE_H
#define LATBINFILE_H

#include <queue>
#include <iostream>
#include <map>
#include <list>
#include <zlib.h>
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
#include "latmeetingindexer.h"
#include "lexicon.h"
//#include "hypothesis.h"
//#include "querykwdlist.h"
#include "timer.h"



#define LATBINFILE_ERROR_IO			2
#define TIME_INDEX_RECORD_FREQUENCY	0.5


//static const int linkArrayRecordWidth = sizeof(ID_t) + sizeof(ID_t) + sizeof(LatticeLink_a) + sizeof(LatticeLink_l);




namespace lse {

//--------------------------------------------------------------------------------
// LatBinFile
//--------------------------------------------------------------------------------	
class LatBinFile {
	
	// statistical variables
	Timer dbgTimer;
	double cumulTime_load_time;
	double cumulTime_load_nodes;
	double cumulTime_load_nodelinks;

public:
	class Node;
	class Link;
	class NodeLinks;
	class NodesArray;
	class LatNodeLinksArray;
	typedef std::map<ID_t, NodeLinks> NodeLinksMap;

	typedef std::vector<LatBinFile::Link> FwdLinks;
	typedef std::vector<LatBinFile::Link> BckLinks;


private:

	class IDWithLikelihood {
		
		public:
			ID_t id;
			TLatViterbiLikelihood likelihood;
			
			friend std::ostream& operator<<(std::ostream& os, const IDWithLikelihood& r) {
				return os << "\tid=" << r.id << "\tlikelihood=" << r.likelihood;
			}
	};
	typedef std::list< IDWithLikelihood > IDWithLikelihoodList;

	
	
	unsigned char * fwdLinksArray;
	unsigned char * bckLinksArray;
	ID_t firstNodeID;
	ID_t lastNodeID;
	
	bool outputFileEmpty;
	ID_t lastInsertedNodeID;
	LatTime lastInsertedTime;

	TLatViterbiLikelihood lastInsertedAlpha;

	long getNodePositionInNodesFile(ID_t nodeID);
	long getNodePositionInMeetingFile(ID_t nodeID);
	
	void readNodeLinksRecord(FILE *fin, LatBinFile::NodeLinks *nodeLinks);
	void readLinksRecord(FILE *fin, LatBinFile::Link &link);

//	Lattice::Node getNodeFromNodesArray(ID_t startNodeID, ID_t id);

	long getNodePositionInNodeArray(ID_t nodeID);

	int getNodeLinksTimeBorders(const std::string filename, LatTime startTime, LatTime endTime, ID_t * startNodeID = NULL, ID_t * endNodeID = NULL);

public:

	void readNodesRecord(FILE *fin, LatBinFile::Node &node);
	void writeNodesRecord(FILE *fin, LatBinFile::Node &node);


	//--------------------------------------------------------------------------------
	// LatBinFile::Node
	//--------------------------------------------------------------------------------
	class Node 
	{
		public:
			ID_t id;
			ID_t W_id;
			LatTime tStart;
			LatTime t;
			TLatViterbiLikelihood conf; ///< confidence
			TLatViterbiLikelihood alpha; ///< alpha (forward probability)
			TLatViterbiLikelihood beta; ///< (backward probability)

			friend std::ostream& operator<<(std::ostream& os, const lse::LatBinFile::Node& node) {
				return os <<"ID="<<node.id<<"\tW_id="<<node.W_id<<"\ttStart="<<node.tStart<<"\tt="<<node.t<<"\tconf="<<node.conf<<"\talpha="<<node.alpha<<"\tbeta="<<node.beta;
			}
			void writeTo(std::ofstream &out); ///< append the binary data to the given file descriptor
			void readFrom(std::ifstream &in); ///< read node's data from the given file descriptor
	};


	//--------------------------------------------------------------------------------
	// LatBinFile::Link
	//--------------------------------------------------------------------------------
	class Link 
	{

		public:

			static const int RecordSize = sizeof(ID_t) + sizeof(TLatViterbiLikelihood);

			ID_t nodeID;
			TLatViterbiLikelihood likelihood;
	//		FwBwValue fw; ///< forward proba.
	//		FwBwValue bw; ///< backward proba.


			bool operator<(const Link& b) {
				return nodeID < b.nodeID;
			}
			
	//		Lattice::Link toLatticeLink(ID_t startNodeID, DirectionType dir);
			friend std::ostream& operator<<(std::ostream& os, const Link& l) {
				return os << "\tnodeID=" << l.nodeID << "\tlikelihood=" << l.likelihood;
			}
	};

	//--------------------------------------------------------------------------------
	// LatBinFile::NodeLinks
	//--------------------------------------------------------------------------------
	class NodeLinks 
	{

		public:
			class Link 
			{
				public:
					ID_t nodeID; ///< ID of end or start node of this link (forward or backward link)
					float conf; ///< confidence of word (represented by this link)

					void writeTo(std::ofstream &out);
					void readFrom(std::ifstream &in);
					void readFrom(unsigned char * a);
			};
			
			ID_t id;
			int fwdLinksCount; // index of first backward link in links array
			int bckLinksCount;
//			bool isFirstNode; // first node in lattice 
		//	unsigned char * links;
			FwdLinks fwdLinks;
			BckLinks bckLinks;

			NodeLinks() { }
			// copy constructor:
			NodeLinks(const NodeLinks &nl);
			~NodeLinks() {
		/*		if (links != NULL)
					delete[] links;
		*/
			}
			
			LatBinFile::Link* bestFwdLink();
			LatBinFile::Link* bestBckLink();

			friend std::ostream& operator<<(std::ostream& os, const NodeLinks& nl) {
				return os << "ID=" << nl.id << "\tfwdLinksCount=" << nl.fwdLinksCount << "\tbckLinksCount=" << nl.bckLinksCount << "\tfwdLinksSize=" << nl.fwdLinks.size() << "\tbckLinksSize" << nl.bckLinks.size();
			}
			void writeHeaderTo(std::ofstream &out);
			void readHeaderFrom(std::ifstream &in);
			void readHeaderFrom(unsigned char ** a);
			static int getHeaderSize(); ///< header size in bytes
//			void read(std::ifstream &in);
//			void free();
	};


	//--------------------------------------------------------------------------------
	// LatBinFile::TimeRecord
	//--------------------------------------------------------------------------------
	class TimeRecord {
		public:
			LatTime t;
			ID_t nodeID;

			friend std::ostream& operator<<(std::ostream& os, const lse::LatBinFile::TimeRecord& tRec) {
				return os <<"t="<<tRec.t<<"\tnodeID="<<tRec.nodeID;
			}
			void writeTo(std::ofstream &out);
			void readFrom(std::ifstream &in);
	};


	//--------------------------------------------------------------------------------
	// LatBinFile::AlphaLastRecord
	//--------------------------------------------------------------------------------
	class AlphaLastRecord {
		public:
			ID_t nodeID;
			TLatViterbiLikelihood alphaLast;
			
			void writeTo(std::ofstream &out);
			void readFrom(std::ifstream &in);
			void readFrom(unsigned char ** a);
	};

	//--------------------------------------------------------------------------------
	// LatBinFile::AlphaLastArray
	//--------------------------------------------------------------------------------
	class AlphaLastArray {
		private:
			AlphaLastRecord *alphaArray;
			int arraySize;
			int lastFoundIndex;

		public:
			AlphaLastArray() : alphaArray(NULL), arraySize(0), lastFoundIndex(0) {};
			~AlphaLastArray() { this->free(); }

			int size() { return this->arraySize; }
			int load (const std::string &filename);
			void print();
			TLatViterbiLikelihood getAlphaLast(ID_t nodeID);
			void free();
	};
	
	//--------------------------------------------------------------------------------
	// LatBinFile::NodesArray
	//--------------------------------------------------------------------------------
	class NodesArray 
	{
		private:	

			LatBinFile::Node *nodesArray;
			int nodesCount;

		public:
			int firstID;
			int lastID;

			static const int nodeRecordWidth = sizeof(LatBinFile::Node);
			
			NodesArray() : nodesArray(NULL), firstID(0), lastID(0) {};
			~NodesArray() { this->free(); }
			
			LatBinFile::Node& operator[](int id);
			ID_t firstNodeID() { return this->firstID; }
			ID_t lastNodeID() { return this->lastID; }
			int size() { return this->nodesCount; }
			bool containsID(ID_t nodeID);
			int load(const std::string filename, int firstID, int lastID);
			void free();
			long getNodePositionInNodesFile(ID_t nodeID);
			int getNodesFileItemsCount(const std::string filename);
			
	}; // class NodesArray
		
		
	//--------------------------------------------------------------------------------
	// LatBinFile::LatNodeLinksArray
	//--------------------------------------------------------------------------------
	class LatNodeLinksArray {

			unsigned char * array; 				///< array of data from nodeLinks file
			unsigned long arraySize; 			///< size of array (in bytes)
			long * arrayPointers; 				///< array of pointers into the array of unsigned chars (node IDs start from 0 and array indices also start from 0)
			unsigned long arrayPointersSize; 	///< size of arrayPointers (not in bytes, but number of values in the array)
			long nodeLinksStartPos;				///< offset to the start of NodeLinks file - this value has to be subtracted from arrayPointers[] values

			

		public:

/*			class Link 
			{
				public:
					ID_t endNodeID;
					TLatViterbiLikelihood likelihood;
			};
*/
			class Cursor 
			{
					unsigned char * ptr;							///< pointer to LatNodeLinksArray::Link record
					int idx;										///< current node's link index
					LatNodeLinksArray *latNodeLinksArray;			///< cursor points to this instance of LatNodeLinksArray
					int linksCount;									///< current node's links count
				public:

					Cursor(LatNodeLinksArray *p_latNodeLinksArray) {
						this->latNodeLinksArray = p_latNodeLinksArray;
					}
					bool gotoNodeID(ID_t nodeID, DirectionType dir);///< move to the given node's first link
					bool next();									///< move to the next link (if possible)
					bool eol();										///< end of links list indicator
					LatBinFile::NodeLinks::Link getLink();			///< get current link
					int getLinksCount() { return this->linksCount; }
			};

			int firstID;
			int lastID;



			LatNodeLinksArray() : array(NULL), arraySize(0), arrayPointers(NULL), arrayPointersSize(0), firstID(0), lastID(0) {};
			~LatNodeLinksArray() {
				this->free();
			}

			ID_t bestFollowingNode(ID_t nodeID, DirectionType dir, TLatViterbiLikelihood * bestLikelihood);
			int getFwdLinksCount(ID_t nodeID);
			int getBckLinksCount(ID_t nodeID);
			bool containsID(ID_t nodeID);
			int load(const std::string filename, ID_t startNodeID, ID_t endNodeID);
			void printLinks(std::ostream &out, ID_t nodeID, DirectionType direction);
			long size(); ///< return size in bytes
			long linksCount(); ///< number of all loaded links (!!!NOT WELL TESTED!!!)
			void free();
		//	long getNodePositionInNodesFile(ID_t nodeID);
	};



	Lattice * lat;
	Lexicon * mpLexicon;
	Lattice::Indexer * mpIndexer;
	LatIndexer * mpInvIdx;
	LatNodeLinksArray nodeLinks;
	NodesArray nodes;
	AlphaLastArray alphaLastArray;
//	float mWipen; ///< word/phoneme insertion penalty (used in the verification step)

	
	LatBinFile() {
		this->lat = NULL;
		this->outputFileEmpty = true;
		this->lastInsertedNodeID = -1; // next nodeID will be 0
		this->lastInsertedTime = -1;
//		this->mWipen = 0;
//		nodes = NULL;
		cumulTime_load_time = 0;
		cumulTime_load_nodes = 0;
		cumulTime_load_nodelinks = 0;
	}
	
	LatBinFile(Lattice &lattice) {
		this->lat = &lattice;
		mpLexicon = lat->lexicon;
		mpIndexer = lat->indexer;

		cumulTime_load_time = 0;
		cumulTime_load_nodes = 0;
		cumulTime_load_nodelinks = 0;
//		nodes = NULL;
	}

/*	~LatBinFile() {
		if (nodes != NULL) {
			delete[] nodes;
		}
	}
*/

	void setLattice(Lattice &lattice) {
		this->lat = &lattice;
		SetLexicon(this->lat->lexicon);
		SetIndexer(this->lat->indexer);
	}
	void SetLexicon(Lexicon *pLexicon) { mpLexicon = pLexicon; }
	void SetIndexer(Lattice::Indexer *pIndexer) { mpIndexer = pIndexer; }
//	void SetWipen(float wipen) { mWipen = wipen; }
//	float GetWipen() {return mWipen; }
	int print_TimeIndex(const std::string filename);
	
	int save(const std::string filename, const std::string meeting_rec, bool append = false, bool createFwdIndex = true);
	int saveAllBestPathNodes(const std::string filename);
	int convertToTextFile(const std::string filename);
	int closeConcatenatedLatticeOutput(const std::string filename, const std::string meeting_rec, bool createFwdIndex = true);
	void cleanOutputFiles(const std::string filename);

//	int load(const std::string filename);
//	int load(const std::string filename, long centerNodePosition, ID_t *centerNodeID);
	int load(const std::string filename, LatTime startTime, LatTime endTime);
	void free(); // free loaded content

//	void getKeywordContext(ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, Hypothesis *hyp, QueryKwdList *queryKwdList, QueryKwdList::iterator iQueryKwd);
//	bool getPhraseKeywordContext(ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, QueryKwdList::iterator curKwd, QueryKwdList *queryKwdList, Hypothesis *hyp);
//	bool matchPhraseContext_recursive(ID_t nodeID, TLatViterbiLikelihood likelihood, QueryKwdList::iterator iQueryKwd, QueryKwdList *queryKwdList, IDWithLikelihoodList *res);
//	bool matchPhraseContext_breadthFirst(ID_t centerNodeID, TLatViterbiLikelihood likelihood, QueryKwdList::iterator iQueryKwd, QueryKwdList *queryKwdList, IDWithLikelihoodList *res);

	void stat();

};

} // namespace lse

#endif
