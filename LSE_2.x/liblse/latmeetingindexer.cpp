#include "latmeetingindexer.h"

using namespace std;
using namespace lse;


void LatMeetingIndexer::addRecord(const LatMeetingIndexerRecord val) {
	val2idMap.insert(make_pair(val.path, val.id));
	id2valMap.insert(make_pair(val.id, val));
}


ID_t LatMeetingIndexer::insertRecord(LatMeetingIndexerRecord *rec)
{
	DBG_FORCE("LatMeetingIndexerRecord::insertRecord("<<rec->path<<")");
	DBG_FORCE("size:"<<val2idMap.size());
	//DBG_FORCE("begin:"<<val2idMap.begin()->first);
	map<string, ID_t>::iterator pos = val2idMap.find(rec->path);
	if (pos != val2idMap.end()) {
		// ID of given rec is returned
		if (id2valMap[pos->second].length != rec->length)
			id2valMap[pos->second].length = rec->length;

		return pos->second;		

	} else {
		// new record is created and it's ID is returned
		lse::ID_t newID = getNewID();
		rec->id = newID;
		addRecord(*rec);
		return newID;
	}
}

ID_t LatMeetingIndexer::getRecID(const string &path) {

	// try to find given val in val2idMap
	map<string, ID_t>::iterator pos = val2idMap.find(path);
//	Val2ID_iter pos = val2idMap.find(val);
	
	if (pos != val2idMap.end()) {
		return pos->second;		

	} else {
		// value has not been found, so ERROR is returned
		return -1;
	}
}

LatMeetingIndexerRecord LatMeetingIndexer::getVal(const lse::ID_t id) {
	
	map<ID_t, LatMeetingIndexerRecord>::iterator pos = id2valMap.find(id);
//	ID2val_iter pos = id2valMap.find(id);

	if (pos != id2valMap.end()) {
		// val with given ID is returned
		return pos->second;
	} 
	else {
		LatMeetingIndexerRecord tmpRec;
		tmpRec.id = -1;
		tmpRec.path = "";
		tmpRec.length = 0;
		// ERROR: there is no such val in the lexicon
		return tmpRec;
	}
}


ID_t LatMeetingIndexer::getNewID() {

	// Sequence search for max key (ID) value
	lse::ID_t max = GI_START_INDEX - 1; // index starts from GI_START_INDEX...max is going to be max+1

	for (map<ID_t, LatMeetingIndexerRecord>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		if (max < i->first) {
			max = i->first;
		}
	}
	return max+1;
}

int LatMeetingIndexer::loadFromFile(const std::string filename) {
	
	// clear both maps
	id2valMap.clear();
	val2idMap.clear();

	DBG_FORCE("val2idMap.size():"<<val2idMap.size());

	std::ifstream in(filename.c_str());
	if (!in.good()) {
		return (2);
	}
	
	LatMeetingIndexerRecord val;
	
	// read all nodes and convert strID into str using lexicon
	while(!in.eof()) {
		
		in >> val.id;
		in >> val.path;
		in >> val.length;

		if (in.eof()) break;
		// add record to both maps
		addRecord(val);
		//DBG("LatMeetingIndexer::load(): "<<val);
	}
	
	DBG_FORCE("val2idMap.size():"<<val2idMap.size());

	in.close();
	
	return 0;
}



//--------------------------------------------------------------------------------
// saveToFile

int LatMeetingIndexer::saveToFile(const std::string filename) {
		
	std::ofstream out(filename.c_str());
	if (out.bad()) {
		return (2);
	}

	// it is not important which map to save to file, because they are identical
	for(map<ID_t, LatMeetingIndexerRecord>::iterator i=id2valMap.begin(); i!=id2valMap.end(); ++i) {
		
		out << i->second.id << "\t" << i->second.path << "\t" << i->second.length << endl;
		
	}
	out.close();
	return 0;
}


//--------------------------------------------------------------------------------
// print()

void LatMeetingIndexer::print() {
	
	for (map<ID_t, LatMeetingIndexerRecord>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		
		std::cout << "ID=" << i->first << '\t' << i->second << std::endl;
		
	}

}

