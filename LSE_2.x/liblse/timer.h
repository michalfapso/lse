#ifndef TIMER_H
#define TIMER_H

#if defined(_WIN32) || defined(MINGW)

	#include <windows.h>
	class Timer {
		LARGE_INTEGER _tstart, _tend;
		LARGE_INTEGER freq;
	public:
		void start(void);
		void end(void);
		double val();
	};

#else

	#include <sys/time.h>
	#include <unistd.h>
	class Timer {
		struct timeval _tstart, _tend;
		struct timezone tz;
	public:
		void start();
		void end();
		double val();
	};

#endif

#endif
