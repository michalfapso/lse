#ifndef NGRAM_H
#define NGRAM_H

#include <sstream>
#include <list>
#include "lattypes.h"

namespace lse {

class Ngram 
{
	int mN;
	LatTime mTStart;
	LatTime mTEnd;
	ID_t *mpItems;
	Confidence mConf;
	int mHash;
	std::vector<ID_t> mNodePath;

public:

	/**
	 * @brief Ngram constructor - empty method - should be followed by a copy constructor
	 */
	Ngram();

	/**
	 * @brief Ngram constructor
	 *
	 * @param n 1..unigram, 2..bigram, 3..trigram, etc.
	 */
	Ngram(int n);

	/**
	 * @brief Ngram copy constructor
	 *
	 * @param &ngram_parent Ngram instance from which to copy
	 */
	Ngram(const Ngram &ngram_parent);

	void CopyParamsFrom(const Ngram &ngram_parent);
//	void CopyParamsFrom(const Ngram* p_ngram_parent) { CopyParamsFrom(*p_ngram_parent); }

	/**
	 * @brief Ngram destructor
	 */
	~Ngram();

	friend std::ostream& operator<<(std::ostream& os, const Ngram &ngram)
	{
		os << ngram.GetStartTime() << "\t" << ngram.GetEndTime() << "\t";
		for (int i=ngram.mN-1; i>=0; i--)
		{
			os << "[" << ngram.GetItemWordId(i) << "]" << (i>0 ? " " : "");
		}
		return os;
	}

	friend inline bool operator==(const Ngram &a, const Ngram &b)
	{
		assert(a.mN == b.mN);
		for (int i=0; i<a.mN; i++)
		{
			if (a.GetItemWordId(i) != b.GetItemWordId(i))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * @brief set n-th item (indexing from 0) of ngram
	 *
	 * @param n item's index
	 * @param item what to write on n-th position of ngram
	 */
	void SetItem(int n, LatTime tStart, LatTime tEnd, ID_t W_id);

	inline void SetStartTime(LatTime t) { mTStart = t; }
	inline void SetEndTime(LatTime t) { mTEnd = t; }

	inline void SetConfidence(Confidence conf) { mConf = conf; }

	inline void AddNodeToPath(ID_t nodeId) { mNodePath.push_back(nodeId); }

	/**
	 * @brief get a word id of n-th item (indexing from 0) of ngram
	 *
	 * @param n item's index
	 *
	 * @return word id
	 */
	inline ID_t GetItemWordId(int n) const { return mpItems[n]; }

	inline LatTime GetStartTime() const { return mTStart; }
	inline LatTime GetEndTime() const { return mTEnd; }

	inline Confidence GetConfidence() const { return mConf; }
	
	inline int GetHash() const { return mHash; }

	inline std::string GetNodePath() const 
	{
		std::ostringstream stream;
		for(std::vector<ID_t>::const_iterator i=mNodePath.begin(); i!=mNodePath.end(); i++)
		{
			stream << " | " << *i;
		}
		return stream.str();
	}

};


typedef std::list<Ngram*> NgramsHashItemList;

class NgramsCursor;
class Ngrams
{
		NgramsHashItemList* mpHash;
		int mHashSize;

		int GetIndex(const Ngram* pNgram) const { return pNgram->GetHash() % mHashSize; }

	public:
		int* mpHashListSizes;
		int mSize;
		friend class NgramsCursor;

		Ngrams(int hashSize) : mHashSize(hashSize) 
		{
			mpHash = new NgramsHashItemList [hashSize];
			mpHashListSizes = new int [hashSize];
			for (int i=0; i<hashSize; i++) mpHashListSizes[i] = 0;
			mSize = 0;
		}

		~Ngrams() {	delete[] mpHash; delete[] mpHashListSizes; }

		void Insert(Ngram* pNgram);
		NgramsHashItemList* GetHashItemList(const Ngram* pNgram);
		int* GetHashItemListSize(const Ngram* pNgram);
		int Size() { return mSize; }
};


class NgramsCursor
{
		const Ngrams* mpNgrams;
		int mHashItem;
		NgramsHashItemList::iterator miNgram;
		Ngram* NextBegin();
	public:
		NgramsCursor(const Ngrams &ngrams) {
			mpNgrams = &ngrams;
			mHashItem = 0;
			miNgram = mpNgrams->mpHash[mHashItem].begin();
		}
		Ngram* Begin();
		Ngram* Next();
};


} // namespace lse
#endif
