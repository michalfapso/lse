#ifndef MLF_H
#define MLF_H

#include <vector>
#include <fstream>
#include <iostream>
#include "lattypes.h"

namespace lse {

	class MlfRec
	{
		public:
			std::string		mRecordPath;
			long double		mStartT;
			long double		mEndT;
			//	LatTime			mStartT;
			//	LatTime 		mEndT;
			std::string		mWord;
			float			mConf;

			void Reset() 
			{
				mRecordPath = "";
				mStartT		= 0;
				mEndT		= 0;
				mWord		= "";
				mConf		= 0;
			}		

			friend std::ostream& operator<<(std::ostream& os, const MlfRec &r) {
				return os << "record:"<<r.mRecordPath<<" "<<r.mStartT<<" "<<r.mEndT<<" "<<r.mWord<<" "<<r.mConf;
			}
	};

	typedef std::vector< MlfRec > MlfParentType;

	class Mlf : public MlfParentType
	{

		std::istream * input_mFile;
		std::ostream * output_mFile;
		std::ofstream * pom_ofstream;

		bool IsEol(const char c);
		bool IsDelimiter(const char c);

		public:
		bool STDOUT_Backoff;
		bool WasLABbefore;
		std::string mlffilename;

		Mlf(const std::string filename = "") 
		{
			input_mFile = new std::istream(std::cin.rdbuf());
			output_mFile = new std::ostream(std::cout.rdbuf());
			pom_ofstream = new std::ofstream;
			STDOUT_Backoff=false;
			WasLABbefore = false;
			mlffilename="";
			if (filename != "") 
			{
				mlffilename=filename;
				Open(filename);
			}
		}
		~Mlf()
		{
			delete input_mFile;
			delete output_mFile;
			delete pom_ofstream;
		}

		void Open(const std::string filename="");
		void Close();
		void Load();
		bool Next(MlfRec *rec);
		void Save(const std::string filename="");
		void AddRecord(MlfRec &rRec);

		void SaveForAppend(const std::string filename = "");
		void SetMLFFileName(const std::string in_mlffilename);
		void SaveMLFHeader();
		void SaveLABHeader(const std::string LabName = "");
		void SaveLABLine(std::string &Line);
		void SaveRecord(MlfRec &rRec);
		void Enable_STDOUT_Backoff();
		void Disable_STDOUT_Backoff();

	};

}

#endif
