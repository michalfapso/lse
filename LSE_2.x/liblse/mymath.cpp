#include <iostream>
#include <iomanip>
#include "mymath.h"
#include <cmath>
#include "lattypes.h"

using namespace lse;
using namespace std;


double lse::logAdd(double a, double b, float treshold) {
//	treshold=50;
//	return (a>b) ? a : b;
	if (a < b) {
		double tmp = a;
		a = b;
		b = tmp;
	}
	double diff = b - a;
//	if (-diff > treshold) {
//		return a;
//	}
//	else
	{
        return log(1.0 + exp(diff)) + a;
	}
//      std::cerr << a << " " << exp(a) << " " << b << " " << exp(b) << " " << log(exp(a) + exp(b))<< std::endl;
//      return log(exp(a) + exp(b));
}

double lse::logit(double p)
{
	if (p <= 0) { return -INF; }
	if (p >= 1) { return +INF; }
	return log(p/(1-p));
}

/*
double dotproduct(Mat<double> *pM1, Mat<double> *pM2)
{
	assert(pM1->columns() == pM2->rows());
	assert(pM1->rows() == 1);
	assert(pM2->columns() == 1);

	int dims = pM1->columns();

	double dotp = static_cast<double>(0.0);
	double *pv1 = const_cast<double *>(pM1->getMem());
	double *pv2 = const_cast<double *>(pM2->getMem());
	int i;
	for(i = 0; i < dims; i++)
		dotp += pv1[i] * pv2[i];


	int rem = dims % 8;
	double dotpp[8];
	for(i = 0; i < 8; i++)
		dotpp[i] = static_cast<double>(0.0);

	int j;
	for(i = 0; i < dims - rem; i += 8)
	{

		for(j = 0; j < 8; j++)
			dotpp[j] += pv1[j] * pv2[j];
		pv1 += 8;
		pv2 += 8;
	}
	for(j = 0; j < rem; j++)
		dotpp[j] += pv1[j] * pv2[j];

	double dotp1 = static_cast<double>(0.0);
	for(i = 0; i < 8; i++)
		dotp1 += dotpp[i];            

	//	int i;
	//	for(i = 0; i < dims; i++)
	//		dotp += pv1[i] * pv2[i];


	return dotp;
}
*/

