#ifndef MYMATH_H
#define MYMATH_H

#include "matrix.h"

namespace lse 
{
	double logAdd(double a, double b, float treshold = 50);
	double logit(double p);

//	double dotproduct(Mat<double> *pM1, Mat<double> *pM2);

	template <typename FP_TYPE>
	FP_TYPE dotproduct(Mat<FP_TYPE> *pM1, Mat<FP_TYPE> *pM2)
	{
		assert(pM1->columns() == pM2->rows());
		assert(pM1->rows() == 1);
		assert(pM2->columns() == 1);

		int dims = pM1->columns();

		FP_TYPE dotp = static_cast<FP_TYPE>(0.0);
		FP_TYPE *pv1 = const_cast<FP_TYPE *>(pM1->getMem());
		FP_TYPE *pv2 = const_cast<FP_TYPE *>(pM2->getMem());
		int i;
		for(i = 0; i < dims; i++)
			dotp += pv1[i] * pv2[i];


		int rem = dims % 8;
		FP_TYPE dotpp[8];
		for(i = 0; i < 8; i++)
			dotpp[i] = static_cast<FP_TYPE>(0.0);

		int j;
		for(i = 0; i < dims - rem; i += 8)
		{

			for(j = 0; j < 8; j++)
				dotpp[j] += pv1[j] * pv2[j];
			pv1 += 8;
			pv2 += 8;
		}
		for(j = 0; j < rem; j++)
			dotpp[j] += pv1[j] * pv2[j];

		float dotp1 = static_cast<FP_TYPE>(0.0);
		for(i = 0; i < 8; i++)
			dotp1 += dotpp[i];            

		//	int i;
		//	for(i = 0; i < dims; i++)
		//		dotp += pv1[i] * pv2[i];


		return dotp;
	}
/*
	template <typename FP_TYPE>
	FP_TYPE dotproduct(Mat<FP_TYPE> &pM1, Mat<FP_TYPE> &pM2)
	{
		dotproduct(&pM1, &pM2);
	}
*/
}

#endif
