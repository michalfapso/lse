#include <vector>
#include <map>
#include "pronlexicon.h"
#include "dctfile.h"

using namespace std;
using namespace lse;

PronLexicon::PronLexicon(Lexicon * pWordLexicon)
{
	SetWordLexicon(pWordLexicon);
}

void PronLexicon::SetWordLexicon(Lexicon * pWordLexicon)
{
	mpWordLexicon = pWordLexicon;
}

void PronLexicon::LoadFromDct(string filename)
{
	map<ID_t, vector<string> > pron_map;

	DctFile dctFile(filename);
	DctFileRec dctRec;
	while(dctFile.Next(&dctRec))
	{
		ID_t word_id = mpWordLexicon->getValID(dctRec.mWord);
		pron_map[word_id].push_back(dctRec.mPron);
	}

	// get the pronounciation array size
	mPronArraySize = 0;
	for (map<ID_t, vector<string> >::iterator iWord = pron_map.begin();
		 iWord != pron_map.end();
		 iWord ++)
	{
		for (vector<string>::iterator iPron = iWord->second.begin();
			 iPron != iWord->second.end();
			 iPron ++)
		{
			mPronArraySize += (*iPron).length() + 1; // +1 is to store the string terminator 0x00
		}
	}

	// create the pronounciation array
	if (mpPronArray != NULL)
	{
		delete[] mpPronArray;
	}
	mpPronArray = new char[mPronArraySize];
	char *p_pron_array_cur = mpPronArray;

	// create the pronounciation array index:
	// wordID => pronounciation variants count, pointer to mpPronArray
	mPronArrayIndexSize = mpWordLexicon->size();
	mpPronArrayIndex = new PronIndexRecord[mPronArrayIndexSize];
	PronIndexRecord *p_pron_array_index_cur = mpPronArrayIndex;

	// notice that words index starts at 1
	for (ID_t word_id=1; word_id<=mPronArrayIndexSize; word_id++)
	{
		map<ID_t, vector<string> >::iterator iWord = pron_map.find(word_id);
		if (iWord == pron_map.end())
		{
			p_pron_array_index_cur->mCount = 0;
			p_pron_array_index_cur->mPronPos = 0;
		}
		else
		{
			p_pron_array_index_cur->mCount = pron_map[word_id].size();
			p_pron_array_index_cur->mPronPos = p_pron_array_cur - mpPronArray;
		}
		p_pron_array_index_cur++;

		for (vector<string>::iterator iPron = iWord->second.begin();
			 iPron != iWord->second.end();
			 iPron ++)
		{
			strcpy(p_pron_array_cur, iPron->c_str());
			p_pron_array_cur += iPron->length() + 1; // +1 is to store the string terminator 0x00
		}
	}
}


PronLexicon::PronVariants PronLexicon::operator[] (const int wordId)
{
	return PronVariants(mpPronArrayIndex[wordId-1].mCount, mpPronArray + mpPronArrayIndex[wordId-1].mPronPos);
}

//--------------------------------------------------------------------------------
//	PronLexicon::PronVariants
//--------------------------------------------------------------------------------
PronLexicon::PronVariants::PronVariants(int count, const char* startPos)
{
	mpBegin = startPos;
	mCount = count;
}

PronLexicon::PronVariants::const_iterator PronLexicon::PronVariants::begin()
{
	return const_iterator(mCount, mpBegin);
}

PronLexicon::PronVariants::const_iterator PronLexicon::PronVariants::end()
{
	return const_iterator(mCount, mpBegin, mCount);
}

//--------------------------------------------------------------------------------
//	PronLexicon::PronVariants::const_iterator
//--------------------------------------------------------------------------------
PronLexicon::PronVariants::const_iterator::const_iterator(int count, const char* startPos, int curItem)
{
	mpBegin = mpCurrentPos = startPos;
	mCount = count;
	mCurrentItem = curItem;
}

bool PronLexicon::PronVariants::const_iterator::operator== (const const_iterator& i) const
{
	return (mpBegin == i.mpBegin && mCurrentItem == i.mCurrentItem);
}

bool PronLexicon::PronVariants::const_iterator::operator!= (const const_iterator& i) const
{
	return !(mpBegin == i.mpBegin && mCurrentItem == i.mCurrentItem);
}

PronLexicon::PronVariants::const_iterator& PronLexicon::PronVariants::const_iterator::operator++()
{
	while (*mpCurrentPos != 0x00) 
	{
		mpCurrentPos++;
	}
	mpCurrentPos++; // go to the beginning of the next item
	mCurrentItem++;
	return *this;
}

const char* PronLexicon::PronVariants::const_iterator::operator*() const
{
	return mpCurrentPos;
}

