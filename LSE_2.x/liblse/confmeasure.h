#ifndef CONFMEASURE_H
#define CONFMEASURE_H

#include "lat.h"

namespace lse
{

class ConfidenceMeasure
{
	protected:
		void UpdateLatticeWithNewLinkScores(Lattice *pLat, double *newScoresArray);
	public:
		virtual void ComputeLinksConfidence(Lattice *pLat) = 0;
};

class ConfidenceMeasure_LogaddOverlapping : public ConfidenceMeasure
{
	public:
		void ComputeLinksConfidence(Lattice *pLat);
};

class ConfidenceMeasure_CMax : public ConfidenceMeasure
{
	public:
		void ComputeLinksConfidence(Lattice *pLat);
};

}; // namespace lse
#endif
