#include "confmeasure.h"
#include "mymath.h"

#define FRAMES_PER_SECOND 100
#define WORD_LENGTH_VARIATION_MULTIPLIER 2

using namespace std;
using namespace lse;

// ConfidenceMeasure {{{
void ConfidenceMeasure::UpdateLatticeWithNewLinkScores(Lattice *pLat, double *newScoresArray)
{
	// set the new scores to all links in the lattice
	TLatViterbiLikelihood arr_bestFwdLikelihood[pLat->nodes.size()]; // array of best outgoing link's confidence
	for (unsigned int i=0; i<pLat->nodes.size(); i++) 
	{
		arr_bestFwdLikelihood[i] = -INF;
	}

	for (Lattice::Links::iterator l=pLat->links.begin(); l!=pLat->links.end(); l++)
	{
		(*l)->confidence = newScoresArray[(*l)->id];

		if (arr_bestFwdLikelihood[(*l)->S] < (*l)->confidence) {
			arr_bestFwdLikelihood[(*l)->S] = (*l)->confidence; // set the best forward confidence
			pLat->nodes[(*l)->S]->bestFwdLinkID = (*l)->id; // set the best forward-link ID
		}
		if (pLat->nodes[(*l)->E]->bestLikelihood < (*l)->confidence) 
		{
			pLat->nodes[(*l)->E]->bestLikelihood = (*l)->confidence;
			pLat->nodes[(*l)->E]->bestBckLinkID = (*l)->id;
			pLat->nodes[(*l)->E]->bestLinkID = (*l)->id;					// id of node's ingoing link with best confidence
			pLat->nodes[(*l)->E]->tStart = pLat->nodes[(*l)->S]->t;		// node's start time = end time of link's start node
		}
	}
}
// }}}

// ConfidenceMeasure_CMax {{{
void ConfidenceMeasure_CMax::ComputeLinksConfidence(Lattice *pLat)
{
	double new_scores[pLat->links.size()];
	bool processed_nodes[pLat->nodes.size()];

	for (unsigned int i=0; i<pLat->nodes.size(); i++)
	{
		processed_nodes[i] = false;
	}

	unsigned int total_overlapped_nodes_count = 0;

	// traverse nodes from the beginning to the end of the lattice
	for (unsigned int iNodeGlob=0; iNodeGlob < pLat->nodes.size(); iNodeGlob++)
	{
		// if the node was already processed, go for another one
		if (processed_nodes[iNodeGlob])
		{
			continue;
		}

		Lattice::Node* p_node_glob = pLat->nodes[iNodeGlob];

		// do not compute the CMax for meta words (!NULL, <s>, ...)
		if (pLat->lexicon->isMetaWord(p_node_glob->W))
		{
			continue;
		}

		//DBG("node_glob: "<<*p_node_glob);
		
		// Create a list of overlapped nodes (according to their links' time boundaries) {{{
		//
		list<Lattice::Node*> overlapped_nodes_list;

		LatTime word_length = p_node_glob->t - p_node_glob->tStartBoundary;
		LatTime t_min = p_node_glob->tStartBoundary - word_length * WORD_LENGTH_VARIATION_MULTIPLIER;
		if (t_min < 0.0) { t_min = 0; }
		LatTime t_max = p_node_glob->t + word_length * WORD_LENGTH_VARIATION_MULTIPLIER;
		LatTime t_group_start = p_node_glob->tStartBoundary;
		LatTime t_group_end = p_node_glob->t;
//		DBG("t_min:"<<t_min<<" t_max:"<<t_max<<" length:"<<word_length);

		unsigned int checked_nodes_count = 0;

//		DBG("Creating a list of possibly overlapped nodes...");
		// the node_glob itself can have more than 1 incoming link
		overlapped_nodes_list.push_back(p_node_glob);
		// check overlapping with nodes/links towards the end of the lattice
		for (unsigned int iNodeB=iNodeGlob+1; iNodeB < pLat->nodes.size(); iNodeB++)
		{
			checked_nodes_count++;
			if (processed_nodes[iNodeB])
			{
				continue;
			}
			Lattice::Node* p_node_b = pLat->nodes[iNodeB];
			//DBG("p_node_b:"<<*p_node_b);
			// if the node's label matches the p_node_glob's label
			if (p_node_b->t > t_max) { break; }
			if (p_node_glob->W != p_node_b->W) { continue; }

			if (is_overlapping(p_node_b->tStartBoundary, p_node_b->t, t_group_start, t_group_end))
			{
				overlapped_nodes_list.push_back(p_node_b);
				if (word_length < p_node_b->t - p_node_b->tStartBoundary)
				{
					word_length = p_node_b->t - p_node_b->tStartBoundary;
				}
				if (t_max < p_node_b->t + word_length * WORD_LENGTH_VARIATION_MULTIPLIER)
				{
					t_max = p_node_b->t + word_length * WORD_LENGTH_VARIATION_MULTIPLIER;
				}
				if (t_group_end < p_node_b->t)
				{
					t_group_end = p_node_b->t;
				}
				//DBG("overlapped! word_length:"<<word_length<<" t_max:"<<t_max);
			}
		}

		// check overlapping with nodes/links towards the beginning of the lattice
		if (iNodeGlob > 0)
		{
			for (unsigned int iNodeB=iNodeGlob-1; iNodeB > 0; iNodeB--) // we do not need to check the very first node
			{
				checked_nodes_count++;
				if (processed_nodes[iNodeB])
				{
					continue;
				}
				Lattice::Node* p_node_b = pLat->nodes[iNodeB];
				// if the node's label matches the iLink
				if (p_node_b->tStartBoundary < t_min) { break; }
				if (p_node_glob->W != p_node_b->W) { continue; }

				if (is_overlapping(p_node_b->tStartBoundary, p_node_b->t, t_group_start, t_group_end))
				{
					overlapped_nodes_list.push_back(p_node_b);
					if (word_length < p_node_b->t - p_node_b->tStartBoundary)
					{
						word_length = p_node_b->t - p_node_b->tStartBoundary;
					}
					if (t_min > p_node_b->tStartBoundary - word_length * WORD_LENGTH_VARIATION_MULTIPLIER)
					{
						t_min = p_node_b->tStartBoundary - word_length * WORD_LENGTH_VARIATION_MULTIPLIER;
					}
					if (t_group_start > p_node_b->tStartBoundary)
					{
						t_group_start = p_node_b->tStartBoundary;
					}
				}
			}
		}
		// }}}

		//DBG("checked_nodes_count:"<<checked_nodes_count<<" overlapped_nodes_list.size(): "<<overlapped_nodes_list.size());

		total_overlapped_nodes_count += overlapped_nodes_list.size();

		// check overlapping of all links of all nodes inside the overlapped_nodes_list
		for (list<Lattice::Node*>::iterator iNodeA=overlapped_nodes_list.begin(); iNodeA!=overlapped_nodes_list.end(); iNodeA++)
		{
			Lattice::Node* p_node_a = *iNodeA;
			processed_nodes[p_node_a->id] = true;
			//DBG("node_a: "<<*p_node_a);

			vector<ID_t>* v_bck_a = &(pLat->bckLinks[p_node_a->id]);
			for (vector<ID_t>::iterator iBckLinkIdA=v_bck_a->begin(); iBckLinkIdA!=v_bck_a->end(); iBckLinkIdA++)
			{
				Lattice::Link *p_link_a = pLat->links[*iBckLinkIdA];
	//			DBG("link_a: "<<*p_link_a);

				new_scores[p_link_a->id] = p_link_a->confidence;
				//DBG("p_link_a: "<<*p_link_a);
				double link_start = pLat->nodes[p_link_a->S]->t;
				double link_end = pLat->nodes[p_link_a->E]->t;

				// we will compute the score for each frame of the link
				double link_length = (pLat->nodes[p_link_a->E]->t - pLat->nodes[p_link_a->S]->t);
				int link_length_frames = (int)(link_length * FRAMES_PER_SECOND);
				if (link_length_frames == 0) {
					// probably a !NULL link
					//CERR("Warning: Link #"<<p_link_a->id<<" has zero length");
					continue;
				}
				double * frames_score = new double[link_length_frames];
				for (int i=0; i<link_length_frames; i++)
				{
					frames_score[i] = p_link_a->confidence;
				}

				// for each node that has incoming links which overlap with the link_a
	//			DBG("Checking links of possibly overlapped nodes...");
				for (list<Lattice::Node*>::iterator iNodeB=overlapped_nodes_list.begin(); iNodeB!=overlapped_nodes_list.end(); iNodeB++)
				{
					Lattice::Node* p_node_b = *iNodeB;
					//DBG("node_b: "<<*p_node_b);
					// these nodes have the same label as the node_a, so we only have to check time boundaries of each link
					vector<ID_t>* v_bck = &(pLat->bckLinks[p_node_b->id]);
					// for each incoming link
					for (vector<ID_t>::iterator iBckLinkId = v_bck->begin(); iBckLinkId != v_bck->end(); iBckLinkId++)
					{
						// do not combine the link with itself
						if (*iBckLinkId == p_link_a->id)
						{
							continue;
						}

						Lattice::Link *p_link_b = pLat->links[*iBckLinkId];
						Lattice::Node *p_bck_node_b = pLat->nodes[p_link_b->S];

	//					DBG("link_b: "<<*p_link_b);

						// if the incoming link overlaps with the link_a, add it's score to the link_a's score frame by frame
						if (is_overlapping(p_bck_node_b->t, p_node_b->t, link_start, link_end))
						{
							//DBG("overlapping!");

							// get an index of the first and the last overlapped frame
							int frame_start = 0;
							if (p_bck_node_b->t > link_start)
							{
								frame_start = (int)((p_bck_node_b->t - link_start) * FRAMES_PER_SECOND);
							}
							int frame_end = link_length_frames;
							if (p_node_b->t < link_end)
							{
								frame_end -= (int)((link_end - p_node_b->t) * FRAMES_PER_SECOND);
							}
							assert(frame_end - frame_start >= 0);

							// for each frame compute the logadd with the score of the overlapped link
							double last_score = frames_score[frame_start];
							double last_score_logadd = logAdd(frames_score[frame_start], p_link_b->confidence);
							for (int i = frame_start; i < frame_end; i++)
							{
								// if the score was the same in the previous frame, just copy it
								if (frames_score[i] == last_score)
								{
									frames_score[i] = last_score_logadd;
								}
								else
								{
									last_score = frames_score[i];
									frames_score[i] = logAdd(frames_score[i], p_link_b->confidence);
								}
							}
						}
					}
				}
				// compute link's score
	//			DBG("Computing link's score");
				double best_score = frames_score[0];
				for (int i=0; i<link_length_frames; i++)
				{
					if (best_score < frames_score[i])
					{
						best_score = frames_score[i];
					}
				}
				new_scores[p_link_a->id] = best_score;
				delete frames_score;
			}
		}
	}

	UpdateLatticeWithNewLinkScores(pLat, new_scores);
	DBG("total_overlapped_nodes_count: "<<total_overlapped_nodes_count);
}
// }}}


// ConfidenceMeasure_LogaddOverlapping {{{
void ConfidenceMeasure_LogaddOverlapping::ComputeLinksConfidence(Lattice *pLat)
{
	double new_scores[pLat->links.size()];

	if (pLat->nodes.empty())
		return;

	// if the lexicon was not defined and W_id is 0 for each node, create a temporary lexicon
	if (pLat->nodes[0]->W_id == 0)
	{
		DBG("CREATING TEMPORARY LEXICON");
		map<string,ID_t> lexicon;
		ID_t new_id = 1;
		int nodes_count = pLat->nodes.size();
		for (int inode = 0; inode < nodes_count; inode++)
		{
			Lattice::Node* p_node = pLat->nodes[inode];
			ID_t word_id = 0;
			map<string,ID_t>::iterator iword = lexicon.find(p_node->W);
			if (iword == lexicon.end())
			{
				word_id = new_id++;
				lexicon.insert(make_pair(p_node->W, word_id));
			}
			else
			{
				word_id = iword->second;
			}
			p_node->W_id = word_id;
		}
	}
/*
	// create a map (word_id -> links list)
	typedef vector<Lattice::Link*> links_list;
	typedef map<ID_t, links_list*> word2links_map;
	word2links_map w2l;
	{
		int nodes_count = pLat->nodes.size();
		for (int inode = 0; inode < nodes_count; inode++)
		{
			Lattice::Node* p_node = pLat->nodes[inode];
			links_list * pll = NULL;
			word2links_map::iterator iw2l = w2l.find(p_node->W_id);
			if (iw2l == w2l.end()) {
				pll = new links_list;
				w2l.insert(make_pair(p_node->W_id, pll));
			} else { 
				pll = iw2l->second; 
			}

			vector<ID_t>* v_bck = &(pLat->bckLinks[p_node->id]);
			for (vector<ID_t>::iterator iBckLinkId = v_bck->begin(); iBckLinkId != v_bck->end(); iBckLinkId++)
			{
				Lattice::Link *p_bck_link = pLat->links[*iBckLinkId];
				pll->push_back(p_bck_link);
			}
		}
	}
*/
	// create a map (word_id -> nodes list)
	typedef vector<Lattice::Node*> nodes_list;
	typedef map<ID_t, nodes_list*> word2nodes_map;
	word2nodes_map w2n;
	{
		int nodes_count = pLat->nodes.size();
		for (int inode = 0; inode < nodes_count; inode++)
		{
			Lattice::Node* p_node = pLat->nodes[inode];
			nodes_list * pnl = NULL;
			word2nodes_map::iterator iw2n = w2n.find(p_node->W_id);
			if (iw2n == w2n.end()) {
				pnl = new nodes_list;
				w2n.insert(make_pair(p_node->W_id, pnl));
			} else { 
				pnl = iw2n->second; 
			}

			pnl->push_back(p_node);

		}
	}


	// compute logadd score for overlapped links
	int links_count = pLat->links.size();
	for (int ilink = 0; ilink < links_count; ilink++)
	{
		Lattice::Link *p_link = pLat->links[ilink];
		new_scores[p_link->id] = p_link->confidence;
//		DBG("p_link: "<<*p_link);
		double link_start = pLat->nodes[p_link->S]->t;
		double link_end = pLat->nodes[p_link->E]->t;
		//string* p_link_label = &(pLat->nodes[p_link->E]->W);
		ID_t link_labelid = pLat->nodes[p_link->E]->W_id;

		
		nodes_list *pnl = w2n[link_labelid];
		int pnl_size = pnl->size();
		for (int inl = 0; inl < pnl_size; inl++)
		{
			Lattice::Node *p_node = (*pnl)[inl];

			if (is_overlapping(p_node->tStartBoundary, p_node->t, link_start, link_end))
			{
				vector<ID_t>* v_bck = &(pLat->bckLinks[p_node->id]);
				// for each incoming link
				for (vector<ID_t>::iterator iBckLinkId = v_bck->begin(); iBckLinkId != v_bck->end(); iBckLinkId++)
				{
					Lattice::Link *p_bck_link = pLat->links[*iBckLinkId];
					// do not combine the link with itself
					if (p_bck_link == p_link)
					{
						continue;
					}
					Lattice::Node *p_bck_node = pLat->nodes[p_bck_link->S];
					// if the incoming link overlaps with the p_link
					if (is_overlapping(p_bck_node->t, p_node->t, link_start, link_end))
					{
//						DBG("p_bck_link: "<<*p_bck_link);
						// add the score of the overlapping link to the p_link's score
//						DBG("logAdd("<<new_scores[p_link->id]<<", "<<p_bck_link->confidence<<") = "<<logAdd(new_scores[p_link->id], p_bck_link->confidence));
						new_scores[p_link->id] = logAdd(new_scores[p_link->id], p_bck_link->confidence);
					}
				}
			}
		}

/*		
		// for all nodes, check if any of it's incoming links overlaps with the link p_link
		links_list *pll = w2l[link_labelid];
		int pll_size = pll->size();
		for (int ill = 0; ill < pll_size; ill++)
		{
			Lattice::Link *p_overlap_link = (*pll)[ill];
			// do not combine the link with itself
			if (p_overlap_link == p_link)
			{
				continue;
			}
			Lattice::Node *p_bck_node = pLat->nodes[p_overlap_link->S];
			Lattice::Node *p_node = pLat->nodes[p_overlap_link->E];
			// if the incoming link overlaps with the p_link
			if (is_overlapping(p_bck_node->t, p_node->t, link_start, link_end))
			{
				//						DBG("p_overlap_link: "<<*p_overlap_link);
				// add the score of the overlapping link to the p_link's score
				//						DBG("logAdd("<<new_scores[p_link->id]<<", "<<p_overlap_link->confidence<<") = "<<logAdd(new_scores[p_link->id], p_overlap_link->confidence));
				new_scores[p_link->id] = logAdd(new_scores[p_link->id], p_overlap_link->confidence);
			}
		}
*/		
	}

/*
	// compute the c_max for each link
	int links_count = pLat->links.size();
	for (int ilink = 0; ilink < links_count; ilink++)
	{
		Lattice::Link *p_link = pLat->links[ilink];
		new_scores[p_link->id] = p_link->confidence;
		//string* p_link_label = &(pLat->nodes[p_link->E]->W);
		ID_t link_labelid = pLat->nodes[p_link->E]->W_id;
//		DBG("p_link: "<<*p_link);
		double link_start = pLat->nodes[p_link->S]->t;
		double link_end = pLat->nodes[p_link->E]->t;

		// for all nodes, check if any of it's incoming links overlaps with the link p_link
		int nodes_count = pLat->nodes.size();
		for (int inode = 0; inode < nodes_count; inode++)
		{
			Lattice::Node* p_node = pLat->nodes[inode];
			// if the node's label matches the iLink
			if (link_labelid != p_node->W_id)
			{
				//assert (*p_link_label != p_node->W);
				continue;
			}
			//assert (*p_link_label == p_node->W);

			if (is_overlapping(p_node->tStartBoundary, p_node->t, link_start, link_end))
			{
				vector<ID_t>* v_bck = &(pLat->bckLinks[p_node->id]);
				// for each incoming link
				for (vector<ID_t>::iterator iBckLinkId = v_bck->begin(); iBckLinkId != v_bck->end(); iBckLinkId++)
				{
					Lattice::Link *p_bck_link = pLat->links[*iBckLinkId];
					// do not combine the link with itself
					if (p_bck_link == p_link)
					{
						continue;
					}
					Lattice::Node *p_bck_node = pLat->nodes[p_bck_link->S];
					// if the incoming link overlaps with the p_link
					if (is_overlapping(p_bck_node->t, p_node->t, link_start, link_end))
					{
//						DBG("p_bck_link: "<<*p_bck_link);
						// add the score of the overlapping link to the p_link's score
//						DBG("logAdd("<<new_scores[p_link->id]<<", "<<p_bck_link->confidence<<") = "<<logAdd(new_scores[p_link->id], p_bck_link->confidence));
						new_scores[p_link->id] = logAdd(new_scores[p_link->id], p_bck_link->confidence);
					}
				}
			}
		}
	}
*/
	UpdateLatticeWithNewLinkScores(pLat, new_scores);

	for (word2nodes_map::iterator i=w2n.begin(); i!=w2n.end(); i++)
	{
		delete i->second;
	}
	/*
	for (word2links_map::iterator i=w2l.begin(); i!=w2l.end(); i++)
	{
		delete i->second;
	}
	*/
}
// }}}

