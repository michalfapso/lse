#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cassert>
#include "lattypes.h"

enum Rights {
	r,
	w,
	x,
	none
};

bool file_exists(const char *filename, Rights rights = none);
long file_size(const char *filename);
char* itoa(int input, char *str, int radix /*not implemented yet - using 10*/);
std::string upcase(std::string s);
std::string lowcase(std::string s);
std::string nonAsciiToOctal(std::string s);
size_t strlcpy(char *dst, const char *src, size_t siz);
char* chomp(char *str, int len = -1);
bool is_overlapping(double t1Start, double t1End, double t2Start, double t2End);
std::string CheckBackslash( std::string InputStr );
std::string DeleteBackslash( std::string InputStr );
void phn2ngram(const std::string src, std::vector<std::string>* dest, int n);
std::string trim_path(const std::string &fullpath, const std::string &dataDirPrefix, const std::string &ext = "");
std::string trim_file_extension(const std::string &fullpath);
std::string trim_outer_spaces(std::string str);
std::string string2hex(const std::string &str);
std::string trim_prefix(const std::string &instr, const std::string &Prefix="~");

template <template <typename U, class = std::allocator<U> > class Container>
void string_split(const std::string& str, const std::string& delimiters, Container<std::string>* tokens )
{
     // Skip delimiters at beginning.
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens->push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
	// if there is no delimiter in the given string, add the given string as the only token
	if (tokens->size() == 0)
	{
		tokens->push_back(str);
	}
}

template <template <typename U, class = std::allocator<U> > class Container>
void string_split(const std::string& str, const std::string& delimiters, Container<std::string>& tokens )
{
     // Skip delimiters at beginning.
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
	// if there is no delimiter in the given string, add the given string as the only token
	if (tokens.size() == 0)
	{
		tokens.push_back(str);
	}
}


template<class T>
void swap(T* elements, int i, int j)
{	T temp = elements[i];
	elements[i] = elements[j];
	elements[j] = temp;
}

template<class T>
void QuickSort
(	T * elements,
	int first, 
	int last
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++)
			if(elements[i] < t)
			{	
				++lastLow;
				swap(elements, lastLow, i);
			}
		swap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last);
	}
}


template<class T>
void QuickSort
(	T *& elements,
	int first, 
	int last,
	bool (*fCmpLess)(const T &a, const T &b)
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++) {
//			std::cout << i << " <=> " << first << std::endl;
			if((*fCmpLess)(elements[i], t))
			{	
				++lastLow;
				swap(elements, lastLow, i);
			}
		}
		swap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1, fCmpLess);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last, fCmpLess);
	}
}


template<typename T, typename Parent>
void QuickSort
(	T * elements,
	int first, 
	int last,
	bool (*fCmpLess)(Parent parent, const T &a, const T &b),
	Parent parent
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++) {
			//std::cout << i << " <=> " << first << std::endl;
			if((*fCmpLess)(parent, elements[i], t))
			{	
				++lastLow;
				swap(elements, lastLow, i);
			}
		}
		swap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1, fCmpLess, parent);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last, fCmpLess, parent);
	}
}


/*
template <class itemType, class indexType>
void QuickSort(itemType arr[], indexType beg, indexType end) {
    if (end > beg + 1) {
        indexType l = beg + 1, r = end;
		itemType piv = arr[beg];
        while (l < r) {
            if (arr[l] > piv) 
                std::swap(arr[l], arr[--r]);
            else 
                l++;
        }
		std::swap(arr[--l], arr[beg]);
        QuickSort(arr, beg, r);
        QuickSort(arr, l, end);
    }
}
*/


/*
template <class itemType, class indexType>
void QuickSort(itemType a[], indexType l, indexType r)
{
  static itemType m;
  static indexType j;
  indexType i;

  if(r > l) {
    m = a[r]; i = l-1; j = r;
    for(;;) {
      while(a[++i] < m);
      while(a[--j] > m);
      if(i >= j) break;
      std::swap(a[i], a[j]);
    }
    std::swap(a[i],a[r]);
    QuickSort(a,l,i-1);
    QuickSort(a,i+1,r);
  }
}
*/

#endif
