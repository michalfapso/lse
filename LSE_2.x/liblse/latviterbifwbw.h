#ifndef VITERBIFWBW_H
#define VITERBIFWBW_H


#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <string>
#include <queue>
#include <string>
#include <iostream>
#include "lat.h"
#include "lattypes.h"

//#include "strindexer.h"
//#include "latindexer.h"
//#include "indexer.h"
//#include "hypothesis.h"

#define DEFAULT_LOGADD_TRESHOLD 50
//#define DBGFWBW(str)    DBG_FORCE(str)
#define DBGFWBW(str)

namespace lse {

class LatViterbiFwBw {
public:


	class Node : public Lattice::Node {

	public:
		TLatViterbiFwBw fwbw;

		Node() : fwbw(INF) {}
	};




//	typedef double ValueType;
//	static const ValueType INF = 1.0e34;

	// Viterbi probas to and from nodes
	double *fwds, *bwds;
	// lengths of partial paths to and from nodes
	int *fwlens, *bwlens;
	unsigned int NN;  // no of lattice nodes.

	float amscale;
	float lmscale;
	float posteriorscale;
	float amscale_kwd; // higher value -> greater differences between hypotheses' confidences
	float lmscale_kwd; // higher value -> greater differences between hypotheses' confidences
	float wipen;

	bool baum_welsh;
	float logAdd_treshold;

	// constructor
	LatViterbiFwBw (Lattice *lat,float wipen, float p_amscale, float p_lmscale, float p_posteriorscale, float p_amscale_kwd, float p_lmscale_kwd, bool p_baum_welsh=true, float p_logAdd_treshold=DEFAULT_LOGADD_TRESHOLD);
	// destructor
	~LatViterbiFwBw ();
	// doing the job
	void computeFwViterbi();
	void computeBwViterbi();
	TLatViterbiLikelihood getLinkConfidence(lse::ID_t linkID, bool scaleOnKeyword = true);
	// print it out to file and load it.
	void writeViterbiFwBw(const std::string filename);
	int loadViterbiFwBw(const std::string filename);

	void computeLinksLikelihood(LatTime startTime = 0.0);
private:
	Lattice *lat;

};


//double logAdd(double a, double b, float treshold = DEFAULT_LOGADD_TRESHOLD);

}

#endif
