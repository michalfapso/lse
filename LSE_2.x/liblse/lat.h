#ifndef LAT_H
#define LAT_H

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "latmeetingindexer.h"
#include "languagemodel.h"
//#include "hypothesis.h"
#include "lexicon.h"
#include "ngram.h"
#include <zlib.h>
#include <cmath>
#include "matrix.h"

#define LAT_ERROR_IO	2


namespace lse {

class Lattice {
	
	friend class LatBinFile;
	friend class LatMlfFile;


public:

	//--------------------------------------------------------------------------------
	//          INDEXER
	//--------------------------------------------------------------------------------
	class Indexer {
			
		public:
			
			LatMeetingIndexer meetings;
			LatIndexer lattices;
			FwdIndex fwdIndex;
	};

	//--------------------------------------------------------------------------------
	//			NODE
	//--------------------------------------------------------------------------------
	class Node {
	public:
		ID_t id; ///< node ID
		LatTime tStart; ///< word's most probable start time (link with the best posterior)
		LatTime t; ///< time
		LatTime t_from; ///< time
		LatTime tStartBoundary; ///< start time (link with the soonest start time)
		ID_t W_id; ///< word id
		std::string W; ///< word
		int v;
		double p; ///< posteriors
		TLatViterbiLikelihood bestLikelihood; ///< best likelihood of all links going into this node
		ID_t bestLinkID; ///< ingoing link with highest likelihood
		ID_t bestFwdLinkID; ///< ingoing link with highest likelihood
		ID_t bestBckLinkID; ///< outgoing link with highest likelihood
		TLatViterbiLikelihood alpha;
		TLatViterbiLikelihood beta;

		Node() : 
			id				(0), 
			tStart			(INF), 
			t				(0.0), //to time in case of TimePrun == true
			t_from		(-1.0), //from time in case of TimePrun == true, useles for TimePrun == false
			tStartBoundary  (INF),
			W_id			(0), 
			W				(""), 
			v				(1), 
			p				(0), 
			bestLikelihood	(-INF), 
			bestLinkID		(-1), 
			bestFwdLinkID	(-1), 
			bestBckLinkID	(-1), 
			alpha			(-INF), 
			beta			(-INF) 
		{}

		void init();

		friend std::ostream& operator<<(std::ostream& os, const lse::Lattice::Node& node);
		friend bool operator<(const Node& l, const Node& r);

		Node(const Node &in_Node){
			id=in_Node.id;
			tStart=in_Node.tStart;
			t=in_Node.t;
			t_from=in_Node.t_from;
			tStartBoundary=in_Node.tStartBoundary;
			W_id=in_Node.W_id;
			W=in_Node.W;
			v=in_Node.v;
			p=in_Node.p;
			bestLikelihood=in_Node.bestLikelihood;
			bestLinkID=in_Node.bestLinkID;
			bestFwdLinkID=in_Node.bestFwdLinkID;
			bestBckLinkID=in_Node.bestBckLinkID;
			alpha=in_Node.alpha;
			beta=in_Node.beta;
		};

	};


	//--------------------------------------------------------------------------------
	// 			LINK
	//--------------------------------------------------------------------------------
	class Link {
	public:
		ID_t id;
		ID_t S; ///< link's start node
		ID_t E; ///< link's end node
		LatticeLink_a a; ///< accoustic proba
		LatticeLink_l l; ///< language proba
		TLatViterbiLikelihood likelihood; ///< link's likelihood = a*amscale + l*lmscale
		TLatViterbiLikelihood confidence; ///< link's confidence
		TLatViterbiLikelihood confidence_noScaleOnKeyword; ///< link's likelihood without scaling on keyword

		Link() : 
			id			(0), 
			S			(0), 
			E			(0), 
			a			(0.0), 
			l			(0.0), 
			likelihood					(-INF),
			confidence					(-INF), 
			confidence_noScaleOnKeyword	(-INF) 
		{}
		
		void init();

		friend std::ostream& operator<<(std::ostream& os, const Link& link);
		friend bool operator<(const Link& l, const Link& r); /// used for sorting FwdLinks and BckLinks

		Link(const Link &in_Link){
			id=in_Link.id;
			S=in_Link.S;
			E=in_Link.E;
			a=in_Link.a;
			l=in_Link.l;
			likelihood=in_Link.likelihood;
			confidence=in_Link.confidence;
			confidence_noScaleOnKeyword=in_Link.confidence_noScaleOnKeyword;
		};

	};


	//--------------------------------------------------------------------------------
	//			LATTICE
	//--------------------------------------------------------------------------------

	typedef std::vector<Lattice::Node*> Nodes; ///< NodeID -> Node
	typedef std::vector<Lattice::Link*> Links; ///< LinkID -> Link

	typedef std::map<ID_t, std::vector<ID_t> > FwdLinks; ///< NODE -> LIST OF OUTGOING LINKS
	typedef std::map<ID_t, std::vector<ID_t> > BckLinks; ///< NODE -> LIST OF INGOING LINKS

	typedef std::map< lse::ID_t, int > T_NumOfDirectAncestors;
	typedef std::map< lse::ID_t, lse::ID_t > T_IDMappingMap;


	enum RecordType {
		e_header,
		e_node,
		e_link,
		e_none
	};

	enum CompressionType {
		gzip,
		none
	};

	enum UpdateLinksType {
		forward,
		backward,
		both
	};

	Nodes nodes;
	Links links;

	FwdLinks fwdLinks;
	BckLinks bckLinks;

	Lexicon * lexicon;
	Indexer * indexer;
        
        ID_t firstNodeID; //this is Fisrt Node in lattice !!! (so no predecesor)
        ID_t lastNodeID;  //this is Last Node in lattice !!! (so no follower)
                  
	int N;
	int L;

	std::string record; // filename without extension
	std::string stream; // stream (LVCSR, KWS, ...)
	std::string filename;
	bool TimePrun;

	float amscale;
	float lmscale;
		
	inline Lattice(Lexicon &lexicon, Indexer &indexer, int N_in=0, int L_in=0, bool in_TimePrun=false) : N(N_in), L(L_in), amscale(1), lmscale(1) {
		this->lexicon = &lexicon;
		this->indexer = &indexer;
		stream = "";
		record = "";
                TimePrun=in_TimePrun;
	}

	Lattice(Lexicon *lexicon, Indexer *indexer=NULL, int N_in=0, int L_in=0, bool in_TimePrun=false) : N(N_in), L(L_in) {
		this->lexicon = lexicon;
		this->indexer = indexer;
		stream = "";
		record = "";
                TimePrun=in_TimePrun;
	}

	~Lattice();
	
	bool isDelimiter(char c);
	bool latticeNodeSortCriterion(const Lattice::Node& n1, const Lattice::Node& n2);
	void addNode(const Lattice::Node& node);
	void addLink(const Lattice::Link& link);
	void addNode(const Lattice::Node *node);
	void addLink(const Lattice::Link *link);
//	void deleteNode(ID_t nodeID, std::vector<ID_t> *delNodeIDList, std::vector<ID_t> *delLinkIDList);
//	void pruneLattice(TLatViterbiLikelihood posteriorTreshold);
//	void removeNodes(std::vector<ID_t> *delNodeIDList);
//	void removeLinks(std::vector<ID_t> *delLinkIDList);
//	void removeNullNodes();
	void printNodes();
	void printLinks();
	Lattice::Node* firstNode();
	Lattice::Node* lastNode();
	Lattice::Link* GetLink(ID_t fromNodeID, ID_t toNodeID);
	void addLattice(Lattice &conlat);
        void SetTimePrun(bool in_TimePrun){TimePrun=in_TimePrun;}
	
	void updateFwdBckLinks(UpdateLinksType type = both);
	void sortFwdBckLinks();
//	bool cmpFwdBckLinks(const ID_t& l, const ID_t& r);
	void sortFwdBckLinks_insertsort(std::vector<ID_t> &s);


	// File save / read
	int loadFromHTKFile(const std::string filename, LatTime latStartTime = 0, long p_lat_time_multiplier = 1, bool onlyAddWordsToLexicon = false, bool removeBackslash = false, bool removeQuotes = true);
	void saveToHTKFile(const std::string filename, bool outputPosteriors = false, bool outputLikelihoods = false, bool outputVariants = false);
  
//	void saveToBinaryFile(const std::string filename, CompressionType compression = none);
//	int loadFromBinaryFile(const std::string filename);
        void saveToDotFile(const std::string filename, bool printNodeNum=false, bool printLinkData=false, bool fillTimeAxis=false, bool showBestPath=false, float setTimeSamplingRate=0.01);
  void savePostToFeaFile(const std::string filename, std::map<std::string, int> phonemes);

	void addToLexicon();

	ID_t getNodeWithZeroAnc(int *in_NumOfDirectAncestors, LatTime *nodeTimes);
	void sortLattice();

	ID_t getEndNode_recursive(ID_t curNodeID);
	ID_t getEndNode(); 		// go through the lattice and return ID of node with no outgoing links (last node)
	ID_t getLastNodeID(); 	// last node in lattice (node with highest ID)
	ID_t getLastLinkID(); 	// last link in lattice (link with highest ID)
	void makeOnlyOneEndNode();
	void recursiveLatProcessing(ID_t curNodeID);
	void trimTimeInterval(LatTime tStart, LatTime tEnd);
	Lattice* cutTimeInterval(LatTime tStart, LatTime tEnd);
	void sortNodesByLinks(std::vector<Lattice::Node> &a);
//	LatTime getNodeStartTime(ID_t nodeID, double *likelihood);

	// PHONE LATTICES
//	void transform2ngram(int n, lse::Lattice* outLat);
//	void transform2ngram_recursive(ID_t nodeID, int maxDepth, int curDepth, std::string W);

	// searching functions
//	int getNodeContext(LatTime startTime, ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, Hypothesis &hyp);
	void GetBestPath(std::vector<ID_t>* pBestPath, ID_t* pStartNode = NULL, std::ostringstream *pathstr=NULL);
	void GetBestPathBackward(std::vector<ID_t>* pBestPath, ID_t* pEndNode = NULL, std::ostringstream *pathstr=NULL);
	int searchWord(const long position, const std::string filename, std::vector<Lattice::Node> &result); ///< WARNING: works only for gzipped binary lattices
	bool nodeExists(ID_t id);

	void setFilename(const std::string filename);
	void print();

	void timeNormalizeLattice();

//	void detectFirstLastNode();
//	void cleanLattice();
//	void deleteLink(ID_t LinkID);
//	void deleteNode(ID_t NodeID);
//	bool cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, std::vector<ID_t> &from, std::vector<ID_t> &to);
//	void insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label);
//	void insertToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label, std::string WordInLm, LanguageModel &LM);

private:
	void GenerateNgrams_recursive(
			Ngrams 					&ngrams, 
			int 					nodeIdCur, 
			int 					nCur, 
			int 					nMax, 
			Ngram 					*ngram, 
			TLatViterbiLikelihood 	alphaLast);
	void ComputeNodesStartTimes();
	bool AddNgramToNgramsList(Ngrams &ngrams, Ngram *ngram, int nMax);
public:		
	void OutputNgrams(std::string filename, int n);
	void GenerateNgramsIndex(int n, std::string meeting_rec);
	void GenerateIndex(std::string meeting_rec);
};



} // namespace lse

#endif
