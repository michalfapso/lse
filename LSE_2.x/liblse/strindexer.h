/*********************************************************************************
 *	TODO: 
 *
 *	Add checksum hash table for each word in map str2idMap to speed up searching
 *
 *	getNewWordID(): there is some useable STL function (hope so)
 *	                store last index in variable
 *
 *	getWordID(): for parallel processing - implementation of semaphor is needed
 *
 */

#ifndef STRINDEXER_H
#define STRINDEXER_H

#include <iostream>
#include <fstream>
#include <map>
#include "lexicon.h"
#include "lattypes.h"
#include "genericindexer.h"


//--------------------------------------------------------------------------------
// StrIndexer class

namespace lse {

class StrIndexer : public GenericIndexer<std::string> {

public:

	~StrIndexer() {}
	
	void writeStruct(std::ofstream &out, const ID_t id, const std::string val);
	int readStruct(std::ifstream &in, ID_t &id, std::string &val);
	
};

}

#endif
