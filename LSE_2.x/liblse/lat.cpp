#include <queue>
#include "lat.h"
#include <cmath>

using namespace std;
using namespace lse;

Lattice::~Lattice()
{
	for (Nodes::iterator i=nodes.begin(); i!=nodes.end(); i++)
		delete *i;
	for (Links::iterator i=links.begin(); i!=links.end(); i++)
		delete *i;
}

void Lattice::Node::init() {
	id=0;
	t=0;
	W="";
	W_id=0;
	v=0;
	p=0;
}


/**
  @brief Get start time of incoming link with best likelihood

  @param likelihood Best ingoing link's likelihood

  @return Node's start time
*/
/*
LatTime Lattice::getNodeStartTime(ID_t nodeID, double *likelihood) {
	
//	bool startTime_isset = false;
	double bestLikelihood = vit->getLinkLikelihood(*(lat->bckLinks[nodeID].begin())); 	// get first link's likelihood
	ID_t bestLinkID = *(lat->bckLinks[nodeID].begin());									

//	DBG("getNodeStartTime()");
	for (vector<ID_t>::iterator i=lat->bckLinks[nodeID].begin(); i!=lat->bckLinks[nodeID].end(); ++i) {

		double curLikelihood = vit->getLinkLikelihood(*i);
//		DBG("Link's likelihood: " << curLikelihood);
		if (curLikelihood > bestLikelihood) {
			bestLikelihood = curLikelihood;
			bestLinkID = *i;
		}
	}
	*likelihood = bestLikelihood;
//	DBG("bestLink: " << bestLinkID << "\t" << lat->links[bestLinkID]);

//	DBG("getNodeStartTime() end");
	return lat->nodes[lat->links[bestLinkID].S].t; 	// end time of best link's start node
}
*/

std::ostream& lse::operator<<(std::ostream& os, const Lattice::Node& node) {
	return os << "ID=" << node.id << "\ttStart="<<node.tStart<<"\ttEnd=" << node.t << "\tW[" << node.W_id << "]=" << node.W << "\tv=" << node.v << "\tp=" << node.p << "\tbestLikelihood=" << node.bestLikelihood << "\talpha=" << node.alpha << "\tbeta=" << node.beta;
}


//--------------------------------------------------------------------------------
bool lse::operator<(const Lattice::Node& l, const Lattice::Node& r) {
	// we need to keep the one with !NULL always as the greater.
	
	if (l.t == r.t) {
		if (l.W=="!NULL") {
			//cout<<"l.W==\"!NULL\""<<"id="<<l.id<<endl;
			return false;
		}
		if (r.W=="!NULL") {
			//cout<<"r.W==\"!NULL\""<<"id="<<l.id<<endl;
			return true;
		}
		// no need to solve the case if none of them
		// is NULL...
		return false;
	}
	else
		return l.t < r.t;
}


bool lse::operator<(const Lattice::Link& l, const Lattice::Link& r) {
	return l.confidence < r.confidence;
}

//--------------------------------------------------------------------------------
std::ostream& lse::operator<<(std::ostream& os, const Lattice::Link& link) {
	return os << "ID=" << link.id << " S=" << link.S << " E=" << link.E << " a=" << link.a << " l=" << link.l << " confidence="<<link.confidence;
}

//--------------------------------------------------------------------------------
void Lattice::Link::init() {
	id=0;
	S=0;
	E=0;
	a=0;
	l=0;
}


//--------------------------------------------------------------------------------
void Lattice::print() {
	cout << "NODES:" << endl;
	printNodes();
	cout << "LINKS:" << endl;
	printLinks();
}


//--------------------------------------------------------------------------------
bool Lattice::latticeNodeSortCriterion(const Lattice::Node& n1, const Lattice::Node& n2) {
	return n1.id < n2.id;
}


//--------------------------------------------------------------------------------
void Lattice::addToLexicon() {
	for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i)	{
		// add record to lexicon if there is no such word and get word's ID in lexicon
		(*i)->W_id = lexicon->getValID((*i)->W);
	}
}


//--------------------------------------------------------------------------------
// Add new node to nodes map

void Lattice::addNode(const Lattice::Node& node) 
{
	addNode(&node);
}

void Lattice::addNode(const Lattice::Node *node) 
{
	assert(nodes.size() == (unsigned int)node->id);
	Node* node_copy = new Node(*node);
	nodes.push_back(node_copy);
}


//--------------------------------------------------------------------------------
// Add new link to links map

void Lattice::addLink(const Lattice::Link& link) 
{
	addLink(&link);
}

void Lattice::addLink(const Lattice::Link *link) 
{
	assert(links.size() == (unsigned int)link->id);
	Link* link_copy = new Link(*link);
	links.push_back(link_copy);
}


/*
void Lattice::deleteNode(ID_t nodeID, vector<ID_t> *delNodeIDList, vector<ID_t> *delLinkIDList) {
	
//	DBG_FORCE("deleting node: "<<nodeID);
	assert(nodeID > 0 && nodeID < ((--nodes.end())->second).id);
	// delete all links connected to the node, which is going to be deleted
	
//	DBG_FORCE("");
//	Links::iterator delLink = links.end();
//	for (vector<ID_t>::iterator linkID=fwdLinks[nodeID].begin(); linkID!=fwdLinks[nodeID].end(); ++linkID) {
//		if ((delLink = links.find(*linkID)) != links.end()) {
//			links.erase(delLink);		// delete link
//			fwdLinks[nodeID].erase(linkID);	// update fwdLinks map
//		}
//	}
//		
//	DBG_FORCE("");
//	for (vector<ID_t>::iterator linkID=bckLinks[nodeID].begin(); linkID!=bckLinks[nodeID].end(); ++linkID) {
//		if ((delLink = links.find(*linkID)) != links.end()) {
//			links.erase(delLink);		// delete link
//			bckLinks[nodeID].erase(linkID);	// update bckLinks map
//		}
//	}
//	
	for (Links::iterator l=links.begin(); l!=links.end(); ++l) {
		if (l->second.S == nodeID || l->second.E == nodeID) {
			delLinkIDList->push_back(l->second.id);
		}
	}

	Nodes::iterator delNode;
	if ((delNode = nodes.find(nodeID)) != nodes.end()) {
//		nodes.erase(delNode);	// delete node
		delNodeIDList->push_back(nodeID);
	}
//	DBG_FORCE("");
//	FwdLinks::iterator delFwdLinks;
//	if ((delFwdLinks = fwdLinks.find(nodeID)) != fwdLinks.end()) {
//		fwdLinks.erase(delFwdLinks);	// delete node
//	}
//	DBG_FORCE("");
//	BckLinks::iterator delBckLinks;
//	if ((delBckLinks = bckLinks.find(nodeID)) != bckLinks.end()) {
//		bckLinks.erase(delBckLinks);	// delete node
//	}
	
}
*/

/*
void Lattice::removeNodes(vector<ID_t> *delNodeIDList) {
	Nodes::iterator delNode;
	for (vector<ID_t>::iterator n=delNodeIDList->begin(); n!=delNodeIDList->end(); ++n) {
		if ((delNode = nodes.find(*n)) != nodes.end())
			nodes.erase(delNode);
	}
}
*/

/*
void Lattice::removeLinks(vector<ID_t> *delLinkIDList) 
{
	Links::iterator delLink;
	for (vector<ID_t>::iterator l=delLinkIDList->begin(); l!=delLinkIDList->end(); ++l) {
		if ((delLink = links.find(*l)) != links.end())
			links.erase(delLink);
	}
}
*/

#if 0
void Lattice::pruneLattice(TLatViterbiLikelihood posteriorTreshold) {
	DBG_FORCE("pruneLattice("<<posteriorTreshold<<")");
	vector<ID_t> delNodeIDList;
	vector<ID_t> delLinkIDList;
	ID_t newLinkID = getLastLinkID() + 1;
	updateFwdBckLinks();
	int counter = 0;
	Nodes::iterator nPred=nodes.end();
	bool remove_nPred = false;
/*	DBG_FORCE("Fwd and Bck Links:");
	for(Nodes::iterator n=nodes.begin(); n!=nodes.end(); ++n, ++counter) {
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("nodeID:"<<n->second.id);
		for (vector<ID_t>::iterator l=fwdLinks[n->second.id].begin(); l!=fwdLinks[n->second.id].end(); ++l) 
			DBG_FORCE("Fwd:"<<links[*l]);
		
		for (vector<ID_t>::iterator l=bckLinks[n->second.id].begin(); l!=bckLinks[n->second.id].end(); ++l) 
			DBG_FORCE("Bck:"<<links[*l]);
	}
*/	
	for(Nodes::iterator n=++nodes.begin(); n!=--nodes.end(); ++n, ++counter) {
		if (remove_nPred) {
			DBG_FORCE("removing node:"<<nPred->second.id);
			nodes.erase(nPred);
			remove_nPred = false;
		}
			
//		DBG_FORCE("--------------------------------------------------");
//		DBG_FORCE("node:"<<n->second);
		if(n->second.p < posteriorTreshold/* && n->second.W_id != lexicon->nullWordID*/) {
			remove_nPred = true;
//			delNodeIDList.push_back(n->second.id);
			// add fwdLinks to delList
			for (vector<ID_t>::iterator l=fwdLinks[n->second.id].begin(); l!=fwdLinks[n->second.id].end(); ++l) {
				ID_t fwdNodeID = links[*l].E;
				// for all backward links for all current node's successors (forward nodes) remove their invalid bckLinks
				for (vector<ID_t>::iterator l_fwd=bckLinks[fwdNodeID].begin(); l_fwd!=bckLinks[fwdNodeID].end(); ++l_fwd) {
					if (*l_fwd == *l) {
						bckLinks[fwdNodeID].erase(l_fwd);
						break; // there is only one bckLink with the same ID
					}
				}
//				DBG_FORCE("FwdLink:"<<links[*l]);
				delLinkIDList.push_back(*l);
			}

			// add bckLinks to delList
			for (vector<ID_t>::iterator l=bckLinks[n->second.id].begin(); l!=bckLinks[n->second.id].end(); ++l) {
				ID_t bckNodeID = links[*l].S;
				// for all backward links for all current node's successors (forward nodes) remove their invalid bckLinks
				for (vector<ID_t>::iterator l_bck=fwdLinks[bckNodeID].begin(); l_bck!=fwdLinks[bckNodeID].end(); ++l_bck) {
					if (*l_bck == *l) {
						fwdLinks[bckNodeID].erase(l_bck);
						break; // there is only one bckLink with the same ID
					}
				}
//				DBG_FORCE("BckLink:"<<links[*l]);
				delLinkIDList.push_back(*l);
			}
			
			
//			for (Links::iterator l=links.begin(); l!=links.end(); ++l) {
//				if (l->second.S == n->second.id || l->second.E == n->second.id) {
//					delLinkIDList.push_back(l->second.id);
//				}
//			}

			// create new links instead of those which are going to be removed
			Lattice::Link tmpLink;
			for (vector<ID_t>::iterator l_bck=bckLinks[n->second.id].begin(); l_bck!=bckLinks[n->second.id].end(); ++l_bck) {
				for (vector<ID_t>::iterator l_fwd=fwdLinks[n->second.id].begin(); l_fwd!=fwdLinks[n->second.id].end(); ++l_fwd) {
					tmpLink.id = newLinkID++;
					tmpLink.S = links[*l_bck].S;
					tmpLink.E = links[*l_fwd].E;
					tmpLink.a = links[*l_bck].a + links[*l_fwd].a;
					tmpLink.l = links[*l_bck].l + links[*l_fwd].l;
					bool linkExists = false;
					for (vector<ID_t>::iterator l=fwdLinks[tmpLink.S].begin(); l!=fwdLinks[tmpLink.S].end(); ++l) {
						if (links[*l].E == tmpLink.E) {
							linkExists = true;
							break;
						}
					}
/*					for (Links::iterator l = links.begin(); l!=links.end(); ++l) {
						if (l->second.S == tmpLink.S && l->second.E == tmpLink.E) {
							linkExists = true;
							break;
						}
					}
*/					if (!linkExists) {
/*						if (links.find(tmpLink.id) != links.end()) {
							DBG_FORCE("rewriting link:"<<links[tmpLink.id]);
						}
*/						addLink(tmpLink);
						fwdLinks[tmpLink.S].push_back(tmpLink.id);
						bckLinks[tmpLink.E].push_back(tmpLink.id);
//						DBG_FORCE("addFwdLink to nodeID:"<<tmpLink.S);
//						DBG_FORCE("addBckLink to nodeID:"<<tmpLink.E);
//						DBG_FORCE("addLink:"<<tmpLink);
					} else {
//						DBG_FORCE("link from "<<tmpLink.S<<" to "<<tmpLink.E<<" exists");
					}
				}
			}
//			deleteNode(n->second.id, &delNodeIDList, &delLinkIDList);
			nPred = n;
		}
	}
	if (remove_nPred) {
		nodes.erase(nPred);
	}

	removeNodes(&delNodeIDList);
	removeLinks(&delLinkIDList);

}
#endif

/*	
void Lattice::removeNullNodes() {
	DBG_FORCE("Lattice::removeNullNodes() has some bug inside => method is disabled");
	vector<ID_t> delNodeIDList;
	vector<ID_t> delLinkIDList;
	updateFwdBckLinks();
	ID_t newLinkID = getLastLinkID() + 1;
	// for all nodes except first and the last one
	for (Nodes::iterator n=++nodes.begin(); n!=--nodes.end(); ++n) {
		if (n->second.W_id == lexicon->nullWordID) {
			
			

			// create new links instead of those which are going to be removed
			for (vector<ID_t>::iterator l_bck=bckLinks[n->second.id].begin(); l_bck!=bckLinks[n->second.id].end(); ++l_bck) {
				for (vector<ID_t>::iterator l_fwd=fwdLinks[n->second.id].begin(); l_fwd!=fwdLinks[n->second.id].end(); ++l_fwd) {
					Lattice::Link tmpLink;
					tmpLink.id = newLinkID++;
					tmpLink.S = links[*l_bck].S;
					tmpLink.E = links[*l_fwd].E;
					tmpLink.a = links[*l_bck].a + links[*l_fwd].a;
					tmpLink.l = links[*l_bck].l + links[*l_fwd].l;
					addLink(tmpLink);
					if (n->second.id == 4) {
						DBG_FORCE("addLink:"<<tmpLink);
					}
				}
			}
			
			// add node to delList
			delNodeIDList.push_back(n->second.id);
			// add fwdLinks to delList
			for (vector<ID_t>::iterator l=fwdLinks[n->second.id].begin(); l!=fwdLinks[n->second.id].end(); ++l) {
				if (n->second.id == 4) {
					DBG_FORCE("FwdLink:"<<links[*l]);
				}
				delLinkIDList.push_back(*l);
			}
			// add bckLinks to delList
			for (vector<ID_t>::iterator l=bckLinks[n->second.id].begin(); l!=bckLinks[n->second.id].end(); ++l) {
				if (n->second.id == 4) {
					DBG_FORCE("BckLink:"<<links[*l]);
				}
				delLinkIDList.push_back(*l);
			}
		}
	}
	removeNodes(&delNodeIDList);
	removeLinks(&delLinkIDList);
}
*/	

//--------------------------------------------------------------------------------
// Print all nodes

void Lattice::printNodes() {
//	cout << "Nodes count: " << nodes.end() << endl;

	for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		cout << (*i)->id << "> " << **i << endl;
	}
}


//--------------------------------------------------------------------------------
// Print all links

void Lattice::printLinks() {

	for(Links::iterator i=links.begin(); i!=links.end(); ++i) {
		cout << *i << endl;
	}		
}



/*--------------------------------------------------------------------------------
 * Convert words in lattice to wordIDs and save lattice to binary file
 * and insert new words to the lexicon 
 *
 */
/*
void Lattice::saveToBinaryFile(const string filename, CompressionType compression) {
	
	LatIndexer::Record latRec;
	
	if (compression == gzip) {
		
		// write COMPRESSED binary
		
		gzFile gz_out = gzopen(filename.c_str(), "wb");
		if (gz_out == NULL) {
//			return (2);		
		}

		// add index for this meeting to indexer->meetings
		latRec.meetingID = indexer->meetings.getValID(filename);

		// write header info (N,L)
		gzwrite(gz_out, reinterpret_cast<char *>(&N), sizeof(N));
		gzwrite(gz_out, reinterpret_cast<char *>(&L), sizeof(L));

		// write all nodes (with wordID instead of word)
		for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
			
			//add index for this node	
			latRec.wordID = lexicon->getValID((i->second).W);
			latRec.position = gztell(gz_out);
			latRec.p = (i->second).p;
			latRec.t = (i->second).t;
			indexer->lattices.addRecord(latRec);

			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).id), sizeof((i->second).id));
			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).t), sizeof((i->second).t));
			gzwrite(gz_out, reinterpret_cast<char *>(&latRec.wordID), sizeof(latRec.wordID));
			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).v), sizeof((i->second).v));
			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).p), sizeof((i->second).p));
		}

		// write all links
		for(Links::iterator i=links.begin(); i!=links.end(); ++i) {
				
			gzwrite(gz_out, reinterpret_cast<char *>(&i->second), sizeof(i->second));
		}		
		
		gzclose(gz_out);
		
	} else {
		
		// write UNCOMPRESSED binary
		
		ofstream out(filename.c_str(), ios::binary); // HTK Standard Lattice File
		
		if (!out.good()) {
//			return (2);
		}

		// write header info (N,L)
		out.write(reinterpret_cast<const char *>(&N), sizeof(N));
		out.write(reinterpret_cast<const char *>(&L), sizeof(L));

		// write all nodes (with wordID instead of word)
		for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
				
			ID_t wordID = lexicon->getValID((i->second).W);
			out.write(reinterpret_cast<const char *>(&(i->second).id), sizeof((i->second).id));
			out.write(reinterpret_cast<const char *>(&(i->second).t), sizeof((i->second).t));
			out.write(reinterpret_cast<const char *>(&wordID), sizeof(wordID));
			out.write(reinterpret_cast<const char *>(&(i->second).v), sizeof((i->second).v));
			out.write(reinterpret_cast<const char *>(&(i->second).p), sizeof((i->second).p));		
		}

		// write all links
		for(Links::iterator i=links.begin(); i!=links.end(); ++i) {
				
			out.write(reinterpret_cast<const char *>(&i->second), sizeof(i->second));
		}		

		out.close();
	
	}


}


//--------------------------------------------------------------------------------
// Read lattice from binary file

int Lattice::loadFromBinaryFile(const string filename) {
	
	setFilename(filename);
	// Clear nodes and links maps
	nodes.clear();
	links.clear();

	if (filename.substr(filename.length()-3,3) == ".gz") {
		
		// read COMPRESSED binary
		
		gzFile gz_in = gzopen(filename.c_str(), "rb");
		if (gz_in == NULL) {
			return (2);		
		}

		// read header info (N,L)
		gzread(gz_in, reinterpret_cast<char *>(&N), sizeof(N));
		gzread(gz_in, reinterpret_cast<char *>(&L), sizeof(L));
		
		Lattice::Node tmpNode;
		Lattice::Link tmpLink;
		
		// read all nodes and convert wordID into word using lexicon
		for(int i=0; i<N; i++) {
				
			ID_t wordID;
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.id)), sizeof(tmpNode.id));
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.t)), sizeof(tmpNode.t));
			gzread(gz_in, reinterpret_cast<char *>(&wordID), sizeof(wordID));
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.v)), sizeof(tmpNode.v));
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.p)), sizeof(tmpNode.p));
			tmpNode.W = lexicon->getVal(wordID);

			addNode(tmpNode);
		}

		
		// read all links
		for(int i=0; i<L; i++) {
				
			gzread(gz_in, reinterpret_cast<char *>(&tmpLink), sizeof(tmpLink));
			addLink(tmpLink);
		}

		gzclose(gz_in);
		
	} else {
				
		ifstream in(filename.c_str(), ios::binary);
		if (!in.good()) {
			return (2);
		}
		// read header info (N,L)
		in.read(reinterpret_cast<char *>(&N), sizeof(N));
		in.read(reinterpret_cast<char *>(&L), sizeof(L));
		
		Lattice::Node tmpNode;
		Lattice::Link tmpLink;
		
		// read all nodes and convert wordID into word using lexicon
		for(int i=0; i<N; i++) {
				
			ID_t wordID;
			in.read(reinterpret_cast<char *>(&(tmpNode.id)), sizeof(tmpNode.id));
			in.read(reinterpret_cast<char *>(&(tmpNode.t)), sizeof(tmpNode.t));
			in.read(reinterpret_cast<char *>(&wordID), sizeof(wordID));
			in.read(reinterpret_cast<char *>(&(tmpNode.v)), sizeof(tmpNode.v));
			in.read(reinterpret_cast<char *>(&(tmpNode.p)), sizeof(tmpNode.p));
			tmpNode.W = lexicon->getVal(wordID);

			addNode(tmpNode);
		}

		
		// read all links
		for(int i=0; i<L; i++) {
				
			in.read(reinterpret_cast<char *>(&tmpLink), sizeof(tmpLink));
			addLink(tmpLink);
		}

		in.close();
	}
	
	return 0;
}

*/

//--------------------------------------------------------------------------------
// Return node with lowest time (Lattice::Node::t)

Lattice::Node* Lattice::firstNode() {

	if (nodes.size() == 0) {
		return NULL;
	}
	return *(nodes.begin());
}


//--------------------------------------------------------------------------------
// Return node with highest time (Lattice::Node::t)

Lattice::Node* Lattice::lastNode() {
	
	if (nodes.size() == 0) {
		return NULL;
	}
	return *(nodes.rbegin());
}


//--------------------------------------------------------------------------------
// add lattice "conlat" to current lattice by !NULL node

void Lattice::addLattice(Lattice &conlat) {

	
//	cout << "----------------------------------------" << endl;
//	DBG_FORCE("Current Lattice: N="<<nodes.size()<<" L="<<links.size());
//	DBG_FORCE("ConLat: N="<<conlat.nodes.size()<<" L="<<conlat.links.size());
	
	// add lattice "conlat" after current lattice only if they are chronologicaly OK
//	if ((lastNode()).t <= (conlat.firstNode()).t) {
	
		// get last link index value
		ID_t lastLinkIndex;
		if (links.size() > 0) {
			lastLinkIndex = links.size()-1;
		} else {
			lastLinkIndex = -1; // new index will be 0
		}

		// get last node index value
		ID_t lastNodeIndex;
	    if (nodes.size() > 0) {
			lastNodeIndex = nodes.size()-1;
		} else {
			lastNodeIndex = -1; // new index will be 0
		}

		float initConnModelValue = 0.0;
		TLatViterbiLikelihood initConnLikelihoodValue = 0.0;
		
//		DBG_FORCE("LastLinkID:"<<lastLinkIndex<<" LastNodeID:"<<lastNodeIndex);
//		DBG_FORCE("getEndNode(): "<<getEndNode());
		// add links from global lattice's end node to the new connection node
		Lattice::Link tmpLink;
		tmpLink.id = ++lastLinkIndex;
		tmpLink.S = getEndNode();
		tmpLink.E = lastNodeIndex + 1; // connection node's ID (connection node will be just added)
		tmpLink.a = initConnModelValue;
		tmpLink.l = initConnModelValue;
		tmpLink.confidence = initConnLikelihoodValue;
		addLink(tmpLink);
		L++;
//		DBG_FORCE("Added link GlobalLat->ConnectionNode: "<<tmpLink);
//			cout << "Added link: " << tmpLink.S << " -> " << tmpLink.E << endl;
		
		// add connection node after the last node of current lattice
		Lattice::Node connectionNode;
		connectionNode.id = ++lastNodeIndex;
		connectionNode.t = (*conlat.nodes.begin())->t;
		connectionNode.W = "<sil>";
		connectionNode.v = 0;
		connectionNode.bestLikelihood = 0.0;
		addNode(connectionNode);
		N++;
//		DBG_FORCE("Added connection node: "<<connectionNode);
		
		ID_t conlatNewFirstNodeID = lastNodeIndex + 1; // conlat's first node ID after it will be added to current lattice
		
		// add links from current lattice's connection node to conlat's first nodes
//		float conLatMinTime = (conlat.firstNode()).t;
			
		tmpLink.id = ++lastLinkIndex;
		tmpLink.S = connectionNode.id;
		tmpLink.E = conlatNewFirstNodeID; // update IDs
		tmpLink.a = initConnModelValue;
		tmpLink.l = initConnModelValue;
		tmpLink.confidence = initConnLikelihoodValue;
		addLink(tmpLink);
		L++;
//		DBG_FORCE("Added link ConnectionNode->ConLat: "<<tmpLink);

//		DBG_FORCE("Adding nodes to global lattice");
		// add nodes from "conlat" to current lattice
		Lattice::Node tmpNode;
		for(Nodes::iterator i=conlat.nodes.begin(); i!=conlat.nodes.end(); ++i) {
			tmpNode = **i;
			tmpNode.id = ++lastNodeIndex;
//			cout << "Adding node with ID=" << tmpNode.id << endl;
			addNode(tmpNode);
			N++;
		}
		
		// add links from lattice "conlat" with updated IDs of start and end nodes	
		for(Links::iterator i=conlat.links.begin(); i!=conlat.links.end(); ++i) {
			tmpLink = **i;
			tmpLink.id = ++lastLinkIndex;
			tmpLink.S += conlatNewFirstNodeID;
			tmpLink.E += conlatNewFirstNodeID;			
			addLink(tmpLink);
			L++;
		}

		updateFwdBckLinks();
/*	
	} else {
		// add before the start node
		DBG_FORCE("(lastNode()).t > (conlat.firstNode()).t ... "<<(lastNode()).t<<" > "<<(conlat.firstNode()).t);
	}
*/
/*
	cout << "----------------------------------------" << endl;
	cout << "LATTICE after adding:" << endl;
	print();
*/
	
}


//--------------------------------------------------------------------------------
// function for use in loadFromHTKFile()

bool Lattice::isDelimiter(char c) {
	switch (c) {

		case '#':
		case ' ':
		case '\n':
		case '\t':
			return true;
			
		default:
			return false;
	}
}
/*
map<ID_t,int> nodeOrderMap;

void Lattice::recursiveLatProcessing_ForTimeClusterSorting(ID_t curNodeID, int *counter) {
	nodeOrderMap[curNodeID] = *counter; // set order value
	
	for (vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
		// call getEndNode_recursive for all nodes outgoing from current node
		if (nodeOrderMap.find(links[*i].E) != nodeOrderMap.end()) { // if there is any order value in current-node-forward-link's end-node
			*counter++;
			recursiveLatProcessing_ForTimeClusterSorting(links[*i].E, counter);			
		}
			
//			if (nodeOrderMap[links[*i].E] > 0) {

		recursiveLatProcessing_ForTimeClusterSorting(links[*i].E);
	}
}
*/
void Lattice::recursiveLatProcessing(ID_t curNodeID) {
	for (vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
		// call getEndNode_recursive for all nodes outgoing from current node
		recursiveLatProcessing(links[*i]->E);
	}
}

/*
void Lattice::sortNodesByLinks(vector<Lattice::Node> &a) {
	DBG_FORCE("sorting...");
	for (unsigned int i=1; i<a.size(); i++) {
		Lattice::Node curNode = a[i];
		unsigned int curNodeMoveFore_idx = i; 				// in front of which idx has to be moved
		for (vector<ID_t>::iterator l=fwdLinks[curNode.id].begin(); l!=fwdLinks[curNode.id].end(); ++l) {	// for each fwdLink
			unsigned int fwdNodeIdx = 0;
			for (vector<Lattice::Node>::iterator n=a.begin(); n!=a.end(); ++n) {							// for each node in lattice
				if (n->id == links[*l].E) {			// found fwdLink's end node in list
				   	if (fwdNodeIdx < i) {			// current node has to be moved prior to it's successors
						curNodeMoveFore_idx = fwdNodeIdx;
					}
					break; 							// stop looking for end node in list
				}
				fwdNodeIdx++;
			}
		}
		
		DBG_FORCE("  "<<i<<"\t->\t"<<curNodeMoveFore_idx);
		
		for (unsigned int j=i; j>curNodeMoveFore_idx; j--) {	// move all nodes between curNodeIdx and curNodeMoveFore_idx one step right
			a[j] = a[j-1];
		}
		a[curNodeMoveFore_idx] = curNode;			// move curNode on curNodeMoveFore_idx position
	}
	DBG_FORCE("sorting...done");
}
*/

//--------------------------------------------------------------------------------
// Load lattice from HTK Standard Lattice File

int Lattice::loadFromHTKFile(const string filename, LatTime latStartTime, long p_lat_time_multiplier, bool onlyAddWordsToLexicon, bool removeBackslash, bool removeQuotes) 
{
	bool gzip=false;
	gzFile gz_lat_file = NULL;
	istream* lat_file = NULL;
	ifstream lat_file_f;
	if (filename == "-") { 
		lat_file = &cin;
	} else if (filename.substr(filename.length()-3,3) == ".gz") {
		gzip = true;
		gz_lat_file = gzopen(filename.c_str(), "rb");
		if (gz_lat_file == NULL) {
			cerr << "Error: opening file " << filename << endl;
			return (2);		
		}
	} else {
		lat_file_f.open(filename.c_str()); // HTK Standard Lattice File

		if (!lat_file_f.good()) {
			cerr << "Error: opening file " << filename << endl;
			return (2);
		}
		lat_file = &lat_file_f;
	}
	setFilename(filename);

	// first clear nodes and links
	N=0;
	L=0;
	nodes.clear();
	links.clear();
//	DBG("nodes.size:" << nodes.size());
	
	DBG("loadFromHTKFile");
	
	vector<Lattice::Node> sorterNodes;
	vector<Lattice::Node> nullNodes;	// for SRI lattices - null nodes are first => move them on the end of nodes list
	
	Lattice::Node tmpNode;
	Lattice::Link tmpLink;

	string s, line, pomstr;
	char c; // current character read from HTK file
	int state = 0; // finite automat state
	bool epsilon = false; // epsilon transition
	RecordType recType = e_header; // type of current block (header, nodes, links)
	bool eow = false; // End Of Word (if delimiter is found)
	bool gotLink = false; // are values in tmpLink correctly read from input?
	bool gotNode = false; // are values in tmpNode correctly read from input?
	
	int counter = 0;
	
	while ( !(gzip?gzeof(gz_lat_file):(lat_file->eof())) ) {
		
		counter++;
//		if (counter % 50000)
//			cout << '.' << flush;
		
		// If epsilon transition is set, nothing is read from input file
		if (epsilon) {
			epsilon = false;
		} else {
			if (gzip) {
				c = gzgetc(gz_lat_file);
			} else {
				lat_file->get(c);
			}

			// If current character is delimiter
			if (isDelimiter(c)) {
				eow = true; // End Of Word
			
				if (c=='#')
					state=100; //comment
					
			} else {
				s += c;
			}				
		}
		
		
		if (eow) {
		
//			cout << "state:" << state << " eps:" << epsilon << endl;

			switch (state) {
				
				case 0:
					// N=, I= ---epsilon---> 2
					if (s.substr(0,2) == "N=") {
						N = atoi((s.substr(2)).c_str());
						nodes.reserve(N);
						state = 1;
					} else if (s.substr(0,6) == "NODES=") {
						N = atoi((s.substr(6)).c_str());
						nodes.reserve(N);
						state = 1;
					} else if (s.substr(0,2) == "I=") {
						recType = e_node;
						epsilon = true;
						state = 2;
					} else if (s.substr(0,6) == "LINKS=") {
						recType = e_node;
						epsilon = true;
						state = 2;
					}
					break;

				case 1:
					// L=
					if (s.substr(0,2) == "L=") {
						L = atoi((s.substr(2)).c_str());
						links.reserve(L);
						// L is increased when new link is added in method addLink()
						state = 2;
					} 
					else if (s.substr(0,6) == "LINKS=") {
						L = atoi((s.substr(6)).c_str());
						links.reserve(L);
						state = 2;				
					}
					break;

				case 2:
					// I=, J= ---epsilon---> 4
					if (s.substr(0,2) == "I=") {
						recType = e_node;
						tmpNode.id = atoi((s.substr(2)).c_str());
						gotNode = true;
						state = 3;
					} else if (s.substr(0,2) == "J=") {
						recType = e_link;
						epsilon = true;
						state = 4;
					}
					break;

				case 3:
					// t=, W=, v=
					if (s.substr(0,2) == "t=") {
						pomstr=s.substr(2);
						string::size_type pos = 0;
						if((pos = pomstr.find(",", 0)) != string::npos){
							tmpNode.t_from = (atof((pomstr.substr(0,pos)).c_str()) / p_lat_time_multiplier) + latStartTime;
							//std::cerr << p_lat_time_multiplier << " " << (pomstr.substr(0,pos-1))<< " " << tmpNode.t_from;
							tmpNode.t = (atof((pomstr.substr(pos+1)).c_str()) / p_lat_time_multiplier) + latStartTime;
							//std::cerr << " " << p_lat_time_multiplier << " " << (pomstr.substr(pos+1)).c_str() << " " << tmpNode.t  <<  std::endl;
							TimePrun = true;                                        
						}else{
							tmpNode.t = (atof(pomstr.c_str()) / p_lat_time_multiplier) + latStartTime;
							//std::cerr << p_lat_time_multiplier << " " << pomstr << " " << tmpNode.t << std::endl;
							tmpNode.t_from = -1; //tmpNode.t;
						}//if timeprun lattice        
					} 
					else if (s.substr(0,2) == "W=") {
						if (removeBackslash)
						{
							string::size_type pos = 0;
							while ((pos = s.find("\\", pos)) != string::npos)
							{
								s.erase(pos,1);
							}
						}
						if (removeQuotes)
						{
							string::size_type pos = 0;
							while ((pos = s.find("\"", pos)) != string::npos)
							{
								s.erase(pos,1);
							}
						}
						tmpNode.W = s.substr(2);
						tmpNode.W_id = lexicon->getValID(tmpNode.W);
					}
					else if (s.substr(0,2) == "v=") {
						tmpNode.v = atoi((s.substr(2)).c_str());
					} 
					else if ( (s.substr(0,2) == "P=") || (s.substr(0,2) == "p=") ) {
						tmpNode.p = atof((s.substr(2)).c_str());
					}
				break;


				case 4:
					// J=
					if (s.substr(0,2) == "J=") {
						recType = e_link;
						tmpLink.id = atoi((s.substr(2)).c_str());
						gotLink = true;
						state = 5;
					}
					break;

				case 5:
					// S=, E=, a=, l=
					if (s.substr(0,2) == "S=") {
						tmpLink.S = atoi((s.substr(2)).c_str());
					}
					else if (s.substr(0,2) == "E=") {
						tmpLink.E = atoi((s.substr(2)).c_str());
					}
					else if (s.substr(0,2) == "a=") {
						tmpLink.a = atof((s.substr(2)).c_str());
					}
					else if (s.substr(0,2) == "l=") {
						tmpLink.l = atof((s.substr(2)).c_str());
					}
					break;

					
				case 100:
					// comment
					break;
					
			} // switch (state)

			// end of record => save the record and go to the start state of current record type
			if (c=='\n' && !epsilon) {
				if (recType == e_header) {
					state = 0;
				} 
				else if (recType == e_node) {
					if (gotNode) {
/*						if (isSRIlattice && tmpNode.W == "!NULL" && !gotLastNullNode) {
							if (!onlyAddWordsToLexicon) 
//								nullNodes.push_back(tmpNode);
							if (tmpNode.p == 1 && tmpNode.id > 0) 
								gotLastNullNode = true;
						} else 
*/						
						{
							if (!onlyAddWordsToLexicon)
//								sorterNodes.push_back(tmpNode);
								addNode(tmpNode);
							else
								this->lexicon->getValID(tmpNode.W);
						}
					}
					tmpNode.init();
					gotNode = false;
//					addNode(tmpNode);
					state = 2;
//					recType = e_link;
				} 
				else if (recType == e_link) {
					if (gotLink) {
						addLink(tmpLink);
					}
					tmpLink.init();
					gotLink = false;
					state = 4;
//					recType = e_none;
				}
				s.clear();
			}

			// new word begins
			if (!epsilon) {
				s.clear();
				eow = false;
			}
			
		} // if (eow)

	} // while (!lat_file.eof())

	if (gzip) {
		gzclose(gz_lat_file);
	} else {
		// since lat_file is an instance of istream, it can not be closed
//		lat_file.close();		
	}


//	DBG_FORCE("nodes.size():"<<nodes.size());

	if (onlyAddWordsToLexicon)
		return 0; // if we are only parsing the lattice file for putting words to the lexicon, then it is done.



//	DBG("nodes.size():"<<sorterNodes.size()<<" links.size:"<<links.size()<<" fwdLinks.size:"<<fwdLinks.size()<<" bckLinks.size:"<<bckLinks.size());
//	for(vector<Lattice::Node>::iterator i=sorterNodes.begin(); i!=sorterNodes.end(); ++i) {
//		cout << *i << endl;
//	}

	// sort nodes by time
	
	// add null nodes to the end of sorterNodes vector
/*	if (isSRIlattice) {
		for (vector<Lattice::Node>::iterator i=nullNodes.begin(); i!=nullNodes.end(); ++i) {
			sorterNodes.push_back(*i);
		}
	}
*/
//	stable_sort(sorterNodes.begin(), sorterNodes.end()); 	// sort nodes by time

//	updateFwdBckLinks(); 							// needed by sortNodesByLinks() !!!
//	sortNodesByLinks(sorterNodes);

//	DBG_FORCE("original lat's endNodeID: "<< getEndNode());

/*	
	map<ID_t, ID_t> translator; 		// old id -> new id
	ID_t idCounter = 0; 				// new id generator
	nodes.clear();
	for (vector<Lattice::Node>::iterator i=sorterNodes.begin(); i!=sorterNodes.end(); ++i) {

		translator[i->id] = idCounter;
		i->id = idCounter;
		addNode(*i);
		idCounter++;
	}
*/


	// save translator file
/*
	string translatorFile = filename + ".trans";
	ofstream out(translatorFile.c_str());
	for(map<ID_t, ID_t>::iterator i=translator.begin(); i!=translator.end(); ++i) {
		
		out << i->first << "->" << i->second << endl;
	}
	out.close();
*/
	
	
	// update start and end node id in each link
/*	for (Links::iterator i=links.begin(); i!=links.end(); ++i) {
		(i->second).S = translator[(i->second).S];
		(i->second).E = translator[(i->second).E];
	}

	updateFwdBckLinks();
*/	
	/*
	DBG_FORCE("================================================================================");
	map<ID_t,void*> nodeCluster;
	map<ID_t,int> nodeClusterWeights;
	LatTime t = 0;
	int cnt = 0;
	DBG_FORCE("nodes.size():"<<nodes.size()<<"\tlinks.size():"<<links.size());
	for (Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		if ((i->second).t == t) {
			cnt++;
			nodeCluster[((i->second).id)] = NULL;
			nodeClusterWeights[((i->second).id)] = 1;
		} else {
			
			DBG_FORCE("t:"<<t<<"\t"<<cnt<<"\t"<<nodeCluster.size());
			nodeCluster.clear();
			t = (i->second).t;
			cnt=0;
			
			if (nodeCluster.size() > 0) { 
				for (int j=0; j<nodeCluster.size(); j++) {	// do it as much times as nodes count in same time
					for (map<ID_t,void*>::iterator n=nodeCluster.begin(); n!=nodeCluster.end(); ++n) {	// for each node in same time
						for (Links::iterator l=links.begin(); l!=links.end(); ++l) {	// for each link in lattice
							if ((l->second).E == n->first && nodes[(l->second).S].t == nodes[n->first].t) {	// select only links with start nodes in same time and end node n
								nodeClusterWeights[n->first] += nodeClusterWeights[(l->second).S];	// add weight of incoming link's node (Standa knows:o)
							}
						}
					}
				}
				//sort by links
				for (map<ID_t,int>::iterator w=nodeClusterWeights.begin(); w!=nodeClusterWeights.end(); ++w) {
					cout << w->second << " ";
				}
				cout << endl << flush;
				
			}
			nodeCluster.clear();
			nodeClusterWeights.clear();
			t = (i->second).t;
			nodeCluster[((i->second).id)] = NULL;
			nodeClusterWeights[((i->second).id)] = 1;
			
		}
	}
	DBG_FORCE("================================================================================");
	*/
	
/*	
	// swap last node in map with lattice's end node
	ID_t endNodeID = getEndNode(); 	// end node in lattice
	ID_t lastNodeID = getLastNodeID(); 	// last node in nodes map
	Node endNode = nodes[endNodeID]; 	// save end node
	Node lastNode = nodes[lastNodeID];	// save last node
	
	nodes.erase(endNodeID);				// remove end and last node from map
	nodes.erase(lastNodeID);
	
	ID_t idTmp = endNode.id;			// swap id of end and last node
	endNode.id = lastNode.id;
	lastNode.id = idTmp;
	
	addNode(lastNode);					// add nodes with swapped IDs back into the map
	addNode(endNode);

	for (Links::iterator i=links.begin(); i!=links.end(); ++i) {	// apply the changes also on links
		if ((i->second).S == endNodeID)  (i->second).S = -2;		// prevent overwriting of new value by next row
		if ((i->second).S == lastNodeID) (i->second).S = endNodeID;
		if ((i->second).E == endNodeID)  (i->second).E = -2;		// prevent overwriting of new value by next row
		if ((i->second).E == lastNodeID) (i->second).E = endNodeID;
	}

	for (Links::iterator i=links.begin(); i!=links.end(); ++i) {	// apply the changes also on links
		if ((i->second).S == -2)  (i->second).S = lastNodeID;		// now it is safe to set these values
		if ((i->second).E == -2)  (i->second).E = lastNodeID;		// ...
	}
*/
//	sortLattice(); // updateFwdBckLinks() is called before and after sorting within sortLattice()

	DBG("nodes.size():"<<nodes.size());
//	DBG_FORCE("fwdLinks[1].size():"<<fwdLinks[1].size());
	
//	DBG_FORCE("recursiveLatProcessing ...");
//	recursiveLatProcessing((nodes.begin())->first);
//	DBG_FORCE("recursiveLatProcessing done")

	DBG("nodes.size():"<<nodes.size()<<" links.size:"<<links.size()<<" fwdLinks.size:"<<fwdLinks.size()<<" bckLinks.size:"<<bckLinks.size());

	updateFwdBckLinks();
	return 0;
}



//--------------------------------------------------------------------------------
// Save lattice to HTK Standard Lattice File
// lattice is gzipped if output file extension is .gz

void Lattice::saveToHTKFile(const string filename, bool outputPosteriors, bool outputLikelihoods, bool outputVariants) {

/*std::cout << "***************************************************************************" << std::endl ;
   for (Links::iterator iLink = links.begin(); iLink != links.end(); ++iLink)
  {
    std::cout << (iLink->second).id << " " << (iLink->second).S << " " << (iLink->second).E << " " << (iLink->second).a << std::endl;
  }//delete nodes

  */
	
	std::string label;
	if (filename.substr(filename.length()-3,3) == ".gz") {
		
		// write COMPRESSED lattice
		
		gzFile gz_out = gzopen(filename.c_str(), "wb");

		// write header info (N,L)
		gzprintf(gz_out, "N=%d\tL=%d\n", N, L);
		
		// write all nodes (with wordID instead of word)
		for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
//			std::cerr << TimePrun << " " << (i->second).t_from <<  std::endl;
			Node* p_node = *i;
			label = p_node->W;
			if( label.substr(0,1) == "'" ){
			  label="\""+label+"\"";
			}
			
			
			if( label != "!NULL"){
				if( TimePrun && (p_node->t_from != -1.0) ){
					if (outputPosteriors) {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\tp=%f\n", p_node->id, p_node->t_from, p_node->t, label.c_str(), p_node->p);
					} else if (outputLikelihoods) {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\tlik=%f\n", p_node->id, p_node->t_from, p_node->t, label.c_str(), p_node->bestLikelihood);
					} else if (outputVariants && (p_node->v != 0)) {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\tv=%d\n", p_node->id, p_node->t_from, p_node->t, label.c_str(), p_node->v);
					} else {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\n", p_node->id, p_node->t_from, p_node->t, label.c_str());
					}
				}else{
					if (outputPosteriors) {
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tp=%f\n", p_node->id, p_node->t, label.c_str(), p_node->p);
					} else if (outputLikelihoods) {
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tlik=%f\n", p_node->id, p_node->t, label.c_str(), p_node->bestLikelihood);
					} else if (outputVariants && (p_node->v != 0)) {	
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tv=%d\n", p_node->id, p_node->t, label.c_str(), p_node->v);
					} else {
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\n", p_node->id, p_node->t, label.c_str());
					}
				}        
			}else{
				if( TimePrun && (p_node->t_from != -1.0) ){
					if (outputPosteriors) {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\tp=%f\n", p_node->id, p_node->t_from, p_node->t, label.c_str(), p_node->p);
					} else if (outputLikelihoods) {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\tlik=%f\n", p_node->id, p_node->t_from, p_node->t, label.c_str(), p_node->bestLikelihood);
					} else if (outputVariants && (p_node->v != 0)) {	
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\tv=%d\n", p_node->id, p_node->t_from, p_node->t, label.c_str(), p_node->v);
					} else {
						gzprintf(gz_out, "I=%d\tt=%f,%f\tW=%s\n", p_node->id, p_node->t_from, p_node->t, label.c_str());
					}
				}else{
					if (outputPosteriors) {
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tp=%f\n", p_node->id, p_node->t, label.c_str(), p_node->p);
					} else if (outputLikelihoods) {
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tlik=%f\n", p_node->id, p_node->t, label.c_str(), p_node->bestLikelihood);
					} else if (outputVariants && (p_node->v != 0)) {	
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tv=%d\n", p_node->id, p_node->t, label.c_str(), p_node->v);
					} else {
						gzprintf(gz_out, "I=%d\tt=%f\tW=%s\n",p_node->id, p_node->t, label.c_str());
					}
				}        
			}
		}	
		// write all links
		for(Links::iterator i=links.begin(); i!=links.end(); ++i) {
			
			gzprintf(gz_out, "J=%d\tS=%d\tE=%d\ta=%.10f\tl=%.10f\n", (*i)->id, (*i)->S, (*i)->E, (*i)->a, (*i)->l);
		}		

		gzclose(gz_out);
		
	} else {

		// write UNCOMPRESSED lattice

		ofstream out(filename.c_str());
		// write header info (N,L)
		out << "N=" << N << "\tL=" << L << endl;
		
		// write all nodes (with wordID instead of word)
		for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
			Node* p_node = *i;
			label = p_node->W;
			if( label.substr(0,1) == "'" ){
			  label="\""+label+"\"";
			}

			if( label != "!NULL"){
			  out << "I="	<< p_node->id << "\tt=";
				if(TimePrun && (p_node->t_from != -1.0) ){
					out << p_node->t_from << ",";
				}
				out << p_node->t << "\tW=" << label; // << "\tv=" << p_node->v
			}else{
				out << "I="   << p_node->id << "\tt=";
				if(TimePrun && (p_node->t_from != -1.0)){
					out << p_node->t_from << ",";
				}
				out << p_node->t << "\tW=" << label;
			}  
//			if (outputPosteriors) {
//				out << "\tp=" << p_node->p;
//			}
			if (outputLikelihoods) {
				out << "\tlik=" << p_node->bestLikelihood;
			}
			if (outputVariants && (p_node->v != 0) ) {
				out << "\tv=" << p_node->v;
			}
//			out << "\ttStartBoundary=" << p_node->tStartBoundary;

			out << endl;
		}

		// write all links
		for(Links::iterator i=links.begin(); i!=links.end(); ++i) {
			
			out << "J="	<< (*i)->id << "\tS=" << (*i)->S << "\tE=" << (*i)->E << setprecision(10) << "\ta=" << (*i)->a << "\tl=" << (*i)->l;
			if (outputPosteriors) {
				out << "\tP=" << exp((*i)->confidence);
			}
//			out << "\ttStart=" << nodes[(*i)->S]->t;
		   	out << endl;
		}		

		out.close();
	}
}



/*--------------------------------------------------------------------------------
 * Save to dot file for use with graphviz - http://www.research.att.com/sw/tools/graphviz/
 *
 */
void Lattice::saveToDotFile(const string filename, bool printNodeNum, bool printLinkData, bool fillTimeAxis, bool showBestPath, float setTimeSamplingRate) {

	typedef map<float, int> float2intMap; // set of all nodes times
	typedef map<int, std::string> int2stringMap; // set of all nodes times
	std::vector<ID_t>* pBestPath;

	pBestPath = new std::vector<ID_t>;

	ofstream out(filename.c_str());
	// write header
	out << "digraph LATTICE {" << endl 
		<< "rankdir = LR" << endl
		//		<< "size = \"100000,100000\"" << endl
		<< "label = \"\"" << endl
		<< "center = 1" << endl
		<< "nodesep = \"0.250000\"" << endl
		<< "ranksep = \"0.400000\"" << endl
		<< "rotate = 90" << endl;


	// create and fill set of all nodes times

	float2intMap time_axis;
	int2stringMap nodelist;

	if( showBestPath ){
		GetBestPath(pBestPath);
	}//if  

	//std::cout << "BLEEEE" << std::flush;

	//	int time_axis_index = 0;
	for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		std::ostringstream pom;
		pom << (*i)->id;
		nodelist[(int)floor((*i)->t/setTimeSamplingRate)]=nodelist[(int)floor((*i)->t/setTimeSamplingRate)]+" "+pom.str();
	}


	// nodes clustering (by time)
	for(int2stringMap::iterator i=nodelist.begin(); i!=nodelist.end(); ++i) {
		out << "{rank=same; T" << (i->first) << (i->second) << ";}" << endl;;
	}


	// write all nodes (with wordID instead of word)
	for(Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		//		int color = (int)(exp((i->second).bestLikelihood) * 255);
		Node* p_node = *i;
		if (p_node->W == "!CONN") {
			out << p_node->id << " [label = \"\", shape=diamond, style=bold, fontsize=14, width=0.5, height=0.5, color=gold]" << endl;
		} else if (p_node->W == "!NULL") {
			if( printNodeNum ){
				out << p_node->id << " [label = \"" << p_node->id << ":!NULL\", shape=ellipse, style=bold, fontsize=14, width=0.5, height=0.5, color=dodgerblue]" << endl;
			}else{
				out << p_node->id << " [label = \"\", shape=ellipse, style=bold, fontsize=14, width=0.5, height=0.5, color=dodgerblue]" << endl;
			}      
		} else {
			if( printNodeNum ){
				out << p_node->id << " [label = \"" << p_node->id << ":" << p_node->W << "\", shape = ellipse, style = bold, fontsize = 14]" << endl;
			}else{
				out << p_node->id << " [label = \"" << p_node->W << "\", shape = ellipse, style = bold, fontsize = 14]" << endl;
			}
		}

		std::string color="color=black";
		bool IsBest=false;
		for(Links::iterator j=links.begin(); j!=links.end(); ++j) {
			Link* p_link = *j;
			if (p_link->S == p_node->id) {
				if( showBestPath ){
					for(unsigned int k=0; k<pBestPath->size(); k++){
						if( (*pBestPath)[k] == p_link->id ){
							IsBest=true;
						}  //if
					}//for

					if( IsBest ){
						color="color=red";
					}else{
						color="color=black";
					}

					IsBest = false;
				}    

				if( printLinkData ){
					out << "  " << p_link->S << " -> " << p_link->E << " [label = \"A:" << p_link->a << ",L:" << p_link->l << "\", " << color << "]" << endl;// << " [label = \"\", fontsize = 14];" << endl;
				}else{
					out << "  " << p_link->S << " -> " << p_link->E << " [label = \"\", " << color << "]" << endl;// << " [label = \"\", fontsize = 14];" << endl;
				}//if prind data
			}
		}		
	}

	out << "subgraph time_axe {" << endl;
	int last=0;
	int max=0, min=(int)9e99;
	for(int2stringMap::iterator i=nodelist.begin(); i!=nodelist.end(); ++i) {
		if( max < (i->first) ){
			max = (i->first);
		}
		if( min > (i->first) ){
			min = (i->first);
		}

		out << " T" << (i->first) << " [label=\"" << (i->first*setTimeSamplingRate) << "\" shape=house, fontsize=10, color=lightblue]" << endl;
		if( fillTimeAxis ){
			if( last < ((i->first)-1) ){
				for(int pom=(last+1); pom<(i->first); pom++){
					out << " T" << pom << " [label=\"" << (pom*setTimeSamplingRate) << "\" shape=house, fontsize=10, color=lightblue]" << endl;
				}
			}
			last=(i->first);
		}  
	}

	bool first=true;
	if( fillTimeAxis ){
		for(int pom=min; pom<=max; pom++) {
			if (first) {
				first = false;
			} else {
				out << " -> ";
			}
			out << " T" << (pom);
		}//for
		out << endl;
	}else{
		for(int2stringMap::iterator i=nodelist.begin(); i!=nodelist.end(); ++i){
			if (first) {
				first = false;
			} else {
				out << " -> ";
			}
			out << " T" << (i->first);
		}//for
		out << endl;
	}

	out << "}" << endl;

	out << "}" << endl;

	out.close();

	delete pBestPath;

}


/*--------------------------------------------------------------------------------
 * Save to dot file for use with graphviz - http://www.research.att.com/sw/tools/graphviz/
 *
 */
void Lattice::savePostToFeaFile(const std::string filename, std::map<std::string, int> phonemes) {
  
  int phnind;
  Mat<float> *Matrix;  
  if (lastNode() == NULL) 
	  return;

  Matrix = new Mat<float>( (int)ceil(lastNode()->t*100), phonemes.size(), 0 );
    
  for(Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i){
    if( (*i)->W != "!NULL" ){
      phnind=phonemes[(*i)->W];
      //std::cout << (*i)->W << " " << (*i)->t_from << " " << (*i)->t << std::endl;
      for( int t=(int)ceil((*i)->t_from*100) ; t<(int)ceil((*i)->t*100) ; t++ ){          
        Matrix->set( t, phnind, ((*i)->p+Matrix->get(t, phnind)) );
      }//for  
    }  
  }//for
        
  Matrix->saveHTK( filename.c_str() );
}


/*--------------------------------------------------------------------------------
 *	searchResult()
 */
/*
int Lattice::searchWord(const long position, const string filename, vector<Lattice::Node> &result) {
	
	// read COMPRESSED binary
	
	gzFile gz_in = gzopen(filename.c_str(), "rb");
	if (gz_in == NULL) {
		return (2);
	}

	Lattice::Node tmpNode;
	ID_t wordID;
	
	gzseek(gz_in, position, SEEK_SET);
	
	// read all nodes and convert wordID into word using lexicon
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.id)), sizeof(tmpNode.id));
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.t)), sizeof(tmpNode.t));
	gzread(gz_in, reinterpret_cast<char *>(&wordID), sizeof(wordID));
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.v)), sizeof(tmpNode.v));
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.p)), sizeof(tmpNode.p));

	tmpNode.W = lexicon->getVal(wordID);
	result.push_back(tmpNode);

	gzclose(gz_in);

	return 0;
}
*/


/*--------------------------------------------------------------------------------
 *	updateFwdBckLinks()
 */
void Lattice::updateFwdBckLinks(UpdateLinksType type) {
	
	if (type == forward) {
		fwdLinks.clear();
		for (Links::iterator i = links.begin(); i != links.end(); ++i) {
			fwdLinks[(*i)->S].push_back((*i)->id);
		}

	} else if (type == backward) {
		bckLinks.clear();
		for (Links::iterator i = links.begin(); i != links.end(); ++i) {
			bckLinks[(*i)->E].push_back((*i)->id);
		}
	
	} else if (type == both) {
		fwdLinks.clear();
		bckLinks.clear();
		for (Links::iterator i = links.begin(); i != links.end(); ++i) {
			fwdLinks[(*i)->S].push_back((*i)->id);
			bckLinks[(*i)->E].push_back((*i)->id);
//			DBG("<UPDATE LINKS> id:" << i->first << "\t" << (i->second).S << "\t->\t" << (i->second).E);
		}
	}
	sortFwdBckLinks();
}


void Lattice::sortFwdBckLinks_insertsort(vector<ID_t> &s) {
//void insertionsort(int s[], int length){
  unsigned int i, j;
  int elem;
  
  for(i=1; i < s.size(); i++){
    elem=s[i]; 
    j=i;
    for(; j > 0 && links[elem]->confidence > links[s[j-1]]->confidence; j--)
      s[j]=s[j-1];
    s[j]=elem;
  }
}



void Lattice::sortFwdBckLinks() {
	for (FwdLinks::iterator i = fwdLinks.begin(); i != fwdLinks.end(); ++i) {
		sortFwdBckLinks_insertsort(i->second);
/*		DBG_FORCE("nodeID: "<<i->first);
		for (vector<ID_t>::iterator j = i->second.begin(); j != i->second.end(); ++j) {
			DBG_FORCE("  fwdLink: "<<links[*j]);
		}*/
	}
	
	for (BckLinks::iterator i = bckLinks.begin(); i != bckLinks.end(); ++i) {
		sortFwdBckLinks_insertsort(i->second);
	}
}



bool Lattice::nodeExists(ID_t id) 
{
	return nodes.size() > (unsigned int)id;
}


void Lattice::GetBestPath(vector<ID_t>* pBestPath, ID_t* pStartNode, std::ostringstream *pathstr)
{
	// ***** FORWARD LINKS *****
	//

	ID_t cur_node_id;
	if (pStartNode == NULL)
	{
		cur_node_id = 0;
	}
	else
	{
		if (*pStartNode == -1)
		{
			*pStartNode = 0;
		}
		cur_node_id = *pStartNode;
	}
	
	//<< " A: " << nodes[cur_node_id].a << " L: " << nodes[cur_node_id].l

	
	if( pathstr != NULL ){
		(*pathstr) << " | Forward BestPath";
	}	

	while (1) 
	{

		if( pathstr != NULL ){
			(*pathstr) <<  " | N: " << cur_node_id <<  " W: " << nodes[cur_node_id]->W << " t: " << nodes[cur_node_id]->t;
		}
		//std::cerr << cur_node_id << " " << fwdLinks[cur_node_id].size() << " - ";
		// if there isn't node with id==cur_node_id then stop forward links processing
//		DBG_FORCE("node #"<<cur_node_id<<" fwdLinks.size():"<<fwdLinks[cur_node_id].size());
		if (fwdLinks[cur_node_id].size() == 0) 
		{
			//			DBG_FORCE("node #"<<cur_node_id<<" has no fwdLinks");
			break;
		}

		ID_t best_link_id = *(fwdLinks[cur_node_id].begin());

		for(vector<ID_t>::iterator i=fwdLinks[cur_node_id].begin(); i!=fwdLinks[cur_node_id].end(); ++i) 
		{
//			DBG_FORCE("link #" << *i << "   " << links[*i].S << "--("<<links[*i].confidence_noScaleOnKeyword<<")-->" << links[*i].E);
			if (links[best_link_id]->confidence_noScaleOnKeyword < links[*i]->confidence_noScaleOnKeyword) 
			{
				best_link_id = *i;
			}
		}
		pBestPath->push_back(best_link_id);

//		DBG_FORCE("Best link: id=" << best_link_id << "   " << links[best_link_id].S << "--("<<links[best_link_id].confidence_noScaleOnKeyword<<")-->" << links[best_link_id].E);

		if( pathstr != NULL ){
			(*pathstr) << " A: " << links[best_link_id]->a << " L: " << links[best_link_id]->l;
		}
		cur_node_id = links[best_link_id]->E;
	}
	//if( pathstr != NULL ){
	//  (*pathstr) << std::endl;
	//}	
}


void Lattice::GetBestPathBackward(vector<ID_t>* pBestPath, ID_t* pEndNode, std::ostringstream *pathstr)
{
	// ***** FORWARD LINKS *****
	//

	ID_t cur_node_id;
	if (pEndNode == NULL)
	{
		cur_node_id = nodes.size()-1;
	}
	else
	{
		if (*pEndNode == -1)
		{
			*pEndNode = nodes.size()-1;
		}
		cur_node_id = *pEndNode;
	}	
	
	if( pathstr != NULL ){
	  (*pathstr) << " | Backward BestPath";
	}	

	while (1) 
	{
                //std::cerr << cur_node_id << " " << fwdLinks[cur_node_id].size() << " - ";
		// if there isn't node with id==cur_node_id then stop forward links processing
//		DBG_FORCE("node #"<<cur_node_id<<" fwdLinks.size():"<<fwdLinks[cur_node_id].size());

    if( pathstr != NULL ){
	    (*pathstr) << " | N: " << cur_node_id << " W: " << nodes[cur_node_id]->W << " t: " << nodes[cur_node_id]->t;
	  }

		if (bckLinks[cur_node_id].size() == 0) 
		{
//			DBG_FORCE("node #"<<cur_node_id<<" has no fwdLinks");
			break;
		}

		ID_t best_link_id = *(bckLinks[cur_node_id].begin());

		for(vector<ID_t>::iterator i=bckLinks[cur_node_id].begin(); i!=bckLinks[cur_node_id].end(); ++i) 
		{
//			DBG_FORCE("link #" << *i << "   " << links[*i].S << "--("<<links[*i].confidence_noScaleOnKeyword<<")-->" << links[*i].E);
			if (links[best_link_id]->confidence_noScaleOnKeyword < links[*i]->confidence_noScaleOnKeyword) 
			{
				best_link_id = *i;
			}
		}
		pBestPath->push_back(best_link_id);

//		DBG_FORCE("Best link: id=" << best_link_id << "   " << links[best_link_id].S << "--("<<links[best_link_id].confidence_noScaleOnKeyword<<")-->" << links[best_link_id].E);
    if( pathstr != NULL ){
	    (*pathstr) << " A: " << links[best_link_id]->a << " L: " << links[best_link_id]->l;
	  }

		cur_node_id = links[best_link_id]->S;
	}
//	if( pathstr != NULL ){
//	  (*pathstr) << std::endl;
//	}

}



/*
int Lattice::getNodeContext(LatTime startTime, ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, Hypothesis &hyp) 
{	
	if (startTime < 0) 
		startTime=0;

	hyp.stream = this->stream;
	hyp.record = this->record;
	
	string context = ""; //nodes[centerNodeID].W;
	Hypothesis::Word centerNodeHypWord;
	centerNodeHypWord.str = lexicon->getVal(nodes[centerNodeID].W_id);
	centerNodeHypWord.start = -1; // we don't know node's start time
	centerNodeHypWord.end = nodes[centerNodeID].t;
	centerNodeHypWord.conf = nodes[centerNodeID].p;

	Hypothesis::Word curHypWord = centerNodeHypWord;
	
	
	DBG("***** getNodesContext *****");
	DBG("centerNodeID:" << centerNodeID << " fwdNodesCount:" << fwdNodesCount << " bckNodesCount:" << bckNodesCount);

	
	ID_t curNodeID = centerNodeID;
	int depth = 0;
	ID_t bestNodeID = centerNodeID;
	string bestWord;


	// ***** BACKWARD LINKS *****
	//
	DBG("******* BACKWARD LINKS *******");
	curNodeID = centerNodeID;
	depth = 0;

	
	while (depth <= bckNodesCount) {
		
		FwdLinks::iterator i = fwdLinks.find(curNodeID);
		if (i == fwdLinks.end()) {
			break;
		} 	


		// don't add "!NULL" to context string
		if (nodes[curNodeID].W_id != lexicon->nullWordID) {
			
			// set new values for hypothesis word
			if (curNodeID != centerNodeID)
				hyp.setFrontWordStartTime(nodes[curNodeID].t);
			
			curHypWord.str = lexicon->getVal(nodes[curNodeID].W_id);
			curHypWord.end = nodes[curNodeID].t;
			curHypWord.conf = nodes[curNodeID].p;
			hyp.push_front_word(curHypWord);
			DBG("Hypothesis.push(): " << curHypWord.str);
		
			context = ' ' + context;
			context = nodes[curNodeID].W + context;
		}

		
		DBG("DEPTH:" << depth);
		DBG("LINKS COUNT: " << fwdLinks[curNodeID].size() );

		bestNodeID = -1;
		for(vector<ID_t>::iterator i=bckLinks[curNodeID].begin(); i!=bckLinks[curNodeID].end(); ++i) {
			DBG("links["<< *i <<"]: " << links[*i] << "\t startNode:" << nodes[links[*i].S]);

			if (nodes[bestNodeID].p < nodes[links[*i].S].p) {
				bestNodeID = links[*i].S;
			}
		}

		DBG("Best node: id=" << bestNodeID << " word=" << nodes[bestNodeID]);

		curNodeID = bestNodeID;
		// if center node hasn't been added yet, add it into hyp's keyword list
		if (centerNodeHypWord.start == -1) {
			centerNodeHypWord.start = nodes[curNodeID].t;
			hyp.addKeyword(centerNodeHypWord);
		}

		
		// increase depth
		if(nodes[curNodeID].W_id != lexicon->nullWordID) 
			depth++;

		DBG("Context:" << context);
//		cin.get();
	}
	hyp.setFrontWordStartTime(startTime); // set start time of first node

	
	DBG("Backward links done...depth=" << depth << " maxDepth=" << fwdNodesCount);


	DBG("FORWARD LINKS");
	// ***** FORWARD LINKS *****
	//
	LatTime predNodeEndTime = centerNodeHypWord.end;
	curNodeID = centerNodeID;
	depth = 0;

	while (depth <= fwdNodesCount) {

		// if there isn't node with id==curNodeID then stop forward links processing
		FwdLinks::iterator iFind = fwdLinks.find(curNodeID);
		if (iFind == fwdLinks.end()) {
			break;
		}
		
		DBG("DEPTH:" << depth);
		DBG("LINKS COUNT: " << fwdLinks[curNodeID].size() );
			
		bestNodeID = -1;
		for(vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
			DBG("links["<< *i <<"]: " << links[*i] << "\t endNode: " << nodes[links[*i].E]);
			if (nodes[bestNodeID].p < nodes[links[*i].E].p) {
				bestNodeID = links[*i].E;
			}
		}

		DBG("Best node: id=" << bestNodeID << " word=" << nodes[bestNodeID].W);

		curNodeID = bestNodeID;
		// don't add "!NULL" to context string
		if (nodes[bestNodeID].W_id != lexicon->nullWordID) {
			// add best node's word to hypothesis
			curHypWord.str = lexicon->getVal(nodes[bestNodeID].W_id);
			curHypWord.start = predNodeEndTime;
			curHypWord.end = nodes[bestNodeID].t;
			curHypWord.conf = nodes[bestNodeID].p;
			hyp.push_back_word(curHypWord);
			
			predNodeEndTime = curHypWord.end;

			context += ' ';
			context += nodes[bestNodeID].W;
			// increase depth
			depth++;
		}
		DBG("Context:" << context);
//		cin.get();
	}



	return 0;
}
*/


ID_t Lattice::getNodeWithZeroAnc(int *in_NumOfDirectAncestors, LatTime *nodeTimes)
{
	ID_t pomNodeID = ID_UNDEF;
	double MinTime = 9e99;
	for(Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i)
	{
		if( (in_NumOfDirectAncestors[(*i)->id] == 0) && (MinTime > nodeTimes[(*i)->id]) )
		{
			MinTime = nodeTimes[(*i)->id];
			pomNodeID = (*i)->id;
		};//if
	};//for
	return pomNodeID;
};//ID_t GetNodeWithZeroAnc()



void Lattice::sortLattice()
{
	DBG_FORCE("Lattice::sortLattice()");
	//	T_NumOfDirectAncestors NumOfDirectAncestors;
	int NumOfDirectAncestors[getLastNodeID()+1];
	//	T_IDMappingMap IDMappingMap;
	int IDMappingMap[getLastNodeID()+1];

	LatTime nodeTimes[getLastNodeID()+1];

	int NewNodeIDCounter=0;
	ID_t CurrentNode;

	updateFwdBckLinks(both);

	BckLinks::iterator BckLinks_it;
	//std::cerr<<"getLastNodeID()+1="<<getLastNodeID()+1<<std::endl;
	//nodeTimes[110] = 4;
	for(Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i)
	{
		//std::cerr << i->first << "=" << i->second.t;
		nodeTimes[(*i)->id] = (*i)->t;
		BckLinks_it = bckLinks.find((*i)->id);
		if( BckLinks_it != bckLinks.end() )
		{
			NumOfDirectAncestors[(*i)->id] = BckLinks_it->second.size();
		}
		else
		{
			NumOfDirectAncestors[(*i)->id] = 0;
		};//if
		//    std::cerr << (*i)->id << "->" << NumOfDirectAncestors[(*i)->id] << " " ;
	};//for
	//  std::cerr << std::endl;
	CurrentNode = getNodeWithZeroAnc(NumOfDirectAncestors, nodeTimes);
	while( CurrentNode != ID_UNDEF)
	{
		//    std::cerr << "Detected node with Zero " << CurrentNode << std::endl;
		FwdLinks::iterator NodeFwdLinks_it = fwdLinks.find(CurrentNode);
		if( NodeFwdLinks_it != fwdLinks.end() )
		{
			for(std::vector<ID_t>::iterator FwdLinks_it = NodeFwdLinks_it->second.begin(); FwdLinks_it != NodeFwdLinks_it->second.end(); ++FwdLinks_it)
			{
				NumOfDirectAncestors[links[*FwdLinks_it]->E]--;
			};//for
		};//if
		IDMappingMap[CurrentNode] = NewNodeIDCounter++;
		NumOfDirectAncestors[CurrentNode] = -1;
		CurrentNode = getNodeWithZeroAnc(NumOfDirectAncestors, nodeTimes);
	};//

	for(Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i)
	{
		if( (NumOfDirectAncestors[(*i)->id] > 0) )
		{
			std::cerr << "ERROR VOLE!! Some of nodes were not proccessed NumOfDirectAncestors[" << (*i)->id << "] = " << NumOfDirectAncestors[(*i)->id] << std::endl << std::flush;
		};//if
	};//for

	/*
	   for(Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i)
	   {
	   DBG( "Old ID " << (*i)->id << " new ID " << IDMappingMap[(*i)->id] );
	   };//for
	   */

//	typedef map<int, Lattice::Node*> NodesMap;
	Nodes newnodes;
	newnodes.resize(nodes.size());
	//  newnodes = new Nodes;
	for(Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i)
	{
		//    (*newnodes)[IDMappingMap[i->second.id]] = i->second;
		//    (*newnodes)[IDMappingMap[i->second.id]].id = IDMappingMap[i->second.id];
		newnodes[IDMappingMap[(*i)->id]] = *i;
		newnodes[IDMappingMap[(*i)->id]]->id = IDMappingMap[(*i)->id];
	};//
	nodes.clear();
/*	for (NodesMap::iterator i = newnodes.begin(); i!=newnodes.end(); i++)
	{
		nodes[i]
	}
*/	nodes = newnodes;

	for(Links::iterator i = links.begin(); i != links.end(); ++i)
	{
		(*i)->S = IDMappingMap[(*i)->S];
		(*i)->E = IDMappingMap[(*i)->E];
	};//

	updateFwdBckLinks(both);

	DBG_FORCE("Lattice::sortLattice()...done");
};//void Lattice::sortLattice(){



void Lattice::setFilename(const string filename) {

	DBG("Lattice::setFilename(): "<< filename);
	this->filename = filename;

	// Set lattice's record name (= filename without extension)
	this->record = filename;
	string::size_type pos = (this->record).rfind ('/');
	if (pos != string::npos) {
		string::size_type pos2 = (this->record).find ('.', pos);
		(this->record).erase(pos2);
	}
	
	this->stream = "LVCSR";
	
	DBG("record:" << record << " stream:" << stream);
}


ID_t Lattice::getEndNode_recursive(ID_t curNodeID) {
//	static ID_t endNodeID = -1;

	// if current node has no successor
	if (fwdLinks[curNodeID].size() == 0) {
//		DBG_FORCE("  FOUND END NODE");
/*		if (endNodeID == -1) {
			endNodeID = curNodeID;
			DBG_FORCE("endNodeID:" << endNodeID);
		} else if (curNodeID != endNodeID)
			DBG_FORCE("endNodeID:" << endNodeID << " curNodeID:" << curNodeID);
		Link endLink;
		endLink.id = getLastLinkID() + 1;
		endLink.S = curNodeID;
		endLink.E = getLastNodeID();
		endLink.a = 1;
		endLink.l = 1;
		DBG_FORCE("Adding link: "<<endLink);
		addLink(endLink);*/
		return curNodeID;
	} 
	
	ID_t res;
	for (vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
		// call getEndNode_recursive for all nodes outgoing from current node
		if ((res = getEndNode_recursive(links[*i]->E)) != -1)
			return res;
	}
	return -1;	
}

ID_t Lattice::getEndNode() {
	Nodes::iterator i = nodes.begin();
	return getEndNode_recursive((*i)->id);
}

#if 0
void Lattice::makeOnlyOneEndNode() {
	Nodes::iterator i = nodes.begin();
	DBG_FORCE("EndNodeID: "<<getEndNode_recursive((*i)->id));
	
/*	
	DBG_FORCE("nodes:"<<nodes.size()<<" links:"<<links.size());
	Node endNode;
	// get last node's ID
	ID_t lastNodeID = getLastNodeID();
	// insert end node into nodes map
	endNode.id = nodes[lastNodeID].id + 1;
	endNode.t = nodes[lastNodeID].t+1;
	endNode.W_id = lexicon->nullWordID;
	endNode.W = "!NULL";
	endNode.v = 1;
	endNode.p = 1;
	DBG_FORCE("Adding node: "<<endNode);
	addNode(endNode);
	
	// insert links going from all end nodes into new end node
	Nodes::iterator i = nodes.begin();
	getEndNode_and_addLink_recursive(i->first);
	updateFwdBckLinks();
	DBG_FORCE("nodes:"<<nodes.size()<<" links:"<<links.size());
*/	
}
#endif

ID_t Lattice::getLastNodeID() {
	return nodes.size()-1;
}

ID_t Lattice::getLastLinkID() {
	return links.size()-1;
}
/*
Lattice::computeAllLinkLikelihood() {

}
*/


void Lattice::trimTimeInterval(LatTime tStart, LatTime tEnd) 
{
	// delete links
	for (Links::iterator l=links.begin(); l!=links.end(); ++l) {
		ID_t S = (*l)->S;
		if (nodes[S]->t < tStart || nodes[S]->t > tEnd) {
			links.erase(l);
			continue;
		} 
		ID_t E = (*l)->E;
		if (nodes[E]->t < tStart || nodes[E]->t > tEnd) {
			links.erase(l);
			continue;
		}
	}
	this->L = links.size();

	// delete nodes
	for (Nodes::iterator n=nodes.begin(); n!=nodes.end(); ++n) {
		LatTime t = (*n)->t;
		if (t < tStart || t > tEnd) {
			nodes.erase(n);
		}
	}
	this->N = nodes.size();
}


Lattice::Link* Lattice::GetLink(ID_t fromNodeID, ID_t toNodeID)
{
	for (vector<ID_t>::iterator iLinkID = fwdLinks[fromNodeID].begin(); iLinkID != fwdLinks[fromNodeID].end(); ++iLinkID)
	{
		Lattice::Link* pLink = links[*iLinkID];
		if (pLink->E == toNodeID)
		{
			return pLink;
		}
	}
	return NULL;
}

/*
void Lattice::detectFirstLastNode()
{
  bool foundFirst=false,foundLast=false;
  firstNodeID=-1;
  lastNodeID=-1;

  for( Nodes::iterator iNode=nodes.begin(); iNode!=nodes.end(); ++iNode )
  {
    if(fwdLinks[iNode->first].size() == 0){
      if( foundLast ){ std::cerr << "ERROR: Lattice has more then one end point" << std::endl << std::flush; }
      foundLast=true;
      lastNodeID=iNode->first;
      std::cout << "Lattice has end point " << lastNodeID << std::endl << std::flush;

      if( (iNode->first) != lastNode()->id){
        for( std::vector<ID_t>::iterator iLinkID=bckLinks[lastNode()->id].begin(); iLinkID!=bckLinks[lastNode()->id].end(); ++iLinkID ){
          links[*iLinkID].E=iNode->first;
        }//for
        for( std::vector<ID_t>::iterator iLinkID=fwdLinks[lastNode()->id].begin(); iLinkID!=fwdLinks[lastNode()->id].end(); ++iLinkID ){
          links[*iLinkID].S=iNode->first;
        }//for
        for( std::vector<ID_t>::iterator iLinkID=bckLinks[iNode->first].begin(); iLinkID!=bckLinks[iNode->first].end(); ++iLinkID ){
          links[*iLinkID].E=lastNode()->id;
        }//for
        

        Node PomNode;
        PomNode=nodes[lastNode()->id];
        nodes[lastNode()->id]=iNode->second;
        nodes[iNode->first]=PomNode;
        nodes[lastNode()->id].id=lastNode()->id;
        nodes[iNode->first].id=iNode->first;

      }//if
      
    }
    if(bckLinks[iNode->first].size() == 0){
      if( foundFirst ){ std::cerr << "ERROR: Lattice has more then one start point" << std::endl << std::flush; }
      foundFirst=true;
      firstNodeID=iNode->first;
      std::cout << "Lattice has start point " << firstNodeID << std::endl << std::flush;

      if( (iNode->first) != 0){
        for( std::vector<ID_t>::iterator iLinkID=fwdLinks[iNode->first].begin(); iLinkID!=fwdLinks[iNode->first].end(); ++iLinkID ){
          links[*iLinkID].S=0;
        }//for
        for( std::vector<ID_t>::iterator iLinkID=fwdLinks[0].begin(); iLinkID!=fwdLinks[0].end(); ++iLinkID ){
          links[*iLinkID].S=iNode->first;
        }//for      
        for( std::vector<ID_t>::iterator iLinkID=bckLinks[0].begin(); iLinkID!=bckLinks[0].end(); ++iLinkID ){
          links[*iLinkID].E=iNode->first;
        }//for      
        Node PomNode;
        PomNode=nodes[0];
        nodes[0]=iNode->second;
        nodes[iNode->first]=PomNode;
        nodes[0].id=0;
        nodes[iNode->first].id=iNode->first;
      }//if
      
    }
  }
  if( ! foundLast ){ std::cerr << "ERROR: Lattice has no end point" << std::endl << std::flush; }
  if( ! foundFirst ){ std::cerr << "ERROR: Lattice has no start point" << std::endl << std::flush; }

  updateFwdBckLinks();
  
}
*/


/*
void Lattice::cleanLattice()
{
  std::cout << "Lattice cleaning" << std::endl;
  bool WasChange=true;
  while( WasChange ){
    WasChange=false; 
    for (Nodes::iterator i = nodes.begin(); i != nodes.end(); ++i)
    {
      if( ((*i)->id != firstNodeID) && ((*i)->id != lastNodeID) &&
          ((fwdLinks[(*i)->id].size() == 0 ) || (bckLinks[(*i)->id].size() == 0 )) )
      {
        deleteNode((*i)->id);
        WasChange=true;
        break;
      }
    }
  }  
}//void Lattice::cleanLattice()
*/

/*
void Lattice::deleteLink(ID_t LinkID)
{
  std::vector<ID_t>::iterator DeleteMe;
  std::map<ID_t, Lattice::Link>::iterator iLink;
  
  //std::cout << "Deleting Link ID " << LinkID << " form " << links[LinkID].S << " to " << links[LinkID].E << std::endl;

  DeleteMe=fwdLinks[links[LinkID].S].end();
  for( std::vector<ID_t>::iterator iLinkID=fwdLinks[links[LinkID].S].begin(); iLinkID!=fwdLinks[links[LinkID].S].end(); ++iLinkID )
  { 
    if( *iLinkID == LinkID ){
      DeleteMe=iLinkID;
      //std::cout << "Found in fwdlinks " << *iLinkID << std::endl;
      break;
    }
  }//for erase from fwdlinks
  if( DeleteMe!=fwdLinks[links[LinkID].S].end() )
  {
    fwdLinks[links[LinkID].S].erase(DeleteMe);
  }//if  

  DeleteMe=bckLinks[links[LinkID].E].end();
  for( std::vector<ID_t>::iterator iLinkID=bckLinks[links[LinkID].E].begin(); iLinkID!=bckLinks[links[LinkID].E].end(); ++iLinkID )
  {
    if( *iLinkID == LinkID ){
      DeleteMe=iLinkID;
      //std::cout << "Found in bcklinks " << *iLinkID << std::endl;
      break;
    }
  }//for erase from bcklinks
  if( DeleteMe!=bckLinks[links[LinkID].E].end() )
  {
    bckLinks[links[LinkID].E].erase(DeleteMe);
  }
  
  iLink=links.find(LinkID);
  if( iLink!=links.end() )
  {
    links.erase(iLink);
  }  
}//Lattice::deleteLink(ID_t LinkID)
*/

/*
void  Lattice::deleteNode(ID_t NodeID)
{
  std::vector<ID_t>::iterator DeleteMe;
  std::map<ID_t, Lattice::Node>::iterator iNode;

  //std::cout << "Deleting Node ID " << NodeID << std::endl;

  while( fwdLinks[NodeID].size() > 0)
  {
    deleteLink(*(fwdLinks[NodeID].begin()));
//    for( std::vector<ID_t>::iterator iLinkID=fwdLinks[NodeID].begin(); iLinkID!=fwdLinks[NodeID].end(); ++iLinkID )
//    {
//      deleteLink(*iLinkID);
//    }//for erase from fwdlinks
  }

  while( bckLinks[NodeID].size() > 0)
  {
    deleteLink(*(bckLinks[NodeID].begin()));
//    for( std::vector<ID_t>::iterator iLinkID=bckLinks[NodeID].begin(); iLinkID!=bckLinks[NodeID].end(); ++iLinkID )
//    {
//      deleteLink(*iLinkID);
//    }//for erase from fwdlinks
  }
  
  iNode=nodes.find(NodeID);
  if( iNode!=nodes.end() )
  {
    nodes.erase(iNode);
  }  
}
*/

/*
//void Lattice::cutOffPieceOfLattice(LatTime FromTime, LatTime ToTime, std::vector<ID_t> &from, std::vector<ID_t> &to)
bool  Lattice::cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, std::vector<ID_t> &from, std::vector<ID_t> &to)
{

  std::map<lse::ID_t,int> LinksToDelete, NodesToDelete;
  bool IsNodeInBEG=false, IsNodeInEND=false;
  std::vector<ID_t>::iterator DeleteMe;

  from.clear();
  to.clear();
      
  for (std::map<ID_t, Lattice::Node>::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
  {
    if( ((iNode->second).t >= FromTimeFrom) && ((iNode->second).t <= FromTimeTo) )
    {
      from.push_back((iNode->second).id);
      IsNodeInBEG=true;
      //std::cout << "Found node in BEG REG, ID: " <<  (iNode->second).id << std::endl;
    }
    if( ((iNode->second).t >= ToTimeFrom) && ((iNode->second).t <= ToTimeTo) )
    {
      to.push_back((iNode->second).id);
      IsNodeInEND=true;
      //std::cout << "Found node in END REG, ID: " <<  (iNode->second).id << std::endl;
    }
    if( ((iNode->second).t > FromTimeTo) && ((iNode->second).t < ToTimeFrom) )
    {
      NodesToDelete[(iNode->second).id]=0;
//      std::cout << "Found node in INNER REG, ID: " <<  (iNode->second).id << std::endl;
    }
  }//for (std::map<ID_t, Lattice::Node>::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)

  if( not (IsNodeInBEG && IsNodeInEND) ){
    return false;
  }
    
  for (std::map<ID_t, Lattice::Link>::iterator iLink = links.begin(); iLink != links.end(); ++iLink)
  {
    if( (nodes[(iLink->second).S].t <= FromTimeTo) && (nodes[(iLink->second).E].t > FromTimeTo) )
    {
      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, over BEG REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
    }
    
    if( (nodes[(iLink->second).S].t < ToTimeFrom) && (nodes[(iLink->second).E].t >= ToTimeFrom) )
    {
      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, over END REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
    }

//    if( (nodes[(iLink->second).S].t >= FromTimeFrom) && (nodes[(iLink->second).E].t <= ToTimeTo) )
//    {
//      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, in OUTER REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
//    }
  }//for links

  
  //std::cout << "Erasing Link ";
  for (std::map<lse::ID_t,int>::iterator iLinkID = LinksToDelete.begin(); iLinkID != LinksToDelete.end(); ++iLinkID)
  {
    //std::cout << (*iLinkID) << " ";
    deleteLink((iLinkID->first));
  }//delete links
//  std::cout << std::endl;
  
//  std::cout << "Erasing Node ";
  for (std::map<lse::ID_t,int>::iterator iNodeID = NodesToDelete.begin(); iNodeID != NodesToDelete.end(); ++iNodeID)
  {
    //std::cout << (*iNodeID) << " ";
    deleteNode((iNodeID->first));
  }//delete nodes
//  std::cout << std::endl;

  
  return true;
}//void cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, std::vector<ID_t> &from, std::vector<ID_t> &to);
*/

/*
void Lattice::insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)
{

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = nodes[*iNodeToID];
      tmpNode.id = LastNode;

      addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = fwdLinks[*iNodeToID].begin(); ifwdlink != fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        addLink(tmpLink);

        fwdLinks[LastNode].push_back(LastLink);
        bckLinks[links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if  

  }//for

  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
    {
    
        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;

        tmpLink.init();
        tmpLink.id = LastLink;
        tmpLink.S  = *iNodeFromID;
        tmpLink.E  = *iNodeToID;
        tmpLink.a  = 0;
        tmpLink.l  = 0;

        addLink(tmpLink);

        fwdLinks[*iNodeFromID].push_back(LastLink);
        bckLinks[*iNodeToID].push_back(LastLink);
        
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;
      
    }//for nodes from nowhere
  }//for nodes to nowhere

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    nodes[*iNodeToID].W = Label;
    nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      links[*iFwdLinkID].l = 0;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere

}//void Lattice::insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)
*/

/*
void Lattice::insertToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label, std::string WordInLm, LanguageModel &LM)
{                 

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = nodes[*iNodeToID];
      tmpNode.id = LastNode;

      addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = fwdLinks[*iNodeToID].begin(); ifwdlink != fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        addLink(tmpLink);

        fwdLinks[LastNode].push_back(LastLink);
        bckLinks[links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if  

  }//for

  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
    {
    
        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;

        tmpLink.init();
        tmpLink.id = LastLink;
        tmpLink.S  = *iNodeFromID;
        tmpLink.E  = *iNodeToID;
        tmpLink.a  = 0;
        tmpLink.l  = LM.GetWordNGramProb(DeleteBackslash((nodes[*iNodeFromID].W)+" "+WordInLm))*2.3026;
        //std::cout << DeleteBackslash(nodes[*iNodeFromID].W) << " " << DeleteBackslash(WordInLm) << " " << (tmpLink.l) << std::endl;

        addLink(tmpLink);

        fwdLinks[*iNodeFromID].push_back(LastLink);
        bckLinks[*iNodeToID].push_back(LastLink);
        
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;
      
    }//for nodes from nowhere
  }//for nodes to nowhere

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    nodes[*iNodeToID].W = Label;
    nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      links[*iFwdLinkID].l = LM.GetWordNGramProb(DeleteBackslash(WordInLm+" "+(nodes[links[*iFwdLinkID].E].W)))*2.3026;
      //std::cout << DeleteBackslash(WordInLm) << " " << DeleteBackslash(nodes[links[*iFwdLinkID].E].W) << " " << (links[*iFwdLinkID].l) << std::endl;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere

}//void Lattice::insertToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)
*/


void Lattice::timeNormalizeLattice()
{
  for(Links::iterator i = links.begin(); i != links.end(); ++i ){
    if((nodes[(*i)->E]->t-nodes[(*i)->S]->t) > 0){
      //std::cout << (*i)->a << " " << (*i)->l << " " << nodes[(*i)->E].t << " " << nodes[(*i)->S].t << std::endl; 
      (*i)->a=(*i)->a/(nodes[(*i)->E]->t-nodes[(*i)->S]->t)/100;
      //(*i)->l=(*i)->l/(nodes[(*i)->E].t-nodes[(*i)->S].t)/100;
      //std::cout << (*i)->a << " " << (*i)->l << std::endl;
    }  
  }//for links
}//void Lattice::insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)



void Lattice::ComputeNodesStartTimes()
{
	for (Nodes::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
	{
		for (vector<ID_t>::iterator iLink = bckLinks[(*iNode)->id].begin(); 
			 iLink != bckLinks[(*iNode)->id].end(); 
			 iLink++)
		{
			if ((*iNode)->tStart > nodes[links[*iLink]->S]->t)
			{
				(*iNode)->tStart = nodes[links[*iLink]->S]->t;
			}
		}
	}
}


bool Lattice::AddNgramToNgramsList(Ngrams &ngrams, Ngram *ngram, int nMax)
{
	bool add = true;
//	int i_overlapping = -1;
//	int delete_count = 0;

	//bool dbg=(ngram->GetItemWordId(2) == 18 && ngram->GetItemWordId(1) == 11 && ngram->GetItemWordId(0) == 22);

	//if (dbg) { DBG_FORCE("h_a:_k "<<*ngram); }

	// check overlapping ngrams
//	if (dbg) { DBG_FORCE("  ngrams.size():"<<ngrams.size()); }
	int checked_items = 0;
	NgramsHashItemList* p_ngrams_list = ngrams.GetHashItemList(ngram);
	int* p_ngrams_list_size = ngrams.GetHashItemListSize(ngram);
//	for (int i=ngrams.size()-1; i>=0; i--)	
	
	//if (dbg) DBG_FORCE("p_ngrams_list.size():"<<*p_ngrams_list_size);
	list<Ngram*>::iterator ipNgram = --p_ngrams_list->end(); 
	if (*p_ngrams_list_size > 0) 
	while (1)
	{
//		if (dbg) cerr << ".";
		bool check_overlapping = true;
		Ngram *pNgram = *ipNgram;
		//Ngram *pNgram = ngrams_list[i];

		// ngrams are sorted by t (end time), so when
		// we are processing ngrams in the reverse order 
		// (from highest t to the lowest t), after reaching
		// the first non-overlapping ngram,
		// we can break the look-for-overlapping-ngrams loop
		//bool dbg2 = (dbg && pNgram->GetItemWordId(2) == 18 && pNgram->GetItemWordId(1) == 11 && pNgram->GetItemWordId(0) == 22);
		//if (dbg2) DBG_FORCE("  pNgram: " << *pNgram);

		if (pNgram->GetEndTime() < ngram->GetStartTime())
		{
			//if (dbg) DBG_FORCE("  pNgram->GetEndTime() < ngram->GetStartTime() ...pNgram:"<<*pNgram);
			break;
		}

		if (ngram->GetHash() != pNgram->GetHash())
		{
			//if (dbg2) DBG_FORCE("  ngram->GetHash() != pNgram->GetHash()");
			check_overlapping = false;
		}
		else
		{
			check_overlapping = *ngram == *pNgram;
/*			for (int i=0; i<nMax; i++)
			{
				if (ngram->GetItemWordId(i) != pNgram->GetItemWordId(i))
				{
					check_overlapping = false;
				}
			}
*/		}
		//if (dbg2) DBG_FORCE("  check_overlapping:"<<check_overlapping);

		if (check_overlapping)
		{
			if (is_overlapping( ngram->GetStartTime(), ngram->GetEndTime(),
								pNgram->GetStartTime(), pNgram->GetEndTime()) )
			{
				//if (dbg2) DBG_FORCE("  is_overlapping");

				// if we are on the first item, it is not needed to add a new one, 
				// so let's just update it's values
				if (ipNgram == --p_ngrams_list->end())
				{
					//if (dbg2) DBG_FORCE("  updating the last item");
					if (ngram->GetStartTime() < pNgram->GetStartTime())
					{
						pNgram->SetStartTime(ngram->GetStartTime());
					}

					if (ngram->GetEndTime() > pNgram->GetEndTime())
					{
						pNgram->SetEndTime(ngram->GetEndTime());
					}

					if (ngram->GetConfidence() > pNgram->GetConfidence())
					{
						pNgram->SetConfidence(ngram->GetConfidence());
					}
					ngram = pNgram;
					add = false;
				}
				// if we are somewhere in the middle of the list,
				// we need to harmlessly remove the overlapping ngram 
				// and update the values of the new one
				else
				{
					if (ngram->GetStartTime() > pNgram->GetStartTime())
					{
						ngram->SetStartTime(pNgram->GetStartTime());
					}

					if (ngram->GetEndTime() < pNgram->GetEndTime())
					{
						ngram->SetEndTime(pNgram->GetEndTime());
					}

					if (ngram->GetConfidence() < pNgram->GetConfidence())
					{
						ngram->SetConfidence(pNgram->GetConfidence());
					}
					// remove the overlapping ngram
					//if (dbg2) DBG_FORCE("removing the overlapping item: "<<*ipNgram);
					list<Ngram*>::iterator iPrev = ipNgram; iPrev++; // it is previouse since we process the list from the end to the beginning
					p_ngrams_list->erase(ipNgram);
					ngrams.mSize--;
					(*p_ngrams_list_size)--;
					ipNgram = iPrev;
				}
/*
				if (i_overlapping < 0)
				{
					i_overlapping = i;
					if (ngram->GetStartTime() < pNgram->GetStartTime())
					{
						pNgram->SetStartTime(ngram->GetStartTime());
					}

					if (ngram->GetEndTime() > pNgram->GetEndTime())
					{
						pNgram->SetEndTime(ngram->GetEndTime());
					}

					if (ngram->GetConfidence() > pNgram->GetConfidence())
					{
						pNgram->SetConfidence(ngram->GetConfidence());
					}
				}
				else
				{
					Ngram *pNgramOverlapping = ngrams[i_overlapping];
					if (pNgramOverlapping->GetStartTime() < pNgram->GetStartTime())
					{
						pNgram->SetStartTime(pNgramOverlapping->GetStartTime());
					}

					if (pNgramOverlapping->GetEndTime() > pNgram->GetEndTime())
					{
						pNgram->SetEndTime(pNgramOverlapping->GetEndTime());
					}

					if (pNgramOverlapping->GetConfidence() > pNgram->GetConfidence())
					{
						pNgram->SetConfidence(pNgramOverlapping->GetConfidence());
					}
					DBG_FORCE("--------------------------------------------------");
					DBG_FORCE("REMOVING NGRAM:");
					DBG_FORCE("ngram:"<<*ngram);
					DBG_FORCE("pNgram:"<<*pNgram);
					DBG_FORCE("pNgramOverlapping:"<<*pNgramOverlapping);
					DBG_FORCE("ngrams[ngrams.size()-1-"<<delete_count<<"]:"<<*(ngrams[ngrams.size()-1-delete_count]));
					DBG_FORCE("--------------------------------------------------");
					ngrams[i_overlapping] = ngrams[ngrams.size()-1-delete_count];
					delete_count++;
					i_overlapping = i;
				}
				add = false;
*/				
			}
		}
		checked_items++;
		if (ipNgram == p_ngrams_list->begin())
		{
			break;
		}
		ipNgram--;
	}

	//if (dbg) DBG_FORCE("checked_items:"<<checked_items);
/*
	for (int i=0; i<delete_count; i++)
	{
		ngrams.pop_back();
	}
*/
	//if (dbg) DBG_FORCE("  add:"<<add);
	if (add)
	{
		p_ngrams_list->push_back(ngram);
		ngrams.mSize++;
		(*p_ngrams_list_size)++;
	}
	return add;
}

void Lattice::GenerateNgrams_recursive(
		Ngrams		 			&ngrams, 
		int 					nodeIdCur, 
		int 					nCur, 
		int 					nMax, 
		Ngram 					*ngram, 
		TLatViterbiLikelihood 	alphaLast)
{
	Lattice::Node* pNodeCur = nodes[nodeIdCur];

	ID_t nodeIdCurWordId = pNodeCur->W_id;
	bool jump_over = lexicon->isMetaWord(nodeIdCurWordId);

	bool ngram_completed = !jump_over && nCur+1 >= nMax;

	vector<ID_t> *pBckLinks = &bckLinks[nodeIdCur];
	for (vector<ID_t>::iterator iLink = pBckLinks->begin(); 
		 iLink != pBckLinks->end();
		 iLink ++)
	{
		Lattice::Link *pLink = links[*iLink];
		LatTime tStart = nodes[pLink->S]->t; // start time of the current node is the end time of the previous one
		Ngram* p_ngramNew;
		if (ngram != NULL)
		{
			p_ngramNew = new Ngram();
			p_ngramNew->CopyParamsFrom(*ngram);
			if (!jump_over)
			{
				p_ngramNew->SetItem(nCur, tStart, pNodeCur->t, pNodeCur->W_id);
			}
			p_ngramNew->SetConfidence( 
					(ngram->GetConfidence() - pNodeCur->alpha + alphaLast) 
					+ (pLink->confidence - pNodeCur->beta /* - alphaLast*/) 
					/* + alphaLast */);
		}
		else 
		{
			p_ngramNew = new Ngram(nMax);
			p_ngramNew->SetConfidence(pLink->confidence);
			if (!jump_over)
			{
				p_ngramNew->SetItem(nCur, tStart, pNodeCur->t, pNodeCur->W_id);
			}
		}

		p_ngramNew->AddNodeToPath(pNodeCur->id);

		bool add = false;
		if (ngram_completed) 
		{
			add = AddNgramToNgramsList(ngrams, p_ngramNew, nMax);
//			add = true; ngrams.push_back(p_ngramNew);
//			DBG_FORCE(p_ngramNew<<" push_back: p_ngramNew = "<<*p_ngramNew);
		}

		int nNext = nCur+(!jump_over ? 1 : 0);
		if (nNext < nMax)
		{
			GenerateNgrams_recursive(ngrams, pLink->S, nNext, nMax, p_ngramNew, alphaLast);
		}

		// we we have not added the ngram to the list of output ngrams, then we should delete it
		if (!add)
		{
			delete p_ngramNew;
		}
	}
}

void Lattice::OutputNgrams(string filename, int n)
{
	/*
	DBG_FORCE("Computing nodes' start times");
	ComputeNodesStartTimes();
	DBG_FORCE("Computing nodes' start times...done");
	*/
	Ngrams ngrams(NGRAMS_HASH_SIZE);
	DBG_FORCE("Generating "<<n<<"-grams");
	TLatViterbiLikelihood alpha_last = (*nodes.rbegin())->alpha;
	for (Nodes::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
	{
		DBG("--------------------------------------------------");
		DBG("Sending tokens from node: "<<**iNode);
		GenerateNgrams_recursive(ngrams, (*iNode)->id, 0, n, NULL, alpha_last);
	}
	DBG_FORCE("Generating "<<n<<"-grams...done");

	DBG_FORCE("OutputNgrams("<<filename<<", "<<n<<")");
	ofstream out(filename.c_str());

	NgramsCursor cur(ngrams);
	for (Ngram *pNgram = cur.Begin(); pNgram != NULL; pNgram = cur.Next())
/*
	for (vector<Ngram*>::iterator iNgram = ngrams.begin();
		 iNgram != ngrams.end();
		 iNgram ++)
*/	{
//			Ngram *pNgram = *iNgram;
		out << pNgram->GetStartTime() << "\t" << pNgram->GetEndTime() << "\t";
		for (int i=n-1; i>=0; i--)
		{
			out << lexicon->getVal(pNgram->GetItemWordId(i)) << (i>0 ? "_" : "");
		}
		out << "\t" << pNgram->GetConfidence();
		out << "\t" << pNgram->GetNodePath();
		out << endl;
	}

	out.close();
}


void Lattice::GenerateNgramsIndex(int n, string meeting_rec)
{
	Ngrams ngrams(NGRAMS_HASH_SIZE);
	DBG_FORCE("Generating ngrams");
	TLatViterbiLikelihood alpha_last = (*nodes.rbegin())->alpha;
	for (Nodes::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
	{
//		if (iNode->second.t > 204) break;
		DBG("--------------------------------------------------");
		DBG("Sending tokens from node: "<<**iNode);
		GenerateNgrams_recursive(ngrams, (*iNode)->id, 0, n, NULL, alpha_last);
	}
	DBG_FORCE("Generating ngrams...done");

	LatIndexer::Record latIndexerRec;
	if ((latIndexerRec.mMeetingID = this->indexer->meetings.getRecID(meeting_rec)) < 0) // add index for this meeting to indexer->meetings
	{
		CERR("ERROR: meeting '"<<meeting_rec<<"' does not exist in the meetings index");
		exit(1);
	}

	if (ngrams.Size() > 0)
	{
		this->indexer->fwdIndex.addMeeting(ngrams.Size());

		NgramsCursor cur(ngrams);
		for (Ngram *pNgram = cur.Begin(); pNgram != NULL; pNgram = cur.Next())
	/*
			for (vector<Ngram*>::iterator iNgram = ngrams.begin();
				 iNgram != ngrams.end();
				 iNgram ++)
	*/
		{
	//		Ngram *pNgram = *iNgram;
			string ngram_str = "";
			for (int i=n-1; i>=0; i--)
			{
				ngram_str += lexicon->getVal(pNgram->GetItemWordId(i)) + (i>0 ? "_" : "");
			}

			latIndexerRec.mWordID = this->lexicon->word2id_readonly(ngram_str);
			latIndexerRec.mNodeID = 0;
			latIndexerRec.mConf = pNgram->GetConfidence();
			latIndexerRec.mStartTime = pNgram->GetStartTime();
			latIndexerRec.mEndTime = pNgram->GetEndTime();

			this->indexer->fwdIndex.addRecord(latIndexerRec, false); // add node to search index
		}
	}
}

void Lattice::GenerateIndex(std::string meeting_rec)
{
	DBG("Lattice::GenerateIndex()");
	LatIndexer::Record latIndexerRec;
	latIndexerRec.mNodeID = 0;
	if ((latIndexerRec.mMeetingID = this->indexer->meetings.getRecID(meeting_rec)) < 0) // add index for this meeting to indexer->meetings
	{
		CERR("ERROR: meeting '"<<meeting_rec<<"' does not exist in the meetings index");
		exit(1);
	}

	this->indexer->fwdIndex.addMeeting(nodes.size());
	for(Lattice::Nodes::iterator i=nodes.begin(); i!=nodes.end(); ++i) 
	{
		latIndexerRec.mWordID = lexicon->word2id_readonly((*i)->W);
		latIndexerRec.mConf = (*i)->bestLikelihood;
		latIndexerRec.mStartTime = (*i)->tStart;
		latIndexerRec.mEndTime = (*i)->t;
		if (!lexicon->isMetaWord(latIndexerRec.mWordID)) { //getValID("!NULL")) {
			indexer->fwdIndex.addRecord(latIndexerRec); // add node to search index
		}
	}
}

