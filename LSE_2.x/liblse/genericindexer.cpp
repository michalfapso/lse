#include "genericindexer.h"

using namespace std;
using namespace lse;
/*
//--------------------------------------------------------------------------------
// If there is no occurence of given val in the lexicon, 
// new record is created in both val->id and id->val maps
// and valID of inserted val is returned,
// 
// Return value: ID of given val

template< class T > ID_t GenericIndexer< T >::getValID(const T val) {

	// try to find given val in val2idMap
	typename map<T, ID_t>::iterator pos = val2idMap.find(val);
//	Val2ID_iter pos = val2idMap.find(val);
	
	if (pos != val2idMap.end()) {
		// ID of given val is returned
		return pos->second;		
	} 
	else {
		// new record is created and it's ID is returned
		ID_t newID = getNewID();
		val2idMap.insert(make_pair(val, newID));
		id2valMap.insert(make_pair(newID, val));	
		return newID;
	}
}


//--------------------------------------------------------------------------------
// If there is a val with given ID in the lexicon, it's ID is returned,
// otherwise NULL is returned
//
// Return value: val with given ID (or NULL)

template< class T > T GenericIndexer< T >::getVal(const ID_t id) {
	
	typename map<ID_t, T>::iterator pos = id2valMap.find(id);
//	ID2val_iter pos = id2valMap.find(id);

	if (pos != id2valMap.end()) {
		// val with given ID is returned
		return pos->second;
	} 
	else {
		// ERROR: there is no such val in the lexicon
		return NULL;
	}
}


//--------------------------------------------------------------------------------
// Return value - next possible valID

template< class T > ID_t GenericIndexer< T >::getNewID() {

	// Sequence search for max key (ID) value
	ID_t max = 0;
	for (typename map<ID_t, T>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		if (max < i->first) {
			max = i->first;
		}
	}
	return max+1;
}


//--------------------------------------------------------------------------------

template<class T> int GenericIndexer<T>::saveToFile(const string filename) {
		
	ofstream out(filename.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	char terminator = 0;
	
	// it is not important which map to save to file, because they are identical
	for(typename map<ID_t, T>::iterator i=id2valMap.begin(); i!=id2valMap.end(); ++i) {

		writeStruct(out, i->first, i->second);
	}
	out.close();
	return 0;
}


//--------------------------------------------------------------------------------

template< class T > 
int GenericIndexer< T >::loadFromFile( string filename ) {
	
	// clear both maps
	id2valMap.clear();
	val2idMap.clear();

	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
		return (2);
	}
	
	ID_t valID;
	T val;
	
	// read all nodes and convert strID into str using lexicon
	while(!in.eof()) {
		
		if (readStruct(in, valID, val) != 0) {
			break;
		}
		// add record to both maps
		val2idMap.insert(make_pair(val, valID));
		id2valMap.insert(make_pair(valID, val));	
	}
	
	in.close();
	
	return 0;
}




template< class T > void GenericIndexer< T >::print() {
	for (typename map<ID_t, T>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		cout << "ID=" << i->first << " " << i->second << endl;
	}

}
*/

