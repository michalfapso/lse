#include "latindexer.h"
#include "common.h"

using namespace std;
using namespace lse;


LatIndexer::RecordsReader::~RecordsReader()
{
	if(mpByTime != NULL) delete[] mpByTime;
}

LatIndexer::LatIndexer() :
	mIsIndexLoaded(false),
	mpWordIdxRecords(NULL),
	mWordIdxRecords_size(0),
	mFirstWordID(0),
	mpSortIdxRecordsArray_byTime_Pointers(NULL)
{
	pthread_mutex_init(&mMutex, NULL);
}

LatIndexer::~LatIndexer()
{
	if (mpWordIdxRecords)
		delete[] mpWordIdxRecords;
	if (mpSortIdxRecordsArray_byTime_Pointers)
		delete[] mpSortIdxRecordsArray_byTime_Pointers;
	pthread_mutex_destroy(&mMutex);
}

//--------------------------------------------------------------------------------
//	LatIndexer::RecordClusters
//--------------------------------------------------------------------------------
void LatIndexer::RecordClusters::sort() 
{
	for (RecordClusters::iterator i=this->begin(); i!=this->end(); ++i) {
		std::sort(i->begin(), i->end());
	}
}


void LatIndexer::RecordClusters::add(LatIndexer::Record rec, bool checkOverlapping) 
{
	if (checkOverlapping)
	{
		// let's find the set for given rec
		for (RecordClusters::iterator iCluster=this->begin(); iCluster!=this->end(); ++iCluster) {
			// compare with all nodes in set
			if (iCluster->W_id == rec.mWordID && iCluster->meetingID == rec.mMeetingID) {
				for (vector<LatIndexer::Record>::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
					//DBG_FORCE("if ("<<rec.t<<" >= "<<iWord->tStart<<" && "<<rec.t<<" <= "<<iWord->t<<")");
					if (rec.mEndTime >= iWord->mStartTime && rec.mEndTime <= iWord->mEndTime) {				//rec's end time is in iWord
						iCluster->addRecord(rec);
						return;
					} 
					//DBG_FORCE("if ("<<rec.tStart<<" >= "<<iWord->tStart<<" && "<<rec.tStart<<" <= "<<iWord->t<<")");
					if (rec.mStartTime >= iWord->mStartTime && rec.mStartTime <= iWord->mEndTime) { 		//rec's start time is in iWord
						iCluster->addRecord(rec);
						return;
					}

					if (rec.mStartTime <= iWord->mStartTime && rec.mEndTime >= iWord->mEndTime) {			//iWord is in rec
						iCluster->addRecord(rec);
						return;
					}
				}
			}
		}
	}
	// if no matching set found, create a new one
//	DBG_FORCE("Creating a new Cluster...");
	LatIndexer::Cluster v;
	v.addRecord(rec);
	v.W_id = rec.mWordID;
	v.meetingID = rec.mMeetingID;
	v.tStart = rec.mStartTime;
	v.tEnd = rec.mEndTime;
	v.conf = rec.mConf;
	this->push_back(v);
//	DBG_FORCE("rec: tStart:"<< rec.tStart << "\t" << (Lattice::Node)rec);
//	DBG_FORCE("done");
//	cin.get();
}

/*
void LatIndexer::RecordClusters::computeOverlappedRecordsLikelihood() 
{
	for (RecordClusters::iterator iCluster=this->begin(); iCluster!=this->end(); ++iCluster) {
		for (Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
			iWord->overlapped_likelihood = iWord->likelihood;		// word's own likelihood
			for (Cluster::iterator iWordOverlapped=iCluster->begin(); iWordOverlapped!=iCluster->end(); ++iWordOverlapped) {
				LatTime start = iWordOverlapped->tStart;
				LatTime end = iWordOverlapped->t;
				if (start < iWord->tStart && end > iWord->t)
					iWord->overlapped_likelihood = logAdd(iWord->overlapped_likelihood, iWordOverlapped->likelihood);
			}
//				iWord->overlapped_likelihood -= 
		}
	}
}
*/

void LatIndexer::RecordClusters::print() 
{
	DBG("size(): "<< size());
	for (RecordClusters::iterator iCluster=this->begin(); iCluster!=this->end(); ++iCluster) {
		cout << "==================================================" << endl << flush;
		DBG("  wClusters.size(): "<<iCluster->size());
		for (Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
//			cout << "overlapped_likelihood:" << iWord->overlapped_likelihood << " likelihood:" << iWord->likelihood << " tStart:" << iWord->tStart << "\t" << *iWord << " " << endl << flush;
			cout << "conf:" << iWord->mConf << " tStart:" << iWord->mStartTime << "\t" << *iWord << " " << endl << flush;
		}
	}
}



//--------------------------------------------------------------------------------
//	LatIndexer::Cluster
//--------------------------------------------------------------------------------
//void LatIndexer::Cluster::addRecord(Cluster::Record rec)
void LatIndexer::Cluster::addRecord(LatIndexer::Record &rec)
{
	if (rec.mStartTime < this->tStart || this->size() == 0) 
	{
		this->tStart = rec.mStartTime;
	}
	if (rec.mEndTime > this->tEnd || this->size() == 0) 
	{
		this->tEnd = rec.mEndTime;
	}
	if (rec.mConf > this->conf || this->size() == 0) 
	{
		this->conf = rec.mConf;
	}
	this->push_back(rec);
}


//--------------------------------------------------------------------------------
std::ostream& lse::operator<<(std::ostream& os, const LatIndexer::Record& rec) 
{
	return os << "meetingID:" << rec.mMeetingID << "\twordID:" << rec.mWordID << "\tconf:" << rec.mConf << "\ttStart:" << rec.mStartTime << "\ttEnd:" << rec.mEndTime << "\tnodeID:" << rec.mNodeID;
}
std::ostream& lse::operator<<(std::ostream& os, const LatIndexer::Occurrence& o)
{
	return os << "meetingID:" << o.mMeetingID << "\tt[" << o.mStartTime << ".." << o.mEndTime << "]\tconf:" << o.mConf;
}

void LatIndexer::Record::PrintDataOnly(std::ostream& os)
{
	os << this->mMeetingID << "\t" << this->mWordID << "\t" << this->mConf << "\t" << this->mStartTime << "\t" << this->mEndTime << endl;
}


#define SORT_REVERSEINDEX_BY_CONFIDENCE
#ifdef SORT_REVERSEINDEX_BY_CONFIDENCE
	//--------------------------------------------------------------------------------
	bool lse::operator<(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		// comparison priority: wordID > conf > meetingID
		if (lv.mWordID == rv.mWordID) {
			return lv.mConf > rv.mConf; // nodes with greater posteriors will be first in containers
		} else {
			return lv.mWordID < rv.mWordID;			
		}		
	}

	//--------------------------------------------------------------------------------
	bool lse::operator>(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		// comparison priority: wordID > conf > meetingID
		if (lv.mWordID == rv.mWordID) {
			return lv.mConf < rv.mConf; // nodes with greater posteriors will be first in containers
		} else {
			return lv.mWordID > rv.mWordID;			
		}		
	}
#else
	//--------------------------------------------------------------------------------
	bool lse::operator<(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		if (lv.mWordID == rv.mWordID) {
			if (lv.mMeetingID == rv.mMeetingID) {
				return lv.mEndTime < rv.mEndTime;
			} else {
				return lv.mMeetingID < rv.mMeetingID;
			}
		} else {
			return lv.mWordID < rv.mWordID;
		}		
	}

	//--------------------------------------------------------------------------------
	bool lse::operator>(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		if (lv.mWordID == rv.mWordID) {
			if (lv.mMeetingID == rv.mMeetingID) {
				return lv.mEndTime > rv.mEndTime;
			} else {
				return lv.mMeetingID > rv.mMeetingID;
			}
		} else {
			return lv.mWordID > rv.mWordID;			
		}		
	}
#endif

/*
char LatIndexer::SortIdxRecord::CmpConf(const SortIdxRecord &lv, const SortIdxRecord &rv)
{
	if (lv.wordID == rv.wordID) {
		return (lv.conf > rv.conf ? -1 : (lv.conf < rv.conf ? 1 : 0))
	} else {
		return (lv.wordID < rv.wordID ? -1 : 1) ;
	}		
}
*/

bool lse::operator<(const LatIndexer::RecordSortedByTime& lv, const LatIndexer::RecordSortedByTime& rv)
{
	if (lv.mWordID == rv.mWordID) {
		if (lv.mMeetingID == rv.mMeetingID) {
			return lv.mEndTime < rv.mEndTime;
		} else {
			return lv.mMeetingID < rv.mMeetingID;
		}
	} else {
		return lv.mWordID < rv.mWordID;
	}		
}

bool lse::operator<(const LatIndexer::RecordSortedByConfidence& lv, const LatIndexer::RecordSortedByConfidence& rv)
{
	if (lv.mWordID == rv.mWordID) {
		return lv.mConf > rv.mConf;
	} else {
		return lv.mWordID < rv.mWordID;
	}		
}



char LatIndexer::Record::CmpConf(const LatIndexer::Record &lv, const LatIndexer::Record &rv)
{
	if (lv.mWordID == rv.mWordID) {
		return (lv.mConf < rv.mConf ? -1 : (lv.mConf > rv.mConf ? 1 : 0));
	} else {
		return (lv.mWordID < rv.mWordID ? -1 : 1) ;
	}		
}

//--------------------------------------------------------------------------------
// addRecord()

void LatIndexer::addRecord(LatIndexer::Record latRec) {
	records.push_back(latRec);
}



//--------------------------------------------------------------------------------
// saveToFile()

int LatIndexer::saveToFile(const string filename) {
	
	ofstream out(filename.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	for(LatIndexer::Records::iterator i=records.begin(); i!=records.end(); ++i) {
		out.write(reinterpret_cast<const char *>(&(*i)), sizeof(*i));
/*		
		out.write(reinterpret_cast<const char *>(&(i->wordID)), sizeof(i->wordID));
		out.write(reinterpret_cast<const char *>(&(i->meetingID)), sizeof(i->meetingID));
		out.write(reinterpret_cast<const char *>(&(i->position)), sizeof(i->position));
		out.write(reinterpret_cast<const char *>(&(i->conf)), sizeof(i->conf));
		out.write(reinterpret_cast<const char *>(&(i->t)), sizeof(i->t));
*/
	}
	out.close();
	return 0;
}



//--------------------------------------------------------------------------------
// loadFromFile()

int LatIndexer::loadFromFile(const string filename) {
	// clear container
	records.clear();

	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
			return (2);
	}

	LatIndexer::Record latRec;

	// read all nodes
	while(!in.eof()) {
		in.read(reinterpret_cast<char *>(&latRec), sizeof(latRec));
		if(in.eof()) break;
/*
		in.read(reinterpret_cast<char *>(&latRec.wordID), sizeof(latRec.wordID));
		in.read(reinterpret_cast<char *>(&latRec.meetingID), sizeof(latRec.meetingID));
		in.read(reinterpret_cast<char *>(&latRec.position), sizeof(latRec.position));
		in.read(reinterpret_cast<char *>(&latRec.conf), sizeof(latRec.conf));
		in.read(reinterpret_cast<char *>(&latRec.t), sizeof(latRec.t));
*/
		// add record to container
		records.push_back(latRec);
	}

	in.close();

	return 0;
}



//--------------------------------------------------------------------------------
// sortFile()

void LatIndexer::sortRecords() {
	sort(records.begin(), records.end());
}


//--------------------------------------------------------------------------------
// print()

void LatIndexer::print() {
	
	for(LatIndexer::Records::iterator i=records.begin(); i!=records.end(); ++i) {
		cout << *i << endl;
	}
}

int LatIndexer::createWordIDIndexFile(const string filename) 
{
	ofstream out_conf;
	ofstream out_time;

	string str = filename + LATINDEXER_EXT_WIDINDEX;
	out_conf.open(str.c_str(), ios::binary);
	if (out_conf.bad()) {
		CERR("ERROR: Cannot open file "<<str);
		EXIT();
	}

	out_time.open((filename + LATINDEXER_EXT_TIMESORT_POINTERS).c_str(), ios::binary);
	if (out_time.bad()) {
		CERR("ERROR: Cannot open file "<<(filename + LATINDEXER_EXT_TIMESORT_POINTERS));
		EXIT();
	}

	ifstream in(filename.c_str(), ios::binary);
	if (in.bad()) {
		CERR("ERROR: Cannot open file "<<filename);
		EXIT();
	}

	ID_t wordID_pred = -1;
	LatIndexer::Record latRec;
	unsigned int pos = 0;
	int pos_null = -1;
	int wordCount = 0; // number of words in the whole index
	unsigned int recordsCount = 0; // number of records for one word
	unsigned int record_idx = 0;
	while(!in.eof()) {
//		pos = in.tellg();
		latRec.ReadFrom(in);
		if (in.eof()) break;
		
		if (wordID_pred != latRec.mWordID) {
			if (wordID_pred == -1) {
				// header
				out_conf.write(reinterpret_cast<const char *>(&latRec.mWordID), sizeof(latRec.mWordID));// first word's ID
				out_conf.write(reinterpret_cast<const char *>(&wordCount), sizeof(wordCount));// how many records are stored in this file - will be set later

			} else {
				out_conf.write(reinterpret_cast<const char*>(&recordsCount), sizeof(recordsCount));
//				DBG_FORCE("wordID:"<<wordID_pred<<" recordsCount:"<<recordsCount);
				// padding
				for (int i = 0; i < latRec.mWordID - wordID_pred - 1; i++) {
					out_conf.write(reinterpret_cast<const char*>(&pos_null), sizeof(pos_null));
					out_conf.write(reinterpret_cast<const char*>(&pos_null), sizeof(pos_null));
					out_time.write(reinterpret_cast<const char*>(&pos_null), sizeof(pos_null));
					wordCount++;
				}
			}
			out_conf.write(reinterpret_cast<const char*>(&pos), sizeof(pos));
			out_time.write(reinterpret_cast<const char*>(&record_idx), sizeof(record_idx));
//			DBG_FORCE("wordID:"<<latRec.mWordID<<" pos:"<<pos);
			recordsCount = 0;
			wordCount++;
//			DBG_FORCE("WRITE: WORD_ID="<<latRec.wordID<<"  wordCount="<<wordCount<<"  pos="<<pos);
			wordID_pred = latRec.mWordID;
		}
		record_idx++;
		recordsCount++;
		pos+=latRec.Size();
	}

//	DBG_FORCE("wordID:"<<latRec.mWordID<<" recordsCount:"<<recordsCount);
	out_conf.write(reinterpret_cast<const char*>(&recordsCount), sizeof(recordsCount));
	out_conf.seekp(sizeof(latRec.mWordID), ios_base::beg); 			// jump to the first record with given wordID
	out_conf.write(reinterpret_cast<const char*>(&wordCount), sizeof(wordCount));
	
//	fclose(fin);
	out_conf.close();
	out_time.close();
	in.close();
	return 0;

}


bool LatIndexer::LoadIndex(const string &filename)
{
	/* read the reverse index hits (sorted by confidence) */
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("LatIndexer::LoadIndex("<<filename<<")");
	DBG_FORCE("SORTED BY CONFIDENCE");
	// POINTERS FILE (wordID -> position in search index file)
	DBG_FORCE("fopen("<<(filename + LATINDEXER_EXT_WIDINDEX).c_str()<<")");
	FILE *finIndex = fopen((filename + LATINDEXER_EXT_WIDINDEX).c_str(), "rb");
	if (finIndex == NULL) {
		CERR("ERROR: cannot open file '"<<(filename + LATINDEXER_EXT_WIDINDEX)<<"'");
		exit(LSE_ERROR_IO);
		return false;
	}

	// read header
	fread(&mFirstWordID, sizeof(mFirstWordID), 1, finIndex);
	fread(&mWordIdxRecords_size, sizeof(long), 1, finIndex);
	DBG_FORCE("firstID: "<<mFirstWordID<<"   array_size:"<<mWordIdxRecords_size);

	// read pointers array
	mpWordIdxRecords = new unsigned long[mWordIdxRecords_size * 2];
	fread(mpWordIdxRecords, sizeof(long), mWordIdxRecords_size * 2, finIndex); // *2 because there are 2 values for each record
	fclose(finIndex);


	// SORTED BY TIME - POINTERS FILE (wordID -> position in search index file sorted by time)
	DBG_FORCE("file_size('"<< (filename + LATINDEXER_EXT_TIMESORT_POINTERS) <<"') = "<<file_size((filename + LATINDEXER_EXT_TIMESORT_POINTERS).c_str()) );
	FILE *finIndexTime = fopen((filename + LATINDEXER_EXT_TIMESORT_POINTERS).c_str(), "rb");
	if (finIndexTime == NULL) {
		CERR("ERROR: cannot open file '"<<(filename + LATINDEXER_EXT_TIMESORT_POINTERS)<<"'");
		exit(LSE_ERROR_IO);
		return false;
	}
	mpSortIdxRecordsArray_byTime_Pointers = new unsigned int[mWordIdxRecords_size];
	fread(mpSortIdxRecordsArray_byTime_Pointers, sizeof(int), mWordIdxRecords_size, finIndexTime);
	fclose(finIndexTime);

	mIsIndexLoaded = true;
	return true;
}

void LatIndexer::dumpWordIdxRecords(int dbgId)
{
	DBG("LatIndexer::dumpWordIdxRecords");
	DBG("mpWordIdxRecords:"<<mpWordIdxRecords);
	DBG("mWordIdxRecords_size:"<<mWordIdxRecords_size);
	char dbg_filename[512];
	sprintf(dbg_filename, "mpWordIdxRecords_%d_%u.dump", dbgId, (unsigned int)pthread_self());
	ofstream ofdbg(dbg_filename);
	ofdbg << "mWordIdxRecords_size:"<<mWordIdxRecords_size<<endl;
	for (unsigned int i=0; i<mWordIdxRecords_size; i++)
	{
		ofdbg << i << ": " << mpWordIdxRecords[i*2] << " " << mpWordIdxRecords[i*2 + 1] << endl;
	}
	ofdbg << "mWordIdxRecords_size:"<<mWordIdxRecords_size<<endl;
	ofdbg << "end"<<endl;
	ofdbg.close();
}

//--------------------------------------------------------------------------------
/* findWord() {{{ */
//
bool LatIndexer::findWord(const string filename, const ID_t wordID, const DocumentsList &docList, LatIndexer::RecordsReader *result, unsigned int pMaxCount, float pOccurrenceThreshold) 
{
	//pthread_mutex_lock(&mMutex);
	//DBG_FORCE("LatIndexer::findWord("<<wordID<<")");
	
	if (!mIsIndexLoaded)
	{
		CERR("ERROR: Index has to be loaded before calling findWord() because of multi-threading concurrency.");
		EXIT();
	}
	
	// is given wordID in the reverse index? If not, do nothing
	if (wordID < mFirstWordID || (unsigned int)wordID > mFirstWordID + mWordIdxRecords_size - 1)
	{
		CERR("ERROR: in findWord(): given wordID '"<<wordID<<"' is not in the index! ... "<<wordID<<" < "<<mFirstWordID<<" || "<<wordID<<" > "<<mFirstWordID<<" + "<<mWordIdxRecords_size<<" - 1");
		EXIT();
	}

	unsigned long startPos = mpWordIdxRecords[(wordID - mFirstWordID) * 2]; // -1 because wordIDs start from 1 (not 0) and !NULL node
	long totalWordRecordsCount = mpWordIdxRecords[(wordID - mFirstWordID) * 2 + 1];
	DBG("startPos: "<<startPos);
	DBG("totalWordRecordsCount: "<<totalWordRecordsCount);

	//assert(totalWordRecordsCount < 1000000);

	if (totalWordRecordsCount <= 0)
	{
		return true;
	}
	DBG_FORCE("totalWordRecordsCount:"<<totalWordRecordsCount);
	bool *pValidWords = new bool[totalWordRecordsCount];
	//DBG_FORCE("startPos:"<<startPos);
	// if there is any occurence in the reverse index
	if (startPos >= 0)
	{
		// SEARCH INDEX
		ifstream fin(filename.c_str(), ios::binary);
		if (!fin.good()) {
			CERR("Error opening file "<<filename);
			EXIT();
		}

		LatIndexer::Record latRec;
		//DBG("sizeof(latRec): "<<sizeof(latRec));
		fin.seekg(startPos, ios_base::beg); 			// jump to the first record with given wordID
		int counter = 0;
		int counter_valid = 0;
		while (!fin.eof()) 
		{
			assert(fin.good());
			latRec.ReadFrom(fin);
//			DBG("latRec: "<<latRec);
			if (fin.eof()) break;
			if (!fin.good())
			{
				CERR("ERROR: !fin.good()");
				EXIT();
			}

			if (latRec.mWordID != wordID) {
				//DBG_FORCE("End of reverse index ... wordID="<<latRec.mWordID<<" reached");
				break;
			} 

			if ((docList.empty() || docList.find(latRec.mMeetingID) != docList.end())
				&& ((unsigned int)counter_valid < pMaxCount) 
				&& (latRec.mConf > pOccurrenceThreshold) ) 
			{
				//DBG_FORCE("valid record: "<<latRec);
				result->push_back(latRec);
				assert(counter < totalWordRecordsCount);
				pValidWords[counter] = true;
				counter_valid++;
			} else {
				//DBG_FORCE("invalid record: "<<latRec);
				assert(counter < totalWordRecordsCount);
				pValidWords[counter] = false;
			}
			counter++;
		}
		DBG("pValidWords setting counter: "<<counter);
		fin.close();
	}

	




	//DBG_FORCE("--------------------------------------------------");
	//DBG_FORCE("SORTED BY TIME");
	/* read the reverse index hits (sorted by time) {{{ */
	
	startPos = sizeof(LatIndexer::RecordIdx) * mpSortIdxRecordsArray_byTime_Pointers[wordID - mFirstWordID]; // -1 because wordIDs start from 1 (not 0) and !NULL node
//	startPos = 0;
	//DBG_FORCE("startPos:"<<startPos);
	// if there is any occurence in the reverse index
	if (startPos >= 0)
	{
#define USE_FSTREAM 1
#if USE_FSTREAM		
		ifstream fin((filename + LATINDEXER_EXT_TIMESORT).c_str(), ios_base::in | ios_base::binary);
		assert(fin.good());
		if (result->mpByTime != NULL)
		{
			delete[] result->mpByTime;
		}
		result->mpByTime = new LatIndexer::RecordIdx[result->size()];
		fin.seekg(startPos, ios_base::beg);			// jump to the first record with given wordID
		assert(fin.good());
		long counter = 0;
		LatIndexer::RecordIdx *pByTimeCursor = result->mpByTime;
		while (!fin.eof() && counter < totalWordRecordsCount) 
		{
			LatIndexer::RecordIdx rec;
			assert(fin.good());
			fin.read((char*)&rec, sizeof(rec));
			assert(fin.good());

			assert(rec < totalWordRecordsCount);
			if (pValidWords[rec])
			{
				//DBG_FORCE("...added");
				*pByTimeCursor = rec;
				pByTimeCursor++;
			}
			counter++;
		}

		fin.close();
#else		

		FILE *fin = fopen((filename + LATINDEXER_EXT_TIMESORT).c_str(), "rb");
		assert(fin);
		assert(!ferror(fin));
		if (result->mpByTime != NULL)
		{
			delete[] result->mpByTime;
		}
		result->mpByTime = new LatIndexer::RecordIdx[result->size()];
		fseek(fin, startPos, SEEK_SET); 			// jump to the first record with given wordID
		assert(!ferror(fin));
		//fread(result->mpByTime, sizeof(LatIndexer::RecordIdx), result->size(), fin);
		long counter = 0;
		//LatIndexer::RecordIdx *pByTimeCursor = result->mpByTime;
		while (!feof(fin) && counter < totalWordRecordsCount) 
		{
			LatIndexer::RecordIdx rec;
			assert(!ferror(fin));
			if (fread(&rec, sizeof(rec), 1, fin) != 1)
			{
				CERR("ERROR reading file");
				EXIT();
			}
			assert(!ferror(fin));
			assert(!feof(fin));

			assert(rec < totalWordRecordsCount);
			if (pValidWords[rec])
			{
				//DBG_FORCE("...added");
				*pByTimeCursor = rec;
				pByTimeCursor++;
			}
			counter++;
		}
		fclose(fin);
#endif
	}
	delete[] pValidWords;
	/* }}} */
	//DBG_FORCE("getting the index sorted by time...done");

	//DBG_FORCE("mpByTime:"<<result->mpByTime);
	//DBG_FORCE("--------------------------------------------------");
	//pthread_mutex_unlock(&mMutex);
	return true;
}
/* }}} */



//================================================================================
// FORWARD INDEX
//================================================================================

/**
  @brief Create forward index file or append to an existing one

  @param p_filename Forward index filename

  @return Zero if OK
*/
int FwdIndex::create(string p_filename) 
{	
	this->mFilename = p_filename;
	DBG_FORCE("Creating fwd index file: "<< this->mFilename);
	this->of.open(this->mFilename.c_str(), ios::binary | ios::app);
	if (!this->of.good()) {
		CERR("ERROR: Cannot open file for writing: "<<p_filename);
		EXIT();
	}
	if (mpData != NULL)
	{
		delete[] mpData;
		mpData = NULL;
		mpDataEndptr = NULL;
		mDataSize = 0;
		mpSortIdxRecordsArray_byTime = NULL;
		mpSortIdxRecordsArray_byConfidence = NULL;
		mpData2ByConf = NULL;
	}
	mActive = false;
	return 0;
}

void FwdIndex::addMeeting(int nodesCount)
{
	DBG_FORCE("FwdIndex::addMeeting("<<nodesCount<<")");
	assert(nodesCount > 0);
	if (mpData != NULL)
	{
		delete[] mpData;
	}
	mpData = new FwdIndex::Record[nodesCount];
	mpDataEndptr = mpData;
	mDataSize = 0;
	mActive = true;
}

void FwdIndex::closeMeeting()
{
	for(unsigned long long i=0; i<this->mDataSize; i++) {
//		DBG("mpData["<<i<<"]: "<<mpData[i]);
		mpData[i].WriteTo(of);
	}
	if (mpData != NULL)
	{
		delete[] mpData;
		mpData = NULL;
		mpDataEndptr = NULL;
		mDataSize = 0;
	}
}

/**
  @brief Append record to the end of the forward index file

  @param latRec Record to be appended
*/
void FwdIndex::addRecord(LatIndexer::Record latRec, bool checkOverlapping) 
{
	/*
	if (latRec.mWordID == 268931)
	{
		DBG_FORCE("==================================================");
		DBG_FORCE("FwdIndex::addRecord(): "<<latRec);
	}
	*/
	if (mActive)
	{
		FwdIndex::Record *pRec = mpDataEndptr;
		FwdIndex::Record *pRecOverlapping = NULL;

		bool bInsert = true; // will the given record be inserted?
	
		// if there are overlapping hypotheses with the same word in the meeting,
		// we will have only the best candidate (it's confidence and nodeID) in the index, 
		// but it will have time boundaries of the whole overlapping group.
		if (checkOverlapping)
		{
			while (--pRec >= mpData)
			{
	//			DBG_FORCE("mpData:"<<(int)mpData<<" pRec:"<<(int)pRec);

				// if there is nothing yet, just insert the record
				if (pRec == NULL)
				{
					break;
				}
/*
				if (pRec->mEndTime < latRec.mStartTime)
				{
					DBG_FORCE("pRec->mEndTime < latRec.mStartTime ... "<< pRec->mEndTime <<"<"<< latRec.mStartTime);
					break;
				}

				if (latRec.mWordID == 268931)
				{
					if (pRec->mEndTime < latRec.mStartTime)
					{
						DBG_FORCE("pRec->mEndTime < latRec.mStartTime ... "<< pRec->mEndTime <<"<"<< latRec.mStartTime);
						DBG_FORCE("BREAK");
						break;
					}
				}
*/				
				/*
				if (pRec->mWordID == latRec.mWordID && latRec.mWordID == 268931)
				{
					DBG_FORCE("--------------------------------------------------");
					DBG_FORCE("pRec:"<<*pRec);
					DBG_FORCE("latRec:"<<latRec);
					DBG_FORCE("is_overlapping:"<<is_overlapping(pRec->mStartTime, pRec->mEndTime, latRec.mStartTime, latRec.mEndTime));
				}
				*/
				// assume that the meetingID is the same
				if (pRec->mWordID == latRec.mWordID)
				{
					if(!is_overlapping(pRec->mStartTimeBoundary, pRec->mEndTimeBoundary, latRec.mStartTime, latRec.mEndTime))
					{
//						break;
					}
					else
					{
						/*
						if (latRec.mWordID == 268931)
						{
							DBG("Overlapping with pRec:"<<*pRec);
							DBG("pRecOverlapping: "<<pRecOverlapping);
						}
						*/
						if (pRecOverlapping == NULL)
						{
							pRec->Join(latRec);
							/*
							if (latRec.mWordID == 268931)
							{
								DBG("pRec->Join(latRec): "<<*pRec);
							}
							*/
							pRecOverlapping = pRec;
						}
						else
						{
							pRec->Join(pRecOverlapping);
							// remove the item pRecOverlapping from the mpData array
							mpDataEndptr--; // shorten the array by 1 item
							mDataSize--;
							*pRecOverlapping = *mpDataEndptr; // and move this item on the position of pRecOverlapping
							pRecOverlapping = pRec;
						}

						bInsert = false;
					}
				}
			}
		}
//		DBG_FORCE("LOOP FINISHED");
		if (bInsert)
		{
			*mpDataEndptr = latRec;
			mpDataEndptr++;
			mDataSize++;
		}

//		this->of.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));
	}
//	cout << ".";
}


/**
  @brief Close the opened forward index file
*/
void FwdIndex::close() 
{
	if (mActive)
	{
		this->of.close();
	}
}


void FwdIndex::sortarray() 
{
	DBG("FwdIndex::sortarray() mDataSize:"<<mDataSize);
	
	// SORT BY TIME
	mpSortIdxRecordsArray_byTime = new LatIndexer::RecordSortedByTime[mDataSize];
	DBG("RecordSortedByTime allocated");
	for (unsigned long long i=0; i<mDataSize; i++)
	{
		LatIndexer::RecordSortedByTime &rOut = mpSortIdxRecordsArray_byTime[i];
		FwdIndex::Record               &rIn  = mpData[i];

		rOut.mRecordIdx = i;
		rOut.mWordID    = rIn.mWordID;
		rOut.mMeetingID = rIn.mMeetingID;
//		rOut.nodeID     = rIn.nodeID;
		rOut.mConf      = rIn.mConf;
//		rOut.tStart     = rIn.tStart;
		rOut.mEndTime   = rIn.mEndTime;
	}
	DBG_FORCE("sorting by time...");
	assert((long long)mDataSize == (int)mDataSize);
	QuickSort(mpSortIdxRecordsArray_byTime, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by time...done");



	// SORT BY CONFIDENCE
	mpSortIdxRecordsArray_byConfidence = new LatIndexer::RecordSortedByConfidence[mDataSize];
	for (unsigned long long i=0; i<mDataSize; i++)
	{
		mpSortIdxRecordsArray_byConfidence[i].mRecordIdx = i;
		mpSortIdxRecordsArray_byConfidence[i].mWordID = mpData[i].mWordID;
//		mpSortIdxRecordsArray_byConfidence[i].meetingID = mpSortIdxRecordsArray_byTime[i].meetingID;
//		mpSortIdxRecordsArray_byConfidence[i].nodeID = mpSortIdxRecordsArray_byTime[i].nodeID;
		mpSortIdxRecordsArray_byConfidence[i].mConf = mpData[i].mConf;
//		mpSortIdxRecordsArray_byConfidence[i].tStart = mpSortIdxRecordsArray_byTime[i].tStart;
//		mpSortIdxRecordsArray_byConfidence[i].t = mpSortIdxRecordsArray_byTime[i].t;
	}
	DBG_FORCE("sorting by confidence...");
	QuickSort(mpSortIdxRecordsArray_byConfidence, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by confidence...done");


	mpData2ByConf = new unsigned long long[mDataSize];
	for (unsigned long long i=0; i<mDataSize; i++)
	{
		mpData2ByConf[mpSortIdxRecordsArray_byConfidence[i].mRecordIdx] = i;
	}
/*
	// SORT BY CONFIDENCE
	mpSortIdxRecordsArray_byConfidence = new LatIndexer::RecordSortedByConfidence[mDataSize];
	for (int i=0; i<mDataSize; i++)
	{
		mpSortIdxRecordsArray_byConfidence[i].recordIdx = i;
		mpSortIdxRecordsArray_byConfidence[i].wordID = mpSortIdxRecordsArray_byTime[i].wordID;
//		mpSortIdxRecordsArray_byConfidence[i].meetingID = mpSortIdxRecordsArray_byTime[i].meetingID;
//		mpSortIdxRecordsArray_byConfidence[i].nodeID = mpSortIdxRecordsArray_byTime[i].nodeID;
		mpSortIdxRecordsArray_byConfidence[i].conf = mpSortIdxRecordsArray_byTime[i].conf;
//		mpSortIdxRecordsArray_byConfidence[i].tStart = mpSortIdxRecordsArray_byTime[i].tStart;
//		mpSortIdxRecordsArray_byConfidence[i].t = mpSortIdxRecordsArray_byTime[i].t;
	}
	DBG_FORCE("sorting by confidence...");
	QuickSort(mpSortIdxRecordsArray_byConfidence, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by confidence...done");
*/

	// mapping looks like this now:
	//
	//   ByConf -> ByTime -> Data
	//
	// but we need this mapping:
	//
	//   ByTime -> ByConf -> Data
	//
	// so we need to do some remapping:
/*
	for (int i=0; i<mDataSize; i++)
	{
		LatIndexer::RecordIdx pom = mpSortIdxRecordsArray_byConfidence[i].recordIdx;
		mpSortIdxRecordsArray_byConfidence[i].recordIdx = mpSortIdxRecordsArray_byTime[mpSortIdxRecordsArray_byConfidence[i].recordIdx].recordIdx;
		mpSortIdxRecordsArray_byTime[pom].recordIdx = i;
	}
*/

/*	

	// sort the forward index to make it a reverse index sorted ***BY CONFIDENCE***
	DBG_FORCE("sorting by confidence...");
	QuickSort(this->mpData, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by confidence...done");
	
	// create an index structure for LatIndexer::Records sorted ***BY TIME***
	if (mpSortIdxRecordsArray_byTime != NULL)
	{
		delete[] mpSortIdxRecordsArray_byTime;
	}
	mpSortIdxRecordsArray_byTime = new LatIndexer::SortIdxRecord[mDataSize];
	ID_t wordID_cur = -1;
	int ptr_cur = 0;
	DBG_FORCE("mDataSize:"<<mDataSize);
	for (int i=0; i<mDataSize; i++)
	{
		if (mpData[i].wordID != wordID_cur)
		{
			DBG("wordID != wordID_cur ... "<<mpData[i].wordID<<" != "<<wordID_cur);
			ptr_cur = 0;
			wordID_cur = mpData[i].wordID;
		}
		else
		{
			DBG("wordID:"<<wordID_cur<<" once more");
		}

		mpSortIdxRecordsArray_byTime[i].recordIdx = ptr_cur++;
		mpSortIdxRecordsArray_byTime[i].t = mpData[i].t;
		mpSortIdxRecordsArray_byTime[i].wordID = mpData[i].wordID;
		mpSortIdxRecordsArray_byTime[i].meetingID = mpData[i].meetingID;
	}
	
//	for(int i=0; i<this->mDataSize; i++) 
//		DBG_FORCE("mpSortIdxRecordsArray_byTime[i].recordIdx:"<<mpSortIdxRecordsArray_byTime[i].recordIdx);
	

	DBG_FORCE("sorting by time...");
//	QuickSort(mpSortIdxRecordsArray_byTime, (int)0, (int)(mDataSize-1), LatIndexer::SortIdxRecord::CmpTime);
	QuickSort(mpSortIdxRecordsArray_byTime, (int)0, (int)(mDataSize-1));
	DBG_FORCE("sorting by time...done");
*/

}

//==================================================
// ALLOCATING MEMORY
//==================================================
void FwdIndex::initArray(unsigned long long allFilesSize) {
	this->mDataSize = (unsigned long long)(allFilesSize / sizeof(LatIndexer::Record));

	if (this->mpData != NULL) delete[] mpData;
	this->mpData = new FwdIndex::Record[this->mDataSize];
	this->mpDataEndptr = this->mpData; // point to the beginning of mpData's free space

	mDataRecordsCount = 0;
}

//==================================================
// APPEND FWD INDEX FILE TO MEMORY
//==================================================
int FwdIndex::appendFile(string p_filename) 
{
	ifstream in(p_filename.c_str(), ios::binary);
	if (!in.good()) {
		cerr << "ERROR: while reading fwd index file: " << p_filename << endl;
		exit(1);
	}

	//DBG_FORCE("starting (endptr = "<<this->mpDataEndptr<<")");
	// read all nodes
	while(!in.eof() && mDataRecordsCount < mDataSize) 
	{
		mpDataEndptr->ReadFrom(in);
		if (in.eof()) break;
		if (mpDataEndptr->mWordID == 0) {
			DBG_FORCE("WARNING: wordid==0 (endptr = "<<this->mpDataEndptr<<") ...skipping");
		} 
		else
		{
			this->mpDataEndptr++;
			mDataRecordsCount++;
		}
	}
//	this->mpDataEndptr--; // the last record was not actually there
	//DBG_FORCE("ending (endptr = "<<this->mpDataEndptr<<")");
	
	in.close();
	return 0;
}


int FwdIndex::save(string p_filename) 
{
	DBG_FORCE(endl<<endl<<"FwdIndex::save()"<<endl<<endl);
	// reverse index sorted by confidence
	ofstream out(p_filename.c_str(), ios::binary);
	if (out.bad()) {
		return (LSE_ERROR_IO);
	}


	if (mpSortIdxRecordsArray_byConfidence == NULL)
	{
		for(unsigned long long i=0; i<this->mDataSize; i++) {
			mpData[i].WriteTo(out);
		}
	}
	else
	{
		for(unsigned long long i=0; i<this->mDataSize; i++) {
			mpData[mpSortIdxRecordsArray_byConfidence[i].mRecordIdx].WriteTo(out);
		}
	}
	out.close();



	if (mpSortIdxRecordsArray_byTime != NULL)
	{
		// pointers to reverse index sorted by time
		out.open((p_filename + LATINDEXER_EXT_TIMESORT).c_str(), ios::binary);
		if (out.bad()) {
			return (LSE_ERROR_IO);
		}

		ID_t wordIdPred = -1;
		int conf_ptr_start = 0;
		int max_conf_idx = 0;

		for(unsigned long long i=0; i<mDataSize; i++) 
		{
			if (wordIdPred != mpData[mpSortIdxRecordsArray_byTime[i].mRecordIdx].mWordID)
			{
				//DBG("wordIdPred != mpData[].wordID ... "<<wordIdPred<<" != "<<mpData[mpSortIdxRecordsArray_byTime[i].mRecordIdx].mWordID);
				conf_ptr_start = i;
				wordIdPred = mpData[mpSortIdxRecordsArray_byTime[i].mRecordIdx].mWordID;
			}
			LatIndexer::RecordIdx conf_idx = mpData2ByConf[mpSortIdxRecordsArray_byTime[i].mRecordIdx] - conf_ptr_start;
			if (conf_idx > max_conf_idx) 
			{
				max_conf_idx = conf_idx;
			}

			//DBG("conf_idx = "<<mpData2ByConf[mpSortIdxRecordsArray_byTime[i].mRecordIdx]<<" - "<<conf_ptr_start<<" = "<<conf_idx);
			out.write(reinterpret_cast<const char *>(&conf_idx), sizeof(LatIndexer::RecordIdx));
		}
		out.close();
		//DBG_FORCE("max_conf_idx:"<<max_conf_idx);
	}

	return 0;
}


void FwdIndex::Print()
{
	DBG_FORCE("mDataSize: "<<mDataSize);
	for(unsigned long long i=0; i<this->mDataSize; i++) {
		DBG_FORCE("["<<i<<"]: " << this->mpData[i]);
	}
	DBG_FORCE("mDataSize: "<<mDataSize);
}


void FwdIndex::Record::Join(const LatIndexer::Record* pWith)
{
    // set the time boundary of the whole overlapped group
	mStartTimeBoundary = min(mStartTimeBoundary, pWith->mStartTime);
	mEndTimeBoundary   = max(mEndTimeBoundary  , pWith->mEndTime  );
	if (mConf < pWith->mConf)
	{
		mConf = pWith->mConf;
		mStartTime = pWith->mStartTime;
		mEndTime = pWith->mEndTime;
		mNodeID = pWith->mNodeID;
	}
}

