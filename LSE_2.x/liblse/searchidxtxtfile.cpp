#include "searchidxtxtfile.h"

using namespace std;
using namespace lse;

SearchIdxTxtFile::SearchIdxTxtFile()
{
	//Init(); // done in Open()
}

SearchIdxTxtFile::SearchIdxTxtFile(const string &filename)
{
	Open(filename);
}

void SearchIdxTxtFile::Open(const string &filename)
{
	Init();

	mFile.open(filename.c_str());

	if (!mFile.good()) {
		CERR("ERROR: opening file " << filename);
		exit (1);
	}
}

void SearchIdxTxtFile::Close()
{
	mFile.close();
}


bool SearchIdxTxtFile::IsEol(const char c)
{
	return (c == '\n');
}

bool SearchIdxTxtFile::IsDelimiter(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}


bool SearchIdxTxtFile::Next(SearchIdxTxtRecord *rec)
{
	static char c 	= 0;
	string 	s		= "";
	
	rec->Reset();

	while(!mFile.eof())
	{
		if (mEpsilon) 
		{
			mEpsilon = false;
		}
		else
		{
			mFile.get(c);
		}
		DBG("mState:"<<mState<<" c:"<<c<<" s:"<<s);

		switch (mState)
		{
			case 0: // read meetingID
					if (IsDelimiter(c))
					{
						rec->mMeetingID = atol(s.c_str());
						s = "";
						mState = 1;
					}
					else
					{
						s += c;
					}
				break;

			case 1: // skip whitespace
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 2;
					}
				break;

			case 2: // read wordID
					if (IsDelimiter(c))
					{
						rec->mWordID = atol(s.c_str());
						s = "";
						mState = 3;
					}
					else
					{
						s += c;
					}
				break;

			case 3: // skip whitespace
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 4;
					}
				break;

			case 4: // read confidence
					if (IsDelimiter(c))
					{
						rec->mConf = atof(s.c_str());
						s = "";
						mState = 5;
					}
					else
					{
						s += c;
					}
				break;

			case 5: // skip whitespace
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 6;
					}
				break;

			case 6: // read start time
					if (IsDelimiter(c))
					{
						rec->mStartTime = atof(s.c_str());
						s = "";
						mState = 7;
					}
					else
					{
						s += c;
					}
				break;

			case 7: // skip whitespace
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 8;
					}
				break;

			case 8: // read end time
					if (IsDelimiter(c))
					{
						rec->mEndTime = atof(s.c_str());
						s = "";
						mState = 9;
						mEpsilon = true;
					}
					else
					{
						s += c;
					}
				break;

			//--------------------------------------------------
			// the next part is optional - read a record index
			//--------------------------------------------------
			case 9: // skip whitespace
					if (IsEol(c))
					{
						mState = 0;
						return true;
					}
					else if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 10;
					}
				break;

			case 10: // read record index
					if (IsDelimiter(c))
					{
						rec->mRecordIdx = atol(s.c_str());
						s = "";
						mState = 11;
						mEpsilon = true;
					}
					else
					{
						s += c;
					}
				break;

			case 11: // skip till the end of the current line
					if (IsEol(c))
					{
						mState = 0;
						return true;
					}
				break;

		} // switch(mState)
	} // while !eof

	return false;
}


