#include "latindexer.h"

using namespace lse;
using namespace std;

/*
void LatIndexer::Record::WriteTo(unsigned char* ptr)
{
	*(reinterpret_cast<ID_t*>(ptr)) = mWordID;
	*(reinterpret_cast<ID_t*>(ptr)) = mMeetingID;
	*(reinterpret_cast<ID_t*>(ptr)) = mNodeID;
	*(reinterpret_cast<float*>(ptr)) = mConf;
	*(reinterpret_cast<LatTime*>(ptr)) = mStartTime;
	*(reinterpret_cast<LatTime*>(ptr)) = mEndTime;
}
*/

void LatIndexer::Record::WriteTo(std::ofstream &out)
{
/*
#ifdef FIX_32_BIT_LENGTH
	out.write(reinterpret_cast<const char *>(&mWordID),    4);
	out.write(reinterpret_cast<const char *>(&mMeetingID), 4);
	out.write(reinterpret_cast<const char *>(&mNodeID),    4);
	out.write(reinterpret_cast<const char *>(&mConf),      4);
	out.write(reinterpret_cast<const char *>(&mStartTime), 4);
	out.write(reinterpret_cast<const char *>(&mEndTime),   4);
#else
*/
	out.write(reinterpret_cast<const char *>(&mWordID),    sizeof(mWordID));
	out.write(reinterpret_cast<const char *>(&mMeetingID), sizeof(mMeetingID));
	out.write(reinterpret_cast<const char *>(&mNodeID),    sizeof(mNodeID));
	out.write(reinterpret_cast<const char *>(&mConf),      sizeof(mConf));
	out.write(reinterpret_cast<const char *>(&mStartTime), sizeof(mStartTime));
	out.write(reinterpret_cast<const char *>(&mEndTime),   sizeof(mEndTime));
//#endif
}

void LatIndexer::Record::ReadFrom(std::ifstream &in)
{
/*
#ifdef FIX_32_BIT_LENGTH
	in.read(reinterpret_cast<char *>(&mWordID),    4);
	in.read(reinterpret_cast<char *>(&mMeetingID), 4);
	in.read(reinterpret_cast<char *>(&mNodeID),    4);
	in.read(reinterpret_cast<char *>(&mConf),      4);
	in.read(reinterpret_cast<char *>(&mStartTime), 4);
	in.read(reinterpret_cast<char *>(&mEndTime),   4);
#else
*/
	in.read(reinterpret_cast<char *>(&mWordID),    sizeof(mWordID));
	in.read(reinterpret_cast<char *>(&mMeetingID), sizeof(mMeetingID));
	in.read(reinterpret_cast<char *>(&mNodeID),    sizeof(mNodeID));
	in.read(reinterpret_cast<char *>(&mConf),      sizeof(mConf));
	in.read(reinterpret_cast<char *>(&mStartTime), sizeof(mStartTime));
	in.read(reinterpret_cast<char *>(&mEndTime),   sizeof(mEndTime));
//#endif
}


int LatIndexer::Record::Size()
{
	return sizeof(mWordID) + sizeof(mMeetingID) + sizeof(mNodeID) + sizeof(mConf) + sizeof(mStartTime) + sizeof(mEndTime);
}

