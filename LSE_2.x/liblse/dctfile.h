#ifndef DCTFILE_H
#define DCTFILE_H

#include <fstream>
#include <cstdlib>
#include "lattypes.h"

namespace lse {

//--------------------------------------------------
//	DctFileRec
//--------------------------------------------------
class DctFileRec
{
public:
	std::string mWord;
	std::string mPron; ///< pronounciation

	void Reset()
	{
		mWord = "";
		mPron = "";
	}
};


//--------------------------------------------------
//	DctFile
//--------------------------------------------------
class DctFile
{
	std::ifstream 	mFile;
	int				mState;
	bool 			mEpsilon;

	bool IsEol(const char c);
	bool IsDelimiter(const char c);

public:

	DctFile();
	DctFile(const std::string &filename);

	void Open(const std::string &filename);
	bool Next(DctFileRec *rec);
	void Close();
};

} // namespace lse

#endif
