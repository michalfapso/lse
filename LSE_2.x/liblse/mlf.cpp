#include <iostream>
#include <iomanip>
#include <fstream>
#include <errno.h>
#include <cstdlib>
#include "mlf.h"

using namespace std;
using namespace lse;


void Mlf::SetMLFFileName(const std::string in_mlffilename)
{
	mlffilename = in_mlffilename;
}

void Mlf::Enable_STDOUT_Backoff()
{
	STDOUT_Backoff = true;
}

void Mlf::Disable_STDOUT_Backoff()
{
	STDOUT_Backoff = false;
}

void Mlf::AddRecord(MlfRec &rRec)
{
	this->push_back(rRec);
}

bool Mlf::IsEol(const char c)
{
	return (c == '\n');
}

bool Mlf::IsDelimiter(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

bool Mlf::Next(MlfRec *rec)
{
	static string 	record_path = "";
	static int		state = 0;

	string 	s		= "";
	char 	c		= 0;
	bool 	epsilon = false;

	rec->Reset();
	if (record_path != "") 
	{
		rec->mRecordPath = record_path;
	}

	while (! input_mFile->eof())
	{
		if (epsilon) 
		{
			epsilon = false;
		}
		else
		{
			input_mFile->get(c);
		}

		//		DBG_FORCE("c:"<<c<<" state:"<<state);
		switch (state)
		{
			case 0: // filename - starting "
				if (c == '"')
				{
					state = 1;
				}
				break;

			case 1: // filename - ending "
				if (c == '"')
				{
					record_path = s;
					rec->mRecordPath = s;
					s = "";
					state = 2;
				} 
				else
				{
					s += c;
				}
				break;

			case 2: // filename eol (end of line)
				if (IsEol(c))
				{
					state = 3;
				}
				break;

			case 3: // skip delimiters in front of the start time
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 4;
				}
				break;

			case 4:	// record - start time
				if (c == '.')
				{
					state = 0;
				}
				else if (IsDelimiter(c))
				{
					rec->mStartT = strtold(s.c_str(),NULL);
					if (errno == ERANGE) {
						std::cerr << "ERROR: strtod(\""<<s.c_str()<<"\") failed...errno=ERANGE" << std::endl;
						std::cerr << "loaded records: "<< this->size()  << std::endl;
						exit(1);
					}
					s = "";
					epsilon = true;
					state = 5;
				}
				else
				{
					s += c;
				}
				break;

			case 5: // skip delimiters between start time and end time
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 6;
				}
				else if (IsEol(c))
				{
					state = 3;
					return true;
				}
				break;

			case 6: // record - end time
				if (IsDelimiter(c))
				{
					rec->mEndT = strtold(s.c_str(),NULL);
					if (errno == ERANGE) {
						std::cerr << "ERROR: strtod(\""<<s.c_str()<<"\") failed...errno=ERANGE" << std::endl;
						std::cerr << "loaded records: "<< this->size() << std::endl;
						exit(1);
					}
					//						rec.mEndT = atof(s.c_str());
					s = "";
					epsilon = true;
					state = 7;
				}
				else
				{
					s += c;
				}
				break;

			case 7: // skip delimiters between end time and word
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 8;
				}
				else if (IsEol(c))
				{
					state = 3;
					return true;
				}
				break;

			case 8: // record - word
				if (IsDelimiter(c))
				{
					rec->mWord = s;
					s = "";
					epsilon = true;
					state = 9;
				}
				else
				{
					s += c;
				}
				break;

			case 9: // skip delimiters between word and score
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 10;
				}
				else if (IsEol(c))
				{
					state = 3;
					return true;
				}
				break;

			case 10: // record - confidence
				if (IsDelimiter(c))
				{
					rec->mConf = atof(s.c_str());
					s = "";
					epsilon = true;
					state = 11;
				}
				else
				{
					s += c;
				}
				break;

			case 11: // skip delimiters between score and end of line
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 12;
				} 
				else if (IsEol(c))
				{
					state = 3;
					return true;
				}
				break;

		} // switch (state)
	} // while (! mlf_file.eof())
	return false;
}


void Mlf::Open(const string filename)
{
	if( filename != "" ){
		mlffilename = filename;
	}

	if( (mlffilename == "") && STDOUT_Backoff ){
		input_mFile->rdbuf( std::cin.rdbuf() );
	}else{
		ifstream * pom;
		pom=new ifstream;
		pom->open(mlffilename.c_str());
		if ( !pom->good() ) {
			cerr << "Error: opening file " << mlffilename << endl;
			exit (1);
		}
		input_mFile->rdbuf(pom->rdbuf());
	}//if  

	this->clear();
}

void Mlf::Close()
{
	output_mFile->flush();
}

void Mlf::Load()
{
	MlfRec 	rec;
	while(Next(&rec)) {
		AddRecord(rec);
	}
}

void Mlf::SaveMLFHeader()
{
	(*output_mFile)<< "#!MLF!#" << endl;
	output_mFile->flush();

}

void Mlf::SaveLABHeader(const string LabName)
{
	if( WasLABbefore ){
		(*output_mFile) << "." << endl;
		WasLABbefore = false;
	}
	if( LabName != "" ){
		(*output_mFile) << "\"" << LabName << "\"" << endl;
		WasLABbefore = true;
	}
	output_mFile->flush();
}

void Mlf::SaveForAppend(const std::string filename)
{
	if( filename != "" ){
		mlffilename = filename;
	}

	if( (mlffilename == "") && STDOUT_Backoff ){
		//cerr << "Seting cout" << endl;
		output_mFile->rdbuf(std::cout.rdbuf());
	}else{
		pom_ofstream->open(mlffilename.c_str());
		if (!pom_ofstream->good()) {
			cerr << "Error: opening file " << mlffilename << endl;
			exit (1);
		}
		//cerr << "Seting file " << mlffilename << endl;
		output_mFile->rdbuf(pom_ofstream->rdbuf());
	}  
}

void Mlf::SaveLABLine(std::string &Line)
{
	(*output_mFile) << Line << endl;
	output_mFile->flush();
}

void Mlf::SaveRecord(MlfRec &rRec)
{
	(*output_mFile) << setprecision(20) << rRec.mStartT << " " << rRec.mEndT << " " << rRec.mWord << " " << rRec.mConf << endl;
	output_mFile->flush();
}

void Mlf::Save(const string filename)
{
	SaveForAppend(filename);
	SaveMLFHeader();

	string record_pred = "";
	for (Mlf::iterator i=begin(); i!=end(); ++i)
	{
		if (i->mRecordPath != record_pred)
		{
			SaveLABHeader( i->mRecordPath );
			record_pred = i->mRecordPath;
		}
		SaveRecord(*(i));
	}
	SaveLABHeader();
}

