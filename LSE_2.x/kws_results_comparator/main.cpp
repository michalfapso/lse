#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "determine_hits.h"
#include "detections_comparator.h"
#include "hypotheses.h"
#include "termlist.h"

using namespace std;
using namespace mlf;

//--------------------------------------------------------------------------------
#ifndef DBG
	#define DBG(str) std::cerr << std::dec << std::setprecision(13) << std::setw(20) << std::left << __FILE__ << ':' << std::setw(5) << std::right << __LINE__ << "> " << str << std::endl << std::flush
#endif
//#define DBG(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cerr << "Application takes filenames as parameters." << endl;
		return 1;
	}

	//application parameters
	char* p_mlf_ref = NULL;
//	char* p_mlf_res = NULL;
	char* p_termlist = NULL;
	float p_duration_seconds = 0;
	Hypothesis::IsHitFunPtr is_hit_fun_ptr = &Hypothesis::IsHit_point_middle_of_result;
	Mlf<Hypothesis> mlf_res1;
	Mlf<Hypothesis> mlf_res2;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-mlf-ref") == 0) {
			j++;
			p_mlf_ref = argv[j];
			
		} else if (strcmp(argv[j], "-mlf-res1") == 0) {
			j++;
			cerr << "Loading results MLF...";
			mlf_res1.Load(argv[j]);
			cerr << "done" << endl;
//			p_mlf_res = argv[j];
			
		} else if (strcmp(argv[j], "-mlf-res2") == 0) {
			j++;
			cerr << "Loading results MLF...";
			mlf_res2.Load(argv[j]);
			cerr << "done" << endl;
//			p_mlf_res = argv[j];
			
		} else if (strcmp(argv[j], "-termlist") == 0) {
			j++;
			p_termlist = argv[j];
			
		} else if (strcmp(argv[j], "-duration-seconds") == 0) {
			j++;
			p_duration_seconds = atof(argv[j]);
			cerr << "Setting duration to " << p_duration_seconds << " seconds." << endl;
			
		} else if (strcmp(argv[j], "-is-hit") == 0) {
			j++;
			is_hit_fun_ptr = Hypothesis::String2IsHitFunPtr(argv[j]);

		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				return 1;
			}
		}
		j++;
	}

	if (!p_mlf_ref || mlf_res1.empty()  || mlf_res2.empty() || !p_termlist || !p_duration_seconds) {
		cerr << "Usage: "<<argv[0]<<" -mlf-ref reference.mlf -mlf-res1 hypotheses1.mlf -mlf-res2 hypotheses1.mlf -termlist termlist.txt" << endl;
		return 1;
	}

	float durationHours = p_duration_seconds/3600;

	cerr << "Loading reference MLF...";
	Mlf<ReferenceMlfRecord>             mlf_ref(p_mlf_ref);
	cerr << "done" << endl;

	DBG("mlf_res1 size: "<<mlf_res1.size());
	DBG("mlf_res2 size: "<<mlf_res2.size());

	MlfWord2Records<ReferenceMlfRecord> w2r_ref(mlf_ref);
	MlfWord2Records<Hypothesis>         w2r_res1(mlf_res1);
	MlfWord2Records<Hypothesis>         w2r_res2(mlf_res2);
	Termlist                            termlist(p_termlist);

	DetermineHits::Process(w2r_ref, w2r_res1, is_hit_fun_ptr);
	DetermineHits::Process(w2r_ref, w2r_res2, is_hit_fun_ptr);

	DetectionsComparator::Compare(w2r_ref, w2r_res1, w2r_res2, is_hit_fun_ptr);
//	DetermineHits::CheckMultipleReferenceHits(w2r_ref);

//	FomScorer::ComputeFom    (w2r_ref, w2r_res, termlist, durationHours);
//	scorer.ComputeFomPerFile (w2r_ref, w2r_res, termlist, durationHours);
//	scorer.ComputeFomWallace (w2r_ref, w2r_res, termlist, durationHours);

	return 0;
}


