#include "lattice_cutter.h"
#include <algorithm>
#include <stdexcept>

using namespace std;
using namespace lse;

void LatticeCutter::Cut(float startTime, float endTime, Lattice *out)
{
	//CERR("endNodeId: "<<mEndNodeId);
	//CERR("startNodeId: "<<mStartNodeId);
	mNodesMap.clear();
	map<ID_t, bool> start_nodes;
	map<ID_t, bool> end_nodes;

	// delete links
	for (Lattice::Links::iterator il=mpLat->links.begin(); il!=mpLat->links.end(); ++il) {
		Lattice::Link *l = *il;
		Lattice::Node *S = mpLat->nodes[l->S];
		Lattice::Node *E = mpLat->nodes[l->E];

		// S - before interval, E - within or after interval
		if (S->t < startTime && E->t >= startTime/* && E->t <= endTime*/) {
			// Add the link only if the end node is not yet in the output lattice
			//DBG("interval_check: S-before("<<S->t<<") E-within-or-after("<<E->t<<")");
			if (start_nodes.find(E->id) == start_nodes.end()) {
				AddLink(out, mStartNodeId, E->id, mpFwbw->fwds[E->id], 0);
				start_nodes[E->id] = true;
				if (E->t > endTime) {
					// Set the correct end time of after-nodes
					out->nodes[mNodesMap[E->id]]->t = endTime;
					// Connect the after-node to the final node
					AddLink(out, E->id, mEndNodeId, mpFwbw->bwds[E->id], 0);
				}
			}
		} 
		// S - within interval, E - after interval
		else if (S->t >= startTime && S->t <= endTime && E->t > endTime) {
			// Add the link only if the start node is not yet in the output lattice
			//DBG("interval_check: S-within("<<S->t<<") E-after("<<E->t<<")");
			if (end_nodes.find(S->id) == end_nodes.end()) {
				AddLink(out, S->id, mEndNodeId, mpFwbw->bwds[S->id], 0);
				end_nodes[S->id] = true;
			}
		}
		// Both within interval
		else if (S->t >= startTime && E->t <= endTime) {
			//AddLink(out, S->id, E->id, l->confidence_noScaleOnKeyword);
			AddLink(out, S->id, E->id, l->a, l->l);
		}
	}

	// Set the correct end time of the first and last node
	out->nodes[0]->t = startTime;
	//out->nodes[out->nodes.size()-1]->t = endTime;
	out->nodes[mNodesMap[mEndNodeId]]->t = endTime;

	// If the final node does not have the highest ID, resort nodes
	if (mNodesMap[mEndNodeId] != (int)out->nodes.size()-1)
	{
		ID_t final_prev = mNodesMap[mEndNodeId];
		ID_t final_cur = out->nodes.size()-1;
		swap(out->nodes[final_prev], out->nodes[final_cur]);
		out->nodes[final_prev]->id = final_prev;
		out->nodes[final_cur]->id  = final_cur;
		// Now for links
		for (unsigned int i=0; i<out->links.size(); i++) {
			Lattice::Link* l = out->links[i];
			if (l->S == final_prev) {
				l->S = final_cur;
			} else if (l->S == final_cur) {
				l->S = final_prev;
			}
			if (l->E == final_prev) {
				l->E = final_cur;
			} else if (l->E == final_cur) {
				l->E = final_prev;
			}
		}
	}

	// subtract startTime from each node's time
	for (unsigned int i=0; i<out->nodes.size(); i++) {
		Lattice::Node* n = out->nodes[i];
		n->t = (float)((int)(n->t * 100) - (int)(startTime * 100)) / 100; // convert time to frames to avoid quantization issues
	}

	out->L = out->links.size();
	out->N = out->nodes.size();
}

void LatticeCutter::AddLink(Lattice *pOut, ID_t startNodeId, ID_t endNodeId, TLatViterbiLikelihood acousticLik, TLatViterbiLikelihood languageLik)
{
	Lattice::Link *l = new Lattice::Link;
	l->id = pOut->links.size();
	l->S = AddNode(pOut, startNodeId);
	l->E = AddNode(pOut, endNodeId);
	l->a = acousticLik;
	l->l = languageLik;
	pOut->links.push_back(l);
}

ID_t LatticeCutter::AddNode(Lattice *pOut, ID_t nodeId, float forceEndTime)
{
	NodesMap::iterator it = mNodesMap.find(nodeId);
	// if the node is already added, just return its ID
	if (it != mNodesMap.end()) {
		return it->second;
	} else {
		Lattice::Node *n = new Lattice::Node;
		Lattice::Node *nOld = mpLat->nodes[nodeId];
		n->id = mNodesMap.size();
		n->t = forceEndTime == -1 ? nOld->t : forceEndTime;
		n->W = nOld->W;
		n->W_id = nOld->W_id;
		pOut->nodes.push_back(n);
		mNodesMap[nodeId] = n->id;
		return n->id;
	}
}
/*
float LatticeCutter::GetCutLikelihood(float startTime, float endTime, const string& filename_lik_out)
{
	int frames_count = ((endTime - startTime)*100);
	float lik_pre = mpLat->nodes[0]->beta;
	float lik_post = mpLat->nodes[0]->beta;

	// delete links
	for (Lattice::Links::iterator il=mpLat->links.begin(); il!=mpLat->links.end(); ++il) {
		Lattice::Link *l = *il;
		Lattice::Node *S = mpLat->nodes[l->S];
		Lattice::Node *E = mpLat->nodes[l->E];

		if (S->t < startTime && E->t >= startTime) {
			if (lik_pre < mpFwbw->fwds[E->id]) {
				lik_pre = mpFwbw->fwds[E->id];
			}
		}
		else if (S->t >= startTime && S->t <= endTime && E->t > endTime) {
			if (lik_post < mpFwbw->bwds[S->id]) {
				lik_post = mpFwbw->bwds[S->id];
			}
		}
	}

	float cut_likelihood = mpLat->nodes[0]->beta - lik_pre - lik_post;
	//cout << mpLat->nodes[0]->beta <<" - "<< lik_pre <<" - "<< lik_post << endl;
	//cout << "cut_likelihood: "<<cut_likelihood << endl;
	cut_likelihood /= frames_count;
	//cout << "cut_likelihood per frame: "<<cut_likelihood<< endl;

	// write the likelihood to the output file
	ofstream f(filename_lik_out.c_str());
	if (!f.good()) {
		throw runtime_error("Can not open file '"+filename_lik_out+"'");
	}
	f << cut_likelihood << endl;
	f.close();

	return cut_likelihood;
}
*/

float LatticeCutter::GetCutLikelihood(float startTime, float endTime, const string& filename_lik_out)
{
	int frames_count = ((endTime - startTime)*100);

	float* lik_logadd = new float[frames_count];
	for (int i=0; i<frames_count; i++) { lik_logadd[i] = 0; }

	float* lik_best = new float[frames_count];
	for (int i=0; i<frames_count; i++) { lik_best[i] = -INF; }

	for (Lattice::Links::iterator il=mpLat->links.begin(); il!=mpLat->links.end(); ++il) {
		Lattice::Link *l = *il;
		Lattice::Node *S = mpLat->nodes[l->S];
		Lattice::Node *E = mpLat->nodes[l->E];

		int frame_start = (S->t - startTime)*100;
		int frame_end   = (E->t - startTime)*100;
		int link_length = frame_end - frame_start;

		if (frame_start < 0) {
			frame_start = 0;
		}
		if (frame_end > frames_count) {
			frame_end = frames_count;
		}

		if (frame_end - frame_start > 0) {
			float lik_per_frame = (l->a * mpLat->amscale + l->l * mpLat->lmscale) / link_length;
			DBG("lik_per_frame: "<<lik_per_frame);
			for (int i=frame_start; i<frame_end; i++) {
				//lik_logadd[i] = logAdd(lik_logadd[i], lik_per_frame);
				lik_logadd[i] = log(exp(lik_logadd[i]) + exp(lik_per_frame));
				if (lik_best[i] < lik_per_frame) {
					lik_best[i] = lik_per_frame;
				}
			}
		}
	}

	ofstream f(filename_lik_out.c_str());
	if (!f.good()) {
		throw runtime_error("Can not open file '"+filename_lik_out+"'");
	}

	float avg_lik_logadd = 0.0f;
	for (int i=0; i<frames_count; i++) { 
		DBG("lik_logadd["<<i<<"]: "<<lik_logadd[i]);
		f << "lik_logadd#"<<i<<" "<<lik_logadd[i] << endl;
		avg_lik_logadd += lik_logadd[i];
	}
	for (int i=0; i<frames_count; i++) { 
		DBG("lik_best["<<i<<"]: "<<lik_best[i]);
		f << "lik_best#"<<i<<" "<<lik_best[i] << endl;
	}
	avg_lik_logadd /= frames_count;

	//f << avg_lik_logadd << endl;

	f.close();

	return avg_lik_logadd;
}

