/*********************************************************************************
 *
 *	Application for converting HTK Standard Lattice File (SLF) to binary file
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "latbinfile.h"
#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
//#include "hypothesis.h"
#include "lexicon.h"
#include "confmeasure.h"

#include "lattice_cutter.h"


using namespace std;
using namespace lse;

//--------------------------------------------------------------------------------
#define DBG(str) DBG_FORCE(str)
//#define DBG(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cerr << "Usage: "<<argv[0]<<" [flags] lattice_in.lat" << endl;
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer indexer;

	//application parameters
	char* p_dot_out = NULL;
	char* p_htk_out = NULL;
	char* p_htk_out_dir = NULL;
	char* p_fwbw_out = NULL;
	bool p_htk_posteriors_output = false;
	bool p_get_best_path = false;

	float p_wi_penalty = 0.0;
	float p_lmscale = 1;
	float p_amscale = 1;
	float p_amscale_kwd = 0.0;
	float p_lmscale_kwd = 0.0;
	float p_posterior_scale = 1.0f;

	bool p_viterbi_baumwelsh = false;
	float p_logAdd_treshold = 7;
	bool p_cut_time_interval = false; 
	float p_cut_t_start = 0;
	float p_cut_t_end = 0;

	ConfidenceMeasure* p_confidence_measure = NULL;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-htk-out") == 0) {
			j++;
			p_htk_out = argv[j];
			
		} else if (strcmp(argv[j], "-htk-out-dir") == 0) {
			j++;
			p_htk_out_dir = argv[j];
			
		} else if (strcmp(argv[j], "-dot-out") == 0) {
			j++;
			p_dot_out = argv[j];
			
		} else if (strcmp(argv[j], "-fwbw-out") == 0) {
			j++;
			p_fwbw_out = argv[j];
			
		} else if (strcmp(argv[j], "-wi-penalty") == 0) {
			j++;
			p_wi_penalty = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-lmscale") == 0) {
			j++;
			p_lmscale = atof(argv[j]);
			DBG_FORCE("lmscale: "<<p_lmscale);

		} else if (strcmp(argv[j], "-amscale") == 0) {
			j++;
			p_amscale = atof(argv[j]);
			DBG_FORCE("amscale: "<<p_amscale);

		} else if (strcmp(argv[j], "-viterbi-fwbw") == 0) {
			p_viterbi_baumwelsh = false;

		} else if (strcmp(argv[j], "-baumwelsh-fwbw") == 0) {
			p_viterbi_baumwelsh = true;

		} else if (strcmp(argv[j], "-cut-time-interval") == 0) {
			p_cut_time_interval = true; 
			j++;
			p_cut_t_start = atof(argv[j]);
			j++;
			p_cut_t_end = atof(argv[j]);
			
		} else if (strcmp(argv[j], "-confidence-measure") == 0) {
			j++;
			if (strcmp(argv[j], "cmax") == 0)
			{
				p_confidence_measure = new ConfidenceMeasure_CMax;
			}
			else if (strcmp(argv[j], "logadd_overlapping") == 0)
			{
				p_confidence_measure = new ConfidenceMeasure_LogaddOverlapping;
			}

		} else if (strcmp(argv[j], "-posterior-scale") == 0) {
			j++;
			p_posterior_scale = atof(argv[j]);

		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		j++;
	}

	string filename = argv[j];

	DBG_FORCE("Processing file: "<<filename);
	if (!file_exists(filename.c_str()))
	{
		CERR("ERROR: file does not exist: "<<filename);
		EXIT();
	}
	
	string latname = filename;

	Lattice lat(&lexicon);
	lat.amscale = p_amscale;
	lat.lmscale = p_lmscale;

	//==================================================
	// READ HTK LATTICE
	//==================================================
	DBG( "reading...");
	if(lat.loadFromHTKFile(filename) != 0) {
		CERR("Error occured while reading lattice file " << filename);
		exit (1);
	}
	DBG( "done\t");
	//==================================================
	// SORT LATTICE
	//==================================================
	bool sorted = true;
	for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
		if ((*l)->S > (*l)->E) {
			sorted = false;
			break;
		}
	}

	if (!sorted) {
		DBG("WARNING: Lattice is not sorted -> sorting...");
		lat.sortLattice();
		DBG("WARNING: Lattice is not sorted -> sorting...done");
	}
	
	//==================================================
	// COMPUTE LINKS LIKELIHOOD (POSTERIORS)
	//==================================================
	DBG("compute links likelihood...");
	LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
	vit.computeFwViterbi();
	vit.computeBwViterbi();
	//vit.computeLinksLikelihood();
	if (p_fwbw_out) 
		vit.writeViterbiFwBw(p_fwbw_out);
	DBG("done   ");

	if (p_confidence_measure != NULL)
	{
		DBG("compute confidence measure...");
		p_confidence_measure->ComputeLinksConfidence(&lat);
		DBG("done");
	}

	//==================================================
	// TRIM TIME
	//==================================================
	if (p_cut_time_interval) 
	{
		Lattice cut(lat.lexicon);

		LatticeCutter cutter(lat, vit);
		cutter.Cut(p_cut_t_start, p_cut_t_end, &cut);

		cutter.GetCutLikelihood(p_cut_t_start, p_cut_t_end, string(p_htk_out) + ".lik");

		LatViterbiFwBw vitcut(&cut, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
		vitcut.computeFwViterbi();
		vitcut.computeBwViterbi();
		vitcut.computeLinksLikelihood();

		if (p_htk_out) {
			cut.saveToHTKFile(p_htk_out, p_htk_posteriors_output, false);
		}
	} else {
		// read cut intervals from stdin
		cout << "Reading records [start_time end_time id] from the standard input..." << endl;
		try {
			string input_line;
			while(cin) {
				// getline(cin, input_line);
				float cut_time_start;
				float cut_time_end;
				string cut_id;

				//DBG("read 1");
				cin >> cut_time_start;
				if (!cin) { break; }
				//DBG("read 2");
				cin >> cut_time_end;
				//DBG("read 3");
				cin >> cut_id;
				//DBG("after read");

				ostringstream out_filename;
				out_filename << p_htk_out_dir << "/" << cut_time_start*100 << "_" << cut_time_end*100 << "_" << cut_id << ".lat";
				cout << "cut["<<cut_time_start<<".."<<cut_time_end<<"] -> "<<out_filename.str() << endl;

				Lattice cut(lat.lexicon);

				LatticeCutter cutter(lat, vit);
				cutter.Cut(cut_time_start, cut_time_end, &cut);

				cutter.GetCutLikelihood(cut_time_start, cut_time_end, out_filename.str() + ".lik");

				LatViterbiFwBw vitcut(&cut, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
				vitcut.computeFwViterbi();
				vitcut.computeBwViterbi();
				vitcut.computeLinksLikelihood();

				if (p_htk_out_dir) {
					cut.saveToHTKFile(out_filename.str(), p_htk_posteriors_output, false);
				}
			}
		} catch (ifstream::failure fail) {
			cout << "stdin read error: " << fail.what();
		}
	}


	//==================================================
	// GET BEST PATH
	//==================================================
	if (p_get_best_path) 
	{
		vector<ID_t> path;
		ID_t first_node_id = -1;
		lat.GetBestPath(&path, &first_node_id);

		cout << "BEST PATH: " << endl;
		for (vector<ID_t>::iterator i=path.begin(); i!=path.end(); ++i)
		{
			cout << " --("<<lat.links[*i]->confidence_noScaleOnKeyword<<")--> " << lat.links[*i]->E;
		}
		cout << endl;
	}

	
	//==================================================
	// HTK OUTPUT
	//==================================================
/*	if (p_htk_out) {
		DBG("htk-export (" << p_htk_out << ")...");
		lat.saveToHTKFile(p_htk_out, p_htk_posteriors_output, false); // COPY OF LATTICE		
		DBG("done\t");
	}
*/
	//==================================================
	// DOT OUTPUT
	//==================================================
	if (p_dot_out) {
		DBG("dot-export...");
		lat.saveToDotFile(p_dot_out);
		DBG("done\t");
	}
	
	DBG( endl << "***** OPERATION COMPLETED *****" << endl);
	if (p_confidence_measure)
		delete p_confidence_measure;
	return (0);
}


