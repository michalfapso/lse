#ifndef LATTICE_CUTTER_H
#define LATTICE_CUTTER_H

#include <map>
#include "lat.h"
#include "latviterbifwbw.h"

namespace lse {
class LatticeCutter {
	public:
		LatticeCutter(Lattice &lat, LatViterbiFwBw &fwbw) : mpLat(&lat), mpFwbw(&fwbw) {
			mEndNodeId = mpLat->getEndNode();
			mStartNodeId = mpLat->firstNode()->id;
		}
		void Cut(float startTime, float endTime, Lattice *out);
		float GetCutLikelihood(float startTime, float endTime, const std::string& filename_lik_out);
	private:
		Lattice *mpLat;
		LatViterbiFwBw *mpFwbw;
		ID_t mEndNodeId;
		ID_t mStartNodeId;

		typedef std::map<ID_t, ID_t> NodesMap;
		NodesMap mNodesMap;

		void AddLink(Lattice *pOut, ID_t startNodeId, ID_t endNodeId, TLatViterbiLikelihood acousticLik, TLatViterbiLikelihood languageLik);
		ID_t AddNode(Lattice *pOut, ID_t nodeId, float forceEndTime = -1);
};
}

#endif
