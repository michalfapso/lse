/*********************************************************************************
 *
 *	Application for converting HTK Standard Lattice File (SLF) to binary file
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include "latbinfile.h"
#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
//#include "hypothesis.h"
#include "lexicon.h"
#include "confmeasure.h"


#define TIMER_START	dbgTimer.start();
#define TIMER_END(msg)	dbgTimer.end();	DBG_FORCE("TIMER["<<msg<<"]: "<<dbgTimer.val());

using namespace std;
using namespace lse;

static const string ext_dot = ".dot";
static const string ext_bin = ".binlat";
static const string ext_copy = ".lat.copy";
static const string ext_htk = ".lat";
static const string ext_alphabetavectors = ".abvec";

Timer dbgIndexingTimer;
Timer dbgTimer;

//--------------------------------------------------------------------------------

#define DBG_MAIN(str) if (p_debug_level > 1) CERR(str)
//#define DBG_MAIN(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes filenames as parameters.";
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer indexer;
	Lattice globalLat(lexicon, indexer); // concatenation of lattices given by application arguments
	LatBinFile latBinFile;
	LatBinFile latBinFileConcat;
	globalLat.N = 1;
	globalLat.L = 0;
	Lattice::Node firstNode;
	firstNode.id=0;
	firstNode.t=0.0;
	firstNode.W="!NULL";
	firstNode.v=0;
	firstNode.bestLikelihood=0.0;
	globalLat.addNode(firstNode); // add empty node

	//application parameters
	bool p_read_binary = false;
	bool p_write_binary = false;
	bool p_write_binary_gzipped = false;
	bool p_write_dot = false;
	bool p_write_htk = false;
	bool p_sort_lattice = false;
	bool p_compute_fwbw = false;
	bool p_make_one_end_node = false;
	string p_lattice_dir_out = ".";
	string p_lexicon_out = "";
	string p_lexicon_in = "";
	bool p_concatlattice_out = false;
	string p_concatlattice_out_filename = "";
	string p_document_name = "";
	string p_meeting_indexer_out = "";
	string p_meeting_indexer_in = "";
	string p_search_index = "";
	string p_search_indexer_in = "";
	string p_generate_wordID_search_index = "";
	string p_data_dir_prefix = "";
	float p_wi_penalty = 0.0;
	string p_mlf_out = "";
	string p_mlf_kwdlist = "";
	float p_mlf_likelihood_treshold = -1; // no treshold is set
	float p_lmscale = 1;
	float p_amscale = 1;
	float p_amscale_kwd = 0.0;
	float p_lmscale_kwd = 0.0;
	string p_latlist_in_file = "";
	bool p_viterbi_baumwelsh = false;
	bool p_compute_links_likelihood = false;
	float p_logAdd_treshold = 7;
	bool p_trim_time_interval = false; 
	float p_trim_t_start = 0;
	float p_trim_t_end = 0;
	bool p_compute_overlapped_words_likelihood = false;
	bool p_likelihood_no_scale_on_keyword = false;
	bool p_check_node_sorting = false;
	int p_debug_level = 0;
	bool p_do_not_add_to_lexicon = false;
//	bool p_input_SRI_lattices = false;
	bool p_htk_posteriors_output = false;
	bool p_read_time_from_filename = false;
	bool p_print_lexicon = false;
	bool p_bin2txt = false;
	string p_generate_lexicon_index = "";
	float p_pruning_posterior_treshold = -INF;
	bool p_do_pruning_posterior_treshold = false;
	bool p_remove_null_nodes = false;
	bool p_generate_best_path_nodes_from_nodes_file = false;
	bool p_write_viterbi = false;
	bool p_only_add_words_to_lexicon = false;
	bool p_readonly_lexicon = false;
	long p_lat_time_multiplier = 1;
	char p_lat_filename_delimiter = '_';
	int p_lat_filename_start_time_field = 0;
	int p_latname_time_multiplier = 100; // frames
	bool p_get_best_path = false;
	string p_generate_label_file = "";
//	bool p_transform2ngram = false;
	bool p_output_alpha_beta_vectors = false;
	string p_write_ngrams = "";
	int p_ngrams_size = 3;
	bool p_generate_ngram_index = false;
	string p_indexed_document = "";
	string p_log = "";
	bool p_htk_remove_backslash = false;
	float p_posterior_scale = 1.0f;

	ConfidenceMeasure* p_confidence_measure = NULL;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-read-binary") == 0) {

			p_read_binary = true;
			
		} else if (strcmp(argv[j], "-write-htk") == 0) {
			
			p_write_htk = true;
			
		} else if (strcmp(argv[j], "-write-binary") == 0) {
			
			p_write_binary = true;
			
		} else if (strcmp(argv[j], "-sort-lattice") == 0) {
			
			p_sort_lattice = true;
			
		} else if (strcmp(argv[j], "-write-binary-gzipped") == 0) {
			
			p_write_binary_gzipped = true;
			
		} else if (strcmp(argv[j], "-lexicon-out") == 0) {

			j++;
			p_lexicon_out = argv[j];
			
		} else if (strcmp(argv[j], "-lexicon-in") == 0) {

			j++;
			p_lexicon_in = argv[j];
			
		} else if (strcmp(argv[j], "-only-add-words-to-lexicon") == 0) {

			p_only_add_words_to_lexicon = true;

		} else if (strcmp(argv[j], "-readonly-lexicon") == 0) {
			
			p_readonly_lexicon = true;
			lexicon.isReadonly = true;
			p_do_not_add_to_lexicon = true;
			
		} else if (strcmp(argv[j], "-print-lexicon") == 0) {

			p_print_lexicon = true;			
			
		} else if (strcmp(argv[j], "-generate-lexicon-index") == 0) {
			
			j++;
			p_generate_lexicon_index = argv[j];

		} else if (strcmp(argv[j], "-concatlattice-out") == 0) {

			p_concatlattice_out = true;

		} else if (strcmp(argv[j], "-document-name") == 0) {

			j++;
			p_document_name = argv[j];
			
		} else if (strcmp(argv[j], "-write-dot") == 0) {

			p_write_dot = true;
			
		} else if (strcmp(argv[j], "-meeting-indexer-out") == 0) {

			j++;
			p_meeting_indexer_out = argv[j];

		} else if (strcmp(argv[j], "-search-indexer-in") == 0 ||
					strcmp(argv[j], "-search-indexer-out") == 0 ||
					strcmp(argv[j], "-search-index") == 0) {

			j++;
			p_search_index = argv[j];
			DBG_MAIN("Setting search index file: " << p_search_index);
			if (indexer.fwdIndex.create(p_search_index) != 0) {
				CERR("ERROR: Creating forward index file: " << p_search_index);
				exit(1);
			}
			/*
			DBG_MAIN( "Loading lattices indices " << argv[j] << "..." << flush);
			if (indexer.lattices.loadFromFile(argv[j]) != 0) {
				DBG_MAIN( "Lattices indexer input file " << argv[j] << " not found ... using empty indexer" << endl << flush);
			}
			*/
			DBG_MAIN( "done" << endl << flush);
		
		} else if (strcmp(argv[j], "-generate-wordid-search-index") == 0) {
			
			j++;
			p_generate_wordID_search_index = argv[j];
			
		} else if (strcmp(argv[j], "-meeting-indexer-in") == 0) {

			j++;
			DBG_MAIN( "Loading meetings indexes " << argv[j] << "..." << flush);
			if (indexer.meetings.loadFromFile(argv[j]) != 0) {
				DBG_MAIN( "Meetings indexer input file " << argv[j] << " not found ... using empty indexer" << endl << flush);
			}
			//DBG_FORCE("indexer.meetings.print()"); indexer.meetings.print();
			DBG_MAIN( "done" << endl << flush);
		
		} else if (strcmp(argv[j], "-data-dir-prefix") == 0) {

			j++;
			p_data_dir_prefix = argv[j];

		} else if (strcmp(argv[j], "-compute-fwbw") == 0) {

			p_compute_fwbw = true;

		} else if (strcmp(argv[j], "-wi-penalty") == 0) {
			
			j++;
			p_wi_penalty = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-mlf-out") == 0) {

			j++;
			p_mlf_out = argv[j];

			ofstream mkfile(p_mlf_out.c_str()); // create MLF file with header !!! overwrite if exists
			mkfile << "#!MLF!#" << endl;
			mkfile.close();

		} else if (strcmp(argv[j], "-mlf-kwdlist") == 0) {

			j++;
			p_mlf_kwdlist = argv[j];
				
		} else if (strcmp(argv[j], "-mlf-likelihood-treshold") == 0) {

			j++;
			p_mlf_likelihood_treshold = atof(argv[j]);

		} else if (strcmp(argv[j], "-lmscale") == 0) {
			j++;
			p_lmscale = atof(argv[j]);
			DBG_FORCE("lmscale: "<<p_lmscale);

		} else if (strcmp(argv[j], "-amscale") == 0) {
			j++;
			p_amscale = atof(argv[j]);
			DBG_FORCE("amscale: "<<p_amscale);

		} else if (strcmp(argv[j], "-amscale-kwd") == 0) {
			j++;
			p_amscale_kwd = atof(argv[j]);
			DBG_FORCE("amscale-kwd: "<<p_amscale_kwd);

		} else if (strcmp(argv[j], "-lmscale-kwd") == 0) {
			j++;
			p_lmscale_kwd = atof(argv[j]);
			DBG_FORCE("lmscale-kwd: "<<p_lmscale_kwd);

		} else if (strcmp(argv[j], "-make-one-end-node") == 0) {
			p_make_one_end_node = true;
			
		} else if (strcmp(argv[j], "-latlist-in") == 0) {
			j++;			
			p_latlist_in_file = argv[j];

		} else if (strcmp(argv[j], "-lattice-dir-out") == 0) {
			j++;
			p_lattice_dir_out = argv[j];
			
		} else if (strcmp(argv[j], "-viterbi-fwbw") == 0) {
			p_viterbi_baumwelsh = false;

		} else if (strcmp(argv[j], "-baumwelsh-fwbw") == 0) {
			p_viterbi_baumwelsh = true;

		} else if (strcmp(argv[j], "-compute-links-likelihood") == 0) {
			p_compute_links_likelihood = true;

		} else if (strcmp(argv[j], "-logadd-treshold") == 0) {
			j++;
			p_logAdd_treshold = atof(argv[j]);

		} else if (strcmp(argv[j], "-trim-time-interval") == 0) {
			p_trim_time_interval = true; 
			j++;
			p_trim_t_start = atof(argv[j]);
			j++;
			p_trim_t_end = atof(argv[j]);
			
		} else if (strcmp(argv[j], "-compute-overlapped-words-likelihood") == 0) {
			p_compute_overlapped_words_likelihood = true;

		} else if (strcmp(argv[j], "-likelihood-no-scale-on-keyword") == 0) {
			p_likelihood_no_scale_on_keyword = true;
						
		} else if (strcmp(argv[j], "-check-node-sorting") == 0) {
			p_check_node_sorting = true;
			
		} else if (strcmp(argv[j], "-debug-level") == 0) {
			j++;
			p_debug_level = atoi(argv[j]);
			
		} else if (strcmp(argv[j], "-do-not-add-to-lexicon") == 0) {
			p_do_not_add_to_lexicon	= true;
/*			
		} else if (strcmp(argv[j], "-input-SRI-lattices") == 0) {
			p_input_SRI_lattices = true;
*/		
		} else if (strcmp(argv[j], "-htk-posteriors-output") == 0) {
			p_htk_posteriors_output = true;
			
		} else if (strcmp(argv[j], "-read-time-from-filename") == 0) {
			p_read_time_from_filename = true;

		} else if (strcmp(argv[j], "-dont-read-time-from-filename") == 0) {
			p_read_time_from_filename = false;
			
		} else if (strcmp(argv[j], "-latname-time-multiplier") == 0) {
			j++;
			p_latname_time_multiplier = atol(argv[j]);
			
		} else if (strcmp(argv[j], "-bin2txt") == 0) {
			p_bin2txt = true;

		} else if (strcmp(argv[j], "-pruning-posterior-treshold") == 0) {
			j++;
			p_pruning_posterior_treshold = atof(argv[j]);
			p_do_pruning_posterior_treshold = true;

		} else if (strcmp(argv[j], "-remove-null-nodes") == 0) {
			p_remove_null_nodes = true;
				
		} else if (strcmp(argv[j], "-generate-best-path-nodes-from-nodes-file") == 0) {
			p_generate_best_path_nodes_from_nodes_file = true;
			
		} else if (strcmp(argv[j], "-write-viterbi") == 0) {
			p_write_viterbi = true;
			
		} else if (strcmp(argv[j], "-lat-time-multiplier") == 0) {
			j++;
			p_lat_time_multiplier = atoi(argv[j]);
	
		} else if (strcmp(argv[j], "-lat-filename-delimiter") == 0) {
			j++;
			p_lat_filename_delimiter = argv[j][0];
			
		} else if (strcmp(argv[j], "-get-best-path") == 0) {
			p_get_best_path = true;
			p_compute_links_likelihood = true;
			
		} else if (strcmp(argv[j], "-generate-label-file") == 0) {
			j++;
			p_generate_label_file = argv[j];
			ofstream flab(p_generate_label_file.c_str(), ios::out);
			if (flab.fail()) {
				cerr << "Error: can not open label file '"<<p_generate_label_file<<"' for writing ... " << endl;
				exit(1);
			}
			flab.close();
			p_compute_links_likelihood = true;
			
		} else if (strcmp(argv[j], "-lat-filename-start-time-field") == 0) {
			j++;
			p_lat_filename_start_time_field = atoi(argv[j]);
			
		} else if (strcmp(argv[j], "-print-alphalast") == 0) {
			j++;
			LatBinFile::AlphaLastArray alphaLastArray;
			alphaLastArray.load(argv[j]);
			alphaLastArray.print();
			cerr << "alphaLast printed...exit application" << endl;
			exit(0);
			
		} else if (strcmp(argv[j], "-output-alpha-beta-vectors") == 0) {
			p_output_alpha_beta_vectors = true;

		} else if (strcmp(argv[j], "-write-ngrams") == 0) {
			j++;
			p_write_ngrams = argv[j];

		} else if (strcmp(argv[j], "-ngrams-size") == 0) {
			j++;
			p_ngrams_size = atoi(argv[j]);

		} else if (strcmp(argv[j], "-generate-ngram-index") == 0) {
			p_generate_ngram_index = true;

		} else if (strcmp(argv[j], "-indexed-meeting") == 0) {
			j++;
			p_indexed_document = argv[j];

		} else if (strcmp(argv[j], "-log") == 0) {
			j++;
			p_log = argv[j];

		} else if (strcmp(argv[j], "-htk-remove-backslash") == 0) {
			p_htk_remove_backslash = true;

		} else if (strcmp(argv[j], "-confidence-measure") == 0) {
			j++;
			if (strcmp(argv[j], "cmax") == 0)
			{
				p_confidence_measure = new ConfidenceMeasure_CMax;
			}
			else if (strcmp(argv[j], "logadd_overlapping") == 0)
			{
				p_confidence_measure = new ConfidenceMeasure_LogaddOverlapping;
			}

		} else if (strcmp(argv[j], "-posterior-scale") == 0) {
			j++;
			p_posterior_scale = atof(argv[j]);

		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		
		
		j++;
	}



	//==================================================
	// LOAD LEXICON
	//==================================================
	if (p_lexicon_in != "") {
		DBG_MAIN( "Loading lexicon " << p_lexicon_in << "..." << flush);
		if (!p_readonly_lexicon || p_generate_lexicon_index != "") {

			DBG_MAIN("writeable...");
			
			if (lexicon.loadFromFile(p_lexicon_in) != 0)
				DBG_MAIN( "Lexicon input file " << p_lexicon_in << " not found ... using empty lexicon" << endl << flush);
			lexicon.getValID("!NULL");			// make !NULL the first word in lexicon
			lexicon.getValID(LATCONCATWORD); 	// this word is not present in lattices, so we have to add it manually
		} else {

			DBG_MAIN("readonly...");

			if (lexicon.loadFromFile_readonly(p_lexicon_in) != 0) {
				cerr << "ERROR: lexicon file " << p_lexicon_in << " not found" << endl;
				exit(1);
			}
		}
		DBG_MAIN( "done" << endl << flush);
	}
	
	
	// if we are going to save lattice to binary format, we need to compute likelihoods
	if ((p_write_binary_gzipped) && !p_compute_links_likelihood) {
		p_compute_links_likelihood = true;
		cout << "Switches -write-binary-gzipped have to be used with -compute-links-likelihood ... -compute-links-likelihood switch forced" << endl << flush;
	}

	DBG_MAIN( endl);

	dbgIndexingTimer.start();

	string filename;
	
	if (p_print_lexicon) {
		if (!p_readonly_lexicon)
			lexicon.print();
		else {
			cerr << "ERROR: printing lexicon allowed only with switch: -only-add-words-to-lexicon" << endl;
			exit(1);
		}
	}

	// this does not depend on lexicon - wordIDs are written within the reverse search index
	if (p_generate_wordID_search_index != "") {
		Timer wordIdIndexTime;
		wordIdIndexTime.start();

		indexer.lattices.createWordIDIndexFile(p_generate_wordID_search_index);

		wordIdIndexTime.end();
		ofstream log((p_generate_wordID_search_index+".log").c_str(), (file_exists((p_generate_wordID_search_index+".log").c_str()) ? ios::out | ios::app : ios::out));
		log << "index_time: " << wordIdIndexTime.val() << endl;
		log.close();
	}

	// for this we need to have load lexicon into maps => should be modified using the readonly lexicon
	if (p_generate_lexicon_index != "") {
		lexicon.createIndexFile(p_generate_lexicon_index);
	}
	

	// clean output files for storing a concatenated lattice
	if (p_concatlattice_out)
	{
		p_concatlattice_out_filename = p_lattice_dir_out + "/" + p_document_name;
		latBinFileConcat.cleanOutputFiles(trim_file_extension(p_concatlattice_out_filename) + ext_bin);
	}

	//==================================================
	// OPEN LATTICE LIST FILE
	//==================================================
	ifstream in_latlist;
	if (p_latlist_in_file != "") {
		in_latlist.open(p_latlist_in_file.c_str());
/*		if (!in_latlist.good()) 
			DBG_FORCE("!GOOD");
		if (in_latlist.bad()) 
			DBG_FORCE("BAD");*/
		if (in_latlist.fail()) {
			cerr << "Error: latlist open failed ... " << p_latlist_in_file << endl;
			exit(1);
		}
	}
	
	// if there are lattices to process and start-time-field is not set, then exit with error
/*	if ((p_latlist_in_file != "" || j < argc) && (p_lat_filename_start_time_field == 0) && !p_only_add_words_to_lexicon && !p_bin2txt) {
		cerr << "warning: set the parameter -lat-filename-start-time-field [NUMBER]";
//		exit(1);
	}
*/
	//==================================================
	// MAIN LOOP - FOR EACH LATTICE FILE
	//==================================================
	DBG_MAIN("Processing files...");
	while ((p_latlist_in_file != "") ? (getline(in_latlist, filename)) : (j < argc)) { 
		// jump over application parameters
		if (p_latlist_in_file != "") 
			DBG_MAIN("Latlist record: "<<filename<<endl);
		
		if (p_latlist_in_file == "") {
			if (argv[j][0] == '-') {
				cerr << "Parameter " << argv[j] << " ignored (try --help)" << endl;
				continue;
			}
			filename = argv[j];
		}
		DBG_MAIN("filename: "<<filename<<endl<<flush);

		DBG_FORCE("Processing file: "<<filename);
		if (!file_exists(filename.c_str()))
		{
			CERR("ERROR: file does not exist: "<<filename);
			EXIT();
		}
		
		//==================================================
		// PROCESS LATTICE FILENAME
		//==================================================
		string latname = filename;
		// erase file extension
		string::size_type strPos = latname.rfind (PATH_SLASH);
		if (strPos != string::npos) {
			string::size_type pos2 = latname.find ('.', strPos);
			latname.erase(pos2);
		}
		// erase path (only filename without extension remains)
		string latname_nopath = latname;
		strPos = latname_nopath.rfind(PATH_SLASH);
		latname_nopath.erase(0, strPos+1);

		DBG_FORCE( "latname_nopath: " << latname_nopath << endl);

		//==================================================
		// GENERATE BEST-PATH NODES FILE FROM COMPLETE NODES FILE
		//==================================================
		if (p_generate_best_path_nodes_from_nodes_file) {
			if (latBinFile.saveAllBestPathNodes(filename) != 0) {
				cerr << "Error while writing to file: " << filename << LATBINFILE_EXT_BESTNODES << endl;
			}
			if (p_latlist_in_file == "") j++; // do not forget to move to the next lattice
			continue;
		}
		
		//==================================================
		// CONVERT BINARY LATTICE TO TXT FILE
		//==================================================
		if (p_read_binary && p_bin2txt) {
			DBG_FORCE("Converting binary lattice to text file...");
			LatBinFile latBinFile;
			latBinFile.convertToTextFile(filename);
			DBG_FORCE("Converting binary lattice to text file...done");
			if (p_latlist_in_file == "") j++;
			continue; // because we can not execute following functions which use lat data structure (we are using latBinFile data structure here)
		}

		//==================================================
		// GET LATTICE'S START TIME
		//==================================================
		LatTime latStartTime = 0;
		if (p_read_time_from_filename) {
			DBG("p_read_time_from_filename");
			string sStartTime = latname_nopath;
			string::size_type pos_1st = 0;
			for (int field_idx=1; field_idx < p_lat_filename_start_time_field; field_idx++) {
				pos_1st = sStartTime.find (p_lat_filename_delimiter, pos_1st+1); // find first _ in latname
				//DBG("pos_1st:"<<pos_1st);
			}
			if (pos_1st != string::npos) {
				string::size_type pos_2nd = latname_nopath.find (p_lat_filename_delimiter, pos_1st+1); // find second _ in latname
				sStartTime = sStartTime.substr(pos_1st+1, pos_2nd - pos_1st - 1);
				latStartTime = atof(sStartTime.c_str()) / p_latname_time_multiplier;
				DBG("Getting start time from filename...sStartTime:"<<sStartTime<<" latStartTime:"<<latStartTime<<endl);
			} else {
				cerr << "Error in lattice's name: Expecting _ in \"" << latname_nopath << "\" ... setting start time to 0.0" << endl << flush;
			}
		}

		if (p_debug_level > 0) cout << "Processing file: " << filename << endl << flush;

		Lattice lat(lexicon, indexer);
		lat.amscale = p_amscale;
		lat.lmscale = p_lmscale;

		//==================================================
		// READ HTK LATTICE
		//==================================================
TIMER_START;
		DBG_MAIN( "reading..." << flush);

		if(lat.loadFromHTKFile(filename, latStartTime, p_lat_time_multiplier, p_only_add_words_to_lexicon, p_htk_remove_backslash) != 0) {
			cerr << "Error occured while reading lattice file " << filename << endl << flush;
			exit (1);
		}
		if (p_only_add_words_to_lexicon) {
			if (p_latlist_in_file == "") j++; // go to the next file (when parsing filenames as command-line parameters)
			continue;
		}
		DBG_MAIN( "done\t" << flush);
TIMER_END("load-lattice");
		//==================================================
		// SORT LATTICE
		//==================================================
		if (p_sort_lattice)
		{
			bool sorted = true;
TIMER_START;			
			for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
				if ((*l)->S > (*l)->E) {
					sorted = false;
					break;
				}
			}
TIMER_END("check-if-lattice-sorted");			

			if (!sorted)
			{
TIMER_START;
				DBG_MAIN("sorting lattice");
				lat.sortLattice();
				DBG_MAIN("sorting lattice...done");
TIMER_END("lattice-sorting");
			}
		}
		
		//==================================================
		// CHECK NODE SORTING
		//==================================================
		if (p_check_node_sorting) {
			DBG_MAIN("lattice link test");
			for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
				if ((*l)->S > (*l)->E) {
					CERR("Error in \"" << filename << "\" ...linkID:"<<(*l)->id<<" "<<(*l)->S<<" -> "<<(*l)->E);
					exit(1);
				}
			}
			DBG_MAIN("lattice link test: done");
		}

		//==================================================
		// TRIM TIME
		//==================================================
		if (p_trim_time_interval) {
			lat.trimTimeInterval(p_trim_t_start, p_trim_t_end);
		}

		//==================================================
		// ADD LATTICE'S WORDS TO LEXICON
		//==================================================
		if (!p_do_not_add_to_lexicon) {
			lat.addToLexicon();
		}
/*
		//==================================================
		// REMOVE NULL NODES
		//==================================================
		if (p_remove_null_nodes) {
			lat.removeNullNodes();
		}

		//==================================================
		// PRUNE LATTICE
		//==================================================
		if (p_do_pruning_posterior_treshold) {
			lat.updateFwdBckLinks();
			lat.pruneLattice(p_pruning_posterior_treshold);
		}

		//==================================================
		// GENERATE NGRAM-LATTICE
		//==================================================
//		DBG_MAIN("p_transform2ngram: "<<p_transform2ngram);
//		if (p_transform2ngram) {
//			DBG_FORCE("generating ngram lattice...");
//			Lattice ngramLat(NULL, NULL);
//			lat.transform2ngram(3, &ngramLat);
//			DBG_FORCE("generating ngram lattice...done");
//		}
*/
		//==================================================
		// COMPUTE LINKS LIKELIHOOD (POSTERIORS)
		//==================================================
		if (p_compute_links_likelihood) {
TIMER_START;
			DBG_MAIN("compute links likelihood..." << flush);
			
			LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
			vit.computeFwViterbi();
			vit.computeBwViterbi();
			vit.computeLinksLikelihood(latStartTime);
			if (p_write_viterbi && p_lattice_dir_out != "") 
				vit.writeViterbiFwBw(p_lattice_dir_out + PATH_SLASH + latname_nopath + ".fwbw");

			DBG_MAIN("done   ");
TIMER_END("compute-links-likelihood");

			if (p_confidence_measure != NULL)
			{
TIMER_START;
				DBG_MAIN("compute confidence measure...");
				p_confidence_measure->ComputeLinksConfidence(&lat);
				DBG_MAIN("done");
TIMER_END("compute-confidence-measure");
			}

		}
		
		//==================================================
		// NGRAMS
		//==================================================
		if (p_write_ngrams != "")
		{
			lat.OutputNgrams(p_write_ngrams, p_ngrams_size);
		}

		//==================================================
		// HTK OUTPUT
		//==================================================
		if (p_write_htk && p_lattice_dir_out != "") {
			DBG( "htk-export (" << p_lattice_dir_out + PATH_SLASH + latname_nopath + ext_copy << ")..." << flush);
			lat.saveToHTKFile(p_lattice_dir_out + PATH_SLASH + latname_nopath + ext_copy, p_htk_posteriors_output, false); // COPY OF LATTICE		
			DBG( "done\t" << flush);
		}

		//==================================================
		// GET BEST PATH
		//==================================================
		if (p_get_best_path) 
		{
			vector<ID_t> path;
			ID_t first_node_id = -1;
			lat.GetBestPath(&path, &first_node_id);

			cout << "BEST PATH: " << endl;
			for (vector<ID_t>::iterator i=path.begin(); i!=path.end(); ++i)
			{
				cout << " --("<<lat.links[*i]->confidence_noScaleOnKeyword<<")--> " << lat.links[*i]->E;
			}
			cout << endl;
		}

		//==================================================
		// GENERATE LABEL FILE
		//==================================================
		if (p_generate_label_file != "") 
		{
			vector<ID_t> path;
			ID_t first_node_id = -1;
			lat.GetBestPath(&path, &first_node_id);
			
			ofstream flab(p_generate_label_file.c_str(), file_exists(p_generate_label_file.c_str()) ? ios::out | ios::app : ios::out);
			if (flab.fail()) {
				cerr << "Error: can not open label file '"<<p_generate_label_file<<"' for writing ... " << endl;
				exit(1);
			}
			DBG("latStartTime:"<<latStartTime);
			for (vector<ID_t>::iterator i=path.begin(); i!=path.end(); ++i)
			{
				Lattice::Link *plink = lat.links[*i];
				//if (lat.nodes[plink->E]->W == "!NULL" || lat.nodes[plink->E]->W[0] == '<')
				if (lat.nodes[plink->E]->W == "!NULL")
					continue;

				flab 
					<< lat.nodes[plink->S]->t * 100 << "00000 "
					<< lat.nodes[plink->E]->t * 100 << "00000 "
					<< lat.nodes[plink->E]->W
					<< endl;
			}
			flab.close();
		}
		
		//==================================================
		// MLF OUTPUT
		//==================================================
		if (p_mlf_out != "") {
			DBG_MAIN( "mlf-export..." << flush);
//			lexicon.print();
			LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
			vit.computeFwViterbi();
			vit.computeBwViterbi();
			vit.computeLinksLikelihood(latStartTime);
//			vit.writeViterbiFwBw(filename + ".fwbw");

//			DBG_FORCE("fwds[lastNode]="<<vit.fwds[vit.NN-1]<<" bwds[firstNode]="<<vit.bwds[0]);
/*			DBG_FORCE("Checking fwds");
			for (ID_t n=0; n<vit.NN; n++) {
				for (vector<ID_t>::iterator l=lat.bckLinks[n].begin(); l!=lat.bckLinks[n].end(); ++l) {
					if (vit.fwds[lat.links[*l].S] > vit.fwds[n]) {
						DBG_FORCE("ERROR: "<<lat.links[*l].S<<" -> "<<n<<" ... fwds: "<<vit.fwds[lat.links[*l].S]<<" > "<<vit.fwds[n]);
					}
				}
			}
			DBG_FORCE("Checking fwds: done");*/
			LatMlfFile latmlffile(&lat, &vit, p_mlf_likelihood_treshold, p_compute_overlapped_words_likelihood);
			if (latmlffile.loadKeywordList(p_mlf_kwdlist) != 0) {
				cout << "Error: processing kwdlist: " << p_mlf_kwdlist << endl << flush;
				exit(1);
			}
			latmlffile.save(p_mlf_out, lat.record);
			DBG_MAIN( "done\t" << flush);
		}
		
		
		//==================================================
		// LATTICE CONCATENATION
		//==================================================
		if (p_concatlattice_out) {
			DBG_MAIN( "adding-to-concatlattice..." << flush);

			latBinFileConcat.setLattice(lat);
			string meeting_filename = trim_file_extension(p_concatlattice_out_filename) + ext_bin;
			DBG("meeting_filename: "<<meeting_filename);

			string meeting_rec = p_document_name + ext_bin;
			DBG_FORCE("meeting_rec: "<<meeting_rec);


			if (p_search_index != "")
			{
				if (!p_generate_ngram_index)
				{
					indexer.fwdIndex.addMeeting(lat.nodes.size());
				}
			}
			DBG("latBinFileConcat.save()");
			latBinFileConcat.save(meeting_filename, meeting_rec, true, !p_generate_ngram_index);
			DBG("latBinFileConcat.save()...done");

//			globalLat.addLattice(lat);
			DBG_MAIN( "done\t" << flush);
		}


		//==================================================
		// FORWARD INDEX
		//==================================================
		if (p_search_index != "")
		{
TIMER_START;
			if (p_indexed_document == "")
			{
				p_indexed_document = p_document_name;
			}

			if (p_generate_ngram_index)
			{
				lat.GenerateNgramsIndex(p_ngrams_size, p_indexed_document);
			}
			else
			{
				lat.GenerateIndex(p_indexed_document);
			}
TIMER_END("filling-forward-index");
		}

		// write lattice to binary file
/*
		if (p_write_binary) {
			DBG_MAIN( "binary-export..." << flush);
			lat.saveToBinaryFile(latname + ext_bin);			
			DBG_MAIN( "done\t" << flush);
		}
*/		

		//==================================================
		// BINARY OUTPUT
		//==================================================
		if (p_write_binary_gzipped && p_concatlattice_out) {
			CERR("ERROR: gzipped binary format is not yet implemented");
			exit(1);
/*			
			DBG_MAIN( "binary-gz-export..." <<endl);
			latBinFile.setLattice(lat);
			string meeting_filename = p_lattice_dir_out + PATH_SLASH + latname_nopath + ext_bin;
			DBG_MAIN(meeting_filename << "..." <<endl);
			
			unsigned int pos = meeting_filename.find( p_data_dir_prefix, 0 );
			string meeting_rec;
			if( pos == 0 ) {
				meeting_rec = meeting_filename.substr(p_data_dir_prefix.length());
			} else {
				meeting_rec = meeting_filename;
			}
			if (latBinFile.save(meeting_filename, meeting_rec) != 0) {
				cerr << "ERROR: Cannot create file " << meeting_filename << endl << flush;
			}
			DBG_MAIN("meeting_filename: "<<meeting_filename<<endl);
			DBG_MAIN("meeting_rec: "<<meeting_rec << endl);
//			lat.saveToBinaryFile(latname + ext_bingz, Lattice::gzip);			
			DBG_MAIN( "done\t" << flush);
*/
		}

		//==================================================
		// CLOSE FORWARD INDEX
		//==================================================
		if (p_search_index != "")
		{
			indexer.fwdIndex.closeMeeting();
		}

		//==================================================
		// DOT OUTPUT
		//==================================================
		if (p_write_dot && p_lattice_dir_out != "") {
			DBG_MAIN( "dot-export..." << flush);
			lat.saveToDotFile(p_lattice_dir_out + PATH_SLASH + latname_nopath + ext_dot);
			DBG_MAIN( "done\t" << flush);
		}


		//==================================================
		// ALPHA & BETA VECTORS OUTPUT
		//==================================================
/*
		if (p_output_alpha_beta_vectors) {
			DBG_MAIN( "alpha & beta vectors output..." );
			lat.SaveAlphaBetaVectors(p_lattice_dir_out + PATH_SLASH + latname_nopath + ext_alphabetavectors);
			DBG_MAIN( "done\t" );
		}
*/
		DBG_MAIN( endl << endl); // insert empty line between lattices info output
		
		if (p_latlist_in_file == "") j++;

	} // while (j < argc)


	//==================================================
	// AFTER PROCESSING LATTICES
	//==================================================

	
	if (p_latlist_in_file != "") in_latlist.close();
	
	DBG("--------------------------------------------------");
	DBG("After processing lattices");
	//==================================================
	// SAVE CONCATENATED LATTICE
	//==================================================
	if (p_concatlattice_out) {
		DBG_MAIN( "saving-concatlattice..." << flush);
/*		if (p_write_binary) {
			DBG_MAIN( "binary..." << flush);
			globalLat.saveToBinaryFile(p_concatlattice_out_filename + ext_bin);
		} 
*/
		
		//==================================================
		// HTK OUTPUT
		//==================================================
		if (p_write_htk) {
			DBG_FORCE("WARNING: Saving concatenated lattice to HTK file format is not implemented");
//			globalLat.saveToHTKFile(p_concatlattice_out_filename + ext_htk);				// without posteriors
//			globalLat.saveToHTKFile(p_concatlattice_out_filename + ext_htk + ".lik", true, true);	// with posteriors
		}

		//==================================================
		// BINARY OUTPUT
		//==================================================
		{
			string meeting_filename = trim_file_extension(p_concatlattice_out_filename) + ext_bin;
			string meeting_rec = p_document_name + ext_bin;

			latBinFileConcat.closeConcatenatedLatticeOutput(meeting_filename, meeting_rec, !p_generate_ngram_index);
		}

		//==================================================
		// DOT OUTPUT
		//==================================================
		if (p_write_dot) {	
			DBG_MAIN( "dot...");
			globalLat.saveToDotFile(p_concatlattice_out_filename + ext_dot);
		}
		DBG_MAIN( "done" << endl);
	}
	
	//==================================================
	// SAVE LEXICON
	//==================================================
//	DBG_FORCE("++OK++");
//	DBG_FORCE("p_lexicon_out = <" << p_lexicon_out << ">");
	if (p_lexicon_out != "") {
//		DBG_FORCE("++saving lexicon++");
		DBG_MAIN( "saving-lexicon..." << flush);
		lexicon.saveToFile(p_lexicon_out);
		DBG_MAIN( "done" << endl);
	}
	
	//==================================================
	// SAVE MEETINGS
	//==================================================
	if (p_meeting_indexer_out != "") {
		DBG_MAIN( "saving-meeting-index...");
		indexer.meetings.saveToFile(p_meeting_indexer_out);
		DBG_MAIN( "done" << endl << flush);
	}
	
	//==================================================
	// SAVE SEARCH INDEX
	//==================================================
	if (p_search_index != "") {
//		DBG_MAIN( "saving-search-index...sorting...");
//		indexer.lattices.sortRecords();
//		DBG_MAIN( "saving..." << flush);
//		indexer.lattices.saveToFile(p_search_indexer);		
//		DBG_MAIN( "done" << endl << flush);
		DBG_MAIN( "closing forward index file...");
		indexer.fwdIndex.close();
	}
	
	dbgIndexingTimer.end();
	if (p_log != "")
	{
		DBG_FORCE("LOG: "<<p_log);
		ofstream log(p_log.c_str(), (file_exists(p_log.c_str()) ? ios::out | ios::app : ios::out));
		log << "indexing_time: " << dbgIndexingTimer.val() << endl;
		log.close();
	}

	DBG_MAIN( endl << "***** OPERATION COMPLETED *****" << endl);
	if (p_confidence_measure)
		delete p_confidence_measure;
	return (0);
}


