#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <errno.h>

#include "../liblse/lattypes.h"
#include "lexicon.h"
#include "lat.h"
#include "mlf.h"
#include "latmlffile.h"
#include "common.h"

using namespace std;
using namespace lse;

string p_mlf_in 		= "";
float  p_mlf_time_mult	= 1;
string p_lexicon_in		= "";
string p_idx_out		= "";
string p_meeting_idx	= "";
int p_lat_filename_start_time_field = 10;
char p_lat_filename_delimiter = '_';
int p_latname_time_multiplier = 100;

int main(int argc, char * argv[]) {
	if (argc != 6) {
		cout << argv[0] << " mlf-in mlf-time-to-sec-multiplier lexicon-in reverse-idx-out meeting-idx-out" << endl;
		return 1;
	}

	p_mlf_in 		= argv[1];
	p_mlf_time_mult	= atof(argv[2]);
	p_lexicon_in 	= argv[3];
	p_idx_out 		= argv[4];
	p_meeting_idx	= argv[5];


	Lattice::Indexer 	indexer;
	Lexicon 			lexicon(Lexicon::readonly);
//	DBG_FORCE("Reading from MLF");
	Mlf 				mlf(p_mlf_in);
//	DBG_FORCE("Reading from MLF...done");

	if (file_exists(p_meeting_idx.c_str())) {
		cout << "Meeting index already exists...loading...";
		indexer.meetings.loadFromFile(p_meeting_idx);
		cout << "done" << endl;
	}
	else
	{
		cout << "Meeting index does not exists...creating a new one" << endl;
	}

//	mlf.Save(p_mlf_in + ".copy");
	

	indexer.fwdIndex.create(p_idx_out);
	if (lexicon.loadFromFile_readonly(p_lexicon_in) != 0)
	{
		DBG_FORCE( "ERROR: Lexicon input file " << p_lexicon_in << " not found");
		exit(1);
	}
	lexicon.print();
/*
	for (Mlf::iterator iMlfRec = mlf.begin(); iMlfRec != mlf.end(); ++iMlfRec)
	{
		DBG_FORCE(*iMlfRec);
	}
*/		

//	for (Mlf::iterator iMlfRec = mlf.begin(); iMlfRec != mlf.end(); ++iMlfRec)
	MlfRec mlf_rec;
	while (mlf.Next(&mlf_rec))
	{
//		DBG_FORCE("iMlfRec: "<<*iMlfRec);
		LatMeetingIndexerRecord	  meetingRec;
//		meetingRec				= mlf.mMeetingIndexer.getVal(iMlfRec->mMeetingID);
		meetingRec.path 		= mlf_rec.mMeetingPath;
		meetingRec.length		= 0; // should be the max. of all mlf_rec.mEndT of each mlf_rec.mFilename


		LatTime latStartTime = 0;
		bool p_read_time_from_filename = true;
		if (p_read_time_from_filename) {
			string sStartTime = mlf_rec.mMeetingPath;
			string::size_type pos_1st = 0;
			for (int field_idx=1; field_idx < p_lat_filename_start_time_field; field_idx++) {
				pos_1st = sStartTime.find (p_lat_filename_delimiter, pos_1st+1); // find first _ in latname
				DBG("pos_1st:"<<pos_1st);
			}
			if (pos_1st != string::npos) {
				string::size_type pos_2nd = mlf_rec.mMeetingPath.find (p_lat_filename_delimiter, pos_1st+1); // find second _ in latname
				sStartTime = sStartTime.substr(pos_1st+1, pos_2nd - pos_1st - 1);
				latStartTime = atof(sStartTime.c_str()) / p_latname_time_multiplier;
//				DBG_MAIN("Getting start time from filename...sStartTime:"<<sStartTime<<" latStartTime:"<<latStartTime<<endl);
			} else {
				cerr << "Error in lattice's name: Expecting _ in \"" << latname_nopath << "\" ... setting start time to 0.0" << endl << flush;
			}
		}


		LatIndexer::Record		  latIndexerRec;		// main searching index record
		latIndexerRec.meetingID	= indexer.meetings.insertRecord(&meetingRec);// add index for this meeting to indexer->meetings
		latIndexerRec.wordID 	= lexicon.word2id_readonly(mlf_rec.mWord);//getValID(LATCONCATWORD);
		latIndexerRec.nodeID 	= -1;
		latIndexerRec.conf   	= mlf_rec.mConf;
		latIndexerRec.tStart 	= latStartTime + mlf_rec.mStartT / p_mlf_time_mult;
		latIndexerRec.t 	 	= latStartTime + mlf_rec.mEndT   / p_mlf_time_mult;

//		DBG_FORCE(mlf_rec);
//		DBG_FORCE(mlf_rec.mStartT<<" / "<<p_mlf_time_mult<<" = "<<latIndexerRec.tStart);
//		DBG_FORCE("latIndexerRec.nodeID:"<< latIndexerRec.nodeID);
//		DBG_FORCE(latIndexerRec);

		indexer.fwdIndex.addRecord(latIndexerRec); // add node to search index
	}
	indexer.fwdIndex.close();
	indexer.meetings.saveToFile(p_meeting_idx);

	mlf.Close();

	return 0;
} // main()

