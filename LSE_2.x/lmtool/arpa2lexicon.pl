#!/usr/bin/perl

$arpa_filename = $ARGV[0];
$LSE_BIN_PATH = "/mnt/matylda/fapso/projects/search-engine/bin/unstable";

open(ARPA, $arpa_filename);
open(LEX_TXT, ">$arpa_filename.lexicon.txt");

$state = 0;
$id = 1;
while(<ARPA>)
{
	chomp;
	if ($state == 0)
	{
		if ($_ =~ /\\1-grams:/)
		{
			$state = 1;
		}
	}
	elsif ($state == 1)
	{
		if ($_ =~ /^\s*$/)
		{
			last;
		}
		($word) = $_ =~ /^[^\s]+\s+([^\s]+)/;
		print LEX_TXT "$id\t$word\n";
		$id++;
	}
}

close(LEX_TXT);

@args = ("$LSE_BIN_PATH/LSElexicon_txt2bin", "$arpa_filename.lexicon.txt", "$arpa_filename.lexicon.IDX");
system(@args) == 0
	or die "system '@args' failed";
