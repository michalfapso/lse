#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/common.h"
#include "../liblse/languagemodel.h"
#include "../liblse/lattypes.h"

using namespace std;
using namespace lse;

int main(int argc, char * argv[]) 
{
/*            
	LanguageModelFile lmFile("/net/matylda2/szoke/tmp1/combined.V0.0.50k.2gram.arpa");
	LanguageModelRec lmRec;
	lmFile.Next(&lmRec);
	DBG_FORCE("lmRec: "<<lmRec);
*/
	LanguageModel lm;
//	lm.Load("/var/mnt/eva/home/users/xf/xfapso00/speech/projects/LSE/branches/LSE_1.x/lmtool/test.arpa");
//	lm.Load("/net/matylda2/szoke/tmp1/combined.V0.0.50k.2gram.arpa");
//	lm.Load("/mnt/scratch01/fapso/exp/icsi-tsd-paper-experiments/arpa/test.arpa");
//	lm.Load("/mnt/scratch01/fapso/exp/icsi-tsd-paper-experiments/arpa/combined.V0.0.50k.2gram.arpa");
//	DBG_FORCE("ABOUT:" << lm.GetWordNGramProb("ABOUT"));
//	DBG_FORCE("ABOUT AACHEN:" << lm.GetWordNGramProb("ABOUT AACHEN"));
        

	lm.Load("./pokus.arpa");
	std::cout << "A A B " << lm.GetWordNGramProb("A A B") << std::endl;
	std::cout << "A B C " << lm.GetWordNGramProb("A B C") << std::endl;
	std::cout << "A B B " << lm.GetWordNGramProb("A B B") << std::endl;
	std::cout << "E D C " << lm.GetWordNGramProb("E D C") << std::endl;
	std::cout << "E E E " << lm.GetWordNGramProb("E E E") << std::endl;
/*
	lm.Load("/net/matylda2/szoke/tmp1/pruned.combined.V0.0.50k.2gram.arpa");
	std::cout << "RIGHT " << lm.GetWordNGramProb("RIGHT") << std::endl;
	std::cout << "OKAY " << lm.GetWordNGramProb("OKAY") << std::endl;
	std::cout << "TO " << lm.GetWordNGramProb("TO") << std::endl;
	std::cout << "OKAY RIGHT " << lm.GetWordNGramProb("OKAY RIGHT") << std::endl;
	std::cout << "RIGHT TO " << lm.GetWordNGramProb("RIGHT TO") << std::endl;
*/
	return 0;
} // main()

