#ifndef MLF_DURATION_H
#define MLF_DURATION_H

#include "mlf.h"

namespace mlf {

class MlfDuration
{
	public:
		MlfDuration(const Mlf<MlfRecord>& mlf) :
			mTotalDurationSecondsSpeech(0),
			mTotalDurationSecondsNonspeech(0)
		{
			for (typename Mlf<MlfRecord>::const_iterator it_f = mlf.begin(); it_f != mlf.end(); it_f++) {
				const std::string& path = it_f->first;
				File<MlfRecord>* f = it_f->second;
				Durations& durations = mContainer[path];
				for (typename Records<MlfRecord>::const_iterator it = f->GetAllRecords().begin(); it != f->GetAllRecords().end(); it++)
				{
					MlfRecord* rec = *it;
					if (IsSpeech(rec->GetWord())) {
						durations.mSpeechSeconds += rec->GetLengthSeconds();
					} else {
						durations.mNonspeechSeconds += rec->GetLengthSeconds();
					}
				}
				mTotalDurationSecondsSpeech += durations.mSpeechSeconds;
				mTotalDurationSecondsNonspeech += durations.mNonspeechSeconds;
			}
		}

		inline float GetTotalDurationSecondsSpeech() const { return mTotalDurationSecondsSpeech; }
		inline float GetTotalDurationSecondsNonspeech() const { return mTotalDurationSecondsNonspeech; }
		inline virtual bool IsSpeech(const std::string& word) { return word != "sil"; }
	protected:
		struct Durations {
			float mSpeechSeconds;
			float mNonspeechSeconds;
		};
		typedef boost::unordered_map<std::string, Durations> Container;
		Container mContainer;
		float mTotalDurationSecondsSpeech;
		float mTotalDurationSecondsNonspeech;
};

}

#endif
