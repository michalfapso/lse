#ifndef MLF_PARSER_H
#define MLF_PARSER_H

#include <iostream>
#include "dbg.h"

namespace mlf {

class MlfRecord;

class MlfParser {
	public:
		MlfParser(std::istream& is) :
			mIStream(is),
			mPathHasChanged(false),
			mPath(""),
			mLineNr(0)
		{}

		bool Next(MlfRecord *rec);
		bool PathHasChanged() const { return mPathHasChanged; }
		inline const std::string& GetPath() const { return mPath; }
	protected:
		inline bool IsEol(const char c) const { return (c == '\n'); }
		inline bool IsDelimiter(const char c) const { return (c == ' ' || c == '\t' || c == '\n'); }
		double StrToFloat(const char* str) const;
		void ProcessPathString(std::string* pStr) const;

		std::istream& mIStream;
		bool mPathHasChanged;
		std::string mPath;
		unsigned int mLineNr;
};

}
#endif
