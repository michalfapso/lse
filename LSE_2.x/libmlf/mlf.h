#ifndef MLF_H
#define MLF_H

#include <boost/shared_ptr.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_convertible.hpp>
#include <boost/unordered_map.hpp>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include "container_interface.h"
#include "dbg.h"
#include "mlf_parser.h"
#include "sorted_map_keys.h"

namespace mlf {


//--------------------------------------------------
// class MlfRecord {{{
//--------------------------------------------------
class MlfRecord {
	public:
		MlfRecord() :
			mStart(0),
			mEnd(0),
			mWord(""),
			mScore(0),
			mHasScore(false)
		{}
		virtual ~MlfRecord() {}

		virtual inline unsigned long      GetStartTime() const {return mStart;}
		virtual inline unsigned long      GetEndTime()   const {return mEnd;}
		virtual inline const std::string& GetWord()      const {return mWord;}
		virtual inline float              GetScore()     const {return mScore;}
		virtual inline bool               HasScore()     const {return mHasScore;}
		virtual inline const std::string& GetFilename()  const {return EMPTY_STRING;}

		virtual inline void SetStartTime(unsigned long t)      {mStart = t;}
		virtual inline void SetEndTime  (unsigned long t)      {mEnd = t;}
		virtual inline void SetWord     (const std::string& w) {mWord = w;}
		virtual inline void SetScore    (float s)              {mScore = s; mHasScore = true;}
		virtual inline void SetFilename (const std::string& f) {}

		virtual void Reset() {
			mStart = 0;
			mEnd = 0;
			mWord = "";
			mScore = 0;
			mHasScore = false;
		}
		inline float GetLengthSeconds() { return (mEnd - mStart)/100; }
		virtual std::ostream& PrintOn(std::ostream& oss) const {
			oss << mStart << "00000 " << mEnd << "00000 " << mWord;
			if (mHasScore) {
				oss << " " << std::fixed << std::setprecision(10) << mScore;
			}
			return oss;
		}
		friend std::ostream& operator<<(std::ostream& oss, const MlfRecord& r) {
			r.PrintOn(oss);
			return oss;
		}
	protected:
		unsigned long mStart;
		unsigned long mEnd;
		std::string   mWord;
		float         mScore;
		bool          mHasScore;
		static const std::string EMPTY_STRING;
};
// }}}
//--------------------------------------------------

//--------------------------------------------------
// class MlfRecordWithFilename {{{
//--------------------------------------------------
class MlfRecordWithFilename : public MlfRecord
{
	public:
		typedef boost::shared_ptr<const std::string> FilenamePtr;
		MlfRecordWithFilename() :
			MlfRecord()
		{}
		virtual void Reset() {
			MlfRecord::Reset();
			mFilename = "";
		}
		virtual inline const std::string& GetFilename() const { return mFilename; }
		virtual inline void SetFilename(const std::string& filename) {mFilename = filename;}
		virtual std::ostream& PrintOn(std::ostream& oss) const {
			MlfRecord::PrintOn(oss);
			//oss << " " << mFilename; // this would not be compatible with MLF output format
			return oss;
		}
		friend std::ostream& operator<<(std::ostream& oss, const MlfRecordWithFilename& r) {
			r.PrintOn(oss);
			return oss;
		}
	protected:
		std::string mFilename;
};
// }}}
//--------------------------------------------------

//--------------------------------------------------
// class MlfRecords {{{
//--------------------------------------------------
template<class TMlfRecord> 
struct MlfRecordsBase {
	typedef ContainerInterface< std::vector<TMlfRecord*> > type;
};

template <class TMlfRecord>
class MlfRecords : public MlfRecordsBase<TMlfRecord>::type 
{
	typedef typename MlfRecordsBase<TMlfRecord>::type Base;
//	BOOST_STATIC_ASSERT((boost::is_convertible<TMlfRecord, MlfRecord>::value));
	public:
		MlfRecords() : mIsSortedByScore(false) {}
		void AddRecord(TMlfRecord* pRec) {
			this->mContainer.push_back(pRec);
			mIsSortedByScore = false;
		}
		friend std::ostream& operator<<(std::ostream& oss, const MlfRecords<TMlfRecord>& r) {
			for (typename Base::const_iterator it = r.mContainer.begin(); it != r.mContainer.end(); it++) {
				oss << **it << std::endl;
			}
			return oss;
		}
		inline bool IsSortedByScore() { return mIsSortedByScore; }
		void SortByScore() {
			if (mIsSortedByScore) return;
			sort(this->begin(), this->end(), MlfRecords<TMlfRecord>::CmpScores); 
			mIsSortedByScore = true;
		}
		static bool CmpScores(const TMlfRecord* a, const TMlfRecord* b) {
			return a->GetScore() > b->GetScore();
		}
	protected:
		bool mIsSortedByScore;
};
// }}}
//--------------------------------------------------

//--------------------------------------------------
// class Mlf {{{
//--------------------------------------------------
template<class TMlfRecord> 
struct MlfBase {
	typedef MapInterface< boost::unordered_map<std::string, MlfRecords<TMlfRecord>*> > type;
};

template <class TMlfRecord>
class Mlf : public MlfBase<TMlfRecord>::type
{
	protected:
		typedef typename MlfBase<TMlfRecord>::type Base;
	public:
		Mlf() {}
		Mlf(const std::string& filename) {
			Load(filename);
		}
		~Mlf() {
			for (typename Base::iterator it = this->begin(); it != this->end(); it++) {
				for (typename MlfRecords<TMlfRecord>::iterator it_rec = it->second->begin(); it_rec != it->second->end(); it_rec++) {
					// Records are created in Mlf class, so they have to be also destroyed here.
					delete *it_rec;
				}
			}
			for (typename Base::iterator it = this->begin(); it != this->end(); it++) {
				delete it->second;
			}
		}

		template <class TContainer>
		void TransformTo(TContainer& c) {
			for (typename Base::const_iterator it = this->begin(); it != this->end(); it++) {
				MlfRecords<TMlfRecord>* recs = it->second;
				for (typename MlfRecords<TMlfRecord>::const_iterator it_rec = recs->begin(); it_rec != recs->end(); it_rec++) {
					c.AddRecord(*it_rec);
				}
			}
		}

		void Load(const std::string& filename="");
		void Load(std::istream& is);

		void Save(const std::string& filename="") const;
		void Save(std::ostream& os) const;

		      MlfRecords<TMlfRecord>* GetFile(const std::string& file, bool createIfNotExists = false);
		const MlfRecords<TMlfRecord>* GetFile(const std::string& file) const;
};
// }}}
//--------------------------------------------------

//class MlfWord2Records : public ContainerInterface< boost::unordered_map<std::string, MlfRecords<TMlfRecord>*> > {




//--------------------------------------------------
// class MlfWord2Records {{{
//--------------------------------------------------
template<class TMlfRecord> 
struct MlfWord2RecordsBase {
	typedef MapInterface< boost::unordered_map<std::string, MlfRecords<TMlfRecord>*> > type;
};

template <class TMlfRecord>
class MlfWord2Records : public MlfWord2RecordsBase<TMlfRecord>::type {
	protected:
		typedef typename MlfWord2RecordsBase<TMlfRecord>::type Base;
		typedef typename Base::iterator It;
	public:
		typedef MlfRecords<TMlfRecord> Recs;

		MlfWord2Records() {}
		MlfWord2Records(Mlf<TMlfRecord>& mlf) { mlf.TransformTo(*this); }

		template <class TContainer>
		void TransformTo(TContainer& c) {
			for (typename Base::const_iterator it = this->begin(); it != this->end(); it++) {
				MlfRecords<TMlfRecord>* recs = it->second;
				for (typename MlfRecords<TMlfRecord>::const_iterator it_rec = recs->begin(); it_rec != recs->end(); it_rec++) {
					c.AddRecord(*it_rec);
				}
			}
		}
		void AddRecord(TMlfRecord* pRec) { 
			Recs* recs = NULL;
			It it = this->mContainer.find(pRec->GetWord());
			if (it == this->mContainer.end()) {
				recs = new Recs;
				this->mContainer.insert(make_pair(pRec->GetWord(), recs));
			} else {
				recs = it->second;
			}
			assert(recs);
			recs->AddRecord(pRec);
		}
};
// }}}
//--------------------------------------------------


//--------------------------------------------------
// class MlfPath2Records {{{
//--------------------------------------------------
template <class TMlfRecord>
struct MlfPath2RecordsBase {
	typedef MapInterface< boost::unordered_map< std::string, MlfRecords<TMlfRecord>* > > type;
};

template <class TMlfRecord>
class MlfPath2Records : public MlfPath2RecordsBase<TMlfRecord>::type
{
	protected:
		typedef typename MlfPath2RecordsBase<TMlfRecord>::type Base;
		typedef typename Base::iterator ItP2r;
		typedef MlfRecords<TMlfRecord> Recs;
		typedef typename Recs::iterator ItRecs;
	public:
		MlfPath2Records() {}
		MlfPath2Records(Mlf<TMlfRecord>& mlf) {
			mlf.TransformTo(&this);
		}
		~MlfPath2Records() {
			for (ItP2r it = this->begin(); it != this->end(); it++) {
				delete it->second;
			}
		}
		void FillFrom(MlfRecords<TMlfRecord>& recs) {
			for (ItRecs it_recs = recs.begin(); it_recs != recs.end(); it_recs++) {
				AddRecord(*it_recs);
			}
		}
		void AddRecord(TMlfRecord* pRec) {
			assert(pRec);
			const std::string& path = pRec->GetFilename();
			ItP2r it = this->mContainer.find(path);
			Recs* path_recs = NULL;
			if (it == this->end()) {
				path_recs = new Recs;
				this->mContainer.insert(make_pair(path, path_recs));
			} else {
				path_recs = it->second;
			}
			assert(path_recs);
			path_recs->AddRecord(pRec);
		}
};
// }}}
//--------------------------------------------------

//--------------------------------------------------
// class MlfWord2Path2Records {{{
//--------------------------------------------------
template <class TMlfRecord>
struct MlfWord2Path2RecordsBase {
	typedef MapInterface< boost::unordered_map< std::string, MlfPath2Records<TMlfRecord>* > > type;
};

template <class TMlfRecord>
class MlfWord2Path2Records : public MlfWord2Path2RecordsBase<TMlfRecord>::type
{
	protected:
		typedef typename MlfWord2Path2RecordsBase<TMlfRecord>::type Base;
		typedef typename Base::iterator ItW2p2r;
		typedef typename Mlf<TMlfRecord>::iterator ItMlf;
		typedef typename Mlf<TMlfRecord>::iterator ItRecs;
		typedef MlfPath2Records<TMlfRecord> P2r;
	public:
		MlfWord2Path2Records() {}
		MlfWord2Path2Records(Mlf<TMlfRecord>& mlf) { mlf.TransformTo(*this); }
		MlfWord2Path2Records(MlfWord2Records<TMlfRecord>& w2r) { w2r.TransformTo(*this); }
		void AddRecord(TMlfRecord* rec) {
			assert(rec);
			P2r* p2r = GetOrAdd(rec->GetWord());
			assert(p2r);
			p2r->AddRecord(rec);
		}
		P2r* GetOrAdd(const std::string& word) {
			ItW2p2r it = this->mContainer.find(word);
			P2r* p2r = NULL;
			if (it == this->mContainer.end()) {
				p2r = new P2r;
				this->mContainer.insert(make_pair(word, p2r));
			} else {
				p2r = it->second;
			}
			return p2r;
		}
		~MlfWord2Path2Records() {
			for (ItW2p2r it = this->mContainer.begin(); it != this->mContainer.end(); it++) {
				delete it->second;
			}
		}
};
// }}}
//--------------------------------------------------


#include "mlf.tpp"

} // namespace mlf
#endif
