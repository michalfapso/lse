#include <sstream>
#include <stdexcept>
#include "mlf_parser.h"
#include "mlf.h"

using namespace std;
using namespace mlf;

void MlfParser::ProcessPathString(std::string* pStr) const
{
	string& s = *pStr;
	size_t pos; 
	// remove path -> keep only filename
	pos = s.rfind('/');
	if (pos != string::npos) {
		s = s.substr(pos+1);
	}
	// remove extension
	pos = s.rfind('.');
	if (pos != string::npos) {
		s = s.substr(0,pos);
	}
}

double MlfParser::StrToFloat(const char* str) const
{
	double d;
	if (!(std::istringstream(str) >> d)) {
		ostringstream oss;
		oss << "Error converting string '" << str << "' to double!";
		throw runtime_error(oss.str());
	}
	return d;
}

bool MlfParser::Next(MlfRecord *rec)
{
	static string 	record_path = "";
	static int		state = 0;

	string 	s		= "";
	char 	c		= 0;
	bool 	epsilon = false;

	rec->Reset();
	mPathHasChanged = false;

	try {

	while (!mIStream.eof())
	{
		if (epsilon) 
		{
			epsilon = false;
		}
		else
		{
			mIStream.get(c);
		}

		//DBG("c:"<<c<<" state:"<<state);
		switch (state)
		{
			case 0: // filename - starting "
				if (c == '"')
				{
					state = 1;
				}
				break;

			case 1: // filename - ending "
				if (c == '"')
				{
					mPath = s;
					ProcessPathString(&mPath);
					mPathHasChanged = true;
					s = "";
					state = 2;
				} 
				else
				{
					s += c;
				}
				break;

			case 2: // filename eol (end of line)
				if (IsEol(c))
				{
					mLineNr++;
					state = 3;
				}
				break;

			case 3: // skip delimiters in front of the start time
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 4;
				}
				break;

			case 4:	// record - start time
				if (c == '.')
				{
					state = 0;
				}
				else if (IsDelimiter(c))
				{
					rec->SetStartTime(round(StrToFloat(s.c_str()) / 100000));
					s = "";
					epsilon = true;
					state = 5;
				}
				else
				{
					s += c;
				}
				break;

			case 5: // skip delimiters between start time and end time
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 6;
				}
				else if (IsEol(c))
				{
					mLineNr++;
					state = 3;
					return true;
				}
				break;

			case 6: // record - end time
				if (IsDelimiter(c))
				{
					rec->SetEndTime(round(StrToFloat(s.c_str()) / 100000));
					s = "";
					epsilon = true;
					state = 7;
				}
				else
				{
					s += c;
				}
				break;

			case 7: // skip delimiters between end time and word
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 8;
				}
				else if (IsEol(c))
				{
					mLineNr++;
					state = 3;
					return true;
				}
				break;

			case 8: // record - word
				if (IsDelimiter(c))
				{
					rec->SetWord(s);
					s = "";
					epsilon = true;
					state = 9;
				}
				else
				{
					s += c;
				}
				break;

			case 9: // skip delimiters between word and score
				if (!IsDelimiter(c))
				{
					epsilon = true;
					state = 10;
				}
				else if (IsEol(c))
				{
					mLineNr++;
					state = 3;
					return true;
				}
				break;

			case 10: // record - confidence
				if (IsDelimiter(c))
				{
					rec->SetScore(StrToFloat(s.c_str()));
					s = "";
					epsilon = true;
					state = 11;
				}
				else
				{
					s += c;
				}
				break;

			case 11: // skip delimiters between score and end of line
//				if (!IsDelimiter(c))
//				{
//					epsilon = true;
//					state = 12;
//				} 
//				else 
				if (IsEol(c))
				{
					mLineNr++;
					state = 3;
//					DBG("rec: "<<*rec);
					return true;
				}
				break;

		} // switch (state)
	} // while (! mlf_file.eof())

	} catch(std::exception& e) {
		std::cerr << "ERROR: exception occured on MLF line " << mLineNr << "." << std::endl;
		throw;
	}
	return false;
}


