#include <iostream>
#include <vector>
#include <boost/unordered_map.hpp>
#include "container_interface.h"
#include "mlf.h"
#include "dbg.h"

using namespace std;
using namespace mlf;

/*
template<class T>
class MapInterfaceTest
{
	public:
		T mT;
		void foo() {
			DBG("foo");
		}
};

template<class TMlfRecord> 
struct MlfBase {
	typedef MapInterfaceTest< TMlfRecord > type;
//	typedef MapInterface< boost::unordered_map<std::string, TMlfRecord*> > type;
};

template<class TMlfRecord>
	using MlfBase2 = MapInterface< boost::unordered_map<std::string, TMlfRecord*> >;

template <class TMlfRecord>
//class Mlf : public MlfBase<TMlfRecord> 
class Mlf : public MlfBase2<TMlfRecord> 
{
	protected:
//		typedef typename MlfBase<TMlfRecord>::type Base;
	public:
		void a() {
			this->foo();
//			for (typename Base::iterator it = this->begin(); it != this->end(); it++) {
//				DBG(it->second);
//			}
		}
};

*/
int main() 
{
//	typedef ContainerInterface< vector<int> > Vec;
//	Vec v;
//	DBG("v.size():"<<v.size());
//	for (Vec::iterator it = v.begin(); it != v.end(); it++) {
//		DBG("v[]: "<<*it);
//	}
//	
//	typedef MapInterface< boost::unordered_map<std::string, int*> > Map;
//	Map m;
//	DBG("m.size():"<<m.size());
//	for (Map::iterator it = m.begin(); it != m.end(); it++) {
//		DBG("v[]: "<<it->second);
//	}
//	
//	Mlf<int> mlf;
//	mlf.a();

	Mlf<MlfRecord> mlf;
	mlf.Load(cin);
	mlf.Save(cout);
	return 0;
}
