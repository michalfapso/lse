#include <boost/type_traits/is_convertible.hpp>

template<class TMlfRecord>
void Mlf<TMlfRecord>::Load(const std::string& filename)
{
	using namespace std;
	ifstream f(filename.c_str());
	if ( !f.good() ) {
		throw runtime_error("Error opening file '"+filename+"'");
	}
	Load(f);
	f.close();
}

template<class TMlfRecord>
void Mlf<TMlfRecord>::Load(std::istream& is)
{
	mlf::MlfParser parser(is);
	TMlfRecord rec;
	MlfRecords<TMlfRecord>* file = NULL;
	while(parser.Next(&rec)) {
		if (parser.PathHasChanged()) {
			file = GetFile(parser.GetPath(), true);
		}
		assert(file);
		rec.SetFilename(parser.GetPath());
		file->AddRecord(new TMlfRecord(rec));
	}
}

template<class TMlfRecord>
void Mlf<TMlfRecord>::Save(const std::string& filename) const
{
	using namespace std;
	ofstream f(filename.c_str());
	if ( !f.good() ) {
		throw runtime_error("Error opening output file '"+filename+"'");
	}
	Save(f);
	f.close();
}

template<class TMlfRecord>
void Mlf<TMlfRecord>::Save(std::ostream& os) const
{
	using namespace std;
	os << "#!MLF!#" << endl;
	SortedMapKeys<typename Base::Container> keys(this->mContainer);
//	vector<string> files;
//	files.reserve(mContainer.size());
//	for (Container::const_iterator it = mContainer.begin(); it != mContainer.end(); it++) {
//		files.push_back(it->first);
//	}
//	sort(files.begin(), files.end());

	for (typename SortedMapKeys<typename Base::Container>::const_iterator it = keys.begin(); it != keys.end(); it++) {
		typename Base::Container::const_iterator it_file = this->mContainer.find(*it);
		assert(it_file != this->mContainer.end());
		os << "\"" << it_file->first << "\"" << endl;
		os << *it_file->second << "." << endl;
	}
}

template<class TMlfRecord>
MlfRecords<TMlfRecord>* Mlf<TMlfRecord>::GetFile(const std::string& file, bool createIfNotExists)
{
	typename Base::Container::const_iterator it = this->mContainer.find(file);
	if (it == this->mContainer.end()) {
		if (createIfNotExists) {
			MlfRecords<TMlfRecord>* f = new MlfRecords<TMlfRecord>;
			this->mContainer.insert(make_pair(file, f));
			return f;
		} else {
			return NULL;
		}
	} else {
		return it->second;
	}
}

template<class TMlfRecord>
const MlfRecords<TMlfRecord>* Mlf<TMlfRecord>::GetFile(const std::string& file) const
{
	typename Base::Container::const_iterator it = this->mContainer.find(file);
	return it == this->mContainer.end() ? NULL : it->second;
}

