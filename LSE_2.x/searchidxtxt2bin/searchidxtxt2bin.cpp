#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/latindexer.h"
#include "../liblse/lattypes.h"
#include "../liblse/searchidxtxtfile.h"

using namespace std;
using namespace lse;

int main(int argc, char * argv[]) 
{
	if (argc == 1) {
		cout << "Usage: "<< argv[0] << " [SEARCHIDX_SORTED_BY_CONFIDENCE.TXT] [SEARCHIDX_SORTED_BY_TIME_WITH_POINTERS.TXT] [SEARCH_OUT.IDX]" << endl;
		return 1;
	}

	char* filename_in_sorted_by_conf = argv[1];
	char* filename_in_sorted_by_time = argv[2];
	string filename_out = argv[3];
	ofstream out;
	SearchIdxTxtFile searchIdxTxtFile;
	SearchIdxTxtRecord rec;

	//--------------------------------------------------
	// the main inverted index - sorted by confidence
	//--------------------------------------------------
	out.open(filename_out.c_str(), ios::binary);
	if (out.bad()) {
		CERR("ERROR: Cannot open file "<<(filename_out).c_str());
		EXIT();
	}

	searchIdxTxtFile.Open(filename_in_sorted_by_conf);

	while(searchIdxTxtFile.Next(&rec))
	{
		DBG("rec: "<<rec);
		rec.WriteTo(out);
	}

	out.close();
	searchIdxTxtFile.Close();

	//--------------------------------------------------
	// another inverted index - sorted by time
	//--------------------------------------------------
	out.open((filename_out + LATINDEXER_EXT_TIMESORT).c_str(), ios::binary);
	if (out.bad()) {
		CERR("ERROR: Cannot open file "<<(filename_out + LATINDEXER_EXT_TIMESORT).c_str());
		EXIT();
	}

	searchIdxTxtFile.Open(filename_in_sorted_by_time);

	while(searchIdxTxtFile.Next(&rec))
	{
		DBG("rec: "<<rec);
		out.write(reinterpret_cast<const char *>(&rec.mRecordIdx), sizeof(rec.mRecordIdx));
	}

	out.close();
	searchIdxTxtFile.Close();




	return 0;
} // main()

