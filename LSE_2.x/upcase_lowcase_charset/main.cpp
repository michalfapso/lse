#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstring>
#include "charset.h"

using namespace std;
using namespace lse;

int main(int argc, char**argv)
{
	if (argc == 1) 
	{
		cerr << "Usage: " << argv[0] << " encoding" << endl;
		cerr << "encoding: cp1250,cp1251,iso8859-1,iso8859-2" << endl;
		exit(1);
	}

	Charset* p_charset;

	int j=1;
	if (strcmp(argv[j], "cp1250") == 0)
	{
		p_charset = new Charset_CP1250;
	}
	else if (strcmp(argv[j], "cp1251") == 0)
	{
		p_charset = new Charset_CP1251;
	}
	else if (strcmp(argv[j], "iso8859-1") == 0)
	{
		p_charset = new Charset_ISO88591;
	}
	else if (strcmp(argv[j], "iso8859-2") == 0)
	{
		p_charset = new Charset_ISO88592;
	}

	string word;
	char c;
	while (!cin.eof())
	{
		c = cin.get();
		if (!cin.good()) break;
#ifdef CASE_LOW
#warning lowcase
		c = p_charset->locase(c);
#else
#warning upcase
		c = p_charset->upcase(c);
#endif
		cout << c;
//		cout << word << " ";
	}
}

