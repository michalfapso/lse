#include <stdlib.h>
#include <iostream>
#include <string>

using namespace std;

bool is_delim(unsigned char c)
{
	return (c==' ' || c=='\t' || c=='\n');
}

int main(int argc, char* argv[])
{
	string q;
	int i=0;
	while (i<argc)
	{
		if (strcmp(argv[i],"-q") == 0)
		{
			i++;
			q = argv[i];
			break;
		}
		i++;
	}

	i = 0;
	int state = 0;
	string w = "";
	unsigned char c;
	bool epsilon = false;
	string source = "";
	bool singlesource = true;
	while (i<q.length())
	{
		if (epsilon) { epsilon = false; } else { c = q[i++]; }
		switch(state)
		{
			case 0:
				if (!is_delim(c))
				{
					epsilon = true;
					state = 1;
				}
				break;
			case 1:
				if (is_delim(c))
				{
					//cout << "w:"<<w<<endl;
					if (w.find('&') == string::npos && w.find('=') == string::npos)
					{
						//cout << "!find" <<endl;
						w += c;
						state = 2;
					}
					else
					{
						if (w.substr(0,4) == "&SRC")
						{
							//cout << "source:" << w.substr(9,3) << endl;
							if (source == "")
							{
								source = w.substr(9,3);
							}
							else if (w.substr(9,3) != source)
							{
								singlesource = false;
							}
						}
						w = "";
						state = 0;
					}
				}
				else
				{
					w += c;
				}
				break;
			case 2:
				w += c;
				break;
		}
	}
	//cout << "search for:" << w << endl;
	if (w[0] == '"')
	{
		w.insert(0,1,'\\');
		w.insert(w.length()-1,1,'\\');
	}
	FILE* ff = fopen("test.ok","w");
	fprintf(ff,"%s",w.c_str());
	fclose(ff);

	if (!singlesource) 
	{
		source = "";
	}
	else
	{
		source += '\\';
	}
//	string run = ".\\data\\searcher\\LSEsearch_srv.exe -hyps-per-stream       2 -time-delta            5 -fwd-nodes-count       0 -bck-nodes-count       0 -phrase-kwd-neighborhood 0.1 -record-delim          _ -record-cutoff-column  2 -port                  7777 -g2p-lexicon           .\\data\\searcher\\gptransc\\EN\\dictionary1.txt -g2p-rules             .\\data\\searcher\\gptransc\\EN\\en.fsm -g2p-symbols           .\\data\\searcher\\gptransc\\EN\\en.sym -g2p-max-variants      2 -g2p-prob-tresh        -1.0 -results-output-type   normal -dont-verify-candidates   -lexicon-idx-lvcsr     .\\data\\searcher\\lexicon\\lvcsr\\ami_lvcsr_dict.lexicon.IDX -meetings-idx-lvcsr    .\\data\\searcher\\idx\\_DOCUMENTS.IDX -search-idx-lvcsr      .\\data\\searcher\\idx\\lvcsr\\"+source+"_INVSEARCH.IDX  -lexicon-idx-phn       .\\data\\searcher\\lexicon\\phn\\ami_phn_dict.lexicon.IDX -meetings-idx-phn      .\\data\\searcher\\idx\\_DOCUMENTS.IDX -search-idx-phn        .\\data\\searcher\\idx\\phn\\"+source+"_INVSEARCH.IDX -dont-verify-candidates -q \"&PHN2NGRAM &EM=true &FNC=0 &BNC=0 &RES=100000 &SEARCHSOURCETYPE=LVCSR_OOVPHN " + w + "\"";
	
	// AMIDA review 2008
	string run = ".\\data\\searcher\\LSEsearch_srv.exe -hyps-per-stream       2 -time-delta            5 -fwd-nodes-count       0 -bck-nodes-count       0 -phrase-kwd-neighborhood 0.1  -results-output-type   normal -dont-verify-candidates   -lexicon-idx-lvcsr     .\\data\\searcher\\lexicon\\lvcsr\\AMIDAfirstReview2008.V1.0.50k.dct.lexicon.IDX -meetings-idx-lvcsr    .\\data\\searcher\\idx\\_DOCUMENTS.IDX -search-idx-lvcsr      .\\data\\searcher\\idx\\lvcsr\\_INVSEARCH.IDX  -dont-verify-candidates -q \"&PHN2NGRAM &EM=true &FNC=0 &BNC=0 &RES=100000 &SEARCHSOURCETYPE=LVCSR " + w + "\"";

	//cout << "system:" << endl << run << endl;

	char line[256];

	//FILE* fp = popen("dir", "r");
	FILE* fp = popen(run.c_str(), "r");
	//FILE* fp = fopen("results.in","r");
	while ( fgets( line, sizeof line, fp))
	{
		printf("%s", line);
	}
	//fclose(fp);
	pclose(fp);
	return 0;
}

