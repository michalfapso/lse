#include <string>
#include <iostream>
#include <fstream>
#include "nlp.h"

using namespace std;

int main(int argc, char**argv)
{
	if (argc == 1) 
	{
		cerr << "Usage: " << argv[0] << " regex_replace_list_file" << endl;
		exit(1);
	}

	Nlp::RegexReplaceList regex_replace_list;
	regex_replace_list.LoadFromFile(argv[1]);

	string word;
	while (cin >> word)
	{
		Nlp::WordList forms_list;
		Nlp::ExpandByRegexList(word, regex_replace_list, &forms_list);
		if (forms_list.size() == 0)
		{
			cout << word << " ";
		}
		else for (Nlp::WordList::iterator iFormStr=forms_list.begin(); iFormStr!=forms_list.end(); iFormStr++)
		{
			cout << *iFormStr << " ";
//			break; // we need to output only one variant
		}
		cout << endl;
	}
}
