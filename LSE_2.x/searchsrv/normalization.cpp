#include "normalization.h"
#include <cmath>
#include "mymath.h"

#define MAX_WORD_LENGTH_PHONEMES 14
#define NORMSCORE_INF_TO_MEANINGFUL_VALUE 15.0f // when the result of normalization is +-infinity, this value is returned instead
#define NORMSCORE_INF 10000000000.0f // value above which the NORMSCORE_INF_TO_MEANINGFUL_VALUE will be applied

using namespace lse;
using namespace std;

void Normalization::LoadNormVector(NormVector** ppNormVector, const char* filename)
{
	if (*ppNormVector) delete *ppNormVector;
	*ppNormVector = new NormVector;

	list<double> norm_list;

	ifstream f(filename);
	if (!f.good())
	{
		CERR("ERROR: Normalization::LoadNormVector('"<<filename<<"'): File not found.");
		EXIT();
	}
	string str;
	while(f >> str)
	{
		norm_list.push_back(atof(str.c_str()));
	}

	(*ppNormVector)->init(norm_list.size(), 1);
	int ii=0;
	for (list<double>::iterator i=norm_list.begin(); i!=norm_list.end(); i++)
	{
		(*ppNormVector)->set(ii++, 0, *i);
	}
}

void Normalization::LoadPhonemeList(const char* filename)
{
	if (mpPhonemeList) delete mpPhonemeList;
	mpPhonemeList = new PhonemeList;

	ifstream f(filename);
	if (!f.good())
	{
		CERR("ERROR: Normalization::LoadNormVector('"<<filename<<"'): File not found.");
		EXIT();
	}
	string str;
	unsigned int i=0;
	while(f >> str)
	{
		mpPhonemeList->push_back(str);
		mPhoneme2IndexMap.insert(make_pair(str, i++));
	}
}

double Normalization::NormalizeScore(Hypothesis::Words* pWords, Confidence score)
{
	Mat<double> v;
	unsigned int v_size = mpPhonemeList->size() + MAX_WORD_LENGTH_PHONEMES + 2; // one more item for the logit-score and another one for the last item with the value 1
	v.init(1, v_size);
	v.set(0,0,logit(exp(score)));
	for (unsigned int i=1; i<v_size-1; i++)
	{
		v.set(0,i, 0);
	}
	v.set(0, v_size-1, 1); // set 1 for the last item

	NormVector* p_norm_vector = NULL;

	unsigned int phoneme_length = 0;
	for (Hypothesis::Words::iterator iWord = pWords->begin();
		 iWord != pWords->end();
		 iWord ++)
	{
		DBG_FORCE("iWord:"<<iWord->str<<" ... "<<iWord->pron);

		// multigram
		if (iWord->str[0] == '_')
		{
			list<string> phonemes;
			string_split(iWord->str, "-", &phonemes);
			phonemes.begin()->erase(0,1); // erase the leading _
			for (list<string>::iterator i=phonemes.begin(); i!=phonemes.end(); i++)
			{
				v.add(0, (int)Phoneme2Index(*i), 1);
				phoneme_length++;
			}
			if (p_norm_vector)
			{
				CERR("ERROR: The whole query has to contain either only words or only mgrams.");
				EXIT();
			}
			p_norm_vector = mpNormVectorMgrams;
		}
		else 
		// word
		{
			list<gpt::PhnTrans::pt_entry> pron_list;
			DBG_FORCE("g2p->GetTranscs("<<iWord->str<<")");
			mpG2P->GetTranscs(iWord->str, &pron_list);
			//mpG2P->GetTranscs(mpCharset->locase(iWord->str), &pron_list);
			DBG_FORCE("pron_list.size():"<<pron_list.size());
			for (list<gpt::PhnTrans::pt_entry>::iterator iPron=pron_list.begin(); iPron!=pron_list.end(); ++iPron)
			{
				DBG_FORCE("PRON: "<<iPron->trans);

				list<string> phonemes;
				string_split(iPron->trans, " ", &phonemes);
				for (list<string>::iterator i=phonemes.begin(); i!=phonemes.end(); i++)
				{
					v.add(0, (int)Phoneme2Index(*i), 1);
					phoneme_length++;
				}
				break; // take into account only the first pronunciation variant
			}
			if (p_norm_vector)
			{
				CERR("ERROR: The whole query has to contain either only words or only mgrams.");
				EXIT();
			}
			p_norm_vector = mpNormVectorWords;
		}
	}

	// set 1 for the item defining the length of the phoneme string of the query
	if (phoneme_length > MAX_WORD_LENGTH_PHONEMES) 
	{ 
		phoneme_length = MAX_WORD_LENGTH_PHONEMES; 
	}
	v.set(0, mpPhonemeList->size() + phoneme_length - 1, 1);

	// print v
	DBG("Score: "<<v.get(0,0));
	for (unsigned int i=1; i<mpPhonemeList->size(); i++)
	{
		DBG(i << "(" << (*mpPhonemeList)[i-1] << "): " << v.get(0,i));
	}
	for (unsigned int i=mpPhonemeList->size(); i<v_size; i++)
	{
		DBG(i << ": " << v.get(0,i));
	}

	assert(p_norm_vector);
	double dotp = dotproduct(&v, p_norm_vector);
	if (dotp > NORMSCORE_INF) { 
		return NORMSCORE_INF_TO_MEANINGFUL_VALUE;
	} else if (dotp == -NORMSCORE_INF) {
		return -NORMSCORE_INF_TO_MEANINGFUL_VALUE;
	}
	return dotp;
}

unsigned int Normalization::Phoneme2Index(const string& phoneme)
{
#ifndef NDEBUG // like assert()
	if (mPhoneme2IndexMap.find(phoneme) == mPhoneme2IndexMap.end())
	{
		CERR("ERROR: phoneme '"<<phoneme<<"' was not found in the normalization phoneme list");
		for (Phoneme2IndexMap::iterator i=mPhoneme2IndexMap.begin(); i!=mPhoneme2IndexMap.end(); i++)
		{
			DBG(i->first<<" -> "<<i->second);
		}
		EXIT();
	}
#endif
	return mPhoneme2IndexMap[phoneme];
}

