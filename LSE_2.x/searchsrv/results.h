#ifndef RESULTS_H
#define RESULTS_H

#include <string>
#include <iostream>
#include <map>
#include <vector>
#include "hypothesis.h"
#include "querykwdlist.h"
#include "normlexicon.h"
#include "searchconfig.h"
#include "common.h"

namespace lse {

class RecordStream;

typedef std::vector< lse::Hypothesis > ResultsBaseType;

class Results;
//================================================================================
//	LocalResults
//================================================================================
class LocalResults
{
	public:
		Results* mpResults;
		QueryKwdList* mpQueryKwdList;
		QueryKwdList::iterator miKwdBegin;
		QueryKwdList::iterator miKwdEnd;
};


//class Results : public std::map< RecordStream, std::vector< lse::Hypothesis > > // map (record+stream -> hypothesis list)
class Results : public ResultsBaseType // map (record+stream -> hypothesis list)
{
		int mAllDocumentsCount;
		int mMaxResultsCount;
		QueryKwdList *mpQueryKwdList;
		float mHardDecisionTreshold;
		
		typedef std::list<lse::Hypothesis *> HypothesisList;
		struct DocumentGroup
		{
			~DocumentGroup();
			HypothesisList * mpHypList;
			std::string      mDocumentName;
			ID_t             mDocumentId;
			unsigned int     mOccurrencesTotal;
			unsigned int     mOccurrencesAboveThreshold;
			float            mScore;
		};
		class DocumentGroups : public std::map<ID_t, DocumentGroup* > 
		{
			public:
				~DocumentGroups();
				unsigned int mDocumentsTotal;
				unsigned int mDocumentsAboveThreshold;
				unsigned int mOccurrencesTotal;
				unsigned int mOccurrencesAboveThreshold;
				std::vector<int> mOrderVector;

				static bool CmpDocuments(DocumentGroups *pDocumentGroups, const int &a, const int &b);
		};
		//typedef std::map<ID_t, DocumentGroup* > DocumentGroups;
		DocumentGroups mDocumentGroups;

		void joinOverlappingResults();
		void sortResults();
		void fillDocumentGroups();

	public:

		int mOovCount;

		Results() : 
			ResultsBaseType(), 
			mAllDocumentsCount(0),
			mMaxResultsCount(-1),
			mHardDecisionTreshold(0.0),
			mOovCount(0)
		{};
		
		void SetAllDocumentsCount(int count) { mAllDocumentsCount = count; }
		void SetHardDecisionTreshold(float treshold) { mHardDecisionTreshold = treshold; }
		void SetQueryKwdList(QueryKwdList *pQueryKwdList);
		void SetMaxHypothesesCount(int max);

		void push_back(lse::Hypothesis hyp);
		void postProcess(Normalization* pNormalization);
		void print(std::ostream *os, SearchConfig* config, float searchTime);
		void NormalizeResults(Normalization* pNormalization);
};


class RecordStream {

	public:	
		std::string record;
		std::string stream;

		RecordStream(std::string record, std::string stream) {
			this->record = record;
			this->stream = stream;
		}
		
		friend bool operator<(const RecordStream& l, const RecordStream& r) {
			if (l.record == r.record) {
				return l.stream < r.stream;
			} else {
				return l.record < r.record;
			}
		}
};

} // namespace lse

#endif
