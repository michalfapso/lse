#include "searchconfig.h"
#include "latbinfile.h"

using namespace lse;
using namespace std;

SearchConfig::~SearchConfig()
{
	if (p_charset)
		delete p_charset;
	if (lexiconLvcsr)
		delete lexiconLvcsr;
	if (lexiconPhn)
		delete lexiconPhn;
	if (indexerLvcsr)
		delete indexerLvcsr;
	if (indexerPhn)
		delete indexerPhn;
	if (mpNormalization)
		delete mpNormalization;
	if (g2p)
		delete g2p;
	if (p_word_mapper)
		delete p_word_mapper;
}

void SearchConfig::ParseCommandLineArguments(int argc, char** &argv)
{
	if (argc == 1) {
		cout << "Usage: " << argv[0] << " OPTIONS" << endl;

		cout << endl;
		cout << "GENERAL OPTIONS:" << endl;
		cout << "-lexicon-idx-lvcsr lexicon.IDX: LVCSR system's lexicon" << endl;
		cout << "-lexicon-idx-phn lexicon.IDX: phonemes plus trigrams lexicon" << endl;
		cout << "-search-idx-lvcsr inv.IDX: inverted index for the LVCSR system" << endl;
		cout << "-search-idx-phn inv.IDX: inverted index for the phoneme-based system" << endl;
		cout << "-documents-idx-lvcsr documents.IDX: index of documents (LVCSR)" << endl;
		cout << "-documents-idx-phn documents.IDX: index of documents (phn)" << endl;
		cout << "-q query: search query" << endl;

		//cout << "-hyps-per-stream" << endl;
		//cout << "-clusters-count" << endl;
		//cout << "-time-delta float: time in seconds allowed between two detections to be considered as a phrase" << endl;
		cout << "-phrase-kwd-neighborhood float: time in seconds allowed between two detections to be considered as a phrase" << endl;
		//cout << "-fwd-nodes-count" << endl;
		//cout << "-bck-nodes-count" << endl;
		cout << "-data-dir-path: in case of verification in lattices" << endl;
		cout << "-data-dir-path-phn path: path to lattices in case the verification in lattices is turned on (LVCSR)" << endl;
		cout << "-data-dir-path-lvcsr path: path to lattices in case the verification in lattices is turned on (phn)" << endl;
		cout << "-port port_number: port on which the searching server will listen. If no port is defined, the searcher acts like a standalone application" << endl;
		cout << "-results-output-type [xml,xml_std,exp,xml_transcripts,mlf]: type of search results output" << endl;
		cout << "-dont-verify-candidates: do not verify search results candidates in lattices" << endl;
		cout << "-results-start-time-subtractor float: time in seconds which will be substracted from each search result's start time" << endl;
		cout << "-charset [cp1250,latin2,latin1]: charset of the query. Needs to be specified when the search engine has to convert the query to upper/lower case" << endl;
		cout << "-upcase-query: the query will be automaticaly converted to uppercase by the search engine" << endl;
		cout << "-word2mgrams cmd: command used for converting a word to multigrams (%s in the cmd will be substituted by the word)" << endl;
		cout << "-mgram-statistics filename: file with multigram statistics used for segmentation of a phoneme string into multigrams" << endl;
		//cout << "-mgram-max-length length: max number of multigrams (default = 5)" << endl;
		cout << "-word2forms cmd: command used for generating other forms (synonyms) of the same word (%s in the cmd will be substituted by the word)" << endl;
#ifdef USE_REGEX
		cout << "-word2forms-regexlist filename: file with a list of regular expressions used for generating other forms of words" << endl;
#endif

		cout << endl;
		cout << "VERBOSITY OPTIONS:" << endl;
		cout << "-show-all-matched-keywords" << endl;
		cout << "-show-all-clusters" << endl;
		cout << "-show-reverse-index-lookup-results" << endl;
		cout << "-show-reverse-index-lookup-results-clusters" << endl;
		cout << "-time-index-print" << endl;
		cout << "-dbg" << endl;

		//cout << "-meeting-list" << endl;
		//cout << "-word-clusters" << endl;
		//cout << "-max-results" << endl;
		//cout << "-only-print-candidates" << endl;
		//cout << "-only-print-candidate-trigrams" << endl;
		//cout << "-xml-output" << endl;

		//cout << "-record-delim" << endl;
		//cout << "-record-cutoff-column" << endl;
		//cout << "-record-channel-column" << endl;

		cout << endl;
		cout << "NORMALIZATION OPTIONS:" << endl;
		cout << "-normalization-phonemelist filename: phoneme list" << endl;
		cout << "-normalization-vector-words filename: vector used for words normalization (text file with numbers)" << endl;
		cout << "-normalization-vector-mgrams filename:  vector used for multigrams normalization (text file with numbers)" << endl;

		//cout << "-hard-decision-treshold" << endl;

		cout << endl;
		cout << "G2P OPTIONS:" << endl;
		cout << "-g2p-lexicon filename: pronunciation lexicon" << endl;
		cout << "-g2p-rules filename: trained g2p rules" << endl;
		cout << "-g2p-symbols filename: g2p symbols" << endl;
		cout << "-g2p-max-variants count: max variants the g2p will generate" << endl;
		cout << "-g2p-scale-prob: g2p scaling probability" << endl;
		cout << "-g2p-prob-tresh: threshold for generated pronunciation variants" << endl;


		//cout << "-phn-query-multiword-to-singleword" << endl;
		//cout << "-phn-query-multiword" << endl;
		//cout << "-results-convert-to-local-time" << endl;
		//cout << "-lat-filename-start-time-field" << endl;
		//cout << "-latname-time-multiplier" << endl;

		exit(1);
	}

//	DBG_FORCE("ParseCommandLineArguments("<<argv<<")");
	int j=1;
	while (j<argc) {

		DBG_FORCE("argv["<<j<<"]:" << argv[j]);
			
		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		
		} else if (strcmp(argv[j], "-lexicon-idx") == 0) {

			CERR("ERROR: instead of -lexicon-idx use -lexicon-idx-[lvcsr|phn]");
			EXIT();
			
		} else if (strcmp(argv[j], "-lexicon-idx-lvcsr") == 0) {
			
			j++;
			DBG_FORCE("argv[j]:"<<argv[j]);
			p_lexicon_lvcsr = argv[j];
			DBG("Loading lexicon " << p_lexicon_lvcsr << "...");
			lexiconLvcsr = new Lexicon(Lexicon::readonly);
			if (lexiconLvcsr->loadFromFile_readonly(p_lexicon_lvcsr) != 0) {
				CERR("ERROR: Lexicon input file " << p_lexicon_lvcsr << " not found");
				EXIT();
			}
			normLexicon_lvcsr.SetWordLexicon(lexiconLvcsr);
			DBG("done");
			
		} else if (strcmp(argv[j], "-lexicon-idx-phn") == 0) {
			
			j++;
			p_lexicon_phn = argv[j];
			DBG("Loading lexicon " << p_lexicon_phn << "...");
			lexiconPhn = new Lexicon(Lexicon::readonly);
			if (lexiconPhn->loadFromFile_readonly(p_lexicon_phn) != 0) {
				CERR("ERROR: Lexicon input file " << p_lexicon_phn << " not found");
				EXIT();
			}
			normLexicon_phn.SetWordLexicon(lexiconPhn);
			DBG("done");
			
		} else if (strcmp(argv[j], "-search-idx") == 0) {

			CERR("ERROR: instead of -search-idx use -search-idx-[lvcsr|phn]");
			EXIT();
			
		} else if (strcmp(argv[j], "-search-idx-lvcsr") == 0) {

			j++;
			p_searchindex_lvcsr = argv[j];
			if (!indexerLvcsr)
				indexerLvcsr = new Lattice::Indexer;
			if (!indexerLvcsr->lattices.LoadIndex(p_searchindex_lvcsr)) {
				CERR("ERROR: Loading inverted index failed.");
				EXIT();
			}

		} else if (strcmp(argv[j], "-search-idx-phn") == 0) {

			j++;
			p_searchindex_phn = argv[j];
			
		} else if (strcmp(argv[j], "-documents-idx") == 0) {

			CERR("ERROR: instead of -meeting-idx use -meeting-idx-[lvcsr|phn]");
			EXIT();
			
		} else if (strcmp(argv[j], "-documents-idx-lvcsr") == 0) {

			j++;
			p_meeting = argv[j];
			DBG("Loading meeting indices " << p_meeting << "...");
			if (!indexerLvcsr)
				indexerLvcsr = new Lattice::Indexer;
			if (indexerLvcsr->meetings.loadFromFile(p_meeting) != 0) {
				CERR("ERROR: Meetings indexer input file " << p_meeting << " not found ... using empty indexer");
				EXIT();
			}
			DBG("done");
		
		} else if (strcmp(argv[j], "-documents-idx-phn") == 0) {

			j++;
			p_meeting = argv[j];
			DBG("Loading meeting indices " << p_meeting << "...");
			if (indexerPhn == NULL)
			{
				indexerPhn = new Lattice::Indexer;
			}
			if (indexerPhn->meetings.loadFromFile(p_meeting) != 0) {
				CERR("ERROR: Meetings indexer input file " << p_meeting << " not found ... using empty indexer");
				EXIT();
			}
			DBG("done");
		


			
		} else if (strcmp(argv[j], "-q") == 0) {

			j++;
			p_query = argv[j];			
			DBG_FORCE("p_query: "<<p_query);
			
		} else if (strcmp(argv[j], "-hyps-per-stream") == 0) {

			j++;
			p_hyps_per_stream = atoi(argv[j]);

		} else if (strcmp(argv[j], "-clusters-count") == 0) {
			
			j++;
			p_clusters_count = atoi(argv[j]);

		} else if (strcmp(argv[j], "-time-delta") == 0) {

			j++;
			p_time_delta = atof(argv[j]);

		} else if (strcmp(argv[j], "-fwd-nodes-count") == 0) {

			j++;
			p_fwd_nodes_count = atoi(argv[j]);

		} else if (strcmp(argv[j], "-bck-nodes-count") == 0) {

			j++;
			p_bck_nodes_count = atoi(argv[j]);
			
		} else if (strcmp(argv[j], "-data-dir-path") == 0) {
			CERR("ERROR: -data-dir-path switch does not exist. Use -data-dir-path-phn or -data-dir-path-lvcsr instead.");
			EXIT();
		
		} else if (strcmp(argv[j], "-data-dir-path-phn") == 0) {
			j++;
			p_data_dir_path_phn = argv[j];
			if (p_data_dir_path_phn[p_data_dir_path_phn.length()-1] == PATH_SLASH)
			{
				p_data_dir_path_phn.erase(p_data_dir_path_phn.length()-1); // remove the last char (slash)
			}
		
		} else if (strcmp(argv[j], "-data-dir-path-lvcsr") == 0) {
			j++;
			p_data_dir_path_lvcsr = argv[j];
			if (p_data_dir_path_lvcsr[p_data_dir_path_lvcsr.length()-1] == PATH_SLASH)
			{
				p_data_dir_path_lvcsr.erase(p_data_dir_path_lvcsr.length()-1); // remove the last char (slash)
			}
			
		} else if (strcmp(argv[j], "-show-all-matched-keywords") == 0) {
			
			p_show_all_matched_keywords = true;
			
		} else if (strcmp(argv[j], "-show-all-clusters") == 0) {

			p_show_all_clusters = true;
			
		} else if (strcmp(argv[j], "-show-reverse-index-lookup-results") == 0) {

			p_show_reverse_index_lookup_results = true;
			
		} else if (strcmp(argv[j], "-show-reverse-index-lookup-results-clusters") == 0) {

			p_show_reverse_index_lookup_results_clusters = true;
			
		} else if (strcmp(argv[j], "-time-index-print") == 0) {
			
			p_time_index_print = true;
			j++;
			LatBinFile latBinFile_tmp;
			latBinFile_tmp.print_TimeIndex(argv[j]);

		} else if (strcmp(argv[j], "-dbg") == 0) {
			
			p_dbg = true;
			
		} else if (strcmp(argv[j], "-phrase-kwd-neighborhood") == 0) {
			
			j++;
			p_phrase_kwd_neiborhood = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-meeting-list") == 0) {

			j++;
			p_meeting_list = argv[j];

		} else if (strcmp(argv[j], "-word-clusters") == 0) {

			p_word_clusters = true;

		} else if (strcmp(argv[j], "-max-results") == 0) {

			j++;
			p_max_results = atoi(argv[j]);
			
		} else if (strcmp(argv[j], "-port") == 0) {

			j++;
			p_port = atoi(argv[j]);

		} else if (strcmp(argv[j], "-only-print-candidates") == 0) {

			p_only_print_candidates = true;
	
		} else if (strcmp(argv[j], "-only-print-candidate-trigrams") == 0) {

			p_only_print_candidate_trigrams = true;
	
		} else if (strcmp(argv[j], "-xml-output") == 0) {

			p_results_output_type = ResultsETypeXml;
	
		} else if (strcmp(argv[j], "-results-output-type") == 0) {

			j++;
			if (strcmp(argv[j], "normal") == 0)
			{
				p_results_output_type = ResultsETypeNormal;
			}
			else if (strcmp(argv[j], "xml") == 0)
			{
				p_results_output_type = ResultsETypeXml;
			}
			else if (strcmp(argv[j], "xml_std") == 0)
			{
				p_results_output_type = ResultsETypeXml_std;
			}
			else if (strcmp(argv[j], "exp") == 0)
			{
				p_results_output_type = ResultsETypeExp;
			}
			else if (strcmp(argv[j], "xml_transcripts") == 0)
			{
				p_results_output_type = ResultsETypeXmlTranscripts;
			}
			else if (strcmp(argv[j], "mlf") == 0)
			{
				p_results_output_type = ResultsETypeMlf;
			}
	
		} else if (strcmp(argv[j], "-record-delim") == 0) {

			j++;
			p_record_delim = argv[j][0];

		} else if (strcmp(argv[j], "-record-cutoff-column") == 0) {

			j++;
			p_record_cutoff_column = atoi(argv[j]);

		} else if (strcmp(argv[j], "-record-channel-column") == 0) {

			j++;
			p_record_channel_column = atoi(argv[j]);

		} else if (strcmp(argv[j], "-normalization-phonemelist") == 0) {

			j++;
			if (!mpNormalization) { mpNormalization = new Normalization(); }
			mpNormalization->LoadPhonemeList(argv[j]);

		} else if (strcmp(argv[j], "-normalization-vector-words") == 0) {

			j++;
			if (!mpNormalization) { mpNormalization = new Normalization(); }
			mpNormalization->LoadNormVectorWords(argv[j]);

		} else if (strcmp(argv[j], "-normalization-vector-mgrams") == 0) {

			j++;
			if (!mpNormalization) { mpNormalization = new Normalization(); }
			mpNormalization->LoadNormVectorMgrams(argv[j]);
/*
		} else if (strcmp(argv[j], "-normalization-lexicon-lvcsr") == 0) {

			j++;
			p_normalization_lexicon_lvcsr = argv[j];
			normLexicon_lvcsr.LoadFromNormLexiconFile(p_normalization_lexicon_lvcsr);

		} else if (strcmp(argv[j], "-normalization-lexicon-phn") == 0) {

			j++;
			p_normalization_lexicon_phn = argv[j];
			normLexicon_phn.LoadFromNormLexiconFile(p_normalization_lexicon_phn);
*/
		} else if (strcmp(argv[j], "-hard-decision-treshold") == 0) {

			j++;
			p_hard_decision_treshold = atof(argv[j]);
/*
		} else if (strcmp(argv[j], "-g2p-dictionary") == 0) {

			j++;
			p_g2p_dictionary = argv[j];
*/
		} else if (strcmp(argv[j], "-g2p-lexicon") == 0) {

			j++;
			p_g2p_lexicon = argv[j];

		} else if (strcmp(argv[j], "-g2p-rules") == 0) {

			j++;
			p_g2p_rules = argv[j];

		} else if (strcmp(argv[j], "-g2p-symbols") == 0) {

			j++;
			p_g2p_symbols = argv[j];

		} else if (strcmp(argv[j], "-g2p-max-variants") == 0) {

			j++;
			p_g2p_max_variants = atoi(argv[j]);

		} else if (strcmp(argv[j], "-g2p-scale-prob") == 0) {

			j++;
			p_g2p_scale_prob = true;

		} else if (strcmp(argv[j], "-g2p-prob-tresh") == 0) {

			j++;
			p_g2p_prob_tresh = atof(argv[j]);
/*
		} else if (strcmp(argv[j], "-wipen") == 0) {

			j++;
			p_wipen = atof(argv[j]);

		} else if (strcmp(argv[j], "-pipen") == 0) {

			j++;
			p_pipen = atof(argv[j]);
*/
		} else if (strcmp(argv[j], "-phn-query-multiword-to-singleword") == 0) {

			p_phn_query_multiword_to_singleword = true;

		} else if (strcmp(argv[j], "-phn-query-multiword") == 0) {

			p_phn_query_multiword_to_singleword = false;

		} else if (strcmp(argv[j], "-dont-verify-candidates") == 0) {

			p_verify_candidates = false;

		} else if (strcmp(argv[j], "-results-convert-to-local-time") == 0) {

			p_results_convert_to_local_time = true;

		} else if (strcmp(argv[j], "-lat-filename-start-time-field") == 0) {

			j++;
			p_lat_filename_start_time_field = atoi(argv[j]);

		} else if (strcmp(argv[j], "-latname-time-multiplier") == 0) {

			j++;
			p_latname_time_multiplier = atof(argv[j]);

		} else if (strcmp(argv[j], "-results-start-time-subtractor") == 0) {

			j++;
			p_results_start_time_subtractor = atof(argv[j]);

		} else if (strcmp(argv[j], "-charset") == 0) {

			j++;
			if (strcmp(argv[j], "cp1250") == 0)
			{
				p_charset = new Charset_CP1250;
			}
			else if (strcmp(argv[j], "latin2") == 0)
			{
				p_charset = new Charset_ISO88592;
			}
			else if (strcmp(argv[j], "latin1") == 0)
			{
				p_charset = new Charset_ISO88591;
			}

		} else if (strcmp(argv[j], "-upcase-query") == 0) {

			p_upcase_query = true;

		} else if (strcmp(argv[j], "-word2mgrams") == 0) {

			j++;
			p_word2mgrams = argv[j];

		} else if (strcmp(argv[j], "-word2forms") == 0) {

			j++;
			p_word2forms = argv[j];

		} else if (strcmp(argv[j], "-word2forms-regexlist") == 0) {

			j++;
			p_word2forms_regexlist = argv[j];

		} else if (strcmp(argv[j], "-mgram-statistics") == 0) {

			j++;
			p_mgram_statistics = argv[j];

		} else if (strcmp(argv[j], "-mgram-max-length") == 0) {

			j++;
			p_mgram_max_length = atoi(argv[j]);

		} else if (strcmp(argv[j], "-word-mapping") == 0) {

			j++;
			p_word_mapping_list = argv[j];

		} else if (strcmp(argv[j], "-word-clusters-list") == 0) {

			j++;
			DBG("argv[j]:"<<argv[j]);
			p_word_clusters_list = argv[j];
//			p_word_clusters_list.resize(500);
//			p_word_clusters_list.assign(argv[j]);

		} else if (strcmp(argv[j], "-kwd-max-occurrences-count") == 0) {

			j++;
			p_kwd_max_occurrences_count = atoi(argv[j]);

		} else if (strcmp(argv[j], "-kwd-occurrence-threshold") == 0) {

			j++;
			p_kwd_occurrence_threshold = atof(argv[j]);

		} else if (strcmp(argv[j], "-max-clients-connected") == 0) {

			j++;
			p_max_clients_connected = atoi(argv[j]);

		} else if (strcmp(argv[j], "-tcp-service-queue-length") == 0) {

			j++;
			p_tcp_service_queue_length = atoi(argv[j]);

		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				CERR("Unknown switch: " << argv[j]);
				EXIT();
			}
		}
		
		j++;
	}

	if (p_word_clusters_list != "" || p_word_mapping_list != "")
	{
		assert(lexiconLvcsr && !lexiconPhn);

		if (!p_word_mapper) {
			p_word_mapper = new WordMapper;
		}
		p_word_mapper->SetLexicon(lexiconLvcsr);

		if (p_word_mapping_list != "" && !p_word_mapper->LoadMappingFromFile(p_word_mapping_list))
		{
			CERR("ERROR while loading word mappings from "<<p_word_mapping_list);
			EXIT();
		}

		if (p_word_clusters_list != "" && !p_word_mapper->LoadClustersFromFile(p_word_clusters_list))
		{
			CERR("ERROR while loading word clusters from "<<p_word_clusters_list);
			EXIT();
		}
	}

	if ((p_mgram_statistics != "" or p_word2mgrams != "") && p_lexicon_phn == "")
	{
		CERR("-lexicon-phn is not defined, although the multigram search is needed. In case of using a hybrid word/multigram recognizer, set the -lexicon-phn to the same value as -lexicon-lvcsr, which should contain both words and multigrams.");
		EXIT();
	}

	DBG_FORCE("g2p->Load("<<p_g2p_rules<<", "<<p_g2p_symbols<<", "<<p_g2p_lexicon<<", "<<p_g2p_max_variants<<", "<<p_g2p_scale_prob<<", "<<p_g2p_prob_tresh<<")");
	if (p_g2p_lexicon != "")
	{
		g2p = new G2P;
		g2p->Load(p_g2p_rules, p_g2p_symbols, p_g2p_lexicon, p_g2p_max_variants, p_g2p_scale_prob, p_g2p_prob_tresh);
	}

	if (mpNormalization)
	{
		if (g2p) { mpNormalization->SetG2P(g2p); }
		if (p_charset) { mpNormalization->SetCharset(p_charset); }

		if (!mpNormalization->Good())
		{
			CERR("ERROR: Normalization needs a g2p, charset, normalization vector and a phoneme list (-g2p-*, -charset, -normalization-vector-words, -normalization-vector-mgrams, -normalization-phonemelist)");
			EXIT()
		}
	}
/*
	if (p_g2p_dictionary != "")
	{
		dct = new Dct(lexiconLvcsr);
		DctFile dct_file(p_g2p_dictionary);
		DctFileRec dct_rec;
		while(dct_file.Next(&dct_rec))
		{
			dct.Add(dct_rec);
		}
	}
*/
}

LatMeetingIndexer* SearchConfig::GetMeetingIndexer()
{
	if (indexerLvcsr)
	{
		return &(indexerLvcsr->meetings);
	}
	else if (indexerPhn)
	{
		return &(indexerPhn->meetings);
	}
	else
	{
		CERR("ERROR: No indexer is set");
		EXIT();
	}
}

