#ifndef HYPOTHESIS_H
#define HYPOTHESIS_H

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <iomanip>
#include "lattypes.h"
#include "latindexer.h"


namespace lse {


/// list of keywords and list of context words
class Hypothesis {
		
public:

	class Word {
	public:
		std::string str;
		LatTime start;
		LatTime end;
		Confidence conf;		
		std::string pron; ///< pronunciation variant
		
		friend std::ostream& operator<<(std::ostream& os, const Hypothesis::Word& w) {
			return os << w.str << "\tSTART " << w.start << "\tEND " << w.end;// << "\tCONFIDENCE " << w.conf;
		}
	
		friend bool operator<(const Hypothesis::Word& l, const Hypothesis::Word &r)
		{
			return (l.start + (l.end-l.start)/2) < (r.start + (r.end-r.start)/2);
		}
	};


	std::string stream;
	std::string record;
	ID_t        recordId;
	std::string channel;
	bool        decision;
	float       score;
	float 		mStartTime;
	float 		mEndTime;
	
	typedef std::list<Hypothesis::Word> Keywords;
	typedef std::list<Hypothesis::Word> Words;

	Words words;
	Keywords keywords;
	int keywordsCount;


	Hypothesis() 
	{
		stream = "";
		record = "";
		recordId = 0;
		channel = "";
		decision = false;
		score = 0.0;
		keywordsCount = 0;
		mStartTime = INF;
		mEndTime = -INF;
	}
	
	// copy constructor:
	Hypothesis(const Hypothesis &h)
	{
		stream = h.stream;
		record = h.record;
		recordId = h.recordId;
		channel = h.channel;
		decision = h.decision;
		score = h.score;
		keywordsCount = h.keywordsCount;
		mStartTime = h.mStartTime;
		mEndTime = h.mEndTime;
		for(Keywords::const_iterator i=h.keywords.begin(); i!=h.keywords.end(); ++i) {
			keywords.push_back(*i);
		}
		for(Words::const_iterator i=h.words.begin(); i!=h.words.end(); ++i) {
			words.push_back(*i);
		}
	}

	// ostream operator
	friend std::ostream& operator<<(std::ostream& os, const Hypothesis& hyp) {
		os << "NUM_KWDS: \t" << hyp.keywords.size() << "("<<hyp.keywordsCount<<")" << std::endl;
		for (Keywords::const_iterator i=hyp.keywords.begin(); i!=hyp.keywords.end(); ++i) {
			os << "KWD \t" << *i << "\tCONFIDENCE " << hyp.score << std::endl;
		}
		os << "NUM_WORDS: \t" << hyp.words.size() << std::endl;
		for (Words::const_iterator i=hyp.words.begin(); i!=hyp.words.end(); ++i) {
			os << "WORD \t" << *i << "\tCONFIDENCE " << hyp.score << std::endl;
		}
		
		return os;
	}

	void printXml(std::ostream *os);
	void printText(std::ostream *os);

	// comparator
	friend bool operator<(const Hypothesis& l, const Hypothesis& r);
	
	void push_back_word(const std::string& str, LatTime start, LatTime end, Confidence conf);
	void push_back_word(Hypothesis::Word w); 
	
	void push_front_word(const std::string& str, LatTime start, LatTime end, Confidence conf);
	void push_front_word(Hypothesis::Word w);
	
	void addKeyword(const std::string& str, LatTime start, LatTime end, Confidence conf);
	void addKeyword(Hypothesis::Word w);
	void addKeywordBack(Hypothesis::Word w);
	void addKeywordFront(Hypothesis::Word w);
	
	void updateKeywordCounter(const std::string& str);
	void updateTimeBoundaries(float startTime, float endTime);
		
	void setFrontWordStartTime(LatTime t);

};

} // namespace lse

#endif

