#ifndef NORMALIZATION_H
#define NORMALIZATION_H

#include "lattypes.h"
#include "hypothesis.h"
#include "g2p.h"
#include "charset.h"
#include "matrix.h"

namespace lse {

class Normalization
{
		typedef std::vector<std::string> PhonemeList;
		typedef Mat<double> NormVector;
		typedef std::map<std::string, unsigned int> Phoneme2IndexMap;

		PhonemeList* mpPhonemeList;
		NormVector* mpNormVectorWords;
		NormVector* mpNormVectorMgrams;
		G2P* mpG2P;
		Charset* mpCharset;
		Phoneme2IndexMap mPhoneme2IndexMap;

		unsigned int Phoneme2Index(const std::string& phoneme);
		void LoadNormVector(NormVector** ppNormVector, const char* filename);
	public:
		Normalization() : mpPhonemeList(NULL), mpNormVectorWords(NULL), mpNormVectorMgrams(NULL), mpG2P(NULL), mpCharset(NULL) {}
		~Normalization() 
		{
			if (mpPhonemeList) delete mpPhonemeList;
			if (mpNormVectorWords) delete mpNormVectorWords;
			if (mpNormVectorMgrams) delete mpNormVectorMgrams;
		}

		bool Good() { 
			//DBG("mpPhonemeList:"<<mpPhonemeList<<" mpNormVectorWords:"<<mpNormVectorWords<<" mpNormVectorMgrams:"<<mpNormVectorMgrams<<" mpG2P:"<<mpG2P<<" mpCharset:"<<mpCharset);
			return mpPhonemeList && (mpNormVectorWords || mpNormVectorMgrams) && mpG2P && mpCharset; 
		}

		void LoadNormVectorWords(const char* filename) { LoadNormVector(&mpNormVectorWords, filename); }
		void LoadNormVectorMgrams(const char* filename) { LoadNormVector(&mpNormVectorMgrams, filename); };
		void LoadPhonemeList(const char* filename);

		double NormalizeScore(Hypothesis::Words* pWords, Confidence score);

		void SetG2P(G2P* pG2P) { mpG2P = pG2P; }
		void SetCharset(Charset* pCharset) { mpCharset = pCharset; }
};

} // namespace lse
#endif

