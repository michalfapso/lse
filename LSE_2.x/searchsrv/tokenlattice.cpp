#include "tokenlattice.h"

using namespace std;
using namespace lse;


//================================================================================
//	operator== (WLR::ID, WLR::ID)
//	
bool lse::operator==(const WLR::ID& l, const WLR::ID& r)
{
//	DBG_SEARCH("comparing token paths: "<<l<<" and "<<r);
	if (l.mSize != r.mSize) {
//		DBG_SEARCH("  l.mSize != r.mSize");
		return false;
	}

	for (int i=0; i<l.mSize; i++) {
		if ((bool)l.mData[i] != (bool)r.mData[i]) {
			return false;
		}
	}
	return true;
}


//================================================================================
//	TokenLattice::CanStartTokenOnKwd()
//	
bool TokenLattice::CanStartTokenOnKwd(ID_t W_id, int *kwdIndex)
{
	*kwdIndex = 0;
	for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); ++iKwd)
	{
		if (iKwd->W_id == W_id) {
			return true;
		}
		(*kwdIndex)++;
	}
	*kwdIndex = -1;
	return false;
}


void WLR::ID::AddKwd(int queryKwdIndex)
{
	if (queryKwdIndex == -1) {
		return;
	}

	if (queryKwdIndex < 0 || queryKwdIndex >= mSize) {
		cout << "WLR::ID::AddKwd(" << queryKwdIndex << ") error    (mData[0.."<<mSize<<"])" << endl;
		exit(1);
	}
		
//	for (int i=0; i<mSize; i++) DBG_SEARCH("mData["<<i<<"]:"<<mData[i]);
	if (mData[queryKwdIndex] == 0)
		kwdsCount++;

	mData[queryKwdIndex]++;
}

//================================================================================
//	WLR::ID::AddKwd()
//
void WLR::AddKwd(int queryKwdIndex)
{
	if (queryKwdIndex == -1) {
		return;
	}

	mID.AddKwd(queryKwdIndex);
	isKwd = true;
}



//================================================================================
//	WLR::addNode()
//	
/*
void WLR::addNode(ID_t nodeID, int queryKwdListSize, int queryKwdIndex)
{
	if (queryKwdIndex != -1) {
		mID.AddKwd(queryKwdIndex);
	}
	
//	nodeHistory.push_back(nodeID);
	
	WLR* wlrNew;
	wlrNew = new WLR(nodeID, queryKwdListSize);
	wlrNew->mNext = this->wlr;
	if(wlrNew->mNext != NULL) wlrNew->mNext->refs++;
	this->wlr = wlrNew;
	wlrNew->refs = 1;

	// add current word's likelihood to the path's score
}
*/

//================================================================================
//	TokenLattice::Node::startNewToken()
//
void TokenLattice::Node::startNewToken(LatBinFile::Node* binNode, QueryKwdList* queryKwdList)
{
//	DBG_FORCE("TokenLattice::Node::startNewToken("<<*binNode<<")");
	// we need to start a new token for each occurence of the current keyword in queryKwdList
	for (QueryKwdList::iterator iKwd=queryKwdList->begin(); iKwd!=queryKwdList->end(); ++iKwd)
	{
		if (iKwd->W_id != binNode->W_id)
			continue;

		WLR* wlrNew 	= new WLR(this->mID, queryKwdList->size());                           // create a new WordLinkRecord
		wlrNew->miKwd 	= iKwd;
		wlrNew->mConf 	= binNode->conf;                                                // assign the new confidence
		wlrNew->mPathLikelihood = 0;
		wlrNew->mAlpha	= binNode->alpha;
		wlrNew->AddKwd	( queryKwdList->getKwdIndex(iKwd) );   // assign proper id according to the node's word

		tokenPathList.addPath(wlrNew);                              // add new TokenPath to the list

//		DBG_FORCE("startNewToken("<<binNode->id<<"): "<<*wlrNew);
	}
}

//================================================================================
/*	WLR::confidenceWithNode() {{{ */
//
double WLR::confidenceWithNode(ID_t nodeID, float linkConf, LatBinFile *binlat) 
{
	DBG_TOKENS("confidenceWithNode("<<nodeID<<")");
	
	DBG_TOKENS("confidenceWithNode()  "<<*this<<" -> "<<nodeID);

	ID_t lastPathNodeID = this->mNodeID;
	DBG_TOKENS("  lastPathNodeID:"<<lastPathNodeID<<" newNodeID:"<<nodeID);
	double alphaLast_path = binlat->alphaLastArray.getAlphaLast(lastPathNodeID);
	double alphaLast_newNode = binlat->alphaLastArray.getAlphaLast(mNodeID);
	if (alphaLast_newNode != alphaLast_path) {
//		DBG_SEARCH("  New sublattice begins at mNodeID:"<<mNodeID);
	}
	DBG_TOKENS("  alphaLast_path:"<<alphaLast_path<<" alphaLast_newNode:"<<alphaLast_newNode);

	/* THEORY: {{{ */
	// WHAT DO WE KNOW (from a stored lattice):
	//
	// node's alpha probability: 
	//   a forward probability from the lattice's start node to this particular node
	//
	// node's beta probability: 
	//   a backward probability from the lattice's end node to this particular node
	//
	// link's confidence: 
	//   assume a link from node A (with alpha_A and beta_A) to node B (with alpha_B and beta_B)
	//   link's likelihood is l_AB
	//   last node's alpha probability in the current lattice is alpha_Last
	//
	//   then: confidence_AB = alpha_A + l_AB + beta_B - alpha_Last
	//
	//
	// WHAT DO WE NEED TO KNOW:
	//
	// link's likelihood:
	//   l_AB = confidence_AB - alpha_A - beta_B + alpha_Last
	//
	//
	// CONCATENATING LINKS:
	//
	// WLR->conf ... confidence of a path
	// A         ... last node of a path
	// B         ... node which is going to be added to the path
	//
	// (WLR->conf + conf_AB) =
	//     WLR->conf - beta_A + alpha_Last           // confidence of a path without the right side
	//   + l_AB + beta_B                             // right side of a path with the new link AB
	//   - alpha_Last                                // normalization over the whole lattice
	//
	// we can substitute l_AB with:
	//   l_AB = conf_AB - alpha_A - beta_B + alpha_Last   // likelihood of the link AB
	/* }}} */

//	double l_AB = linkConf - binlat->nodes[lastPathNodeID].alpha - binlat->nodes[nodeID].beta + alphaLast_newNode;
//	float length = (binlat->nodes[nodeID].t - binlat->nodes[lastPathNodeID].t);
//	double kwdScale = (length > 0) ? (l_AB * KWD_SCALE_MULTIPLIER / length) : 0;
	float kwdScale = 0;
	DBG_TOKENS("CanAddWiPen("<<binlat->nodes[nodeID].W_id<<"):"<<binlat->mpLexicon->CanAddWiPen(binlat->nodes[nodeID].W_id));
	DBG_TOKENS("getNullWordID:"<<binlat->mpLexicon->getNullWordID());
//	float wipen = (binlat->mpLexicon->CanAddWiPen(binlat->nodes[nodeID].W_id) ? binlat->GetWipen() : 0);
	DBG_TOKENS("kwdScale:"<<kwdScale);
//	DBG_TOKENS("wipen:"<<wipen);
	double newScore;
	if (alphaLast_path == alphaLast_newNode) 
	{

		newScore = (this->mConf - binlat->nodes[lastPathNodeID].beta + alphaLast_path)
						 + (linkConf - binlat->nodes[lastPathNodeID].alpha + alphaLast_newNode)
						 - (alphaLast_path)
//						 + wipen // it is already included in the link's confidence
						 + kwdScale;
		
		DBG_TOKENS("  conf = "<<linkConf);
		DBG_TOKENS("  a1 = "<<binlat->nodes[lastPathNodeID].alpha);
		DBG_TOKENS("  b2 = "<<binlat->nodes[nodeID].beta);
		DBG_TOKENS("  conf - a1 - b2 + alast = "<<linkConf - binlat->nodes[lastPathNodeID].alpha - binlat->nodes[nodeID].beta + alphaLast_newNode);

		DBG_TOKENS("  A = "<<setprecision(13)<<this->mConf - binlat->nodes[lastPathNodeID].beta + alphaLast_path);
		DBG_TOKENS("  B = "<<setprecision(13)<<linkConf - binlat->nodes[lastPathNodeID].alpha + alphaLast_newNode);
		DBG_TOKENS("  C = "<<setprecision(13)<<alphaLast_path);
		DBG_TOKENS("  newScore = A + B - C = "<<setprecision(13)<<newScore);
		DBG_TOKENS("  newScore = ("<<this->mConf<<" - "<<binlat->nodes[lastPathNodeID].beta<<" + "<<alphaLast_path
				<< ") + ("<<linkConf<<" - "<<binlat->nodes[lastPathNodeID].alpha<<" + "<<alphaLast_newNode 
				<< ") - ("<<alphaLast_path<< ") = "<<newScore);
				
	} else {
		// since the l_AB and beta_B are both = 0, it is the same as above!!!
		newScore = (this->mConf - binlat->nodes[lastPathNodeID].beta + alphaLast_path)
						 - (alphaLast_path)
//						 + wipen
						 + kwdScale;
						 
		
		DBG_TOKENS("  newScore = ("<<this->mConf<<" - "<<binlat->nodes[lastPathNodeID].beta<<" + "<<alphaLast_path
				<< ") - ("<<alphaLast_path<< ") = "<<newScore);
				
	}
	
	DBG_TOKENS("confidenceWithNode()...done");

	return newScore;
}
/* }}} */


//================================================================================
//	TokenPathList::getBestTokenPath()
//
WLR* TokenPathList::getBestTokenPath()
{
	TokenPathList::iterator res = getBestTokenPathIterator();
	return (res==this->end() ? NULL : *res);
}

//================================================================================
//	TokenPathList::getBestTokenPathIterator()
//
TokenPathList::iterator TokenPathList::getBestTokenPathIterator()
{
//	DBG_SEARCH("TokenPathList::getBestTokenPath()");
	// if there is no token in the pathList, return NULL
	// otherwise return the best path
	if (this->size() == 0)
	{
		return this->end();
	}
	else
	{
		TokenPathList::iterator iBestPath = this->begin();
		for (TokenPathList::iterator iPath = this->begin(); iPath != this->end(); iPath++)
		{
//			DBG_SEARCH("cmp: "<<**iBestPath<<" with "<<**iPath);
			if (**iBestPath < **iPath) 
			{
//				DBG_SEARCH("better path found");
				iBestPath = iPath;
			}
//			DBG_SEARCH("iBestPath: "<<**iBestPath);
		}
//		DBG_SEARCH("TokenPathList::getBestTokenPath()...done");
		return iBestPath;
	}
}

//================================================================================
//	FreeTokens()
//
void TokenLattice::FreeTokens()
{
//	DBG_SEARCH("FreeTokens()");
//	DBG_SEARCH("  WLRcounter = "<<WLRcounter);
		
	for (int i = 0; i < nodesCount; i++)
	{
//		DBG_SEARCH("Tokens on node["<<nodes[i].mID<<"]: "<<nodes[i].tokenPathList.size());
//		nodes[i].tokenPathList.print();
	}

	for (int i = 0; i < nodesCount; i++)
	{
//		DBG_SEARCH("================================================================================");
//		DBG_SEARCH("Erasing tokens on node:"<<nodes[i].mID<<" tokenPathList.size():"<<nodes[i].tokenPathList.size());
//		for (unsigned int j = 0; j < nodes[i].tokenPathList.size(); j++)
		while (nodes[i].tokenPathList.size() > 0)
		{
//			DBG_SEARCH("    "<< **(nodes[i].tokenPathList.begin()));
			nodes[i].tokenPathList.freePath(nodes[i].tokenPathList.begin());
//			(*nodes[i].tokenPathList.begin())->free();
//			nodes[i].tokenPathList.erase(nodes[i].tokenPathList.begin());
		}
	}
//	DBG_SEARCH("  WLRcounter = "<<WLRcounter);
//	DBG_SEARCH("FreeTokens()...done");
}
/*
void TokenLattice::print()
{
	for (int i = 0; i < nodesCount; i++)
	{
		for (unsigned int j = 0; j < nodes[i].tokenPathList.size(); j++)
		{
			DBG_SEARCH("    "<< **(nodes[i].tokenPathList.begin()));
			(*nodes[i].tokenPathList.begin())->free();
	
}

*/

bool TokenLattice::JumpOverNode(ID_t wordId)
{
	for (vector<ID_t>::iterator i=verificationJumpOverList.begin(); i!=verificationJumpOverList.end(); i++)
	{
		if (*i == wordId)
		{
			return true;
		}
	}
	return false;
}

//================================================================================
//	FitsQuery()
//
bool TokenLattice::FitsQuery(WLR* fromPath, ID_t toNodeID, QueryKwdList::iterator* piCurKwd)
{
	*piCurKwd = queryKwdList->end();

	bool print_dbg = false;//fromPath->mNodeID == 751;

//	DBG_FORCE("FitsQuery("<<fromPath->mNodeID<<","<<toNodeID<<")");
/*	
	if (fromPath->mNodeID > 1990 && fromPath->mNodeID < 2000)
		DBG_FORCE(fromPath->mNodeID);
*/	
	if (print_dbg) DBG_FORCE("TokenLattice::FitsQuery("<<toNodeID<<", "<<*fromPath<<")");
	
	if (fromPath->mID.IsEmpty()) {
//		DBG_FORCE("  fromPath->mID.IsEmpty()");
		return true;
	}
	

	
	WLR* pWlr = fromPath;
	LatBinFile::Node* pToNode = &binlat->nodes[toNodeID];
	int nodesCountOnThePath = 0; // we have also toNodeID on the path

	// always propagate tokens to !NULL nodes
	if (JumpOverNode(pToNode->W_id))
	{
		return true;
	}
	
	// for each node on the path (processing from the end to the start node)
	while (pWlr != NULL)
	{
		if (print_dbg) DBG_FORCE("  pWlr != NULL: ");
		LatBinFile::Node* pWlrNode = &binlat->nodes[pWlr->mNodeID];
		// if we are on a node on the path which represents a keyword from the query
		if (pWlr->isKwd) 
		{
			if (print_dbg) DBG_FORCE("  pWlrNode: "<<*pWlrNode);
			if (print_dbg) DBG_FORCE("  pWlr->isKwd");
			if (print_dbg) DBG_FORCE("  pToNode: "<<*pToNode);
			
			QueryKwdList::iterator iKwd=pWlr->miKwd;
			
			if (print_dbg) DBG_FORCE("    "<<*iKwd);
			
			QueryKwdList::iterator iKwdNext=iKwd; iKwdNext++;
				
			// if the case of phrase, we have to check if there is no other word 
			// between toNodeID and the nearest keyword on the token's path
			if (iKwd->isNextInPhrase) 
			{
				if (iKwdNext == queryKwdList->end())
					return false;
				if (print_dbg) DBG_FORCE("  iKwdNext != queryKwdList->end()");
				if (print_dbg) DBG_FORCE("  iKwdNext: "<<*iKwdNext);
				
				if (print_dbg) DBG_FORCE("  inPhrase");
				if (nodesCountOnThePath != 0)
					return false;

				// if there is no keyword in the queryKwdList after the iKwd
				// if there is another keyword in query after iWlr's keyword,
				// we need to continue with processing other keywords in the query
				if (iKwdNext->W_id != pToNode->W_id)
					return false;
				
				if (print_dbg) DBG_FORCE("  iKwdNext->W_id == pToNode->W_id");
			}
			// if the time distance between pWlrNode and pToNode does not fit the query
			else 
			{
				// if pToNode is a query keyword, then check the neighbourhood (time) distance from iKwd to pToNode
				if (queryKwdList->contains(pToNode->W_id) && (pToNode->tStart - pWlrNode->t) < iKwd->nb_next)	
					return false;
				// check the forward nodes count - if it fits the query
				if (nodesCountOnThePath > queryKwdList->fwd_nodes_count)
					return false;
			}
			

			
			
			if (print_dbg) DBG_FORCE("    neighborhood distance OK");
			// if all the checks passed, set the pToNode keyword's iterator and return true
			*piCurKwd = iKwdNext;
			return true;
		}

		// if we are not on a !NULL node
		if (!JumpOverNode(pWlrNode->W_id))
		{
			nodesCountOnThePath++;
		}

		pWlr = pWlr->mNext;
	}

	if (print_dbg) DBG_FORCE("  return false");
	return false;
}

//================================================================================
//	TokenLattice::SendToken()
//	
bool TokenLattice::SendToken(ID_t fromNodeID, ID_t toNodeID, float linkConf)
{
//	DBG_SEARCH("SendToken("<<fromNodeID<<", "<<toNodeID<<", "<<linkConf<<")");
	
	int toNodeKwdIndex = queryKwdList->getKwdIndex(binlat->nodes[toNodeID].W_id);

	// for each fromNode's tokenPath
	for (TokenPathList::iterator iFromWlr=(*this)[fromNodeID].tokenPathList.begin(); iFromWlr!=(*this)[fromNodeID].tokenPathList.end(); ++iFromWlr)
	{
		WLR* pFromWlr = *iFromWlr;
//		DBG_SEARCH("pFromWlr: "<<*pFromWlr);
		double newWlrConfidence = pFromWlr->confidenceWithNode(toNodeID, linkConf, binlat);
		bool canSendToken = true;
		TokenPathList::iterator iToWlr=(*this)[toNodeID].tokenPathList.begin();
		QueryKwdList::iterator iCurKwd;
		// is the new path valid according to the query?
		if (!FitsQuery(pFromWlr, toNodeID, &iCurKwd))
		{
			canSendToken = false;
		}
		else
		{
			// for each toNode's tokenPath
			// compare token in fromNode with all tokens in toNode
			// and check if it can be send further from fromNode to toNode
			while (iToWlr != (*this)[toNodeID].tokenPathList.end())
			{
				WLR* pToWlr = *iToWlr;
//				DBG_SEARCH("  iToWlr: "<<*pToWlr);

				// can we merge them?
				if ((pFromWlr->mID + toNodeKwdIndex) == pToWlr->mID)
				{
					bool print_dbg = false;//pFromWlr->mNodeID == 92774;
					
					if (print_dbg) DBG_SEARCH("  found path with the same mID: "<<*pFromWlr<<" == "<<*pToWlr);
					// if the fromNode's token is better, than erase the toNode's one. If it is worse, don't send it further
					if (newWlrConfidence > pToWlr->mConf) 
					{
						if (print_dbg) DBG_SEARCH("  the new path has higher confidence: "<<newWlrConfidence<<" > "<<pToWlr->mConf);
						(*this)[toNodeID].tokenPathList.freePath(iToWlr);
					} 
					else 
					{
						if (print_dbg) DBG_SEARCH("  the new path has lower or equal confidence ("<<newWlrConfidence<<" <= "<<pToWlr->mConf<<") => don't send the token further");
						canSendToken = false;
					}
					break; // no other path with the same mID can be found
				}
				++iToWlr;
			}
		}
		
//		DBG_SEARCH("  canSendToken:"<<canSendToken);
		if (canSendToken) 
		{
//			DBG_SEARCH("canSendToken: "<<*pFromWlr<<" -> "<<toNodeID);
			WLR* wlrNew 			= new WLR(toNodeID, queryKwdList->size());                           // create a new WordLinkRecord
			wlrNew->mID 			= pFromWlr->mID;
			wlrNew->miKwd 			= iCurKwd;
			wlrNew->fwdContextLen 	= pFromWlr->fwdContextLen;
			wlrNew->mConf 			= newWlrConfidence;                                                // assign the new confidence
			wlrNew->mPathLikelihood = pFromWlr->mPathLikelihood + linkConf;
			wlrNew->mAlpha			= pFromWlr->mAlpha;
			wlrNew->setNext(pFromWlr);                                                  // create a link to the previous node on the path
			// if we are on a keyword from query
			int kwdIndex 			= queryKwdList->getKwdIndex(iCurKwd);
			if (kwdIndex != -1)
			{
				// if the token's history fits the query
//				if (FitsQuery(pFromWlr, toNodeID)) 
				{
					wlrNew->AddKwd( kwdIndex );   // assign proper id according to the node's word ( = query kwd )
				}
			}
			bool add_token_to_node = true;
			if (wlrNew->mID.GetKwdsCount() == (int)queryKwdList->size()) 
			{
//				DBG_FORCE("wlrNew->fwdContextLen: "<<wlrNew->fwdContextLen);
				if (wlrNew->fwdContextLen == queryKwdList->fwd_nodes_count)
				{
					DBG("resultPathList.addPath(): "<<*wlrNew);
					this->resultPathList.addPath(wlrNew);
					add_token_to_node = false;
				}
				wlrNew->fwdContextLen++;
//				DBG_FORCE("wlrNew->fwdContextLen 2: "<<wlrNew->fwdContextLen);
			}
			if (add_token_to_node) 
			{
				(*this)[toNodeID].tokenPathList.addPath(wlrNew);                              // add new TokenPath to the list
//				DBG_FORCE("wlrNew: "<<*wlrNew);
			}
		}
	}
//	DBG_SEARCH("SendToken()...done");
	return true;
	
}


//================================================================================
//	TokenLattice::ForwardPass()
//	
void TokenLattice::ForwardPass(InvIdxRecordsCluster* pCluster, QueryKwdList* pQueryKwdList)
{
	DBG_FORCE(endl<<endl<<endl);
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("ForwardPass("<<*pCluster<<")");
	LatBinFile::LatNodeLinksArray::Cursor cur(&(binlat->nodeLinks));
	if (binlat->nodes.lastID - binlat->nodes.firstID == 0) {
		CERR("ERROR: binlat->nodes.size() = 0 !!! ...the nodes file was not loaded properly");
		exit(1);
	}
	DBG_FORCE("ForwardPass... node_id = "<<binlat->nodes.firstID<<" ... "<<binlat->nodes.lastID);
	// for each node in the loaded part of lattice
//	for (ID_t node_id = binlat->nodes.firstID; node_id <= binlat->nodes.lastID; node_id++)
	for (ID_t node_id = binlat->nodes.firstID; node_id < binlat->nodes.lastID; node_id++)
	{
//		LatTime tStart = binlat->nodes[node_id].tStart;
		LatTime tEnd   = binlat->nodes[node_id].t;

		if (tEnd < pCluster->mStartTime - GROUP_TIME_OFFSET)
		{
			DBG_TOKENS("skipping node #"<<node_id<<" t["<<binlat->nodes[node_id].tStart<<".."<<binlat->nodes[node_id].t<<"]");
			continue;
		}
		if (pQueryKwdList->fwd_nodes_count == 0 && tEnd > pCluster->mEndTime + GROUP_TIME_OFFSET)
		{
			DBG_TOKENS("tEnd > pCluster->mEndTime ... "<<tEnd <<">"<< pCluster->mEndTime);
			DBG_TOKENS("skipping node #"<<node_id<<" t["<<binlat->nodes[node_id].tStart<<".."<<binlat->nodes[node_id].t<<"]");
			break;
		}

		DBG_TOKENS("--------------------------------------------------");
		DBG_TOKENS("nodeID:"<<node_id<<" W_id:"<<binlat->nodes[node_id].W_id);
		DBG_TOKENS((*this)[node_id]);

		// if we are on a kwd from query, then start a new token from here
		DBG_TOKENS("pQueryKwdList->contains(W_id): "<<pQueryKwdList->contains(binlat->nodes[node_id].W_id));
		if(pQueryKwdList->contains(binlat->nodes[node_id].W_id)) {
			bool fits_in_cluster = false;
//			DBG_FORCE("pQueryKwdList->contains("<<binlat->nodes[node_id].W_id<<")");
			// for all keywords from the query
			for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); iKwd != pQueryKwdList->end(); ++iKwd)
			{
				if (iKwd->W_id == binlat->nodes[node_id].W_id) 
				{
					// for each cluster of the keyword in the current group
					if (pCluster->mStartTime <= tEnd && pCluster->mEndTime >= tEnd)
					{
//							DBG_FORCE("KWD:"<<iKwd->W_id<<" NODEID:"<<node_id<<" pOccurrence->tStart <= tEnd && pOccurrence->tEnd >= tEnd ... "<<pOccurrence->tStart<<" <= "<<tEnd<<" && "<<pOccurrence->tEnd<<" >= "<<tEnd);
						fits_in_cluster = true;
						break;
					}
				}
				if (fits_in_cluster) break;
			}
			DBG_TOKENS("fits_in_cluster:"<<fits_in_cluster);
			if (fits_in_cluster)
			{
				(*this)[node_id].startNewToken(&binlat->nodes[node_id], pQueryKwdList);
//				DBG_FORCE("newTokenStarted "<<(*this)[node_id]);
			}
		}

		DBG_TOKENS("binlat->node->mConf: "<<binlat->nodes[node_id].conf);
		DBG_TOKENS("gotoNodeID("<<node_id<<")...");
		cur.gotoNodeID(node_id, FWD);
		DBG_TOKENS("gotoNodeID("<<node_id<<")...done");
		bool tokenSent = false;
		LatBinFile::NodeLinks::Link bestLink;
		bestLink.nodeID = -1;
		// try to send tokens from the current node (nodeID) to all it's successors (forward nodes)
		while (!cur.eol()) 
		{
			DBG_TOKENS("");
			LatBinFile::NodeLinks::Link l = cur.getLink();
			DBG_TOKENS("");
			if (bestLink.nodeID == -1 || bestLink.conf < l.conf) {
				bestLink.conf = l.conf;
				bestLink.nodeID = l.nodeID;					
			}
			DBG_TOKENS("");
				
			cur.next();
			DBG_TOKENS("nodes.containsID("<<l.nodeID<<"): "<<binlat->nodes.containsID(l.nodeID));
			if (!binlat->nodes.containsID(l.nodeID))
				continue; // if the link is going to jump out of the read part of lattice
			DBG_TOKENS("");
			DBG_TOKENS((*this)[l.nodeID]);
			tokenSent = SendToken(node_id, l.nodeID, l.conf); // send the token to the forward node
			DBG_TOKENS((*this)[l.nodeID]);
		}

		
		// if no token was sent from the current node 
		// or if the best path from this node is going out of the loaded part of lattice
		// then add the best token to the result list
		if (!tokenSent || 
			!binlat->nodes.containsID(bestLink.nodeID)) 
		{
			// put the node's best TokenPath into the TokenLattice's result pathList
			WLR* bestTokenPath = (*this)[node_id].tokenPathList.getBestTokenPath();
			if (bestTokenPath != NULL)
			{
				DBG_TOKENS("node_id:"<<node_id<<" tokenLattice.resultPathList.push_back("<<*bestTokenPath<<")");
				
				// if we want only an exact match, we have to check the keywords count on the best path
				// and add it to the results only if kwdsCount is the same as in the query
				// !!! WILL NOT WORK IF THERE IS AN OPERATOR "OR" OR "NOT" IN THE QUERY !!!
				if (pQueryKwdList->mExactMatch) {
					if ( bestTokenPath->mID.GetKwdsCount() == (int)pQueryKwdList->size() )
					{
						this->resultPathList.addPath(bestTokenPath);
					}
				} else {
					this->resultPathList.addPath(bestTokenPath);
				}
			}
//			(*this)[node_id].tokenPathList.freeAllTokens();
		}
	} // for each node
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE(endl<<endl<<endl);
}




