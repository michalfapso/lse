#include "charset.h"
//#include "/homes/kazi/szoke/PROJECTS/LSE/branches/LSE_2.x/liblse/lattypes.h"
#include "lattypes.h"

using namespace std;
using namespace lse;

// Charset_ISO88591 {{{
unsigned char Charset_ISO88591::upcase(unsigned char c)
{
	if (c >= 0x61 && c <= 0x7a) // a..z
	{
		return c - 0x20;
	}
	else
	{
		return c;
	}
}
unsigned char Charset_ISO88591::locase(unsigned char c)
{
	if (c >= 0x41 && c <= 0x5a) // a..z
	{
		return c + 0x20;
	}
	else
	{
		return c;
	}
}
/*
std::string Charset_ISO88591::upcase(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(upcase(c));
	}
	return res;
}

std::string Charset_ISO88591::locase(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(locase(c));
	}
	return res;
}
*/
// }}}

// Charset_ISO88592 {{{
unsigned char Charset_ISO88592::upcase(unsigned char c)
{
	if (c >= 0x61 && c <= 0x7a) // a..z
	{
		return c - 0x20;
	}
	switch(c)
	{
		case 0xB1: // LATIN SMALL LETTER A WITH OGONEK
		case 0xB3: // LATIN SMALL LETTER L WITH STROKE
		case 0xB5: // LATIN SMALL LETTER L WITH CARON
		case 0xB6: // LATIN SMALL LETTER S WITH ACUTE
		case 0xB9: // LATIN SMALL LETTER S WITH CARON
		case 0xBA: // LATIN SMALL LETTER S WITH CEDILLA
		case 0xBB: // LATIN SMALL LETTER T WITH CARON
		case 0xBC: // LATIN SMALL LETTER Z WITH ACUTE
		case 0xBE: // LATIN SMALL LETTER Z WITH CARON
		case 0xBF: // LATIN SMALL LETTER Z WITH DOT ABOVE
			return c - 0x10;

		case 0xE0: // LATIN SMALL LETTER R WITH ACUTE
		case 0xE1: // LATIN SMALL LETTER A WITH ACUTE
		case 0xE2: // LATIN SMALL LETTER A WITH CIRCUMFLEX
		case 0xE3: // LATIN SMALL LETTER A WITH BREVE
		case 0xE4: // LATIN SMALL LETTER A WITH DIAERESIS
		case 0xE5: // LATIN SMALL LETTER L WITH ACUTE
		case 0xE6: // LATIN SMALL LETTER C WITH ACUTE
		case 0xE7: // LATIN SMALL LETTER C WITH CEDILLA
		case 0xE8: // LATIN SMALL LETTER C WITH CARON
		case 0xE9: // LATIN SMALL LETTER E WITH ACUTE
		case 0xEA: // LATIN SMALL LETTER E WITH OGONEK
		case 0xEB: // LATIN SMALL LETTER E WITH DIAERESIS
		case 0xEC: // LATIN SMALL LETTER E WITH CARON
		case 0xED: // LATIN SMALL LETTER I WITH ACUTE
		case 0xEE: // LATIN SMALL LETTER I WITH CIRCUMFLEX
		case 0xEF: // LATIN SMALL LETTER D WITH CARON
		case 0xF0: // LATIN SMALL LETTER D WITH STROKE
		case 0xF1: // LATIN SMALL LETTER N WITH ACUTE
		case 0xF2: // LATIN SMALL LETTER N WITH CARON
		case 0xF3: // LATIN SMALL LETTER O WITH ACUTE
		case 0xF4: // LATIN SMALL LETTER O WITH CIRCUMFLEX
		case 0xF5: // LATIN SMALL LETTER O WITH DOUBLE ACUTE
		case 0xF6: // LATIN SMALL LETTER O WITH DIAERESIS
		case 0xF8: // LATIN SMALL LETTER R WITH CARON
		case 0xF9: // LATIN SMALL LETTER U WITH RING ABOVE
		case 0xFA: // LATIN SMALL LETTER U WITH ACUTE
		case 0xFB: // LATIN SMALL LETTER U WITH DOUBLE ACUTE
		case 0xFC: // LATIN SMALL LETTER U WITH DIAERESIS
		case 0xFD: // LATIN SMALL LETTER Y WITH ACUTE
		case 0xFE: // LATIN SMALL LETTER T WITH CEDILLA
			return c - 0x20;

		default: return c;
	}
}

unsigned char Charset_ISO88592::locase(unsigned char c)
{
	if (c >= 0x41 && c <= 0x5a) // a..z
	{
		return c + 0x20;
	}
	switch (c)
	{
		case 0xA1: // LATIN CAPITAL LETTER A WITH OGONEK
		case 0xA3: // LATIN CAPITAL LETTER L WITH STROKE
		case 0xA5: // LATIN CAPITAL LETTER L WITH CARON
		case 0xA6: // LATIN CAPITAL LETTER S WITH ACUTE
		case 0xA9: // LATIN CAPITAL LETTER S WITH CARON
		case 0xAA: // LATIN CAPITAL LETTER S WITH CEDILLA
		case 0xAB: // LATIN CAPITAL LETTER T WITH CARON
		case 0xAC: // LATIN CAPITAL LETTER Z WITH ACUTE
		case 0xAE: // LATIN CAPITAL LETTER Z WITH CARON
		case 0xAF: // LATIN CAPITAL LETTER Z WITH DOT ABOVE
			return c + 0x10;

		case 0xC0: // LATIN CAPITAL LETTER R WITH ACUTE
		case 0xC1: // LATIN CAPITAL LETTER A WITH ACUTE
		case 0xC2: // LATIN CAPITAL LETTER A WITH CIRCUMFLEX
		case 0xC3: // LATIN CAPITAL LETTER A WITH BREVE
		case 0xC4: // LATIN CAPITAL LETTER A WITH DIAERESIS
		case 0xC5: // LATIN CAPITAL LETTER L WITH ACUTE
		case 0xC6: // LATIN CAPITAL LETTER C WITH ACUTE
		case 0xC7: // LATIN CAPITAL LETTER C WITH CEDILLA
		case 0xC8: // LATIN CAPITAL LETTER C WITH CARON
		case 0xC9: // LATIN CAPITAL LETTER E WITH ACUTE
		case 0xCA: // LATIN CAPITAL LETTER E WITH OGONEK
		case 0xCB: // LATIN CAPITAL LETTER E WITH DIAERESIS
		case 0xCC: // LATIN CAPITAL LETTER E WITH CARON
		case 0xCD: // LATIN CAPITAL LETTER I WITH ACUTE
		case 0xCE: // LATIN CAPITAL LETTER I WITH CIRCUMFLEX
		case 0xCF: // LATIN CAPITAL LETTER D WITH CARON
		case 0xD0: // LATIN CAPITAL LETTER D WITH STROKE
		case 0xD1: // LATIN CAPITAL LETTER N WITH ACUTE
		case 0xD2: // LATIN CAPITAL LETTER N WITH CARON
		case 0xD3: // LATIN CAPITAL LETTER O WITH ACUTE
		case 0xD4: // LATIN CAPITAL LETTER O WITH CIRCUMFLEX
		case 0xD5: // LATIN CAPITAL LETTER O WITH DOUBLE ACUTE
		case 0xD6: // LATIN CAPITAL LETTER O WITH DIAERESIS
		case 0xD8: // LATIN CAPITAL LETTER R WITH CARON
		case 0xD9: // LATIN CAPITAL LETTER U WITH RING ABOVE
		case 0xDA: // LATIN CAPITAL LETTER U WITH ACUTE
		case 0xDB: // LATIN CAPITAL LETTER U WITH DOUBLE ACUTE
		case 0xDC: // LATIN CAPITAL LETTER U WITH DIAERESIS
		case 0xDD: // LATIN CAPITAL LETTER Y WITH ACUTE
		case 0xDE: // LATIN CAPITAL LETTER T WITH CEDILLA
			return c + 0x20;

		default: return c;
	}
}

unsigned char Charset_ISO88592::from_CP1250(unsigned char c)
{

	switch(c)
	{
		case 0xa5: return 0xa1; break; // s hook
		case 0xbc: return 0xa5; break; // t hook
		case 0x8c: return 0xa6; break; // z hook
		case 0x8a: return 0xa9; break; // l hook
		case 0x8d: return 0xab; break; // l hook
		case 0x8f: return 0xac; break; // l hook
		case 0x8e: return 0xae; break; // l hook
		case 0xb9: return 0xb1; break; // l hook
		case 0xbe: return 0xb5; break; // l hook
		case 0x9c: return 0xb6; break; // l hook
		case 0xa1: return 0xb7; break; // l hook
		case 0x9a: return 0xb9; break; // l hook
		case 0x9d: return 0xbb; break; // l hook
		case 0x9f: return 0xbc; break; // l hook
		case 0x9e: return 0xbe; break; // l hook
		default: return c; break;
	}	
}

unsigned char Charset_ISO88592::from_ISO88592(unsigned char c)
{
	return c;
}
/*
std::string Charset_ISO88592::upcase(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(upcase(c));
	}
	return res;
}

std::string Charset_ISO88592::locase(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(locase(c));
	}
	return res;
}
*/
std::string Charset_ISO88592::from_CP1250(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(from_CP1250(c));
	}
	return res;
}

std::string Charset_ISO88592::from_ISO88592(const std::string &str)
{
	return str;
}
// }}}

// Charset_CP1250 {{{
unsigned char Charset_CP1250::upcase(unsigned char c)
{
	if (c >= 0x61 && c <= 0x7a) // a..z
	{
		return c - 0x20;
	}
	else if (c >= 0xe0 && c <= 0xfe)
	{
		return c - 0x20;
	}
	else switch(c)
	{
		case 0x9a: return 0x8a; break; // s hook
		case 0x9d: return 0x8d; break; // t hook
		case 0x9e: return 0x8e; break; // z hook
		case 0xbe: return 0xbc; break; // l hook
		default: return c; break;
	}
}

unsigned char Charset_CP1250::locase(unsigned char c)
{
	if (c >= 0x41 && c <= 0x5a) // a..z
	{
		return c + 0x20;
	}
	else if (c >= 0xc0 && c <= 0xde)
	{
		return c + 0x20;
	}
	else switch(c)
	{
		case 0x8a: return 0x9a; break; // s hook
		case 0x8d: return 0x9d; break; // t hook
		case 0x8e: return 0x9e; break; // z hook
		case 0xbc: return 0xbe; break; // l hook
		default: return c; break;
	}
}

unsigned char Charset_CP1250::from_ISO88592(unsigned char c)
{

	switch(c)
	{
		case 0xa1: return 0xa5; break; // s hook
		case 0xa5: return 0xbc; break; // t hook
		case 0xa6: return 0x8c; break; // z hook
		case 0xa9: return 0x8a; break; // l hook
		case 0xab: return 0x8d; break; // l hook
		case 0xac: return 0x8f; break; // l hook
		case 0xae: return 0x8e; break; // l hook
		case 0xb1: return 0xb9; break; // l hook
		case 0xb5: return 0xbe; break; // l hook
		case 0xb6: return 0x9c; break; // l hook
		case 0xb7: return 0xa1; break; // l hook
		case 0xb9: return 0x9a; break; // l hook
		case 0xbb: return 0x9d; break; // l hook
		case 0xbc: return 0x9f; break; // l hook
		case 0xbe: return 0x9e; break; // l hook
		default: return c; break;
	}	
}

unsigned char Charset_CP1250::from_CP1250(unsigned char c)
{
	return c;
}
/*
std::string Charset_CP1250::upcase(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(upcase(c));
	}
	return res;
}

std::string Charset_CP1250::locase(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(locase(c));
	}
	return res;
}
*/
std::string Charset_CP1250::from_ISO88592(const std::string &str)
{
	string res;
	res.reserve(str.length());
	for (unsigned int i=0; i<str.length(); i++)
	{
		unsigned char c = str[i];
		res.push_back(from_ISO88592(c));
	}
	return res;
}

std::string Charset_CP1250::from_CP1250(const std::string &str)
{
	return str;
}

// }}}


unsigned char Charset_CP1251::upcase(unsigned char c)
{
	if (c >= 0x61 && c <= 0x7a){
		return c - 0x20;// a..z LATIN
	}else if (c >= 0xe0 /*&& c <= 0xff*/){
		return c - 0x20; // a..z
//0xE0    0x0430  #CYRILLIC SMALL LETTER A
//0xE1    0x0431  #CYRILLIC SMALL LETTER BE
//0xE2    0x0432  #CYRILLIC SMALL LETTER VE
//0xE3    0x0433  #CYRILLIC SMALL LETTER GHE
//0xE4    0x0434  #CYRILLIC SMALL LETTER DE
//0xE5    0x0435  #CYRILLIC SMALL LETTER IE
//0xE6    0x0436  #CYRILLIC SMALL LETTER ZHE
//0xE7    0x0437  #CYRILLIC SMALL LETTER ZE
//0xE8    0x0438  #CYRILLIC SMALL LETTER I
//0xE9    0x0439  #CYRILLIC SMALL LETTER SHORT I
//0xEA    0x043A  #CYRILLIC SMALL LETTER KA
//0xEB    0x043B  #CYRILLIC SMALL LETTER EL
//0xEC    0x043C  #CYRILLIC SMALL LETTER EM
//0xED    0x043D  #CYRILLIC SMALL LETTER EN
//0xEE    0x043E  #CYRILLIC SMALL LETTER O
//0xEF    0x043F  #CYRILLIC SMALL LETTER PE
//0xF0    0x0440  #CYRILLIC SMALL LETTER ER
//0xF1    0x0441  #CYRILLIC SMALL LETTER ES
//0xF2    0x0442  #CYRILLIC SMALL LETTER TE
//0xF3    0x0443  #CYRILLIC SMALL LETTER U
//0xF4    0x0444  #CYRILLIC SMALL LETTER EF
//0xF5    0x0445  #CYRILLIC SMALL LETTER HA
//0xF6    0x0446  #CYRILLIC SMALL LETTER TSE
//0xF7    0x0447  #CYRILLIC SMALL LETTER CHE
//0xF8    0x0448  #CYRILLIC SMALL LETTER SHA
//0xF9    0x0449  #CYRILLIC SMALL LETTER SHCHA
//0xFA    0x044A  #CYRILLIC SMALL LETTER HARD SIGN
//0xFB    0x044B  #CYRILLIC SMALL LETTER YERU
//0xFC    0x044C  #CYRILLIC SMALL LETTER SOFT SIGN
//0xFD    0x044D  #CYRILLIC SMALL LETTER E
//0xFE    0x044E  #CYRILLIC SMALL LETTER YU
//0xFF    0x044F  #CYRILLIC SMALL LETTER YA
	}else switch(c){
		case 0x83: return 0x81; break; //#CYRILLIC SMALL LETTER GJE
		case 0x90: return 0x80; break; //#CYRILLIC SMALL LETTER DJE
		case 0x9a: return 0x8a; break; //#CYRILLIC SMALL LETTER LJE
		case 0x9c: return 0x8c; break; //#CYRILLIC SMALL LETTER NJE
		case 0x9d: return 0x8d; break; //#CYRILLIC SMALL LETTER KJE
		case 0x9e: return 0x8e; break; //#CYRILLIC SMALL LETTER TSHE
		case 0x9f: return 0x8f; break; //#CYRILLIC SMALL LETTER DZHE
		case 0xa2: return 0xa1; break; //#CYRILLIC SMALL LETTER SHORT U
		case 0xb3: return 0xb2; break; //#CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I
		case 0xb4: return 0xa5; break; //#CYRILLIC SMALL LETTER GHE WITH UPTURN
		case 0xb8: return 0xa8; break; //#CYRILLIC SMALL LETTER IO
		case 0xba: return 0xaa; break; //#CYRILLIC SMALL LETTER UKRAINIAN IE
		case 0xbc: return 0xa3; break; //#CYRILLIC SMALL LETTER JE
		case 0xbe: return 0xbd; break; //#CYRILLIC SMALL LETTER DZE
		case 0xbf: return 0xaf; break; //#CYRILLIC SMALL LETTER YI

		default: return c; break;
	}
}


unsigned char Charset_CP1251::locase(unsigned char c)
{
	if (c >= 0x41 && c <= 0x5a){
		return c + 0x20;// a..z LATIN
	}else if (c >= 0xc0 && c <= 0xdf){
		return c + 0x20; // a..z
//0xC0    0x0410  #CYRILLIC CAPITAL LETTER A
//0xC1    0x0411  #CYRILLIC CAPITAL LETTER BE
//0xC2    0x0412  #CYRILLIC CAPITAL LETTER VE
//0xC3    0x0413  #CYRILLIC CAPITAL LETTER GHE
//0xC4    0x0414  #CYRILLIC CAPITAL LETTER DE
//0xC5    0x0415  #CYRILLIC CAPITAL LETTER IE
//0xC6    0x0416  #CYRILLIC CAPITAL LETTER ZHE
//0xC7    0x0417  #CYRILLIC CAPITAL LETTER ZE
//0xC8    0x0418  #CYRILLIC CAPITAL LETTER I
//0xC9    0x0419  #CYRILLIC CAPITAL LETTER SHORT I
//0xCA    0x041A  #CYRILLIC CAPITAL LETTER KA
//0xCB    0x041B  #CYRILLIC CAPITAL LETTER EL
//0xCC    0x041C  #CYRILLIC CAPITAL LETTER EM
//0xCD    0x041D  #CYRILLIC CAPITAL LETTER EN
//0xCE    0x041E  #CYRILLIC CAPITAL LETTER O
//0xCF    0x041F  #CYRILLIC CAPITAL LETTER PE
//0xD0    0x0420  #CYRILLIC CAPITAL LETTER ER
//0xD1    0x0421  #CYRILLIC CAPITAL LETTER ES
//0xD2    0x0422  #CYRILLIC CAPITAL LETTER TE
//0xD3    0x0423  #CYRILLIC CAPITAL LETTER U
//0xD4    0x0424  #CYRILLIC CAPITAL LETTER EF
//0xD5    0x0425  #CYRILLIC CAPITAL LETTER HA
//0xD6    0x0426  #CYRILLIC CAPITAL LETTER TSE
//0xD7    0x0427  #CYRILLIC CAPITAL LETTER CHE
//0xD8    0x0428  #CYRILLIC CAPITAL LETTER SHA
//0xD9    0x0429  #CYRILLIC CAPITAL LETTER SHCHA
//0xDA    0x042A  #CYRILLIC CAPITAL LETTER HARD SIGN
//0xDB    0x042B  #CYRILLIC CAPITAL LETTER YERU
//0xDC    0x042C  #CYRILLIC CAPITAL LETTER SOFT SIGN
//0xDD    0x042D  #CYRILLIC CAPITAL LETTER E
//0xDE    0x042E  #CYRILLIC CAPITAL LETTER YU
//0xDF    0x042F  #CYRILLIC CAPITAL LETTER YA
        }else switch(c){
		case 0x81: return 0x83; break; //#CYRILLIC CAPITAL LETTER GJE
		case 0x80: return 0x90; break; //#CYRILLIC CAPITAL LETTER DJE
		case 0x8a: return 0x9a; break; //#CYRILLIC CAPITAL LETTER LJE
		case 0x8c: return 0x9c; break; //#CYRILLIC CAPITAL LETTER NJE
		case 0x8d: return 0x9d; break; //#CYRILLIC CAPITAL LETTER KJE
		case 0x8e: return 0x9e; break; //#CYRILLIC CAPITAL LETTER TSHE
		case 0x8f: return 0x9f; break; //#CYRILLIC CAPITAL LETTER DZHE
		case 0xa1: return 0xa2; break; //#CYRILLIC CAPITAL LETTER SHORT U
		case 0xb2: return 0xb3; break; //#CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I
		case 0xb5: return 0xa4; break; //#CYRILLIC CAPITAL LETTER GHE WITH UPTURN
		case 0xa8: return 0xb8; break; //#CYRILLIC CAPITAL LETTER IO
		case 0xaa: return 0xba; break; //#CYRILLIC CAPITAL LETTER UKRAINIAN IE
		case 0xa3: return 0xbc; break; //#CYRILLIC CAPITAL LETTER JE
		case 0xbd: return 0xbe; break; //#CYRILLIC CAPITAL LETTER DZE
		case 0xaf: return 0xbf; break; //#CYRILLIC CAPITAL LETTER YI

		default: return c; break;
	}
}


