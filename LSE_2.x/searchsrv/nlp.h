#ifndef NLP_H
#define NLP_H

#ifdef USE_REGEX
#include <boost/regex.hpp>
#endif

#include <list>
#include <sstream>
#include "lattypes.h"

class Nlp
{
	public:
		class RegexReplace
		{
			public:
				std::string mSearch;
				std::string mReplace;
		};

		class RegexReplaceList : public std::list<RegexReplace>
		{
			public:
				void LoadFromFile(const char* filename);
		};

		typedef std::list<std::string> WordList;
		static void ExpandByRegexList(std::string what, const RegexReplaceList& rRegexList, WordList* pResultsList);
};

#endif
