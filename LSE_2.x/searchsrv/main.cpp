/*********************************************************************************
 *
 *	Application for searching in binary lattices filesystem
 *
 *
 *
 *
 *
 */



#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <cstdio>
#include <signal.h>
#include <errno.h>
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
#include "latbinfile.h"
#include "hypothesis.h"
#include "querykwdlist.h"
#include "lexicon.h"
#include "wordclusters.h"
#include "timer.h"
#include "common.h"
#include "results.h"
#include "search.h"
#include "g2p.h"
#include "normlexicon.h"
#include "nlp.h"
#include "mgrams.h"
#include "searchconfig.h"
#include "wordmapper.h"


// server include files
#ifndef _WIN32
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <pthread.h>
#endif

using namespace std;
using namespace lse;


#define DBG_MAIN(str) if (config->p_dbg) cerr << __FILE__ << ':' << setw(5) << __LINE__ << "> " << str << endl << flush;

#define TIMER_START	dbgTimer.start();
#define TIMER_END(msg)	dbgTimer.end();	DBG_FORCE("TIMER["<<msg<<"]: "<<dbgTimer.val());

// sockets imeplementation constants
static const int BUFFER = 2048;
unsigned int max_clients_connected = 0;

// files extensions
static const string ext_dot = ".dot";
static const string ext_bin = ".bin";
static const string ext_copy = ".lat.copy";
static const string ext_bingz = ".bin.gz";
static const string ext_htk = ".lat";

Timer dbgTimer;
double cumulTime_load;
double cumulTime_load_time;
double cumulTime_load_nodes;
double cumulTime_load_nodelinks;
double cumulTime_getContext;

/* forward declarations {{{ */
void searchKeyword(SearchConfig *config, QueryKwdList *queryKwdList, Results *pResults);
bool isKwdSeparator(char c);
LatTime str2LatTime(std::string str);
bool parseQuery(const string p_query, SearchConfig *config, QueryKwdList *queryKwdList, ostringstream *output);
void processQuery(const string p_query, SearchConfig *config, ostringstream *output);
bool parse_query_convert_to_float(const char* str, const string varName, ostringstream *output, float *f);
bool send_client_message(int sd, const string &msg);
/* }}} */

#ifndef _WIN32
void * service(void *arg);
#endif

class GlobalVariables {
	public:
		GlobalVariables() : mpConfig(NULL) {}

		SearchConfig* mpConfig;
} global_variables;

pthread_mutex_t main_mutex = PTHREAD_MUTEX_INITIALIZER;
unsigned int clients_connected;

void termination_handler (int signum)
{
	CERR("server terminated by signal "<<signum<<".");
	CERR("clients connected: "<<clients_connected);
	CERR("max clients connected: "<<max_clients_connected);
	if (global_variables.mpConfig)
		delete global_variables.mpConfig;
	exit(0);
}


// structure used for passing arguments to the service (thread) function
class ServiceParams {
public:
	int sd;
	SearchConfig *config;
};


//--------------------------------------------------------------------------------

/* isQueryVar {{{ */
bool isQueryVar(string q, int kwdStartPos, const string &identifier) 
{
	return q.substr(kwdStartPos, identifier.length()) == identifier;
}
/* }}} */

/* getQueryVarValue {{{ */
const string getQueryVarValue(string q, int kwdStartPos, int kwdSpacePos, const string identifier) 
{
	return q.substr(kwdStartPos + identifier.length(), kwdSpacePos - kwdStartPos - identifier.length());
}
/* }}} */

/* main() {{{ */
int main(int argc, char * argv[]) 
{
	if (signal (SIGINT, termination_handler) == SIG_IGN)
		signal (SIGINT, SIG_IGN);
	if (signal (SIGHUP, termination_handler) == SIG_IGN)
		signal (SIGHUP, SIG_IGN);
	if (signal (SIGTERM, termination_handler) == SIG_IGN)
		signal (SIGTERM, SIG_IGN);

	SearchConfig* config = new SearchConfig;
	global_variables.mpConfig = config;

	// print arguments
	for (int i=0; i<argc; i++)
	{
		DBG_FORCE("APPLICATION ARGUMENTS: "<<argv[i]);
	}

	config->ParseCommandLineArguments(argc, argv);

	if (config->p_query != "") {
	//--------------------------------------------------------------------------------
	//                                 STANDALONE
	//--------------------------------------------------------------------------------
		DBG("STANDALONE");
		ostringstream output;
		processQuery(config->p_query, config, &output);
		cout << output.str();

	} 
#ifndef _WIN32	
	else {
	//--------------------------------------------------------------------------------
	//                                 SERVER
	//--------------------------------------------------------------------------------
		struct   protoent  *ptrp;     /* pointer to a protocol table entry */
		struct   sockaddr_in sad;     /* structure to hold server's address */
		struct   sockaddr_in cad;     /* structure to hold client's address */
		int      sd;                  /* socket descriptors */
		int      alen;                /* length of address */
		pthread_t  tid;               /* variable to hold thread ID */

		memset((char  *)&sad,0,sizeof(sad)); /* clear sockaddr structure   */
		sad.sin_family = AF_INET;            /* set family to Internet     */
		sad.sin_addr.s_addr = INADDR_ANY;    /* set the local IP address */

		if (config->p_port > 0)                          /* test for illegal value    */
			sad.sin_port = htons((u_short)(config->p_port));
		else {                                /* print error message and exit */
			CERR("bad port number " << config->p_port);
			EXIT();
		}

		/* Map TCP transport protocol name to protocol number */

		if ( ((int)(ptrp = getprotobyname("tcp"))) == 0)  {
			CERR("cannot map \"tcp\" to protocol number");
			EXIT();
		}

		/* Create a socket */
		sd = socket (PF_INET, SOCK_STREAM, ptrp->p_proto);
		if (sd < 0) {
			CERR("socket creation failed");
			EXIT();
		}

		int optval = 1;
		socklen_t optlen = sizeof(optval);
		if(setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &optval, optlen) < 0) {
			perror("setsockopt()");
			close(sd);
			EXIT();
		}
/*
		int optval = 1;
		socklen_t optlen = sizeof(optval);
		if(setsockopt(sd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
			perror("setsockopt()");
			close(sd);
			EXIT();
		}
*/
		/* Bind a local address to the socket */
		if (bind(sd, (struct sockaddr *)&sad, sizeof (sad)) < 0) {
			CERR("bind failed");
			EXIT();
		}

		/* Specify a size of request queue */
		if (listen(sd, config->p_tcp_service_queue_length) < 0) {
			CERR("listen failed");
			EXIT();
		}

		alen = sizeof(cad);

		/* Main server loop - accept and handle requests */
		CERR("Server up and running.");
		clients_connected = 0;
		while (1) 
		{
			cout << "SERVER: Waiting for contact ..." << endl;

			int sd_client = 0;
			if (  (sd_client = accept(sd, (struct sockaddr *)&cad, (socklen_t*)&alen)) < 0) 
			{
				CERR("accept failed");
				EXIT();
			}

			ServiceParams* serviceParams = new ServiceParams;  /* arguments for service function */
			serviceParams->sd = sd_client;
			serviceParams->config = config;
 			 
			CERR("client's address:"<<inet_ntoa((in_addr)cad.sin_addr));
			if (clients_connected >= config->p_max_clients_connected)
			{
				send_client_message(serviceParams->sd, "Too many clients connected");
			}
			else
			{
				DBG_FORCE("creating thread");
				pthread_create(&tid, NULL, service, (void *) serviceParams );
				pthread_detach(tid);
				DBG_FORCE("creating thread...done");
			}
		}
		close(sd);

	} // SERVER or STANDALONE
#endif
	delete config;

	return 0;
}
/* }}} */

#ifndef _WIN32
/* Server network communication routines {{{ */
//--------------------------------------------------------------------------------
//	sendall
//--------------------------------------------------------------------------------
/**
  @brief Send all data from buffer buf to socket s

  @param s socke+t
  @param *buf buffer
  @param *len length of buffer

  @return -1 on failure, 0 on success
*/
int sendall(int s, const char *buf, int *len)
{
	int total = 0;        // how many bytes we've sent
	int bytesleft = *len; // how many we have left to send
	int n = 0;

	while(total < *len) {
		DBG("sendall(): sent:"<<total<<" left:"<<bytesleft);
		n = send(s, buf+total, bytesleft, MSG_NOSIGNAL);
//		DBG("sendall(): n="<<n);
		if (n == -1) 
		{ 
			CERR("Client socket disconnected");
			break; 
		}
		total += n;
		bytesleft -= n;
	}

	*len = total; // return number actually sent here

	return n==-1?-1:0; // return -1 on failure, 0 on success
} 
// }}}

bool send_client_message(int sd, const string &msg)
{
	ostringstream output;
	output 
		<< "<?xml version=\"1.0\"?>" << endl
		<< "<results>" << endl
		<< "  <msg type=\"info\">" << msg << "</msg>" << endl
		<< "</results>" << endl;

	int len = output.str().length();
	if (sendall(sd, output.str().c_str(), &len) == -1) 
	{
		CERR("write(): Buffer written just partially");
		return false;
	} else {
		CERR("message sent");
	}
	return true;
}

//--------------------------------------------------------------------------------
//	service {{{
//--------------------------------------------------------------------------------
/**
  @brief function running in separate thread(s) to serve clients

  @param *arg structure for passing arguments to thread function
*/
void * service(void *arg)
{
	ServiceParams *serviceParams = (ServiceParams *) arg;
	int n, r;
	char buf[BUFFER];

	pthread_mutex_lock(&main_mutex);
	{
		clients_connected++;
		if (max_clients_connected < clients_connected)
			max_clients_connected = clients_connected;
		CERR("connected clients: "<<clients_connected);
	}
	pthread_mutex_unlock(&main_mutex);

	if ((n = read(serviceParams->sd, buf, BUFFER)) > 0) {

		buf[n] = 0;			// put the string-terminator at the end of received string		
		chomp(buf,n);		// remove newline character(s) from the end of the string
		cout << "incoming query: " << buf << endl;
		
		// query -> search results (text output)
		ostringstream output;
		processQuery(buf, serviceParams->config, &output);
		
		DBG("sendall...");
		DBG("sending data:"<<output.str().length());
		r = output.str().length();
		if (sendall(serviceParams->sd, output.str().c_str(), &r) == -1) 
		{
			CERR("write(): Buffer written just partially");
		} else {
			CERR("client served");
		}
		DBG("data sent");
	}
	else
	{
		CERR("ERROR: read() <= 0");
	}

	close(serviceParams->sd);

	pthread_mutex_lock(&main_mutex);
	{
		clients_connected--;
		CERR("connected clients: "<<clients_connected);
	}
	pthread_mutex_unlock(&main_mutex);

	delete serviceParams;
	pthread_exit(0);
}
// }}}

#endif

//--------------------------------------------------------------------------------
//	processQuery
//--------------------------------------------------------------------------------
/**
  @brief process the given query (parse it and generate search results)

  @param p_query given query
  @param *lexicon pointer to lexicon
  @param *indexer pointer to indexer
  @param *output pointer to output stream
*/
/* processQuery {{{ */
void processQuery(
		const string p_query, 
		SearchConfig *config,
		ostringstream *output) 
{
//	QueryKwdList queryKwdList(lexicon);
	QueryKwdList queryKwdList;
	//SearchSource searchSource = lattice; // let's search in lattice as default 
	Timer search_timer;
	search_timer.start();

	queryKwdList.hyps_per_stream = config->p_hyps_per_stream;
	queryKwdList.time_delta = config->p_time_delta;
	queryKwdList.fwd_nodes_count = config->p_fwd_nodes_count;
	queryKwdList.bck_nodes_count = config->p_bck_nodes_count;
	queryKwdList.phrase_kwd_neighborhood = config->p_phrase_kwd_neiborhood;
	queryKwdList.max_results = config->p_max_results;

	if (!parseQuery(p_query, config, &queryKwdList, output))
		return;

	Results results; // map (record+stream -> hypothesis list)
	results.SetQueryKwdList(&queryKwdList);
	results.SetAllDocumentsCount(config->indexerLvcsr->meetings.size());
	results.SetMaxHypothesesCount(queryKwdList.max_results);
	results.SetHardDecisionTreshold(queryKwdList.mThreshold);

	for (DocumentsList::iterator i=queryKwdList.mValidDocuments.begin(); i!=queryKwdList.mValidDocuments.end(); ++i) {
		DBG_FORCE("DocumentsList: " << i->first);
	}

	searchKeyword(config, &queryKwdList, &results);
TIMER_START;
	results.postProcess(config->mpNormalization);
TIMER_END("post-processing-results");
	search_timer.end();
	results.print(output, config, search_timer.val());
	CERR("Searching-time: "<<search_timer.val());
}
/* }}} */

//--------------------------------------------------------------------------------
//	parseQuery
//--------------------------------------------------------------------------------
/**
  @brief parse the given query and store it into queryKwdList structure

  @param p_query given query
  @param *config configuration parameters
  @param *searchSource 

  @return zero on success
*/
/* parseQuery {{{ */
bool parseQuery(
		const string p_query, 
		SearchConfig *config,
		QueryKwdList *queryKwdList,
		ostringstream *output) 
{
	queryKwdList->query_str = p_query;
	queryKwdList->mExactMatch = true;
	queryKwdList->mThreshold = config->p_hard_decision_treshold;
	DBG_FORCE("Searching for '"<<p_query<<"'");
	if (p_query != "") {
		float q_nb = -1;
		string::size_type kwdStartPos = 0;
		string::size_type kwdSpacePos = 0;
		QueryKwd kwd;
//		QueryKwd *pKwdTimeSet = NULL;
		bool firstPhraseKwd = false;
		bool lastPhraseKwd = false;
		int kwd_id = 1;

		int state = 0;
		bool epsilon = false; // epsilon transition (without moving to the next word)

		while (kwdStartPos < p_query.length()) {
			DBG("kwdStartPos:"<<kwdStartPos<<" p_query.length():"<<p_query.length());
			if (!epsilon) {
				for (;isKwdSeparator(p_query[kwdStartPos]); kwdStartPos++) {} // jump over white space
				
				if ((kwdSpacePos = p_query.find(' ', kwdStartPos)) == string::npos) { // find end of current keyword
					kwdSpacePos = p_query.length();
				}
				
				kwd.W = p_query.substr(kwdStartPos, kwdSpacePos - kwdStartPos);
				if (config->p_charset && config->p_upcase_query)
				{
					kwd.W = config->p_charset->upcase(kwd.W);
				}
				//kwd.W = upcase(kwd.W);
//				kwd.W = nonAsciiToOctal(kwd.W);
				DBG("kwd.W:"<<kwd.W);
				kwd.mId = kwd_id++;
				kwd.greaterThan = false;
				kwd.lessThan = false;
				kwd.confidence_min_isset = false;
				kwd.confidence_max_isset = false;
				kwd.time_min_type = null;
				kwd.time_max_type = null;
				kwd.nb_prev = q_nb;
				kwd.nb_next = q_nb;
				kwd.isNextInPhrase = false;
			} else {
				epsilon = false; // reset epsilon
			}

			DBG("switch ("<<state<<")");
			switch (state) {

				// beginning of the keyword
				case 0:
/*					
					// c(...) < x, c(...) > y, ...
					if (kwd.W.substr(0, 2) == "c(") {
						
						DBG("  trimming");
						kwd.W.erase(0,2);					// trim "c(" from beginning 
						kwd.W.erase(kwd.W.length()-1,1); 	// and ")" from the end
						DBG("  trimmed kwd: "<<kwd.W);
						// if the word is in the word list, load it's parameters. Otherwise add it to the word list
						QueryKwdList::iterator iKwd;
						if ((iKwd = queryKwdList->find(kwd.W)) != queryKwdList->end()) {
							kwd = *iKwd;
						} else {
							queryKwdList->push_back(kwd);
						}
						state = 1;
						
					} else
*/
					// phrase: "KWD1 KWD2"
					if (p_query[kwdStartPos] == '"') {
						DBG_MAIN("  Phrase start found");
						kwd.W.erase(0,1);// erase " from kwd
						while(kwd.W[0] == ' ')
							kwd.W.erase(0,1);
						kwdStartPos ++; 					// jump over "
						state = 2;
						firstPhraseKwd = true;
						if (kwd.W.length() > 0)
							epsilon = true; 					// we need to know whether this is the last word in phrase (ending with ")
						else
							epsilon = false;
					} else
/*
					// neighborhood: nb=<float>
					if (isQueryVar(p_query, kwdStartPos, "nb=")) 
					{						
						float f;
						if (!parse_query_convert_to_float(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "nb=").c_str(), "nb", output, &f))
							return false;
						q_nb = f;
						DBG_MAIN("  q_nb = "<<q_nb);

					} else

					// -hyps-per-stream
					if (isQueryVar(p_query, kwdStartPos, "&HPS=")) 
					{
						queryKwdList->hyps_per_stream = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&HPS=").c_str());
						DBG_MAIN("  hyps_per_stream = "<<queryKwdList->hyps_per_stream);

					} else

					// -time-delta
					if (isQueryVar(p_query, kwdStartPos, "&TD=")) 
					{
						queryKwdList->time_delta = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&TD=").c_str());
						DBG_MAIN("  time_delta = "<<queryKwdList->time_delta);

					} else

					// -fwd-nodes-count
					if (isQueryVar(p_query, kwdStartPos, "&FNC=")) 
					{
						queryKwdList->fwd_nodes_count = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&FNC=").c_str());
						DBG_MAIN("  fwd_nodes_count = "<<queryKwdList->fwd_nodes_count);

					} else

					// -bck-nodes-count
					if (isQueryVar(p_query, kwdStartPos, "&BNC=")) 
					{
						queryKwdList->bck_nodes_count = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&BNC=").c_str());
						DBG_MAIN("  bck_nodes_count = "<<queryKwdList->bck_nodes_count);

					} else
*/
					// -phrase-kwd-neighborhood
					if (isQueryVar(p_query, kwdStartPos, "&PNB=")) 
					{
						float f;
						if (!parse_query_convert_to_float(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&PNB=").c_str(), "&PNB", output, &f))
							return false;
						queryKwdList->phrase_kwd_neighborhood = f;
						DBG_MAIN("  phrase_kwd_neighborhood = "<<queryKwdList->phrase_kwd_neighborhood);

					} else
/*
					// -max-results
					if (isQueryVar(p_query, kwdStartPos, "&RES=")) 
					{
						queryKwdList->max_results = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&RES=").c_str());
						DBG_MAIN("  max_results = "<<queryKwdList->max_results);

					} else
*/
					if (isQueryVar(p_query, kwdStartPos, "&SRC=")) 
					{
//						CERR("ERROR: specifying a source (&SRC=...) is not supported");
//						EXIT();
						LatMeetingIndexerRecord meetingRec;
						meetingRec.path = getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&SRC=");
						queryKwdList->mValidDocuments.insert(make_pair(config->indexerLvcsr->meetings.getRecID(meetingRec.path), 0));
						DBG_MAIN("  SRC = "<<meetingRec.path);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&DOCLISTONLY")) 
					{
//						CERR("ERROR: specifying a source (&SRC=...) is not supported");
//						EXIT();
						queryKwdList->mDoclistOnly = true;
						DBG_MAIN("  DOCLISTONLY");

					} else

					if (isQueryVar(p_query, kwdStartPos, "&EXPANDWORDS")) 
					{
//						CERR("ERROR: specifying a source (&SRC=...) is not supported");
//						EXIT();
						queryKwdList->mExpandWordsToForms = true;
						DBG_MAIN("  EXPANDWORDS");

					} else

					// -hard-decision-threshold
					if (isQueryVar(p_query, kwdStartPos, "&THRESHOLD=")) 
					{
						float f;
						if (!parse_query_convert_to_float(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&THRESHOLD=").c_str(), "&THRESHOLD", output, &f))
							return false;
						queryKwdList->mThreshold = f;
						DBG_MAIN("  THRESHOLD = "<<queryKwdList->mThreshold);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&EM=")) 
					{
						queryKwdList->mExactMatch = (getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&EM=") == "true" ? true : false);
						DBG_MAIN("  EM = "<<queryKwdList->mExactMatch);

					} else
/*
					if (isQueryVar(p_query, kwdStartPos, "&OVERLAP")) 
					{
						queryKwdList->mOverlapping = true;
						DBG_MAIN("  OVERLAP = "<<queryKwdList->mOverlapping);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&EM")) 
					{
						queryKwdList->mExactMatch = true;
						DBG_MAIN("  EM = true");

					} else

					if (isQueryVar(p_query, kwdStartPos, "&NGRAM2PHN")) 
					{
						queryKwdList->mNgram2Phn = true;
						DBG_MAIN("  NGRAM2PHN = true");

					} else
						
					if (isQueryVar(p_query, kwdStartPos, "&PHN2NGRAM")) 
					{
						queryKwdList->mPhn2Ngram = true;
						DBG_MAIN("  PHN2NGRAM = true");

					} else
						
					if (isQueryVar(p_query, kwdStartPos, "&ALLSUCCESSFULTOKENS")) 
					{
						queryKwdList->mReturnAllSuccessfulTokens = true;
						DBG_MAIN("  ALLSUCCESSFULTOKENS = true");

					} else
						
					if (isQueryVar(p_query, kwdStartPos, "&ID=")) 
					{
						queryKwdList->mTermId = getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&ID=");
						DBG_MAIN("  mTermId = "<<queryKwdList->mTermId);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&SEARCHSOURCETYPE=")) 
					{
						queryKwdList->mSearchSource = SearchSource::ParseString(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&SEARCHSOURCETYPE=").c_str());
						DBG_MAIN("  SEARCHSOURCE = "<<queryKwdList->mSearchSource);

					} else

					// words order operators
					if (kwd.W == "<") {
						(--queryKwdList->end())->lessThan = true;
					} else 
						
					if (kwd.W == ">") {
						(--queryKwdList->end())->greaterThan = true;
					} else
						
					// time operator
					if (kwd.W.substr(0, 2) == "t(") {
						
						DBG("  trimming");
						kwd.W.erase(0,2);					// trim "c(" from beginning 
						kwd.W.erase(kwd.W.length()-1,1); 	// and ")" from the end
						DBG("  trimmed kwd: "<<kwd.W);
						// if the word is in the word list, load it's parameters. Otherwise add it to the word list
						QueryKwdList::iterator iKwd;
						if ((iKwd = queryKwdList->find(kwd.W)) != queryKwdList->end()) {
							kwd = *iKwd;
							pKwdTimeSet = &(*iKwd);
						} else {
							queryKwdList->push_back(kwd);
							pKwdTimeSet = &(*queryKwdList->rbegin());
						}
						DBG_MAIN("setting time for kwd:"<<kwd);
						state = 5;
						
					} else 
*/
					// other words
					{
						kwd.nb_prev = q_nb;
						kwd.nb_next = q_nb;
						queryKwdList->push_back(kwd);
						DBG("  normal keyword:"<<kwd.W);
					}
					break;
/*
				case 1:

					// confidence operators
					if (kwd.W == "<") {
						state = 3;
					} else if (kwd.W == ">") {
						state = 4;
					}

					break;
*/
				case 2:
					
					// within the phrase
					if (p_query[kwdSpacePos-1] == '"') {
						DBG_MAIN("  Phrase end found");
						kwd.W.erase(kwd.W.length()-1,1); // erase " from kwd
						kwdSpacePos --; // cut " off
						state = 0;
						lastPhraseKwd = true;
					}
					DBG_MAIN("  p_query[kwdSpacePos-1]:"<<p_query[kwdSpacePos-1]);

					kwd.nb_prev = firstPhraseKwd ? q_nb : queryKwdList->phrase_kwd_neighborhood;
					kwd.nb_next = lastPhraseKwd ? q_nb : queryKwdList->phrase_kwd_neighborhood;
					DBG_MAIN("  inPhrase...kwd:"<<kwd.W);
					
					if (firstPhraseKwd) {
						firstPhraseKwd = false;
					} else {
						if (queryKwdList->size() > 0)
							(--(queryKwdList->end())) -> isNextInPhrase = true;
					}
					if (lastPhraseKwd) lastPhraseKwd = false; 

					queryKwdList->push_back(kwd);
					
					break;
/*				
				case 3:
					// c(...) < x
					{
					float f;
					if (!parse_query_convert_to_float(kwd.W.c_str(), "c(...) < x", output, &f))
						return false;
					(--queryKwdList->end())->confidence_max = f;
					(--queryKwdList->end())->confidence_max_isset = true;
					state = 0;
					}
					break;

				case 4:
					// c(...) > x
					{
					float f;
					if (!parse_query_convert_to_float(kwd.W.c_str(), "c(...) > x", output, &f))
						return false;
					(--queryKwdList->end())->confidence_min = f;
					(--queryKwdList->end())->confidence_min_isset = true;
					state = 0;
					}
					break;
					
				case 5:
					// timing operators
					if (kwd.W == "<") {
						state = 6;
					} else if (kwd.W == ">") {
						state = 7;
					}
					break;

				case 6:
					// t(...) < x
					pKwdTimeSet->time_max = str2LatTime(kwd.W.c_str());
					if (pKwdTimeSet->time_max >= 0) {
						pKwdTimeSet->time_max_type = lse::time;
					} else {
						pKwdTimeSet->time_max_type = lse::percents;
						pKwdTimeSet->time_max *= -1; // convert it back to positive value
					}
					
					DBG_MAIN("time_max:"<<pKwdTimeSet->time_max);
					state = 0;
					break;

				case 7:
					// t(...) > x
					pKwdTimeSet->time_min = str2LatTime(kwd.W.c_str());
					if (pKwdTimeSet->time_min >= 0) {
						pKwdTimeSet->time_min_type = lse::time;
					} else {
						pKwdTimeSet->time_min_type = lse::percents;
						pKwdTimeSet->time_min *= -1; // convert it back to positive value
					}
					
					DBG_MAIN("time_min:"<<pKwdTimeSet->time_min);
					state = 0;
					break;
*/

					
			} // switch (state)		
			if (!epsilon)
			{
				kwdStartPos = kwdSpacePos + 1;
			}
		} // while (kwdStartPos < p_query.length())

		for (QueryKwdList::iterator kwd = queryKwdList->begin(); kwd != queryKwdList->end(); ++kwd) {
			// erase all " from the kwd {{{
			string::size_type loc = kwd->W.find( '"', 0 );
			while( loc != string::npos ) {
				kwd->W.erase(loc,1);
				loc = kwd->W.find_first_of( "\" \t\n", loc );
			}
			// }}}
		}

		{
			// erase empty keywords
			QueryKwdList::iterator kwd = queryKwdList->begin();
			while (kwd != queryKwdList->end())
			{
				if (kwd->W == "")
				{
					kwd = queryKwdList->erase(kwd);
				}
				else
				{
					kwd++;
				}
			}
		}
	} 
	return true;
}
/* }}} */

//--------------------------------------------------------------------------------
//	isValidPath
//--------------------------------------------------------------------------------
/**
  @brief check if words from query are bound (with the neighborhood operator)

  @param iCluster current cluster
  @param iKwdCur current keyword
  @param kwdList list of all keywords from query

  @return true if there are words in clusters which fits all bound keywords
*/
/* isValidPath {{{ */
/*
bool isValidPath(QueryKwd::RevIdxRecord *pRevIdxRecord, 
				 QueryKwdList::iterator iKwdCur, 
				 QueryKwdList* kwdList)
{

	// if we are at the last kwd in query 
	// or the current kwd is not binded with the next kwd from query, 
	// then this path is valid
	if (iKwdCur == kwdList->end() || iKwdCur->nb_next == -1) {
		return true;
	}
	
	// if current cluster did not set any following cluster, we are on the wrong path
	if (!pRevIdxRecord->mBestFwdKwdClusterFound) {
		return false;
	}	

	return isValidPath(pRevIdxRecord->mpBestFwdKwdCluster, ++iKwdCur, kwdList);
	
}
*/
/* }}} */

//--------------------------------------------------------------------------------
//	searchKeyword
//--------------------------------------------------------------------------------
/**
  @brief search for given parsed query using indexer and lexicon and write the results to output stream os

  @param *lexicon
  @param *indexer
  @param *queryKwdList parsed query
  @param *os output stream
*/
/* searchKeyword {{{ */
void searchKeyword(
		SearchConfig		*config,
		QueryKwdList 		*queryKwdList, 
		Results 			*pResults) 
{
	DBG("searchKeyword()");
	
	DBG_FORCE("queryKwdList.size():"<<queryKwdList->size());

	// load word2forms_regexlist {{{
	Nlp::RegexReplaceList word2forms_regex_replace_list;
	if ( config->p_word2forms_regexlist != "" )
	{
		word2forms_regex_replace_list.LoadFromFile(config->p_word2forms_regexlist.c_str());
	}
	// }}}
	
	Mgrams mgrams;
	if ( config->p_mgram_statistics != "" )
	{
		mgrams.LoadStatistics(config->p_mgram_statistics.c_str());
	}

	/* get wordID for keywords {{{ */
	for (QueryKwdList::iterator i = queryKwdList->begin(); i != queryKwdList->end(); ++i) 
	{
		DBG_FORCE("WORD: "<<i->W);

		bool is_oov = true;
		if (config->lexiconLvcsr)
		{
			is_oov = (i->W_id = config->lexiconLvcsr->word2id_readonly(i->W, false)) < 0;
		}

		// add the given word as the first variant to the list of variants
		if (!is_oov)
		{
			QueryKwd form;
			form.W = i->W;
			form.W_id = i->W_id;
			form.mId = i->mId;
			i->mForms.push_back(form);
		}

		// add word forms from WordMapper {{{
		if (config->p_word_mapper && queryKwdList->mExpandWordsToForms)
		{
			DBG("--------------------------------------------------");
			DBG("looking for mappings");
			WordMapper::IdList idlist;
			config->p_word_mapper->GetWordMappingIdList(i->W, &idlist);
			QueryKwd form;
			for (WordMapper::IdList::const_iterator it = idlist.begin(); it != idlist.end(); it++)
			{
				form.W_id = *it;
				bool found = false;
				for (QueryKwd::Forms::iterator iForm = i->mForms.begin(); iForm != i->mForms.end(); iForm++)
				{
					if (iForm->W_id == form.W_id) {
						found = true;
						break;
					}
				}
				if (found) {
					continue;
				}
				form.W = config->lexiconLvcsr->id2word_readonly(*it);
				form.mId = i->mId;
				i->mForms.push_back(form);
				DBG("map to form: "<<form.W<<" ["<<form.W_id<<"]");
				if (is_oov) 
				{
					//i->mForms.pop_back();
					is_oov = false; // the word is not an oov anymore because a word form which is in-vocabulary was added
					i->W_id = form.W_id; // @@@
				}
			}
			DBG("--------------------------------------------------");
		}
		// }}}

		// expand word to more forms {{{
		if ( word2forms_regex_replace_list.size() )
		{
			Nlp::WordList forms_list;
			string w = config->p_charset ? config->p_charset->locase(i->W) : i->W;
			Nlp::ExpandByRegexList(w, word2forms_regex_replace_list, &forms_list);
			for (Nlp::WordList::iterator iFormStr=forms_list.begin(); iFormStr!=forms_list.end(); iFormStr++)
			{
				QueryKwd form;
				form.W = config->p_charset ? config->p_charset->upcase(*iFormStr) : *iFormStr;
				DBG("new form: "<<form.W);
				form.W_id = config->lexiconLvcsr->word2id_readonly(form.W, false);
				DBG("new form W_id: "<<form.W_id);
				i->mForms.push_back(form);
			}
		}
		if ( config->p_word2forms != "" )
		{
			string cmd = config->p_word2forms;
			string::size_type loc = cmd.find( "\%s", 0 );
			if( loc == string::npos ) {
				CERR("ERROR: -word2forms has to contain \%s as a tag for word to be processed.");
				EXIT();
			}
			cmd.replace(loc, 2, i->W);
			DBG_FORCE("cmd: "<<cmd);

			// Read the output of the command and fill the pronunciation variants list with it
			string data;
			char buffer[512];
			//size_t len = 0;
			FILE* stream = popen(cmd.c_str(), "r");
			ssize_t read = 0;
			//while ( (read = getline(&buffer, &len, stream)) != -1 )
			while ( fgets(buffer, sizeof(buffer), stream) != NULL )
			{
				QueryKwd form;
				form.W = buffer;

				// Remove the newline characters
				size_t loc;
				if ((loc = form.W.rfind( 0x0d, read )) != string::npos)
				{
					form.W[loc] = 0x00;
				}
				if ((loc = form.W.rfind( 0x0a, read )) != string::npos)
				{
					form.W[loc] = 0x00;
				}

				form.W_id = config->lexiconLvcsr->word2id_readonly(form.W, false);

				DBG("trans: "<<form);
				i->mForms.push_back(form);
			}
			pclose(stream);
		}
		// }}}

		// if the word is not in the lvcsr lexicon
		if ( is_oov ) {
			cerr << "Word " << i->W << " has not been found in LVCSR lexicon... trying to convert to phonemes" << endl;
			pResults->mOovCount++;
			i->W_id = -pResults->mOovCount; // assign a unique W_id for each OOV
		}

		for (QueryKwd::Forms::iterator j=i->mForms.begin(); j!=i->mForms.end(); j++)
		{
			if ((queryKwdList->mSearchSource == SearchSource::LVCSR_OOVPHN && is_oov) ||
				 queryKwdList->mSearchSource == SearchSource::LVCSR_PHN ||
				 queryKwdList->mSearchSource == SearchSource::PHN)
			{
				if (config->g2p != NULL)
				{
					// get list of transcriptions - pronounciation forms
					list<gpt::PhnTrans::pt_entry> pron_list;
					DBG_FORCE("g2p->GetTranscs("<<j->W<<")");
					config->g2p->GetTranscs(config->p_charset->locase(j->W), &pron_list);
					//config->g2p->GetTranscs(j->W, &pron_list);
					DBG_FORCE("pron_list.size():"<<pron_list.size());
					for (list<gpt::PhnTrans::pt_entry>::iterator iPron=pron_list.begin(); iPron!=pron_list.end(); ++iPron)
					{
						DBG_FORCE("PRON: "<<iPron->trans);
					}

					// convert phonemes to multigrams
					if (config->p_mgram_statistics != "")
					{
						for (list<gpt::PhnTrans::pt_entry>::iterator iPron=pron_list.begin(); iPron!=pron_list.end(); ++iPron)
						{
							Mgrams::PhonemeString phn_str;
							string_split(iPron->trans, " ", (list<std::string>*)&phn_str);
							Mgrams::MgramStringList mgram_list;
							mgrams.PhonemeStringToMgrams(phn_str, config->p_mgram_max_length, &mgram_list);

							list<gpt::PhnTrans::pt_entry> pron_list_mgrams;
							gpt::PhnTrans::pt_entry form;
							// for each mgram segmentation
							for (Mgrams::MgramStringList::iterator iMgrams = mgram_list.begin(); iMgrams != mgram_list.end(); iMgrams++)
							{
								form.trans = iMgrams->ToString(' ');
								form.prob = iPron->prob;
								pron_list_mgrams.push_back(form);
								DBG_FORCE("MGRAM string: "<<iPron->trans);
							}
							j->SetType(QueryKwd::EType_MgramString);
							j->SetPronList(&pron_list_mgrams, queryKwdList, config->lexiconPhn); // lexiconPhn should be used to allow merging of word/subword systems. Therefore when using a hybrid system, the lexiconPhn should point to the lexiconLvcsr
						}
					}
					else // use phoneme strings directly
					{
						// create phn and ngram QueryKwdList for the current keyword form j
						j->SetType(QueryKwd::EType_PhonemeString);
						j->SetPronList(&pron_list, queryKwdList, config->lexiconPhn); 
					}
				}
				else if (config->p_word2mgrams != "")
				{
					string cmd = config->p_word2mgrams;
					string::size_type loc = cmd.find( "\%s", 0 );
					if( loc == string::npos ) {
						CERR("ERROR: -word2mgrams has to contain \%s as a tag for word to be transcribed to multigrams.");
						EXIT();
					}
					cmd.replace(loc, 2, j->W);
					DBG_FORCE("cmd: "<<cmd);

					// Read the output of the command and fill the pronunciation forms list with it
					list<gpt::PhnTrans::pt_entry> pron_list;
					string data;
					char buffer[512];
					//size_t len = 0;
					FILE* stream = popen(cmd.c_str(), "r");
					ssize_t read = 0;
					//while ( (read = getline(&buffer, &len, stream)) != -1 )
					while ( fgets(buffer, sizeof(buffer), stream) != NULL )
					{
						gpt::PhnTrans::pt_entry form;
						form.trans = buffer;
						form.prob = 1.0;

						// Remove the newline characters
						size_t loc;
						if ((loc = form.trans.rfind( 0x0d, read )) != string::npos)
						{
							form.trans[loc] = 0x00;
						}
						if ((loc = form.trans.rfind( 0x0a, read )) != string::npos)
						{
							form.trans[loc] = 0x00;
						}

						DBG("trans: "<<form.trans);
						pron_list.push_back(form);
					}
					pclose(stream);
					j->SetType(QueryKwd::EType_MgramString);
					j->SetPronList(&pron_list, queryKwdList, config->lexiconPhn); // lexiconPhn should be used to allow merging of word/subword systems. Therefore when using a hybrid system, the lexiconPhn should point to the lexiconLvcsr
				}
			}
		} // for each word's form
	} // for each word in query
	/* }}} */

	DBG_FORCE("queryKwdList before postprocessing:"); queryKwdList->print();
//	EXIT();
	/* convert multiword phoneme query to a single-word one {{{ */
	if (config->p_phn_query_multiword_to_singleword && queryKwdList->size() > 1)
	{
		vector<string> pron_list_str;

		QueryKwdList::iterator iKwdPrev = queryKwdList->begin();
		QueryKwdList::iterator iKwdCur = queryKwdList->begin(); iKwdCur++;
		// for all forms of the word
		for (QueryKwd::Forms::iterator iKwdPrevForm = iKwdPrev->mForms.begin();
			iKwdPrevForm != iKwdPrev->mForms.end();
			iKwdPrevForm++)
		{
			// for the first keyword simply copy the pronounciation variants
			for (PronVariants::iterator iPronPrev = iKwdPrevForm->mPronList.begin(); 
				iPronPrev != iKwdPrevForm->mPronList.end();
				iPronPrev ++)
			{
				pron_list_str.push_back(trim_outer_spaces(iPronPrev->mPtEntry.trans));
			}
		}
		// for all other keywords expand the pronounciation variants with "sil" and without "sil"
		while (iKwdCur != queryKwdList->end()) 
		{
			int pron_list_prev_size = pron_list_str.size();
			for (int iPronPrev = 0; iPronPrev < pron_list_prev_size; iPronPrev++)
			{
				// for all forms of the keyword iKwdCur
				for (QueryKwd::Forms::iterator iKwdCurForm = iKwdCur->mForms.begin();
					iKwdCurForm != iKwdCur->mForms.end();
					iKwdCurForm++)
				{
					// for all pronunciation variants of the word's form
					for (PronVariants::iterator iPronCur = iKwdCurForm->mPronList.begin(); 
						iPronCur != iKwdCurForm->mPronList.end();
						iPronCur ++)
					{
						string new_pron;
						new_pron = pron_list_str[iPronPrev] + " " + trim_outer_spaces(iPronCur->mPtEntry.trans);
						pron_list_str.push_back(new_pron);

						new_pron = pron_list_str[iPronPrev] + " sil " + trim_outer_spaces(iPronCur->mPtEntry.trans);
						pron_list_str.push_back(new_pron);
					}
				}
			}
			for (int iPronPrev = 0; iPronPrev < pron_list_prev_size; iPronPrev++)
			{
				pron_list_str.erase(pron_list_str.begin());
			}
			iKwdCur++;
		}
		list<gpt::PhnTrans::pt_entry> pron_list;
		for(vector<string>::iterator i=pron_list_str.begin(); i!=pron_list_str.end(); i++) 
		{ 
			DBG_FORCE("pron_list_str[]:"<<*i);	
			gpt::PhnTrans::pt_entry pron;
			pron.trans = *i;
			pron_list.push_back(pron);
		}

		string all_keywords = "";
		for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); iKwd++)
		{
			all_keywords += (all_keywords.length() > 0 ? " " : "") + iKwd->W;
		}
		QueryKwd kwd_all_keywords;

		QueryKwdList* p_query_singleword = new QueryKwdList;
		p_query_singleword->CopyParams(*queryKwdList); // copy query parameters
		p_query_singleword->push_back(kwd_all_keywords);
		p_query_singleword->begin()->SetPronList(&pron_list, p_query_singleword, config->lexiconPhn);
		p_query_singleword->print();
		queryKwdList = p_query_singleword;
	}
	/* }}} */

	/* allow only pronounciation variants longer then N phonemes (N=3) {{{ */
	for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); iKwd++)
	{
		for (QueryKwd::Forms::iterator iKwdForm = iKwd->mForms.begin(); 
			iKwdForm != iKwd->mForms.end(); 
			iKwdForm++)
		{
			if (iKwdForm->GetType() == QueryKwd::EType_PhonemeString)
			{
				bool changed = true;
				while (changed)
				{
					changed = false;
					for (PronVariants::iterator iPron = iKwdForm->mPronList.begin(); 
						iPron != iKwdForm->mPronList.end();
						iPron ++)
					{
						int phn_count = 0;
						for(string::size_type pos = 0; pos != string::npos; pos = iPron->mPtEntry.trans.find(" ", pos+1))
						{
							phn_count++;
						}
						if (phn_count < NGRAM_LENGTH)
						{
							DBG_FORCE("removing pronounciation variant:"<<iPron->mPtEntry.trans<<" phn_count:"<<phn_count);
							iKwdForm->mPronList.erase(iPron);
							changed = true;
							break;
						}
					} // for all pronunciation variants
				}
			}
		} // for all word's forms
	} // for all words in query
	/* }}} */

	DBG_FORCE("queryKwdList after postprocessing:"); queryKwdList->print();

	Search search(queryKwdList);

	search.SetLexiconPhn(config->lexiconPhn);
	search.SetLexiconLvcsr(config->lexiconLvcsr);
	search.SetIndexerPhn(config->indexerPhn);
	search.SetIndexerLvcsr(config->indexerLvcsr);
//	search.SetValidMeetings(validMeetings);

	search.SetRecordChannelColumn(config->p_record_channel_column);
	search.SetRecordNameCutoffColumn(config->p_record_cutoff_column);
	search.SetRecordDelim(config->p_record_delim);

	
	// create list of matching index entries (from the reverse index)
TIMER_START;
	DBG("config->p_kwd_max_occurrences_count: "<<config->p_kwd_max_occurrences_count);
	search.InvertedIndexLookup_LvcsrPhn(queryKwdList, config->p_searchindex_lvcsr, config->p_searchindex_phn, config->p_kwd_max_occurrences_count, config->p_kwd_occurrence_threshold);
TIMER_END("reverse-index-lookup");

	// show matched keywords
	if (config->p_show_all_matched_keywords) {
		search.PrintMatchedKeywords(queryKwdList);
	}

	/* show inverted index lookup results {{{ */
	if (config->p_show_reverse_index_lookup_results) {
		for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); ++iKwd) {
			for (QueryKwd::Forms::iterator iForm = iKwd->mForms.begin(); iForm != iKwd->mForms.end(); iForm ++)
			{
				DBG_FORCE("CLUSTERS LIST FOR KWD FORM: "<<iForm->W);
				DBG_FORCE("searchResults.size(): "<<iForm->searchResults.size());
//				iQueryKwd->recClusters.sort();
//				iQueryKwd->recClusters.print();
			}
		}
	}
	/* }}} */


	/* create PHN clusters {{{ */
TIMER_START;
	search.InvertedIndexLookupResultsClustering_Phn(queryKwdList);
TIMER_END("computing neighbourhood hypotheses (PHN)");

TIMER_START;
	search.FillInvIdxRecordsClusters_Phn(queryKwdList);
TIMER_END("creating clusters of neighbourhood hypotheses (PHN)");

TIMER_START;
	search.SortInvIdxRecordsClusters_Phn(queryKwdList);
TIMER_END("sorting clusters of neighbourhood hypotheses (PHN)");
	/* }}} */

	// show clusters
	if (config->p_show_all_clusters) 
	{
		search.PrintInvIdxRecordsClusters_Phn(queryKwdList);
	}

	/* create LVCSR + PHN clusters {{{ */
TIMER_START;
	search.CombineInvertedIndexLookupResults_LvcsrPhn(queryKwdList, config->p_kwd_max_occurrences_count);
TIMER_END("combining inverted index hits (LVCSR + PHN)");

TIMER_START;
	search.InvertedIndexLookupResultsClustering_LvcsrPhn(queryKwdList);
TIMER_END("computing neighbourhood hypotheses (LVCSR + PHN)");

	if (config->p_show_reverse_index_lookup_results_clusters)
	{
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("PrintInvertedIndexLookupResultsClusters_LvcsrPhn()");
		search.PrintInvertedIndexLookupResultsClusters_LvcsrPhn(queryKwdList);
		DBG_FORCE("PrintInvertedIndexLookupResultsClusters_LvcsrPhn()...done");
		DBG_FORCE("--------------------------------------------------");
	}

//	search.PrintInvertedIndexLookupResultsClusters_LvcsrPhn(queryKwdList);

TIMER_START;
	search.FillInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
TIMER_END("creating clusters of neighbourhood hypotheses (LVCSR + PHN)");

TIMER_START;
	search.SortInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
TIMER_END("sorting clusters of neighbourhood hypotheses (LVCSR + PHN)");
	/* }}} */

	// show clusters
	if (config->p_show_all_clusters) 
	{
		search.PrintInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
	}

	if (config->p_verify_candidates)
	{
		search.VerifyCandidates(config, queryKwdList, pResults);
	}
	else
	{
		search.InvIdxRecordsClusters2Results(config, queryKwdList, pResults);
	}
}
/* }}} */


/* isKwdSeparator {{{*/
bool isKwdSeparator(char c) 
{
	return (c==' ' || c=='\t' || c=='\n');
}
/*}}}*/

bool parse_query_convert_to_float(const char* str, const string varName, ostringstream *output, float *f)
{
	char * endptr = 0;
	*f = strtof(str, &endptr);
	if (endptr == str)
	{
		*output << "<msg type=\"error\">ERROR: "<<varName<<" value '"<<str<<"' is not a floating point number</msg>" << endl;
		return false;
	}
	return true;
}

//--------------------------------------------------------------------------------
//	str2LatTime
//--------------------------------------------------------------------------------
/**
  @brief converts the input string to seconds

  @param str input string

  @return time in seconds
*/
/* str2LatTime {{{ */
LatTime str2LatTime(std::string str) 
{
	LatTime res = 0;
	string strTmp = "";
	float multiplier = 0;
	for (unsigned int i=0; i<str.length(); i++) {
		switch (str[i]) {
			
			case 'h':
//				DBG_FORCE("hours: <"<<strTmp<<">");
				multiplier = 3600; 	// 1 hour = 3600 sec
				break;

			case 'm':
//				DBG_FORCE("minutes: <"<<strTmp<<">");
				multiplier = 60; 	// 1 min = 60 sec
				break;

			case 's':
//				DBG_FORCE("seconds: <"<<strTmp<<">");
				multiplier = 1; 	// 1 sec = 1 sec :o)
				break;

			case '%':
				DBG("percents: <"<<strTmp<<">");
				multiplier = -0.01;
				break;

			default:
				strTmp += str[i];

		}

		if (multiplier != 0) {
			res += atof(strTmp.c_str()) * multiplier;
			multiplier = 0;
			strTmp.clear();
		}
	}
	DBG("res: "<<res);
	return res;
}
/* }}} */

