#ifndef G2P_H
#define G2P_H

#include <list>
#include <string>
#include "gptrans.h"
#include "phntrans.h"

namespace lse {

class G2P
{
	protected:
		gpt::GPTrans GPT;
		gpt::PhnTrans PT;
		gpt::Lexicon Lex;
		
	public:
		bool Load(std::string filenameRules, std::string filenameSymbols, std::string filenameLexicon, int maxVariants, bool scaleProb, float probTresh);
		
		int GetTranscs(std::string word, std::list<gpt::PhnTrans::pt_entry> *list);
		
};

} // namespace lse

#endif
