#ifndef CHARSET_H
#define CHARSET_H

#include <string>

namespace lse {

class Charset
{
//		std::string id;
	public:
//		Charset(const char* charsetName) : id(charsetName) {}
		virtual std::string getIsoName() = 0;
		virtual unsigned char upcase(unsigned char c) = 0;
		virtual unsigned char locase(unsigned char c) = 0;
		virtual unsigned char from_ISO88592(unsigned char c) = 0;
		virtual unsigned char from_CP1250(unsigned char c) = 0;
		virtual std::string upcase(const std::string &str)
		{
			std::string res;
			res.reserve(str.length());
			for (unsigned int i=0; i<str.length(); i++)
			{
				unsigned char c = str[i];
				res.push_back(upcase(c));
			}
			return res;
		}
		virtual std::string locase(const std::string &str)
		{
			std::string res;
			res.reserve(str.length());
			for (unsigned int i=0; i<str.length(); i++)
			{
				unsigned char c = str[i];
				res.push_back(locase(c));
			}
			return res;
		}
		virtual std::string from_ISO88592(const std::string &str) = 0;
		virtual std::string from_CP1250(const std::string &str) = 0;
};

class Charset_ISO88591 : public Charset
{
	public:
		virtual std::string getIsoName() { return "ISO8859-1"; }
		unsigned char upcase(unsigned char c);
		unsigned char locase(unsigned char c);
//		std::string upcase(const std::string &str);
//		std::string locase(const std::string &str);

		unsigned char from_ISO88592(unsigned char c) { return 0; }
		unsigned char from_CP1250(unsigned char c) { return 0; }
		std::string from_ISO88592(const std::string &str) { return ""; }
		std::string from_CP1250(const std::string &str) { return ""; }
};

class Charset_CP1250 : public Charset
{
	public:
		virtual std::string getIsoName() { return "CP1250"; }
		unsigned char upcase(unsigned char c);
		unsigned char locase(unsigned char c);
		unsigned char from_ISO88592(unsigned char c);
		unsigned char from_CP1250(unsigned char c);
//		std::string upcase(const std::string &str);
//		std::string locase(const std::string &str);
		std::string from_ISO88592(const std::string &str);
		std::string from_CP1250(const std::string &str);
};

class Charset_ISO88592 : public Charset
{
	public:
		virtual std::string getIsoName() { return "ISO8859-2"; }
		unsigned char upcase(unsigned char c);
		unsigned char locase(unsigned char c);
                unsigned char from_CP1250(unsigned char c);
                unsigned char from_ISO88592(unsigned char c);
//		std::string upcase(const std::string &str);
//		std::string locase(const std::string &str);
		std::string from_CP1250(const std::string &str);
		std::string from_ISO88592(const std::string &str);
//		int outOfCzechCharset(const std::string &str);
};

class Charset_CP1251 : public Charset
{
	public:
		virtual std::string getIsoName() { return "CP1251"; }
		unsigned char upcase(unsigned char c);
		unsigned char locase(unsigned char c);
//		std::string upcase(const std::string &str);
//		std::string locase(const std::string &str);

		unsigned char from_ISO88592(unsigned char c) { return 0; }
		unsigned char from_CP1250(unsigned char c) { return 0; }
		std::string from_ISO88592(const std::string &str) { return ""; }
		std::string from_CP1250(const std::string &str) { return ""; }
};


} // namespace lse
#endif

