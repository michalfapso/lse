#include <algorithm> // sort()
#include <math.h>
#include "results.h"
#include "hsv2rgb.h"

using namespace std;
using namespace lse;

Results::DocumentGroup::~DocumentGroup()
{
	if (mpHypList)
	{
//		RELEASING THE ELEMENTS IN mpHypList IS NOT NEEDED, BECAUSE THEY ARE JUST REFERENCES AND THEY WERE CREATED IN ANOTHER OBJECT		
//		for (HypothesisList::iterator i=mpHypList->begin(); i!=mpHypList->end(); i++)
//			delete *i;
		delete mpHypList;
	}
}

Results::DocumentGroups::~DocumentGroups()
{
	for (iterator i=begin(); i!=end(); i++)
		if (i->second)
			delete i->second;
}

void Results::SetQueryKwdList(QueryKwdList *pQueryKwdList)
{
	mpQueryKwdList = pQueryKwdList;
}

void Results::postProcess(Normalization* pNormalization) 
{
	DBG_FORCE("Results::postProcess()");
	if (pNormalization)
	{
		this->NormalizeResults(pNormalization);
	}

//	this->sortResults();

	for (ResultsBaseType::iterator iHyp=this->begin(); iHyp!=this->end(); ++iHyp)
	{
		if (iHyp->score > 0)
			iHyp->score = 0;
		iHyp->score = exp(iHyp->score);
	}

	fillDocumentGroups();

	// if the query is longer than 1 word, filter out overlapping results
//	if (mpQueryKwdList->size() > 1)
	{
		DBG_FORCE("before joining overlapping results: "<<mDocumentGroups.mOccurrencesTotal);
		this->joinOverlappingResults();
		DBG_FORCE("after joining overlapping results: "<<mDocumentGroups.mOccurrencesTotal);
	}
}

void Results::joinOverlappingResults()
{
	for (DocumentGroups::iterator iDoc=mDocumentGroups.begin(); iDoc!=mDocumentGroups.end(); iDoc++)
	{
		DocumentGroup *pdoc = iDoc->second;
//		DBG("doc_id:"<<iDoc->first);
		
		// compare results only within 1 RecordStream
		HypothesisList::iterator iHyp1=pdoc->mpHypList->begin();
		while (iHyp1 != pdoc->mpHypList->end())
		{
			bool iHyp1_erased = false;
			HypothesisList::iterator iHyp2=pdoc->mpHypList->begin();
			while (iHyp2 != pdoc->mpHypList->end()) 
			{
				bool iHyp2_erased = false;
				Hypothesis* phyp1 = *iHyp1;
				Hypothesis* phyp2 = *iHyp2;
				//DBG("phyp1:"<<phyp1<<" phyp2:"<<phyp2);
				if (phyp1 == phyp2) {
					++iHyp2;
					continue; 
				}
				// do not compare hypotheses from different records
				//if ( !(phyp1->record == phyp2->record && phyp1->channel == phyp2->channel) ) continue;

				// don't compare the same results

				LatTime phyp1_start = phyp1->mStartTime;
				LatTime phyp1_end   = phyp1->mEndTime;
				LatTime phyp2_start = phyp2->mStartTime;
				LatTime phyp2_end   = phyp2->mEndTime;

				// overlapping detection
				if (is_overlapping(phyp1_start, phyp1_end, phyp2_start, phyp2_end))
				{
					//DBG_FORCE("overlapping results: ["<<phyp1_start<<".."<<phyp1_end<<"] ["<<phyp2_start<<".."<<phyp2_end<<"]");// << endl << "hyp1:" <<*phyp1 <<endl<< "hyp2:"<<*phyp2);

					bool phyp1_is_better = phyp1->keywords.size() > phyp2->keywords.size();
					if (phyp1->keywords.size() == phyp2->keywords.size()) {
						phyp1_is_better = phyp1->score > phyp2->score;
					}

					// remove the worse one
					if (phyp1_is_better) {
						//DBG("erase iHyp2");
						iHyp2 = pdoc->mpHypList->erase(iHyp2);
						iHyp2_erased = true;
					} else {
						//DBG("erase iHyp1");
						iHyp1 = pdoc->mpHypList->erase(iHyp1);
						iHyp1_erased = true;
						// have to break now to move to the next iHyp1
						break;
					}
					--pdoc->mOccurrencesTotal;
					--pdoc->mOccurrencesAboveThreshold;
					--mDocumentGroups.mOccurrencesTotal;
					--mDocumentGroups.mOccurrencesAboveThreshold;
//						overlapping = true;
//						break;
				}
				if (!iHyp2_erased) {
					++iHyp2;
				}
			} // for iHyp2
			if (!iHyp1_erased) {
				++iHyp1;
			}
//				if (overlapping) break;
		} // for iHyp1
	} // for all document groups
}
/*
void Results::joinOverlappingResults(LatMeetingIndexer* pDocumentsIdx)
{
	volatile bool overlapping = true;
	while (overlapping) 
	{
		overlapping = false;
		// compare results only within 1 RecordStream
		for (ResultsBaseType::iterator iHyp1=this->begin(); !overlapping, iHyp1!=this->end(); ++iHyp1)
		{
			for (ResultsBaseType::iterator iHyp2=this->begin(); !overlapping, iHyp2!=this->end(); ++iHyp2) 
			{
				// do not compare hypotheses from different records
				if ( !(iHyp1->record == iHyp2->record && iHyp1->channel == iHyp2->channel) ) continue;

				// don't compare the same results
				if (iHyp1 == iHyp2) continue; 

				LatTime iHyp1_start = iHyp1->mStartTime;
				LatTime iHyp1_end   = iHyp1->mEndTime;
				LatTime iHyp2_start = iHyp2->mStartTime;
				LatTime iHyp2_end   = iHyp2->mEndTime;

				// overlapping detection
				if (is_overlapping(iHyp1_start, iHyp1_end, iHyp2_start, iHyp2_end))
				{
					DBG_FORCE("overlapping results: ["<<iHyp1_start<<".."<<iHyp1_end<<"] ["<<iHyp2_start<<".."<<iHyp2_end<<"]");// << endl << "hyp1:" <<*iHyp1 <<endl<< "hyp2:"<<*iHyp2);

					bool iHyp1_is_better = iHyp1->keywords.size() > iHyp2->keywords.size();
					if (iHyp1->keywords.size() == iHyp2->keywords.size()) {
						iHyp1_is_better = iHyp1->score > iHyp2->score;
					}

					// remove the worse one
					if (iHyp1_is_better) {
						this->erase(iHyp2);
					} else {
						this->erase(iHyp1);
					}
					overlapping = true;
					break;
				}
			} // for iHyp2
			if (overlapping) break;
		} // for iHyp1
	} // while (overlapping)
}
*/
void Results::sortResults()
{
	// sort all results by score
	sort(this->begin(), this->end());
}

void Results::fillDocumentGroups()
{
	mDocumentGroups.mOccurrencesAboveThreshold = 0;
	mDocumentGroups.mOccurrencesTotal = 0;
	for (ResultsBaseType::iterator i=this->begin(); i!=this->end(); ++i) 
	{
		Hypothesis *phyp = &(*i);
		DocumentGroup *pdoc = NULL;
		DocumentGroups::iterator igroup = mDocumentGroups.find(phyp->recordId);
		if (igroup == mDocumentGroups.end()) {
			pdoc = new DocumentGroup;
			pdoc->mpHypList = new HypothesisList;
			pdoc->mDocumentName = phyp->record;
			pdoc->mDocumentId = phyp->recordId;
			pdoc->mOccurrencesAboveThreshold = 0;
			pdoc->mOccurrencesTotal = 0;
			mDocumentGroups.insert(make_pair(phyp->recordId, pdoc));
		} else {
			pdoc = igroup->second;
		}
		assert(pdoc);
		if (phyp->score > mHardDecisionTreshold)
		{
			mDocumentGroups.mOccurrencesAboveThreshold++;
			mDocumentGroups.mOccurrencesTotal++;
			pdoc->mOccurrencesAboveThreshold++;
			pdoc->mOccurrencesTotal++;
			pdoc->mpHypList->push_back(phyp);
		}
		else
		{
			mDocumentGroups.mOccurrencesTotal++;
			pdoc->mOccurrencesTotal++;
		}
	}

	mDocumentGroups.mDocumentsAboveThreshold = 0;
	mDocumentGroups.mDocumentsTotal = mDocumentGroups.size();
	for (DocumentGroups::iterator idoc = mDocumentGroups.begin(); idoc != mDocumentGroups.end(); idoc++)
	{
		if (idoc->second->mOccurrencesAboveThreshold > 0)
			mDocumentGroups.mDocumentsAboveThreshold++;
	}

	// score documents
	for (DocumentGroups::iterator idoc = mDocumentGroups.begin(); idoc != mDocumentGroups.end(); idoc++)
	{
		DocumentGroup *pdoc = idoc->second;
		
		//int tf = pdoc->mOccurrencesAboveThreshold; // term frequency
		float tf = 0; // term frequency
		for (HypothesisList::iterator ihyp = pdoc->mpHypList->begin(); ihyp != pdoc->mpHypList->end(); ihyp++)
		{
			Hypothesis *phyp = *ihyp;
			tf += phyp->score;
		}
		int df = mDocumentGroups.mDocumentsAboveThreshold; // document frequency
		int d = mAllDocumentsCount; // total number of documents
		float idf = df == 0 ? 0 : log( (float)d / df ); // inverse document frequency (do not divide by zero)
		if (idf == 0) // if the term appears in all documents, do not use the idf
			idf = 1;
		float w = 1.0;
		float sc = w * (float)tf * idf; // simple similarity coefficient
//		float sc = (2 * tf*idf) / (tf*idf * tf*idf); // Dice similarity coefficient
		/*
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("tf = "<<tf);
		DBG_FORCE("df = "<<df);
		DBG_FORCE("d = "<<d);
		if (df == 0)
			DBG_FORCE("idf = 0");
		else
			DBG_FORCE("idf = log("<<(d/df)<<") = "<<idf);
		DBG_FORCE("sc = "<<tf<<" * "<<idf<<" = "<<sc);
		*/
		pdoc->mScore = sc;

/*
		// cosine distance
		int N = mAllDocumentsCount;
		int ft = mDocumentGroups.mDocumentsAboveThreshold;
		float wqt = log2((float)N/ft + 1);

		int fdt = pdoc->mOccurrencesAboveThreshold;
		float wdt = log2((float)fdt + 1);

		pdoc->mScore = (wqt * wdt) / sqrt(wqt*wqt * wdt*wdt);

		DBG("N = "<<mAllDocumentsCount);
		DBG("ft = "<<ft);
		DBG("wqt = "<<wqt);
		DBG("fdt = "<<fdt);
		DBG("wdt = "<<wdt);
		DBG("score = (wqt * wdt) / sqrt(wqt*wqt * wdt*wdt) = "<<(wqt*wdt)<<" / sqrt("<<(wqt*wqt * wdt*wdt)<<") = "<<(wqt*wdt)<<" / "<<sqrt(wqt*wqt * wdt*wdt)<<" = "<<pdoc->mScore);
*/		
	}

	// ordering of documents
	mDocumentGroups.mOrderVector.reserve(mDocumentGroups.size());
	for (DocumentGroups::iterator idoc=mDocumentGroups.begin(); idoc!=mDocumentGroups.end(); idoc++)
		mDocumentGroups.mOrderVector.push_back(idoc->first);
	QuickSort(&mDocumentGroups.mOrderVector[0], 0, (int)mDocumentGroups.mOrderVector.size()-1, DocumentGroups::CmpDocuments, &mDocumentGroups);
}

bool Results::DocumentGroups::CmpDocuments(DocumentGroups *pDocumentGroups, const int &a, const int &b)
{
	DocumentGroups::iterator ia = pDocumentGroups->find(a);
	DocumentGroups::iterator ib = pDocumentGroups->find(b);
	assert(ia != pDocumentGroups->end());
	assert(ib != pDocumentGroups->end());
	return ia->second->mScore > ib->second->mScore;
//	return (*pDocumentGroups)[a] < (*pDocumentGroups[b]);
}

void Results::print(ostream *os, SearchConfig* config, float searchTime) 
{
	DBG_FORCE("Results::print()");
	DBG_FORCE("count: "<<this->size());
	if (config->p_results_output_type == ResultsETypeXml)
	{
		*os << "<?xml version=\"1.0\" encoding=\""<<config->p_charset->getIsoName()<<"\"?>" << endl; 
		*os << "<results>" << endl; 
		*os << "<header>" << endl; 
		string query_html_string = mpQueryKwdList->query_str;
		size_t pos = 0;
		while ((pos = query_html_string.find('&', pos)) != string::npos)
		{
			query_html_string.replace(pos, 1, "&amp;");
			pos += 5;
		}
		*os << "  <query>" << endl; 
		*os << "    <user>" << query_html_string << "</user>" << endl;
		*os << "    <expanded>" << endl; 
		for (QueryKwdList::iterator iKwd = mpQueryKwdList->begin(); iKwd != mpQueryKwdList->end(); iKwd ++) 
		{
			*os << "      <word str=\"" << iKwd->W << "\">";
			for (QueryKwd::Forms::iterator iForm = iKwd->mForms.begin(); iForm != iKwd->mForms.end(); iForm ++)
			{
				*os << iForm->W << " ";
			}
			*os << "</word>" << endl;
		}
		*os << "    </expanded>" << endl; 
		*os << "  </query>" << endl; 
		os->setf(ios::fixed);
		streamsize prec = os->precision();
		os->precision(4);
		*os << "  <search_time>"<< searchTime <<"</search_time>" << endl; 
		os->precision(prec);
		os->unsetf(ios::fixed);
		*os << "  <threshold>"<< mHardDecisionTreshold <<"</threshold>" << endl; 
		*os << "  <occurrences_total>"<<mDocumentGroups.mOccurrencesTotal<<"</occurrences_total>" << endl; 
		*os << "  <occurrences_above_threshold>"<<mDocumentGroups.mOccurrencesAboveThreshold<<"</occurrences_above_threshold>" << endl; 
		*os << "  <all_indexed_documents>"<<mAllDocumentsCount<<"</all_indexed_documents>" << endl; 
		*os << "  <documents_total>"<<mDocumentGroups.mDocumentsTotal<<"</documents_total>" << endl; 
		*os << "  <documents_above_threshold>"<<mDocumentGroups.mDocumentsAboveThreshold<<"</documents_above_threshold>" << endl; 
		*os << "</header>" << endl; 
	}
	else if (config->p_results_output_type == ResultsETypeXmlTranscripts)
	{
 		*os << "<transcription>" << endl;
 		*os << "  <data>" << endl;
	}
	else if (config->p_results_output_type == ResultsETypeMlf)
	{
 		*os << "!#MLF#!" << endl;
	}
	else if (config->p_results_output_type == ResultsETypeXml_std)
	{
 		*os << "  <detected_termlist termid=\""<<mpQueryKwdList->mTermId<<"\""
		    << " term_search_time=\""<<searchTime<<"\""
		    << " oov_term_count=\""<<mOovCount<<"\">" << endl;
	}
	
	if (this->mOovCount > 0)
	{
		if (config->p_results_output_type == ResultsETypeXmlTranscripts ||
			config->p_results_output_type == ResultsETypeXml)
		{
			*os << "    <msg type=\"error\">" 
				<< "One or more words are not in the recognition vocabulary.";

			if (mpQueryKwdList->mSearchSource == SearchSource::LVCSR_OOVPHN ||
					mpQueryKwdList->mSearchSource == SearchSource::LVCSR_PHN ||
					mpQueryKwdList->mSearchSource == SearchSource::PHN)
			{
				*os << " The search results may have poor accuracy.";
			}

			*os << "</msg>" << endl;
		}
	}

	if (this->size() == 0)
	{
		if (config->p_results_output_type == ResultsETypeXmlTranscripts || 
			config->p_results_output_type == ResultsETypeXml)
		{
			*os << "    <msg type=\"info\">No results found.</msg>" << endl;
		}
	}

	if (config->p_results_output_type == ResultsETypeXml)
	{
		*os << "<data>" << endl;
		//for (DocumentGroups::iterator idoc = mDocumentGroups.begin(); idoc != mDocumentGroups.end(); idoc++)
		for (unsigned int i=0; i<mDocumentGroups.size(); i++)
		{
			DocumentGroups::iterator idoc = mDocumentGroups.find(mDocumentGroups.mOrderVector[i]);
			assert(idoc != mDocumentGroups.end());
			DocumentGroup *pdoc = idoc->second;
			*os << "<document id=\"" << pdoc->mDocumentName << "\""
			    << " occurrences_total=\""<<pdoc->mOccurrencesTotal<<"\""
			    << " occurrences_above_threshold=\""<<pdoc->mOccurrencesAboveThreshold<<"\""
			    << " length=\"" << config->indexerLvcsr->meetings.getVal(pdoc->mDocumentId).length << "\"";

			os->setf(ios::fixed);
			streamsize prec = os->precision();
			os->precision(4);
			*os << " score=\"" << pdoc->mScore << "\">";
			os->precision(prec);
			os->unsetf(ios::fixed);

			*os << endl;

			//DBG_FORCE("mDoclistOnly:"<<mpQueryKwdList->mDoclistOnly);
			if (!mpQueryKwdList->mDoclistOnly)
			{
				for (HypothesisList::iterator ihyp = pdoc->mpHypList->begin(); ihyp != pdoc->mpHypList->end(); ihyp++)
				{
					Hypothesis *phyp = *ihyp;
					LatTime start = phyp->mStartTime - config->p_results_start_time_subtractor;
					LatTime end = phyp->mEndTime;
					*os << "    <occurrence"
						<< " start=\""<< start <<"\""
						<< " end=\""<< end <<"\"";
					os->setf(ios::fixed);
					streamsize prec = os->precision();
					os->precision(4);
					*os << " score=\""<< fixed << phyp->score << "\"";
					os->precision(prec);
					os->unsetf(ios::fixed);
					*os << ">";
					phyp->printText(os);
					*os << "</occurrence>" << endl;
				}
			}
			*os << "</document>" << endl;
		}
		*os << "</data>" << endl;
		*os << "</results>" << endl; 
	}
	else
	{
		for (ResultsBaseType::iterator i=this->begin(); i!=this->end(); ++i) 
		{		
			LatTime start = i->mStartTime - config->p_results_start_time_subtractor;
			LatTime end = i->mEndTime;
			string::size_type dotPos = i->record.rfind(".binlat");
			if (dotPos != string::npos) {
				i->record.erase(dotPos, i->record.length() - dotPos);
			}

			if (config->p_results_convert_to_local_time)
			{
				string latname_nopath = i->record;
				string::size_type strPos = latname_nopath.rfind(PATH_SLASH);
				latname_nopath.erase(0, strPos+1);

				LatTime latStartTime = 0;
				string sStartTime = latname_nopath;
				string::size_type pos_1st = 0;
				for (int field_idx=1; field_idx < config->p_lat_filename_start_time_field; field_idx++) {
					pos_1st = sStartTime.find (config->p_record_delim, pos_1st+1); // find first _ in latname
					DBG("pos_1st:"<<pos_1st);
				}
				if (pos_1st != string::npos) {
					string::size_type pos_2nd = latname_nopath.find (config->p_record_delim, pos_1st+1); // find second _ in latname
					sStartTime = sStartTime.substr(pos_1st+1, pos_2nd - pos_1st - 1);
					latStartTime = atof(sStartTime.c_str()) / config->p_latname_time_multiplier;
					DBG("Getting start time from filename...sStartTime:"<<sStartTime<<" latStartTime:"<<latStartTime<<endl);
				} else {
					CERR("Error in lattice's name: Expecting _ in \"" << latname_nopath << "\" ... setting start time to 0.0");
				}
				start -= latStartTime;
				end -= latStartTime;
			}
			if (config->p_results_output_type == ResultsETypeXml)
			{
				*os << "<hypothesis stream=\""<<i->stream<<"\" "
					<< " record=\""<<i->record<<"\" "
					<< " score=\""<< fixed << setprecision(4) << i->score <<"\" >" << endl;
				i->printXml(os);
				*os << "</hypothesis>" << endl;
			}
			else if (config->p_results_output_type == ResultsETypeXmlTranscripts)
			{
				int h=0,m=0,s=0;
				s = (int)start + (int)(config->p_results_start_time_subtractor); // to have the correct timelabel
				h = (int)(s / 3600);
				s %= 3600;
				m = (int)(s / 60);
				s %= 60;
				ostringstream bgcolor;
				int hue_max = 120;
				int hue_min = 0;
				int hue = (int)( hue_min + (i->score * (hue_max - hue_min)) );
				float sat = 0.7f;
				float val = 1;
				int r=0, g=0, b=0;
				convert_hsv_to_rgb(hue,sat,val,&r,&g,&b);
	/*			
				int color_min = 100;
				int color_max = 255;
				int r = (int)( color_min + (1-exp_score) * (color_max - color_min) );
				int g = (int)( color_min + exp_score * (color_max - color_min) );
				int b = (int)( color_min );
	*/			
				bgcolor.setf ( ios::hex, ios::basefield );
				bgcolor << "#" << (int)r << (int)g << (int)b;
				//bgcolor << "#" << hue << " " << sat << " " << val;
				//bgcolor << "#" << (int)r << " " << (int)g << " " << (int)b;
				*os << "    <record "
					<< " document=\""<<i->record<<"\""
					<< " channel=\""<<i->channel<<"\""
					<< " start=\""<< start <<"\""
					<< " end=\""<< end <<"\""
					<< " score=\""<< (int)(i->score*100) <<"\""
					<< " bgcolor=\""<< bgcolor.str() <<"\""
					<< " timelabel=\""<< h << "h:" << m << "m:" << s << "s\""
					<< ">" << endl;
				i->printText(os); *os << endl;
				*os << "    </record>" << endl;
			}
			else if (config->p_results_output_type == ResultsETypeMlf)
			{
				unsigned int mlf_time_multiplier = 10000000;
				*os << "\"*/" << i->record << ".rec\"" << endl
					<< (unsigned long)(mlf_time_multiplier/100 * round(start*100)) << " "
					<< (unsigned long)(mlf_time_multiplier/100 * round(end*100)) << " "
					<< mpQueryKwdList->mTermId << " "
					<< fixed << setprecision(4) << i->score
					<< endl
					<< "." 
					<< endl;
			}
			else if (config->p_results_output_type == ResultsETypeXml_std)
			{
				*os << "    <term file=\""<< i->record <<"\""
					<< " channel=\""<<i->channel<<"\""
					<< " tbeg=\""<< start <<"\""
					<< " dur=\""<< end - start <<"\""
					<< " score=\""<< fixed << setprecision(4) << i->score <<"\""
					<< " decision=\""<< (i->decision ? "YES" : "NO") << "\""
	//				<< " pron=\""<< (i->pron) << "\""
					<< "/>" << endl;
			}
			else if (config->p_results_output_type == ResultsETypeExp)
			{
				*os << i->record
					<< " " << mpQueryKwdList->mTermId
					<< " " << mOovCount
					<< " " << start
					<< " " << end
					<< endl;
			}
			else
			{
				*os << "HYPOTHESIS:" << endl;
		//		*os << "STREAM: \t" << (i->first).stream << endl;
				//*os << "STREAM: \t" << i->stream << endl;
				*os << "STREAM: LVCSR\t" << endl;
				*os << "RECORD: \t" << i->record << endl;
	//			*os << "CHANNEL: \t" << i->channel << endl;
				*os << "COUNT: \t1" << endl;
				*os << "SCORE: \t" << fixed << setprecision(4) << i->score<< endl;
				*os << *i << endl;
			}
		}
	}
	if (config->p_results_output_type == ResultsETypeXml_std)
	{
 		*os << "  </detected_termlist>" << endl;
	}
	else if (config->p_results_output_type == ResultsETypeXmlTranscripts)
	{
 		*os << "  </data>" << endl;
 		*os << "</transcription>" << endl;
	}
	DBG_FORCE("Results::print()...done");
}

void Results::push_back(lse::Hypothesis hyp)
{
	// check if there is some limit on number of hypotheses and
	// if the hypothesis is not empty
	// 
	// TODO it will not work if &ALLSUCCESSFULTOKENS is set because
	// the first rescored hypothesis may return even more results than
	// is set in &RES=... so no other rescored hypothesis will be pushed
	// into results collection
	
	//DBG("Results::push_back(): "<<hyp);
	if (((int)this->size() < mMaxResultsCount || mMaxResultsCount == -1) &&
		(hyp.keywordsCount > 0))
	{
		((ResultsBaseType*)this)->push_back(hyp);
	}
}

void Results::SetMaxHypothesesCount(int max)
{
	mMaxResultsCount = max;
}
		
/*
std::ostream& operator<<(std::ostream& os, const Results& res)
{
	for (Results::const_iterator i=res.begin(); i!=res.end(); ++i) 
	{		
		os << "HYPOTHESIS:" << endl;
		os << "STREAM: \t" << (i->first).stream << endl;
		os << "RECORD: \t" << (i->first).record << endl;
		os << "COUNT: \t" << (i->second).size() << endl;
		for (vector<Hypothesis>::const_iterator j=(i->second).begin(); j!=(i->second).end(); ++j) {
			os << *j << endl;
		}
	}
	return os;
}
*/

void Results::NormalizeResults(Normalization* pNormalization)
{
	DBG_FORCE("Results::NormalizeResults()");
	
	for (Results::iterator iRes=this->begin(); iRes!=this->end(); ++iRes)
	{
		DBG_FORCE("result score:"<<iRes->score);
		DBG_FORCE("result:"<<*iRes);
		list<const std::string*> words;

		iRes->score = pNormalization->NormalizeScore(&iRes->keywords, iRes->score);

//		iRes->decision = iRes->score > mHardDecisionTreshold;
		DBG_FORCE("result score NORMALIZED:"<<iRes->score);
		DBG_FORCE("result NORMALIZED:"<<*iRes);
	}
}

