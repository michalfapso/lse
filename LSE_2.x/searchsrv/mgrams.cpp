#include "mgrams.h"
#include <cstdlib>
#include <cmath>

#define MGRAM_PHONEME_DELIMITER 'X'

using namespace std;

void Mgrams::Statistics::Add(StatRecord* pRec)
{
	mContainer.insert(make_pair(pRec->mLabel.ToString(MGRAM_PHONEME_DELIMITER), pRec));
}

Mgrams::StatRecord* Mgrams::Statistics::GetRecord(const std::string phnStr)
{
	Container::iterator i = mContainer.find(phnStr);
	return i != mContainer.end() ? i->second : NULL;
}

void Mgrams::LoadStatistics(const char* filename)
{
	ifstream in(filename);
	if (!in.good())
	{
		CERR("ERROR: Mgram::LoadStatistics(): failed to open input file '"<<filename<<"'");
		EXIT();
	}

	DBG("Mgrams::LoadStatistics()");
	try
	{
		int state = 0;
		string s;
		StatRecord* p_rec = NULL;
		while(!in.eof())
		{
			//DBG("state:"<<state);
			switch (state)
			{
				case 0:
					p_rec = new StatRecord;
					state = 1;

				case 1:
					in >> s;
					if (s == "->")
						state = 2;
					else
						p_rec->mLabel.push_back(s);
					break;

				case 2:
					in >> s;
					p_rec->mLogLik = atof(s.c_str());
					in >> s;
					p_rec->mOccurrences = atof(s.c_str());
					mStatistics.Add(p_rec);
					//DBG("StatRecord: "<<*p_rec);
					state = 0;
					break;
			}
		}
	}
	catch (exception &e)
	{
		CERR("ERROR: Mgram::LoadStatistics() processing the input file ("<<filename<<"): "<<e.what());
		EXIT();
	}
	DBG("Mgrams::LoadStatistics()...done");
}

Mgrams::Score Mgrams::PSeq(int from, int to, const PhonemeString& seq)
{
	//    my( $From, $To, $Seq, $LSeq ) = @_;    
	//    my( $Ob, @Pom, $PomFrom );
	if( from > to ){
		CERR("Warning, in PSeq from > to!");
		abort();
	}
	if( (from < 1) || (to > (int)seq.size()) )
	{
		return Prob0;
	}
	else
	{
		string s = seq.ToString(from, to, MGRAM_PHONEME_DELIMITER);

		StatRecord* p_rec;
		if( (p_rec = mStatistics.GetRecord(s)) != NULL ) {
			return p_rec->mLogLik;
		} else {
			return ProbNotExists * (to - from + 1);
//			CERR("ERROR: "<<s<<" not found!");
		}
	}
}


void Mgrams::PhonemeStringToMgrams(const PhonemeString &phonemeString, unsigned int maxMgramLength, MgramStringList* pMgramStringList)
{
	class Minima
	{
		public:
			float mProb;
			string mMgram;
			int mLength;
	};
	Minima minimas[400];
	unsigned int array_delta = 100; // problem with negative index in array -> shift by this value
	unsigned int GlobalSegments = 0;

	//DBG("NGrams::PhonemeStringToMgrams()");
	//DBG("forward");
	// forward
	for(unsigned int i=1; i<=maxMgramLength; i++){
		minimas[ array_delta + -1*i ].mProb = Prob0;
	}
	minimas[ array_delta + 0 ].mProb = 0;
	for(unsigned int t=1; t<=phonemeString.size(); t++){
		//DBG("t:"<<t);
		Minima min;
		min.mProb = Prob0;
		min.mMgram = "";
		min.mLength = 0;
		for(unsigned int i=1; i<=maxMgramLength; i++){
			//DBG("i:"<<i);
			Score pom = minimas[array_delta + t-i].mProb + PSeq( t-i+1, t, phonemeString);
			//DBG("pom:"<<pom<<" min.mProb:"<<min.mProb);
			if( pom > min.mProb){
				//DBG("pom > min.mProb ... "<<pom<<" > "<<min.mProb);
				min.mProb = pom;
				min.mMgram = phonemeString.ToString(t-i+1, t, MGRAM_PHONEME_DELIMITER);
				min.mLength = i;
			}
		}// for n
		minimas[ array_delta + t].mProb = min.mProb;
		minimas[ array_delta + t].mMgram = min.mMgram;
		minimas[ array_delta + t].mLength = min.mLength;
		//DBG("minimas["<<t<<"]: "<<min.mProb<<" "<<min.mMgram<<" "<<min.mLength);
	}// for t

	//DBG("backward");
    // backward
    unsigned int t = phonemeString.size();
    int MGramCount = 0;
	map<string,int> occurrences;
	PhonemeString mgram_string;
	while( t>=1 )
	{
		//DBG("--------------------------------------------------");
		//DBG("t:"<<t);
		string mgram = minimas[ array_delta + t].mMgram;
		//DBG("mgram:"<<mgram);
		if( occurrences.find(mgram) == occurrences.end() ){
			occurrences.insert(make_pair(mgram, 0));
		}
		occurrences[mgram] += 1;
		//DBG("occurrences:"<<occurrences[mgram]);
		//float PosNow = minimas[ array_delta + t ].mProb;
		//float PosBef = minimas[ array_delta + t - minimas[ array_delta + t ].mLength ].mProb;
		//DBG("PosNow:"<<PosNow<<" PosBef:"<<PosBef);
		StatRecord* p_rec;
		Score PosDelta;
		if( (p_rec = mStatistics.GetRecord(minimas[ array_delta + t ].mMgram)) != NULL ) {
			PosDelta = p_rec->mLogLik;
		} else {
			PosDelta = ProbNotExists;
		}
		//DBG("PosDelta:"<<PosDelta);
//        if( ceil( (PosNow - PosBef )*1000) != ceil( PosDelta*1000 ) ){
            //print( Minimas[ array_delta + t ]{"NGram"}." ".ceil((PosNow - PosBef )*1000)." ".ceil(PosDelta*1000 )."!!!!!!!!!!!!!!!!!!!! Somewhere is an error\n");
//		}
		//print(Minimas[ array_delta + t]{"NGram"}."-");
		MGramCount++;
		mgram_string.push_front(minimas[ array_delta + t].mMgram);
		//DBG("mgram_string.push_back(): "<<minimas[ array_delta + t].mMgram);
		t = t - minimas[ array_delta + t ].mLength;
		//DBG("t="<<t);
		GlobalSegments++;
	} // while
	DBG("mgram_string: " <<mgram_string);
	pMgramStringList->push_back(mgram_string);
}

