#ifndef WORDMAPPER_H
#define WORDMAPPER_H

#include <string>
#include <map>
#include <list>
#include "lattypes.h"
#include "lexicon.h"

namespace lse {

class WordMapper
{
	public:
		class IdList : public std::list<ID_t>
		{
			public:
				void push_back_if_not_there(ID_t what) {
					for (iterator i=begin(); i!=end(); i++) {
						if (*i == what)
							return;
					}
					push_back(what);
				}
		};
	protected:
		typedef std::map<std::string, IdList*> Map;
		typedef std::map<ID_t, IdList*> Clusters;
		Map mMap;
		Clusters mClusters;
		Lexicon* mpLexicon;

		int AddClusterIdList(ID_t wid, WordMapper::IdList* pidlist);
	public:
		~WordMapper();
		bool LoadMappingFromFile(std::string filename);
		bool LoadClustersFromFile(std::string filename);
		int GetWordMappingIdList(const std::string& word, WordMapper::IdList* pidlist);
		void SetLexicon(Lexicon* pLex) { mpLexicon = pLex; }
};
}

#endif
