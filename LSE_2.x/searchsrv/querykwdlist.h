#ifndef QUERYKWDLIST_H
#define QUERYKWDLIST_H

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <iomanip>
#include "lattypes.h"
#include "latindexer.h"
#include "lexicon.h"
#include "phntrans.h"
//#include "InvIdxRecordsClusters.h"

namespace lse {

#define DEFAULT_HYPS_PER_STREAM 			2
#define DEFAULT_TIME_DELTA 					0
#define DEFAULT_FWD_NODES_COUNT 			0
#define DEFAULT_BCK_NODES_COUNT 			0
#define DEFAULT_PHRASE_KWD_NEIGHBORHOOD 	0.0
#define DEFAULT_MAX_RESULTS 				-1 // -1 means no limit

enum TimeValueType {
	null,
	percents,
	time
};

class QueryKwdList;
class QueryKwd;
class PronVariant;
typedef std::vector<PronVariant> PronVariants;
class InvIdxRecordsClusters;


class QueryKwd 
{
	protected:
		void GeneratePronQueryKwdList(PronVariant *pv, Lexicon *pLexiconPhn, QueryKwdList* pParentQueryKwdList);

	public:
		class InvIdxRecord : public LatIndexer::Record
		{
			public:
				typedef std::vector< InvIdxRecord* > InvIdxRecordList;
				InvIdxRecordList mFwdClusters;
				InvIdxRecordList mBckClusters;
				InvIdxRecord* mpBestFwdKwdCluster;
				bool mBestFwdKwdClusterFound; 				// true, if there is some matching cluster in the following kwd
		};

		class InvIdxRecords : public LatIndexer::RecordsReader
		{
			public:
				std::vector<InvIdxRecord> mvByConfidence;

				InvIdxRecords() : LatIndexer::RecordsReader() {}

				void push_back(const LatIndexer::Record &rec);
				unsigned int size();
				InvIdxRecord* FindOccurrence(const LatIndexer::Occurrence& where);
		};

		class InvIdxRecordsCursor 
		{
				InvIdxRecords *mpInvIdxRecords;
				std::vector<InvIdxRecord>::iterator miInvIdxRecord;
			public:
				InvIdxRecordsCursor(InvIdxRecords *pInvIdxRecords) : mpInvIdxRecords(pInvIdxRecords) 
				{
					miInvIdxRecord = mpInvIdxRecords->mvByConfidence.begin(); 
				}

				void Next()
				{
					miInvIdxRecord++;
				}

				InvIdxRecord& Get()
				{
					return *miInvIdxRecord;
				}
		};

		//typedef std::vector<InvIdxRecord *> InvIdxRecordList;


	public:
		typedef std::vector<QueryKwd> Forms;
		enum EType { EType_Word, EType_MgramString, EType_PhonemeString };

		std::string 	W;
		ID_t 			W_id;
		ID_t			mId; ///< id of the word within the query. If there are more words with the same W_id, the mId is always unique.
		float 			nb_prev; ///< previous word's neighborhood (how far can be two keywords)
		float 			nb_next; ///< next word's neighborhood (how far can be two keywords)
		bool 			isNextInPhrase; ///< if the current kwd is in phrase with the next one in QueryKwdList
		float 			confidence_min; ///< min confidence treshold
		float 			confidence_max; ///< max confidence treshold
		bool 			confidence_min_isset;
		bool 			confidence_max_isset;
		LatTime 		time_min;
		LatTime 		time_max;
		TimeValueType 	time_min_type;
		TimeValueType 	time_max_type;
		bool 			lessThan;
		bool 			greaterThan;
		bool 			mIsNgram;
		std::string 	mInvertedIndex;
		
		Forms									mForms; ///< vector of synonyms or substandard forms of the keyword
		InvIdxRecords 							searchResults;
	//	LatIndexer::RecordClusters 				recClusters;
		std::vector< ID_t >						phonemes; ///< WordID of all phonemes in the n-gram
		PronVariants							mPronList;
		InvIdxRecordsClusters*					mpClusters; ///< combined LVCSR and PHN
		EType			mType;
		

		QueryKwd() : W(""),
					 W_id(-1), 
					 mId(-1),
					 nb_prev(-1), 
					 nb_next(-1), 
					 isNextInPhrase(false), 
					 confidence_min(-INF),
					 confidence_max( INF),
					 confidence_min_isset(false),
					 confidence_max_isset(false),
					 time_min(0),
					 time_max(0),
					 time_min_type(null),
					 time_max_type(null),
					 lessThan(false),
					 greaterThan(false),
					 mIsNgram(false),
					 mpClusters(NULL),
					 mType(EType_Word)
		{}

		~QueryKwd();
		
		void SetInvertedIndex(std::string idx) { mInvertedIndex = idx; }
		void SetPronList(std::list<gpt::PhnTrans::pt_entry> *pPronList, QueryKwdList *parentKwdList, Lexicon *pLexiconPhn);
		friend std::ostream& operator<<(std::ostream& os, const lse::QueryKwd& kwd);
		void SetType(EType t) { mType = t; }
		EType GetType() { return mType; }
		bool ContainsForm(ID_t wordId);
};


class PronVariant
{
	public:
		gpt::PhnTrans::pt_entry	mPtEntry;
		QueryKwdList*			mpQueryKwdListPhn;
		QueryKwdList*			mpQueryKwdListNgram;
//		SearchClusterGroups*	mpClusterGroupsNgram;

		friend std::ostream& operator<<(std::ostream& os, const lse::PronVariant& pron);
};

class InvIdxRecordsCluster;
class InvIdxRecordsClusters;
typedef std::vector<InvIdxRecordsCluster*> InvIdxRecordsClustersPList;


class OccurrenceWithSource : public LatIndexer::Occurrence
{
	public:
		SourceType::Enum mSourceType;
};


class OccurrenceWithSourceList : public LatIndexer::Occurrence
{
		LatIndexer::OccurrencePList mOccurrencePList;
		std::vector<SourceType::Enum> mSourceTypeList;
	public:
		void Insert(LatIndexer::Occurrence& occurrence, SourceType::Enum sourceType) {
			Insert(&occurrence, sourceType);
		}
		void Insert(LatIndexer::Occurrence* pOccurrence, SourceType::Enum sourceType);
		LatIndexer::Occurrence* GetOccurrence(int index) const { return mOccurrencePList[index]; }
		SourceType::Enum GetSourceType(int index) const { return mSourceTypeList[index]; }
		int size() const { return mOccurrencePList.size(); }
};


//================================================================================
//	InvIdxRecordsCluster
//================================================================================
// kwd.W_id -> InvIdxRecordList
class InvIdxRecordsCluster : public std::map<ID_t, OccurrenceWithSourceList>, public LatIndexer::Occurrence
{
	public:
		//ID_t					mMeetingID;
		//TLatViterbiLikelihood 	mConf; // min confidence of all clusters
		int 					mValidKeywordsCount;				
		SourceType::Enum 		mSourceType;
		PronVariant*			mpPronVariant;
		QueryKwd*				mpForm; ///< the QueryKwd::Form the cluster belongs to
		//LatTime					mStartTime;
		//LatTime					mEndTime;
		InvIdxRecordsClustersPList mFwdClusters;
		InvIdxRecordsClustersPList mBckClusters;
		InvIdxRecordsCluster* mpBestFwdCluster;
		bool mBestFwdClusterFound; 				// true, if there is some matching cluster in the following kwd

		InvIdxRecordsCluster() : LatIndexer::Occurrence(), mValidKeywordsCount(0), mSourceType(SourceType::Unknown), mpPronVariant(NULL), mpForm(NULL), mpBestFwdCluster(NULL), mBestFwdClusterFound(false)/*, mConf(-INF), mStartTime(INF), mEndTime(-INF)*/ {}
		InvIdxRecordsCluster(const LatIndexer::Occurrence& o) : LatIndexer::Occurrence(), mValidKeywordsCount(0), mSourceType(SourceType::Unknown), mpForm(NULL)
		{
			mMeetingID = o.mMeetingID;
			mStartTime = o.mStartTime;
			mEndTime = o.mEndTime;
			mConf = o.mConf;
		}

		friend std::ostream& operator<<(std::ostream& os, const InvIdxRecordsCluster& c);
		
		void Insert(ID_t W_id, LatIndexer::Occurrence* pOccurrence, SourceType::Enum sourceType);
		void Insert(const QueryKwd& kwd, LatIndexer::Occurrence* pOccurrence, SourceType::Enum sourceType) { Insert(kwd.W_id, pOccurrence, sourceType); }
		void Insert(const QueryKwd* kwd, LatIndexer::Occurrence* pOccurrence, SourceType::Enum sourceType) { Insert(*kwd, pOccurrence, sourceType); }
		bool ContainsCluster(const QueryKwd& kwd, LatIndexer::Occurrence* pCmpOccurrence);
		bool ContainsCluster(const QueryKwd* kwd, LatIndexer::Occurrence* pCmpOccurrence) { return ContainsCluster(*kwd, pCmpOccurrence); }
		LatTime GetStartTime() { return mStartTime; }
		LatTime GetEndTime()   { return mEndTime; }
		LatTime GetConfidence()   { return mConf; }

		void SetSourceType(SourceType::Enum type) { mSourceType = type; }
		SourceType::Enum GetSourceType() { return mSourceType; }

		void SetPronVariant(PronVariant* pPronVariant) { mpPronVariant = pPronVariant; }
		PronVariant* GetPronVariant() { return mpPronVariant; }

		void SetKwdForm(QueryKwd* pKwd) { mpForm = pKwd; }
		QueryKwd* GetKwdForm() { return mpForm; }
};

typedef InvIdxRecordsCluster* InvIdxRecordsCluster_Pointer;

//================================================================================
//	InvIdxRecordsClusters
//================================================================================
// list of InvIdxRecordsCluster items
class InvIdxRecordsClusters : public std::vector< InvIdxRecordsCluster > 
{
	public:
		InvIdxRecordsCluster_Pointer* mpSortedByTime; ///< array of pointers to InvIdxRecordsCluster sorted by time
		struct SortType
		{
			enum Enum
			{
				ByTime,
				ByConf,
				ByTimeAndConf
			};
		};
		//QueryKwdList::iterator iBestKwd; ///< the keyword with the least number of hits in the reverse index
		
		InvIdxRecordsClusters() : mpSortedByTime(NULL) {}
		~InvIdxRecordsClusters() { if(mpSortedByTime) delete[] mpSortedByTime; }
		static bool cmp_InvIdxRecordsCluster(const InvIdxRecordsCluster& lv, const InvIdxRecordsCluster& rv);
		static bool cmp_PRevIdxRecord(const QueryKwd::InvIdxRecord* lv, const QueryKwd::InvIdxRecord* rv);
		static bool cmp_InvIdxRecordsCluster_ByTime(const InvIdxRecordsCluster_Pointer& lv, const InvIdxRecordsCluster_Pointer& rv);
		void SortClusters(SortType::Enum sortType);
		InvIdxRecordsCluster* FindCluster(const LatIndexer::Occurrence &where);
		InvIdxRecordsCluster* FindCluster(const LatIndexer::Occurrence *pWhere) { return FindCluster(*pWhere); }
		void Print();
};

//================================================================================
//	InvIdxRecordsClustersCursor
//================================================================================
class InvIdxRecordsClustersCursor 
{
//				InvIdxRecordsClusters::iterator miCluster;
		int mClusterIdx;
		int mSize;
		InvIdxRecordsClusters* mpInvIdxRecordsClusters;
	public:
		InvIdxRecordsClustersCursor() : mClusterIdx(0) {}
		InvIdxRecordsClustersCursor(InvIdxRecordsClusters *pInvIdxRecordsClusters) : 
			mClusterIdx(0)
		{
			SetInvIdxRecordsClusters(pInvIdxRecordsClusters);
		}
		void SetInvIdxRecordsClusters(InvIdxRecordsClusters *pInvIdxRecordsClusters)
		{ 
			mpInvIdxRecordsClusters = pInvIdxRecordsClusters;
			mSize = mpInvIdxRecordsClusters->size();
		}
		void SetInvIdxRecordsClusters(InvIdxRecordsClusters &rInvIdxRecordsClusters)
		{
			SetInvIdxRecordsClusters(&rInvIdxRecordsClusters);
		}
		void Next()	{ mClusterIdx++; }
		//InvIdxRecordsCluster& Get() { return (*mpInvIdxRecordsClusters)[mClusterIdx]; }
		InvIdxRecordsCluster* Get() { return &((*mpInvIdxRecordsClusters)[mClusterIdx]); }
		InvIdxRecordsClusters* GetClustersList() { return mpInvIdxRecordsClusters; }
		bool Eol() { return mClusterIdx >= mSize; }
};

//================================================================================
//	SearchSource
//================================================================================
struct SearchSource
{
	public:
		enum Enum
		{
			Unknown,
			LVCSR,
			PHN,
			LVCSR_PHN,
			LVCSR_OOVPHN
		};
		static SearchSource::Enum ParseString(std::string str)
		{
			if(str == "LVCSR") {
				return LVCSR;
			} else if (str == "PHN") {
				return PHN;
			} else if (str == "LVCSR_PHN") {
				return LVCSR_PHN;
			} else if (str == "LVCSR_OOVPHN") {
				return LVCSR_OOVPHN;
			} else {
				return Unknown;
			}
		}
};

//================================================================================
//	QueryKwdList
//================================================================================
class QueryKwdList : public std::vector< QueryKwd > 
{
	public:
	private:
		Lexicon*		mpLexicon;
	public:
		std::string     query_str;
		unsigned int 	hyps_per_stream;
		float 			time_delta;
		int 			fwd_nodes_count;
		int 			bck_nodes_count;
		float 			phrase_kwd_neighborhood;
		int 			max_results;
		bool 			mExactMatch;
		bool 			mNgram2Phn;
		bool			mPhn2Ngram;
		int				mPhonemesCnt;
		int				mUniqSize;					///< unique keywords count
		bool 			mReturnAllSuccessfulTokens;
		bool			mOverlapping;
		bool			mDoclistOnly;
		float			mThreshold;
		bool			mExpandWordsToForms;
		std::string		mTermId;
		SearchSource::Enum mSearchSource;
		InvIdxRecordsClusters mInvIdxRecordsClusters;
		DocumentsList   mValidDocuments;
		
		QueryKwdList() :
	//		mpLexicon					( pLexicon ),
			query_str		 			( "" ),
			hyps_per_stream 			( DEFAULT_HYPS_PER_STREAM ),
			time_delta 					( DEFAULT_TIME_DELTA ),
			fwd_nodes_count 			( DEFAULT_FWD_NODES_COUNT ),
			bck_nodes_count 			( DEFAULT_BCK_NODES_COUNT ),
			phrase_kwd_neighborhood 	( DEFAULT_PHRASE_KWD_NEIGHBORHOOD ),
			max_results 				( DEFAULT_MAX_RESULTS ),
			mExactMatch					( false ),
			mNgram2Phn					( false ),
			mPhn2Ngram					( false ),
			mPhonemesCnt				( 0 ),
			mUniqSize					( 0 ),
			mReturnAllSuccessfulTokens	( false ),
			mOverlapping				( false ),
			mDoclistOnly				( false ),
			mThreshold					( -INF ),
			mExpandWordsToForms			( false ),
			mTermId						( "" ),
			mSearchSource				( SearchSource::LVCSR_OOVPHN )
		{}

		void CopyParams(QueryKwdList &rParent)
		{
			mpLexicon					= rParent.mpLexicon;
			
			query_str					= rParent.query_str;
			hyps_per_stream				= rParent.hyps_per_stream;
			time_delta					= rParent.time_delta;
			fwd_nodes_count				= rParent.fwd_nodes_count;
			bck_nodes_count				= rParent.bck_nodes_count;
			phrase_kwd_neighborhood		= rParent.phrase_kwd_neighborhood;
			max_results					= rParent.max_results;
			mExactMatch					= rParent.mExactMatch;
			mNgram2Phn					= rParent.mNgram2Phn;
			mPhn2Ngram					= rParent.mPhn2Ngram;
			mPhonemesCnt				= rParent.mPhonemesCnt;
			mUniqSize					= rParent.mUniqSize;
			mReturnAllSuccessfulTokens 	= rParent.mReturnAllSuccessfulTokens;
			mOverlapping			 	= rParent.mOverlapping;
			mDoclistOnly			 	= rParent.mDoclistOnly;
			mThreshold				 	= rParent.mThreshold;
			mExpandWordsToForms		 	= rParent.mExpandWordsToForms;
			mTermId						= rParent.mTermId;
			mSearchSource				= rParent.mSearchSource;
			for (DocumentsList::iterator i=rParent.mValidDocuments.begin(); i!=rParent.mValidDocuments.end(); i++)
				mValidDocuments.insert(std::make_pair(i->first, i->second));
		}
		void CopyParams(QueryKwdList* pParent) { CopyParams(*pParent); }
			
		
		bool contains(ID_t W_id);
		bool contains(std::string W);
		QueryKwdList::iterator find(std::string W);
		int getKwdIndex(ID_t W_id);
		int getKwdIndex(QueryKwdList::const_iterator iKwd);
		void print();
		void push_back(QueryKwd &rQueryKwd);

		/**
		 * @brief Add all words from the given query
		 *
		 * @param pQueryKwdList_From Source query
		 */
		void AddWords(QueryKwdList* pQueryKwdList_From);

		/**
		 * @brief Create a net of words' occurences (occurences creating a phrase which occure close together will be connected) 
		 */
		void InvertedIndexLookupResultsClustering();
		void QueryKwdClustersClustering();

		void PrintInvertedIndexLookupResultsClusters();

		/**
		 * @brief Get the count of unique keywords in query
		 *
		 * @return unique keywords count
		 */
		int GetUniqSize() { return mUniqSize; }

		QueryKwdList::iterator GetLeastInvIdxRecordsKwd();
		QueryKwdList::iterator GetLeastQueryClustersKwd();

		friend std::ostream& operator<<(std::ostream& os, const lse::QueryKwdList& kwdlist);

	public:
		void FillInvIdxRecordsClusters(SourceType::Enum sourceType);
		void SortInvIdxRecordsClusters(InvIdxRecordsClusters::SortType::Enum sortType);
		void PrintInvIdxRecordsClusters();
	private:
		void FillInvIdxRecordsClustersFwd_recursive(
				InvIdxRecordsCluster *group, 
				QueryKwd::InvIdxRecord* curInvIdxRecord, 
				QueryKwdList::iterator curKwd,
				SourceType::Enum sourceType);

		void FillInvIdxRecordsClustersBck_recursive(
				InvIdxRecordsCluster *group, 
				QueryKwd::InvIdxRecord* curInvIdxRecord, 
				QueryKwdList::iterator curKwd,
				SourceType::Enum sourceType);

	public:
		void FillQueryClusters();
	private:
		void FillQueryClustersFwd_recursive(
				InvIdxRecordsCluster* pParentCluster, 
				InvIdxRecordsCluster* pCurCluster, 
				QueryKwdList::iterator curKwd);

		void FillQueryClustersBck_recursive(
				InvIdxRecordsCluster* pParentCluster, 
				InvIdxRecordsCluster* pCurCluster, 
				QueryKwdList::iterator curKwd);
};


} // namespace lse

#endif
