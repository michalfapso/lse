#include "nlp.h"
#include <cstdlib>
#include <fstream>

using namespace std;

void Nlp::RegexReplaceList::LoadFromFile(const char* filename)
{
	ifstream in(filename);
	if (!in.good())
	{
		CERR("ERROR: file not found ("<<filename<<")");
		EXIT();
	}

	Nlp::RegexReplace rep;
	try
	{
		while(!in.eof())
		{
			in >> rep.mSearch;
			if (in.eof())
				break;
			in >> rep.mReplace;
			DBG(rep.mSearch<<" -> "<<rep.mReplace);
			push_back(rep);
		}
	}
	catch (exception &e)
	{
		CERR("ERROR: loading the word2forms_regexlist ("<<filename<<"): "<<e.what());
		EXIT();
	}	
}

void Nlp::ExpandByRegexList(std::string what, const RegexReplaceList& rRegexList, WordList* pResultsList)
{
#ifdef USE_REGEX
	DBG("Nlp::ExpandByRegexList('"<<what<<"')");
	int line=1;
	for (RegexReplaceList::const_iterator i=rRegexList.begin(); i!=rRegexList.end(); i++)
	{
		std::ostringstream t(std::ios::out | std::ios::binary);
		std::ostream_iterator<char> oi(t);
		boost::regex_replace(oi, what.begin(), what.end(), boost::regex(i->mSearch), i->mReplace, boost::match_default | boost::format_no_copy);
		if (t.str().length() > 0)
		{
			DBG_FORCE("new_str @line "<<line<<":"<<t.str());
			pResultsList->push_back(t.str());
		}
		line++;
	}
	DBG("Nlp::ExpandByRegexList()...done");
#else
#warning USE_REGEX is disabled!
	CERR("ERROR: the application was compiled without the boost::regex");
	EXIT();
#endif
}

