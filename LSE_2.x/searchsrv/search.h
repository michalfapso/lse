#ifndef SEARCH_H
#define SEARCH_H

#include <vector>
#include <list>
#include <map>
#include "lattypes.h"
#include "latbinfile.h"
#include "latindexer.h"
#include "querykwdlist.h"
#include "tokenlattice.h"
#include "hypothesis.h"
//#include "InvIdxRecordsClusters.h"
#include "results.h"
#include "searchconfig.h"

namespace lse {

//#define DBG_SEARCH(str) DBG_FORCE(str)
#define DBG_SEARCH(str)
	
//========================================================================================
// Search
//========================================================================================
class Search {
	public:

	private:
		LatBinFile*	binlat;
		Lexicon				*mpLexiconLvcsr;
		Lexicon				*mpLexiconPhn;
		Lattice::Indexer 	*mpIndexerLvcsr;
		Lattice::Indexer 	*mpIndexerPhn;

		float *nodesScore;

		int mRecordChannelColumn;
		int mRecordNameCutoffColumn;
		char mRecordDelim;
	public:

		QueryKwdList *mpQueryKwdList;

		Search(QueryKwdList *p_queryKwdList) : 
			binlat(NULL),
			mpLexiconLvcsr(NULL),
			mpLexiconPhn(NULL),
			mpIndexerLvcsr(NULL),
			mpIndexerPhn(NULL),
			nodesScore(NULL), 
			mRecordChannelColumn(0),
			mRecordNameCutoffColumn(0),
			mRecordDelim('_')
		{
			mpQueryKwdList = p_queryKwdList;
		};

		void SetRecordChannelColumn(int col) { mRecordChannelColumn = col; }
		void SetRecordNameCutoffColumn(int col) { mRecordNameCutoffColumn = col; }
		void SetRecordDelim(char delim) { mRecordDelim = delim; }
		void SetBinlat(LatBinFile &p_binlat) { this->binlat = &p_binlat; }
		void SetQueryKwdList(QueryKwdList* pQueryKwdList) { this->mpQueryKwdList = pQueryKwdList; }

		void SetLexiconPhn(Lexicon *lexiconPhn) 				{ mpLexiconPhn = lexiconPhn; }
		void SetLexiconLvcsr(Lexicon *lexiconLvcsr) 			{ mpLexiconLvcsr = lexiconLvcsr; }

		void SetIndexerPhn(Lattice::Indexer *indexerPhn) 		{ mpIndexerPhn = indexerPhn; }
		void SetIndexerLvcsr(Lattice::Indexer *indexerLvcsr) 	{ mpIndexerLvcsr = indexerLvcsr; }



		void InvertedIndexLookup_LvcsrPhn(QueryKwdList* pQueryKwdList, std::string filenameSearchindexLvcsr, std::string filenameSearchindexPhn, unsigned int pMaxCount, float pOccurrenceThreshold);

		void CombineInvertedIndexLookupResults_LvcsrPhn(QueryKwdList* pQueryKwdList, unsigned int pMaxCount);

		void InvertedIndexLookupResultsClustering_LvcsrPhn(QueryKwdList* pQueryKwdList);
		void InvertedIndexLookupResultsClustering_Phn(QueryKwdList* pQueryKwdList);

		void PrintInvertedIndexLookupResultsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList);

		void FillInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList);
		void FillInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList);

		void SortInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList);
		void SortInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList);

		void PrintInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList);
		void PrintInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList);

		bool RescoreHits(Lexicon *pLexicon, InvIdxRecordsCluster* pCluster, QueryKwdList* pQueryKwdList, LatMeetingIndexerRecord meetingRec, SearchConfig* pConfig, Hypothesis &hypSettings, Results *results);

		void GetNodesContext(Lexicon *pLexicon, ID_t nodeID, DirectionType dir, Hypothesis *hyp, QueryKwdList* pQueryKwdList);
		void ExpandPhonemes(const QueryKwdList* src, QueryKwdList* dest);
		void NgramsCluster2PhnCluster(InvIdxRecordsCluster* pClusterNgrams, InvIdxRecordsCluster* pClusterPhonemes, Lexicon* pLexicon);
		void PrintCandidates(InvIdxRecordsCluster* pGroup, QueryKwdList* pQueryKwdList, LatMeetingIndexerRecord &rMeetingRec);
		void PrintCandidateTrigrams(Lexicon *pLexicon, InvIdxRecordsCluster* pGroup, QueryKwdList* pQueryKwdList, LatMeetingIndexerRecord &rMeetingRec);
		void PrintMatchedKeywords(QueryKwdList* pQueryKwdList);

		std::string GetMeetingChannelFromMeetingName(const std::string &meetingName, SearchConfig* pConfig);
		std::string CutoffMeetingName(const std::string &meetingName, SearchConfig* pConfig);

		void InvIdxRecordsClusters2Results(
				SearchConfig *pConfig,
				QueryKwdList *pQueryKwdList,
				Results *pResults);

		void VerifyCandidates(
				SearchConfig *pConfig,
				QueryKwdList *pQueryKwdList,
				Results *pResults);

	private:
		/*
		void VerifyCandidates_Singleword(
				SearchConfig *pConfig,
				QueryKwdList *pQueryKwdList,
				Results *pResults);
		*/
		bool VerifyCandidates_SubQuery_SelectSource(
				SearchConfig* pConfig, 
				QueryKwdList* pQueryKwdList, 
				QueryKwdList::iterator iKwdBegin,
				QueryKwdList::iterator iKwdEnd,
				InvIdxRecordsCluster* pCluster,
				SourceType::Enum sourceType,
				Results* pResults);

		bool VerifyCandidates_SingleSource(
				SearchConfig* pConfig,
				QueryKwdList* pQueryKwdList,
				InvIdxRecordsCluster* pCluster,
				Lattice::Indexer* pIndexer,
				Lexicon* pLexicon,
				std::string* pDataDirPath,
				Results* pResults,
				SourceType::Enum sourceType);

		void VerifyCandidates_HybridSources(
				SearchConfig* pConfig,
				QueryKwdList* pQueryKwdList,
				InvIdxRecordsCluster* pCluster,
				std::vector<LocalResults*>* pLocalResultsPList,
				Results* pGlobalResults);
	private:

		QueryKwdList::iterator GetLeastInvIdxRecordsKwd();

		enum EFrontBack {
			front,
			back,
		};

		struct HistRecord {
			ID_t 		W_id;
			std::string	W;
		};

		typedef std::list<HistRecord> HistContainer;
	
		void addToHypothesis(ID_t curNodeID, LatTime tStart, float conf, Hypothesis *hyp, Lexicon *pLexicon, QueryKwdList* pQueryKwdList, EFrontBack frontback);
		void AddPathToHypothesis(WLR* pBestPath, Hypothesis* pHyp, ID_t* pFirstNodeID, Lexicon *pLexicon, QueryKwdList* pQueryKwdList);

}; // class Search


} // namespace lse

#endif

