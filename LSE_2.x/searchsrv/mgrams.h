#ifndef MGRAM_H
#define MGRAM_H

// /mnt/matylda6/szoke/THESE/EXPERIMENTS/LATT2MGRAM/MGRAMS
// segment_DICT_using_multigrams_phn.pl

#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <list>
#include <string>

#include "lattypes.h"

class Mgrams
{
	public:
		static const float Prob0 = -10e10;
		static const float ProbNotExists = -20;

		typedef std::list<std::string> MgramString;
		typedef float Score;

		// class PhonemeString {{{
		class PhonemeString : public std::list<std::string>
		{
			public:
				friend std::ostream& operator<<(std::ostream& os, const PhonemeString& l) {
					bool first = true;
					for (const_iterator i = l.begin(); i != l.end(); i++)
					{
						if (first) 
							first = false; 
						else 
							os << "_";
						os << *i;
					}
					return os;
				}

				std::string ToString(int from, int to, char delimiter='_') const {
					std::string res;
					int c=1;
					bool first = true;
					for (const_iterator i=begin(); i!=end(); i++)
					{
						if (from <= c && c <= to)
						{
							if (first) 
								first = false; 
							else 
								res += delimiter;
							res += *i;
						}
						c++;
					}
					return res;
				}

				std::string ToString(char delimiter='_') const {
					std::string res;
					bool first = true;
					for (const_iterator i=begin(); i!=end(); i++)
					{
						if (first) 
							first = false; 
						else 
							res += delimiter;
						res += *i;
					}
					return res;
				}
		};
		// }}}

		class MgramStringList : public std::list<PhonemeString>
		{
			public:
				friend std::ostream& operator<<(std::ostream& os, const MgramStringList& l) {
					bool first = true;
					for (const_iterator i = l.begin(); i != l.end(); i++)
					{
						if (first) 
							first = false; 
						else 
							os << ", ";
						os << *i;
					}
					return os;
				}
		};

		// class StatRecord {{{
		class StatRecord
		{
			public:
				friend std::ostream& operator<<(std::ostream& os, const StatRecord& r) {
					return os << r.mLabel << " " << r.mLogLik << " " << r.mOccurrences;
				}
				PhonemeString mLabel;
				Score mLogLik;
				float mOccurrences;
		};
		// }}}

		// class Statistics {{{
		class Statistics
		{
				typedef std::map<std::string, StatRecord*> Container;
				Container mContainer;
			public:
				friend std::ostream& operator<<(std::ostream& os, const Statistics& s) {
					for (Container::const_iterator i = s.mContainer.begin(); i != s.mContainer.end(); i++)
					{
						os << *i->second << std::endl;
					}
					return os;
				}
				void Add(StatRecord* pRec);
				StatRecord* GetRecord(const std::string phnStr);
				StatRecord* GetRecord(const PhonemeString& phnStr) { return GetRecord(phnStr.ToString()); }
		};
		// }}}

		Statistics mStatistics;

		void LoadStatistics(const char* filename);
		void PhonemeStringToMgrams(const PhonemeString &phonemeString, unsigned int maxMgramLength, MgramStringList* pMgramStringList);

	private:
		Score PSeq(int from, int to, const PhonemeString& seq);
};

#endif
