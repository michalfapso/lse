#ifndef HSV2RGB_H
#define HSV2RGB_H

void convert_hsv_to_rgb(float hue, float s, float v, int *r, int *g, int *b)
{
	float p1, p2, p3, i, f;
	float xh;
	float nr=0, ng=0, nb=0;
//	extern float hue,  s,  v;  /* hue (0.0 to 360.0, is circular, 0=360)
//								  s and v are from 0.0 - 1.0) */
//	extern long  r2,  g2,  b2; /* values from 0 to 63 */

	if (hue == 360.0)
		hue = 0.0;           /* (THIS LOOKS BACKWARDS)       */

	xh = hue / 60.;                   /* convert hue to be in 0,6       */
	i = (float)floor((double)xh);    /* i = greatest integer <= h    */
	f = xh - i;                     /* f = fractional part of h     */
	p1 = v * (1 - s);
	p2 = v * (1 - (s * f));
	p3 = v * (1 - (s * (1 - f)));

	switch ((int) i)
	{
		case 0:
			nr = v;
			ng = p3;
			nb = p1;
			break;
		case 1:
			nr = p2;
			ng = v;
			nb = p1;
			break;
		case 2:
			nr = p1;
			ng = v;
			nb = p3;
			break;
		case 3:
			nr = p1;
			ng = p2;
			nb = v;
			break;
		case 4:
			nr = p3;
			ng = p1;
			nb = v;
			break;
		case 5:
			nr = v;
			ng = p1;
			nb = p2;
			break;
	}

	*r = (int)(nr * 255.); /* Normalize the values to 63 */
	*g = (int)(ng * 255.);
	*b = (int)(nb * 255.);
	return;
}

#endif

