#include "hypothesis.h"

using namespace std;
using namespace lse;


bool lse::operator<(const Hypothesis& l, const Hypothesis& r) 
{
//	    return (l.stream < r.stream) && (l.record < r.record);
	    if (l.keywordsCount == r.keywordsCount)
		{
			return (l.score > r.score);
		}
		else
		{
			return (l.keywordsCount > r.keywordsCount);
		}
}

void Hypothesis::updateTimeBoundaries(float startTime, float endTime)
{
	if (mStartTime > startTime)
		mStartTime = startTime;
	if (mEndTime < endTime)
		mEndTime = endTime;
}

void Hypothesis::push_back_word(const string& str, LatTime start, LatTime end, Confidence conf) {
	
	Hypothesis::Word newHypothesisWord;
	newHypothesisWord.str = str;
	newHypothesisWord.start = start;
	newHypothesisWord.end = end;
	newHypothesisWord.conf = conf;
	words.push_back(newHypothesisWord);
	updateTimeBoundaries(start, end);
}

void Hypothesis::push_back_word(Hypothesis::Word w) {
	words.push_back(w);
}



void Hypothesis::push_front_word(const string& str, LatTime start, LatTime end, Confidence conf) {
	
	Hypothesis::Word newHypothesisWord;
	newHypothesisWord.str = str;
	newHypothesisWord.start = start;
	newHypothesisWord.end = end;
	newHypothesisWord.conf = conf;
	words.push_front(newHypothesisWord);
	updateTimeBoundaries(start, end);
}

void Hypothesis::push_front_word(Hypothesis::Word w) {
	words.push_front(w);
	updateTimeBoundaries(w.start, w.end);
}


void Hypothesis::addKeyword(const string& str, LatTime start, LatTime end, Confidence conf) 
{
	Hypothesis::Word newKwd;
	newKwd.str = str;
	newKwd.start = start;
	newKwd.end = end;
	newKwd.conf = conf;
//	score = score == 0 ? conf : logAdd(conf,score);
	addKeyword(newKwd);
}

void Hypothesis::addKeyword(Hypothesis::Word w) 
{
	updateKeywordCounter(w.str);
	updateTimeBoundaries(w.start, w.end);

	if (keywords.size() > 0 && w < *keywords.begin())
	{
		//DBG("keywords.push_front(): "<<w);
		keywords.push_front(w);
	}
	else
	{
		//DBG("keywords.push_back(): "<<w);
		keywords.push_back(w);
	}
}

void Hypothesis::addKeywordBack(Hypothesis::Word w) {
	updateKeywordCounter(w.str);
	updateTimeBoundaries(w.start, w.end);
	keywords.push_back(w);
}

void Hypothesis::addKeywordFront(Hypothesis::Word w) {
	updateKeywordCounter(w.str);
	updateTimeBoundaries(w.start, w.end);
	keywords.push_front(w);
}

void Hypothesis::updateKeywordCounter(const string& str)
{
	bool found = false;
	for (Keywords::iterator iKwd=keywords.begin(); iKwd!=keywords.end(); ++iKwd)
	{
		if (iKwd->str == str)
		{
			found = true;
			break;
		}
	}
	
	if (!found) 
	{
		keywordsCount++;
	}
}
	
void Hypothesis::setFrontWordStartTime(LatTime t) {
	DBG("setFronWordStartTime(" << t << ")");
	if (words.size() > 0)
		words.front().start = t;
}

void Hypothesis::printXml(ostream *os)
{
		*os << "\t<keywords count=\"" << this->keywordsCount << "\">" << endl;
		for (Keywords::const_iterator i=this->keywords.begin(); i!=this->keywords.end(); ++i) {
			*os << "\t\t<kwd start=\""<< i->start <<"\" end=\""<< i->end <<"\" score=\""<< i->conf <<"\">"<< i->str 
				<< "</kwd>" << endl;
		}
		*os << "\t</keywords>" << endl;
		
		*os << "\t<words>" << endl;
		for (Words::const_iterator i=this->words.begin(); i!=this->words.end(); ++i) {
			*os << "\t\t<kwd start=\""<< i->start <<"\" end=\""<< i->end <<"\" score=\""<< i->conf <<"\">"<< i->str 
				<< "</kwd>" << endl;
		}
		*os << "\t</words>" << endl;
}

void Hypothesis::printText(ostream *os)
{
	for (Keywords::const_iterator i=this->keywords.begin(); i!=this->keywords.end(); ++i) {
		*os << i->str << " ";
	}
}

