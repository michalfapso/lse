#include "wordmapper.h"
#include <fstream>
#include <sstream>

using namespace std;
using namespace lse;

WordMapper::~WordMapper()
{
	for (Map::iterator i=mMap.begin(); i!=mMap.end(); i++) {
		delete i->second;
	}

	typedef map<IdList*, bool> ClusterReleased;
	ClusterReleased cluster_released;
	for (Clusters::iterator i=mClusters.begin(); i!=mClusters.end(); i++) 
	{
		ClusterReleased::iterator iRel = cluster_released.find(i->second);
		if (iRel == cluster_released.end())
		{
			delete i->second;
			cluster_released.insert(make_pair(i->second, true));
		}
	}
}

bool WordMapper::LoadMappingFromFile(std::string filename)
{
	ifstream in(filename.c_str());
	if (!in.good())
	{
		CERR("ERROR: file not found ("<<filename<<")");
		return false;
	}

	// each line in the input file should look like:
	// WORD_TO WORD_FROM
	// ie:
	// �VEJKA�KA ZVEJKACKA
	string w_to;
	string w_from;
	try
	{
		while(!in.eof())
		{
			in >> w_to;
			if (in.eof())
				break;
			in >> w_from;

			ID_t wid = mpLexicon->word2id_readonly(w_to, 0);
			if (wid == -1)
			{
				CERR("ERROR: word '"<<w_to<<"' is not in the lexicon. All words in the first column in the word mapping list '"<<filename<<"' have to be in the lexicon.");
				EXIT();
			}
			IdList* pidlist = NULL;
			Map::iterator it = mMap.find(w_from);
			if (it != mMap.end()) {
				pidlist = it->second; 
			} else {
				pidlist = new IdList;
				mMap.insert(make_pair(w_from, pidlist));
			}
			pidlist->push_back(wid);
		}
	}
	catch (exception &e)
	{
		CERR("ERROR: loading mappings from file "<<filename<<": "<<e.what());
		return false;
	}
	return true;
}

bool WordMapper::LoadClustersFromFile(std::string filename)
{
	ifstream in(filename.c_str());
	if (!in.good())
	{
		CERR("ERROR: file not found ("<<filename<<")");
		return false;
	}

	// each line in the input file should contain words fitting to one cluster
	// ie:
	// �VEJKA�KA �VEJKA�KOU �VEJKA�KY �VEJKA�KOV�
	string w;
	try
	{
		while(!in.eof())
		{
			char line[2048];
			in.getline(line, 2048);
			istringstream iss(line);

			IdList* pidlist = new IdList;
//			DBG("-------cluster--------");
			while(!iss.eof())
			{
				iss >> w;
				assert(w != "");
				ID_t wid = mpLexicon->word2id_readonly(w, 0);
				if (wid == -1) {
#warning THIS SHOULD CAUSE AN ERROR AND EXIT ...NOW ALLOWED ONLY FOR DEVELOPMENT PURPOSES
					CERR("ERROR: word '"<<w<<"' was not found in the lexicon. All words in the clusters list '"<<filename<<"' have to be in lexicon.");
					continue;
//					EXIT();
				}
				mClusters.insert(make_pair(wid, pidlist));
				pidlist->push_back(wid);
//				DBG("w: "<<w<<" id="<<wid);
			}
		}
	}
	catch (exception &e)
	{
		CERR("ERROR: loading clusters from file "<<filename<<": "<<e.what());
		return false;
	}
	return true;
}


int WordMapper::GetWordMappingIdList(const std::string& word, WordMapper::IdList* pidlist)
{
	int s = pidlist->size();
	// mappings first
	Map::iterator it = mMap.find(word);
	if (it == mMap.end())
	{
		DBG("No mapping found");
		ID_t wid = mpLexicon->word2id_readonly(word, 0);
		if (wid == -1) {
			return 0;
		}
		// if there is no mapping for the word, try to find it in clusters
		if (AddClusterIdList(wid, pidlist) == 0) {
			DBG("pidlist->push_back():"<<wid);
			pidlist->push_back(wid);
		}
	}
	else
	{
		// for all words to which the given word is mapped
		for (IdList::iterator i=it->second->begin(); i!=it->second->end(); i++)
		{
			DBG("word '"<<word<<"' mapped to: "<<*i);
			if (AddClusterIdList(*i, pidlist) == 0) {
				DBG("pidlist->push_back():"<<*i);
				pidlist->push_back_if_not_there(*i);
			}
		}
	}
	return pidlist->size() - s;
}

int WordMapper::AddClusterIdList(ID_t wid, WordMapper::IdList* pidlist)
{
	DBG("Looking for cluster for word "<<wid);
	int s = pidlist->size();
	// find the word in clusters
	Clusters::iterator it = mClusters.find(wid);
	if (it == mClusters.end()) {
		// if there is no cluster for this word in mClusters
		DBG("no cluster found");
		return 0;
	} else {
		// if there is a cluster, add all words from that cluster
		DBG("Cluster for word "<<wid<<" found. Size:"<<it->second->size());
		for (IdList::iterator ii=it->second->begin(); ii!=it->second->end(); ii++) {
			DBG("Cluster found: pidlist->push_back():"<<*ii);
			pidlist->push_back_if_not_there(*ii);
		}
	}
	return pidlist->size() - s;
}
