/*********************************************************************************
 *
 *	Application for converting HTK Standard Lattice File (SLF) to binary file
 *
 *
 *
 *
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include "latbinfile.h"
#include "latmlffile.h"
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
//#include "hypothesis.h"
#include "lexicon.h"
#include "confmeasure.h"

#include "fea_writer.h"

using namespace std;
using namespace lse;

static const string ext_fea = ".fea";
static const string ext_htk = ".lat";

Timer dbgTimer;

//--------------------------------------------------------------------------------

#ifndef DBG
	#define DBG(str) DBG_FORCE(str)
#endif
//#define DBG(str)

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cerr << "Application takes filenames as parameters.";
		return 1;
	}

	Lexicon lexicon(Lexicon::readwrite); // stores records of type: wordID -> word  and  word -> wordID

	//application parameters
	char* p_fea_out = NULL;
	char* p_orig_fea_in = NULL;
	char* p_word2row_mapping_in = NULL;
	bool  p_sort_lattice = false;

	float p_wi_penalty = 0.0;
	float p_lmscale = 1.0;
	float p_amscale = 1.0;

	char *p_lat_out = NULL;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-fea-out") == 0) {
			j++;
			p_fea_out = argv[j];
			
		} else if (strcmp(argv[j], "-orig-fea-in") == 0) {
			j++;
			p_orig_fea_in = argv[j];
			
		} else if (strcmp(argv[j], "-word2row-mapping") == 0) {
			j++;
			p_word2row_mapping_in = argv[j];
			
		} else if (strcmp(argv[j], "-sort-lattice") == 0) {
			p_sort_lattice = true;
			
		} else if (strcmp(argv[j], "-wi-penalty") == 0) {
			j++;
			p_wi_penalty = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-lmscale") == 0) {
			j++;
			p_lmscale = atof(argv[j]);
			DBG_FORCE("lmscale: "<<p_lmscale);

		} else if (strcmp(argv[j], "-amscale") == 0) {
			j++;
			p_amscale = atof(argv[j]);
			DBG_FORCE("amscale: "<<p_amscale);

		} else if (strcmp(argv[j], "-lat-out") == 0) {
			j++;
			p_lat_out = argv[j];
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		j++;
	}

	if (j >= argc) {
		cerr << "Application takes filenames as parameters.";
		exit(1);
	}
	string filename = argv[j];

	DBG_FORCE("Processing file: "<<filename);
	if (!file_exists(filename.c_str()))
	{
		CERR("ERROR: file does not exist: "<<filename);
		EXIT();
	}
	
	Lattice lat(&lexicon);
	lat.amscale = p_amscale;
	lat.lmscale = p_lmscale;

	//==================================================
	// READ HTK LATTICE
	//==================================================
	DBG( "reading...");
	if(lat.loadFromHTKFile(filename) != 0) {
		CERR("Error occured while reading lattice file " << filename);
		exit (1);
	}
	DBG( "done\t");

	//==================================================
	// CHECK WHETHER THE LATTICE IS SORTED TOPOLOGICALY
	//==================================================
	bool sorted = true;
	for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
		if ((*l)->S > (*l)->E) {
			sorted = false;
			break;
		}
	}
	if (!sorted)
	{
		if (!p_sort_lattice) CERR("WARNING: Lattice '"<<filename<<"' is not sorted! ...sorting lattice");
		lat.sortLattice();
		if (!p_sort_lattice) CERR("sorting lattice...done");
	}

	//==================================================
	// COMPUTE LINKS LIKELIHOOD (POSTERIORS)
	//==================================================
	/*
	DBG("compute links likelihood...");
	LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
	vit.computeFwViterbi();
	vit.computeBwViterbi();
	vit.computeLinksLikelihood();
	if (p_fwbw_out) 
		vit.writeViterbiFwBw(p_fwbw_out);

	cout << "total_likelihood: " << lat.nodes[0]->beta << endl;

	//for (Lattice::Links::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
	//	cout << (*l)->S << " -> " << (*l)->E << " = " << (*l)->confidence << endl;
	//}
	DBG("done   ");
	*/

	//==================================================
	// HTK LATTICE OUTPUT
	//==================================================
	if (p_lat_out) {
		lat.saveToHTKFile(p_lat_out, true); // COPY OF LATTICE
	}

	//==================================================
	// FEA OUTPUT
	//==================================================
	if (p_fea_out) {
		DBG("fea-export to "<<p_fea_out<<" ...");
		FeaWriter feawr(&lat);
		feawr.LoadWordToRowMapping(p_word2row_mapping_in);
		feawr.Process();
		if (p_orig_fea_in) {
			feawr.ReplaceByOrigFeatures(p_orig_fea_in);
		}
		feawr.Output(p_fea_out);
		DBG("done\t");
	}
	
	DBG( endl << "***** OPERATION COMPLETED *****" << endl);
	return (0);
}


