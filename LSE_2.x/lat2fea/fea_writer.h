#ifndef FST_WRITER_H
#define FST_WRITER_H

#include <map>
#include <sstream>
#include <boost/unordered_map.hpp>
#include "lat.h"

namespace lse {
class FeaWriter {
	public:
		FeaWriter(Lattice *pLat) : 
			mpLat(pLat),
			mpWord2Row(NULL),
			mpMat(NULL)
		{ }
		void LoadWordToRowMapping(const char* filename);
		void Process();
		void Output(const char* filename);
		void ReplaceByOrigFeatures(const char* origFeaturesFilenameIn);
	private:
		Lattice *mpLat;
		typedef boost::unordered_map<std::string, int> Word2Row;
		Word2Row *mpWord2Row;
		Mat<float> *mpMat; // feature matrix
};
}

#endif
