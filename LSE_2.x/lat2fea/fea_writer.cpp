#include <cmath>
#include "fea_writer.h"
#include "latviterbifwbw.h"

using namespace std;
using namespace lse;

void FeaWriter::Process() 
{

	delete mpMat;
	assert(mpLat);
	assert(mpWord2Row);
	DBG("Creating matrix "<<(int)ceil(mpLat->lastNode()->t*100)<<"x"<<mpWord2Row->size());
	mpMat = new Mat<float>( (int)ceil(mpLat->lastNode()->t*100), mpWord2Row->size(), -INF );
	DBG("mpMat->rows():"<<mpMat->rows());

	float p_wi_penalty = 0.0;
	float p_lmscale = 1.0;
	float p_amscale = 1.0;
	float p_amscale_kwd = 1.0;
	float p_lmscale_kwd = 1.0;
	float p_posterior_scale = 1.0f;
	bool  p_viterbi_baumwelsh = false;
	float p_logAdd_treshold = 7;
	LatViterbiFwBw vit(mpLat, p_wi_penalty, p_amscale, p_lmscale, p_posterior_scale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
	vit.computeFwViterbi();
	vit.computeBwViterbi();
	vit.computeLinksLikelihood();

	for (Lattice::Links::iterator iLink=mpLat->links.begin(); iLink!=mpLat->links.end(); ++iLink) {
		// int color = (int)(exp((i->second).bestLikelihood) * 255);
		// FST: main graph - NODES
		Lattice::Link* l = *iLink;

		const string& w = mpLat->nodes[l->E]->W;
		if (w == "!NULL") {
			// nothing to be done for !NULL links - they carry no words
			continue;
		}

		// Maybe instead of likelihood, a posterior probability should be computed from fwbw likelihoods
		int start_frame = static_cast<int>(round(mpLat->nodes[l->S]->t*100));
		int end_frame   = static_cast<int>(round(mpLat->nodes[l->E]->t*100));
		//DBG("start_frame:"<<start_frame<<" end_frame:"<<end_frame);
		#define FEA_USE_PROBABILITY_INSTEAD_OF_LIKELIHOOD
		#ifdef FEA_USE_PROBABILITY_INSTEAD_OF_LIKELIHOOD
		float log_prob = vit.getLinkConfidence(l->id, false);
		#else
		int link_length = end_frame - start_frame;
		float lik = l->a * mpLat->amscale + l->l * mpLat->lmscale;
		float lik_per_frame = lik / link_length;
		#endif

		Word2Row::const_iterator it = mpWord2Row->find(w);
		if (it == mpWord2Row->end()) {
			throw runtime_error("Word '"+w+"' was not found in the Word2Row map");
		}
		int word_position = it->second;


//		if (w == "er__2") {
//			DBG(mpLat->nodes[l->S]->t<<" .. "<<mpLat->nodes[l->E]->t<<" -> "<<start_frame<<" .. "<<end_frame);
//			DBG(l->S<<"->"<<l->E<<" lik:"<<lik<<" W:"<<w<<" row:"<<word_position<<" length:"<<link_length<<" lik_per_frame:"<<lik_per_frame);
//		}

		for (int i=start_frame; i<end_frame; i++) {
			#ifdef FEA_USE_PROBABILITY_INSTEAD_OF_LIKELIHOOD
			float new_lik = logAdd(log_prob, mpMat->get(i, word_position));
			#else
			float new_lik = logAdd(lik_per_frame, mpMat->get(i, word_position));
			#endif
			//if (w == "er__2" && new_lik > 0) {
			//if (i==10052 && w == "er__2") {
			//	DBG("frame:"<<i<<" logadd "<<mpMat->get(i, word_position)<<" "<<lik_per_frame<<" = "<<new_lik);
			//}
			mpMat->set(i, word_position, new_lik); // logadd the likelihoods
		}
	}
}

void FeaWriter::Output(const char* filename) 
{
	assert(mpMat);
	mpMat->saveHTK(filename);
}

void FeaWriter::ReplaceByOrigFeatures(const char* origFeaturesFilenameIn)
{
	assert(mpMat);
	Mat<float> mat_orig;
	mat_orig.loadHTK(origFeaturesFilenameIn);
	// Allow for at most 3 frames difference between mpMat and mat_orig
	if (abs(mpMat->rows() - mat_orig.rows()) > 3 || mpMat->columns() != mat_orig.columns()) {
		cerr << "mpMat->rows() != mat_orig.rows() || mpMat->columns() != mat_orig.columns() ... " << mpMat->rows() << " != " << mat_orig.rows() << " || "<< mpMat->columns() << " != " << mat_orig.columns() << endl;
		exit(1);
	}
	for (int r=0; r<mpMat->rows(); r++) {
		for (int c=0; c<mpMat->columns(); c++) {
			if (mpMat->get(r,c) > -10000) {
				if (r < mat_orig.rows()) {
					float orig_val = mat_orig.get(r,c);
					if (orig_val > 0) {
						CERR("ERROR: Log-posteriors are expected in the original feature matrix! (value="<<orig_val<<")");
						EXIT();
					}
					mpMat->set(r,c,orig_val);
				} else {
					mpMat->set(r,c,-INF);
				}
			}
		}
	}
}
 
void FeaWriter::LoadWordToRowMapping(const char* filename)
{
	delete mpWord2Row;
	mpWord2Row = new Word2Row();

	ifstream f(filename);
	if (!f.good()) {
		throw runtime_error("Can not open file '"+string(filename)+"' for reading.");
	}

	int i = 0;
	while (!f.eof()) {
		string word;
		f >> word;
		if (word.empty()) { continue; }
		(*mpWord2Row)[word]	= i;
		//DBG(word<<" -> "<<i);
		i++;
	}
	f.close();
	DBG("LoadWordToRowMapping(): "<<i<<" words loaded");
}

