#include <string>
#include <iostream>
#include <fstream>
#include "mgrams.h"

using namespace std;

int main(int argc, char**argv)
{
	if (argc == 1) 
	{
		cerr << "Usage: " << argv[0] << " mgram_statistics_file" << endl;
		exit(1);
	}

	Mgrams mgrams;
	mgrams.LoadStatistics(argv[1]);
	//DBG("Statistics:" << mgrams.mStatistics);
	Mgrams::PhonemeString phn_str;
/*	string s;
	while (cin >> s)
	{
		phn_str.push_back(s);
	}
*/	

phn_str.push_back("ax");
phn_str.push_back("b");
phn_str.push_back("ae");
phn_str.push_back("n");
phn_str.push_back("t");
phn_str.push_back("ax");
phn_str.push_back("n");
phn_str.push_back("m");
phn_str.push_back("ax");
phn_str.push_back("n");
phn_str.push_back("t");

/*
phn_str.push_back("a");
phn_str.push_back("p");
phn_str.push_back("s");
phn_str.push_back("o");
phn_str.push_back("l");
phn_str.push_back("u");
phn_str.push_back("t");
phn_str.push_back("N");
phn_str.push_back("e");
*/
/*
	phn_str.push_back("ey");
	phn_str.push_back("r");
	phn_str.push_back("ax");
	phn_str.push_back("n");
	phn_str.push_back("s");
	phn_str.push_back("en");
*/	
	DBG("phn_str: "<<phn_str);
	Mgrams::MgramStringList mgram_string_list;
	mgrams.PhonemeStringToMgrams(phn_str, 5, &mgram_string_list);
	DBG("mgram_string_list: "<<mgram_string_list);
	return 0;
}
