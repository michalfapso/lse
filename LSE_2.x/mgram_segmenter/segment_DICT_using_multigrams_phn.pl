#!/usr/bin/perl -w
###############
# Script for generating stat file for state splitting script
# by Igor Szoke
# 16.02.2004
# scriptname.pl MLFEHMMFileName OutputTable
###############

if( @ARGV != 4 ){

  print("\n segment_DICT_using_multigrams_phn.pl DICTFileName MGramStatfile MaxN-Gram SegmentedDICT\n\n");
  print("DICTFileName -> Path and FileName of MLF to segment\n");
  print("MGramFile -> Path to read multigram statistics \n");
  print("MaxN-Gram -> 1-MaxN multigram \n");
  print("SegmentedDICTFileName -> Path and FileName of segmented MLF \n");

  print("\n\n");

}else{
  use POSIX qw(ceil floor);

  $ArrayDelta = 100; #problem with negative index in array -> shift by this value
  $Prob0 = -9.9999e300;
  $ProbNotExists = -20;
  $MaxIterations = 0;


  $MLFFileName = $ARGV[0];
  $MGramFileName = $ARGV[1];
  $MaxNGram = $ARGV[2];
  #$PruningFaktor = $ARGV[3];
  #$EndDelete = $ARGV[4];
  $SegmentedMLF = $ARGV[3];


  open( MGram, $MGramFileName ) or die " cannot open $MGramFileName \n";
  while( $RadekMGram=<MGram> ){
    chomp($RadekMGram);
    #002|007|046 -> -6.2322254601395 309
    $RadekMGram =~ /\s*([\w\d\_\-\|]+)\s+->\s+([\d\.-]+)\s+(\d+)/;
    $MGramName = $1;
    $MGramLogLik = $2;
    $MGramOccur = $3;

    $NGramsPk{$MGramName}{"Prob"} = $MGramLogLik;
    $NGramsPk{$MGramName}{"Deleted"} = 0;
  }#while MGram;
  close( MGram );

  sub PSeq {
    my( $From, $To, $Seq, $LSeq ) = @_;    
    my( $Ob, @Pom, $PomFrom );
    if( $From > $To ){
      print( "Warning, in PSeq From > To!\n");
    }
    if( ($From < 1) || ($To > $LSeq) ){
      return $Prob0;
    }else{
      @Pom = split(/\|/, $Seq);
      $Ob = "";
      for( $PomFrom=$From; $PomFrom<=$To; $PomFrom++ ){
        $Ob = $Ob.$Pom[$PomFrom-1]."|";
      }#for
      chop($Ob);

      #$Ob = substr($Seq, ($From-1)*4, ($To-$From)*4+3 );

      if( exists $NGramsPk{$Ob} ){
      #if( $NGramsPk{$Ob}{"Deleted"} == 0 ){
        return $NGramsPk{$Ob}{"Prob"};
      }else{
        return $ProbNotExists*($To - $From + 1);
        print( " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Observation not found!\n");
      }
    }
  }#sub

  sub Seq {
    my( $From, $To, $Seq, $LSeq ) = @_;    
    my( $Ob, @Pom, $PomFrom );
    if( $From > $To ){
      print( "Warning, in PSeq From > To!\n");
    }
    if( ($From < 1) || ($To > $LSeq) ){
      return "";
    }else{
      @Pom = split(/\|/, $Seq);
      $Ob = "";
      for( $PomFrom=$From; $PomFrom<=$To; $PomFrom++ ){
        $Ob = $Ob.$Pom[$PomFrom-1]."|";
      }#for
      chop($Ob);
                                                                                                                                                                                        
      #$Ob = substr($Seq, ($From-1)*4, ($To-$From)*4+3 );
      if( exists $NGramsPk{$Ob} ){
      #if( $NGramsPk{$Ob}{"Deleted"} == 0 ){
      #if( $NGramsPk{$Ob} ){
        return $Ob;
      }else{
        return $Ob;
	print( " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Observation not found!\n");
      }
    }
  }#sub

  open( MLFFile, $MLFFileName ) or die " cannot open $MLFFileName \n";
  open( SegMLFFile, ">".$SegmentedMLF ) or die " cannot open $SegmentedMLF \n";

  $GlobalLikeli = 0;
  $GlobalSegments = 0;
  while( $RadekMLF=<MLFFile> ){
    chomp($RadekMLF);
    $RadekMLF =~ /([^\s^\t]+)[\s\t]+(.*)/;
    $DICT_LABEL=$1;
    $DICT_VAR=$2;
        
        
    $UtterString = $DICT_VAR;
    $UtterLength = 1;
    $UtterCount = 0;
    $UtterString =~ s/[\s\t]+/\|/g;

    $Odkud=0;
    $Kde=index($UtterString,"|",$Odkud);
    while( index($UtterString,"|",$Odkud) != -1 ){
      $Odkud=index($UtterString,"|",$Odkud)+1;
      $UtterLength++;
    }
    
    print( $DICT_LABEL." -> ".$DICT_VAR." -> ".$UtterString."\n");


      #Forward
	undef $Minimas;
	for( $l = 1; $l<=$MaxNGram; $l++){
		$Minimas[ $ArrayDelta + -1*$l ]{"Prob"} = $Prob0;
	}
	$Minimas[ $ArrayDelta + 0 ]{"Prob"} = 0;
	for( $t = 1; $t<=$UtterLength; $t++){
		print("t:$t\n");
		$Minimum = $Prob0;
		$MinimumNGram = "";
		$MinimumLength = 0;
		for( $l = 1; $l<=$MaxNGram; $l++){
			print("l:$l\n");
#			print("dbg: ".($t-$l+1)."\n");
			$pom = $Minimas[ $ArrayDelta + $t-$l]{"Prob"}+&PSeq( $t-$l+1, $t, $UtterString, $UtterLength );
			print("pom:$pom\n");
#print(&Seq( $t-$l+1, $t, $UtterString, $UtterLength )." ".$Minimas[ $ArrayDelta + $t-$l]{"Prob"}."+".&PSeq( $t-$l+1, $t, $UtterString, $UtterLength )."\n");
			if( $pom > $Minimum){
				print("pom > Minimum ... $pom > $Minimum\n");
				$Minimum = $pom;
				$MinimumNGram = &Seq( $t-$l+1, $t, $UtterString, $UtterLength );
				$MinimumLength = $l;
			}
#print($t.",".$l.": ".$pom." ".&Seq( $t-$l+1, $t, $UtterString, $UtterLength )." ".$l."\n");
		}# for n
		$Minimas[ $ArrayDelta + $t]{"Prob"} = $Minimum;
		$Minimas[ $ArrayDelta + $t]{"NGram"} = $MinimumNGram;
		$Minimas[ $ArrayDelta + $t]{"Length"} = $MinimumLength;
		print("Minimas[$t]: $Minimum $MinimumNGram $MinimumLength\n");

	}# for t

        #foreach $zi ( keys %NGramsPk ){
        #  foreach $zi1 ( keys %{$NGramsPk{$zi}} ){
        #  print($zi."->".$NGramsPk{$zi}." ".$NGramsPk{$zi}{"Deleted"}." ".$NGramsPk{$zi}{"Prob"}."\n");
        #}



      #print($UtterString."\n");
#Backward
	$t = $UtterLength;
	$MGramCount = 0;
	while( $t>=1 ){
		print ("--------------------------------------------------\n");
		print ("t:$t\n");
		$NGram = $Minimas[ $ArrayDelta + $t]{"NGram"};
		print ("NGram:$NGram\n");
		if( not exists ($NGramsPk1{ $NGram }{"Occur"}) ){
			$NGramsPk1{ $NGram }{"Occur"} = 0;
		}
		$NGramsPk1{ $NGram }{"Occur"} = $NGramsPk1{ $NGram }{"Occur"} + 1;
		print("Occur:".$NGramsPk1{ $NGram }{"Occur"}."\n");
		$PosNow = $Minimas[ $ArrayDelta + $t ]{"Prob"};
		$PosBef = $Minimas[ $ArrayDelta + $t - $Minimas[ $ArrayDelta + $t ]{"Length"} ]{"Prob"};
		print("PosNow:$PosNow PosBef:$PosBef\n");
		if( not exists $NGramsPk{ $Minimas[ $ArrayDelta + $t]{"NGram"} } ){
			$PosDelta = $ProbNotExists;
		}else{
			$PosDelta = $NGramsPk{ $Minimas[ $ArrayDelta + $t]{"NGram"} }{"Prob"};
		}#if
		print("PosDelta:$PosDelta\n");
		if( ceil( ($PosNow - $PosBef )*1000) != ceil( $PosDelta*1000 ) ){
#print( $Minimas[ $ArrayDelta + $t ]{"NGram"}." ".ceil(($PosNow - $PosBef )*1000)." ".ceil($PosDelta*1000 )."!!!!!!!!!!!!!!!!!!!! Somewhere is an error\n");
		}
#print($Minimas[ $ArrayDelta + $t]{"NGram"}."-");
		$MGramCount++;
		$MGramArr[$MGramCount] = $Minimas[ $ArrayDelta + $t]{"NGram"};
		print("MGramArr[$MGramCount] = ".($Minimas[ $ArrayDelta + $t]{"NGram"})."\n");
		$t = $t - $Minimas[ $ArrayDelta + $t ]{"Length"};
		print("t=$t\n");
		$GlobalSegments++;
	}#while

      $GlobalLikeli = $GlobalLikeli + $Minimas[ $ArrayDelta + $UtterLength ]{"Prob"};
      #print("\n");
      $CountPointer = 1;
      $PomStr=$DICT_LABEL."\t";
      for( $pom=$MGramCount; $pom>=1; $pom-- ){
        @Data = split(/\|/, $MGramArr[$pom]);

        $MGramArr[$pom] =~ s/\|/X/g ;
        $PomStr.=$MGramArr[$pom]." ";
      }#for
      chop($PomStr);
      print( SegMLFFile $PomStr."\n");
  }#while read
  close( MLFFile );
  close( SegMLFFile );

  print("Global likeli: ".($GlobalLikeli).", Global segments: ".$GlobalSegments."\n");
}#if parameters


