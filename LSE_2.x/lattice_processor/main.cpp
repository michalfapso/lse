#include <iostream>
#include <fstream>
#include <string>
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
#include "latbinfile.h"
#include "lexicon.h"
#include "wordclusters.h"
#include "timer.h"
#include "common.h"
#include "latmeetingindexer.h"

using namespace std;
using namespace lse;


#define DBG_MAIN(str) if (p_dbg) cerr << __FILE__ << ':' << setw(5) << __LINE__ << "> " << str << endl << flush;

#define TIMER_START	dbgTimer.start();
#define TIMER_END(msg)	dbgTimer.end();	DBG_FORCE("TIMER["<<msg<<"]: "<<dbgTimer.val());

// sockets imeplementation constants
static const int BUFFER = 2048;
static const int QUEUE = 5; 	// length of the queue of waiting connections

// files extensions
static const string ext_dot = ".dot";
static const string ext_bin = ".bin";
static const string ext_copy = ".lat.copy";
static const string ext_bingz = ".bin.gz";
static const string ext_htk = ".lat";
static const string ext_alphavectors = ".alpha";
static const string ext_betavectors = ".beta";


Timer dbgTimer;

string p_meeting = "";
string p_data_dir_path = "";
string p_out_dir = "";
int p_miliseconds_per_frame = 10;


typedef map<ID_t, int> MeetingsList;


bool cmp_NodesAlphaSort(const LatBinFile::Node& lv, const LatBinFile::Node& rv)
{
	if (lv.t == rv.t)
	{
		return lv.alpha > rv.alpha;
	} else {
		return lv.t < rv.t;
	}
}

bool cmp_NodesBetaSort(const LatBinFile::Node& lv, const LatBinFile::Node& rv)
{
	if (lv.t == rv.t)
	{
		return lv.beta > rv.beta;
	} else {
		return lv.t < rv.t;
	}
}


int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Here should be some HELP (TODO!!!)" << endl;
		return 1;
	}
	
	Lexicon lexicon(Lexicon::readonly); // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer indexer;
	LatBinFile latBinFile_tmp;
		
	int j=1;
	while (j<argc) {

		DBG("argv["<<j<<"]:" << argv[j]);
			
		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		
		} else if (strcmp(argv[j], "-meetings-idx") == 0) {

			j++;
			p_meeting = argv[j];
			DBG("Loading meetings indexes " << p_meeting << "...");
			if (indexer.meetings.loadFromFile(p_meeting) != 0) {
				cerr << "ERROR: Meetings indexer input file " << p_meeting << " not found ... using empty indexer" << endl << flush;
				exit(1);
			}
			DBG("done");
		
		} else if (strcmp(argv[j], "-data-dir-path") == 0) {

			j++;
			p_data_dir_path = argv[j];
			if (p_out_dir == "")
			{
				p_out_dir = p_data_dir_path;
			}
			
		} else if (strcmp(argv[j], "-out-dir") == 0) {

			j++;
			p_out_dir = argv[j];
			
		} else if (strcmp(argv[j], "-miliseconds-per-frame") == 0) {

			j++;
			p_miliseconds_per_frame = atoi(argv[j]);
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				exit(1);
			}
		}
		
		j++;
	}

	for (LatMeetingIndexer::Id2valMap::iterator iMeeting = indexer.meetings.id2valMap.begin();
		 iMeeting != indexer.meetings.id2valMap.end();
		 iMeeting++)
	{
		DBG_FORCE("meetingID:" << iMeeting->first << " path:"<< iMeeting->second);
		LatBinFile binlat;
//		LatBinFile::LatNodeLinksArray::Cursor* linkcur;
		binlat.load(p_data_dir_path + (iMeeting->second).path, 0, -1);
		int frames_count = (int)(binlat.nodes[binlat.nodes.size()-1].t * ((int)(1000/p_miliseconds_per_frame)));
/*
		DBG_FORCE("linksCount:"<<binlat.nodeLinks.linksCount());
		linkcur = new LatBinFile::LatNodeLinksArray::Cursor(&(binlat.nodeLinks));
*/


		vector<LatBinFile::Node> sorted_nodes;

		//
		// LOAD NODES INTO A SORTABLE VECTOR
		//
		for (int nodeID = 0; nodeID < binlat.nodes.size(); nodeID++)
		{
//			DBG_FORCE("nodeID:"<<nodeID<<" "<<binlat.nodes[nodeID]);
			sorted_nodes.push_back(binlat.nodes[nodeID]);
/*			
			linkcur->gotoNodeID(nodeID, FWD);
			while (!linkcur->eol())
			{
				LatBinFile::NodeLinks::Link link = linkcur->getLink();
				DBG_FORCE("nodeID:"<<link.nodeID<<" confidence:"<<link.conf);
				DBG_FORCE("alpha:");
				linkcur->next();
			}
*/
		}
//		DBG_FORCE("nodes loaded");

		int last_frame;
		LatBinFile::Node *pNodePred = NULL;
		//
		// SORT NODES BY ALPHA
		//
		sort(sorted_nodes.begin(), sorted_nodes.end(), cmp_NodesAlphaSort);
		double alpha[frames_count];

		last_frame = 0;
		pNodePred = NULL; // predecessor node
		for (vector<LatBinFile::Node>::iterator iNode = sorted_nodes.begin(); iNode != sorted_nodes.end(); iNode++)
		{
			// nodes are sorted by time and alpha, so that if the previous node has the same time as the current one,
			// we can simply jump over the current node
			if (pNodePred) if (pNodePred->t == iNode->t) 
			{
				continue;
			}
			// get the correct next node (keep on mind that we need to jump over the nodes with the same time)
			vector<LatBinFile::Node>::iterator iNodeNext = iNode;
			while (iNodeNext->t == iNode->t)
			{
				iNodeNext++;
			}
//			DBG_FORCE("--------------------------------------------------------------------------------");
//			DBG_FORCE(*iNode);
//			DBG_FORCE("iNodeNext:"<<*iNodeNext);
//			DBG_FORCE(iNode->t<<" - "<<last_frame<<"*((int)(1000/"<<p_miliseconds_per_frame<<"))");
			int to_frame = (int)round( ( iNode->t - last_frame/((int)(1000/p_miliseconds_per_frame)) ) * ((int)(1000/p_miliseconds_per_frame)) );
//			DBG_FORCE("to_frame:"<<to_frame);
//			if (pNodePred) DBG_FORCE("inc_per_frame = "<<pNodePred->alpha<<" - "<<iNode->alpha<<") / ("<<to_frame<<" - "<<last_frame<<")");
			float inc_per_frame = 0.0;
			if (pNodePred)
			{
				inc_per_frame = (iNode->alpha - pNodePred->alpha) / (to_frame - last_frame);
			}
//			DBG_FORCE("inc_per_frame:"<<inc_per_frame);
			for (int i = last_frame; i < to_frame; i++)
			{
				alpha[i] = pNodePred->alpha + inc_per_frame*(i - last_frame);
//				DBG_FORCE("alpha["<<i<<"]: "<<alpha[i]);
			}
			last_frame = to_frame;
			pNodePred = &(*iNode);
		}


		//
		// SORT NODES BY BETA
		//
		sort(sorted_nodes.begin(), sorted_nodes.end(), cmp_NodesBetaSort);
		double beta[frames_count];

		last_frame = 0;
		pNodePred = NULL; // predecessor node
		for (vector<LatBinFile::Node>::iterator iNode = sorted_nodes.begin(); iNode != sorted_nodes.end(); iNode++)
		{
			// nodes are sorted by time and beta, so that if the previous node has the same time as the current one,
			// we can simply jump over the current node
			if (pNodePred) if (pNodePred->t == iNode->t) 
			{
				continue;
			}
			// get the correct next node (keep on mind that we need to jump over the nodes with the same time)
			vector<LatBinFile::Node>::iterator iNodeNext = iNode;
			while (iNodeNext->t == iNode->t)
			{
				iNodeNext++;
			}
//			DBG_FORCE("--------------------------------------------------------------------------------");
//			DBG_FORCE(*iNode);
			int to_frame = (int)round( ( iNode->t - last_frame/((int)(1000/p_miliseconds_per_frame)) ) * ((int)(1000/p_miliseconds_per_frame)) );
//			DBG_FORCE("to_frame:"<<to_frame);
//			if (pNodePred) DBG_FORCE("inc_per_frame = "<<pNodePred->beta<<" - "<<iNode->beta<<") / ("<<to_frame<<" - "<<last_frame<<")");
			float inc_per_frame = 0.0;
			if (pNodePred)
			{
				inc_per_frame = (pNodePred->beta - iNode->beta) / (to_frame - last_frame);
			}
//			DBG_FORCE("inc_per_frame:"<<inc_per_frame);
			for (int i = last_frame; i < to_frame; i++)
			{
				beta[i] = pNodePred->beta - inc_per_frame*(i - last_frame);
//				DBG_FORCE("beta["<<i<<"]: "<<beta[i]);
			}
			last_frame = to_frame;
			pNodePred = &(*iNode);
		}

		ofstream out_alpha((p_out_dir + (iMeeting->second).path + ext_alphavectors).c_str());
		ofstream out_beta((p_out_dir + (iMeeting->second).path + ext_betavectors).c_str());
		for (int i=0; i<frames_count; i++)
		{
			out_alpha << alpha[i] << endl;
			out_beta << beta[i] << endl;
		}
		out_alpha.close();
		out_beta.close();

	} // for all meetings (lattices)



	return 0;
}


