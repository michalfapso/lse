#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "determine_hits.h"
#include "fom_scorer.h"
#include "hypotheses.h"
#include "mlf_with_hit_info.h"

using namespace std;
using namespace mlf;

//--------------------------------------------------------------------------------
#ifndef DBG
	#define DBG(str) std::cerr << std::dec << std::setprecision(13) << std::setw(20) << std::left << __FILE__ << ':' << std::setw(5) << std::right << __LINE__ << "> " << str << std::endl << std::flush
#endif
//#define DBG(str)

int main(int argc, char * argv[]) {
//	if (argc == 1) {
//		cerr << "Application takes filenames as parameters." << endl;
//		return 1;
//	}

	//application parameters
	char* p_mlf_ref = NULL;
	char* p_mlf_out = NULL;
	char* p_roc_out = NULL;
//	char* p_mlf_res = NULL;
	char* p_termlist = NULL;
	float p_duration_seconds = 0;
	bool  p_is_pooled = false;
	bool  p_is_utterrance_norm_simulation = false;
	Hypothesis::IsHitFunPtr is_hit_fun_ptr = &Hypothesis::IsHit_point_middle_of_result;
	Mlf<Hypothesis> mlf_res;

	int j=1;
	while (j<argc) {

		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		}
	
		if (strcmp(argv[j], "-mlf-ref") == 0) {
			j++;
			p_mlf_ref = argv[j];
			
		} else if (strcmp(argv[j], "-mlf-res") == 0) {
			j++;
			cerr << "Loading results MLF...";
			mlf_res.Load(argv[j]);
			cerr << "done" << endl;
//			p_mlf_res = argv[j];

		} else if (strcmp(argv[j], "-termlist") == 0) {
			j++;
			p_termlist = argv[j];
			
		} else if (strcmp(argv[j], "-duration-seconds") == 0) {
			j++;
			p_duration_seconds = atof(argv[j]);
			cerr << "Setting duration to " << p_duration_seconds << " seconds." << endl;
			
		} else if (strcmp(argv[j], "-is-hit") == 0) {
			j++;
			is_hit_fun_ptr = Hypothesis::String2IsHitFunPtr(argv[j]);

		} else if (strcmp(argv[j], "-mlf-out-with-hit-info") == 0) {
			j++;
			p_mlf_out = argv[j];
			
		} else if (strcmp(argv[j], "-roc-out") == 0) {
			j++;
			p_roc_out = argv[j];
			
		} else if (strcmp(argv[j], "-pooled") == 0) {
			p_is_pooled = true;
			
		} else if (strcmp(argv[j], "-utterrance-norm-simulation") == 0) {
			p_is_utterrance_norm_simulation = true;
			
		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				cerr << "Unknown switch: " << argv[j] << endl << flush;
				return 1;
			}
		}
		j++;
	}

	if (!p_mlf_ref || mlf_res.empty() || !p_termlist || !p_duration_seconds) {
		cerr << "Usage: "<<argv[0]<<" -mlf-ref reference.mlf -mlf-res hypotheses.mlf -termlist termlist.txt -duration-seconds SPEECH_SECONDS -is-hit IS_HIT_FUN -roc-out out.roc [-pooled | -utterrance-norm-simulation]" << endl;
		cerr << "IS_HIT_FUN = [" << Hypothesis::IsHitFunGetStringNames() << "]" << endl;

		if (!p_mlf_ref)          { cerr << "...missing -mlf-ref" << endl; }
		if (mlf_res.empty())     { cerr << "...empty -mlf-res" << endl; }
		if (!p_termlist)         { cerr << "...missing -termlist" << endl; }
		if (!p_duration_seconds) { cerr << "...missing -duration-seconds" << endl; }
		return 1;
	}

	float durationHours = p_duration_seconds/3600;

	cerr << "Loading reference MLF...";
	Mlf<ReferenceMlfRecord>             mlf_ref(p_mlf_ref);
	cerr << "done" << endl;

	DBG("mlf_res size: "<<mlf_res.size());

	MlfWord2Records<ReferenceMlfRecord> w2r_ref(mlf_ref);
	MlfWord2Records<Hypothesis>         w2r_res(mlf_res);
	Termlist                            termlist(p_termlist);

	DetermineHits::Process(w2r_ref, w2r_res, is_hit_fun_ptr);
	DetermineHits::CheckMultipleReferenceHits(w2r_ref);

	Fom fom(durationHours, FomScorer::GetOccCount(w2r_ref, termlist));
	if (p_is_pooled) {
		cerr << "ERROR: -pooled is not yet implemented!" << endl;
		exit(1);
		fom = FomScorer::ComputeFomPooled  (w2r_ref, w2r_res, termlist, durationHours);
	} else if (p_is_utterrance_norm_simulation) {
		fom = FomScorer::ComputeFomPerFile (w2r_ref, w2r_res, termlist, durationHours);
	} else {
		fom = FomScorer::ComputeFom        (w2r_ref, w2r_res, termlist, durationHours);
	}
//	scorer.ComputeFomPerFile (w2r_ref, w2r_res, termlist, durationHours);
//	scorer.ComputeFomWallace (w2r_ref, w2r_res, termlist, durationHours);
	

	if (p_roc_out) {
		ofstream f(p_roc_out);
		if ( !f.good() ) {
			throw runtime_error("Error opening output file '"+std::string(p_roc_out)+"'");
		}
		const FomVector& v = fom.GetFomVector();
		f << "0 0" << std::endl;
		for (unsigned int i=0; i<v.size(); i++) {
			f << ((i+0.5) / durationHours) << " " << ((float)v.Get(i) / fom.GetRefOccCount() * 100) << std::endl;
		}
		f.close();
	}

	if (p_mlf_out) {
		MlfWithHitInfo::Save(mlf_res, p_mlf_out);
	}
	return 0;
}


