#!/bin/bash

. args_lib.sh

required_vars RESULTS_OUT REFERENCE_MLF TERMLIST SECONDS_PER_DATA

if [ -z $INPUT_MLF ]; then
	INPUT_MLF="$@"
fi

IS_HIT_METHOD=HALF_SEC_AROUND_REF # MID_RES_IN_REF | MID_REF_IN_RES | MID_BOTH | HALF_SEC_AROUND_REF
../kws_scorer \
	-mlf-ref $REFERENCE_MLF \
	-mlf-res $INPUT_MLF \
	-termlist $TERMLIST \
	-duration-seconds $SECONDS_PER_DATA \
	-is-hit $IS_HIT_METHOD \
	-roc-out $RESULTS_OUT.roc \
	| sort -k5,5nr \
	| awk '$1=="Overall:" {overall=$0; next} {print} END{print overall}' \
	> $RESULTS_OUT

# Utterrance normalization simulation
../kws_scorer \
	-mlf-ref $REFERENCE_MLF \
	-mlf-res $INPUT_MLF \
	-termlist $TERMLIST \
	-duration-seconds $SECONDS_PER_DATA \
	-is-hit $IS_HIT_METHOD \
	-roc-out $RESULTS_OUT.uttnormroc \
	-utterrance-norm-simulation \
	> $RESULTS_OUT.uttnorm

