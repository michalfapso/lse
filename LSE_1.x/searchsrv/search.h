#ifndef SEARCH_H
#define SEARCH_H

#include <vector>
#include <list>
#include <map>
#include "lattypes.h"
#include "latbinfile.h"
#include "latindexer.h"
#include "querykwdlist.h"
#include "tokenlattice.h"
#include "hypothesis.h"
#include "SearchClusterGroups.h"
#include "results.h"

namespace lse {

//#define DBG_SEARCH(str) DBG_FORCE(str)
#define DBG_SEARCH(str)
	
//========================================================================================
// Search
//========================================================================================
class Search {
	public:

	private:
		LatBinFile*	binlat;
		Lexicon*	mpLexicon;

		float *nodesScore;

		int mRecordChannelColumn;
		int mRecordNameCutoffColumn;
		char mRecordDelim;
	public:

		QueryKwdList *queryKwdList;
		SearchClusterGroups clusterGroups;

		Search(QueryKwdList *p_queryKwdList) : 
			mpLexicon(NULL), 
			nodesScore(NULL), 
			mRecordChannelColumn(0),
			mRecordNameCutoffColumn(0),
			mRecordDelim('_')
		{
			queryKwdList = p_queryKwdList;
		};

		void SetRecordChannelColumn(int col) { mRecordChannelColumn = col; }
		void SetRecordNameCutoffColumn(int col) { mRecordNameCutoffColumn = col; }
		void SetRecordDelim(char delim) { mRecordDelim = delim; }
		void SetBinlat(LatBinFile &p_binlat) { this->binlat = &p_binlat; this->mpLexicon = p_binlat.lat->lexicon; }
		void SetLexicon(Lexicon* pLexicon) { this->mpLexicon = pLexicon; }
		void SetQueryKwdList(QueryKwdList* pQueryKwdList) { this->queryKwdList = pQueryKwdList; }

		void RescoreHits(SearchClusterGroup* pGroup, LatMeetingIndexerRecord meetingRec, Results *results);
		void FillClusterGroups();
		void SortClusterGroups() { clusterGroups.sortGroups(); }
		void PrintClusterGroups();
		void GetNodesContext(ID_t nodeID, DirectionType dir, Hypothesis *hyp);
		void ExpandPhonemes(const QueryKwdList* src, QueryKwdList* dest);
		void PrintCandidates(SearchClusterGroup* pGroup, LatMeetingIndexerRecord &rMeetingRec);
		void PrintCandidateTrigrams(SearchClusterGroup* pGroup, LatMeetingIndexerRecord &rMeetingRec);

		std::string GetMeetingChannelFromMeetingName(const std::string &meetingName);
		std::string CutoffMeetingName(const std::string &meetingName);

	private:

		QueryKwdList::iterator getBestKwd();

		void fillClustersGroupFwd_recursive(SearchClusterGroup *group, 
											QueryKwd::RevIdxRecord* curRevIdxRecord, 
											QueryKwdList::iterator curKwd);

		void fillClustersGroupBck_recursive(SearchClusterGroup *group, 
											QueryKwd::RevIdxRecord* curRevIdxRecordIdxRecord, 
											QueryKwdList::iterator curKwd);

		enum EFrontBack {
			front,
			back,
		};

		struct HistRecord {
			ID_t 		W_id;
			std::string	W;
		};

		typedef std::list<HistRecord> HistContainer;
	
		void addToHypothesis(ID_t curNodeID, LatTime tStart, float conf, Hypothesis *hyp, EFrontBack frontback);
		void AddPathToHypothesis(WLR* pBestPath, Hypothesis* pHyp, ID_t* pFirstNodeID);

}; // class Search


} // namespace lse

#endif

