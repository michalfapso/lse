#include <algorithm> // sort()
#include "normalize.h"
#include "resultsgroups.h"

using namespace std;
using namespace lse;


void Results::SetQueryKwdList(QueryKwdList *pQueryKwdList)
{
	mpQueryKwdList = pQueryKwdList;
}

void Results::postProcess(NormLexicon *pNormLexicon) 
{
	this->NormalizeResults(pNormLexicon);
	this->sortResults();
	this->joinOverlappingResults();
}

void Results::joinOverlappingResults()
{
	// for each result group
//	for (Results::iterator iRS=this->begin(); iRS!=this->end(); ++iRS)
	for (ResultsGroups::iterator iGrp=resultsGroups.begin(); iGrp!=resultsGroups.end(); ++iGrp)
	{
		volatile bool overlapping = true;
		while (overlapping) 
		{
			overlapping = false;
			// compare results only within 1 RecordStream
//			for (vector<Hypothesis>::iterator iHyp1=(iRS->second).begin(); !overlapping, iHyp1!=(iRS->second).end(); ++iHyp1)
//			for (vector<Hypothesis>::iterator iHyp1=iRS->begin(); !overlapping, iHyp1!=iRS->end(); ++iHyp1)
			for (ResultsGroup::iterator iRes1=iGrp->begin(); !overlapping, iRes1!=iGrp->end(); ++iRes1)
			{
//				for (vector<Hypothesis>::iterator iHyp2=(iRS->second).begin(); !overlapping, iHyp2!=(iRS->second).end(); ++iHyp2) 
//				for (vector<Hypothesis>::iterator iHyp2=iRS->begin(); !overlapping, iHyp2!=iRS->end(); ++iHyp2) 
				for (ResultsGroup::iterator iRes2=iGrp->begin(); !overlapping, iRes2!=iGrp->end(); ++iRes2) 
				{
					Results::iterator iHyp1 = *iRes1;
					Results::iterator iHyp2 = *iRes2;
					// don't compare the same results
					if (iHyp1 == iHyp2) continue; 

					LatTime iHyp1_start = iHyp1->words.begin()->start;
					LatTime iHyp1_end   = (--iHyp1->words.end())->end;
					LatTime iHyp2_start = iHyp2->words.begin()->start;
					LatTime iHyp2_end   = (--iHyp2->words.end())->end;
/*
					DBG_FORCE("iHyp1_start: " << iHyp1_start << " iHyp1_end: " << iHyp1_end);
					DBG_FORCE("iHyp2_start: " << iHyp2_start << " iHyp2_end: " << iHyp2_end);
					DBG_FORCE("iHyp1_score: " << iHyp1->score);
					DBG_FORCE("iHyp2_score: " << iHyp2->score);
*/						
					// overlapping detection
					if ((iHyp1_start >= iHyp2_start && iHyp1_start <  iHyp2_end) ||
						(iHyp1_end   >  iHyp2_start && iHyp1_end   <= iHyp2_end) ||
						(iHyp1_start <  iHyp2_start && iHyp1_end   >  iHyp2_end))
					{
						DBG_FORCE("overlapping results:" << endl << *iHyp1 << *iHyp2);

						bool iHyp1_is_better = iHyp1->keywords.size() > iHyp2->keywords.size();
						if (iHyp1->keywords.size() == iHyp2->keywords.size()) {
							iHyp1_is_better = iHyp1->score > iHyp2->score;
						}
						
						// remove the worse one
						if (iHyp1_is_better) {
//							cout << "iHyp1 is better" << endl << flush;
//							(iRS->second).erase(iHyp2);
							iGrp->erase(iRes2);
						} else {
//							cout << "iHyp2 is better" << endl << flush;
//							(iRS->second).erase(iHyp1);
							iGrp->erase(iRes1);
						}
						DBG_FORCE("Results::joinOverlappingResults() ... 1 result erased");
//						cout << "erased" << endl << flush;
						overlapping = true;
						break;
					}
				} // for iHyp2
				if (overlapping) break;
			} // for iHyp1
		} // while (overlapping)
	} // for all RecordStreams
	exit(1);
}

Results::ResultsGroups::iterator Results::findResultsGroup(Results::iterator iRes)
{
	// for each results group
	for (ResultsGroups::iterator iGrp=resultsGroups.begin(); iGrp!=resultsGroups.end(); ++iGrp) 
	{
		// if the record is the same in the group and in the given hypothesis (iRes)
		if (iGrp->record == iRes->record) {
			DBG("Group found: "<<iGrp->record);
			return iGrp;
		}
	}
	return resultsGroups.end();
}

void Results::sortResults()
{
	// first sort all results by score
	sort(this->begin(), this->end());

	// CREATE GROUPS OF RESULTS WHICH BELONGS TO THE SAME SPEECH RECORD
	// for each result
	for (Results::iterator iRes=this->begin(); iRes!=this->end(); ++iRes)
	{
		DBG("result:"<<*iRes);
		ResultsGroups::iterator iOutGroup;
		// if there is already some result from the same speech record
		if ( (iOutGroup = findResultsGroup(iRes)) != resultsGroups.end() )
		{
			iOutGroup->push_back(iRes);
		} 
		else {
			DBG("New group: "<<iRes->record);
			// create a new group
			ResultsGroup grp(iRes->record);
			// add there the current Result iterator
			grp.push_back(iRes);
			// add this group to the groups container
			resultsGroups.push_back(grp);
		}
	}
}

void Results::print(ostream *os, EType type, float searchTime) 
{
	DBG_FORCE("Results::print()");
	DBG_FORCE("count: "<<resultsGroups.size());
	if (type == ETypeXml)
	{
		*os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << endl; 
	}
	else if (type == ETypeXml_std)
	{
 		*os << "  <detected_termlist termid=\""<<mpQueryKwdList->mTermId<<"\""
		    << " term_search_time=\""<<searchTime<<"\""
		    << " oov_term_count=\""<<mOovCount<<"\">" << endl;
	}
	for (ResultsGroups::iterator i=resultsGroups.begin(); i!=resultsGroups.end(); ++i) 
	{		
		if (type == ETypeXml)
		{
			for (ResultsGroup::iterator j=i->begin(); j!=i->end(); ++j) {
				*os << "<hypothesis stream=\"LVCSR\" "
					<< " record=\""<<i->record<<"\" "
					<< " score=\""<< fixed << setprecision(4) << (**j).score <<"\" >" << endl;
				(*j)->printXml(os);
				*os << "</hypothesis>" << endl;
			}
		}
		else if (type == ETypeXml_std)
		{
			for (ResultsGroup::iterator j=i->begin(); j!=i->end(); ++j) {
				*os << "    <term file=\""<< i->record <<"\" "
					<< " channel=\""<<(*j)->channel<<"\" "
					<< " tbeg=\""<< ((*j)->keywords.begin())->start <<"\""
					<< " dur=\""<< ((*j)->keywords.rbegin())->end - ((*j)->keywords.begin())->start <<"\""
					<< " score=\""<< fixed << setprecision(4) << (*j)->score <<"\""
					<< " decision=\""<< ((*j)->decision ? "YES" : "NO") << "\"/>" << endl;
			}
		}
		else
		{
			*os << "HYPOTHESIS:" << endl;
	//		*os << "STREAM: \t" << (i->first).stream << endl;
			*os << "STREAM: \tLVCSR" << endl;
			*os << "RECORD: \t" << i->record << endl;
			*os << "COUNT: \t" << i->size() << endl;
	//		for (vector<Hypothesis>::iterator j=(i->second).begin(); j!=(i->second).end(); ++j) {
			for (ResultsGroup::iterator j=i->begin(); j!=i->end(); ++j) {
				*os << "SCORE: \t" << fixed << setprecision(4) << (**j).score<< endl;
				*os << **j << endl;
			}
		}
	}
	if (type == ETypeXml_std)
	{
 		*os << "  </detected_termlist>" << endl;

	}
	DBG_FORCE("Results::print()...done");
}

void Results::push_back(lse::Hypothesis hyp)
{
	// check if there is some limit on number of hypotheses and
	// if the hypothesis is not empty
	// 
	// TODO it will not work if &ALLSUCCESSFULTOKENS is set because
	// the first rescored hypothesis may return even more results than
	// is set in &RES=... so no other rescored hypothesis will be pushed
	// into results collection
	if (((int)this->size() < mMaxResultsCount || mMaxResultsCount == -1) &&
		(hyp.keywordsCount > 0))
	{
		((ResultsBaseType*)this)->push_back(hyp);
	}
}

void Results::SetMaxHypothesesCount(int max)
{
	mMaxResultsCount = max;
}
		
/*
std::ostream& operator<<(std::ostream& os, const Results& res)
{
	for (Results::const_iterator i=res.begin(); i!=res.end(); ++i) 
	{		
		os << "HYPOTHESIS:" << endl;
		os << "STREAM: \t" << (i->first).stream << endl;
		os << "RECORD: \t" << (i->first).record << endl;
		os << "COUNT: \t" << (i->second).size() << endl;
		for (vector<Hypothesis>::const_iterator j=(i->second).begin(); j!=(i->second).end(); ++j) {
			os << *j << endl;
		}
	}
	return os;
}
*/

void Results::NormalizeResults(NormLexicon *pNormLexicon)
{
	DBG_FORCE("Results::NormalizeResults()");
	for (Results::iterator iRes=this->begin(); iRes!=this->end(); ++iRes)
	{
		DBG_FORCE("result score:"<<iRes->score);
		DBG_FORCE("result:"<<*iRes);

		// if there is already some result from the same speech record
		for (Hypothesis::Words::iterator iWord = iRes->words.begin();
			 iWord != iRes->words.end();
			 iWord ++)
		{
			iRes->score -= pNormLexicon->GetWordNormConst(iWord->str);
		}
		iRes->decision = iRes->score > 0;
		DBG_FORCE("result score NORMALIZED:"<<iRes->score);
		DBG_FORCE("result NORMALIZED:"<<*iRes);
	}
}

