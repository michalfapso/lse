#include <algorithm> // sort()
#include "results.h"

using namespace std;
using namespace lse;


void Results::SetQueryKwdList(QueryKwdList *pQueryKwdList)
{
	mpQueryKwdList = pQueryKwdList;
}

void Results::postProcess(NormLexicon *pNormLexicon) 
{
	if (pNormLexicon->IsActive())
	{
		this->NormalizeResults(pNormLexicon);
	}
//	this->sortResults();
//	this->joinOverlappingResults();
}

void Results::joinOverlappingResults()
{
		volatile bool overlapping = true;
		while (overlapping) 
		{
			overlapping = false;
			// compare results only within 1 RecordStream
//			for (vector<Hypothesis>::iterator iHyp1=(iRS->second).begin(); !overlapping, iHyp1!=(iRS->second).end(); ++iHyp1)
//			for (vector<Hypothesis>::iterator iHyp1=iRS->begin(); !overlapping, iHyp1!=iRS->end(); ++iHyp1)
			for (ResultsBaseType::iterator iHyp1=this->begin(); !overlapping, iHyp1!=this->end(); ++iHyp1)
			{
//				for (vector<Hypothesis>::iterator iHyp2=(iRS->second).begin(); !overlapping, iHyp2!=(iRS->second).end(); ++iHyp2) 
//				for (vector<Hypothesis>::iterator iHyp2=iRS->begin(); !overlapping, iHyp2!=iRS->end(); ++iHyp2) 
				for (ResultsBaseType::iterator iHyp2=this->begin(); !overlapping, iHyp2!=this->end(); ++iHyp2) 
				{
					// do not compare hypotheses from different records
					if ( !(iHyp1->record == iHyp2->record && iHyp1->channel == iHyp2->channel) ) continue;

					// don't compare the same results
					if (iHyp1 == iHyp2) continue; 

					LatTime iHyp1_start = iHyp1->words.begin()->start;
					LatTime iHyp1_end   = (--iHyp1->words.end())->end;
					LatTime iHyp2_start = iHyp2->words.begin()->start;
					LatTime iHyp2_end   = (--iHyp2->words.end())->end;
/*
					DBG_FORCE("iHyp1_start: " << iHyp1_start << " iHyp1_end: " << iHyp1_end);
					DBG_FORCE("iHyp2_start: " << iHyp2_start << " iHyp2_end: " << iHyp2_end);
					DBG_FORCE("iHyp1_score: " << iHyp1->score);
					DBG_FORCE("iHyp2_score: " << iHyp2->score);
*/						
					// overlapping detection
					if ((iHyp1_start >= iHyp2_start && iHyp1_start <  iHyp2_end) ||
						(iHyp1_end   >  iHyp2_start && iHyp1_end   <= iHyp2_end) ||
						(iHyp1_start <  iHyp2_start && iHyp1_end   >  iHyp2_end))
					{
						DBG_FORCE("overlapping results:" << endl << *iHyp1 << *iHyp2);

						bool iHyp1_is_better = iHyp1->keywords.size() > iHyp2->keywords.size();
						if (iHyp1->keywords.size() == iHyp2->keywords.size()) {
							iHyp1_is_better = iHyp1->score > iHyp2->score;
						}
						
						// remove the worse one
						if (iHyp1_is_better) {
//							cout << "iHyp1 is better" << endl << flush;
//							(iRS->second).erase(iHyp2);
							this->erase(iHyp2);
						} else {
//							cout << "iHyp2 is better" << endl << flush;
//							(iRS->second).erase(iHyp1);
							this->erase(iHyp1);
						}
						DBG_FORCE("Results::joinOverlappingResults() ... 1 result erased");
//						cout << "erased" << endl << flush;
						overlapping = true;
						break;
					}
				} // for iHyp2
				if (overlapping) break;
			} // for iHyp1
		} // while (overlapping)
}

void Results::sortResults()
{
	// sort all results by score
	sort(this->begin(), this->end());
}

void Results::print(ostream *os, EType type, float searchTime) 
{
	DBG_FORCE("Results::print()");
	DBG_FORCE("count: "<<this->size());
	if (type == ETypeXml)
	{
		*os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << endl; 
	}
	else if (type == ETypeXml_std)
	{
 		*os << "  <detected_termlist termid=\""<<mpQueryKwdList->mTermId<<"\""
		    << " term_search_time=\""<<searchTime<<"\""
		    << " oov_term_count=\""<<mOovCount<<"\">" << endl;
	}
	for (ResultsBaseType::iterator i=this->begin(); i!=this->end(); ++i) 
	{		
		if (type == ETypeXml)
		{
			*os << "<hypothesis stream=\"LVCSR\" "
				<< " record=\""<<i->record<<"\" "
				<< " score=\""<< fixed << setprecision(4) << i->score <<"\" >" << endl;
			i->printXml(os);
			*os << "</hypothesis>" << endl;
		}
		else if (type == ETypeXml_std)
		{
			*os << "    <term file=\""<< i->record <<"\""
				<< " channel=\""<<i->channel<<"\""
				<< " tbeg=\""<< (i->keywords.begin())->start <<"\""
				<< " dur=\""<< (i->keywords.rbegin())->end - (i->keywords.begin())->start <<"\""
				<< " score=\""<< fixed << setprecision(4) << i->score <<"\""
				<< " decision=\""<< (i->decision ? "YES" : "NO") << "\"/>" << endl;
		}
		else
		{
			*os << "HYPOTHESIS:" << endl;
	//		*os << "STREAM: \t" << (i->first).stream << endl;
			*os << "STREAM: \tLVCSR" << endl;
			*os << "RECORD: \t" << i->record << endl;
//			*os << "CHANNEL: \t" << i->channel << endl;
			*os << "COUNT: \t1" << endl;
			*os << "SCORE: \t" << fixed << setprecision(4) << i->score<< endl;
			*os << *i << endl;
		}
	}
	if (type == ETypeXml_std)
	{
 		*os << "  </detected_termlist>" << endl;

	}
	DBG_FORCE("Results::print()...done");
}

void Results::push_back(lse::Hypothesis hyp)
{
	// check if there is some limit on number of hypotheses and
	// if the hypothesis is not empty
	// 
	// TODO it will not work if &ALLSUCCESSFULTOKENS is set because
	// the first rescored hypothesis may return even more results than
	// is set in &RES=... so no other rescored hypothesis will be pushed
	// into results collection
	if (((int)this->size() < mMaxResultsCount || mMaxResultsCount == -1) &&
		(hyp.keywordsCount > 0))
	{
		((ResultsBaseType*)this)->push_back(hyp);
	}
}

void Results::SetMaxHypothesesCount(int max)
{
	mMaxResultsCount = max;
}
		
/*
std::ostream& operator<<(std::ostream& os, const Results& res)
{
	for (Results::const_iterator i=res.begin(); i!=res.end(); ++i) 
	{		
		os << "HYPOTHESIS:" << endl;
		os << "STREAM: \t" << (i->first).stream << endl;
		os << "RECORD: \t" << (i->first).record << endl;
		os << "COUNT: \t" << (i->second).size() << endl;
		for (vector<Hypothesis>::const_iterator j=(i->second).begin(); j!=(i->second).end(); ++j) {
			os << *j << endl;
		}
	}
	return os;
}
*/

void Results::NormalizeResults(NormLexicon *pNormLexicon)
{
//	DBG_FORCE("Results::NormalizeResults()");
	for (Results::iterator iRes=this->begin(); iRes!=this->end(); ++iRes)
	{
//		DBG_FORCE("result score:"<<iRes->score);
//		DBG_FORCE("result:"<<*iRes);

		// if there is already some result from the same speech record
		for (Hypothesis::Words::iterator iWord = iRes->words.begin();
			 iWord != iRes->words.end();
			 iWord ++)
		{
			iRes->score -= pNormLexicon->GetWordNormConst(iWord->str);
		}
		iRes->score -= pNormLexicon->GetWordNormConst("sil");
		iRes->decision = iRes->score > mHardDecisionTreshold;
//		DBG_FORCE("result score NORMALIZED:"<<iRes->score);
//		DBG_FORCE("result NORMALIZED:"<<*iRes);
	}
}

