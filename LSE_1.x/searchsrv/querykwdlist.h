#ifndef QUERYKWDLIST_H
#define QUERYKWDLIST_H

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <iomanip>
#include "lattypes.h"
#include "latindexer.h"
#include "lexicon.h"
//#include "search.h"

namespace lse {

#define DEFAULT_HYPS_PER_STREAM 			2
#define DEFAULT_TIME_DELTA 					0
#define DEFAULT_FWD_NODES_COUNT 			5
#define DEFAULT_BCK_NODES_COUNT 			5
#define DEFAULT_PHRASE_KWD_NEIGHBORHOOD 	0.0
#define DEFAULT_MAX_RESULTS 				-1 // -1 means no limit



enum TimeValueType {
	null,
	percents,
	time
};

class QueryKwdList;

class QueryKwd {

public:
	class RevIdxRecord : public LatIndexer::Record
	{
		public:
			typedef std::vector< RevIdxRecord* > RevIdxRecordList;
			RevIdxRecordList mFwdClusters;
			RevIdxRecordList mBckClusters;
			RevIdxRecord* mpBestFwdKwdCluster;
			bool mBestFwdKwdClusterFound; 				// true, if there is some matching cluster in the following kwd
	};

	class RevIdxRecords : public LatIndexer::RecordsReader
	{
		public:
			std::vector<RevIdxRecord> mvByConfidence;

			void push_back(const LatIndexer::Record &rec);
			unsigned int size();

			RevIdxRecords() : LatIndexer::RecordsReader() {}
	};

	typedef std::vector<RevIdxRecord *> RevIdxRecordList;

public:
	std::string 	W;
	ID_t 			W_id;
	float 			nb_prev; ///< previous word's neighborhood (how far can be two keywords)
	float 			nb_next; ///< next word's neighborhood (how far can be two keywords)
	bool 			isNextInPhrase; ///< if the current kwd is in phrase with the next one in QueryKwdList
	float 			confidence_min; ///< min confidence treshold
	float 			confidence_max; ///< max confidence treshold
	bool 			confidence_min_isset;
	bool 			confidence_max_isset;
	LatTime 		time_min;
	LatTime 		time_max;
	TimeValueType 	time_min_type;
	TimeValueType 	time_max_type;
	bool 			lessThan;
	bool 			greaterThan;
	bool 			mIsNgram;
	
	RevIdxRecords 							searchResults;
//	LatIndexer::RecordClusters 				recClusters;
	std::vector< ID_t >						phonemes; ///< WordID of all phonemes in the n-gram
	

	QueryKwd() : W(""),
				 W_id(-1), 
				 nb_prev(-1), 
				 nb_next(-1), 
				 isNextInPhrase(false), 
				 confidence_min(-INF),
				 confidence_max( INF),
				 confidence_min_isset(false),
				 confidence_max_isset(false),
				 time_min(0),
				 time_max(0),
				 time_min_type(null),
				 time_max_type(null),
				 lessThan(false),
				 greaterThan(false),
				 mIsNgram(false)
	{}
	
	friend std::ostream& operator<<(std::ostream& os, const lse::QueryKwd& kwd);
};

class QueryKwdList : public std::vector< QueryKwd > {
	Lexicon*		mpLexicon;
public:
	unsigned int 	hyps_per_stream;
	float 			time_delta;
	int 			fwd_nodes_count;
	int 			bck_nodes_count;
	float 			phrase_kwd_neighborhood;
	int 			max_results;
	bool 			mExactMatch;
	bool 			mNgram2Phn;
	bool			mPhn2Ngram;
	int				mPhonemesCnt;
	int				mUniqSize;					///< unique keywords count
	bool 			mReturnAllSuccessfulTokens;
	bool			mOverlapping;
	std::string		mTermId;


	
	QueryKwdList(Lexicon* pLexicon) :
		mpLexicon					( pLexicon ),
		hyps_per_stream 			( DEFAULT_HYPS_PER_STREAM ),
		time_delta 					( DEFAULT_TIME_DELTA ),
		fwd_nodes_count 			( DEFAULT_FWD_NODES_COUNT ),
		bck_nodes_count 			( DEFAULT_BCK_NODES_COUNT ),
		phrase_kwd_neighborhood 	( DEFAULT_PHRASE_KWD_NEIGHBORHOOD ),
		max_results 				( DEFAULT_MAX_RESULTS ),
		mExactMatch					( false ),
		mNgram2Phn					( false ),
		mPhn2Ngram					( false ),
		mPhonemesCnt				( 0 ),
		mUniqSize					( 0 ),
		mReturnAllSuccessfulTokens	( false ),
		mOverlapping				( false ),
		mTermId						( "" )
	{}

	void CopyParams(QueryKwdList &rParent)
	{
		mpLexicon					= rParent.mpLexicon;
		
		hyps_per_stream				= rParent.hyps_per_stream;
		time_delta					= rParent.time_delta;
		fwd_nodes_count				= rParent.fwd_nodes_count;
		bck_nodes_count				= rParent.bck_nodes_count;
		phrase_kwd_neighborhood		= rParent.phrase_kwd_neighborhood;
		max_results					= rParent.max_results;
		mExactMatch					= rParent.mExactMatch;
		mNgram2Phn					= rParent.mNgram2Phn;
		mPhn2Ngram					= rParent.mPhn2Ngram;
		mPhonemesCnt				= rParent.mPhonemesCnt;
		mUniqSize					= rParent.mUniqSize;
		mReturnAllSuccessfulTokens 	= rParent.mReturnAllSuccessfulTokens;
		mOverlapping			 	= rParent.mOverlapping;
		mTermId						= rParent.mTermId;
	}
	void CopyParams(QueryKwdList* pParent) { CopyParams(*pParent); }
		
	
	bool contains(ID_t W_id);
	bool contains(std::string W);
	QueryKwdList::iterator find(std::string W);
	int getKwdIndex(ID_t W_id);
	int getKwdIndex(QueryKwdList::const_iterator iKwd);
	void print();
	void push_back(QueryKwd &rQueryKwd);

	/**
	 * @brief Get the count of unique keywords in query
	 *
	 * @return unique keywords count
	 */
	int GetUniqSize() { return mUniqSize; }
};

} // namespace lse

#endif
