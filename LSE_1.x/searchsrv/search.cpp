#include <algorithm>
#include "search.h"

using namespace std;
using namespace lse;


void Search::ExpandPhonemes(const QueryKwdList* src, QueryKwdList* dest)
{
	

	int 			length = 3;
	float			nb_prev = 1;
	float			nb_next = 1;
	HistContainer	hist;

	dest->mUniqSize = 0;

	for (QueryKwdList::const_iterator iKwd = src->begin(); iKwd != src->end(); ++iKwd)
	{
		HistRecord histRec;
		histRec.W_id = iKwd->W_id;
		histRec.W	 = iKwd->W;
		hist.push_back(histRec);

		if ((int)hist.size() == length)
		{
			QueryKwd kwd;
			kwd.W 						= "";
			for (HistContainer::iterator iHistRec=hist.begin(); iHistRec!=hist.end(); ++iHistRec)
			{
				kwd.phonemes.push_back(iHistRec->W_id);
				kwd.W 				   += (kwd.W=="" ? "" : "_") + iHistRec->W;
			}
			kwd.W_id 					= mpLexicon->word2id_readonly(kwd.W);
			kwd.nb_prev 				= nb_prev;
			kwd.nb_next					= nb_next;
			kwd.isNextInPhrase			= false;
			kwd.confidence_min_isset	= false;
			kwd.confidence_max_isset	= false;
			kwd.time_min				= iKwd->time_min;
			kwd.time_max				= iKwd->time_max;
			kwd.time_min_type			= iKwd->time_min_type;
			kwd.time_max_type			= iKwd->time_max_type;
			kwd.lessThan				= false;
			kwd.greaterThan				= false;
			kwd.mIsNgram				= true;
			
			dest->push_back(kwd);
			hist.pop_front();
		}
	}

	if (dest->size() > 0)
	{
		(dest->begin())->nb_prev = -1;
		(--dest->end())->nb_next = -1;
	}
}

QueryKwdList::iterator Search::getBestKwd()
{
	// select the keyword with the minimal number of clusters = the most discriminative keyword
	QueryKwdList::iterator iBestKwd = queryKwdList->begin();
	for (QueryKwdList::iterator iQueryKwd = queryKwdList->begin(); iQueryKwd != queryKwdList->end(); ++iQueryKwd) 
	{
		if (iQueryKwd->searchResults.size() < iBestKwd->searchResults.size())
		{
			iBestKwd = iQueryKwd;
		}
	}
	return iBestKwd;
}

void Search::fillClustersGroupFwd_recursive(SearchClusterGroup *group, 
											QueryKwd::RevIdxRecord* curRevIdxRecord, 
											QueryKwdList::iterator curKwd)
{
	// FORWARD
	DBG_SEARCH("Search::fillClustersGroupFwd_recursive()");
	QueryKwdList::iterator iFwdKwd = curKwd;
	iFwdKwd++;

	if (curRevIdxRecord->mFwdClusters.size() == 0)
	{
		group->mEndTime = curRevIdxRecord->t;
	}
	
	// for all forward clusters of the current cluster
	for (QueryKwd::RevIdxRecordList::iterator iRevRec = curRevIdxRecord->mFwdClusters.begin();
		 iRevRec != curRevIdxRecord->mFwdClusters.end();
		 ++iRevRec)
	{
		// We need to check if the current cluster is not already in the keyword's cluster list
		// because the current cluster (iRevRec) could have been already added to the group
		// for the forward keyword (iFwdKwd) 
		// (it may happen because of a longer chain of keywords in the query, 
		// so that i.e. the previous keyword has 2 clusters, which are not overlapped,
		// but they have the same forward clusters associated)
		if (!group->containsCluster(iFwdKwd, (**iRevRec))) 
		{
			group->Insert(iFwdKwd, *iRevRec);
		} 
		// let's dive one more step into recursion
		if (iFwdKwd != --(this->queryKwdList->end()))
		{
			fillClustersGroupFwd_recursive(group, *iRevRec, iFwdKwd);
		}
	}
	DBG_SEARCH("Search::fillClustersGroupFwd_recursive()...done");
}


void Search::fillClustersGroupBck_recursive(SearchClusterGroup *group, 
											QueryKwd::RevIdxRecord* curRevIdxRecord, 
											QueryKwdList::iterator curKwd)
{
	// BACKARD
	QueryKwdList::iterator iBckKwd = curKwd;
	iBckKwd--;

	if (curRevIdxRecord->mBckClusters.size() == 0)
	{
		group->mStartTime = curRevIdxRecord->tStart;
	}
		
	// for all backward clusters of the current cluster
	for (QueryKwd::RevIdxRecordList::iterator iRevRec = curRevIdxRecord->mBckClusters.begin();
		 iRevRec != curRevIdxRecord->mBckClusters.end();
		 ++iRevRec)
	{
		// add the backward cluster to the group for the corresponding keyword
		// to see the reason of calling containsCluster see the method fillClustersGroupFwd_recursive
		if (!group->containsCluster(iBckKwd, (**iRevRec)))
		{
			group->Insert(iBckKwd, *iRevRec);
		}
		// if we are on the first kwd, the recursion is terminated
		if (iBckKwd != this->queryKwdList->begin())
		{
			fillClustersGroupBck_recursive(group, *iRevRec, iBckKwd);
		}
	}
}
		


void Search::FillClusterGroups()
{
	DBG_SEARCH("Search::fillClusterGroups()");
	QueryKwdList::iterator iBestKwd = this->getBestKwd();
	clusterGroups.iBestKwd = iBestKwd;
	DBG_SEARCH("  iBestKwd:"<< iBestKwd->W_id);
	
	// for all clusters of iBestKwd
	for (vector<QueryKwd::RevIdxRecord>::iterator iBestKwdRec = iBestKwd->searchResults.mvByConfidence.begin();
		 iBestKwdRec != iBestKwd->searchResults.mvByConfidence.end();
		 iBestKwdRec ++)
	{
		SearchClusterGroup tmpGroup;
		// add the current cluster into the tmpGroup
		tmpGroup.Insert(iBestKwd->W_id, &(*iBestKwdRec));

		fillClustersGroupFwd_recursive(&tmpGroup, &(*iBestKwdRec), iBestKwd);
		fillClustersGroupBck_recursive(&tmpGroup, &(*iBestKwdRec), iBestKwd);

		// add the group to the list of cluster groups
		this->clusterGroups.push_back(tmpGroup);
	}
	DBG_SEARCH("Search::fillClusterGroups()...done");
}

void Search::PrintClusterGroups()
{
	int groupCounter = 0;
	// each group
	for (SearchClusterGroups::iterator iGroup=this->clusterGroups.begin(); iGroup!=this->clusterGroups.end(); ++iGroup)
	{
		CERR("--------------------------------------------------------------------------------");
		CERR("ClusterGroup #" << ++groupCounter << " " << endl << *iGroup);
	}
	CERR("--------------------------------------------------------------------------------");
	CERR("Search::PrintClusterGroups()...done");
}


void Search::addToHypothesis(ID_t curNodeID, LatTime tStart, float conf, Hypothesis *hyp, EFrontBack frontback)
{
//	DBG_FORCE("Search::addToHypothesis()");
	if (!mpLexicon->isMetaWord(binlat->nodes[curNodeID].W_id)) 
	{
		Hypothesis::Word curHypWord;
		curHypWord.str = mpLexicon->id2word_readonly(binlat->nodes[curNodeID].W_id);
		curHypWord.start = tStart;
		curHypWord.end = binlat->nodes[curNodeID].t;
		curHypWord.conf = conf; // Likelihood of best link coming into current node
		if (frontback == back) {
			hyp->push_back_word(curHypWord);
		} else 
		if (frontback == front) {
			hyp->push_front_word(curHypWord);
		}
		if (queryKwdList->contains(binlat->nodes[curNodeID].W_id)) {
			if (frontback == front) {
				hyp->addKeywordFront(curHypWord);
			} else {
				hyp->addKeywordBack(curHypWord);
			}
		}
//		DBG_SEARCH("addToHypothesis() curNodeID:"<<curNodeID<<"\tW_id:"<<binlat->nodes[curNodeID].W_id<<"\tconf:"<<conf<<"\tword:"<<curHypWord.str);
	} else {
//		DBG_SEARCH("addToHypothesis() curNodeID:"<<curNodeID<<"\t W_id:"<<binlat->nodes[curNodeID].W_id<<"\tconf:"<<conf<<"\tword:<META>");
	}
//	DBG_FORCE("Search::addToHypothesis() ... done");
}
	

void Search::GetNodesContext(ID_t nodeID, DirectionType dir, Hypothesis *hyp)
{
	// *** BACKWARD LINKS ***
//	DBG_FORCE("Search::GetNodesContext("<<nodeID<<", "<<(dir==FWD?"FWD":"BCK")<<")");
	ID_t nextNodeID;
	ID_t curNodeID = nodeID;
	ID_t bestNodeID;
	TLatViterbiLikelihood bestLinkLikelihood;
	int count = 0;
	int count_max = (dir==FWD ? queryKwdList->fwd_nodes_count : queryKwdList->bck_nodes_count);
//	DBG_FORCE("count_max:"<<count_max);
	while (count < count_max) 
	{
//		DBG_FORCE("curNodeID:"<<curNodeID);
		if ((bestNodeID = binlat->nodeLinks.bestFollowingNode(curNodeID, dir, &bestLinkLikelihood)) == -1) { // if first node found (which has no backward links), then break the loop
//			DBG_FORCE("nodeID:"<<curNodeID<<" ...first node found -> backward processing termination");
			break;
		}

		nextNodeID = bestNodeID;
		if (!binlat->nodes.containsID(nextNodeID)) { // if we are going to jump out of read lattice
//			DBG_FORCE("nextNodeID:"<<nextNodeID<<" ...going to jump out of read lattice -> backward processing termination");
			break;
		}

//		DBG_FORCE("curNodeID:"<<curNodeID<<" nextNodeID:"<<nextNodeID);
		// add hypothesis (the first kwd on the best path has been added to the hypothesis already in rescoreHits())
		if (dir == BCK && count > 0) 
		{
			addToHypothesis(curNodeID, binlat->nodes[nextNodeID].t, bestLinkLikelihood, hyp, (dir==FWD ? back : front));
		}
		else if (dir == FWD)
		{
			addToHypothesis(curNodeID, binlat->nodes[curNodeID].tStart, bestLinkLikelihood, hyp, (dir==FWD ? back : front));
		}
		count++;
		curNodeID = nextNodeID; // go to the next node
	}
//	DBG_FORCE("Search::GetNodesContext() ... done");
}



//================================================================================
//	AddPathToHypothesis
//================================================================================
void Search::AddPathToHypothesis(WLR* pBestPath, Hypothesis* pHyp, ID_t* pFirstNodeID)
{
	if (pBestPath == NULL) {
		DBG_FORCE("!!! bestPath->nodeHistory.size() == 0 !!!");
		return;
	}
	
	if (pBestPath != NULL) *pFirstNodeID = pBestPath->mNodeID;
	LatBinFile::LatNodeLinksArray::Cursor cur(&(binlat->nodeLinks));
	WLR* iWlr = pBestPath;
	while (iWlr->mNext != NULL)
	{
//		DBG_SEARCH("================================================================================");
//		DBG_SEARCH("iNodeID:"<<iWlr->mNodeID);
//			tokenLattice[*iNodeID].tokenPathList.print();
		cur.gotoNodeID(iWlr->mNodeID, BCK);
		while (!cur.eol()) 
		{
			LatBinFile::NodeLinks::Link l = cur.getLink();
			if (l.nodeID == iWlr->mNext->mNodeID) 
			{
				addToHypothesis(iWlr->mNodeID, binlat->nodes[iWlr->mNext->mNodeID].t, l.conf, pHyp, front);
			}
			cur.next();
		}
		iWlr = iWlr->mNext;
		*pFirstNodeID = iWlr->mNodeID;
	}

	TLatViterbiLikelihood predLinkLikelihood;
	ID_t predNodeID = binlat->nodeLinks.bestFollowingNode(iWlr->mNodeID, BCK, &predLinkLikelihood); // if first node found (which has no backward links), then break the loop
	DBG_FORCE("predNodeID = "<<predNodeID<<"   conf = "<<predLinkLikelihood);
	
	// now we need to add the first node (with a proper start time)
	addToHypothesis(iWlr->mNodeID, binlat->nodes[iWlr->mNodeID].tStart, binlat->nodes[iWlr->mNodeID].conf, pHyp, front);
}

void Search::RescoreHits(SearchClusterGroup* pGroup, LatMeetingIndexerRecord meetingRec, Results *results) 
{
	
	DBG_FORCE("Search::rescoreHits()");

	// create token lattice
	TokenLattice tokenLattice(binlat, queryKwdList);
	
	// do the forward pass on binlat with queryKwdList and add all results into tokenLattice.resultPathList
	tokenLattice.ForwardPass(pGroup);

	DBG_FORCE("tokenLattice.resultPathList.size(): "<<tokenLattice.resultPathList.size());
	

	// if we have not found any result, return
	if (tokenLattice.resultPathList.size() != 0)
	{
		TokenPathList::iterator iTokenPath = tokenLattice.resultPathList.begin();

/*		PRINT RESULTS

		while(iTokenPath != tokenLattice.resultPathList.end())
		{
			DBG_FORCE("tokenLattice.resultPathList[]:"<<**iTokenPath);
			iTokenPath++;
		}
*/
		if (!queryKwdList->mReturnAllSuccessfulTokens)
		{
			iTokenPath = tokenLattice.resultPathList.getBestTokenPathIterator();
		}

		while(iTokenPath != tokenLattice.resultPathList.end())
		{
			Hypothesis hyp;
			hyp.stream = "LVCSR";
			hyp.keywordsCount = 0;
			// get record's channel
			hyp.channel = GetMeetingChannelFromMeetingName(meetingRec.path);
			// cut off additional info from record's name
			hyp.record = CutoffMeetingName(meetingRec.path);

//			DBG_FORCE("tokenLattice.resultPathList.print():");
//			tokenLattice.resultPathList.print();
			
			WLR* pTokenPath = *iTokenPath;
			if (queryKwdList->mReturnAllSuccessfulTokens)
			{
				iTokenPath++;
			}
			DBG("returned path:"<<*pTokenPath);

			
			ID_t path_first_node_id = -1; // will be set within AddPathToHypothesis()
			AddPathToHypothesis(pTokenPath, &hyp, &path_first_node_id); // add all words from the best path to hyp and set path_first_node_id
			hyp.score = pTokenPath->mConf; // set the score of the hypothesis as the sore of the best path
			// now start the backward links processing from the first node on the best path to get the context before it
			if (queryKwdList->bck_nodes_count > 0)
			{
			DBG("GetNodesContext("<<path_first_node_id<<", BCK)");
				GetNodesContext(path_first_node_id, BCK, &hyp);
			}

			// don't add empty results
			results->push_back(hyp);
//			DBG_FORCE("results->push_back(hyp): "<<hyp);
			// if we want to return only the best path, then it's done
			if (!queryKwdList->mReturnAllSuccessfulTokens)
			{
				break;
			}
		}
	}

	// LET'S FREE ALL WLRs
	DBG_SEARCH("DELETING WLRs (resultPathList.size(): "<<tokenLattice.resultPathList.size()<<")");
	while (tokenLattice.resultPathList.size() > 0)
	{
		tokenLattice.resultPathList.freePath(tokenLattice.resultPathList.begin());
	}
	tokenLattice.FreeTokens();
	
	DBG_SEARCH("Search::rescoreHits() done");
}


void Search::PrintCandidates(SearchClusterGroup* pGroup, LatMeetingIndexerRecord &rMeetingRec)
{
	cout << "\"" << rMeetingRec.path << "\"" << endl;
	cout << setprecision(13) << pGroup->GetStartTime() * 10000000 << " " << pGroup->GetEndTime() * 10000000;
	for (QueryKwdList::iterator iKwd=queryKwdList->begin(); iKwd!=queryKwdList->end(); ++iKwd)
	{
		cout << " " << iKwd->W;
	}
//				cout << " " << pGroup->conf;
	cout << endl;
	cout << "." << endl;
}

void Search::PrintCandidateTrigrams(SearchClusterGroup* pGroup, LatMeetingIndexerRecord &rMeetingRec)
{
	cout << "\"" << rMeetingRec.path << "\"" << endl;

	string phn_str = "";
	for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); ++iKwd)
	{
		phn_str += (phn_str=="" ? "" : "_") + iKwd->W;
	}

	cout << "@" << setprecision(13) << (int)(pGroup->mStartTime * 10000000) << " " << (int)(pGroup->mEndTime * 10000000) << " " << phn_str << endl;
	for (SearchClusterGroup::iterator iClusterList = pGroup->begin(); iClusterList != pGroup->end(); ++iClusterList)
	{
		string ngram = mpLexicon->id2word_readonly(iClusterList->first);

		for (QueryKwd::RevIdxRecordList::iterator iRevRec = iClusterList->second.begin(); iRevRec != iClusterList->second.end(); ++iRevRec)
		{
			double normconf = (*iRevRec)->conf / ((*iRevRec)->t * 100 - (*iRevRec)->tStart * 100);
			cout << setprecision(13) << (int)((*iRevRec)->tStart * 10000000) << " " << (int)((*iRevRec)->t * 10000000) << " " << ngram << " " << normconf << endl;
		}
	}
	cout << "." << endl;
}


string Search::GetMeetingChannelFromMeetingName(const string &meetingName)
{
	// get record's channel
	if (mRecordChannelColumn > 0)
	{
		string::size_type pos = 0;
		for (int field_idx=1; field_idx < mRecordChannelColumn; field_idx++) {
			pos = meetingName.find (mRecordDelim, pos+1); // find first _ in latname
			DBG("pos:"<<pos);
		}
		if (pos != string::npos) {
			string::size_type pos_2nd = meetingName.find (mRecordDelim, pos+1); // find second _ in latname
			if (pos_2nd == string::npos)
			{
				pos_2nd = meetingName.find ('.', pos+1);
			}
			return meetingName.substr(pos+1, pos_2nd - pos - 1);
		}
	}
	return "";
}


string Search::CutoffMeetingName(const std::string &meetingName)
{
	// cut off additional info from record's name
	if (mRecordNameCutoffColumn > 0)
	{
		string::size_type pos = 0;
		for (int field_idx=1; field_idx < mRecordNameCutoffColumn; field_idx++) {
			pos = meetingName.find (mRecordDelim, pos+1); // find first _ in latname
			DBG("pos:"<<pos);
		}
		if (pos != string::npos) {
			return meetingName.substr(0, pos);
		} else {
			CERR("ERROR in record's name: Expecting '"<<mRecordDelim<<"' in '" << meetingName << "' ...using full record's path");
		}
	}
	return "";
}


