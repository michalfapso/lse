#include "querykwdlist.h"

using namespace std;
using namespace lse;


	
void QueryKwdList::push_back(QueryKwd &rQueryKwd)
{
	DBG_FORCE("QueryKwdList::push_back("<<rQueryKwd<<")");
	if (rQueryKwd.W.find('_') != string::npos) 
	{ 
		rQueryKwd.mIsNgram = true;
		DBG_FORCE("mNgram2Phn="<<mNgram2Phn);
		if (mNgram2Phn)
		{
			DBG_FORCE("StringSplit()");
			vector<string> phonemes_str;
			string_split(rQueryKwd.W, "_", phonemes_str);
			for (vector<string>::iterator i=phonemes_str.begin(); i!=phonemes_str.end(); ++i)
			{
				rQueryKwd.phonemes.push_back(mpLexicon->word2id_readonly(*i));
				mPhonemesCnt++;
			}
			DBG_FORCE("StringSplit()...done " << rQueryKwd.phonemes.size());
		}
	}

	bool found = false;
	for (QueryKwdList::iterator iKwd=begin(); iKwd!=end(); ++iKwd)
	{
		if (iKwd->W == rQueryKwd.W)
		{
			found = true;
		}
	}
	if (!found)
	{
		mUniqSize++;
	}
	((vector< QueryKwd >*)this)->push_back(rQueryKwd);
}

int QueryKwdList::getKwdIndex(ID_t W_id)
{
	int kwdIndex = 0;
	for (QueryKwdList::iterator iKwd = this->begin(); iKwd != this->end(); ++iKwd)
	{
		if (iKwd->W_id == W_id) {
			return kwdIndex;
		}
		kwdIndex++;
	}
	kwdIndex = -1; // not found
	return kwdIndex;
}

int QueryKwdList::getKwdIndex(QueryKwdList::const_iterator iKwd)
{
	int kwdIndex = 0;
	for (QueryKwdList::iterator i = this->begin(); i != this->end(); ++i)
	{
		if (iKwd == i) {
			return kwdIndex;
		}
		kwdIndex++;
	}
	kwdIndex = -1; // not found
	return kwdIndex;
}

bool QueryKwdList::contains(ID_t W_id)
{
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
	{
		if (i->W_id == W_id)
		{
			return true;
		}
		else if (mNgram2Phn)
		{
			for (vector<ID_t>::iterator iPhn=i->phonemes.begin(); iPhn!=i->phonemes.end(); ++iPhn)
			{
				if (*iPhn == W_id)
				{
					return true;
				}
			}
		}
	}
	return false;
}

bool QueryKwdList::contains(std::string W) 
{
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
		if (i->W == W)
			return true;
	return false;
}

QueryKwdList::iterator QueryKwdList::find(std::string W) 
{
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
		if (i->W == W)
			return i;
	return this->end();
}

std::ostream& lse::operator<<(std::ostream& os, const QueryKwd& kwd) 
{
	char buffer[20];
	sprintf(buffer, "%.2f", kwd.time_min);
	std::string time_min_str = kwd.time_min_type == null ? "-inf" : buffer;
	sprintf(buffer, "%.2f", kwd.time_max);
	std::string time_max_str = kwd.time_max_type == null ? "inf" : buffer;

	if (kwd.time_min_type == lse::time) 
		time_min_str += "s";
	else if (kwd.time_min_type == lse::percents)
		time_min_str += " * 100%";

	if (kwd.time_max_type == lse::time) 
		time_max_str += "s";
	else if (kwd.time_max_type == lse::percents)
		time_max_str += " * 100%";
	
	os << kwd.W << ":" << kwd.W_id 
	   << "\tnb_prev:" << kwd.nb_prev 
	   << "\tnb_next:" << kwd.nb_next 
	   << (kwd.isNextInPhrase ? "\tnextKwdInPhrase" : "")
	   << "\tconf:<" << (kwd.confidence_min_isset ? kwd.confidence_min : -INF) << ".." << (kwd.confidence_max_isset ? kwd.confidence_max : INF) <<">"
	   << "\ttime:<" << time_min_str << ".." << time_max_str << ">"
	   << (kwd.lessThan ? "\tlessThan" : "")
	   << (kwd.greaterThan ? "\tgreaterThan" : "");

	os << " phonemes:"<<kwd.phonemes.size() << " ";
	for (vector<ID_t>::const_iterator iPhn=kwd.phonemes.begin(); iPhn!=kwd.phonemes.end(); ++iPhn)
	{
		os << *iPhn << " ";
	}

	return os;
}

void QueryKwdList::print()
{
	DBG_FORCE("--------------------------------------------------");
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
	{
		DBG_FORCE(*i);
	}
	DBG_FORCE("mUniqSize:"<<mUniqSize);
	DBG_FORCE("--------------------------------------------------");
}

void QueryKwd::RevIdxRecords::push_back(const LatIndexer::Record &rec)
{
	QueryKwd::RevIdxRecord recRevIdxRecord;
	recRevIdxRecord.wordID 		= rec.wordID;
	recRevIdxRecord.meetingID 	= rec.meetingID;
	recRevIdxRecord.nodeID 		= rec.nodeID;
	recRevIdxRecord.conf 		= rec.conf;		// confidence
	recRevIdxRecord.tStart 		= rec.tStart;	// word's start time
	recRevIdxRecord.t 			= rec.t;		// word's end time
	mvByConfidence.push_back(recRevIdxRecord);
}

unsigned int QueryKwd::RevIdxRecords::size()
{
	return mvByConfidence.size();
}
