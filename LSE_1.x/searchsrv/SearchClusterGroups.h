#ifndef SEARCHCLUSTERGROUPS_H
#define SEARCHCLUSTERGROUPS_H

#include <vector>
#include <map>
#include "lattypes.h"
#include "querykwdlist.h"


namespace lse {

//================================================================================
//	SearchClusterGroup
//================================================================================
// kwd.W_id -> list of Clusters
class SearchClusterGroup : public std::map<ID_t, QueryKwd::RevIdxRecordList>
{
	public:
		ID_t					mMeetingID;
		TLatViterbiLikelihood 	conf; // min confidence of all clusters
		int 					validKeywordsCount;				
		LatTime					mStartTime;
		LatTime					mEndTime;

		SearchClusterGroup() : mMeetingID(-1), conf(0), validKeywordsCount(0), mStartTime(INF), mEndTime(-INF) {}

		friend std::ostream& operator<<(std::ostream& os, const SearchClusterGroup& g) 
		{
			os << "meetingID:"<<g.mMeetingID<<" conf:"<<g.conf<<" kwds:"<<g.validKeywordsCount<<" t["<<g.mStartTime<<" .. "<<g.mEndTime<<"]" << std::endl;
			
			// each keyword (group[keyword])
			for (SearchClusterGroup::const_iterator iGroupItem=g.begin(); iGroupItem!=g.end(); ++iGroupItem)
			{
				os << "  kwd: " << iGroupItem->first << std::endl;
				// each cluster (group[keyword]->clusters)
				for (QueryKwd::RevIdxRecordList::const_iterator iRevRec=iGroupItem->second.begin(); iRevRec!=iGroupItem->second.end(); ++iRevRec)
				{
					os << "     [" << (*iRevRec)->meetingID << "] " 
					   << (*iRevRec)->tStart << " ... " << (*iRevRec)->t
					   << " (conf:" << (*iRevRec)->conf << ")"
					   << std::endl;
				}
			}
			return os;
		}
		
		void Insert(ID_t W_id, QueryKwd::RevIdxRecord* pRevIdxRecord);
		void Insert(QueryKwdList::iterator iKwd, QueryKwd::RevIdxRecord* pRevIdxRecord) { Insert(iKwd->W_id, pRevIdxRecord); }
		bool containsCluster(QueryKwdList::iterator iKwd, const QueryKwd::RevIdxRecord &cmpRevIdxRecord);
		LatTime GetStartTime() { return mStartTime; }
		LatTime GetEndTime()   { return mEndTime; }
};

//================================================================================
//	SearchClusterGroups
//================================================================================
// list of "Kwd->Clusters" groups
class SearchClusterGroups : public std::vector< SearchClusterGroup > 
{
	public:
		QueryKwdList::iterator iBestKwd; ///< the keyword with the least number of hits in the reverse index
		
		static bool cmp_SearchClusterGroup(const SearchClusterGroup& lv, const SearchClusterGroup& rv);
		static bool cmp_PRevIdxRecord(const QueryKwd::RevIdxRecord* lv, const QueryKwd::RevIdxRecord* rv);
		void sortGroups();
};


} // namespace lse

#endif
