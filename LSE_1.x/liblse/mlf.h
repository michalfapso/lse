#ifndef MLF_H
#define MLF_H

#include <vector>
#include <fstream>
#include "lattypes.h"
#include "latmeetingindexer.h"

namespace lse {

class MlfRec
{
public:
//	ID_t		 	mMeetingID;
	std::string		mMeetingPath;
	long double		mStartT;
	long double		mEndT;
//	LatTime			mStartT;
//	LatTime 		mEndT;
	std::string		mWord;
	float			mConf;

	void Reset() 
	{
		mMeetingPath= "";
		mStartT		= 0;
		mEndT		= 0;
		mWord		= "";
		mConf		= 0;
	}		
	
	friend std::ostream& operator<<(std::ostream& os, const MlfRec &r) {
		return os << "meeting:"<<r.mMeetingPath<<" "<<r.mStartT<<" "<<r.mEndT<<" "<<r.mWord<<" "<<r.mConf;
	}
};

typedef std::vector< MlfRec > MlfParentType;

class Mlf : public MlfParentType
{
	
	std::ifstream mFile;
	
	bool IsEol(const char c);
	bool IsDelimiter(const char c);
	
public:
	LatMeetingIndexer mMeetingIndexer;

	Mlf(const std::string filename = "") 
	{ 
		if (filename != "") 
		{
			Open(filename);
		}
	}

	void Open(const std::string filename);
	void Close();
	void Load();
	bool Next(MlfRec *rec);
	void Save(const std::string filename);
	void AddRecord(MlfRec &rRec);
};

}

#endif
