#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

enum Rights {
	r,
	w,
	x,
	none
};

bool file_exists(const char *filename, Rights rights = none);
long file_size(const char *filename);
char* itoa(int input, char *str, int radix /*not implemented yet - using 10*/);
void upcase(std::string s);
size_t strlcpy(char *dst, const char *src, size_t siz);
char* chomp(char *str, int len = -1);
void string_split(const std::string& str, const std::string& delimiters, std::vector<std::string>& tokens );
bool is_overlapping(double t1Start, double t1End, double t2Start, double t2End);
std::string CheckBackslash( std::string InputStr );
std::string DeleteBackslash( std::string InputStr );
std::string recode(std::string str);

template<class T>
void swap(T* elements, int i, int j)
{	T temp = elements[i];
	elements[i] = elements[j];
	elements[j] = temp;
}

template<class T>
void QuickSort
(	T * elements,
	unsigned int first, 
	unsigned int last
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		unsigned lastLow = first;
		for (unsigned i = first + 1; i <= last; i++)
			if(elements[i] < t)
			{	
				++lastLow;
				swap(elements, lastLow, i);
			}
		swap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last);
	}
}


template<class T>
void QuickSort
(	T * elements,
	unsigned int first, 
	unsigned int last,
	bool (*fCmpLess)(const T &a, const T &b)
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		unsigned lastLow = first;
		for (unsigned i = first + 1; i <= last; i++)
			if((*fCmpLess)(elements[i], t))
			{	
				++lastLow;
				swap(elements, lastLow, i);
			}
		swap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1, fCmpLess);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last, fCmpLess);
	}
}


/*
template <class itemType, class indexType>
void QuickSort(itemType arr[], indexType beg, indexType end) {
    if (end > beg + 1) {
        indexType l = beg + 1, r = end;
		itemType piv = arr[beg];
        while (l < r) {
            if (arr[l] > piv) 
                std::swap(arr[l], arr[--r]);
            else 
                l++;
        }
		std::swap(arr[--l], arr[beg]);
        QuickSort(arr, beg, r);
        QuickSort(arr, l, end);
    }
}
*/


/*
template <class itemType, class indexType>
void QuickSort(itemType a[], indexType l, indexType r)
{
  static itemType m;
  static indexType j;
  indexType i;

  if(r > l) {
    m = a[r]; i = l-1; j = r;
    for(;;) {
      while(a[++i] < m);
      while(a[--j] > m);
      if(i >= j) break;
      std::swap(a[i], a[j]);
    }
    std::swap(a[i],a[r]);
    QuickSort(a,l,i-1);
    QuickSort(a,i+1,r);
  }
}
*/

#endif
