#include "normlexiconfile.h"

using namespace std;
using namespace lse;

NormLexiconFile::NormLexiconFile()
{
	Init();
}

NormLexiconFile::NormLexiconFile(const string &filename)
{
	Init();
	Open(filename);
}



bool NormLexiconFile::IsEol(const char c)
{
	return (c == '\n');
}

bool NormLexiconFile::IsDelimiter(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}


bool NormLexiconFile::Next(NormLexiconFileRec *rec)
{
	static char c 	= 0;
	string 	s		= "";
	
	rec->Reset();

	while(!mFile.eof())
	{
		if (mEpsilon) 
		{
			mEpsilon = false;
		}
		else
		{
			mFile.get(c);
		}
//		DBG_FORCE("mState:"<<mState<<" c:"<<c);

		switch (mState)
		{
			case 0: // read word
					if (IsDelimiter(c))
					{
						rec->mWord = s;
						s = "";
						mState = 1;
					}
					else
					{
						// ignore the escape-slash
						if (c != '\\')
						{
							s += c;
						}
					}
				break;

			case 1: // skip whitespace
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 2;
					}
				break;

			case 2: // read normalization constant
					if (IsEol(c))
					{
						rec->mNormConst = atof(s.c_str());
						s = "";
						mState = 0;
						return true;
					}
					else
					{
						s += c;
					}
				break;

		} // switch(mState)
	} // while !eof

	return false;
}


void NormLexiconFile::Open(const string &filename)
{
	mFile.open(filename.c_str());

	if (!mFile.good()) {
		CERR("ERROR: opening file " << filename);
		exit (1);
	}
}

void NormLexiconFile::Close()
{
	mFile.close();
}
