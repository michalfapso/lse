#include "ngram.h"
#include <math.h>

using namespace lse;
using namespace std;

Ngram::Ngram()
{
	mpItems = NULL;
}

Ngram::Ngram(int n)
{
	mN = n;
	mTStart = INFINITY;
	mTEnd = 0.0;
	mpItems = new ID_t[mN];
}

Ngram::Ngram(const Ngram &ngram_parent)
{
	CopyParamsFrom(ngram_parent);
}


void Ngram::CopyParamsFrom(const Ngram &ngram_parent)
{
	mN = ngram_parent.mN;
	mTStart = ngram_parent.mTStart;
	mTEnd = ngram_parent.mTEnd;
//	DBG_FORCE("creating mpItems");
	if (mpItems == NULL)
	{
		mpItems = new ID_t[mN];
	}
//	DBG_FORCE("copying mpItems ... "<<ngram_parent.mpItems<<" -> "<<mpItems );
	memcpy(mpItems, ngram_parent.mpItems, sizeof(ID_t)*mN);
//	DBG_FORCE("copying mpItems done");
}

Ngram::~Ngram()
{
	delete[] mpItems;
}



void Ngram::SetItem(int n, LatTime tStart, LatTime tEnd, ID_t W_id)
{
	assert(n < mN);
	mpItems[n] = W_id;
	if (mTStart > tStart) 
	{
		mTStart = tStart;
	}
	if (mTEnd < tEnd)
	{
		mTEnd = tEnd;
	}
}
