#ifndef NORMLEXICONFILE_H
#define NORMLEXICONFILE_H

#include <fstream>
#include "lattypes.h"

namespace lse {

//--------------------------------------------------
//	DctFileRec
//--------------------------------------------------
class NormLexiconFileRec
{
public:
	std::string mWord;
	float mNormConst; ///< pronounciation

	void Reset()
	{
		mWord = "";
		mNormConst = 0.0;
	}
};


//--------------------------------------------------
//	DctFile
//--------------------------------------------------
class NormLexiconFile
{
	std::ifstream 	mFile;
	int				mState;
	bool 			mEpsilon;

	bool IsEol(const char c);
	bool IsDelimiter(const char c);

	void Init()
	{
		mState = 0;
		mEpsilon = false;
	}

public:

	NormLexiconFile();
	NormLexiconFile(const std::string &filename);

	void Open(const std::string &filename);
	bool Next(NormLexiconFileRec *rec);
	void Close();
};

} // namespace lse

#endif
