#ifndef NGRAM_H
#define NGRAM_H

#include "lattypes.h"

namespace lse {

class Ngram 
{
	int mN;
	LatTime mTStart;
	LatTime mTEnd;
	ID_t *mpItems;

public:

	/**
	 * @brief Ngram constructor - empty method - should be followed by a copy constructor
	 */
	Ngram();

	/**
	 * @brief Ngram constructor
	 *
	 * @param n 1..unigram, 2..bigram, 3..trigram, etc.
	 */
	Ngram(int n);

	/**
	 * @brief Ngram copy constructor
	 *
	 * @param &ngram_parent Ngram instance from which to copy
	 */
	Ngram(const Ngram &ngram_parent);

	void CopyParamsFrom(const Ngram &ngram_parent);
//	void CopyParamsFrom(const Ngram* p_ngram_parent) { CopyParamsFrom(*p_ngram_parent); }

	/**
	 * @brief Ngram destructor
	 */
	~Ngram();




	/**
	 * @brief set n-th item (indexing from 0) of ngram
	 *
	 * @param n item's index
	 * @param item what to write on n-th position of ngram
	 */
	void SetItem(int n, LatTime tStart, LatTime tEnd, ID_t W_id);

	inline void SetStartTime(LatTime t) { mTStart = t; }
	inline void SetEndTime(LatTime t) { mTEnd = t; }

	/**
	 * @brief get a word id of n-th item (indexing from 0) of ngram
	 *
	 * @param n item's index
	 *
	 * @return word id
	 */
	inline ID_t GetItemWordId(int n) { return mpItems[n]; }

	inline LatTime GetStartTime() { return mTStart; }
	inline LatTime GetEndTime() { return mTEnd; }

};

} // namespace lse
#endif
