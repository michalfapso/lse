#include <iostream>
#include <iomanip>
#include <fstream>
#include <errno.h>
#include <math.h>
#include "languagemodel.h"
// #include "common.h"

using namespace std;
using namespace lse;


bool LanguageModelFile::IsEol(const char c)
{
	return (c == '\n');
}

bool LanguageModelFile::IsDelimiter(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

bool LanguageModelFile::ReadHeader()
{
	static char c 	= 0;

	string 	s		= "";

	while (! (mGzipped ? gzeof(mFileGz):(mFile.eof())) )
	{
		if (mEpsilon) 
		{
			mEpsilon = false;
		}
		else
		{
			if (mGzipped) {
				c = gzgetc(mFileGz);
			} else {
				mFile.get(c);
			}
		}
//		DBG_FORCE("mState:"<<mState<<" c:"<<c);

		switch (mState)
		{
			case 0: // header
					if (c == '\\')
					{
						mState = 1;
					}
				break;

			case 1: // \data
					if (c == '\\')
					{
						if (s == "data")
						{
							s = "";
							mState = 2;
						}
						else
						{
							CERR("ERROR: incorrect header format");
							exit(1);
						}
					}
					else
					{
						s += c;
					}
				break;

			case 2: 
					if (IsEol(c))
					{
						mState = 3;
					}
				break;

			case 3:
					if (IsDelimiter(c))
					{
						if (s == "ngram")
						{
							s = "";
							mState = 4;
						}
						else
						{
							CERR("ERROR: incorrect header format");
							exit(1);
						}
					}
					else
					{
						s += c;
					}
				break;

			case 4:
					if (c == '=')
					{
						if (atoi(s.c_str()) == ++mLastNgramIndex)
						{
							s = "";
							mState = 5;
						}
						else
						{
							CERR("ERROR: incorrect header format ("<<atoi(s.c_str())<<" != "<<mLastNgramIndex<<")");
							exit(1);
						}
					}
					else
					{
						s += c;
					}
				break;

			case 5:
					if (IsEol(c))
					{
						int count = atol(s.c_str());
						if (count > 0)
						{
							mNgramsCount.push_back(count);
						}
						s = "";
						mState = 7;
					}
					else if (!IsDelimiter(c))
					{
						s += c;
					}
				break;
/*
			case 6:
					if (IsEol(c))
					{
						mState = 7;
					}
				break;
*/
			case 7: // check the end of the header
					if (IsEol(c))
					{
						mLastNgramIndex = 0;
						mState = 8; // end of the header;
						return true;
					}
					else
					{
						mEpsilon = true;
						mState = 3;
					}
				break;

		} // switch (mState)
	} // while (! mlf_file.eof())
	return false;
}

bool LanguageModelFile::Next(LanguageModelRec *rec)
{
	static char c 	= 0;

	int	ngrams_read = 0;
	string 	s		= "";

	rec->Reset();
		
	while (! (mGzipped ? gzeof(mFileGz):(mFile.eof())) )
	{
		if (mEpsilon) 
		{
			mEpsilon = false;
		}
		else
		{
			if (mGzipped) {
				c = gzgetc(mFileGz);
			} else {
				mFile.get(c);
			}
		}
//		DBG_FORCE("mState:"<<mState<<" c:"<<c);

		switch (mState)
		{
			case 8:
					if (c == '\\')
					{
						mState = 9;
					}
				break;

			case 9: // go through \1-grams:\n
					if (IsEol(c))
					{
						if (s == "end\\")
						{
							return false;
						}
						mLastNgramIndex++;
//						DBG_FORCE("mLastNgramIndex:"<<mLastNgramIndex);

						s = "";
						mState = 90;
					}
					else
					{
						s += c;
					}
				break;

			case 90:
					if (IsEol(c))
					{
						mState = 8;
					}
					else if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 10;
					}
				break;

			case 10: // mP (probability)
					if (IsDelimiter(c))
					{
						rec->mP = strtold(s.c_str(),NULL);
						if (errno == ERANGE) {
							CERR("ERROR: strtod(\""<<s.c_str()<<"\") failed...errno=ERANGE");
							exit(1);
						}
						s = "";
						mState = 11;
					}
					else
					{
						s += c;
					}
				break;

			case 11: // jump over blank chars
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						ngrams_read = 0;
						mState = 12;
					}
				break;

			case 12: // mWords (ngrams)
					if (IsDelimiter(c))
					{
						rec->mWords.push_back(s);
						s = "";
						if (++ngrams_read >= mLastNgramIndex)
						{
//							DBG_FORCE("ngrams_read:"<<ngrams_read<<" mLastNgramIndex:"<<mLastNgramIndex);
							mEpsilon = true;
							mState = 13;
						}
					}
					else
					{
						s += c;
					}
				break;

			case 13: // check if the record is read or it is needed to read mBackoff yet
					if (IsEol(c))
					{
						mState = 90;
//						DBG_FORCE("RETURN TRUE");
						return true;
					}
					else if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 14;
					}
				break;

			case 14: // mBackoff
					if (IsDelimiter(c))
					{
						rec->mBackoff = strtold(s.c_str(),NULL);
						if (errno == ERANGE) {
							CERR("ERROR: strtod(\""<<s.c_str()<<"\") failed...errno=ERANGE");
							exit(1);
						}
						s = "";
						mEpsilon = true;
						mState = 15;
//						DBG_FORCE("RETURN TRUE");
						return true;
					}
					else
					{
						s += c;
					}
				break;

			case 15: // look for the end of record line
					if (IsEol(c))
					{
						mState = 90;
					}
				break;
			
		} // switch (mState)
	} // while (! mlf_file.eof())
	return false;
}


void LanguageModelFile::Open(const string filename)
{
	Init();

	if (filename.substr(filename.length()-3,3) == ".gz") 
	{
		mGzipped = true;
		mFileGz = gzopen(filename.c_str(), "rb");
		if (mFileGz == NULL) 
		{
			CERR("ERROR: opening file " << filename);
			exit(2);		
		}
	} 
	else 
	{
		mFile.open(filename.c_str()); // HTK Standard Lattice File
		if (!mFile.good()) 
		{
			CERR("ERROR: opening file " << filename);
			exit(2);
		}
	}

	ReadHeader();
}

void LanguageModelFile::Close()
{
	if (mGzipped)
	{
		gzclose(mFileGz);
	}
	else
	{
		mFile.close();
	}
}


/**
 * @brief Find a node by it's word id
 *
 * @param childrenLevel Unigrams...1, bigrams...2, ...
 * @param *parentNode Pointer to a parent node. If looking for an unigram, set to NULL.
 * @param wordID Id of the word you are looking for.
 *
 * @return Pointer to the place where the node was found or NULL if it was not.
 */
unsigned char* LanguageModel::SearchChildren(int childrenLevel, unsigned char *parentNode, ID_t wordID)
{
	DBG_FORCE("LanguageModel::SearchChildren("<<childrenLevel<<")");
	DBG_FORCE("parentNode:"<<hex<<(int)parentNode);

	Array *children_a = mpNodeArrays[childrenLevel-1];

	int left = 0;
	int right;
	int mid = 0;

	unsigned char *children;
	if (parentNode == NULL)
	{
		children = children_a->mpArray;
		right = children_a->mCount - 1;
	}
	else
	{
		NodeArray *parent_a = (NodeArray*)mpNodeArrays[childrenLevel-2];
		children = parent_a->GetNodeChildren(parentNode);
		right = parent_a->GetNodeChildrenCount(parentNode) - 1;
		DBG_FORCE("parent node: "<<(*parent_a)[parentNode]);
	}

	DBG_FORCE("left:"<<left<<" right:"<<right);


	DBG_FORCE("SEARCHING...");
	while (left <= right)
	{
        mid = (int)(floor((left+right)/2));
		unsigned char *p_mid = children + mid * children_a->GetItemSize();
		ID_t mid_word_id = children_a->GetNodeWordId(p_mid);
		DBG_FORCE("mid_word_id:"<<mid_word_id);
        if (wordID > mid_word_id)
		{
            left  = mid+1;
		}
        else if (wordID < mid_word_id)
		{
            right = mid-1;
		}
        else
		{
			DBG_FORCE("SEARCHING...done...FOUND: "<<(*children_a)[p_mid]);
            return p_mid;
		}
	}
	DBG_FORCE("SEARCHING...done...NOT FOUND");
	return NULL;
}

LanguageModel::Node LanguageModel::NodeArray::operator[](unsigned char *pNode)
{
	Node n;
	n.mP = *((mtP*)							pNode); pNode += sizeof(mtP);	// probability
	n.mWordId = *((ID_t*)					pNode); pNode += sizeof(ID_t);// word id
	n.mBackoff = *((mtBackoff*)				pNode); pNode += sizeof(mtBackoff);	// backoff
	n.mpChildren = *((unsigned char**)		pNode); pNode += sizeof(unsigned char*);// pointer to children
	n.mChildrenCount = *((mtChildrenCount*)	pNode); pNode += sizeof(int);	// children count
	return n;
}

LanguageModel::Node LanguageModel::Array::operator[](unsigned char *pNode)
{
	Node n;
	n.mP = *((mtP*)							pNode); pNode += sizeof(mtP);	// probability
	n.mWordId = *((ID_t*)					pNode); pNode += sizeof(ID_t);// word id
	n.mBackoff = 0;
	n.mpChildren = NULL;
	n.mChildrenCount = 0;
	return n;
}

void LanguageModel::NodeArray::Add(mtP p, ID_t wordId, mtBackoff b)
{
	DBG("NodeArray::Add("<<p<<", "<<wordId<<", "<<b<<")");
	*((mtP*)			mpEmptyPos) = p;		mpEmptyPos += sizeof(mtP);	// probability
	*((ID_t*)			mpEmptyPos)	= wordId;	mpEmptyPos += sizeof(ID_t);// word id
	*((mtBackoff*)		mpEmptyPos)	= b;		mpEmptyPos += sizeof(mtBackoff);	// backoff
	*((unsigned char**)	mpEmptyPos)	= NULL;		mpEmptyPos += sizeof(unsigned char*);// pointer to children
	*((int*)			mpEmptyPos) = 0;		mpEmptyPos += sizeof(int);	// children count
}

void LanguageModel::LeafArray::Add(mtP p, ID_t wordId)
{
//	DBG_FORCE("LeafArray::Add");
//	DBG_FORCE("mpArray="<<(int)mpArray);
//	DBG_FORCE("mpEmptyPos="<<(int)mpEmptyPos);
//	DBG_FORCE("mSize="<<mSize);
	*((mtP*)			mpEmptyPos) = p;		mpEmptyPos += sizeof(mtP);	// probability
	*((ID_t*)			mpEmptyPos)	= wordId;	mpEmptyPos += sizeof(ID_t);	// word id
}

LanguageModel::mtChildrenCount LanguageModel::NodeArray::GetNodeChildrenCount(unsigned char *pNode)
{
	return *( (mtChildrenCount*) (pNode + sizeof(mtP) + sizeof(ID_t) + sizeof(mtBackoff) + sizeof(unsigned char*)) );
}

unsigned char* LanguageModel::NodeArray::GetNodeChildren(unsigned char *pNode)
{
	return *( (unsigned char**) (pNode + sizeof(mtP) + sizeof(ID_t) + sizeof(mtBackoff)) );
}

LanguageModel::mtBackoff LanguageModel::NodeArray::GetNodeBackoff(unsigned char *pNode)
{
	return *( (mtBackoff*) (pNode + sizeof(mtP) + sizeof(ID_t)) );
}

LanguageModel::mtP LanguageModel::Array::GetNodeP(unsigned char *pNode)
{
	return *( (mtP*) (pNode) );
}

ID_t LanguageModel::Array::GetNodeWordId(unsigned char *pNode)
{
	return *( (ID_t*) (pNode + sizeof(mtP)) );
}

void LanguageModel::NodeArray::SetNodeChildrenCount(unsigned char *pNode, mtChildrenCount count)
{
	*( (mtChildrenCount*) (pNode + sizeof(mtP) + sizeof(ID_t) + sizeof(mtBackoff) + sizeof(unsigned char*)) ) = count;
}

void LanguageModel::NodeArray::SetNodeChildren(unsigned char *pNode, unsigned char *pChildren)
{
	*( (unsigned char**) (pNode + sizeof(mtP) + sizeof(ID_t) + sizeof(mtBackoff)) ) = pChildren;
}

void LanguageModel::Load(const string filename)
{
	string lexicon_filename = filename + ".lexicon.IDX";
	if (mLexicon.loadFromFile_readonly(lexicon_filename) != 0) {
		CERR("ERROR: Lexicon input file " << lexicon_filename << " not found");
		exit(1);
	}      

	LanguageModelFile lmFile(filename);

	mpNodeArrays = new LanguageModel::Array*[lmFile.mNgramsCount.size()];
	for (int i=0; i<(int)lmFile.mNgramsCount.size(); i++)
	{
		DBG_FORCE("creating mpNodeArrays["<<i<<"]");
		if (i < (int)lmFile.mNgramsCount.size()-1)
		{
			mpNodeArrays[i] = new NodeArray(lmFile.mNgramsCount[i]);
		}
		else
		{
			mpNodeArrays[i] = new LeafArray(lmFile.mNgramsCount[i]);
		}
	}

	LanguageModelRec rec;
	unsigned char *parent_node_pred = NULL;
	unsigned char *parent_node = NULL;
	int nodes_counter = 0, OldN = 0;
	int children_count = 0;
	int n = 0;
	while(lmFile.Next(&rec)) 
	{
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("rec: "<<rec);
  		n = rec.mWords.size(); // n of 'n-gram'
//		DBG_FORCE("n: "<<n);

		parent_node = NULL;
		for (int i=1; i<n; i++) // start searching from unigrams (i=1)
		{
			parent_node = SearchChildren(i, parent_node, mLexicon.word2id_readonly(rec.mWords[i-1]));
			if (parent_node == NULL)
			{
				CERR("ERROR: parent node "+rec.mWords[i-1]+" does not exist");
				exit(1);
			}
//			DBG_FORCE("PARENT_NODE: "<<(*((NodeArray*)mpNodeArrays[i-1]))[parent_node]);
		}

		if (parent_node != parent_node_pred)
		{
			if (parent_node_pred != NULL)
			{
				((NodeArray*)mpNodeArrays[n-2])->SetNodeChildrenCount(parent_node_pred, children_count);
				DBG("SetNodeChildrenCount: "<<(*((NodeArray*)mpNodeArrays[n-2]))[parent_node_pred]);
			}
			((NodeArray*)mpNodeArrays[n-2])->SetNodeChildren(parent_node, mpNodeArrays[n-1]->mpEmptyPos);
			children_count = 0;
			parent_node_pred = parent_node;
		}

		if (n < (int)lmFile.mNgramsCount.size())
		{
//			unsigned char *p_node = mpNodeArrays[n-1]->mpEmptyPos;
			((NodeArray*)mpNodeArrays[n-1])->Add(rec.mP, mLexicon.word2id_readonly(rec.mWords[n-1]), rec.mBackoff);
//			DBG_FORCE("NODE: "<<(*((NodeArray*)mpNodeArrays[n-1]))[p_node]);
		}
		else
		{
//			unsigned char *p_node = mpNodeArrays[n-1]->mpEmptyPos;
			((LeafArray*)mpNodeArrays[n-1])->Add(rec.mP, mLexicon.word2id_readonly(rec.mWords[n-1]));
//			DBG_FORCE("NODE: "<<(*((LeafArray*)mpNodeArrays[n-1]))[p_node]);
		}
		children_count++;


		nodes_counter++;

		if (OldN != n)
		{
			cerr << std::endl << "Processing " << n << "-gram" << std::endl << std::flush;
			OldN = n;       
		}
		if (nodes_counter % 10000 == 0)
		{
			cerr << "." << std::flush;
		}
		if (nodes_counter % 1000000 == 0)
		{
			cerr << std::endl << std::flush;
		}
                
 
//		if (nodes_counter % 1000 == 0) cerr << '.';
//		if (nodes_counter == lmFile.mNgramsCount[0]) cerr << endl << "unigrams processed" << endl;
		


//		DBG_FORCE("parent:"<<*parent_node);
//		DBG_FORCE("  add node:"<<node);
	}

	// check if it is needed to update the last parent node's children count
	DBG("rec: "<<rec);
	if (n > 1)
	{
		if (parent_node != NULL)
		{
			((NodeArray*)mpNodeArrays[n-2])->SetNodeChildrenCount(parent_node, children_count);
			DBG("SetNodeChildrenCount: "<<(*((NodeArray*)mpNodeArrays[n-2]))[parent_node]);
		}
	}
//	mLexicon.print();

	DBG_FORCE("nodes: "<<nodes_counter);
	cerr << endl;
}

float LanguageModel::GetWordNGramProb(std::string WordString)
{
        //mpRootNode = new LanguageModelNode;
        //LanguageModelRec rec;
        //LanguageModelNode node;

        vector<string> tokens;
        unsigned char *parent_node = NULL;
        unsigned char *previous_node;
	ID_t WordIndex;

        
        string_split(WordString, " ", tokens);
        int N = tokens.size();

        for (int i=0; i<N; i++){
          //std::cout << "Processing token " << i << " " << tokens[i] << std::endl;
          previous_node = parent_node;
	  WordIndex = mLexicon.word2id_readonly(tokens[i]);
	  if( WordIndex == -1 ){
            WordIndex = mLexicon.word2id_readonly("<unk>");
            if( WordIndex == -1 ){
	      std::cerr << "Word " << tokens[i] << " is not in dictionary and also <unk> in not in dictionary."<< std::endl << std::flush;
              exit(1);
            }
	  }
          parent_node = SearchChildren(i+1, parent_node, WordIndex );
          if( parent_node == NULL ){
            if( i<(N-1) ){
              std::cerr << "ERROR: parent node on level " << i << " (from " << N << " levels) does not exist." << std::endl;
              exit(1);
            }else{
              std::string BWordString;
              BWordString=tokens[1];
              for (int j=2; j<N; j++){
                BWordString=BWordString+" "+tokens[j];
              }//for
              //std::cout << "i=" << i << " B=" << ((NodeArray*)mpNodeArrays[i-1])->GetNodeBackoff(previous_node) << " NGRAM=" << BWordString << std::endl;
              //std::cout << "Backoffing " << BWordString << " | ";
              return (((NodeArray*)mpNodeArrays[i-1])->GetNodeBackoff(previous_node)+GetWordNGramProb(BWordString) );
            }//if
          }//if
        }//for
        //std::cout << "N=" << N << " P=" << mpNodeArrays[N-1]->GetNodeP(parent_node) << std::endl;
        return (mpNodeArrays[N-1]->GetNodeP(parent_node));
}

bool  LanguageModel::IsWordInDict(std::string Word)
{
  return ( mLexicon.word2id_readonly(Word) != -1 );
}

