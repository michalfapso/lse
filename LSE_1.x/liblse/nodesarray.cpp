#include "latbinfile.h"


using namespace std;
using namespace lse;

/*
ID_t NodesArray::bestFwdNode(ID_t nodeID, TLatViterbiLikelihood * bestLikelihood) {
	if(!(nodeID >= firstID && nodeID <= lastID)) {
		return -1;
	}
	
	DBG("nodesArray["<<nodeID<<"]: "<<(*this)[nodeID]);
	ID_t bestNodeID = (*this)[nodeID].bestFwdNodeID;
	if(!(bestNodeID >= firstID && bestNodeID <= lastID)) {
		return -1;
	}
	*bestLikelihood = (*this)[bestNodeID].p;
	return bestNodeID;
}

ID_t NodesArray::bestBckNode(ID_t nodeID, TLatViterbiLikelihood * bestLikelihood) {
	if(!(nodeID >= firstID && nodeID <= lastID)) {
		return -1;
	}

	DBG("nodesArray["<<nodeID<<"]: "<<(*this)[nodeID]);
	ID_t bestNodeID = (*this)[nodeID].bestBckNodeID;
	*bestLikelihood = (*this)[nodeID].p;
	return bestNodeID;
}
*/
int LatBinFile::NodesArray::getNodesFileItemsCount(const string filename) {
	
	long size;
	if ( (size=file_size(filename.c_str())) != -1 )	{
		return (int)(size / sizeof(LatBinFile::Node));
	} else {
		cerr << "Cannot determine the size of NodesArray file" << endl << flush;
		exit(1);
	}
}

int LatBinFile::NodesArray::load(const string filename, int firstID, int lastID) {
	
	
	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
		return (LAT_ERROR_IO);
	}


	long nodesCount;
	if ((nodesCount = file_size(filename.c_str())) == -1) {
		return (LAT_ERROR_IO);
	};
	DBG_FORCE("nodesCount:"<<nodesCount<<" sizeof(LatBinFile::Node):"<<sizeof(LatBinFile::Node));
	nodesCount /= sizeof(LatBinFile::Node);
	DBG_FORCE("nodesCount:"<<nodesCount);
	
	if (lastID == -1) {
		lastID = nodesCount - 1; // end node id
	}

	this->firstID = firstID;
	this->lastID = lastID;

	DBG_FORCE("NodesArray::load ... firstID:"<<firstID<<" lastID:"<<lastID);
//	long nodesArraySize = lastID - firstID + 1;//(getNodePositionInNodesFile(lastID) + nodeRecordWidth) - getNodePositionInNodesFile(firstID);
	// create nodes array in memory
	if (nodesArray != NULL) {
		delete[] nodesArray;
	}
	
	this->nodesCount = lastID - firstID + 1;
	nodesArray = new LatBinFile::Node[this->nodesCount];

	// seek to the start node
	in.seekg(getNodePositionInNodesFile(firstID), ios_base::beg);
	//DBG_FORCE("getNodePositionInNodesFile("<<firstID<<") = "<<getNodePositionInNodesFile(firstID));
	
	// read nodes from file to array ending with end node
	LatBinFile::Node tmpNode;
	for (int i=0; i<this->nodesCount; i++) {
		tmpNode.readFrom(in);
		nodesArray[i] = tmpNode;
//		DBG_FORCE("nodesArray["<<i<<"]: "<<tmpNode);
	}

	in.close();
	return 0;
}

LatBinFile::Node& LatBinFile::NodesArray::operator[](int id) 
{	
//	assert(id >= firstID && id <= lastID);
	DBG("NodesArray::operator[] id="<<id);
	if(!(id >= firstID && id <= lastID)) {
		cerr << "ERROR: NodesArray::operator[]: ID="<<id<<" is out of borders <"<<firstID<<","<<lastID<<">"<<endl<<flush;
		exit(1);
	};

	DBG("NodesArray::operator[] id - firstID = " << id << " - " << firstID << " = " << id - firstID);
	return nodesArray[id - firstID];
}


long LatBinFile::NodesArray::getNodePositionInNodesFile(ID_t nodeID) 
{
	return nodeID * sizeof(LatBinFile::Node);
}

bool LatBinFile::NodesArray::containsID(ID_t nodeID) 
{
	return (this->firstID <= nodeID && nodeID <= this->lastID);
/*	
	if (nodeID < this->firstID || nodeID > this->lastID) { // if we are going to jump out of read lattice
		return false;
	} else {
		return true;
	}
*/
}

void LatBinFile::NodesArray::free() 
{
	if (this->nodesArray != NULL) {
		delete[] this->nodesArray;
		this->nodesArray = NULL;
	}
	this->nodesCount = 0;
	this->firstID = 0;
	this->lastID = 0;
}
