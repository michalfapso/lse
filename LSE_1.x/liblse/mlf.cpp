#include <iostream>
#include <iomanip>
#include <fstream>
#include <errno.h>
#include "mlf.h"

using namespace std;
using namespace lse;

void Mlf::AddRecord(MlfRec &rRec)
{
	this->push_back(rRec);
}

bool Mlf::IsEol(const char c)
{
	return (c == '\n');
}

bool Mlf::IsDelimiter(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

bool Mlf::Next(MlfRec *rec)
{
	static string 	meeting_path = "";
	static int		state = 0;

	string 	s		= "";
	char 	c		= 0;
	bool 	epsilon = false;

	rec->Reset();
	if (meeting_path != "") 
	{
		rec->mMeetingPath = meeting_path;
	}
		
	while (! mFile.eof())
	{
		if (epsilon) 
		{
			epsilon = false;
		}
		else
		{
			mFile.get(c);
		}

//		DBG_FORCE("c:"<<c<<" state:"<<state);
		switch (state)
		{
			case 0: // filename - starting "
					if (c == '"')
					{
						state = 1;
					}
				break;

			case 1: // filename - ending "
					if (c == '"')
					{
						meeting_path = s;
						rec->mMeetingPath = s;
//						LatMeetingIndexerRecord meeting_rec;
//						meeting_rec.path = s;
//						rec.mMeetingID = mMeetingIndexer.insertRecord(&meeting_rec);
						s = "";
						state = 2;
					} 
					else
					{
						s += c;
					}
				break;

			case 2: // filename eol (end of line)
					if (IsEol(c))
					{
						state = 3;
					}
				break;

			case 3: // skip delimiters in front of the start time
					if (!IsDelimiter(c))
					{
						epsilon = true;
						state = 4;
					}
				break;

			case 4:	// record - start time
					if (c == '.')
					{
						state = 0;
					}
					else if (IsDelimiter(c))
					{
						rec->mStartT = strtold(s.c_str(),NULL);
						if (errno == ERANGE) {
							CERR("ERROR: strtod(\""<<s.c_str()<<"\") failed...errno=ERANGE");
							CERR("loaded records: "<< this->size());
							exit(1);
						}
						s = "";
						epsilon = true;
						state = 5;
					}
					else
					{
						s += c;
					}
				break;

			case 5: // skip delimiters between start time and end time
					if (!IsDelimiter(c))
					{
						epsilon = true;
						state = 6;
					}
					else if (IsEol(c))
					{
						state = 3;
						return true;
					}
				break;
				
			case 6: // record - end time
					if (IsDelimiter(c))
					{
						rec->mEndT = strtold(s.c_str(),NULL);
						if (errno == ERANGE) {
							CERR("ERROR: strtod(\""<<s.c_str()<<"\") failed...errno=ERANGE");
							CERR("loaded records: "<< this->size());
							exit(1);
						}
//						rec.mEndT = atof(s.c_str());
						s = "";
						epsilon = true;
						state = 7;
					}
					else
					{
						s += c;
					}
				break;

			case 7: // skip delimiters between end time and word
					if (!IsDelimiter(c))
					{
						epsilon = true;
						state = 8;
					}
					else if (IsEol(c))
					{
						state = 3;
						return true;
					}
				break;
				
			case 8: // record - word
					if (IsDelimiter(c))
					{
						rec->mWord = s;
						s = "";
						epsilon = true;
						state = 9;
					}
					else
					{
						s += c;
					}
				break;

			case 9: // skip delimiters between word and score
					if (!IsDelimiter(c))
					{
						epsilon = true;
						state = 10;
					}
					else if (IsEol(c))
					{
						state = 3;
						return true;
					}
				break;

			case 10: // record - confidence
					if (IsDelimiter(c))
					{
						rec->mConf = atof(s.c_str());
						s = "";
						epsilon = true;
						state = 11;
					}
					else
					{
						s += c;
					}
				break;

			case 11: // skip delimiters between score and end of line
					if (!IsDelimiter(c))
					{
						epsilon = true;
						state = 12;
					} 
					else if (IsEol(c))
					{
						state = 3;
						return true;
					}
				break;
			
		} // switch (state)
	} // while (! mlf_file.eof())
	return false;
}


void Mlf::Open(const string filename)
{
	mFile.open(filename.c_str());
	if (!mFile.good()) {
		cerr << "Error: opening file " << filename << endl;
		exit (1);
	}

	this->clear();

}

void Mlf::Close()
{
	mFile.close();
}

void Mlf::Load()
{
	MlfRec 	rec;
	while(Next(&rec)) {
		AddRecord(rec);
	}
}



void Mlf::Save(const string filename)
{
	ofstream mlf_file;

	mlf_file.open(filename.c_str());
	if (!mlf_file.good()) {
		cerr << "Error: opening file " << filename << endl;
		exit (1);
	}
	
	mlf_file << "#!MLF!#" << endl;

//	ID_t meeting_id_pred = -1;
	string meeting_pred = "";	
	for (Mlf::iterator i=begin(); i!=end(); ++i)
	{
//		if (i->mMeetingID != meeting_id_pred)
		if (i->mMeetingPath != meeting_pred)
		{
//			if (meeting_id_pred != -1)
			if (meeting_pred != "")
			{
				mlf_file << '.' << endl;
			}
			mlf_file << '"' << i->mMeetingPath << '"' << endl;
			meeting_pred = i->mMeetingPath;
//			mlf_file << '"' << mMeetingIndexer.getVal(i->mMeetingID).path << '"' << endl;
//			meeting_id_pred = i->mMeetingID;
		}
		mlf_file << setprecision(20) << i->mStartT << " " << i->mEndT << " " << i->mWord << " " << i->mConf << endl;
	}
//	if (meeting_id_pred != -1)
	if (meeting_pred != "")
	{
		mlf_file << '.' << endl;
	}
}

