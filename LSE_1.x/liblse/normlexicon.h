#ifndef NORMLEXICON_H
#define NORMLEXICON_H

#include "lexicon.h"
#include "lattypes.h"

namespace lse {

//--------------------------------------------------
//	NormLexicon
//--------------------------------------------------
class NormLexicon
{
private:
	//--------------------------------------------------
	//	NormIndexRecord
	//--------------------------------------------------
	class NormIndexRecord 
	{
	public:
		unsigned char mCount; ///< pronounciation variants count
		int mNormPos; ///< relative position of a list of pronounciation variants from the beginning of the pronounciation array
	};
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

private:

	Lexicon *mpWordLexicon;

	float 	*mpNormArray;
	int		mNormArraySize;

public:

	NormLexicon(Lexicon * pWordLexicon = NULL, std::string normLexiconFilename = "");

	bool IsActive();

	void SetWordLexicon(Lexicon * pWordLexicon);
	void LoadFromNormLexiconFile(std::string filename);

	float operator[] (const ID_t wordId);
	float GetWordNormConst(const std::string &word);
};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

} // namespace lse

#endif
