#ifndef LATMLFFILE_H
#define LATMLFFILE_H

#include <vector>
#include "lattypes.h"
#include "lat.h"
#include "latviterbifwbw.h"
#include "wordclusters.h"
#include "mymath.h"
#include <math.h>

namespace lse {


class LatMlfFile {
/*	
	class Node : public Lattice::Node {
	public:
		LatTime tStart;
		double likelihood;				// word's own likelihood
		double overlapped_likelihood;	// word's own likelihood plus aliquote likelihood of overlapping words
		
		Node& operator=(const Lattice::Node& r) {
			// Handle self-assignment:
//			if(this == &right) return *this;
			id = r.id;
			t = r.t;
			W_id = r.W_id;
			W = r.W;
			v = r.v;
			p = r.p;
			return *this;
	    }
		
		friend bool operator<(const LatMlfFile::Node& l, const LatMlfFile::Node& r) {
			if (l.overlapped_likelihood == r.overlapped_likelihood) {
				return l.likelihood > r.likelihood;
			} else
				return l.overlapped_likelihood > r.overlapped_likelihood; // move higher values to the front of the container
		}
	};
	
//	typedef std::vector< LatMlfFile::Node > Cluster;
	
	
	
	class WordClusters {
		public:

			class Cluster : public std::vector< LatMlfFile::Node > {
				public:	ID_t W_id; // word ID
			};
			
			typedef std::vector< Cluster > AllClusters;
			AllClusters allClusters;
			
			void add(LatMlfFile::Node node);
			void print();
			void sort();
	};

*/	
	
	lse::Lattice *lat;					// lattice
	lse::LatViterbiFwBw *vit;			// viterbi forward / backward
	std::vector<lse::ID_t> kwdList;		// keywords list
	WordClusters wClusters;						// sets of overlaying words
	float likelihood_treshold;			// used for words filtering
	float lmscale;
	bool compute_overlapped_words_likelihood;

	bool validWord(ID_t wordID);
	LatTime getMinStartTime(ID_t nodeID);
//	LatTime getNodeStartTime(ID_t nodeID, double *likelihood);
	
public:	
	
	

	
	LatMlfFile(lse::Lattice *lattice, lse::LatViterbiFwBw *viterbi, float p_likelihood_treshold, bool p_compute_overlapped_words_likelihood = false) {
		this->lat = lattice;
		this->vit = viterbi;
		this->likelihood_treshold = p_likelihood_treshold;
		this->compute_overlapped_words_likelihood = p_compute_overlapped_words_likelihood;
	}

	int loadKeywordList(std::string filename);
	int save(const std::string filename, const std::string latName);
		
};

} // namespace lse



#endif
