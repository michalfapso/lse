#include <unistd.h>
#include <sys/stat.h>
#include "common.h"
#include "lattypes.h"
#include <iostream>
#include <algorithm>
#include <string>
#include <cctype>
#include <errno.h>

using namespace std;

bool file_exists(const char *filename, Rights rights) {
	int mode = F_OK; // default == just check whether the file exists
	switch (rights) {
		case r: mode = R_OK; break;
		case w: mode = W_OK; break;
		case x: mode = X_OK; break;
		case none: mode = F_OK; break;
	}
	return access(filename, mode) == 0; // return true if rights are OK / file exists
}


long file_size(const char *filename) {
	struct stat results;
    if (stat(filename, &results) == 0) {
		return results.st_size;
	} else {
		return -1;
	}
}



char* itoa(int input, char *str, int radix /*not implemented yet - using 10*/){
//    static char buffer[16];
    snprintf(str,sizeof(str),"%d",input);
	cout << "itoa("<<input<<", "<<str<<")" << endl;
    return str;
}

void upcase(string s) {
	// uppercase all characters
	for(string::iterator ch = s.begin(); ch != s.end(); ++ch) {
		*ch = toupper(*ch);
	}
/*
	transform (s.begin(), s.end(),    // source
               s.begin(),             // destination
               toupper);              // operation
*/
}


// used in sockets server implementation
size_t strlcpy(char *dst, const char *src, size_t siz)
{
	char *d = dst;
	const char *s = src;
	size_t n = siz;

	if (n != 0 && --n != 0) {
		do {
			if ((*d++ = *s++) == 0)
				break;
		} while (--n != 0);
	}

	if (n == 0) {
		if (siz != 0)
			*d = '\0';
		while (*s++)
			;
	}

	return(s - src - 1);
}

// remove the newline character(s) from the end of the given string str
char* chomp(char *str, int len) 
{
	if (len == -1) 	{
		len = strlen(str);
	}
	while (str[len-1] == 0x0D || str[len-1] == 0x0A)
	{
		len--;
		str[len] = 0x00; // replace newline with string-terminator
	}
	return str;
}


void string_split(const std::string& str, const std::string& delimiters, std::vector<std::string>& tokens )
{
     // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}

bool is_overlapping(double t1Start, double t1End, double t2Start, double t2End)
{
	return !(t1End < t2Start || t1Start > t2End);
/*
	return (t1Start < t2Start && t2Start < t1End) || 
		   (t1Start < t2End   && t2End   < t1End) ||
		   (t1Start < t2Start && t2End   < t1End);
*/
}

std::string CheckBackslash( std::string InputStr )
{
  unsigned int i=0;
  while(i<InputStr.npos){
    i=InputStr.find("\\",i);
    if(i<InputStr.npos){
      InputStr.insert(i,"\\");
      i=i+2;
    }
  }//while
  return InputStr;
}

std::string DeleteBackslash( std::string InputStr )
{
  unsigned int i=0;
  while(i<InputStr.npos){
    i=InputStr.find("\\",i);
    if(i<InputStr.npos){
      InputStr.erase(i,1);
    }
  }//while
  return InputStr;
}

std::string recode(std::string str)
{
	std::string::size_type pos;
	while ( (pos = str.find("\\")) != string::npos )
	{
		string nr_str = str.substr(pos, 3);
		char c = atol(nr_str.c_str());
		str.replace(pos, 1, 1, c);
		str.erase(pos+1, 3);
	}
	return str;
}

