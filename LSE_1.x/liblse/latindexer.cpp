#include "latindexer.h"
#include "common.h"

using namespace std;
using namespace lse;



//--------------------------------------------------------------------------------
//	LatIndexer::RecordClusters
//--------------------------------------------------------------------------------
void LatIndexer::RecordClusters::sort() 
{
	for (RecordClusters::iterator i=this->begin(); i!=this->end(); ++i) {
		std::sort(i->begin(), i->end());
	}
}


void LatIndexer::RecordClusters::add(LatIndexer::Record rec, bool checkOverlapping) 
{
	if (checkOverlapping)
	{
		// let's find the set for given rec
		for (RecordClusters::iterator iCluster=this->begin(); iCluster!=this->end(); ++iCluster) {
			// compare with all nodes in set
			if (iCluster->W_id == rec.wordID && iCluster->meetingID == rec.meetingID) {
				for (vector<LatIndexer::Record>::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
					//DBG_FORCE("if ("<<rec.t<<" >= "<<iWord->tStart<<" && "<<rec.t<<" <= "<<iWord->t<<")");
					if (rec.t >= iWord->tStart && rec.t <= iWord->t) {				//rec's end time is in iWord
						iCluster->addRecord(rec);
						return;
					} 
					//DBG_FORCE("if ("<<rec.tStart<<" >= "<<iWord->tStart<<" && "<<rec.tStart<<" <= "<<iWord->t<<")");
					if (rec.tStart >= iWord->tStart && rec.tStart <= iWord->t) { 		//rec's start time is in iWord
						iCluster->addRecord(rec);
						return;
					}

					if (rec.tStart <= iWord->tStart && rec.t >= iWord->t) {			//iWord is in rec
						iCluster->addRecord(rec);
						return;
					}
				}
			}
		}
	}
	// if no matching set found, create a new one
//	DBG_FORCE("Creating a new Cluster...");
	LatIndexer::Cluster v;
	v.addRecord(rec);
	v.W_id = rec.wordID;
	v.meetingID = rec.meetingID;
	v.tStart = rec.tStart;
	v.tEnd = rec.t;
	v.conf = rec.conf;
	this->push_back(v);
//	DBG_FORCE("rec: tStart:"<< rec.tStart << "\t" << (Lattice::Node)rec);
//	DBG_FORCE("done");
//	cin.get();
}

/*
void LatIndexer::RecordClusters::computeOverlappedRecordsLikelihood() 
{
	for (RecordClusters::iterator iCluster=this->begin(); iCluster!=this->end(); ++iCluster) {
		for (Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
			iWord->overlapped_likelihood = iWord->likelihood;		// word's own likelihood
			for (Cluster::iterator iWordOverlapped=iCluster->begin(); iWordOverlapped!=iCluster->end(); ++iWordOverlapped) {
				LatTime start = iWordOverlapped->tStart;
				LatTime end = iWordOverlapped->t;
				if (start < iWord->tStart && end > iWord->t)
					iWord->overlapped_likelihood = logAdd(iWord->overlapped_likelihood, iWordOverlapped->likelihood);
			}
//				iWord->overlapped_likelihood -= 
		}
	}
}
*/

void LatIndexer::RecordClusters::print() 
{
	DBG("allClusters.size(): "<< allClusters.size());
	for (RecordClusters::iterator iCluster=this->begin(); iCluster!=this->end(); ++iCluster) {
		cout << "==================================================" << endl << flush;
		DBG("  wClusters.size(): "<<iCluster->size());
		for (Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
//			cout << "overlapped_likelihood:" << iWord->overlapped_likelihood << " likelihood:" << iWord->likelihood << " tStart:" << iWord->tStart << "\t" << *iWord << " " << endl << flush;
			cout << "conf:" << iWord->conf << " tStart:" << iWord->tStart << "\t" << *iWord << " " << endl << flush;
		}
	}
}



//--------------------------------------------------------------------------------
//	LatIndexer::Cluster
//--------------------------------------------------------------------------------
//void LatIndexer::Cluster::addRecord(Cluster::Record rec)
void LatIndexer::Cluster::addRecord(LatIndexer::Record &rec)
{
	if (rec.tStart < this->tStart || this->size() == 0) 
	{
		this->tStart = rec.tStart;
	}
	if (rec.t > this->tEnd || this->size() == 0) 
	{
		this->tEnd = rec.t;
	}
	if (rec.conf > this->conf || this->size() == 0) 
	{
		this->conf = rec.conf;
	}
	this->push_back(rec);
}


//--------------------------------------------------------------------------------
std::ostream& lse::operator<<(std::ostream& os, const LatIndexer::Record& rec) {
	return os << "meetingID:" << rec.meetingID << "\twordID:" << rec.wordID << "\tnodeID:" << rec.nodeID<< "\tconf:" << rec.conf << "\ttStart:" << rec.tStart << "\ttEnd:" << rec.t;
}


#define SORT_REVERSEINDEX_BY_CONFIDENCE
#ifdef SORT_REVERSEINDEX_BY_CONFIDENCE
	//--------------------------------------------------------------------------------
	bool lse::operator<(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		// comparison priority: wordID > conf > meetingID
		if (lv.wordID == rv.wordID) {
			return lv.conf > rv.conf; // nodes with greater posteriors will be first in containers
		} else {
			return lv.wordID < rv.wordID;			
		}		
	}

	//--------------------------------------------------------------------------------
	bool lse::operator>(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		// comparison priority: wordID > conf > meetingID
		if (lv.wordID == rv.wordID) {
			return lv.conf < rv.conf; // nodes with greater posteriors will be first in containers
		} else {
			return lv.wordID > rv.wordID;			
		}		
	}
#else
	//--------------------------------------------------------------------------------
	bool lse::operator<(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		if (lv.wordID == rv.wordID) {
			if (lv.meetingID == rv.meetingID) {
				return lv.t < rv.t;
			} else {
				return lv.meetingID < rv.meetingID;
			}
		} else {
			return lv.wordID < rv.wordID;
		}		
	}

	//--------------------------------------------------------------------------------
	bool lse::operator>(const LatIndexer::Record& lv, const LatIndexer::Record& rv) {
		if (lv.wordID == rv.wordID) {
			if (lv.meetingID == rv.meetingID) {
				return lv.t > rv.t;
			} else {
				return lv.meetingID > rv.meetingID;
			}
		} else {
			return lv.wordID > rv.wordID;			
		}		
	}
#endif

/*
char LatIndexer::SortIdxRecord::CmpConf(const SortIdxRecord &lv, const SortIdxRecord &rv)
{
	if (lv.wordID == rv.wordID) {
		return (lv.conf > rv.conf ? -1 : (lv.conf < rv.conf ? 1 : 0))
	} else {
		return (lv.wordID < rv.wordID ? -1 : 1) ;
	}		
}
*/

bool lse::operator<(const LatIndexer::RecordSortedByTime& lv, const LatIndexer::RecordSortedByTime& rv)
{
	if (lv.wordID == rv.wordID) {
		if (lv.meetingID == rv.meetingID) {
			return lv.t < rv.t;
		} else {
			return lv.meetingID < rv.meetingID;
		}
	} else {
		return lv.wordID < rv.wordID;
	}		
}

bool lse::operator<(const LatIndexer::RecordSortedByConfidence& lv, const LatIndexer::RecordSortedByConfidence& rv)
{
	if (lv.wordID == rv.wordID) {
		return lv.conf > rv.conf;
	} else {
		return lv.wordID < rv.wordID;
	}		
}



char LatIndexer::Record::CmpConf(const LatIndexer::Record &lv, const LatIndexer::Record &rv)
{
	if (lv.wordID == rv.wordID) {
		return (lv.conf < rv.conf ? -1 : (lv.conf > rv.conf ? 1 : 0));
	} else {
		return (lv.wordID < rv.wordID ? -1 : 1) ;
	}		
}


//--------------------------------------------------------------------------------
// addRecord()

void LatIndexer::addRecord(LatIndexer::Record latRec) {
	records.push_back(latRec);
}



//--------------------------------------------------------------------------------
// saveToFile()

int LatIndexer::saveToFile(const string filename) {
	
	ofstream out(filename.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	for(LatIndexer::Records::iterator i=records.begin(); i!=records.end(); ++i) {
		out.write(reinterpret_cast<const char *>(&(*i)), sizeof(*i));
/*		
		out.write(reinterpret_cast<const char *>(&(i->wordID)), sizeof(i->wordID));
		out.write(reinterpret_cast<const char *>(&(i->meetingID)), sizeof(i->meetingID));
		out.write(reinterpret_cast<const char *>(&(i->position)), sizeof(i->position));
		out.write(reinterpret_cast<const char *>(&(i->conf)), sizeof(i->conf));
		out.write(reinterpret_cast<const char *>(&(i->t)), sizeof(i->t));
*/
	}
	out.close();
	return 0;
}



//--------------------------------------------------------------------------------
// loadFromFile()

int LatIndexer::loadFromFile(const string filename) {
	// clear container
	records.clear();

	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
			return (2);
	}

	LatIndexer::Record latRec;

	// read all nodes
	while(!in.eof()) {
		in.read(reinterpret_cast<char *>(&latRec), sizeof(latRec));
		if(in.eof()) break;
/*
		in.read(reinterpret_cast<char *>(&latRec.wordID), sizeof(latRec.wordID));
		in.read(reinterpret_cast<char *>(&latRec.meetingID), sizeof(latRec.meetingID));
		in.read(reinterpret_cast<char *>(&latRec.position), sizeof(latRec.position));
		in.read(reinterpret_cast<char *>(&latRec.conf), sizeof(latRec.conf));
		in.read(reinterpret_cast<char *>(&latRec.t), sizeof(latRec.t));
*/
		// add record to container
		records.push_back(latRec);
	}

	in.close();

	return 0;
}



//--------------------------------------------------------------------------------
// sortFile()

void LatIndexer::sortRecords() {
	sort(records.begin(), records.end());
}


//--------------------------------------------------------------------------------
// print()

void LatIndexer::print() {
	
	for(LatIndexer::Records::iterator i=records.begin(); i!=records.end(); ++i) {
		cout << *i << endl;
	}
}

int LatIndexer::createWordIDIndexFile(const string filename) {
	string str = filename + LATINDEXER_EXT_WIDINDEX;
	FILE *fout = fopen(str.c_str(), "wb");
	if (fout == NULL) {
		return LSE_ERROR_IO;
	}

	FILE *fout_timeidx = fopen((filename + LATINDEXER_EXT_TIMESORT_POINTERS).c_str(), "wb");
	if (fout == NULL) {
		return LSE_ERROR_IO;
	}

	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
		return (2);
	}

	ID_t wordID_pred = -1;
	LatIndexer::Record latRec;
	int pos = 0;
	int pos_null = -1;
	int recordsCount = 0;
	int record_idx = 0;
	while(!in.eof()) {
		pos = in.tellg();
		in.read(reinterpret_cast<char *>(&latRec), sizeof(latRec));
		if (in.eof()) break;
		
		if (wordID_pred != latRec.wordID) {
			if (wordID_pred == -1) {
				// header
				fwrite(&latRec.wordID, sizeof(latRec.wordID), 1, fout);	// first word's ID
				fwrite(&recordsCount, sizeof(recordsCount), 1, fout);	// how many records are stored in this file - will be set later

			} else {
				// padding
				for (int i = 0; i < latRec.wordID - wordID_pred - 1; i++) {
					fwrite(&pos_null, sizeof(pos_null), 1, fout);
					fwrite(&pos_null, sizeof(pos_null), 1, fout_timeidx);
					recordsCount++;
				}
			}
			fwrite(&pos, sizeof(pos), 1, fout);
			fwrite(&record_idx, sizeof(record_idx), 1, fout_timeidx);
			recordsCount++;
//			DBG_FORCE("WRITE: WORD_ID="<<latRec.wordID<<"  recordsCount="<<recordsCount<<"  pos="<<pos);
			wordID_pred = latRec.wordID;
		}
		record_idx++;
	}

	fseek(fout, sizeof(latRec.wordID), SEEK_SET); 			// jump to the first record with given wordID
	fwrite(&recordsCount, sizeof(recordsCount), 1, fout);	// how many records are stored in this file - will be set later
		
//	fclose(fin);
	fclose(fout);
	fclose(fout_timeidx);
	in.close();
	return 0;

}

//--------------------------------------------------------------------------------
/* findWord() {{{ */
//
int LatIndexer::findWord(const string filename, const ID_t wordID, LatIndexer::RecordsReader *result) 
{
	DBG_FORCE("LatIndexer::findWord("<<wordID<<")");
	
	/* read the reverse index hits (sorted by confidence) {{{ */
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("SORTED BY CONFIDENCE");
	// POINTERS FILE (wordID -> position in search index file)
	FILE *finIndex = fopen((filename + LATINDEXER_EXT_WIDINDEX).c_str(), "rb");
	if (finIndex == NULL) {
		CERR("ERROR: cannot open file 'filename + LATINDEXER_EXT_TIMESORT_POINTERS'");
		exit(LSE_ERROR_IO);
	}

	// read header
	fread(&this->firstWordID, sizeof(this->firstWordID), 1, finIndex);
	fread(&this->recordsArrayPointers_size, sizeof(long), 1, finIndex);
	DBG_FORCE("firstID: "<<firstWordID<<"   array_size:"<<recordsArrayPointers_size);

	// is given wordID in the reverse index? If not, do nothing
	if (wordID < this->firstWordID || wordID > this->firstWordID + this->recordsArrayPointers_size - 1)
	{
		DBG_FORCE(wordID<<" < "<<this->firstWordID<<" || "<<wordID<<" > "<<this->firstWordID<<" + "<<this->recordsArrayPointers_size<<" - 1");
		return -1;
	}
	
	// read pointers array
	this->recordsArrayPointers = new long[this->recordsArrayPointers_size];
	fread(this->recordsArrayPointers, sizeof(long), this->recordsArrayPointers_size, finIndex);
	fclose(finIndex);

	long startPos = this->recordsArrayPointers[wordID - this->firstWordID]; // -1 because wordIDs start from 1 (not 0) and !NULL node
	DBG_FORCE("startPos:"<<startPos);
	// if there is any occurence in the reverse index
	if (startPos >= 0)
	{
		// SEARCH INDEX
		FILE *fin = fopen(filename.c_str(), "rb");
		if (fin == NULL) {
			return LSE_ERROR_IO;
		}

		/* OUTPUT FIRST 100 WORDS FROM SEARCH INDEX
		
		   for (int i=0; i<100; i++) {
				long startPos = this->recordsArrayPointers[i];
				LatIndexer::Record latRec;
				fseek(fin, startPos, SEEK_SET); 			// jump to the first record with given wordID
				if (!feof(fin)) {
					fread(&latRec, sizeof(latRec), 1, fin);
					DBG_FORCE("wordID:"<<i<<" startPos:"<<startPos<<"  "<<latRec);
				}
			}
		*/

		LatIndexer::Record latRec;
		DBG_FORCE("sizeof(latRec): "<<sizeof(latRec));
		fseek(fin, startPos, SEEK_SET); 			// jump to the first record with given wordID
		while (!feof(fin)) {
			fread(&latRec, sizeof(latRec), 1, fin);

//			DBG_FORCE("latRec: "<<latRec);
			
			if (feof(fin)) break;
			if (latRec.wordID != wordID) {
				DBG_FORCE("End of reverse index for wordID="<<latRec.wordID<<" reached");
				break;
			} else {
				result->push_back(latRec);
			}
		}
		fclose(fin);
	}
	delete[] this->recordsArrayPointers;
	/* }}} */






	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("SORTED BY TIME");
	/* read the reverse index hits (sorted by time) {{{ */
	if (result->mpByTime != NULL)
	{
		delete[] result->mpByTime;
	}
	result->mpByTime = new int[result->size()];
	
	// POINTERS FILE (wordID -> position in search index file sorted by time)
	DBG_FORCE("file_size('"<< (filename + LATINDEXER_EXT_TIMESORT_POINTERS) <<"') = "<<file_size((filename + LATINDEXER_EXT_TIMESORT_POINTERS).c_str()) );
	FILE *finIndexTime = fopen((filename + LATINDEXER_EXT_TIMESORT_POINTERS).c_str(), "rb");
	if (finIndexTime == NULL) {
		CERR("ERROR: cannot open file '"<<(filename + LATINDEXER_EXT_TIMESORT_POINTERS)<<"'");
		exit(LSE_ERROR_IO);
	}

	// read pointers array
	this->mpSortIdxRecordsArray_byTime_Pointers = new int[this->recordsArrayPointers_size];
	fread(this->mpSortIdxRecordsArray_byTime_Pointers, sizeof(int), this->recordsArrayPointers_size, finIndexTime);
	fclose(finIndexTime);

	startPos = sizeof(LatIndexer::RecordIdx) * this->mpSortIdxRecordsArray_byTime_Pointers[wordID - this->firstWordID]; // -1 because wordIDs start from 1 (not 0) and !NULL node
//	startPos = 0;
	DBG_FORCE("startPos:"<<startPos);
	// if there is any occurence in the reverse index
	if (startPos >= 0)
	{
		// SEARCH INDEX
		DBG_FORCE("file_size('"<< (filename + LATINDEXER_EXT_TIMESORT) <<"') = "<<file_size((filename + LATINDEXER_EXT_TIMESORT).c_str()) );
		FILE *fin = fopen((filename + LATINDEXER_EXT_TIMESORT).c_str(), "rb");
		if (fin == NULL) {
			return LSE_ERROR_IO;
		}

		if (result->mpByTime != NULL)
		{
			delete[] result->mpByTime;
		}
		result->mpByTime = new LatIndexer::RecordIdx[result->size()];

		LatIndexer::RecordIdx recIdxRec;
		DBG_FORCE("sizeof(recIdxRec): "<<sizeof(recIdxRec));
		fseek(fin, startPos, SEEK_SET); 			// jump to the first record with given wordID
		fread(result->mpByTime, sizeof(LatIndexer::RecordIdx), result->size(), fin);
		DBG_FORCE("mpByTime:"<<result->mpByTime);
		DBG_FORCE("mpByTime[0]:"<<result->mpByTime[0]);
		DBG_FORCE("mpByTime[1]:"<<result->mpByTime[1]);
		DBG_FORCE("mpByTime[3]:"<<result->mpByTime[3]);

/*
		fseek(fin, startPos, SEEK_SET); 			// jump to the first record with given wordID
//		fseek(fin, 0, SEEK_SET); 			// jump to the first record with given wordID
		int i = 0;
		while (!feof(fin)) {
			fread(&recIdxRec, sizeof(recIdxRec), 1, fin);

//			DBG_FORCE("recIdxRec: "<<recIdxRec);
			
			if (feof(fin)) break;

//			DBG_FORCE("recIdxRec["<<i<<"]: "<<recIdxRec);
			if (i<(int)result->size())
			{
				result->mpByTime[i] = recIdxRec;
			}
			i++;
		}
		DBG_FORCE("mpByTime:"<<result->mpByTime);
		DBG_FORCE("mpByTime[0]:"<<result->mpByTime[0]);
		DBG_FORCE("mpByTime[1]:"<<result->mpByTime[1]);
		DBG_FORCE("mpByTime[3]:"<<result->mpByTime[3]);
*/
		fclose(fin);
	}
	delete[] this->mpSortIdxRecordsArray_byTime_Pointers;
	/* }}} */
	DBG_FORCE("getting the index sorted by time...done");

	DBG_FORCE("mpByTime:"<<result->mpByTime);
	DBG_FORCE("--------------------------------------------------");
	return 0;
}
/* }}} */



//================================================================================
// FORWARD INDEX
//================================================================================

/**
  @brief Create forward index file or append to an existing one

  @param p_filename Forward index filename

  @return Zero if OK
*/
int FwdIndex::create(string p_filename) 
{	
	this->mFilename = p_filename;
	DBG_FORCE("Creating fwd index file: "<< this->mFilename);
	this->of.open(this->mFilename.c_str(), ios::binary | ios::app);
	if (this->of.bad()) {
		return LSE_ERROR_IO;
	}
	if (mpData != NULL)
	{
		delete[] mpData;
		mpData = NULL;
		mpDataEndptr = NULL;
		mDataSize = 0;
		mpSortIdxRecordsArray_byTime = NULL;
		mpSortIdxRecordsArray_byConfidence = NULL;
		mpData2ByConf = NULL;
	}
	mActive = false;
	return 0;
}

void FwdIndex::addMeeting(int nodesCount)
{
	DBG_FORCE("FwdIndex::addMeeting("<<nodesCount<<")");
	assert(nodesCount > 0);
	if (mpData != NULL)
	{
		delete[] mpData;
	}
	mpData = new LatIndexer::Record[nodesCount];
	mpDataEndptr = mpData;
	mDataSize = 0;
	mActive = true;
}

void FwdIndex::closeMeeting()
{
	for(int i=0; i<this->mDataSize; i++) {
		this->of.write(reinterpret_cast<const char *>(&(this->mpData[i])), sizeof(LatIndexer::Record));
	}
	if (mpData != NULL)
	{
		delete[] mpData;
		mpData = NULL;
		mpDataEndptr = NULL;
		mDataSize = 0;
	}
}

/**
  @brief Append record to the end of the forward index file

  @param latRec Record to be appended
*/
void FwdIndex::addRecord(LatIndexer::Record latRec) 
{
	DBG("FwdIndex::addRecord(): "<<latRec);
	if (mActive)
	{
		LatIndexer::Record *pRec = mpDataEndptr;
		bool bInsert = true; // will the given record be inserted?
	
		// if there are overlapping hypotheses with the same word in the meeting,
		// we will have only the best candidate (it's confidence and nodeID) in the index, 
		// but it will have time boundaries of the whole overlapping group.
		while (--pRec >= mpData)
		{
//			DBG_FORCE("mpData:"<<(int)mpData<<" pRec:"<<(int)pRec);
			if (pRec == NULL)
			{
				break;
			}
			if (pRec->t < latRec.tStart)
			{
				break;
			}
			// assume that the meetingID is the same
			if (pRec->wordID == latRec.wordID &&
				is_overlapping(pRec->tStart, pRec->t, latRec.tStart, latRec.t))
			{
				pRec->tStart = min(pRec->tStart, latRec.tStart);
				pRec->t      = max(pRec->t     , latRec.t     );
				pRec->conf   = max(pRec->conf  , latRec.conf  );
				if (pRec->conf < latRec.conf)
				{
					pRec->nodeID = latRec.nodeID;
				}
				bInsert = false;
			}
		}
//		DBG_FORCE("LOOP FINISHED");
		if (bInsert)
		{
			*mpDataEndptr = latRec;
			mpDataEndptr++;
			mDataSize++;
		}
//		this->of.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));
	}
//	cout << ".";
}


/**
  @brief Close the opened forward index file
*/
void FwdIndex::close() 
{
	if (mActive)
	{
		this->of.close();
	}
}


void FwdIndex::sortarray() 
{

	// SORT BY TIME
	mpSortIdxRecordsArray_byTime = new LatIndexer::RecordSortedByTime[mDataSize];
	for (int i=0; i<mDataSize; i++)
	{
		mpSortIdxRecordsArray_byTime[i].recordIdx = i;
		mpSortIdxRecordsArray_byTime[i].wordID = mpData[i].wordID;
		mpSortIdxRecordsArray_byTime[i].meetingID = mpData[i].meetingID;
//		mpSortIdxRecordsArray_byTime[i].nodeID = mpData[i].nodeID;
		mpSortIdxRecordsArray_byTime[i].conf = mpData[i].conf;
//		mpSortIdxRecordsArray_byTime[i].tStart = mpData[i].tStart;
		mpSortIdxRecordsArray_byTime[i].t = mpData[i].t;
	}
	DBG_FORCE("sorting by time...");
	QuickSort(mpSortIdxRecordsArray_byTime, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by time...done");



	// SORT BY CONFIDENCE
	mpSortIdxRecordsArray_byConfidence = new LatIndexer::RecordSortedByConfidence[mDataSize];
	for (int i=0; i<mDataSize; i++)
	{
		mpSortIdxRecordsArray_byConfidence[i].recordIdx = i;
		mpSortIdxRecordsArray_byConfidence[i].wordID = mpData[i].wordID;
//		mpSortIdxRecordsArray_byConfidence[i].meetingID = mpSortIdxRecordsArray_byTime[i].meetingID;
//		mpSortIdxRecordsArray_byConfidence[i].nodeID = mpSortIdxRecordsArray_byTime[i].nodeID;
		mpSortIdxRecordsArray_byConfidence[i].conf = mpData[i].conf;
//		mpSortIdxRecordsArray_byConfidence[i].tStart = mpSortIdxRecordsArray_byTime[i].tStart;
//		mpSortIdxRecordsArray_byConfidence[i].t = mpSortIdxRecordsArray_byTime[i].t;
	}
	DBG_FORCE("sorting by confidence...");
	QuickSort(mpSortIdxRecordsArray_byConfidence, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by confidence...done");


	mpData2ByConf = new int[mDataSize];
	for (int i=0; i<mDataSize; i++)
	{
		mpData2ByConf[mpSortIdxRecordsArray_byConfidence[i].recordIdx] = i;
	}
/*
	// SORT BY CONFIDENCE
	mpSortIdxRecordsArray_byConfidence = new LatIndexer::RecordSortedByConfidence[mDataSize];
	for (int i=0; i<mDataSize; i++)
	{
		mpSortIdxRecordsArray_byConfidence[i].recordIdx = i;
		mpSortIdxRecordsArray_byConfidence[i].wordID = mpSortIdxRecordsArray_byTime[i].wordID;
//		mpSortIdxRecordsArray_byConfidence[i].meetingID = mpSortIdxRecordsArray_byTime[i].meetingID;
//		mpSortIdxRecordsArray_byConfidence[i].nodeID = mpSortIdxRecordsArray_byTime[i].nodeID;
		mpSortIdxRecordsArray_byConfidence[i].conf = mpSortIdxRecordsArray_byTime[i].conf;
//		mpSortIdxRecordsArray_byConfidence[i].tStart = mpSortIdxRecordsArray_byTime[i].tStart;
//		mpSortIdxRecordsArray_byConfidence[i].t = mpSortIdxRecordsArray_byTime[i].t;
	}
	DBG_FORCE("sorting by confidence...");
	QuickSort(mpSortIdxRecordsArray_byConfidence, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by confidence...done");
*/

	// mapping looks like this now:
	//
	//   ByConf -> ByTime -> Data
	//
	// but we need this mapping:
	//
	//   ByTime -> ByConf -> Data
	//
	// so we need to do some remapping:
/*
	for (int i=0; i<mDataSize; i++)
	{
		LatIndexer::RecordIdx pom = mpSortIdxRecordsArray_byConfidence[i].recordIdx;
		mpSortIdxRecordsArray_byConfidence[i].recordIdx = mpSortIdxRecordsArray_byTime[mpSortIdxRecordsArray_byConfidence[i].recordIdx].recordIdx;
		mpSortIdxRecordsArray_byTime[pom].recordIdx = i;
	}
*/

/*	

	// sort the forward index to make it a reverse index sorted ***BY CONFIDENCE***
	DBG_FORCE("sorting by confidence...");
	QuickSort(this->mpData, (int)0, (int)(this->mDataSize-1));
	DBG_FORCE("sorting by confidence...done");
	
	// create an index structure for LatIndexer::Records sorted ***BY TIME***
	if (mpSortIdxRecordsArray_byTime != NULL)
	{
		delete[] mpSortIdxRecordsArray_byTime;
	}
	mpSortIdxRecordsArray_byTime = new LatIndexer::SortIdxRecord[mDataSize];
	ID_t wordID_cur = -1;
	int ptr_cur = 0;
	DBG_FORCE("mDataSize:"<<mDataSize);
	for (int i=0; i<mDataSize; i++)
	{
		if (mpData[i].wordID != wordID_cur)
		{
			DBG("wordID != wordID_cur ... "<<mpData[i].wordID<<" != "<<wordID_cur);
			ptr_cur = 0;
			wordID_cur = mpData[i].wordID;
		}
		else
		{
			DBG("wordID:"<<wordID_cur<<" once more");
		}

		mpSortIdxRecordsArray_byTime[i].recordIdx = ptr_cur++;
		mpSortIdxRecordsArray_byTime[i].t = mpData[i].t;
		mpSortIdxRecordsArray_byTime[i].wordID = mpData[i].wordID;
		mpSortIdxRecordsArray_byTime[i].meetingID = mpData[i].meetingID;
	}
	
//	for(int i=0; i<this->mDataSize; i++) 
//		DBG_FORCE("mpSortIdxRecordsArray_byTime[i].recordIdx:"<<mpSortIdxRecordsArray_byTime[i].recordIdx);
	

	DBG_FORCE("sorting by time...");
//	QuickSort(mpSortIdxRecordsArray_byTime, (int)0, (int)(mDataSize-1), LatIndexer::SortIdxRecord::CmpTime);
	QuickSort(mpSortIdxRecordsArray_byTime, (int)0, (int)(mDataSize-1));
	DBG_FORCE("sorting by time...done");
*/

}

//==================================================
// ALLOCATING MEMORY
//==================================================
void FwdIndex::initArray(int allFilesSize) {
	this->mDataSize = (int)(allFilesSize / sizeof(LatIndexer::Record));

	if (this->mpData != NULL) delete[] mpData;
	this->mpData = new LatIndexer::Record[this->mDataSize];
	this->mpDataEndptr = this->mpData; // point to the beginning of mpData's free space

	mDataRecordsCount = 0;

	DBG_FORCE("mpData: "<<(int)mpData);
	mpData->wordID = 13;
	DBG_FORCE("mpData: "<<(int)mpData);
}

//==================================================
// APPEND FWD INDEX FILE TO MEMORY
//==================================================
int FwdIndex::appendFile(string p_filename) {
	LatIndexer::Record latRec;
		
	ifstream in(p_filename.c_str(), ios::binary);
	if (!in.good()) {
		cerr << "ERROR: while reading fwd index file: " << p_filename << endl;
		exit(1);
	}

	DBG_FORCE("starting (endptr = "<<this->mpDataEndptr<<")");
	// read all nodes
	while(!in.eof() && mDataRecordsCount < mDataSize) 
	{
		in.read(reinterpret_cast<char *>(this->mpDataEndptr), sizeof(latRec));
		if (in.eof()) break;
		if (mpDataEndptr->wordID == 0) {
			DBG_FORCE("WARNING: wordid==0 (endptr = "<<this->mpDataEndptr<<") ...skipping");
		} 
		else
		{
			this->mpDataEndptr++;
			mDataRecordsCount++;
		}
	}
//	this->mpDataEndptr--; // the last record was not actually there
	DBG_FORCE("ending (endptr = "<<this->mpDataEndptr<<")");
	
	in.close();
	return 0;
}


int FwdIndex::save(string p_filename) 
{
	// reverse index sorted by confidence
	ofstream out(p_filename.c_str(), ios::binary);
	if (out.bad()) {
		return (LSE_ERROR_IO);
	}


	if (mpSortIdxRecordsArray_byConfidence == NULL)
	{
		for(int i=0; i<this->mDataSize; i++) {
			out.write(reinterpret_cast<const char *>(&(this->mpData[i])), sizeof(LatIndexer::Record));
		}
	}
	else
	{
		for(int i=0; i<this->mDataSize; i++) {
			out.write(reinterpret_cast<const char *>(&(this->mpData[mpSortIdxRecordsArray_byConfidence[i].recordIdx])), sizeof(LatIndexer::Record));
		}
	}
	out.close();



	if (mpSortIdxRecordsArray_byTime != NULL)
	{
		// pointers to reverse index sorted by time
		out.open((p_filename + LATINDEXER_EXT_TIMESORT).c_str(), ios::binary);
		if (out.bad()) {
			return (LSE_ERROR_IO);
		}

		ID_t wordIdPred = -1;
		int conf_ptr_start = 0;
		int max_conf_idx = 0;

		for(int i=0; i<mDataSize; i++) 
		{
			if (wordIdPred != mpData[mpSortIdxRecordsArray_byTime[i].recordIdx].wordID)
			{
				DBG("wordIdPred != mpData[].wordID ... "<<wordIdPred<<" != "<<mpData[mpSortIdxRecordsArray_byTime[i].recordIdx].wordID);
				conf_ptr_start = i;
				wordIdPred = mpData[mpSortIdxRecordsArray_byTime[i].recordIdx].wordID;
			}
			LatIndexer::RecordIdx conf_idx = mpData2ByConf[mpSortIdxRecordsArray_byTime[i].recordIdx] - conf_ptr_start;
			if (conf_idx > max_conf_idx) 
			{
				max_conf_idx = conf_idx;
			}

			DBG("conf_idx = "<<mpData2ByConf[mpSortIdxRecordsArray_byTime[i].recordIdx]<<" - "<<conf_ptr_start<<" = "<<conf_idx);
			out.write(reinterpret_cast<const char *>(&conf_idx), sizeof(LatIndexer::RecordIdx));
		}
		out.close();
		DBG_FORCE("max_conf_idx:"<<max_conf_idx);
	}

	return 0;
}


void FwdIndex::Print()
{
	DBG_FORCE("mDataSize: "<<mDataSize);
	for(int i=0; i<this->mDataSize; i++) {
		DBG_FORCE("["<<i<<"]: " << this->mpData[i]);
	}
	DBG_FORCE("mDataSize: "<<mDataSize);
}
