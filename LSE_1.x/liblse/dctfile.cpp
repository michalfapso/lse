#include "dctfile.h"

using namespace std;
using namespace lse;

DctFile::DctFile()
{

}

DctFile::DctFile(const string &filename)
{
	Open(filename);
}



bool DctFile::IsEol(const char c)
{
	return (c == '\n');
}

bool DctFile::IsDelimiter(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}


bool DctFile::Next(DctFileRec *rec)
{
	static char c 	= 0;
	string 	s		= "";
	
	rec->Reset();

	while(!mFile.eof())
	{
		if (mEpsilon) 
		{
			mEpsilon = false;
		}
		else
		{
			mFile.get(c);
		}
//		DBG_FORCE("mState:"<<mState<<" c:"<<c);

		switch (mState)
		{
			case 0: // read word
					if (IsDelimiter(c))
					{
						rec->mWord = s;
						s = "";
						mState = 1;
					}
					else
					{
						// ignore the escape-slash
						if (c != '\\')
						{
							s += c;
						}
					}
				break;

			case 1: // skip whitespace
					if (!IsDelimiter(c))
					{
						mEpsilon = true;
						mState = 2;
					}
				break;

			case 2: // read pronounciation
					if (IsEol(c))
					{
						rec->mPron = s;
						s = "";
						mState = 0;
						return true;
					}
					else
					{
						s += c;
					}
				break;

		} // switch(mState)
	} // while !eof

	return false;
}


void DctFile::Open(const string &filename)
{
	mFile.open(filename.c_str());

	if (!mFile.good()) {
		CERR("ERROR: opening file " << filename);
		exit (1);
	}
}

void DctFile::Close()
{
	mFile.close();
}
