#include "wordclusters.h"

using namespace std;
using namespace lse;

void WordClusters::sort() {
	
	for (AllClusters::iterator i=allClusters.begin(); i!=allClusters.end(); ++i) {
		std::sort(i->begin(), i->end());
	}
}


void WordClusters::add(WordClusters::Node node) {

//	DBG_FORCE("add()");
	// let's find the set for given node
	for (AllClusters::iterator iCluster=allClusters.begin(); iCluster!=allClusters.end(); ++iCluster) {
		// compare with all nodes in set
		if (iCluster->W_id == node.W_id) {
			for (vector<WordClusters::Node>::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
				//DBG_FORCE("if ("<<node.t<<" >= "<<iWord->tStart<<" && "<<node.t<<" <= "<<iWord->t<<")");
				if (node.t >= iWord->tStart && node.t <= iWord->t) {				//node's end time is in iWord
					iCluster->push_back(node);
					return;
				} 
				//DBG_FORCE("if ("<<node.tStart<<" >= "<<iWord->tStart<<" && "<<node.tStart<<" <= "<<iWord->t<<")");
				if (node.tStart >= iWord->tStart && node.tStart <= iWord->t) { 		//node's start time is in iWord
					iCluster->push_back(node);
					return;
				}

				if (node.tStart <= iWord->tStart && node.t >= iWord->t) {			//iWord is in node
					iCluster->push_back(node);
					return;
				}
			}
		}
	}
	// if no matching set found, create a new one
//	DBG_FORCE("Creating a new Cluster...");
	WordClusters::Cluster v;
	v.push_back(node);
	v.W_id = node.W_id;
	allClusters.push_back(v);
//	DBG_FORCE("node: tStart:"<< node.tStart << "\t" << (Lattice::Node)node);
//	DBG_FORCE("done");
//	cin.get();
}



void WordClusters::print() {
	DBG("allClusters.size(): "<< allClusters.size());
	for (AllClusters::iterator iCluster=allClusters.begin(); iCluster!=allClusters.end(); ++iCluster) {
		cout << "==================================================" << endl << flush;
		DBG("  wClusters.size(): "<<iCluster->size());
		for (WordClusters::Cluster::iterator iWord=iCluster->begin(); iWord!=iCluster->end(); ++iWord) {
			cout << "overlapped_likelihood:" << iWord->overlapped_likelihood << " likelihood:" << iWord->likelihood << " tStart:" << iWord->tStart << "\t" << *iWord << " " << endl << flush;
		}
	}
}
