#include <queue>
#include "lat.h"

using namespace std;
using namespace lse;


void Lattice::Node::init() {
	id=0;
	t=0;
	W="";
	W_id=0;
	v=0;
	p=0;
}


/**
  @brief Get start time of incoming link with best likelihood

  @param likelihood Best ingoing link's likelihood

  @return Node's start time
*/
/*
LatTime Lattice::getNodeStartTime(ID_t nodeID, double *likelihood) {
	
//	bool startTime_isset = false;
	double bestLikelihood = vit->getLinkLikelihood(*(lat->bckLinks[nodeID].begin())); 	// get first link's likelihood
	ID_t bestLinkID = *(lat->bckLinks[nodeID].begin());									

//	DBG("getNodeStartTime()");
	for (vector<ID_t>::iterator i=lat->bckLinks[nodeID].begin(); i!=lat->bckLinks[nodeID].end(); ++i) {

		double curLikelihood = vit->getLinkLikelihood(*i);
//		DBG("Link's likelihood: " << curLikelihood);
		if (curLikelihood > bestLikelihood) {
			bestLikelihood = curLikelihood;
			bestLinkID = *i;
		}
	}
	*likelihood = bestLikelihood;
//	DBG("bestLink: " << bestLinkID << "\t" << lat->links[bestLinkID]);

//	DBG("getNodeStartTime() end");
	return lat->nodes[lat->links[bestLinkID].S].t; 	// end time of best link's start node
}
*/

std::ostream& lse::operator<<(std::ostream& os, const Lattice::Node& node) {
	return os << "ID=" << node.id << "\ttStart="<<node.tStart<<"\ttEnd=" << node.t << "\tW[" << node.W_id << "]=" << node.W << "\tv=" << node.v << "\tp=" << node.p << "\tbestLikelihood=" << node.bestLikelihood << "\talpha=" << node.alpha << "\tbeta=" << node.beta;
}


//--------------------------------------------------------------------------------
bool lse::operator<(const Lattice::Node& l, const Lattice::Node& r) {
	// we need to keep the one with !NULL always as the greater.
	
	if (l.t == r.t) {
		if (l.W=="!NULL") {
			//cout<<"l.W==\"!NULL\""<<"id="<<l.id<<endl;
			return false;
		}
		if (r.W=="!NULL") {
			//cout<<"r.W==\"!NULL\""<<"id="<<l.id<<endl;
			return true;
		}
		// no need to solve the case if none of them
		// is NULL...
		return false;
	}
	else
		return l.t < r.t;
}


bool lse::operator<(const Lattice::Link& l, const Lattice::Link& r) {
	return l.confidence < r.confidence;
}

//--------------------------------------------------------------------------------
std::ostream& lse::operator<<(std::ostream& os, const Lattice::Link& link) {
	return os << "ID=" << link.id << " S=" << link.S << " E=" << link.E << " a=" << link.a << " l=" << link.l << " confidence="<<link.confidence;
}

//--------------------------------------------------------------------------------
void Lattice::Link::init() {
	id=0;
	S=0;
	E=0;
	a=0;
	l=0;
}


//--------------------------------------------------------------------------------
void Lattice::print() {
	cout << "NODES:" << endl;
	printNodes();
	cout << "LINKS:" << endl;
	printLinks();
}


//--------------------------------------------------------------------------------
bool Lattice::latticeNodeSortCriterion(const Lattice::Node& n1, const Lattice::Node& n2) {
	return n1.id < n2.id;
}


//--------------------------------------------------------------------------------
void Lattice::addToLexicon() {
	for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i)	{
		// add record to lexicon if there is no such word and get word's ID in lexicon
		(i->second).W_id = lexicon->getValID((i->second).W);
	}
}


//--------------------------------------------------------------------------------
// Add new node to nodes map

void Lattice::addNode(Lattice::Node node) {

//	if (node.id == 395)
//		DBG("node[395] " << node);
	nodes[node.id] = node;
//	N++;
}

void Lattice::addNode(Lattice::Node *node) {

//	if (node.id == 395)
//		DBG("node[395] " << node);
	nodes[node->id] = *node;
//	N++;
}


//--------------------------------------------------------------------------------
// Add new link to links map

void Lattice::addLink(Lattice::Link link) {

	links[link.id] = link;
//	nodes[link.S].succ_links.push_back(link.id);
//	L++;
//	cout << "adding link: " << link << endl;
}

void Lattice::addLink(Lattice::Link *link) {

	links[link->id] = *link;
//	nodes[link.S].succ_links.push_back(link.id);
//	L++;
//	cout << "adding link: " << link << endl;
}


/*
void Lattice::deleteNode(ID_t nodeID, vector<ID_t> *delNodeIDList, vector<ID_t> *delLinkIDList) {
	
//	DBG_FORCE("deleting node: "<<nodeID);
	assert(nodeID > 0 && nodeID < ((--nodes.end())->second).id);
	// delete all links connected to the node, which is going to be deleted
	
//	DBG_FORCE("");
//	LinkMap::iterator delLink = links.end();
//	for (vector<ID_t>::iterator linkID=fwdLinks[nodeID].begin(); linkID!=fwdLinks[nodeID].end(); ++linkID) {
//		if ((delLink = links.find(*linkID)) != links.end()) {
//			links.erase(delLink);		// delete link
//			fwdLinks[nodeID].erase(linkID);	// update fwdLinks map
//		}
//	}
//		
//	DBG_FORCE("");
//	for (vector<ID_t>::iterator linkID=bckLinks[nodeID].begin(); linkID!=bckLinks[nodeID].end(); ++linkID) {
//		if ((delLink = links.find(*linkID)) != links.end()) {
//			links.erase(delLink);		// delete link
//			bckLinks[nodeID].erase(linkID);	// update bckLinks map
//		}
//	}
//	
	for (LinkMap::iterator l=links.begin(); l!=links.end(); ++l) {
		if (l->second.S == nodeID || l->second.E == nodeID) {
			delLinkIDList->push_back(l->second.id);
		}
	}

	NodeMap::iterator delNode;
	if ((delNode = nodes.find(nodeID)) != nodes.end()) {
//		nodes.erase(delNode);	// delete node
		delNodeIDList->push_back(nodeID);
	}
//	DBG_FORCE("");
//	FwdLinks::iterator delFwdLinks;
//	if ((delFwdLinks = fwdLinks.find(nodeID)) != fwdLinks.end()) {
//		fwdLinks.erase(delFwdLinks);	// delete node
//	}
//	DBG_FORCE("");
//	BckLinks::iterator delBckLinks;
//	if ((delBckLinks = bckLinks.find(nodeID)) != bckLinks.end()) {
//		bckLinks.erase(delBckLinks);	// delete node
//	}
	
}
*/

void Lattice::removeNodes(vector<ID_t> *delNodeIDList) {
	NodeMap::iterator delNode;
	for (vector<ID_t>::iterator n=delNodeIDList->begin(); n!=delNodeIDList->end(); ++n) {
		if ((delNode = nodes.find(*n)) != nodes.end())
			nodes.erase(delNode);
	}
}

void Lattice::removeLinks(vector<ID_t> *delLinkIDList) {
	LinkMap::iterator delLink;
	for (vector<ID_t>::iterator l=delLinkIDList->begin(); l!=delLinkIDList->end(); ++l) {
		if ((delLink = links.find(*l)) != links.end())
			links.erase(delLink);
	}
}
	
void Lattice::removeNode_PreserveLinks(ID_t nodeID) {
		
}

void Lattice::pruneLattice(TLatViterbiLikelihood posteriorTreshold) {
	DBG_FORCE("pruneLattice("<<posteriorTreshold<<")");
	vector<ID_t> delNodeIDList;
	vector<ID_t> delLinkIDList;
	ID_t newLinkID = getLastLinkID() + 1;
	updateFwdBckLinks();
	int counter = 0;
	NodeMap::iterator nPred=nodes.end();
	bool remove_nPred = false;
/*	DBG_FORCE("Fwd and Bck Links:");
	for(NodeMap::iterator n=nodes.begin(); n!=nodes.end(); ++n, ++counter) {
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("nodeID:"<<n->second.id);
		for (vector<ID_t>::iterator l=fwdLinks[n->second.id].begin(); l!=fwdLinks[n->second.id].end(); ++l) 
			DBG_FORCE("Fwd:"<<links[*l]);
		
		for (vector<ID_t>::iterator l=bckLinks[n->second.id].begin(); l!=bckLinks[n->second.id].end(); ++l) 
			DBG_FORCE("Bck:"<<links[*l]);
	}
*/	
	for(NodeMap::iterator n=++nodes.begin(); n!=--nodes.end(); ++n, ++counter) {
		if (remove_nPred) {
			DBG_FORCE("removing node:"<<nPred->second.id);
			nodes.erase(nPred);
			remove_nPred = false;
		}
			
//		DBG_FORCE("--------------------------------------------------");
//		DBG_FORCE("node:"<<n->second);
		if(n->second.p < posteriorTreshold/* && n->second.W_id != lexicon->nullWordID*/) {
			remove_nPred = true;
//			delNodeIDList.push_back(n->second.id);
			// add fwdLinks to delList
			for (vector<ID_t>::iterator l=fwdLinks[n->second.id].begin(); l!=fwdLinks[n->second.id].end(); ++l) {
				ID_t fwdNodeID = links[*l].E;
				// for all backward links for all current node's successors (forward nodes) remove their invalid bckLinks
				for (vector<ID_t>::iterator l_fwd=bckLinks[fwdNodeID].begin(); l_fwd!=bckLinks[fwdNodeID].end(); ++l_fwd) {
					if (*l_fwd == *l) {
						bckLinks[fwdNodeID].erase(l_fwd);
						break; // there is only one bckLink with the same ID
					}
				}
//				DBG_FORCE("FwdLink:"<<links[*l]);
				delLinkIDList.push_back(*l);
			}

			// add bckLinks to delList
			for (vector<ID_t>::iterator l=bckLinks[n->second.id].begin(); l!=bckLinks[n->second.id].end(); ++l) {
				ID_t bckNodeID = links[*l].S;
				// for all backward links for all current node's successors (forward nodes) remove their invalid bckLinks
				for (vector<ID_t>::iterator l_bck=fwdLinks[bckNodeID].begin(); l_bck!=fwdLinks[bckNodeID].end(); ++l_bck) {
					if (*l_bck == *l) {
						fwdLinks[bckNodeID].erase(l_bck);
						break; // there is only one bckLink with the same ID
					}
				}
//				DBG_FORCE("BckLink:"<<links[*l]);
				delLinkIDList.push_back(*l);
			}
			
			
//			for (LinkMap::iterator l=links.begin(); l!=links.end(); ++l) {
//				if (l->second.S == n->second.id || l->second.E == n->second.id) {
//					delLinkIDList.push_back(l->second.id);
//				}
//			}

			// create new links instead of those which are going to be removed
			Lattice::Link tmpLink;
			for (vector<ID_t>::iterator l_bck=bckLinks[n->second.id].begin(); l_bck!=bckLinks[n->second.id].end(); ++l_bck) {
				for (vector<ID_t>::iterator l_fwd=fwdLinks[n->second.id].begin(); l_fwd!=fwdLinks[n->second.id].end(); ++l_fwd) {
					tmpLink.id = newLinkID++;
					tmpLink.S = links[*l_bck].S;
					tmpLink.E = links[*l_fwd].E;
					tmpLink.a = links[*l_bck].a + links[*l_fwd].a;
					tmpLink.l = links[*l_bck].l + links[*l_fwd].l;
					bool linkExists = false;
					for (vector<ID_t>::iterator l=fwdLinks[tmpLink.S].begin(); l!=fwdLinks[tmpLink.S].end(); ++l) {
						if (links[*l].E == tmpLink.E) {
							linkExists = true;
							break;
						}
					}
/*					for (LinkMap::iterator l = links.begin(); l!=links.end(); ++l) {
						if (l->second.S == tmpLink.S && l->second.E == tmpLink.E) {
							linkExists = true;
							break;
						}
					}
*/					if (!linkExists) {
/*						if (links.find(tmpLink.id) != links.end()) {
							DBG_FORCE("rewriting link:"<<links[tmpLink.id]);
						}
*/						addLink(tmpLink);
						fwdLinks[tmpLink.S].push_back(tmpLink.id);
						bckLinks[tmpLink.E].push_back(tmpLink.id);
//						DBG_FORCE("addFwdLink to nodeID:"<<tmpLink.S);
//						DBG_FORCE("addBckLink to nodeID:"<<tmpLink.E);
//						DBG_FORCE("addLink:"<<tmpLink);
					} else {
//						DBG_FORCE("link from "<<tmpLink.S<<" to "<<tmpLink.E<<" exists");
					}
				}
			}
//			deleteNode(n->second.id, &delNodeIDList, &delLinkIDList);
			nPred = n;
		}
	}
	if (remove_nPred) {
		nodes.erase(nPred);
	}

	removeNodes(&delNodeIDList);
	removeLinks(&delLinkIDList);

}


void Lattice::removeNullNodes() {
	DBG_FORCE("Lattice::removeNullNodes() has some bug inside => method is disabled");
/*	
	vector<ID_t> delNodeIDList;
	vector<ID_t> delLinkIDList;
	updateFwdBckLinks();
	ID_t newLinkID = getLastLinkID() + 1;
	// for all nodes except first and the last one
	for (NodeMap::iterator n=++nodes.begin(); n!=--nodes.end(); ++n) {
		if (n->second.W_id == lexicon->nullWordID) {
			
			

			// create new links instead of those which are going to be removed
			for (vector<ID_t>::iterator l_bck=bckLinks[n->second.id].begin(); l_bck!=bckLinks[n->second.id].end(); ++l_bck) {
				for (vector<ID_t>::iterator l_fwd=fwdLinks[n->second.id].begin(); l_fwd!=fwdLinks[n->second.id].end(); ++l_fwd) {
					Lattice::Link tmpLink;
					tmpLink.id = newLinkID++;
					tmpLink.S = links[*l_bck].S;
					tmpLink.E = links[*l_fwd].E;
					tmpLink.a = links[*l_bck].a + links[*l_fwd].a;
					tmpLink.l = links[*l_bck].l + links[*l_fwd].l;
					addLink(tmpLink);
					if (n->second.id == 4) {
						DBG_FORCE("addLink:"<<tmpLink);
					}
				}
			}
			
			// add node to delList
			delNodeIDList.push_back(n->second.id);
			// add fwdLinks to delList
			for (vector<ID_t>::iterator l=fwdLinks[n->second.id].begin(); l!=fwdLinks[n->second.id].end(); ++l) {
				if (n->second.id == 4) {
					DBG_FORCE("FwdLink:"<<links[*l]);
				}
				delLinkIDList.push_back(*l);
			}
			// add bckLinks to delList
			for (vector<ID_t>::iterator l=bckLinks[n->second.id].begin(); l!=bckLinks[n->second.id].end(); ++l) {
				if (n->second.id == 4) {
					DBG_FORCE("BckLink:"<<links[*l]);
				}
				delLinkIDList.push_back(*l);
			}
		}
	}
	removeNodes(&delNodeIDList);
	removeLinks(&delLinkIDList);
*/	
}

//--------------------------------------------------------------------------------
// Print all nodes

void Lattice::printNodes() {
//	cout << "Nodes count: " << nodes.end() << endl;

	for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		cout << (i->first) << "> " << (i->second) << endl;
	}
}


//--------------------------------------------------------------------------------
// Print all links

void Lattice::printLinks() {

	for(LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
		cout << (i->second) << endl;
	}		
}



/*--------------------------------------------------------------------------------
 * Convert words in lattice to wordIDs and save lattice to binary file
 * and insert new words to the lexicon 
 *
 */
/*
void Lattice::saveToBinaryFile(const string filename, CompressionType compression) {
	
	LatIndexer::Record latRec;
	
	if (compression == gzip) {
		
		// write COMPRESSED binary
		
		gzFile gz_out = gzopen(filename.c_str(), "wb");
		if (gz_out == NULL) {
//			return (2);		
		}

		// add index for this meeting to indexer->meetings
		latRec.meetingID = indexer->meetings.getValID(filename);

		// write header info (N,L)
		gzwrite(gz_out, reinterpret_cast<char *>(&N), sizeof(N));
		gzwrite(gz_out, reinterpret_cast<char *>(&L), sizeof(L));

		// write all nodes (with wordID instead of word)
		for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
			
			//add index for this node	
			latRec.wordID = lexicon->getValID((i->second).W);
			latRec.position = gztell(gz_out);
			latRec.p = (i->second).p;
			latRec.t = (i->second).t;
			indexer->lattices.addRecord(latRec);

			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).id), sizeof((i->second).id));
			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).t), sizeof((i->second).t));
			gzwrite(gz_out, reinterpret_cast<char *>(&latRec.wordID), sizeof(latRec.wordID));
			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).v), sizeof((i->second).v));
			gzwrite(gz_out, reinterpret_cast<char *>(&(i->second).p), sizeof((i->second).p));
		}

		// write all links
		for(LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
				
			gzwrite(gz_out, reinterpret_cast<char *>(&i->second), sizeof(i->second));
		}		
		
		gzclose(gz_out);
		
	} else {
		
		// write UNCOMPRESSED binary
		
		ofstream out(filename.c_str(), ios::binary); // HTK Standard Lattice File
		
		if (!out.good()) {
//			return (2);
		}

		// write header info (N,L)
		out.write(reinterpret_cast<const char *>(&N), sizeof(N));
		out.write(reinterpret_cast<const char *>(&L), sizeof(L));

		// write all nodes (with wordID instead of word)
		for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
				
			ID_t wordID = lexicon->getValID((i->second).W);
			out.write(reinterpret_cast<const char *>(&(i->second).id), sizeof((i->second).id));
			out.write(reinterpret_cast<const char *>(&(i->second).t), sizeof((i->second).t));
			out.write(reinterpret_cast<const char *>(&wordID), sizeof(wordID));
			out.write(reinterpret_cast<const char *>(&(i->second).v), sizeof((i->second).v));
			out.write(reinterpret_cast<const char *>(&(i->second).p), sizeof((i->second).p));		
		}

		// write all links
		for(LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
				
			out.write(reinterpret_cast<const char *>(&i->second), sizeof(i->second));
		}		

		out.close();
	
	}


}


//--------------------------------------------------------------------------------
// Read lattice from binary file

int Lattice::loadFromBinaryFile(const string filename) {
	
	setFilename(filename);
	// Clear nodes and links maps
	nodes.clear();
	links.clear();

	if (filename.substr(filename.length()-3,3) == ".gz") {
		
		// read COMPRESSED binary
		
		gzFile gz_in = gzopen(filename.c_str(), "rb");
		if (gz_in == NULL) {
			return (2);		
		}

		// read header info (N,L)
		gzread(gz_in, reinterpret_cast<char *>(&N), sizeof(N));
		gzread(gz_in, reinterpret_cast<char *>(&L), sizeof(L));
		
		Lattice::Node tmpNode;
		Lattice::Link tmpLink;
		
		// read all nodes and convert wordID into word using lexicon
		for(int i=0; i<N; i++) {
				
			ID_t wordID;
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.id)), sizeof(tmpNode.id));
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.t)), sizeof(tmpNode.t));
			gzread(gz_in, reinterpret_cast<char *>(&wordID), sizeof(wordID));
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.v)), sizeof(tmpNode.v));
			gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.p)), sizeof(tmpNode.p));
			tmpNode.W = lexicon->getVal(wordID);

			addNode(tmpNode);
		}

		
		// read all links
		for(int i=0; i<L; i++) {
				
			gzread(gz_in, reinterpret_cast<char *>(&tmpLink), sizeof(tmpLink));
			addLink(tmpLink);
		}

		gzclose(gz_in);
		
	} else {
				
		ifstream in(filename.c_str(), ios::binary);
		if (!in.good()) {
			return (2);
		}
		// read header info (N,L)
		in.read(reinterpret_cast<char *>(&N), sizeof(N));
		in.read(reinterpret_cast<char *>(&L), sizeof(L));
		
		Lattice::Node tmpNode;
		Lattice::Link tmpLink;
		
		// read all nodes and convert wordID into word using lexicon
		for(int i=0; i<N; i++) {
				
			ID_t wordID;
			in.read(reinterpret_cast<char *>(&(tmpNode.id)), sizeof(tmpNode.id));
			in.read(reinterpret_cast<char *>(&(tmpNode.t)), sizeof(tmpNode.t));
			in.read(reinterpret_cast<char *>(&wordID), sizeof(wordID));
			in.read(reinterpret_cast<char *>(&(tmpNode.v)), sizeof(tmpNode.v));
			in.read(reinterpret_cast<char *>(&(tmpNode.p)), sizeof(tmpNode.p));
			tmpNode.W = lexicon->getVal(wordID);

			addNode(tmpNode);
		}

		
		// read all links
		for(int i=0; i<L; i++) {
				
			in.read(reinterpret_cast<char *>(&tmpLink), sizeof(tmpLink));
			addLink(tmpLink);
		}

		in.close();
	}
	
	return 0;
}

*/

//--------------------------------------------------------------------------------
// Return node with lowest time (Lattice::Node::t)

Lattice::Node Lattice::firstNode() {

	if (nodes.size() == 0) {
		Lattice::Node zeroNode;
		return zeroNode;
	}
	return nodes.begin()->second;
/*	
	Lattice::Node min_node = (nodes.begin())->second;

	for(NodeMap::iterator i=(nodes.begin()); i!=nodes.end(); ++i) {
		if ((i->second).t < min_node.t) {
			min_node = i->second;
		}
	}
	return min_node;
*/	
}


//--------------------------------------------------------------------------------
// Return node with highest time (Lattice::Node::t)

Lattice::Node Lattice::lastNode() {
	
	if (nodes.size() == 0) {
		Lattice::Node zeroNode;
		return zeroNode;
	}
		
	return nodes.rbegin()->second;
/*		
	NodeMap::iterator i = nodes.begin();
	Lattice::Node max_node = i->second;
	
	for(NodeMap::iterator i=(nodes.begin()); i!=nodes.end(); ++i) {
		if ((i->second).t > max_node.t) {
			max_node = i->second;
		}
	}
	
	return max_node;
*/
}


//--------------------------------------------------------------------------------
// add lattice "conlat" to current lattice by !NULL node

void Lattice::addLattice(Lattice &conlat) {

	
//	cout << "----------------------------------------" << endl;
//	DBG_FORCE("Current Lattice: N="<<nodes.size()<<" L="<<links.size());
//	DBG_FORCE("ConLat: N="<<conlat.nodes.size()<<" L="<<conlat.links.size());
	
	// add lattice "conlat" after current lattice only if they are chronologicaly OK
//	if ((lastNode()).t <= (conlat.firstNode()).t) {
	
		// get last link index value
		ID_t lastLinkIndex;
		if (links.size() > 0) {
			lastLinkIndex = (--links.end())->first;
		} else {
			lastLinkIndex = -1; // new index will be 0
		}

		// get last node index value
		ID_t lastNodeIndex;
	    if (nodes.size() > 0) {
			lastNodeIndex = (--nodes.end())->first;
		} else {
			lastNodeIndex = -1; // new index will be 0
		}

		float initConnModelValue = 0.0;
		TLatViterbiLikelihood initConnLikelihoodValue = 0.0;
		
//		DBG_FORCE("LastLinkID:"<<lastLinkIndex<<" LastNodeID:"<<lastNodeIndex);
//		DBG_FORCE("getEndNode(): "<<getEndNode());
		// add links from global lattice's end node to the new connection node
		Lattice::Link tmpLink;
		tmpLink.id = ++lastLinkIndex;
		tmpLink.S = getEndNode();
		tmpLink.E = lastNodeIndex + 1; // connection node's ID (connection node will be just added)
		tmpLink.a = initConnModelValue;
		tmpLink.l = initConnModelValue;
		tmpLink.confidence = initConnLikelihoodValue;
		addLink(tmpLink);
		L++;
//		DBG_FORCE("Added link GlobalLat->ConnectionNode: "<<tmpLink);
//			cout << "Added link: " << tmpLink.S << " -> " << tmpLink.E << endl;
		
		// add connection node after the last node of current lattice
		Lattice::Node connectionNode;
		connectionNode.id = ++lastNodeIndex;
		connectionNode.t = ((conlat.nodes.begin())->second).t;
		connectionNode.W = "<sil>";
		connectionNode.v = 0;
		connectionNode.bestLikelihood = 0.0;
		addNode(connectionNode);
		N++;
//		DBG_FORCE("Added connection node: "<<connectionNode);
		
		ID_t conlatNewFirstNodeID = lastNodeIndex + 1; // conlat's first node ID after it will be added to current lattice
		
		// add links from current lattice's connection node to conlat's first nodes
//		float conLatMinTime = (conlat.firstNode()).t;
			
		tmpLink.id = ++lastLinkIndex;
		tmpLink.S = connectionNode.id;
		tmpLink.E = conlatNewFirstNodeID; // update IDs
		tmpLink.a = initConnModelValue;
		tmpLink.l = initConnModelValue;
		tmpLink.confidence = initConnLikelihoodValue;
		addLink(tmpLink);
		L++;
//		DBG_FORCE("Added link ConnectionNode->ConLat: "<<tmpLink);

//		DBG_FORCE("Adding nodes to global lattice");
		// add nodes from "conlat" to current lattice
		Lattice::Node tmpNode;
		for(NodeMap::iterator i=conlat.nodes.begin(); i!=conlat.nodes.end(); ++i) {
			tmpNode = i->second;
			tmpNode.id = ++lastNodeIndex;
//			cout << "Adding node with ID=" << tmpNode.id << endl;
			addNode(tmpNode);
			N++;
		}
		
		// add links from lattice "conlat" with updated IDs of start and end nodes	
		for(LinkMap::iterator i=conlat.links.begin(); i!=conlat.links.end(); ++i) {
			tmpLink = i->second;
			tmpLink.id = ++lastLinkIndex;
			tmpLink.S += conlatNewFirstNodeID;
			tmpLink.E += conlatNewFirstNodeID;			
			addLink(tmpLink);
			L++;
		}

		updateFwdBckLinks();
/*	
	} else {
		// add before the start node
		DBG_FORCE("(lastNode()).t > (conlat.firstNode()).t ... "<<(lastNode()).t<<" > "<<(conlat.firstNode()).t);
	}
*/
/*
	cout << "----------------------------------------" << endl;
	cout << "LATTICE after adding:" << endl;
	print();
*/
	
}


//--------------------------------------------------------------------------------
// function for use in loadFromHTKFile()

bool Lattice::isDelimiter(char c) {
	switch (c) {

		case '#':
		case ' ':
		case '\n':
		case '\t':
			return true;
			
		default:
			return false;
	}
}
/*
map<ID_t,int> nodeOrderMap;

void Lattice::recursiveLatProcessing_ForTimeClusterSorting(ID_t curNodeID, int *counter) {
	nodeOrderMap[curNodeID] = *counter; // set order value
	
	for (vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
		// call getEndNode_recursive for all nodes outgoing from current node
		if (nodeOrderMap.find(links[*i].E) != nodeOrderMap.end()) { // if there is any order value in current-node-forward-link's end-node
			*counter++;
			recursiveLatProcessing_ForTimeClusterSorting(links[*i].E, counter);			
		}
			
//			if (nodeOrderMap[links[*i].E] > 0) {

		recursiveLatProcessing_ForTimeClusterSorting(links[*i].E);
	}
}
*/
void Lattice::recursiveLatProcessing(ID_t curNodeID) {
	for (vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
		// call getEndNode_recursive for all nodes outgoing from current node
		recursiveLatProcessing(links[*i].E);
	}
}

void Lattice::sortNodesByLinks(vector<Lattice::Node> &a) {
	DBG_FORCE("sorting...");
	for (unsigned int i=1; i<a.size(); i++) {
		Lattice::Node curNode = a[i];
		unsigned int curNodeMoveFore_idx = i; 				// in front of which idx has to be moved
		for (vector<ID_t>::iterator l=fwdLinks[curNode.id].begin(); l!=fwdLinks[curNode.id].end(); ++l) {	// for each fwdLink
			unsigned int fwdNodeIdx = 0;
			for (vector<Lattice::Node>::iterator n=a.begin(); n!=a.end(); ++n) {							// for each node in lattice
				if (n->id == links[*l].E) {			// found fwdLink's end node in list
				   	if (fwdNodeIdx < i) {			// current node has to be moved prior to it's successors
						curNodeMoveFore_idx = fwdNodeIdx;
					}
					break; 							// stop looking for end node in list
				}
				fwdNodeIdx++;
			}
		}
		
		DBG_FORCE("  "<<i<<"\t->\t"<<curNodeMoveFore_idx);
		
		for (unsigned int j=i; j>curNodeMoveFore_idx; j--) {	// move all nodes between curNodeIdx and curNodeMoveFore_idx one step right
			a[j] = a[j-1];
		}
		a[curNodeMoveFore_idx] = curNode;			// move curNode on curNodeMoveFore_idx position
	}
	DBG_FORCE("sorting...done");
}

//--------------------------------------------------------------------------------
// Load lattice from HTK Standard Lattice File

int Lattice::loadFromHTKFile(const string filename, LatTime latStartTime, long p_lat_time_multiplier, bool onlyAddWordsToLexicon) {
	
	bool gzip=false;
	gzFile gz_lat_file = NULL;
	ifstream lat_file;
	if (filename.substr(filename.length()-3,3) == ".gz") {
		gzip = true;
		gz_lat_file = gzopen(filename.c_str(), "rb");
		if (gz_lat_file == NULL) {
			cerr << "Error: opening file " << filename << endl;
			return (2);		
		}
	} else {
		lat_file.open(filename.c_str()); // HTK Standard Lattice File
		
		if (!lat_file.good()) {
			cerr << "Error: opening file " << filename << endl;
			return (2);
		}
	}
	setFilename(filename);
	
	// first clear nodes and links
	N=0;
	L=0;
	nodes.clear();
	links.clear();
//	DBG("nodes.size:" << nodes.size());
	
	DBG("loadFromHTKFile");
	
	vector<Lattice::Node> sorterNodes;
	vector<Lattice::Node> nullNodes;	// for SRI lattices - null nodes are first => move them on the end of nodes list
	
	Lattice::Node tmpNode;
	Lattice::Link tmpLink;

	string s, line;
	char c; // current character read from HTK file
	int state = 0; // finite automat state
	bool epsilon = false; // epsilon transition
	RecordType recType = e_header; // type of current block (header, nodes, links)
	bool eow = false; // End Of Word (if delimiter is found)
	bool gotLink = false; // are values in tmpLink correctly read from input?
	bool gotNode = false; // are values in tmpNode correctly read from input?
	
	int counter = 0;
	
	while ( !(gzip?gzeof(gz_lat_file):(lat_file.eof())) ) {
		
		counter++;
//		if (counter % 50000)
//			cout << '.' << flush;
		
		// If epsilon transition is set, nothing is read from input file
		if (epsilon) {
			epsilon = false;
		} else {
			if (gzip) {
				c = gzgetc(gz_lat_file);
			} else {
				lat_file.get(c);
			}

			// If current character is delimiter
			if (isDelimiter(c)) {
				eow = true; // End Of Word
			
				if (c=='#')
					state=100; //comment
					
			} else {
				s += c;
			}				
		}
		
		
		if (eow) {
		
//			cout << "state:" << state << " eps:" << epsilon << endl;

			switch (state) {
				
				case 0:
					// N=, I= ---epsilon---> 2
					if (s.substr(0,2) == "N=") {
						N = atoi((s.substr(2)).c_str());
						// N is increased when new node is added in method addNode()
						state = 1;
					} else if (s.substr(0,6) == "NODES=") {
						N = atoi((s.substr(6)).c_str());
						state = 1;
					} else if (s.substr(0,2) == "I=") {
						recType = e_node;
						epsilon = true;
						state = 2;
					} else if (s.substr(0,6) == "LINKS=") {
						recType = e_node;
						epsilon = true;
						state = 2;
					}
					break;

				case 1:
					// L=
					if (s.substr(0,2) == "L=") {
						L = atoi((s.substr(2)).c_str());
						// L is increased when new link is added in method addLink()
						state = 2;
					} 
					else if (s.substr(0,6) == "LINKS=") {
						L = atoi((s.substr(6)).c_str());
						state = 2;				
					}
					break;

				case 2:
					// I=, J= ---epsilon---> 4
					if (s.substr(0,2) == "I=") {
						recType = e_node;
						tmpNode.id = atoi((s.substr(2)).c_str());
						gotNode = true;
						state = 3;
					} else if (s.substr(0,2) == "J=") {
						recType = e_link;
						epsilon = true;
						state = 4;
					}
					break;

				case 3:
					// t=, W=, v=
					if (s.substr(0,2) == "t=") {
						tmpNode.t = (atof((s.substr(2)).c_str()) / p_lat_time_multiplier) + latStartTime;
					} 
					else if (s.substr(0,2) == "W=") {
						/*
						string::size_type pos = 0;
						while ((pos = s.find("\\", pos)) != string::npos)
						{
							s.erase(pos,1);
						}
						*/
						tmpNode.W = s.substr(2);
					}
					else if (s.substr(0,2) == "v=") {
						tmpNode.v = atoi((s.substr(2)).c_str());
					} 
					else if (s.substr(0,2) == "p=") {
						tmpNode.p = atof((s.substr(2)).c_str());
					}
				break;


				case 4:
					// J=
					if (s.substr(0,2) == "J=") {
						recType = e_link;
						tmpLink.id = atoi((s.substr(2)).c_str());
						gotLink = true;
						state = 5;
					}
					break;

				case 5:
					// S=, E=, a=, l=
					if (s.substr(0,2) == "S=") {
						tmpLink.S = atoi((s.substr(2)).c_str());
					}
					else if (s.substr(0,2) == "E=") {
						tmpLink.E = atoi((s.substr(2)).c_str());
					}
					else if (s.substr(0,2) == "a=") {
						tmpLink.a = atof((s.substr(2)).c_str());
					}
					else if (s.substr(0,2) == "l=") {
						tmpLink.l = atof((s.substr(2)).c_str());
					}
					break;

					
				case 100:
					// comment
					break;
					
			} // switch (state)

			// end of record => save the record and go to the start state of current record type
			if (c=='\n' && !epsilon) {
				if (recType == e_header) {
					state = 0;
				} 
				else if (recType == e_node) {
					if (gotNode) {
/*						if (isSRIlattice && tmpNode.W == "!NULL" && !gotLastNullNode) {
							if (!onlyAddWordsToLexicon) 
//								nullNodes.push_back(tmpNode);
							if (tmpNode.p == 1 && tmpNode.id > 0) 
								gotLastNullNode = true;
						} else 
*/						
						{
							if (!onlyAddWordsToLexicon)
//								sorterNodes.push_back(tmpNode);
								addNode(tmpNode);
							else
								this->lexicon->getValID(tmpNode.W);
						}
					}
					tmpNode.init();
					gotNode = false;
//					addNode(tmpNode);
					state = 2;
//					recType = e_link;
				} 
				else if (recType == e_link) {
					if (gotLink) {
						addLink(tmpLink);
					}
					tmpLink.init();
					gotLink = false;
					state = 4;
//					recType = e_none;
				}
				s.clear();
			}

			// new word begins
			if (!epsilon) {
				s.clear();
				eow = false;
			}
			
		} // if (eow)

	} // while (!lat_file.eof())

	if (gzip) {
		gzclose(gz_lat_file);
	} else {
		lat_file.close();
	}


//	DBG_FORCE("nodes.size():"<<nodes.size());

	if (onlyAddWordsToLexicon)
		return 0; // if we are only parsing the lattice file for putting words to the lexicon, then it is done.



//	DBG("nodes.size():"<<sorterNodes.size()<<" links.size:"<<links.size()<<" fwdLinks.size:"<<fwdLinks.size()<<" bckLinks.size:"<<bckLinks.size());
//	for(vector<Lattice::Node>::iterator i=sorterNodes.begin(); i!=sorterNodes.end(); ++i) {
//		cout << *i << endl;
//	}

	// sort nodes by time
	
	// add null nodes to the end of sorterNodes vector
/*	if (isSRIlattice) {
		for (vector<Lattice::Node>::iterator i=nullNodes.begin(); i!=nullNodes.end(); ++i) {
			sorterNodes.push_back(*i);
		}
	}
*/
//	stable_sort(sorterNodes.begin(), sorterNodes.end()); 	// sort nodes by time

//	updateFwdBckLinks(); 							// needed by sortNodesByLinks() !!!
//	sortNodesByLinks(sorterNodes);

//	DBG_FORCE("original lat's endNodeID: "<< getEndNode());

/*	
	map<ID_t, ID_t> translator; 		// old id -> new id
	ID_t idCounter = 0; 				// new id generator
	nodes.clear();
	for (vector<Lattice::Node>::iterator i=sorterNodes.begin(); i!=sorterNodes.end(); ++i) {

		translator[i->id] = idCounter;
		i->id = idCounter;
		addNode(*i);
		idCounter++;
	}
*/


	// save translator file
/*
	string translatorFile = filename + ".trans";
	ofstream out(translatorFile.c_str());
	for(map<ID_t, ID_t>::iterator i=translator.begin(); i!=translator.end(); ++i) {
		
		out << i->first << "->" << i->second << endl;
	}
	out.close();
*/
	
	
	// update start and end node id in each link
/*	for (LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
		(i->second).S = translator[(i->second).S];
		(i->second).E = translator[(i->second).E];
	}

	updateFwdBckLinks();
*/	
	/*
	DBG_FORCE("================================================================================");
	map<ID_t,void*> nodeCluster;
	map<ID_t,int> nodeClusterWeights;
	LatTime t = 0;
	int cnt = 0;
	DBG_FORCE("nodes.size():"<<nodes.size()<<"\tlinks.size():"<<links.size());
	for (NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		if ((i->second).t == t) {
			cnt++;
			nodeCluster[((i->second).id)] = NULL;
			nodeClusterWeights[((i->second).id)] = 1;
		} else {
			
			DBG_FORCE("t:"<<t<<"\t"<<cnt<<"\t"<<nodeCluster.size());
			nodeCluster.clear();
			t = (i->second).t;
			cnt=0;
			
			if (nodeCluster.size() > 0) { 
				for (int j=0; j<nodeCluster.size(); j++) {	// do it as much times as nodes count in same time
					for (map<ID_t,void*>::iterator n=nodeCluster.begin(); n!=nodeCluster.end(); ++n) {	// for each node in same time
						for (LinkMap::iterator l=links.begin(); l!=links.end(); ++l) {	// for each link in lattice
							if ((l->second).E == n->first && nodes[(l->second).S].t == nodes[n->first].t) {	// select only links with start nodes in same time and end node n
								nodeClusterWeights[n->first] += nodeClusterWeights[(l->second).S];	// add weight of incoming link's node (Standa knows:o)
							}
						}
					}
				}
				//sort by links
				for (map<ID_t,int>::iterator w=nodeClusterWeights.begin(); w!=nodeClusterWeights.end(); ++w) {
					cout << w->second << " ";
				}
				cout << endl << flush;
				
			}
			nodeCluster.clear();
			nodeClusterWeights.clear();
			t = (i->second).t;
			nodeCluster[((i->second).id)] = NULL;
			nodeClusterWeights[((i->second).id)] = 1;
			
		}
	}
	DBG_FORCE("================================================================================");
	*/
	
/*	
	// swap last node in map with lattice's end node
	ID_t endNodeID = getEndNode(); 	// end node in lattice
	ID_t lastNodeID = getLastNodeID(); 	// last node in nodes map
	Node endNode = nodes[endNodeID]; 	// save end node
	Node lastNode = nodes[lastNodeID];	// save last node
	
	nodes.erase(endNodeID);				// remove end and last node from map
	nodes.erase(lastNodeID);
	
	ID_t idTmp = endNode.id;			// swap id of end and last node
	endNode.id = lastNode.id;
	lastNode.id = idTmp;
	
	addNode(lastNode);					// add nodes with swapped IDs back into the map
	addNode(endNode);

	for (LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {	// apply the changes also on links
		if ((i->second).S == endNodeID)  (i->second).S = -2;		// prevent overwriting of new value by next row
		if ((i->second).S == lastNodeID) (i->second).S = endNodeID;
		if ((i->second).E == endNodeID)  (i->second).E = -2;		// prevent overwriting of new value by next row
		if ((i->second).E == lastNodeID) (i->second).E = endNodeID;
	}

	for (LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {	// apply the changes also on links
		if ((i->second).S == -2)  (i->second).S = lastNodeID;		// now it is safe to set these values
		if ((i->second).E == -2)  (i->second).E = lastNodeID;		// ...
	}
*/
//	sortLattice(); // updateFwdBckLinks() is called before and after sorting within sortLattice()

	DBG("nodes.size():"<<nodes.size());
//	DBG_FORCE("fwdLinks[1].size():"<<fwdLinks[1].size());
	
//	DBG_FORCE("recursiveLatProcessing ...");
//	recursiveLatProcessing((nodes.begin())->first);
//	DBG_FORCE("recursiveLatProcessing done")

	DBG("nodes.size():"<<nodes.size()<<" links.size:"<<links.size()<<" fwdLinks.size:"<<fwdLinks.size()<<" bckLinks.size:"<<bckLinks.size());

	updateFwdBckLinks();
	return 0;
}



//--------------------------------------------------------------------------------
// Save lattice to HTK Standard Lattice File
// lattice is gzipped if output file extension is .gz

void Lattice::saveToHTKFile(const string filename, bool outputPosteriors, bool outputLikelihoods) {

/*std::cout << "***************************************************************************" << std::endl ;
   for (LinkMap::iterator iLink = links.begin(); iLink != links.end(); ++iLink)
  {
    std::cout << (iLink->second).id << " " << (iLink->second).S << " " << (iLink->second).E << " " << (iLink->second).a << std::endl;
  }//delete nodes

  */
	if (filename.substr(filename.length()-3,3) == ".gz") {
		
		// write COMPRESSED lattice
		
		gzFile gz_out = gzopen(filename.c_str(), "wb");

		// write header info (N,L)
		gzprintf(gz_out, "N=%d\tL=%d\n", N, L);
		
		// write all nodes (with wordID instead of word)
		for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
			
			if (outputPosteriors) {
				gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tv=%d\tp=%f\n", (i->second).id, (i->second).t, (i->second).W.c_str(), (i->second).v, (i->second).p);
			} else if (outputLikelihoods) {
				gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tv=%d\tlik=%f\n", (i->second).id, (i->second).t, (i->second).W.c_str(), (i->second).v, (i->second).bestLikelihood);
			} else {
				gzprintf(gz_out, "I=%d\tt=%f\tW=%s\tv=%d\n", (i->second).id, (i->second).t, (i->second).W.c_str(), (i->second).v);
			}
		}

		// write all links
		for(LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
			
			gzprintf(gz_out, "J=%d\tS=%d\tE=%d\ta=%f\tl=%f\n", (i->second).id, (i->second).S, (i->second).E, (i->second).a, (i->second).l);
		}		

		gzclose(gz_out);
		
	} else {

		// write UNCOMPRESSED lattice

		ofstream out(filename.c_str());
		// write header info (N,L)
		out << "N=" << N << "\tL=" << L << endl;
		
		// write all nodes (with wordID instead of word)
		for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
			if( (i->second).W != "!NULL"){
			  out << "I="	<< (i->second).id << "\tt=" << (i->second).t << "\tW=" << (i->second).W << "\tv=" << (i->second).v;
                        }else{
                          out << "I="   << (i->second).id << "\tt=" << (i->second).t << "\tW=" << (i->second).W;
                        }  
		   	if (outputPosteriors) {
				out << "\tp=" << (i->second).p;
			}
			if (outputLikelihoods) {
				out << "\tlik=" << (i->second).bestLikelihood;
			}
			out << endl;
		}

		// write all links
		for(LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
			
			out << "J="	<< (i->second).id << "\tS=" << (i->second).S << "\tE=" << (i->second).E << "\ta=" << (i->second).a << "\tl=" << (i->second).l;
			if (outputLikelihoods) {
				out << "\tlik=" << (i->second).confidence;
			}
		   	out << endl;
		}		

		out.close();
	}
}



/*--------------------------------------------------------------------------------
 * Save to dot file for use with graphviz - http://www.research.att.com/sw/tools/graphviz/
 *
 */
void Lattice::saveToDotFile(const string filename) {
	
	typedef map<float, int> float2intMap; // set of all nodes times

	ofstream out(filename.c_str());
	// write header
	out << "digraph LATTICE {" << endl 
		<< "rankdir = LR" << endl
//		<< "size = \"100000,100000\"" << endl
		<< "label = \"\"" << endl
		<< "center = 1" << endl
		<< "nodesep = \"0.250000\"" << endl
		<< "ranksep = \"0.400000\"" << endl
		<< "orientation = Landscape" << endl;
	

	// create and fill set of all nodes times 	
	float2intMap time_axis;
	int time_axis_index = 0;
	for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		
		time_axis[(i->second).t] = time_axis_index++;
	}
	
	
	
	// nodes clustering (by time)
	for(float2intMap::iterator i=time_axis.begin(); i!=time_axis.end(); ++i) {
		
		out << "{rank=same; T" << (i->second);
		for(NodeMap::iterator j=nodes.begin(); j!=nodes.end(); ++j) {
			if ((j->second).t == (i->first)) {
				out << " " << (j->second).id;
			}
		}
		out << ";}" << endl;
	}
	
	
	// write all nodes (with wordID instead of word)
	for(NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
//		int color = (int)(exp((i->second).bestLikelihood) * 255);
		
		if ((i->second).W == "!CONN") {
			out << (i->second).id << " [label = \"\", shape=diamond, style=bold, fontsize=14, width=0.5, height=0.5, color=gold]" << endl;
		} else if ((i->second).W == "!NULL") {
			out << (i->second).id << " [label = \"" << (i->second).id << ":0\", shape=circle, style=bold, fontsize=14, width=0.5, height=0.5, color=dodgerblue]" << endl;
		} else {
//			out << (i->second).id << " [label = \"" << (i->second).id << ":" << (i->second).W << "\", shape = circle, style = bold, fontsize = 14]" << endl;
			out << (i->second).id << " [label = \"" << (i->second).id << ":"<< (i->second).W << "\", shape = circle, style = bold, fontsize = 14]" << endl;
		}
			
		for(LinkMap::iterator j=links.begin(); j!=links.end(); ++j) {
			if ((j->second).S == (i->second).id) {
				out << "  " << (j->second).S << " -> " << (j->second).E << " [label = \"" << (j->second).confidence_noScaleOnKeyword << "\"]" << endl;// << " [label = \"\", fontsize = 14];" << endl;
			}
		}		
	}

	out << "subgraph time_axe {" << endl;
	for(float2intMap::iterator i=time_axis.begin(); i!=time_axis.end(); ++i) {
		
		out << " T" << (i->second) << " [label=\"" << (i->first) << "\" shape=house, fontsize=10, color=lightblue]" << endl;
	}

	bool first=true;
	for(float2intMap::iterator i=time_axis.begin(); i!=time_axis.end(); ++i) {
		
		if (first) {
			first = false;
		} else {
			out << " -> ";
		}
		
		out << " T" << (i->second);
	}
	out << endl;

	out << "}" << endl;
		
	out << "}" << endl;

	out.close();

}



/*--------------------------------------------------------------------------------
 *	searchResult()
 */
/*
int Lattice::searchWord(const long position, const string filename, vector<Lattice::Node> &result) {
	
	// read COMPRESSED binary
	
	gzFile gz_in = gzopen(filename.c_str(), "rb");
	if (gz_in == NULL) {
		return (2);
	}

	Lattice::Node tmpNode;
	ID_t wordID;
	
	gzseek(gz_in, position, SEEK_SET);
	
	// read all nodes and convert wordID into word using lexicon
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.id)), sizeof(tmpNode.id));
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.t)), sizeof(tmpNode.t));
	gzread(gz_in, reinterpret_cast<char *>(&wordID), sizeof(wordID));
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.v)), sizeof(tmpNode.v));
	gzread(gz_in, reinterpret_cast<char *>(&(tmpNode.p)), sizeof(tmpNode.p));

	tmpNode.W = lexicon->getVal(wordID);
	result.push_back(tmpNode);

	gzclose(gz_in);

	return 0;
}
*/


/*--------------------------------------------------------------------------------
 *	updateFwdBckLinks()
 */
void Lattice::updateFwdBckLinks(UpdateLinksType type) {
	
	if (type == forward) {
		fwdLinks.clear();
		for (LinkMap::iterator i = links.begin(); i != links.end(); ++i) {
			fwdLinks[(i->second).S].push_back(i->first);
		}

	} else if (type == backward) {
		bckLinks.clear();
		for (LinkMap::iterator i = links.begin(); i != links.end(); ++i) {
			bckLinks[(i->second).E].push_back(i->first);
		}
	
	} else if (type == both) {
		fwdLinks.clear();
		bckLinks.clear();
		for (LinkMap::iterator i = links.begin(); i != links.end(); ++i) {
			fwdLinks[(i->second).S].push_back(i->first);
			bckLinks[(i->second).E].push_back(i->first);
//			DBG("<UPDATE LINKS> id:" << i->first << "\t" << (i->second).S << "\t->\t" << (i->second).E);
		}
	}
	sortFwdBckLinks();
}


void Lattice::sortFwdBckLinks_insertsort(vector<ID_t> &s) {
//void insertionsort(int s[], int length){
  unsigned int i, j;
  int elem;
  
  for(i=1; i < s.size(); i++){
    elem=s[i]; 
    j=i;
    for(; j > 0 && links[elem].confidence > links[s[j-1]].confidence; j--)
      s[j]=s[j-1];
    s[j]=elem;
  }
}



void Lattice::sortFwdBckLinks() {
	for (FwdLinks::iterator i = fwdLinks.begin(); i != fwdLinks.end(); ++i) {
		sortFwdBckLinks_insertsort(i->second);
/*		DBG_FORCE("nodeID: "<<i->first);
		for (vector<ID_t>::iterator j = i->second.begin(); j != i->second.end(); ++j) {
			DBG_FORCE("  fwdLink: "<<links[*j]);
		}*/
	}
	
	for (BckLinks::iterator i = bckLinks.begin(); i != bckLinks.end(); ++i) {
		sortFwdBckLinks_insertsort(i->second);
	}
}


void Lattice::printFwdBckLinks() {
	for (LinkMap::iterator i = links.begin(); i != links.end(); ++i) {
//		for (vector<ID_t>)
	}	
}


bool Lattice::nodeExists(ID_t id) {
	NodeMap::iterator i = nodes.find(id);
	if (i == nodes.end())
		return false;
	else 
		return true;

}


void Lattice::GetBestPath(vector<ID_t>* pBestPath, ID_t* pStartNode)
{
	// ***** FORWARD LINKS *****
	//

	ID_t cur_node_id;
	if (pStartNode == NULL)
	{
		cur_node_id = nodes.begin()->first;
	}
	else
	{
		if (*pStartNode == -1)
		{
			*pStartNode = nodes.begin()->first;
		}
		cur_node_id = *pStartNode;
	}

	while (1) 
	{
		// if there isn't node with id==cur_node_id then stop forward links processing
//		DBG_FORCE("node #"<<cur_node_id<<" fwdLinks.size():"<<fwdLinks[cur_node_id].size());
		if (fwdLinks[cur_node_id].size() == 0) 
		{
//			DBG_FORCE("node #"<<cur_node_id<<" has no fwdLinks");
			break;
		}

		ID_t best_link_id = -1;

		for(vector<ID_t>::iterator i=fwdLinks[cur_node_id].begin(); i!=fwdLinks[cur_node_id].end(); ++i) 
		{
//			DBG_FORCE("link #" << *i << "   " << links[*i].S << "--("<<links[*i].confidence_noScaleOnKeyword<<")-->" << links[*i].E);
			if (links[best_link_id].confidence_noScaleOnKeyword < links[*i].confidence_noScaleOnKeyword) 
			{
				best_link_id = *i;
			}
		}
		pBestPath->push_back(best_link_id);

//		DBG_FORCE("Best link: id=" << best_link_id << "   " << links[best_link_id].S << "--("<<links[best_link_id].confidence_noScaleOnKeyword<<")-->" << links[best_link_id].E);

		cur_node_id = links[best_link_id].E;
	}
}


/*
int Lattice::getNodeContext(LatTime startTime, ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, Hypothesis &hyp) 
{	
	if (startTime < 0) 
		startTime=0;

	hyp.stream = this->stream;
	hyp.record = this->record;
	
	string context = ""; //nodes[centerNodeID].W;
	Hypothesis::Word centerNodeHypWord;
	centerNodeHypWord.str = lexicon->getVal(nodes[centerNodeID].W_id);
	centerNodeHypWord.start = -1; // we don't know node's start time
	centerNodeHypWord.end = nodes[centerNodeID].t;
	centerNodeHypWord.conf = nodes[centerNodeID].p;

	Hypothesis::Word curHypWord = centerNodeHypWord;
	
	
	DBG("***** getNodesContext *****");
	DBG("centerNodeID:" << centerNodeID << " fwdNodesCount:" << fwdNodesCount << " bckNodesCount:" << bckNodesCount);

	
	ID_t curNodeID = centerNodeID;
	int depth = 0;
	ID_t bestNodeID = centerNodeID;
	string bestWord;


	// ***** BACKWARD LINKS *****
	//
	DBG("******* BACKWARD LINKS *******");
	curNodeID = centerNodeID;
	depth = 0;

	
	while (depth <= bckNodesCount) {
		
		FwdLinks::iterator i = fwdLinks.find(curNodeID);
		if (i == fwdLinks.end()) {
			break;
		} 	


		// don't add "!NULL" to context string
		if (nodes[curNodeID].W_id != lexicon->nullWordID) {
			
			// set new values for hypothesis word
			if (curNodeID != centerNodeID)
				hyp.setFrontWordStartTime(nodes[curNodeID].t);
			
			curHypWord.str = lexicon->getVal(nodes[curNodeID].W_id);
			curHypWord.end = nodes[curNodeID].t;
			curHypWord.conf = nodes[curNodeID].p;
			hyp.push_front_word(curHypWord);
			DBG("Hypothesis.push(): " << curHypWord.str);
		
			context = ' ' + context;
			context = nodes[curNodeID].W + context;
		}

		
		DBG("DEPTH:" << depth);
		DBG("LINKS COUNT: " << fwdLinks[curNodeID].size() );

		bestNodeID = -1;
		for(vector<ID_t>::iterator i=bckLinks[curNodeID].begin(); i!=bckLinks[curNodeID].end(); ++i) {
			DBG("links["<< *i <<"]: " << links[*i] << "\t startNode:" << nodes[links[*i].S]);

			if (nodes[bestNodeID].p < nodes[links[*i].S].p) {
				bestNodeID = links[*i].S;
			}
		}

		DBG("Best node: id=" << bestNodeID << " word=" << nodes[bestNodeID]);

		curNodeID = bestNodeID;
		// if center node hasn't been added yet, add it into hyp's keyword list
		if (centerNodeHypWord.start == -1) {
			centerNodeHypWord.start = nodes[curNodeID].t;
			hyp.addKeyword(centerNodeHypWord);
		}

		
		// increase depth
		if(nodes[curNodeID].W_id != lexicon->nullWordID) 
			depth++;

		DBG("Context:" << context);
//		cin.get();
	}
	hyp.setFrontWordStartTime(startTime); // set start time of first node

	
	DBG("Backward links done...depth=" << depth << " maxDepth=" << fwdNodesCount);


	DBG("FORWARD LINKS");
	// ***** FORWARD LINKS *****
	//
	LatTime predNodeEndTime = centerNodeHypWord.end;
	curNodeID = centerNodeID;
	depth = 0;

	while (depth <= fwdNodesCount) {

		// if there isn't node with id==curNodeID then stop forward links processing
		FwdLinks::iterator iFind = fwdLinks.find(curNodeID);
		if (iFind == fwdLinks.end()) {
			break;
		}
		
		DBG("DEPTH:" << depth);
		DBG("LINKS COUNT: " << fwdLinks[curNodeID].size() );
			
		bestNodeID = -1;
		for(vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
			DBG("links["<< *i <<"]: " << links[*i] << "\t endNode: " << nodes[links[*i].E]);
			if (nodes[bestNodeID].p < nodes[links[*i].E].p) {
				bestNodeID = links[*i].E;
			}
		}

		DBG("Best node: id=" << bestNodeID << " word=" << nodes[bestNodeID].W);

		curNodeID = bestNodeID;
		// don't add "!NULL" to context string
		if (nodes[bestNodeID].W_id != lexicon->nullWordID) {
			// add best node's word to hypothesis
			curHypWord.str = lexicon->getVal(nodes[bestNodeID].W_id);
			curHypWord.start = predNodeEndTime;
			curHypWord.end = nodes[bestNodeID].t;
			curHypWord.conf = nodes[bestNodeID].p;
			hyp.push_back_word(curHypWord);
			
			predNodeEndTime = curHypWord.end;

			context += ' ';
			context += nodes[bestNodeID].W;
			// increase depth
			depth++;
		}
		DBG("Context:" << context);
//		cin.get();
	}



	return 0;
}
*/


void Lattice::sortNodesByTime() {
/*
	vector<LatticeNodeForSorter> sortedNodes;
	LatticeNodeForSorter tmpSorterNode;
	for (NodeMap::iterator i=nodes.begin(); i!=nodes.end(); ++i) {
		tmpSorterNode.idOld = i->id;
		tmpSorterNode.t = i->t;
		sortedNodes.push_back(tmpSorterNode);
	}

	sort(sortedNodes.begin(), sortedNodes.end());

	ID_t idCounter = 0;
	for (vector<LatticeNodeForSorter>::iterator i=sortedNodes.begin(); i!=sortedNodes.end(); ++i) {
		i->id = idCounter++;
	}

	//update each link's start and end nodes
	for (LinkMap::iterator i=links.begin(); i!=links.end(); ++i) {
		i->
	}
*/
}



//typedef std::map< lse::ID_t, int > T_NumOfDirectAncestors;
//typedef std::map< lse::ID_t, lse::ID_t > T_IDMappingmap;

//ID_t Lattice::getNodeWithZeroAnc(T_NumOfDirectAncestors &in_NumOfDirectAncestors)
ID_t Lattice::getNodeWithZeroAnc(int *in_NumOfDirectAncestors, LatTime *nodeTimes)
{
	ID_t pomNodeID = ID_UNDEF;
	double MinTime = 9e99;
	for(int i = 0; i < (int)nodes.size(); i++)
	{
		if( (in_NumOfDirectAncestors[i] == 0) && (MinTime > nodeTimes[i]) )
		{
			MinTime = nodeTimes[i];
			pomNodeID = i;
		};//if
	};//for
	return pomNodeID;
};//ID_t GetNodeWithZeroAnc()



void Lattice::sortLattice()
{
	DBG_FORCE("Lattice::sortLattice()");
//	T_NumOfDirectAncestors NumOfDirectAncestors;
	int NumOfDirectAncestors[nodes.size()];
//	T_IDMappingMap IDMappingMap;
	int IDMappingMap[nodes.size()];

	LatTime nodeTimes[nodes.size()];
	
	int NewNodeIDCounter=0;
	ID_t CurrentNode;

	updateFwdBckLinks(both);

	BckLinks::iterator BckLinks_it;
	for(NodeMap::iterator Node_it = nodes.begin(); Node_it != nodes.end(); ++Node_it) 
	{
		nodeTimes[Node_it->first] = Node_it->second.t;
		BckLinks_it = bckLinks.find(Node_it->first);
		if( BckLinks_it != bckLinks.end() )
		{
			NumOfDirectAncestors[Node_it->second.id] = BckLinks_it->second.size();
		}
		else
		{
			NumOfDirectAncestors[Node_it->second.id] = 0;
		};//if
		//    std::cerr << Node_it->second.id << "->" << NumOfDirectAncestors[Node_it->second.id] << " " ;
	};//for
	//  std::cerr << std::endl;
	CurrentNode = getNodeWithZeroAnc(NumOfDirectAncestors, nodeTimes);
	while( CurrentNode != ID_UNDEF)
	{
		//    std::cerr << "Detected node with Zero " << CurrentNode << std::endl;
		FwdLinks::iterator NodeFwdLinks_it = fwdLinks.find(CurrentNode);
		if( NodeFwdLinks_it != fwdLinks.end() )
		{
			for(std::vector<ID_t>::iterator FwdLinks_it = NodeFwdLinks_it->second.begin(); FwdLinks_it != NodeFwdLinks_it->second.end(); ++FwdLinks_it)
			{
				NumOfDirectAncestors[links[*FwdLinks_it].E]--;
			};//for
		};//if
		IDMappingMap[CurrentNode] = NewNodeIDCounter++;
		NumOfDirectAncestors[CurrentNode] = -1;
		CurrentNode = getNodeWithZeroAnc(NumOfDirectAncestors, nodeTimes);
/*
		for(T_NumOfDirectAncestors::iterator NodeID_it = NumOfDirectAncestors.begin(); NodeID_it != NumOfDirectAncestors.end(); ++NodeID_it){
			std::cerr << NodeID_it->first << "->" << NodeID_it->second << " " ;
		};//for
		std::cerr << std::endl;
*/
	};//

	for(int i = 0; i < (int)nodes.size(); i++)
	{
		if( (NumOfDirectAncestors[i] > 0) )
		{
			std::cerr << "ERROR VOLE!! Some of nodes were not proccessed NumOfDirectAncestors[" <<i<< "] = " << NumOfDirectAncestors[i] << std::endl << std::flush;
		};//if
	};//for

	for(int i = 0; i < (int)nodes.size(); i++)
	{
		DBG( "Old ID " << i << " new ID " << IDMappingMap[i] );
	};//for

	NodeMap NewNodeMap;
	//  NewNodeMap = new NodeMap;
	for(NodeMap::iterator NodeID_it = nodes.begin(); NodeID_it != nodes.end(); ++NodeID_it)
	{
		//    (*NewNodeMap)[IDMappingMap[NodeID_it->second.id]] = NodeID_it->second;
		//    (*NewNodeMap)[IDMappingMap[NodeID_it->second.id]].id = IDMappingMap[NodeID_it->second.id];
		NewNodeMap[IDMappingMap[NodeID_it->second.id]] = NodeID_it->second;
		NewNodeMap[IDMappingMap[NodeID_it->second.id]].id = IDMappingMap[NodeID_it->second.id];
	};//
	nodes.clear();
	nodes = NewNodeMap;

	for(LinkMap::iterator LinkID_it = links.begin(); LinkID_it != links.end(); ++LinkID_it)
	{
		LinkID_it->second.S = IDMappingMap[LinkID_it->second.S];
		LinkID_it->second.E = IDMappingMap[LinkID_it->second.E];
	};//

	updateFwdBckLinks(both);

	DBG_FORCE("Lattice::sortLattice()...done");
};//void Lattice::sortLattice(){



void Lattice::setFilename(const string filename) {

	DBG("Lattice::setFilename(): "<< filename);
	this->filename = filename;

	// Set lattice's record name (= filename without extension)
	this->record = filename;
	string::size_type pos = (this->record).rfind ('/');
	if (pos != string::npos) {
		string::size_type pos2 = (this->record).find ('.', pos);
		(this->record).erase(pos2);
	}
	
	this->stream = "LVCSR";
	
	DBG("record:" << record << " stream:" << stream);
}


ID_t Lattice::getEndNode_recursive(ID_t curNodeID) {
//	static ID_t endNodeID = -1;

	// if current node has no successor
	if (fwdLinks[curNodeID].size() == 0) {
//		DBG_FORCE("  FOUND END NODE");
/*		if (endNodeID == -1) {
			endNodeID = curNodeID;
			DBG_FORCE("endNodeID:" << endNodeID);
		} else if (curNodeID != endNodeID)
			DBG_FORCE("endNodeID:" << endNodeID << " curNodeID:" << curNodeID);
		Link endLink;
		endLink.id = getLastLinkID() + 1;
		endLink.S = curNodeID;
		endLink.E = getLastNodeID();
		endLink.a = 1;
		endLink.l = 1;
		DBG_FORCE("Adding link: "<<endLink);
		addLink(endLink);*/
		return curNodeID;
	} 
	
	ID_t res;
	for (vector<ID_t>::iterator i=fwdLinks[curNodeID].begin(); i!=fwdLinks[curNodeID].end(); ++i) {
		// call getEndNode_recursive for all nodes outgoing from current node
		if ((res = getEndNode_recursive(links[*i].E)) != -1)
			return res;
	}
	return -1;	
}

ID_t Lattice::getEndNode() {
	NodeMap::iterator i = nodes.begin();
	return getEndNode_recursive(i->first);
}

void Lattice::makeOnlyOneEndNode() {
	NodeMap::iterator i = nodes.begin();
	DBG_FORCE("EndNodeID: "<<getEndNode_recursive(i->first));
	
/*	
	DBG_FORCE("nodes:"<<nodes.size()<<" links:"<<links.size());
	Node endNode;
	// get last node's ID
	ID_t lastNodeID = getLastNodeID();
	// insert end node into nodes map
	endNode.id = nodes[lastNodeID].id + 1;
	endNode.t = nodes[lastNodeID].t+1;
	endNode.W_id = lexicon->nullWordID;
	endNode.W = "!NULL";
	endNode.v = 1;
	endNode.p = 1;
	DBG_FORCE("Adding node: "<<endNode);
	addNode(endNode);
	
	// insert links going from all end nodes into new end node
	NodeMap::iterator i = nodes.begin();
	getEndNode_and_addLink_recursive(i->first);
	updateFwdBckLinks();
	DBG_FORCE("nodes:"<<nodes.size()<<" links:"<<links.size());
*/	
}


ID_t Lattice::getLastNodeID() {
	NodeMap::reverse_iterator lastIter = nodes.rbegin();
	return lastIter->first;
}

ID_t Lattice::getLastLinkID() {
	LinkMap::reverse_iterator lastIter = links.rbegin();
	return lastIter->first;
}
/*
Lattice::computeAllLinkLikelihood() {

}
*/


void Lattice::trimTimeInterval(LatTime tStart, LatTime tEnd) {

	// delete links
	for (LinkMap::iterator l=links.begin(); l!=links.end(); ++l) {
		ID_t S = (l->second).S;
		if (nodes[S].t < tStart || nodes[S].t > tEnd) {
			links.erase(l);
			continue;
		} 
		ID_t E = (l->second).E;
		if (nodes[E].t < tStart || nodes[E].t > tEnd) {
			links.erase(l);
			continue;
		}
	}
	this->L = links.size();

	// delete nodes
	for (NodeMap::iterator n=nodes.begin(); n!=nodes.end(); ++n) {
		LatTime t = (n->second).t;
		if (t < tStart || t > tEnd) {
			nodes.erase(n);
		}
	}
	
	this->N = nodes.size();
}

/*
void Lattice::transform2ngram_recursive(ID_t firstNodeID, ID_t curNodeID, int maxDepth, int curDepth, string W, Lattice* outLat, map<ID_t,ID_t>* translator) {
	curDepth++; 				// go deeper
	W += (W=="" ? "" : "_") + nodes[curNodeID].W; 		// add current node to the n-gram

	if (curDepth == maxDepth) {
		// create a new node
		Lattice::Node newNode;
		newNode.id = outLat->getLastNodeID();
		newNode.t = nodes[curNodeID].t;
		newNode.W = W;
		outLat->addNode(newNode);
		*translator[firstNodeID] = newNode.id;
		*translator[curNodeID] = newNode.id;

		// if there is some preceding node
		for (vector<ID_t>::iterator l=bckLinks[firstNodeID].begin(); l!=bckLinks[firstNodeID].end(); ++l) {
			Lattice::Link newLink;
			newLink.id = outLat->getLastLinkID();
			newLink.S = *translator[];
		}
		
		DBG_FORCE("curDepth: "<<curDepth<<"\t ngram: "<<W);
		return;
	}

	if (fwdLinks[curNodeID].size() == 0) {
		return;
	}
	

	for (vector<ID_t>::iterator l=fwdLinks[curNodeID].begin(); l!=fwdLinks[curNodeID].end(); ++l) {
		int nextNodeID = links[*l].E;
		// continue generating current ngram
		transform2ngram_recursive(firstNodeID, nextNodeID, maxDepth, curDepth, W, outLat);
		// start generating a new ngram begining at current node
		transform2ngram_recursive(curNodeID, nextNodeID, maxDepth, 0, "", outLat);
	}
}

void Lattice::transform2ngram(int n, lse::Lattice* outLat) {
//	Queue<ID_t> waitingNodes;

	map<ID_t,ID_t> translator; // original nodeID -> ngram nodeID

	transform2ngram_recursive(nodes.begin()->second.id, nodes.begin()->second.id, n, 0, "", outLat);
	
//	for (NodeMap::iterator n=nodes.begin(); n!=nodes.end(); ++n) {	}
}
*/


Lattice::Link* Lattice::GetLink(ID_t fromNodeID, ID_t toNodeID)
{
	for (vector<ID_t>::iterator iLinkID = fwdLinks[fromNodeID].begin(); iLinkID != fwdLinks[fromNodeID].end(); ++iLinkID)
	{
		Lattice::Link* pLink = &(links[*iLinkID]);
		if (pLink->E == toNodeID)
		{
			return pLink;
		}
	}
	return NULL;
}

void Lattice::detectFirstLastNode()
{
  bool foundFirst=false,foundLast=false;
  firstNodeID=-1;
  lastNodeID=-1;

  for( NodeMap::iterator iNode=nodes.begin(); iNode!=nodes.end(); ++iNode )
  {
    if(fwdLinks[iNode->first].size() == 0){
      if( foundLast ){ std::cerr << "ERROR: Lattice has more then one end point" << std::endl << std::flush; }
      foundLast=true;
      lastNodeID=iNode->first;
      std::cout << "Lattice has end point " << lastNodeID << std::endl << std::flush;

      if( (iNode->first) != lastNode().id){
        for( std::vector<ID_t>::iterator iLinkID=bckLinks[lastNode().id].begin(); iLinkID!=bckLinks[lastNode().id].end(); ++iLinkID ){
          links[*iLinkID].E=iNode->first;
        }//for
        for( std::vector<ID_t>::iterator iLinkID=fwdLinks[lastNode().id].begin(); iLinkID!=fwdLinks[lastNode().id].end(); ++iLinkID ){
          links[*iLinkID].S=iNode->first;
        }//for
        for( std::vector<ID_t>::iterator iLinkID=bckLinks[iNode->first].begin(); iLinkID!=bckLinks[iNode->first].end(); ++iLinkID ){
          links[*iLinkID].E=lastNode().id;
        }//for
        

        Node PomNode;
        PomNode=nodes[lastNode().id];
        nodes[lastNode().id]=iNode->second;
        nodes[iNode->first]=PomNode;
        nodes[lastNode().id].id=lastNode().id;
        nodes[iNode->first].id=iNode->first;

      }//if
      
    }
    if(bckLinks[iNode->first].size() == 0){
      if( foundFirst ){ std::cerr << "ERROR: Lattice has more then one start point" << std::endl << std::flush; }
      foundFirst=true;
      firstNodeID=iNode->first;
      std::cout << "Lattice has start point " << firstNodeID << std::endl << std::flush;

      if( (iNode->first) != 0){
        for( std::vector<ID_t>::iterator iLinkID=fwdLinks[iNode->first].begin(); iLinkID!=fwdLinks[iNode->first].end(); ++iLinkID ){
          links[*iLinkID].S=0;
        }//for
        for( std::vector<ID_t>::iterator iLinkID=fwdLinks[0].begin(); iLinkID!=fwdLinks[0].end(); ++iLinkID ){
          links[*iLinkID].S=iNode->first;
        }//for      
        for( std::vector<ID_t>::iterator iLinkID=bckLinks[0].begin(); iLinkID!=bckLinks[0].end(); ++iLinkID ){
          links[*iLinkID].E=iNode->first;
        }//for      
        Node PomNode;
        PomNode=nodes[0];
        nodes[0]=iNode->second;
        nodes[iNode->first]=PomNode;
        nodes[0].id=0;
        nodes[iNode->first].id=iNode->first;
      }//if
      
    }
  }
  if( ! foundLast ){ std::cerr << "ERROR: Lattice has no end point" << std::endl << std::flush; }
  if( ! foundFirst ){ std::cerr << "ERROR: Lattice has no start point" << std::endl << std::flush; }

  updateFwdBckLinks();
  
}



void Lattice::cleanLattice()
{
  std::cout << "Lattice cleaning" << std::endl;
  bool WasChange=true;
  while( WasChange ){
    WasChange=false; 
    for (std::map<ID_t, Lattice::Node>::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
    {
      if( ((iNode->second).id != firstNodeID) && ((iNode->second).id != lastNodeID) &&
          ((fwdLinks[(iNode->second).id].size() == 0 ) || (bckLinks[(iNode->second).id].size() == 0 )) )
      {
        deleteNode((iNode->second).id);
        WasChange=true;
        break;
      }
    }
  }  
}//void Lattice::cleanLattice()


void Lattice::deleteLink(ID_t LinkID)
{
  std::vector<ID_t>::iterator DeleteMe;
  std::map<ID_t, Lattice::Link>::iterator iLink;
  
  //std::cout << "Deleting Link ID " << LinkID << " form " << links[LinkID].S << " to " << links[LinkID].E << std::endl;

  DeleteMe=fwdLinks[links[LinkID].S].end();
  for( std::vector<ID_t>::iterator iLinkID=fwdLinks[links[LinkID].S].begin(); iLinkID!=fwdLinks[links[LinkID].S].end(); ++iLinkID )
  { 
    if( *iLinkID == LinkID ){
      DeleteMe=iLinkID;
      //std::cout << "Found in fwdlinks " << *iLinkID << std::endl;
      break;
    }
  }//for erase from fwdlinks
  if( DeleteMe!=fwdLinks[links[LinkID].S].end() )
  {
    fwdLinks[links[LinkID].S].erase(DeleteMe);
  }//if  

  DeleteMe=bckLinks[links[LinkID].E].end();
  for( std::vector<ID_t>::iterator iLinkID=bckLinks[links[LinkID].E].begin(); iLinkID!=bckLinks[links[LinkID].E].end(); ++iLinkID )
  {
    if( *iLinkID == LinkID ){
      DeleteMe=iLinkID;
      //std::cout << "Found in bcklinks " << *iLinkID << std::endl;
      break;
    }
  }//for erase from bcklinks
  if( DeleteMe!=bckLinks[links[LinkID].E].end() )
  {
    bckLinks[links[LinkID].E].erase(DeleteMe);
  }
  
  iLink=links.find(LinkID);
  if( iLink!=links.end() )
  {
    links.erase(iLink);
  }  
}//Lattice::deleteLink(ID_t LinkID)
   
void  Lattice::deleteNode(ID_t NodeID)
{
  std::vector<ID_t>::iterator DeleteMe;
  std::map<ID_t, Lattice::Node>::iterator iNode;

  //std::cout << "Deleting Node ID " << NodeID << std::endl;

  while( fwdLinks[NodeID].size() > 0)
  {
    deleteLink(*(fwdLinks[NodeID].begin()));
//    for( std::vector<ID_t>::iterator iLinkID=fwdLinks[NodeID].begin(); iLinkID!=fwdLinks[NodeID].end(); ++iLinkID )
//    {
//      deleteLink(*iLinkID);
//    }//for erase from fwdlinks
  }

  while( bckLinks[NodeID].size() > 0)
  {
    deleteLink(*(bckLinks[NodeID].begin()));
//    for( std::vector<ID_t>::iterator iLinkID=bckLinks[NodeID].begin(); iLinkID!=bckLinks[NodeID].end(); ++iLinkID )
//    {
//      deleteLink(*iLinkID);
//    }//for erase from fwdlinks
  }
  
  iNode=nodes.find(NodeID);
  if( iNode!=nodes.end() )
  {
    nodes.erase(iNode);
  }  
}//Lattice::deleteLink(ID_t LinkID)

//void Lattice::cutOffPieceOfLattice(LatTime FromTime, LatTime ToTime, std::vector<ID_t> &from, std::vector<ID_t> &to)
bool  Lattice::cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, std::vector<ID_t> &from, std::vector<ID_t> &to)
{

  std::map<lse::ID_t,int> LinksToDelete, NodesToDelete;
  bool IsNodeInBEG=false, IsNodeInEND=false;
  std::vector<ID_t>::iterator DeleteMe;

  from.clear();
  to.clear();
      
  for (std::map<ID_t, Lattice::Node>::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
  {
    if( ((iNode->second).t >= FromTimeFrom) && ((iNode->second).t <= FromTimeTo) )
    {
      from.push_back((iNode->second).id);
      IsNodeInBEG=true;
      //std::cout << "Found node in BEG REG, ID: " <<  (iNode->second).id << std::endl;
    }
    if( ((iNode->second).t >= ToTimeFrom) && ((iNode->second).t <= ToTimeTo) )
    {
      to.push_back((iNode->second).id);
      IsNodeInEND=true;
      //std::cout << "Found node in END REG, ID: " <<  (iNode->second).id << std::endl;
    }
    if( ((iNode->second).t > FromTimeTo) && ((iNode->second).t < ToTimeFrom) )
    {
      NodesToDelete[(iNode->second).id]=0;
//      std::cout << "Found node in INNER REG, ID: " <<  (iNode->second).id << std::endl;
    }
  }//for (std::map<ID_t, Lattice::Node>::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)

  if( not (IsNodeInBEG && IsNodeInEND) ){
    return false;
  }
    
  for (std::map<ID_t, Lattice::Link>::iterator iLink = links.begin(); iLink != links.end(); ++iLink)
  {
    if( (nodes[(iLink->second).S].t <= FromTimeTo) && (nodes[(iLink->second).E].t > FromTimeTo) )
    {
      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, over BEG REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
    }
    
    if( (nodes[(iLink->second).S].t < ToTimeFrom) && (nodes[(iLink->second).E].t >= ToTimeFrom) )
    {
      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, over END REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
    }

//    if( (nodes[(iLink->second).S].t >= FromTimeFrom) && (nodes[(iLink->second).E].t <= ToTimeTo) )
//    {
//      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, in OUTER REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
//    }
  }//for links

  
  //std::cout << "Erasing Link ";
  for (std::map<lse::ID_t,int>::iterator iLinkID = LinksToDelete.begin(); iLinkID != LinksToDelete.end(); ++iLinkID)
  {
    //std::cout << (*iLinkID) << " ";
    deleteLink((iLinkID->first));
  }//delete links
//  std::cout << std::endl;
  
//  std::cout << "Erasing Node ";
  for (std::map<lse::ID_t,int>::iterator iNodeID = NodesToDelete.begin(); iNodeID != NodesToDelete.end(); ++iNodeID)
  {
    //std::cout << (*iNodeID) << " ";
    deleteNode((iNodeID->first));
  }//delete nodes
//  std::cout << std::endl;

  
  return true;
}//void cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, std::vector<ID_t> &from, std::vector<ID_t> &to);

void Lattice::insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)
{

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = nodes[*iNodeToID];
      tmpNode.id = LastNode;

      addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = fwdLinks[*iNodeToID].begin(); ifwdlink != fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        addLink(tmpLink);

        fwdLinks[LastNode].push_back(LastLink);
        bckLinks[links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if  

  }//for

  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
    {
    
        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;

        tmpLink.init();
        tmpLink.id = LastLink;
        tmpLink.S  = *iNodeFromID;
        tmpLink.E  = *iNodeToID;
        tmpLink.a  = 0;
        tmpLink.l  = 0;

        addLink(tmpLink);

        fwdLinks[*iNodeFromID].push_back(LastLink);
        bckLinks[*iNodeToID].push_back(LastLink);
        
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;
      
    }//for nodes from nowhere
  }//for nodes to nowhere

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    nodes[*iNodeToID].W = Label;
    nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      links[*iFwdLinkID].l = 0;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere

}//void Lattice::insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)

void Lattice::insertToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label, std::string WordInLm, LanguageModel &LM)
{                 

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = nodes[*iNodeToID];
      tmpNode.id = LastNode;

      addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = fwdLinks[*iNodeToID].begin(); ifwdlink != fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        addLink(tmpLink);

        fwdLinks[LastNode].push_back(LastLink);
        bckLinks[links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if  

  }//for

  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
    {
    
        ID_t LastLink = getLastLinkID()+1;
        Lattice::Link tmpLink;

        tmpLink.init();
        tmpLink.id = LastLink;
        tmpLink.S  = *iNodeFromID;
        tmpLink.E  = *iNodeToID;
        tmpLink.a  = 0;
        tmpLink.l  = LM.GetWordNGramProb(DeleteBackslash((nodes[*iNodeFromID].W)+" "+WordInLm))*2.3026;
        //std::cout << DeleteBackslash(nodes[*iNodeFromID].W) << " " << DeleteBackslash(WordInLm) << " " << (tmpLink.l) << std::endl;

        addLink(tmpLink);

        fwdLinks[*iNodeFromID].push_back(LastLink);
        bckLinks[*iNodeToID].push_back(LastLink);
        
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;
      
    }//for nodes from nowhere
  }//for nodes to nowhere

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    nodes[*iNodeToID].W = Label;
    nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      links[*iFwdLinkID].l = LM.GetWordNGramProb(DeleteBackslash(WordInLm+" "+(nodes[links[*iFwdLinkID].E].W)))*2.3026;
      //std::cout << DeleteBackslash(WordInLm) << " " << DeleteBackslash(nodes[links[*iFwdLinkID].E].W) << " " << (links[*iFwdLinkID].l) << std::endl;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere

}//void Lattice::insertToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)



void Lattice::timeNormalizeLattice()
{
  for(lse::Lattice::LinkMap::iterator iLinkID = links.begin(); iLinkID != links.end(); ++iLinkID ){
    if((nodes[(iLinkID->second).E].t-nodes[(iLinkID->second).S].t) > 0){
      //std::cout << (iLinkID->second).a << " " << (iLinkID->second).l << " " << nodes[(iLinkID->second).E].t << " " << nodes[(iLinkID->second).S].t << std::endl; 
      (iLinkID->second).a=(iLinkID->second).a/(nodes[(iLinkID->second).E].t-nodes[(iLinkID->second).S].t)/100;
      //(iLinkID->second).l=(iLinkID->second).l/(nodes[(iLinkID->second).E].t-nodes[(iLinkID->second).S].t)/100;
      //std::cout << (iLinkID->second).a << " " << (iLinkID->second).l << std::endl;
    }  
  }//for links
}//void Lattice::insertToLattice(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label)




void Lattice::OutputNgrams_recursive(vector<Ngram*> &ngrams, int nodeIdCur, int nCur, int nMax, Ngram *ngram)
{

	bool debug = false;	
	if (debug) if (nCur==0) cerr << "|" << nodeIdCur << "|";
	if (debug) cerr << ".";

	Lattice::Node* pNodeCur = &nodes[nodeIdCur];

	ID_t nodeIdCurWordId = pNodeCur->W_id;
	bool jump_over = lexicon->isMetaWord(nodeIdCurWordId);
	bool add = true;

	if (!jump_over)
	{
		if (nCur+1 >= nMax) 
		{
			if (ngram == NULL)
			{
				ngram = new Ngram(nMax);
			}
			ngram->SetItem(nCur, pNodeCur->tStart, pNodeCur->t, pNodeCur->W_id);

			// check overlapping ngrams
			for (vector<Ngram*>::reverse_iterator iNgram = ngrams.rbegin();
				 iNgram != ngrams.rend();
				 iNgram ++)
			{
				bool check_overlapping = true;
				Ngram *pNgram = *iNgram;

				// ngrams should be sorted by t (end time), so when
				// we are processing ngrams in the reverse order 
				// (from highest t to the lowest t), after reaching
				// the first ngram distant from the current one,
				// by more than twice it's length,
				// we can break the loop of finding overlapping
				// ngrams
/*
				if (pNgram->GetEndTime() < ngram->GetStartTime() - (ngram->GetEndTime() - ngram->GetStartTime()))
				{
					break;
				}
*/
				for (int i=nMax-1; i>=0; i--)
				{
					if (ngram->GetItemWordId(i) != pNgram->GetItemWordId(i))
					{
						check_overlapping = false;
						break;
					}
				}

				if (check_overlapping)
				{
					if (debug) cerr << "c";
					if (is_overlapping( ngram->GetStartTime(), ngram->GetEndTime(),
										pNgram->GetStartTime(), pNgram->GetEndTime()) )
					{
						if (ngram->GetStartTime() < pNgram->GetStartTime())
						{
							pNgram->SetStartTime(ngram->GetStartTime());
						}

						if (ngram->GetEndTime() > pNgram->GetEndTime())
						{
							pNgram->SetEndTime(ngram->GetEndTime());
						}

						add = false;
						break;
					}
				}
			}

			if (add)
			{
				ngrams.push_back(ngram);
			}
			
			return;
		}
	}

//	DBG_FORCE("nodeIdCur:"<<nodeIdCur<<" nCur:"<<nCur<<" nMax:"<<nMax<<" ngram:"<<ngram);
//	DBG_FORCE("fwdLinks.size:"<<fwdLinks[nodeIdCur].size());
	for (vector<ID_t>::iterator iLink = fwdLinks[nodeIdCur].begin(); 
		 iLink != fwdLinks[nodeIdCur].end();
		 iLink ++)
	{
		Ngram* p_ngramNew;
		if (!jump_over && ngram != NULL)
		{
			p_ngramNew = new Ngram();
			p_ngramNew->CopyParamsFrom(*ngram);
			p_ngramNew->SetItem(nCur, pNodeCur->tStart, pNodeCur->t, pNodeCur->W_id);
		} 
		else 
		{
			p_ngramNew = new Ngram(nMax);
		}
		OutputNgrams_recursive(ngrams, links[*iLink].E, nCur+(!jump_over&&ngram!=NULL ? 1 : 0), nMax, p_ngramNew);
	}

	// we we have not added the ngram to the list of output ngrams, then we should delete it
	if (!add)
	{
		delete ngram;
	}
	
}

void Lattice::OutputNgrams(string filename, int n)
{
	vector<Ngram*> ngrams;
	DBG_FORCE("Generating "<<n<<"-grams");
	for (NodeMap::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)
	{
		OutputNgrams_recursive(ngrams, iNode->second.id, 0, n, NULL);
	}
	DBG_FORCE("Generating "<<n<<"-grams...done");

	DBG_FORCE("OutputNgrams("<<filename<<", "<<n<<")");
	ofstream out(filename.c_str());
/*	
	for (vector<int*>::iterator iNgram1 = ngrams.begin();
		 iNgram1 != ngrams.end();
		 iNgram1 ++)
	{
		bool output = true;

		// check overlapping ngrams
		for (vector<int*>::iterator iNgram2 = ngrams.begin();
			 iNgram2 != ngrams.end();
			 iNgram2 ++)
		{
			if (is_overlapping( nodes[(*iNgram1)].tStart, nodes[(*iNgram1)].tEnd,
								nodes[(*iNgram2)].tStart, nodes[(*iNgram2)].tEnd) )
			{

			}
		}
	}
*/

	for (vector<Ngram*>::iterator iNgram = ngrams.begin();
		 iNgram != ngrams.end();
		 iNgram ++)
	{
		Ngram *pNgram = *iNgram;
		out << pNgram->GetStartTime() << "\t" << pNgram->GetEndTime() << "\t";
		for (int i=0; i<n; i++)
		{
			out << lexicon->getVal(pNgram->GetItemWordId(i)) << (i<n-1 ? " " : "");
		}
		out << endl;
	}

	out.close();
}

