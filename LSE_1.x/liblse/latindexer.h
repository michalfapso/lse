/*********************************************************************************
 *	TODO: 
 *
 *	Add checksum hash table for each word in map str2idMap to speed up searching
 *
 *	getNewWordID(): there is some useable STL function (hope so)
 *	                store last index in variable
 *
 *	getWordID(): for parallel processing - implementation of semaphor is needed
 *
 */

#ifndef LATINDEXER_H
#define LATINDEXER_H


#include <iostream>
#include <sys/stat.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include "latindexer.h"
#include "lattypes.h"
#include "genericindexer.h"
#include "mymath.h"
#include "common.h"

#define LATINDEXER_EXT_WIDINDEX ".wid"
#define LATINDEXER_EXT_TIMESORT ".timesort"
#define LATINDEXER_EXT_TIMESORT_POINTERS ".timesort_ptr"

namespace lse {


//==================================================
// LatIndexer
//==================================================
class LatIndexer {
	
public:	
	class SortIdxRecord;
	class Record;
	class Cluster;

	class Records : public std::vector<LatIndexer::Record>
	{
		public:
			int *mpByTime;

			Records() : mpByTime(NULL) {}
	};

	Records records;
private: 
	Record * recordsArray;
	long recordsArray_size;

	long * recordsArrayPointers;
	long recordsArrayPointers_size;
	ID_t firstWordID;

	SortIdxRecord * mpSortIdxRecordsArray_byTime;
	int * mpSortIdxRecordsArray_byTime_Pointers;

public:

	// An interface class for sending an output of reverse index hits to any outer funcion/method
	// ( for more info see LatIndexer::findWord() )
	class RecordsReader
	{
		public:
			int *mpByTime;

			RecordsReader() : mpByTime(NULL) {}

			/**
			 * @brief Add a LatIndexer::Record instance to a container
			 *
			 * @param LatIndexer::Record reverse index hit
			 */
			virtual void push_back(const LatIndexer::Record &rec) = 0;

			/**
			 * @brief Get the size of a container of LatIndexer::Record items
			 *
			 * @return Size of the container
			 */
			virtual unsigned int size() = 0;

			virtual ~RecordsReader() { if(mpByTime != NULL) delete[] mpByTime; }
	};

	class SortIdxRecord
	{
		public:


			static char CmpTime(const SortIdxRecord &lv, const SortIdxRecord &rv);
			friend bool operator<(const SortIdxRecord& lv, const SortIdxRecord& rv);
	};

	typedef int RecordIdx;

	class RecordSortedByTime
	{
		public:
			RecordIdx recordIdx; ///< index of a record within a particular keyword's hits in a reverse index
			LatTime t; ///< time of the record in the reverse index
			ID_t wordID; ///< word id of the record in the reverse index
			ID_t meetingID; ///< meeting id of the record in the reverse index
			float conf;		// confidence

			friend bool operator<(const RecordSortedByTime& lv, const RecordSortedByTime& rv);
	};

	class RecordSortedByConfidence
	{
		public:
			RecordIdx recordIdx; ///< index of a record within a particular keyword's hits in a reverse index
			ID_t wordID; ///< word id of the record in the reverse index
			float conf;		// confidence of the record in the reverse index

			friend bool operator<(const RecordSortedByConfidence& lv, const RecordSortedByConfidence& rv);
	};

	//==================================================
	// Record
	//==================================================
	class Record {

		public:
			ID_t wordID;
			ID_t meetingID;
			long nodeID;
			// META-INFO
			float conf;		// confidence
			LatTime tStart;	// word's start time
			LatTime t;		// word's end time

			friend bool operator==(const Record& l, const Record& r) {
				return (l.wordID == r.wordID &&
						l.meetingID == r.meetingID &&
						l.nodeID == r.nodeID &&
						l.conf == r.conf &&
						l.tStart == r.tStart &&
						l.t == r.t);
			}
			friend bool operator!=(const Record& l, const Record& r) { return !(l==r); }
			friend bool operator<(const Record& lv, const Record& rv);
			friend bool operator>(const Record& lv, const Record& rv);
			friend std::ostream& operator<<(std::ostream& os, const Record& rec);

			static char CmpConf(const Record &lv, const Record &rv);
			static inline bool CmpLessWordId(const Record &lv, const Record &rv) { return lv.wordID < rv.wordID; }
	};


	//==================================================
	// RecordClusters
	//==================================================
	class RecordClusters : public std::vector< LatIndexer::Cluster > {

		public:


//			void add(Cluster::Record rec);
			void add(LatIndexer::Record rec, bool checkOverlapping = true);
//			void computeOverlappedRecordsLikelihood();
			void print();
			void sort();
	};


	typedef LatIndexer::Cluster * PCluster;
	typedef std::vector< PCluster > ClustersList;


//	class Cluster : public std::vector< Cluster::Record > {
	class Cluster : public std::vector< LatIndexer::Record > {
		public:	

			ID_t W_id; 					// word ID
			ID_t meetingID;				// meeting ID
			LatTime tStart; 			// min time of all records
			LatTime tEnd;				// max time of all records
			TLatViterbiLikelihood conf;	// max confidence of all records
			

//					LatIndexer::RecordClusters::AllClusters::iterator bestFwdKwdCluster; 	// best cluster of the next keyword from search query
//			std::vector< Cluster >::iterator bestFwdKwdCluster; 	// best cluster of the next keyword from search query
			Cluster *bestFwdKwdCluster; 	// best cluster of the next keyword from search query
			bool bestFwdKwdCluster_found; 				// true, if there is some matching cluster in the following kwd
			
			// list of all forward clusters which fit the query
			ClustersList fwdClusters;
			ClustersList bckClusters;
			
			Cluster() : W_id(-1), meetingID(-1), tStart(0.00), tEnd(0.00), conf(-INF) {}
			friend bool operator==(const Cluster& l, const Cluster& r) {
				return (l.W_id == r.W_id &&
						l.meetingID == r.meetingID &&
						l.tStart == r.tStart &&
						l.tEnd == r.tEnd &&
						l.conf == r.conf);
			}
			
			friend bool operator!=(const Cluster& l, const Cluster& r) { return !(l==r); }

			friend bool operator<(const Cluster& l, const Cluster& r) 
			{
				assert(l.W_id == r.W_id);
				if (l.meetingID == r.meetingID)
				{
					return l.tEnd < r.tStart;
				}
				else
				{
					return l.meetingID < r.meetingID;
				}
			}
			friend bool operator>(const Cluster& l, const Cluster& r)
			{
				assert(l.W_id == r.W_id);
				if (l.meetingID == r.meetingID)
				{
					return l.tStart > r.tEnd;
				}
				else
				{
					return l.meetingID > r.meetingID;
				}
			}
			friend bool operator<=(const Cluster& l, const Cluster& r) { return (l<r) || (l==r); }
			friend bool operator>=(const Cluster& l, const Cluster& r) { return (l>r) || (l==r); }

			friend std::ostream& operator<<(std::ostream& os, const Cluster& c)
			{
				return os << "W_id: "<<c.W_id<<" meetingID:"<<c.meetingID<<" t["<<c.tStart<<".."<<c.tEnd<<"] conf:"<<c.conf;
			}
	
//			void addRecord(Cluster::Record rec);
			void addRecord(LatIndexer::Record &rec);
	};


	
	LatIndexer() : recordsArray(NULL), recordsArray_size(0) {}

	// reverse index methods
	void addRecord(Record latRec);
	int saveToFile(const std::string filename);
	int loadFromFile(const std::string filename);
	void sortRecords();
	void print();
	int findWord(const std::string filename, const ID_t wordID, RecordsReader *result);
	int createWordIDIndexFile(const std::string filename);
	
};



//==================================================
// FwdIndex
//==================================================
class FwdIndex {
//		static const int kSMALL_ENOUGH = 15; // when the subarrays get small enough you switch from quicksort to an alternative like selection sort

		std::string mFilename;
		std::ofstream of;
		bool mActive;

		LatIndexer::Record* mpData; 			///< array of forward index records
		LatIndexer::Record* mpDataEndptr; 	///< pointer to the first empty element in mpData array
		int mDataRecordsCount;
		long mDataSize;						///< number of LatIndexer::Record items in mpData array

//		long* mpSortIdxRecordsArray_byTime_Pointers; ///< hash: wordID -> mpSortIdxRecordsArray_byTime items


		LatIndexer::RecordSortedByTime* mpSortIdxRecordsArray_byTime;
		LatIndexer::RecordSortedByConfidence* mpSortIdxRecordsArray_byConfidence;
		int * mpData2ByConf;
		
//		void sortarray_swap(LatIndexer::Record d[], int l, int k);
//		int sortarray_partition( LatIndexer::Record d[], int left, int right);
//		void sortarray_QSort( LatIndexer::Record d[], int left, int right);
//		void sortarray_selectionSort(LatIndexer::Record mpData[], int left, int right);
	public:

		FwdIndex() : mActive(false),
					 mpData(NULL),
					 mpDataEndptr(NULL),
					 mDataRecordsCount(0),
					 mDataSize(0),
					 mpSortIdxRecordsArray_byTime(NULL),
					 mpSortIdxRecordsArray_byConfidence(NULL),
					 mpData2ByConf(NULL)
		{}

		~FwdIndex() {
			if (mpData != NULL) delete[] mpData;
		}
		int create(std::string p_filename);
		void addMeeting(int nodesCount);
		void closeMeeting();
		void addRecord(LatIndexer::Record latRec);
		void close();

		// methods for converting from the forward index to the reverse one
		void initArray(int allFilesSize);
		int appendFile(std::string p_filename);
		int save(std::string p_filename);
		void sortarray();
		void Print();
};

} // namespace lse

#endif
