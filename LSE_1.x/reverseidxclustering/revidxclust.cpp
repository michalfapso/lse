#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/common.h"
#include "../liblse/latindexer.h"
#include "../liblse/lattypes.h"

using namespace std;
using namespace lse;

string p_idx_out = "";
string p_idx_in = "";
int p_meeting_idx = 0;

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes forward index filenames as parameters." << endl;
		return 1;
	}

	int j=1;
	while (j<argc) {

		DBG_FORCE("argv["<<j<<"]:" << argv[j]);
			
		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		
		} else if (strcmp(argv[j], "-idx-out") == 0) {
			j++;
			p_idx_out = argv[j];
			DBG_FORCE("-idx-out: "<< p_idx_out);
		} else if (strcmp(argv[j], "-idx-in") == 0) {
			j++;
			p_idx_in = argv[j];
			DBG_FORCE("-idx-in: "<< p_idx_in);
		} else if (strcmp(argv[j], "-meeting-idx") == 0) {
			j++;
			p_meeting_idx = atoi(argv[j]);
			DBG_FORCE("-idx-in: "<< p_meeting_idx);
		}
		j++;
	} // for each param

	LatIndexer revIndex;

	revIndex.loadFromFile(p_idx_in);
	for (LatIndexer::Records::iterator iRec=revIndex.records.begin(); iRec!=revIndex.records.end(); iRec++)
	{
		iRec->meetingID = p_meeting_idx;
	}
	revIndex.saveToFile(p_idx_out);

	return 0;
} // main()

