#!/bin/bash

export BIN_DIR=/mnt/matylda/fapso/projects/search-engine/bin/unstable

$BIN_DIR/LSElattice_processor \
	-meetings-idx /mnt/scratch01/fapso/exp/icsi-tsd-paper-experiments/lattices.system.lvcsr.singlefiles/_MEETING.IDX \
	-data-dir-path /mnt/scratch01/fapso/exp/icsi-tsd-paper-experiments/lattices.system.lvcsr.singlefiles/ \
	-miliseconds-per-frame 10

#	-meetings-idx /mnt/scratch01/fapso/exp/icsi-tsd-paper-experiments/lattices.system.lvcsr.singlefiles/_MEETING_SMALL.IDX \
