#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/latindexer.h"
#include "../liblse/lattypes.h"

using namespace std;
using namespace lse;

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes search index filename as parameter.";
		return 1;
	}

	char* filename = argv[1];

	ifstream in(filename, ios::binary);
	if (!in.good()) {
		cerr << "ERROR: while reading search index file: " << filename << endl;
		exit(1);
	}

	// read all nodes
	LatIndexer::Record latRec;
//	while(!in.eof()) {
	while(1) {	// because eof() will be set after the invalid in.read()
		in.read(reinterpret_cast<char *>(&latRec), sizeof(latRec));
		if (in.eof()) {
			DBG("EOF()");
			break;
		}
		cout << latRec << endl;
	}
	in.close();
	return 0;

} // main()

