#include "lat.h"
#include "lexicon.h"

using namespace lse; // Lattice Search Engine

int main() {
	Lexicon lexicon; // stores records of type: wordID -> word  and  word -> wordID
	Lattice lat(&lexicon);
	
	// creating nodes
	Lattice::Node tmpNode;
	
	tmpNode.id = 0;			// ID
	tmpNode.t = 0;			// Time
	tmpNode.W = "!NULL";	// Word
	lat.addNode(tmpNode);	// insert node into lattice
		
	tmpNode.id = 1;
	tmpNode.t = 1;
	tmpNode.W = "HELLO";
	lat.addNode(tmpNode);
	
	tmpNode.id = 2;
	tmpNode.t = 2;
	tmpNode.W = "WORLD";
	lat.addNode(tmpNode);
	
	// creating links
	Lattice::Link tmpLink;

	tmpLink.id = 0;
	tmpLink.S = 0;
	tmpLink.E = 1;
	tmpLink.a = -200;
	tmpLink.l = -2;
	lat.addLink(tmpLink);
		
	tmpLink.id = 1;
	tmpLink.S = 0;
	tmpLink.E = 2;
	tmpLink.a = -230;
	tmpLink.l = -2.5;
	lat.addLink(tmpLink);
		
	tmpLink.id = 2;
	tmpLink.S = 1;
	tmpLink.E = 2;
	tmpLink.a = -253.7;
	tmpLink.l = -2.13;
	lat.addLink(tmpLink);
		
	lat.saveToHTKFile("simple-lattice.lat"); // COPY OF LATTICE		
}

