#include <iostream>
#include <zlib.h>

using namespace std;

int main() {
	gzFile gz_lat_file = gzopen("/homes/eva/xf/xfapso00/speech/projects/lattice/data/marta/sw1-04388_001907_002327.lat.gz", "rb");
	if (gz_lat_file == NULL) {
		cerr << "Error: Could not open file" << endl;
		return (-1);		
	}

	char c;
	while(!gzeof(gz_lat_file)) {
		c = gzgetc(gz_lat_file);
		cout << c;
	}
	gzclose(gz_lat_file);
}
