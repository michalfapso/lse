#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "../liblse/common.h"
#include "../liblse/latindexer.h"
#include "../liblse/lattypes.h"

using namespace std;
using namespace lse;

string p_lexicon = "";

int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Application takes forward index filenames as parameters." << endl;
		return 1;
	}

	int j=1;
	while (j<argc) {

		DBG_FORCE("argv["<<j<<"]:" << argv[j]);
			
		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		
		} else if (strcmp(argv[j], "-lexicon-idx") == 0) {
			j++;
			p_lexicon = argv[j];
			DBG_FORCE("-lexicon-idx: "<< p_lexicon);

		} else if (strcmp(argv[j], "-latlist-in") == 0) {
			j++;
			p_lexicon = argv[j];
			DBG_FORCE("-lexicon-idx: "<< p_lexicon);
		} else {
			cerr << "ERROR: switch " << argv[j] << " is not supported" << endl;
			exit(1);
		}
		j++;
	} // for each param

/*	

//================================================================================
//	SOLUTION FOR MULTIPLE FILES - NOT WORKING YET DUE TO SOME PROBLEMS WITH LEXICON MERGING
//================================================================================

	int firstfile_idx = j;
	long completesize = 0;
	
	//==================================================
	// GETTING FILES' SIZE
	//==================================================
	char* filename;
	while (j<argc) {
		filename = argv[j];
		long filesize_tmp = file_size(filename);
		cout << "file_size(" << filename << ") = " << filesize_tmp << endl;
		completesize += filesize_tmp;
		j++;
	}

	//==================================================
	// ALLOCATING MEMORY
	//==================================================
	cout << "Allocating memory: " << completesize << " bytes...";
	unsigned char* fwdidx = new unsigned char[completesize];
	unsigned char* fwdidx_ptr = fwdidx;
	cout << "done" << endl;
	
	//==================================================
	// ADDING FWD INDEX FILES TO MEMORY
	//==================================================
	j = firstfile_idx;
	while (j<argc) {
		filename = argv[j];
		cout << "adding fwd index: " << filename << endl;
		
		FILE *fin = fopen(filename, "rb");
		if (fin == NULL) {
			return (LSE_ERROR_IO);		
		}

		long filesize_tmp = file_size(filename);
		fread(fwdidx_ptr, sizeof(unsigned char), filesize_tmp, fin);
		fclose(fin);
				
		j++;
	}
*/


	if (j < argc-1) {
		cerr << "ERROR: too many files specified:" << endl;
		while(j < argc) {
			cerr << "       " << argv[j] << endl;
			j++;
		}
		exit(1);
	}
	
	FwdIndex fwdIndex;
	char* filename = argv[j];
/*
	LatIndexer::Record latRec;
	long filesize = file_size(filename);
	long arraysize = (int)(filesize / sizeof(latRec));
	DBG_FORCE("file_size(" << filename << ") = " << filesize);
	DBG_FORCE("arraysize: " << arraysize);
*/
	//==================================================
	// ALLOCATING MEMORY
	//==================================================
	fwdIndex.load(filename);
/*	cout << "Allocating memory: " << filesize << " bytes...";
	LatIndexer::Record* fwdidx = new LatIndexer::Record[arraysize];
	LatIndexer::Record* fwdidx_ptr = fwdidx;
	cout << "done" << endl;
	
	//==================================================
	// LOAD FWD INDEX FILE TO MEMORY
	//==================================================
	cout << "adding fwd index: " << filename << endl;
		
	ifstream in(filename, ios::binary);
	if (!in.good()) {
		cerr << "ERROR: while reading fwd index file: " << filename << endl;
		exit(1);		
	}

	// read all nodes
	fwdidx_ptr = fwdidx;
	while(!in.eof()) {
		in.read(reinterpret_cast<char *>(fwdidx_ptr), sizeof(latRec));
		fwdidx_ptr++;
	}
	in.close();
*/	
				
	//==================================================
	// SORT FWD INDEX
	//==================================================
	fwdIndex.sortarray();

	
	//==================================================
	// SAVE SORTED INDEX TO FILE
	//==================================================
	fwdIndex.save(p_idx_out);
/*	ofstream out(p_idx_out.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	for(int i=0; i<arraysize; i++) {
		out.write(reinterpret_cast<const char *>(&(fwdidx[i])), sizeof(LatIndexer::Record));
	}
	out.close();
*/
	return 0;
} // main()

