-- global
create table docs(
	id_doc integer primary key autoincrement,
	path text,
	unique(path)
);


-- MTxt
create table MTxtDictionary(
	id_unit integer primary key autoincrement,
	label text,
	unique(label)
);

create table MTxtIndex(
	id integer primary key autoincrement,
	id_doc integer,
	id_label integer,
	pos integer
);
create index i_MTxtIndex_id_doc on MTxtIndex(id_doc);
create index i_MTxtIndex_id_label on MTxtIndex(id_label);


-- MKws
create table MKwsDictionaryLvcsr(
	id_unit integer primary key autoincrement,
	label text,
	unique(label)
);

create table MKwsDictionaryPhn(
	id_unit integer primary key autoincrement,
	label text,
	unique(label)
);

create table MKwsInvIndexLvcsr(
	id integer primary key autoincrement,
	id_doc integer,
	id_label integer,
	start_time real,
	end_time real,
	confidence real
);
create index i_MKwsInvIndexLvcsr_id_doc on MKwsInvIndexLvcsr(id_doc);
create index i_MKwsInvIndexLvcsr_id_label on MKwsInvIndexLvcsr(id_label);
create index i_MKwsInvIndexLvcsr_start_time on MKwsInvIndexLvcsr(start_time);
create index i_MKwsInvIndexLvcsr_confidence on MKwsInvIndexLvcsr(confidence);

