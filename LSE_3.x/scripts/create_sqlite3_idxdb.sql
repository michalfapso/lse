create table MKwsInvIndexLvcsr(
	id integer primary key autoincrement,
	id_doc integer,
	id_label integer,
	start_time real,
	end_time real,
	confidence real
);
create index i_MKwsInvIndexLvcsr_id_doc on MKwsInvIndexLvcsr(id_doc);
create index i_MKwsInvIndexLvcsr_id_label on MKwsInvIndexLvcsr(id_label);
create index i_MKwsInvIndexLvcsr_start_time on MKwsInvIndexLvcsr(start_time);
create index i_MKwsInvIndexLvcsr_confidence on MKwsInvIndexLvcsr(confidence);

