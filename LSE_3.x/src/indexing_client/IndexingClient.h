#ifndef INDEXINGCLIENT_H
#define INDEXINGCLIENT_H

#include <string>
#include <istream>
#include <sstream>

#include <boost/asio.hpp>

#include "dbg.h"
#include "ModulesContainer.h"
#include "ApplicationType.h"
#include "ConfigFile.h"
#include "IndexingClient_Interface.h"
#include "Connection.h"

class IndexingClient : public IndexingClient_Interface
{
	private:
		boost::asio::io_service mIoService;
		Connection_ptr mpConnection;
		ConfigFile* mpConfig;
		std::string mHostname;
		std::string mPort;
		ModulesContainer* mpModulesContainer;
		enum { mHeaderLength = 8 };

	public:

		IndexingClient(ConfigFile* pConfig, std::string hostname, std::string port);
		~IndexingClient();

		ID Index(std::string module, std::string document, std::string file);
		ID SendAssignIdMsg(const std::string &moduleId, const std::string &msg);
		void SendForwardIndexToServer(const std::string &moduleId, const std::string &fwdIndex);

		ModulesContainer* GetModules() { return mpModulesContainer; }
		ID GetDocId(std::string document);
};

#endif

