#include "IndexingClient.h"

using namespace std;
using boost::asio::ip::tcp;

IndexingClient::IndexingClient(ConfigFile* pConfig, std::string hostname, std::string port) 
	: mpConfig(pConfig), mHostname(hostname), mPort(port) 
{ 
	DBG("IndexingClient::IndexingClient()"); 
	mpModulesContainer = new ModulesContainer(mpConfig);
	mpModulesContainer->SetIndexingClient(this);
	mpModulesContainer->InitModules(ApplicationType::EIndexingClient);
	try
	{
		DBG("connecting to '"<<mHostname<<":"<<mPort<<"'...");
		boost::asio::ip::tcp::resolver resolver(mIoService);
		//boost::asio::ip::tcp::resolver::query query(tcp::v4(), mHostname, mPort);
		boost::asio::ip::tcp::resolver::query query(mHostname, mPort);
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		boost::asio::ip::tcp::resolver::iterator end;


		// Try each endpoint until we successfully establish a connection.
		mpConnection.reset(new Connection(mIoService));
		boost::system::error_code error = boost::asio::error::host_not_found;
		while (error && endpoint_iterator != end)
		{
			DBG("trying another endpoint");
			mpConnection->GetSocket().close();
			mpConnection->GetSocket().connect(*endpoint_iterator++, error);
		}
		if (error)
			throw boost::system::system_error(error);
	}
	catch (std::exception &e)
	{
		CERR("Exception: "<<e.what());
		EXIT();
	}
}

IndexingClient::~IndexingClient() 
{ 
	DBG("IndexingClient::~IndexingClient()"); 
	delete mpModulesContainer;
	mpConnection->GetSocket().close();
}

ID IndexingClient::GetDocId(std::string document)
{
	try
	{
		ostringstream ss_data;
		ss_data << "assign_id doc " << document;
		mpConnection->write(ss_data.str());

		// Read the response status line.
		vector<char> response;
		mpConnection->read(response);
		//if (error) throw boost::system::system_error(error);

		std::string response_string(&response[0], response.size());
		DBG("response: " << response_string);
		std::istringstream response_stream(response_string);
		ID id = ID_INVALID;
		response_stream >> id;
		DBG("response id: " << id);
		return id;
	} 
	catch (std::exception &e)
	{
		CERR("Exception: "<<e.what());
		EXIT();
	}
	return ID_INVALID;
}

/**
 * @brief Index file
 *
 * @param module Id of the module used for indexing
 * @param document Document name
 * @param file File to index
 *
 * @return Document's ID
 */
ID IndexingClient::Index(std::string module, std::string document, std::string file)
{
	sem::M* m = mpModulesContainer->GetModule(module);
	if (m == NULL)
	{
		CERR("IndexingClient::Index() module "<<module<<" does not exist");
		EXIT();
	}

	ID docId = GetDocId(document);
	if (docId == ID_INVALID)
	{
		CERR("IndexingClient::GetDocId() returned ID_INVALID");
		EXIT();
	}

	DBG("m->Index("<<docId<<", "<<file<<")");
	m->Index(docId, file);
	return docId;
}

void IndexingClient::SendForwardIndexToServer(const std::string &moduleId, const std::string &fwdIndex)
{
	try
	{
		ostringstream ss_data;
		ss_data << "fwd_idx " << moduleId << " " << fwdIndex;
		mpConnection->write(ss_data.str());

		// Read the response status line.
		vector<char> response;
		mpConnection->read(response);
		std::string response_string(&response[0], response.size());

		DBG("response: " << response_string);
		if (response_string != "ok")
		{
			CERR("ERROR: sending forward index to the indexing server failed.");
			EXIT();
		}
	}
	catch (std::exception &e)
	{
		CERR("Exception: "<<e.what());
		EXIT();
	}
}


ID IndexingClient::SendAssignIdMsg(const std::string &moduleId, const std::string &msg)
{
	//DBG("IndexingClient::SendAssignIdMsg()");
	try
	{
		ostringstream ss_data;
		ss_data << "assign_id msg " << moduleId << " " << msg;
		mpConnection->write(ss_data.str());

		// Read the response status line.
		vector<char> response;
		mpConnection->read(response);
		std::string response_string(&response[0], response.size());

		//DBG("response:"<<response_string);

		std::istringstream response_stream(response_string);
		ID id = ID_INVALID;
		response_stream >> id;
		//DBG("response id: " << id);

		return id;
	}
	catch (std::exception &e)
	{
		CERR("SocketException: "<<e.what());
		EXIT();
	}
	return ID_INVALID;
}

