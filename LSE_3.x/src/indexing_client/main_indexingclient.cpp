#include <iostream>
#include <string>
#include <exception>
#include <getopt.h>

#include "dbg.h"
#include "IndexingClient.h"
#include "ConfigFile.h"

using namespace std;

static int verbose_flag = 0;

int main(int argc, char* argv[])
{
	DBG("main()");
	DBG("argc:"<<argc);

	string hostname = "localhost";
	string port = "30000";
	string module = "";
	string document = "";
	string file = "";
	string configfile = "";
	
	// process command-line arguments {{{
	while (1)
	{
		static struct option long_options[] =
		{
			/* These options set a flag. */
			{"verbose", no_argument,       &verbose_flag, 1},
			{"brief",   no_argument,       &verbose_flag, 0},
			/* These options don't set a flag.
			   We distinguish them by their indices. */
			{"hostname", required_argument, 0, 'h'},
			{"port",	 required_argument, 0, 'p'},
			{"module",   required_argument, 0, 'm'},
			{"document", required_argument, 0, 'd'},
			{"file",	 required_argument, 0, 'f'},
			{"config",	 required_argument, 0, 'c'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;

		int c = getopt_long (argc, argv, "h:p:m:d:f:c:",
				long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;


		switch (c)
		{

			case 0:
				/* If this option set a flag, do nothing else now. */
				if (long_options[option_index].flag != 0)
					break;
				printf ("option %s", long_options[option_index].name);
				if (optarg)
					printf (" with arg %s", optarg);
				printf ("\n");
				break;

			case 'h':
				hostname = optarg;
				break;
			case 'p':
				port = optarg;
				break;
			case 'm':
				module = optarg;
				break;
			case 'd':
				document = optarg;
				break;
			case 'f':
				file = optarg;
				break;
			case 'c':
				configfile = optarg;
				break;
			case '?':
				// getopt_long already printed an error message
				break;
			default:
				abort();
		}
	}
	// }}} process command-line arguments

	ConfigFile *config = new ConfigFile(configfile);

	IndexingClient *cli = new IndexingClient(config, hostname, port);
//	cli->Start();
	cli->Index(module, document, file);
	delete cli;
	delete config;
	return 0;
}

