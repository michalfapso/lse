#ifndef GLOBAL_H
#define GLOBAL_H

#include "common/dbg.h"
#include <cassert>
#include <string>

#define UINT32_MAX ((2 << 31) - 1)

#define CHECK_FILE(f, filename) \
	if (!f) \
	{ \
		CERR("Error opening file \""<<(filename)<<"\""); \
		EXIT(); \
	} 

typedef /*unsigned*/ int ID;
#define ID_INVALID 0

typedef float Time;

typedef float Score;
/*
std::string sqlstr(const std::string &s)
{
	DBG("sqlstr("<<s<<")...");
	std::string res = s;

	for (std::string::size_type pos = res.find('\'', 0);
		 pos != std::string::npos;
		 pos = res.find('\'', pos+1))
	{
		DBG("pos:"<<pos);
		res.insert(pos, 1, '\\');
		pos++;
	}
	DBG("sqlstr("<<s<<")="<<res<<"...done");
	return res;
}
*/

#endif
