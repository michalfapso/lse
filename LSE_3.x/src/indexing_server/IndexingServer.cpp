#include <boost/regex.hpp>
#include "IndexingServer.h"
#include "dbg.h"

#include <sstream>
#include <algorithm>

#define COMPARE_SUBSTRING(str_long, str_short) \
	(str_long.compare(0, strlen(str_short), str_short) == 0)

using namespace std;

// IndexingSession::handle_read_data() {{{
/**
 * @brief Handle client's request
 *
 * @param e Error code
 */
void IndexingSession::handle_read_data(const boost::system::error_code& e)
{
	DBG("IndexingSession::handle_read_data()");
//	DBG("mInboundData:"<<mInboundData);
	if (!e)
	{
		// Extract the data structure from the data just received.
        std::string inbound_data(&mInboundData[0], mInboundData.size());
		DBG("mInboundData:"<<inbound_data);
		//std::print(mInboundData.first(), mInboundData.last(), 0, "");
		//std::copy(mInboundData.begin(), mInboundData.end(), std::ostream_iterator<char>(std::cout, ""));

		ostringstream oss;

		if (COMPARE_SUBSTRING(inbound_data, "assign_id doc"))
		{
			boost::cmatch m;
			boost::regex expression("assign_id\\s*doc\\s*(.*)");
			if (boost::regex_match(inbound_data.c_str(), m, expression))
			{
				string doc(m[1].first, m[1].second);
				DBG("path:"<<doc);
				ID id = mpIndexingServer->AddToDocumentsList(doc);
				oss << id;
				DBG("document id '"<<id<<"' sent to client");
			}
		}

		else if (COMPARE_SUBSTRING(inbound_data, "assign_id msg"))
		{
			boost::cmatch m;
			boost::regex expression("assign_id\\s*msg\\s*([^\\s]*)\\s*(.*)");
			if (boost::regex_match(inbound_data.c_str(), m, expression))
			{
				string module(m[1].first, m[1].second);
				string msg(m[2].first, m[2].second);
				DBG("message for module '"<<module<<"': '"<<msg<<"'");
				ID id = mpIndexingServer->ProcessAssignIdMsg(module, msg);
				DBG("result:"<<id);
				oss << id;
			}
		}

		else if (COMPARE_SUBSTRING(inbound_data, "fwd_idx"))
		{
			int pos1 = inbound_data.find(' ', 0) + 1;
			int pos2 = inbound_data.find(' ', pos1);
			string module_id(inbound_data, pos1, pos2-pos1);
			//DBG("module_id: "<<module_id);
			const char * fwd_idx = &(inbound_data.c_str()[pos2+1]);
			//DBG("fwd_idx:"<<fwd_idx);
			mpIndexingServer->ProcessForwardIndex(module_id, fwd_idx);
			oss << "ok";
		}

		else if (COMPARE_SUBSTRING(inbound_data, "quit"))
		{
			DBG("quit command received");
			mpIndexingServer->Quit();
			return;
		}

		else
		{
			CERR("ERROR: Unknown message '"<<inbound_data<<"'");
		}

		DBG("sending message '"<<oss.str()<<"'...");
		mpConnection->async_write(oss.str(),
				boost::bind(&IndexingSession::handle_write, this,
					boost::asio::placeholders::error));

		// wait for other requests
		mpConnection->async_read(mInboundData,
				boost::bind(&IndexingSession::handle_read_data, this,
					boost::asio::placeholders::error));
	}
	else
	{
		if (e == boost::asio::error::eof)
		{
			DBG("end of session");
			delete this;
		}
		else
		{
			CERR("ERROR:"<<e.message());
		}
	}
}
// }}}

// IndexingSession::handle_write() {{{
void IndexingSession::handle_write(const boost::system::error_code& e)
{
	// Nothing to do here
	DBG("IndexingSession::handle_write()");
	if (e) {
		DBG("error:"<<e);
		throw boost::system::system_error(e);
	}
}
// }}}

// IndexingServer::GetId_Callback() {{{
int IndexingServer::GetId_Callback(void *id, int argc, char **argv, char **azColName){
//	*((ostringstream*)oss) << (argv[0] ? argv[0] : "NULL");// << endl;
	*((ID*)id) = (argv[0] ? atol(argv[0]) : ID_INVALID);
	return 0;
}
// }}}

// IndexingServer::IndexingServer() {{{
IndexingServer::IndexingServer (
		ConfigFile* pConfig,
		boost::asio::io_service& io_service,
		const boost::asio::ip::tcp::endpoint& endpoint) 
: 
	mpConfig(pConfig), 
	mAcceptor(io_service, endpoint)
{ 
	DBG("IndexingServer::IndexingServer()"); 

	// open database
	string database_filename = (string)mpConfig->Value("Main", "database");
	DBG("sqlite3_open(\""<<database_filename<<"\")");
	int rc = sqlite3_open(database_filename.c_str(), &mpDb);
	if( rc ){
		fprintf(stderr, "Can't open database[%s]: %s\n", database_filename.c_str(), sqlite3_errmsg(mpDb));
		sqlite3_close(mpDb);
		EXIT();
	}

	DBG("sqlite3_busy_handler("<<(int)mpDb<<")");
	sqlite3_busy_handler(mpDb, IndexingServer::SqliteBusyHandler_Callback, this);

	mpDocuments = new Documents(mpDb);

	// init modules
	mpModulesContainer = new ModulesContainer(mpConfig);
	mpModulesContainer->SetIndexingServer(this);
	mpModulesContainer->InitModules(ApplicationType::EIndexingServer);

	// Start an accept operation for a new connection.
	async_accept();
/*	IndexingSession_ptr new_session(new IndexingSession(mAcceptor.io_service(), this));
	mAcceptor.async_accept(new_session->GetSocket(),
			boost::bind(&IndexingServer::handle_accept, this,
				boost::asio::placeholders::error, new_session));
*/
}
// }}}

// IndexingServer::~IndexingServer() {{{
IndexingServer::~IndexingServer() 
{ 
	DBG("IndexingServer::~IndexingServer()"); 
	sqlite3_close(mpDb);
	delete mpModulesContainer;
	delete mpDocuments;
}
// }}}

// IndexingServer::AddToDocumentsList {{{
ID IndexingServer::AddToDocumentsList(const std::string &path)
{
	char *zErrMsg = 0;
	int rc;

	ID id = ID_INVALID;

	string sql = "BEGIN TRANSACTION; INSERT INTO docs(path) VALUES('"+path+"'); SELECT MAX(id_doc) FROM docs; COMMIT;";
	DBG("sql:"<<sql);
	rc = sqlite3_exec(mpDb, sql.c_str(), GetId_Callback, &id, &zErrMsg);
	// if there is already a record with the same path in the table,
	// just return it's id_doc value
	if( rc==SQLITE_CONSTRAINT ) {
		rc = sqlite3_exec(mpDb, "END;", NULL, NULL, &zErrMsg);

		string sql = "SELECT id_doc FROM docs WHERE path='"+path+"'";
		DBG("sql:"<<sql);
		rc = sqlite3_exec(mpDb, sql.c_str(), GetId_Callback, &id, &zErrMsg);
	} 
	// if any other error appears
	if( rc!=SQLITE_OK ){
		rc = sqlite3_exec(mpDb, "END;", NULL, NULL, &zErrMsg);

		fprintf(stderr, "SQL error[%d]: %s\n", rc, zErrMsg);
		sqlite3_free(zErrMsg);
	}
	return id;
}
// }}}

// IndexingServer::ProcessAssignIdMsg {{{
ID IndexingServer::ProcessAssignIdMsg(const std::string &moduleId, const std::string &msg)
{
	DBG("IndexingServer::ProcessAssignIdMsg("<<moduleId<<", "<<msg<<")");
	sem::M* m = mpModulesContainer->GetModule(moduleId);
	assert(m);
	return m->ProcessAssignIdMsg(msg);
}
// }}}

// IndexingServer::ProcessForwardIndex() {{{
void IndexingServer::ProcessForwardIndex(const std::string &moduleId, const std::string &fwdIndex)
{
	DBG("IndexingServer::ProcessForwardIndex("<<moduleId<<", "<<fwdIndex<<")");
	sem::M* m = mpModulesContainer->GetModule(moduleId);
	assert(m);
	m->ProcessForwardIndex(fwdIndex);
}
// }}}

int IndexingServer::SqliteBusyHandler_Callback(void *pArg, int counter)
{
//	IndexingServer* p_srv = reinterpret_cast<IndexingServer*>(pArg);
	CERR("SqliteBusyHandler_Callback()");
	return 1;
}

