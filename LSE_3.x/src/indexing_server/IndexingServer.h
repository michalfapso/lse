#ifndef INDEXINGSERVER_H
#define INDEXINGSERVER_H

#include <string>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>

#include "dbg.h"
#include "sqlite3.h"
#include "IndexingServer_Interface.h"
#include "ModulesContainer.h"
#include "ConfigFile.h"
#include "Connection.h"
#include "Documents.h"

//#define INDEXINGSERVER_INCOMING_BUFFER 500

class IndexingServer;

class IndexingSession
{
	public:
		IndexingSession(boost::asio::io_service& io_service, IndexingServer* pIndexingServer)
			: mpConnection(new Connection(io_service)), mpIndexingServer(pIndexingServer)
		{
			DBG("IndexingSession()");
		}

		~IndexingSession()
		{
			DBG("~IndexingSession()");
		}

		boost::asio::ip::tcp::socket& GetSocket() 
		{ 
			return mpConnection->GetSocket(); 
		}

		void start()
		{
			mpConnection->async_read(mInboundData,
					boost::bind(&IndexingSession::handle_read_data, this,
						boost::asio::placeholders::error));
		}
		void handle_read_data(const boost::system::error_code& e);

		void handle_write(const boost::system::error_code& e);
/*
		void handle_read_header(const boost::system::error_code& e);
*/
	private:
		Connection_ptr mpConnection;
		IndexingServer* mpIndexingServer;

		/// Holds the inbound data.
		std::vector<char> mInboundData;
};

//typedef boost::shared_ptr<IndexingSession> IndexingSession_ptr;
typedef IndexingSession* IndexingSession_ptr;


class IndexingServer: public IndexingServer_Interface
{
	protected:
		sqlite3* mpDb;
		ModulesContainer* mpModulesContainer;
		ConfigFile* mpConfig;
		Documents* mpDocuments;

		boost::asio::ip::tcp::acceptor mAcceptor;
	public:
		IndexingServer(
				ConfigFile* pConfig, 
				boost::asio::io_service& io_service,
				const boost::asio::ip::tcp::endpoint& endpoint);

		void handle_accept(const boost::system::error_code& e, IndexingSession_ptr session)
		{
			DBG("IndexingServer::handle_accept()...");
			if (!e)
			{
				session->start();
				async_accept();
			}
			else
			{
				// An error occurred. Log it and return. Since we are not starting a new
				// accept operation the io_service will run out of work to do and the
				// server will exit.
				CERR("ERROR: "<<e.message());
			}
			DBG("IndexingServer::handle_accept()...done");
		}

	public:
		~IndexingServer();

		ID AddToDocumentsList(const std::string &path);
		ID ProcessAssignIdMsg(const std::string &moduleId, const std::string &msg);
		void ProcessForwardIndex(const std::string &moduleId, const std::string &fwdIndex);
		void Quit() { mAcceptor.close(); }

		typedef int (*SqliteBusyHandler_Callback_T)(void *pArg, int counter);
	protected:
		static int SqliteBusyHandler_Callback(void *pArg, int counter);
		static int GetId_Callback(void *id, int argc, char **argv, char **azColName);

		void async_accept()
		{
			// Start an accept operation for a new connection.
			IndexingSession_ptr new_session(new IndexingSession(mAcceptor.io_service(), this));
			mAcceptor.async_accept(new_session->GetSocket(),
					boost::bind(&IndexingServer::handle_accept, this,
						boost::asio::placeholders::error, new_session));
		}

};

#endif

