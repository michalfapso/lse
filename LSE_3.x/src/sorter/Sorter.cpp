#include "Sorter.h"
#include "M/MIndexer.h"
#include "dbg.h"

#include <sstream>
#include <algorithm>

using namespace std;
using namespace sem;

// Sorter::Sorter() {{{
Sorter::Sorter (ConfigFile* pConfig) 
: 
	mpConfig(pConfig)
{ 
	DBG("IndexingServer::IndexingServer()"); 

	// open database
	string database_filename = (string)mpConfig->Value("Main", "database");
	int rc = sqlite3_open(database_filename.c_str(), &mpDb);
	if( rc ){
		fprintf(stderr, "Can't open database[%s]: %s\n", database_filename.c_str(), sqlite3_errmsg(mpDb));
		sqlite3_close(mpDb);
		exit(1);
	}

	// init modules
	mpModulesContainer = new ModulesContainer(mpConfig);
	mpModulesContainer->InitModules(ApplicationType::ESorter);
}
// }}}

// Sorter::~Sorter() {{{
Sorter::~Sorter() 
{ 
	DBG("Sorter::~Sorter()"); 
	sqlite3_close(mpDb);
	delete mpModulesContainer;
}
// }}}

void Sorter::SortAll_EachModule_Callback(void* pArg, const std::string &moduleId, sem::M* pM)
{
	//Sorter* p_sorter = reinterpret_cast<Sorter*>(pArg);
	DBG("SortAll_EachModule_Callback(\""<<moduleId<<"\")");
	pM->SortIndex();
}

void Sorter::SortAll()
{
	DBG("Sorter::SortAll()");
	mpModulesContainer->ForEachModule( SortAll_EachModule_Callback, this );
}

