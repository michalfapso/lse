#ifndef SORTER_H
#define SORTER_H

#include <string>

#include "dbg.h"
#include "sqlite3.h"
#include "ModulesContainer.h"
#include "ConfigFile.h"

class Sorter
{
	protected:
		sqlite3* mpDb;
		ModulesContainer* mpModulesContainer;
		ConfigFile* mpConfig;

	public:
		Sorter(ConfigFile* pConfig);
		~Sorter();

		static void SortAll_EachModule_Callback(void* pArg, const std::string &moduleId, sem::M* pM);
		void SortAll();
};

#endif

