#ifndef SERVER_H
#define SERVER_H

// server include files
#include <string>

#include "dbg.h"
#include "ServerSocket.h"
#include "SocketException.h"

class Server
{
	protected:
		u_short mPort;

	public:
		Server(u_short port) : mPort(port) { DBG("Server::Server()"); }
		virtual ~Server() { DBG("Server::~Server()"); }
		virtual void SetServiceParams() = 0; // abstract method - needs to be implemented in all subclasses
		virtual void SetServiceParamsSocket(ServerSocket *s) = 0;
		virtual void* GetServiceParamsPtr() = 0;

		// Start {{{
		void Start(void *(*start_routine)(void*)) 
		{
			pthread_t  tid;               /* variable to hold thread ID */

			/* Main server loop - accept and handle requests */
			CERR("Server up and running.");
			SetServiceParams();

			try
			{
				ServerSocket server ( mPort );
				while(1)
				{
					ServerSocket* p_new_sock = new ServerSocket();
					server.accept(*p_new_sock);

					SetServiceParamsSocket(p_new_sock);
					DBG_FORCE("creating server thread");
					pthread_create(&tid, NULL, start_routine, (void *) GetServiceParamsPtr() );
					DBG_FORCE("server thread created");
				}
			}
			catch (SocketException &e)
			{
				CERR("Exception was caught:" << e.description());
			}
		}
		// }}}
};

#endif

