#ifndef DBG_H
#define DBG_H

#include <string>
#include <iostream>
#include <iomanip>


//#define DBG_FORCE(str)
#define __DBG_FILENAME_WIDTH 30
#define __DBG_OUT(str) \
	std::dec \
	<< std::setprecision(13) \
	<< std::setw(__DBG_FILENAME_WIDTH) \
	<< std::left \
	<< (strlen(__FILE__) <= __DBG_FILENAME_WIDTH ? __FILE__ : std::string(__FILE__).substr(strlen(__FILE__) - __DBG_FILENAME_WIDTH, __DBG_FILENAME_WIDTH))  \
	<< ':' \
	<< std::setw(5) \
	<< std::right \
	<< __LINE__ \
	<< "> " \
	<< str \
	<< std::endl \
	<< std::flush

#ifdef DEBUG
	#define DBG_FORCE(str) std::cerr << __DBG_OUT(str)
	#define DBG(str) DBG_FORCE(str)
#else
	#define DBG_FORCE(str)
	#define DBG(str)
#endif


//#define COUT(str) std::cout << __FILE__ << ':' << std::setw(5) << __LINE__ << "> " << str << std::endl << std::flush
#define COUT(str) std::cout << __DBG_OUT(str)
//#define CERR(str) std::cerr << __FILE__ << ':' << std::setw(5) << __LINE__ << "> " << str << std::endl << std::flush
#define CERR(str) std::cerr << __DBG_OUT(str)
#define EXIT() CERR("EXIT...aborting application"); abort();
#define CINGET() CERR("Press ENTER to continue..."); std::cin.get();

#define ERROR(str) CERR("ERROR in " << __PRETTY_FUNCTION__ << ": " << str); EXIT();


#define CHECK_SQLITE_RES_OK(rc, zErrMsg, sql)			\
	if( rc!=SQLITE_OK ) {								\
		CERR("SQL error["<<rc<<"]: "<<zErrMsg<<"\n");	\
		CERR("sql: "<<sql);								\
		sqlite3_free(zErrMsg);							\
		EXIT();											\
	}


#endif
