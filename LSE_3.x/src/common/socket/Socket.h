// Definition of the Socket class

#ifndef Socket_class
#define Socket_class


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>
#include <iostream>

const int MAXHOSTNAME = 200;
const int MAXCONNECTIONS = 5;
const int MAXRECV = 500;

class Socket
{
 public:
  Socket();
  Socket(const Socket& s);
  virtual ~Socket();

  // Server initialization
  bool create();
  bool bind ( const int port );
  bool listen() const;
  bool accept ( Socket& ) const;

  // Client initialization
  bool connect ( const std::string host, const int port );

  // Data Transimission
  bool send ( const std::string ) const;
  int recv ( std::string& ) const;


  void set_non_blocking ( const bool );

  bool is_valid() const { return m_sock != -1; }

  void dbg() { 
	  std::cout << "m_sock:" << m_sock << std::endl; 
	  for (unsigned int i=0; i<sizeof(m_addr); i++) {
		  std::cout << (int)(((char*)(&m_addr))[i]) << " ";
	  }
	  std::cout << std::endl;
  }
  int get_sock() {return m_sock;}
 private:

  int m_sock;
  sockaddr_in m_addr;


};


#endif
