#ifndef QUICKSORT_H
#define QUICKSORT_H


template<typename T>
void QuickSortSwap(T* elements, int i, int j)
{	T temp = elements[i];
	elements[i] = elements[j];
	elements[j] = temp;
}

template<typename T, typename SwapArg>
void QuickSortSwap
(
	T * elements, 
	int i, 
	int j, 
	void (*fSwapAfter)(int aIdx, int bIdx, SwapArg* pArg),
	SwapArg* pSwapArg
)
{
	T temp = elements[i];
	elements[i] = elements[j];
	elements[j] = temp;
	if (fSwapAfter)
	{
		(*fSwapAfter)(i, j, pSwapArg);
	}
}


template<class T>
void QuickSort
(	T * elements,
	int first, 
	int last
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++)
			if(elements[i] < t)
			{	
				++lastLow;
				QuickSortSwap(elements, lastLow, i);
			}
		QuickSortSwap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last);
	}
}

template<class T>
void QuickSort
(	T * elements,
	int first, 
	int last,
	bool (*fCmpLess)(const T &a, const T &b)
)
{	
	if(first < last)
	{	T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++)
			if((*fCmpLess)(elements[i], t))
			{	
				++lastLow;
				QuickSortSwap(elements, lastLow, i);
			}
		QuickSortSwap(elements, first, lastLow);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1, fCmpLess);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last, fCmpLess);
	}
}



template<typename T, typename CmpArg, typename SwapArg>
void QuickSort
(	
	T * elements,
	int first, 
	int last,
	bool (*fCmpLess)(const T &a, const T &b, CmpArg* pArg),
	CmpArg* pCmpArg,
	void (*fSwapAfter)(int aIdx, int bIdx, SwapArg* pArg),
	SwapArg* pSwapArg
)
{
	if(first < last)
	{	
		T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++)
			if((*fCmpLess)(elements[i], t, pCmpArg))
			{	
				++lastLow;
				QuickSortSwap(elements, lastLow, i, fSwapAfter, pSwapArg);
			}
		QuickSortSwap(elements, first, lastLow, fSwapAfter, pSwapArg);
		if(lastLow != first) 
			QuickSort(elements, first, lastLow - 1, fCmpLess, pCmpArg, fSwapAfter, pSwapArg);
		if(lastLow != last)
			QuickSort(elements, lastLow + 1,last, fCmpLess), pCmpArg, fSwapAfter, pSwapArg;
	}
}

template<typename T, typename CmpArg, typename SwapArg>
void QuickSort_CmpIdx
(	
	T * elements,
	int first, 
	int last,
	bool (*fCmpLessIdx)(int aIdx, int bIdx, CmpArg* pArg),
	CmpArg* pCmpArg,
	void (*fSwapAfter)(int aIdx, int bIdx, SwapArg* pArg),
	SwapArg* pSwapArg
)
{
	if(first < last)
	{	
		T t = elements[first];		// t is the pivot. 
		int lastLow = first;
		for (int i = first + 1; i <= last; i++)
			if((*fCmpLessIdx)(i, first, pCmpArg))
			{	
				++lastLow;
				QuickSortSwap(elements, lastLow, i, fSwapAfter, pSwapArg);
			}
		QuickSortSwap(elements, first, lastLow, fSwapAfter, pSwapArg);
		if(lastLow != first) 
			QuickSort_CmpIdx(elements, first, lastLow - 1, fCmpLessIdx, pCmpArg, fSwapAfter, pSwapArg);
		if(lastLow != last)
			QuickSort_CmpIdx(elements, lastLow + 1,last, fCmpLessIdx, pCmpArg, fSwapAfter, pSwapArg);
	}
}

#endif
