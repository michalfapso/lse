#ifndef CHOMP_H
#define CHOMP_H

char* chomp(char *str, int len) 
{
	if (len == -1) 	{
		len = strlen(str);
	}
	while (str[len-1] == 0x0D || str[len-1] == 0x0A)
	{
		len--;
		str[len] = 0x00; // replace newline with string-terminator
	}
	return str;
}

#endif
