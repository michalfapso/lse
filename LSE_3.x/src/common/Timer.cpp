#include "timer.h"

#if defined(_WIN32) || defined(MINGW)
	
	void Timer::start(void)
	{
		static int first = 1;

		if(first) {
			QueryPerformanceFrequency(&freq);
			first = 0;
		}
		QueryPerformanceCounter(&_tstart);
	}

	void Timer::end(void)
	{
		QueryPerformanceCounter(&_tend);
	}
	double Timer::val()
	{
		return ((double)_tend.QuadPart - (double)_tstart.QuadPart) / ((double)freq.QuadPart);
	}

#else

	void Timer::start(void) {
		gettimeofday(&this->_tstart, &tz);
	}

	void Timer::end(void) {
		gettimeofday(&_tend,&tz);
	}

	double Timer::val() {
		double t1, t2;

		t1 =  (double)_tstart.tv_sec + (double)_tstart.tv_usec/(1000*1000);
		t2 =  (double)_tend.tv_sec + (double)_tend.tv_usec/(1000*1000);
		return t2-t1;
	}

#endif

