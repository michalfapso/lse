/*
	Compiling:
		g++ -Wall Chameleon.cpp ConfigFile.cpp ConfigFileTest.cpp -o ConfigFileTest.exe


	config.txt:

	[section_1]
	foo  = bar
	water= h2o

	[section_2]
	foo  = foo
	water= wet
	four = 4.2

 */

#include "ConfigFile.h"

#include <iostream>

int main() 
{
	ConfigFile cf("ConfigFileTest.ini");

	std::string foo;
	std::string water;
	double      four;

	foo   = (std::string)cf.Value("section_1","foo"  );
	water = (std::string)cf.Value("section_2","water");
	four  = (double)cf.Value("section_2","four" );

	std::cout << foo   << std::endl;
	std::cout << water << std::endl;
	std::cout << four  << std::endl;

	return 0;
}
