#ifndef DOCUMENTS_H
#define DOCUMENTS_H

#include "global.h"
#include "sqlite3.h"

class Documents
{
		sqlite3* mpDb;

		static int GetId_Callback(void *id, int argc, char **argv, char **azColName);
		static int GetString_Callback(void *str, int argc, char **argv, char **azColName);
	public:
		Documents(sqlite3* pDb) : mpDb(pDb) {}

		ID Add(const std::string &path);
		std::string GetDocName(ID docId);
		ID GetDocId(std::string docName);
};

#endif
