#ifndef CONNECTION_H
#define CONNECTION_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/tuple/tuple.hpp>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>

#include "dbg.h"

class Connection
{
	public:
/*		
		static void SendWithHeader(boost::asio::ip::tcp::socket &s, const ostringstream &ssData)
		{
			std::ostringstream ss_header;
			ss_header << std::setw(mHeaderLength) << std::hex << ssData.str().length();

			std::vector<boost::asio::const_buffer> buffers;
			buffers.push_back(boost::asio::buffer(ss_header.str()));
			buffers.push_back(boost::asio::buffer(ssData.str()));

			boost::asio::write(s, buffers);
		}
*/

		/// Constructor.
		Connection(boost::asio::io_service& io_service)
			: mSocket(io_service)
		{
		}

		~Connection()
		{
			DBG("Connection::~Connection()");
		}

		/// Get the underlying socket. Used for making a connection or for accepting
		/// an incoming connection.
		boost::asio::ip::tcp::socket& GetSocket()
		{
			return mSocket;
		}

		// write {{{
		/// Synchronously write a data structure to the socket.
		void write(const std::string &outboundData)
		{
			//DBG("Connection::async_write()");
			// Format the header.
			char outbound_header[mHeaderLength];
			sprintf(outbound_header, "%8x", outboundData.size());

			// Write the serialized data to the socket. We use "gather-write" to send
			// both the header and the data in a single write operation.
			std::vector<boost::asio::const_buffer> buffers;
			buffers.push_back(boost::asio::buffer(outbound_header));
			buffers.push_back(boost::asio::buffer(outboundData));
			boost::asio::write(mSocket, buffers);
		}
		// }}}

		// async_write {{{
		/// Asynchronously write a data structure to the socket.
		template <typename Handler>
		void async_write(const std::string &outboundData, Handler handler)
		{
			DBG("Connection::async_write()");
			// Format the header.
			char outbound_header[mHeaderLength];
			sprintf(outbound_header, "%8x", outboundData.size());

			// Write the serialized data to the socket. We use "gather-write" to send
			// both the header and the data in a single write operation.
			std::vector<boost::asio::const_buffer> buffers;
			buffers.push_back(boost::asio::buffer(outbound_header));
			buffers.push_back(boost::asio::buffer(outboundData));
			boost::asio::async_write(mSocket, buffers, handler);
		}
		// }}}



		void read(std::vector<char>& inboundData)
		{
			// read the header
			char inbound_header[mHeaderLength];

			boost::asio::read(mSocket, boost::asio::buffer(inbound_header));

			std::istringstream is(std::string(inbound_header, mHeaderLength));
			std::size_t inbound_data_size = 0;
			if (!(is >> std::hex >> inbound_data_size))
			{
				// Header doesn't seem to be valid. Inform the caller.
				DBG("ERROR: Invalid header");
				return;
			}
			//DBG("inbound_data_size:"<<inbound_data_size);

			// Start an asynchronous call to receive the data.
			inboundData.resize(inbound_data_size);
			//DBG("inboundData.size():"<<inboundData.size());

			boost::asio::read(mSocket, boost::asio::buffer(inboundData));
		}


		/// Asynchronously read a data structure from the socket.
		template <typename Handler>
		void async_read(std::vector<char>& inboundData, Handler handler)
		{
			//DBG("Connection::async_read()");
			// Issue a read operation to read exactly the number of bytes in a header.
			void (Connection::*f)(
					const boost::system::error_code&,
					std::vector<char>&, boost::tuple<Handler>)
				= &Connection::handle_read_header<Handler>;

			boost::asio::async_read(mSocket, boost::asio::buffer(mInboundHeader),
					boost::bind(f,
						this, boost::asio::placeholders::error, boost::ref(inboundData),
						boost::make_tuple(handler)));
		}

		/// Handle a completed read of a message header. The handler is passed using
		/// a tuple since boost::bind seems to have trouble binding a function object
		/// created using boost::bind as a parameter.
		template <typename Handler>
		void handle_read_header(const boost::system::error_code& e,
				std::vector<char>& inboundData, boost::tuple<Handler> handler)
		{
			//DBG("Connection::handle_read_header()");
			if (e)
			{
				boost::get<0>(handler)(e);
			}
			else
			{
				// Determine the length of the data.
				std::istringstream is(std::string(mInboundHeader, mHeaderLength));
				std::size_t inbound_data_size = 0;
				if (!(is >> std::hex >> inbound_data_size))
				{
					// Header doesn't seem to be valid. Inform the caller.
					boost::system::error_code error(boost::asio::error::invalid_argument);
					boost::get<0>(handler)(error);
					return;
				}
				//DBG("inbound_data_size:"<<inbound_data_size);

				// Start an asynchronous call to receive the data.
				inboundData.resize(inbound_data_size);
				//DBG("inboundData.size():"<<inboundData.size());
				void (Connection::*f)(
						const boost::system::error_code&,
						std::vector<char>&, boost::tuple<Handler>)
					= &Connection::handle_read_data<Handler>;

				//std::vector<char> v(inboundData.begin(), inboundData.end());
				//DBG("v.size():"<<v.size());

				boost::asio::async_read(mSocket, boost::asio::buffer(inboundData),
						boost::bind(f, this,
							boost::asio::placeholders::error, boost::ref(inboundData), handler));
			}
		}

		/// Handle a completed read of message data.
		template <typename Handler>
		void handle_read_data(const boost::system::error_code& e,
				std::vector<char>& inboundData, boost::tuple<Handler> handler)
		{
			//DBG("Connection::handle_read_data()");
			if (e)
			{
				boost::get<0>(handler)(e);
			}
			else
			{
				// Inform caller that data has been received ok.
				boost::get<0>(handler)(e);
			}
		}

	private:
		/// The underlying socket.
		boost::asio::ip::tcp::socket mSocket;

		/// The size of a fixed length header.
		enum { mHeaderLength = 8 };

		/// Holds an inbound header.
		char mInboundHeader[mHeaderLength];

};

typedef boost::shared_ptr<Connection> Connection_ptr;

#endif

