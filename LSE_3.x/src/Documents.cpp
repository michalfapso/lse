#include "Documents.h"

#include <sstream>

using namespace std;

// Documents::GetId_Callback() {{{
int Documents::GetId_Callback(void *id, int argc, char **argv, char **azColName)
{
//	*((ostringstream*)oss) << (argv[0] ? argv[0] : "NULL");// << endl;
	*((ID*)id) = (argv[0] ? atol(argv[0]) : ID_INVALID);
	return 0;
}
// }}}

// Documents::GetString_Callback() {{{
int Documents::GetString_Callback(void *str, int argc, char **argv, char **azColName)
{
	// !!! Maybe argv[0] shoud be copied to str instead of just assigning a pointer
	*((string*)str) = argv[0];
	return 0;
}
// }}}

// Documents::Add() {{{
ID Documents::Add(const std::string &path)
{
	char *zErrMsg = 0;
	int rc;

	ID id = ID_INVALID;

	string sql = "BEGIN TRANSACTION; INSERT INTO docs(path) VALUES('"+path+"'); SELECT MAX(id_doc) FROM docs; COMMIT;";
	CERR("sql:"<<sql);
	rc = sqlite3_exec(mpDb, sql.c_str(), GetId_Callback, &id, &zErrMsg);
	// if there is already a record with the same path in the table,
	// just return it's id_doc value
	if( rc==SQLITE_CONSTRAINT ) {
		rc = sqlite3_exec(mpDb, "END;", NULL, NULL, &zErrMsg);

		string sql = "SELECT id_doc FROM docs WHERE path='"+path+"'";
		CERR("sql:"<<sql);
		rc = sqlite3_exec(mpDb, sql.c_str(), GetId_Callback, &id, &zErrMsg);
	} 
	// if any other error appears
	if( rc!=SQLITE_OK ){
		rc = sqlite3_exec(mpDb, "END;", NULL, NULL, &zErrMsg);

		fprintf(stderr, "SQL error[%d]: %s\n", rc, zErrMsg);
		sqlite3_free(zErrMsg);
	}
	return id;
}
// }}}

ID Documents::GetDocId(std::string docName)
{
	char *zErrMsg = 0;
	int rc;

	ID doc_id = ID_INVALID;

	ostringstream sql;
	sql << "SELECT id_doc FROM docs WHERE path = '" << docName << "';";
	//DBG("sql:"<<sql.str());
	rc = sqlite3_exec(mpDb, sql.str().c_str(), GetId_Callback, &doc_id, &zErrMsg);
	// if any other error appears
	if( rc!=SQLITE_OK ){
		fprintf(stderr, "SQL error[%d]: %s\n", rc, zErrMsg);
		sqlite3_free(zErrMsg);
		EXIT();
	}
	return doc_id;
}

// Documents::GetDocName() {{{
std::string Documents::GetDocName(ID docId)
{
	char *zErrMsg = 0;
	int rc;

	string doc_name = "";

	ostringstream sql;
	sql << "SELECT path FROM docs WHERE id_doc = " << docId << ";";
	//DBG("sql:"<<sql.str());
	rc = sqlite3_exec(mpDb, sql.str().c_str(), GetString_Callback, &doc_name, &zErrMsg);
	// if any other error appears
	if( rc!=SQLITE_OK ){
		fprintf(stderr, "SQL error[%d]: %s\n", rc, zErrMsg);
		sqlite3_free(zErrMsg);
		EXIT();
	}
	return doc_name;
}
// }}}


