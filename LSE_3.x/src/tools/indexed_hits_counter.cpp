#include "modules/ModulesContainer.h"
#include "ConfigFile.h"
#include "modules/MKws/liblse/lat.h"

#include "sqlite3.h"

using namespace sem;
using namespace std;
using namespace lse;

static ConfigFile* get_config()
{
	ConfigFile *config = new ConfigFile();
	config->Value("MKws_indexer", "p_compute_links_likelihood", true);
	config->Value("MKws_indexer", "p_htk_remove_backslash", true);
	config->Value("MKws_indexer", "p_output_lattice_statistics", true);
	return config;
}

static ConfigFile* get_config_lvcsr()
{
	ConfigFile *config = get_config();
	config->Value("MKws_indexer", "p_data_type", "LVCSR"); // we will index LVCSR data
	config->Value("MKws", "work_with_lvcsr", "true");
	config->Value("MKws", "work_with_phn", "false");
	return config;
}

static ConfigFile* get_config_phn()
{
	ConfigFile *config = get_config();
//	config->Value("MKws_indexer", "p_write_ngrams", "/home/miso/tmp/ngrams.txt");
	config->Value("MKws_indexer", "p_data_type", "PHN"); // we will index PHN data
	config->Value("MKws_indexer", "p_generate_ngram_index", true); // index ngrams
	config->Value("MKws", "work_with_lvcsr", "false");
	config->Value("MKws", "work_with_phn", "true");
	return config;
}

class MyIndexingClient : public IndexingClient_Interface
{
		int mLastId;
	public:
		MyIndexingClient() { mLastId = 0; }
		virtual ~MyIndexingClient() {}
		virtual ID SendAssignIdMsg(const std::string &moduleId, const std::string &msg) { return ++mLastId; }
		virtual void SendForwardIndexToServer(const std::string &moduleId, const std::string &fwdIndex) {}
};

class MyMKwsIndexer : public MKwsIndexer
{
	public:
		std::ostringstream mStatistics;

		MyMKwsIndexer(MKws* pMKws, MKwsDictionary* pDictionary) : MKwsIndexer(pMKws, pDictionary) { }

		void SaveForwardIndexHitsCounts(const char* filename)
		{
			std::ofstream of(filename);
			if (!of.good())
			{
				CERR("ERROR: file '"<<filename<<"' can not be opened for writing");
				EXIT();
			}
			bool ignore_sil = (bool)mpMKws->GetConfig()->Value("MKws_indexer", "ignore_sil", false);
			// for each indexed label (word id)
			for (FwdLabelsContainer::iterator i=mpFwdLabelsContainer->begin(); i!=mpFwdLabelsContainer->end(); i++)
			{
				if (ignore_sil)
				{
					if (mpDictionary->GetUnitLabel(i->first) == "sil")
					{
						continue;
					}
				}
				of << i->first << "\t" << mpDictionary->GetUnitLabel(i->first) << "\t" << i->second->GetHits()->GetSize() << endl;
			}
		}
		unsigned int GetForwardIndexHitsCount()
		{
			unsigned int count = 0;
			bool ignore_sil = (bool)mpMKws->GetConfig()->Value("MKws_indexer", "ignore_sil", false);
			// for each indexed label (word id)
			for (FwdLabelsContainer::iterator i=mpFwdLabelsContainer->begin(); i!=mpFwdLabelsContainer->end(); i++)
			{
				//DBG("LABEL: " << mpDictionary->GetUnitLabel(i->first));
				if (ignore_sil)
				{
					if (mpDictionary->GetUnitLabel(i->first) == "sil")
					{
						continue;
					}
				}
				FwdLabel* l = i->second;
				count += l->GetHits()->GetSize();
			}
			return count;
		}
		void OutputLatticeStatistics(std::ostream &os, const lse::Lattice &lat)
		{
			mStatistics << "AllNodes: " << lat.nodes.size();
			unsigned int count = 0;
			for (Lattice::NodeMap::const_iterator i = lat.nodes.begin(); i!=lat.nodes.end(); i++)
			{
				if (i->second.W == "!NULL")
				{
					count++;
				}
			}
			mStatistics << " NullNodes: " << count;
			mStatistics << " Links: " << lat.links.size();
		}
		virtual void OnLatticeLoaded(lse::Lattice * pLat) 
		{
			string meta_W = "<sil>";
			ID meta_W_id = mpDictionary->AddUnit(meta_W);
			//DBG("meta_W_id: " << meta_W_id);

			for (Lattice::NodeMap::iterator i = pLat->nodes.begin(); i!=pLat->nodes.end(); i++)
			{
				if (mpDictionary->IsMetaWord(i->second.W_id))
				{
					i->second.W = meta_W;
					i->second.W_id = meta_W_id;
				}
			}
			vector<std::string> meta_list_new;
			meta_list_new.push_back(meta_W);
			mpDictionary->SetMetaWordsList(meta_list_new);
		}
};

class MyMKwsDictionary : public MKwsDictionary
{
	protected:
		ID mLastUnitId;
		bool mIgnoreSil;
	public:
		MyMKwsDictionary(MKws* pMKws, bool ignoreSil) : MKwsDictionary(pMKws), mLastUnitId(0), mIgnoreSil(ignoreSil) { } 

		bool UseLocal() { return true; }
		void InitGlobal() {}
		ID AddUnit(const std::string &label) 
		{ 
			//DBG("MyMKwsDictionary::AddUnit('"<<label<<"')");
			ID unit_id = ++mLastUnitId;
			AddUnitLocal(unit_id, label); 
			return unit_id;
		}

		bool IsMetaWord(ID wordId)
		{
			//DBG("IsMetaWord: "<<GetUnitLabel(wordId));
			if (mIgnoreSil && wordId == GetUnitId("sil")) 
				return true;

			return MKwsDictionary::IsMetaWord(wordId);
			// (wordId != ID_INVALID) && ((wordId == GetUnitId("!NULL") || wordId == GetUnitId("<s>") || wordId == GetUnitId("</s>")));
		}
};

class MyMKws : public MKws
{
	protected:
		MyMKwsDictionary* mpDictionaryLvcsr;
		MyMKwsIndexer*    mpIndexerLvcsr;
		MyMKwsDictionary* mpDictionaryPhn;
		MyMKwsIndexer*    mpIndexerPhn;
	public:
		MyMKws(ConfigFile* pConfig) : MKws(pConfig) 
		{
			mpDictionaryLvcsr = NULL;
			mpDictionaryPhn = NULL;

			if ((bool)GetConfig()->Value("MKws", "work_with_lvcsr", false))
			{
				mpDictionaryLvcsr = new MyMKwsDictionary(this, (bool)GetConfig()->Value("MKws_indexer", "ignore_sil", false));
				mpIndexerLvcsr = new MyMKwsIndexer(this, mpDictionaryLvcsr);
			}

			if ((bool)GetConfig()->Value("MKws", "work_with_phn", false))
			{
				mpDictionaryPhn = new MyMKwsDictionary(this, (bool)GetConfig()->Value("MKws_indexer", "ignore_sil", false));
				mpIndexerPhn = new MyMKwsIndexer(this, mpDictionaryPhn);
			}
		}
		virtual MyMKws* GetGlobalInstance() { return static_cast<MyMKws*>(mpGlobalParent); }
		virtual MyMKwsDictionary* GetDictionaryLvcsr() { return static_cast<MyMKwsDictionary*>(GetGlobalInstance()->mpDictionaryLvcsr); }
		virtual MyMKwsIndexer* GetIndexerLvcsr() { return static_cast<MyMKwsIndexer*>(mpIndexerLvcsr); }
		virtual MyMKwsDictionary* GetDictionaryPhn() { return static_cast<MyMKwsDictionary*>(GetGlobalInstance()->mpDictionaryPhn); }
		virtual MyMKwsIndexer* GetIndexerPhn() { return static_cast<MyMKwsIndexer*>(mpIndexerPhn); }
};

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		cout << "Input lattice was not defined." << endl;
		cout << "Usage: "<<argv[0]<<" [-H counts_filename] [-l skip_words_listfile] lattice_filename" << endl;
		exit(1);
	}

#ifdef NGRAMS	
	bool phn = true; // false => LVCSR
#else
	bool phn = false;
#endif

	ConfigFile *config = phn ? get_config_phn() : get_config_lvcsr();

	char* counts_filename = NULL;

	int arg_idx = 1;
	while (argv[arg_idx][0] == '-')
	{
		if (strcmp(argv[arg_idx], "-H") == 0)
		{
			arg_idx++;
			counts_filename = argv[arg_idx++];
		}
		else if (strcmp(argv[arg_idx], "-s") == 0)
		{
			arg_idx++;
			config->Value("MKws_indexer", "ignore_sil", true); // index ngrams
		}
		else if (strcmp(argv[arg_idx], "-l") == 0)
		{
			arg_idx++;
			config->Value("MKws_indexer", "meta_words_list", argv[arg_idx++]); // index ngrams
		}
	}

	MyIndexingClient cli;

	MyMKws* m = new MyMKws(config);
	m->SetIndexingClient(&cli);
	m->InitGlobal(ApplicationType::EIndexingClient);
	//DBG("dictionary_size:"<<m->GetDictionaryLvcsr()->GetSize());
	DBG("app_type:"<<m->GetAppType());

	//DBG("m->GetDictionaryLvcsr()->IsMetaWord(1):"<<m->GetDictionaryLvcsr()->IsMetaWord(1));

	// fill the index
	ID doc_id = 1;
	MyMKwsIndexer* indexer = phn ? m->GetIndexerPhn() : m->GetIndexerLvcsr();
	indexer->Index(doc_id, argv[arg_idx]);
	//DBG("dictionary_size:"<<m->GetDictionaryLvcsr()->GetSize());
	unsigned int count = indexer->GetForwardIndexHitsCount();
	DBG("GetForwardIndexHitsCount(): " << count);
	cout << indexer->mStatistics.str() << " IndexedHits: " << count << endl;

	if (counts_filename)
	{
		indexer->SaveForwardIndexHitsCounts(counts_filename);
	}

	delete config;
	return 0;
}

