#ifndef RESULTSLIST_H
#define RESULTSLIST_H

#include <list>
#include <vector>
#include <map>
#include <limits>

#include "global.h"
#include "ConfigFile.h"
#include "Documents.h"

class ResultsList
{
	public:
		enum Results_EType {
			ResultsETypeNormal,		// text format
			ResultsETypeXml,		// XML format for MBrowser
			ResultsETypeXml_std,	// XML format for STD evals
			ResultsETypeExp,		// Suitable for experiments, easy to parse
			ResultsETypeXmlTranscripts,	// XML format for MBrowser transcripts
			ResultsETypeMlf,		// MLF (Mastel Label File)
		};

		class Document;

		// class Result {{{
		class Result
		{
				Score mScore;
				Time mStartTime;
				Time mEndTime;
				std::string mStream; ///< identifier of the stream in which the result was found
				std::string mLabel; ///< label of the result (can be a found word or a phrase or sentence)
			public:
				Result(Score score = 0, Time startTime = 0, Time endTime = 0)
					: mScore(score), mStartTime(startTime), mEndTime(endTime)
				{}
				virtual ~Result() {}

				static bool CmpScore(Result* a, Result* b)
				{
					return a->GetScore() > b->GetScore();
				}

				Score GetScore() const { return mScore; }
				void SetScore(Score score) { mScore = score; }

				Time GetStartTime() const { return mStartTime; }
				void SetStartTime(Time time) { mStartTime = time; }

				Time GetEndTime() const { return mEndTime; }
				void SetEndTime(Time time) { mEndTime = time; }

				std::string GetStream() const  { return mStream; }
				void SetStream(std::string stream) { mStream = stream; }

				std::string GetLabel() const { return mLabel; }
				void SetLabel(std::string label) { mLabel = label; }

				virtual void Print(std::ostream &os, Document* pDocument, Results_EType eType, ConfigFile* config); ///< this method can be overloaded in specialized modules' result types
		};
		// }}}

		// class Document {{{
		class Document
		{
			public:
				typedef std::vector<Result*> Container;
			private:
				Container mContainer;
				ID mDocId;
				Time mStartTime;
				Time mEndTime;
				Score mScore;
				std::string mDocName;
			public:
				Document(ID docId = ID_INVALID) : mDocId(docId), mStartTime(0), mEndTime(0), mScore(-std::numeric_limits<Score>::infinity()), mDocName("") {}

				friend std::ostream& operator<<(std::ostream& os, const Document &d) {
					return os << "id:"<<d.mDocId << " t:["<<d.mStartTime<<".."<<d.mEndTime<<"] score:"<<d.mScore<<" name:"<<d.mDocName;
				}

				static bool CmpScore(Document* a, Document* b)
				{
					return a->GetScore() > b->GetScore();
				}

				void AddResult(Score score = 0, Time startTime = 0, Time endTime = 0)
				{
					if (mStartTime > startTime) mStartTime = startTime;
					if (mEndTime < endTime) mEndTime = endTime;
					if (mScore < score) 
					{
						DBG("AddResult() mScore:"<<mScore<<" score:"<<score);
						mScore = score;
					}
					mContainer.push_back(new Result(score, startTime, endTime));
				}

				void AddResult(Result* pRes)
				{
					if (mStartTime > pRes->GetStartTime()) mStartTime = pRes->GetStartTime();
					if (mEndTime < pRes->GetEndTime()) mEndTime = pRes->GetEndTime();
					if (mScore < pRes->GetScore()) 
					{
						DBG("AddResult() mScore:"<<mScore<<" score:"<<pRes->GetScore());
						mScore = pRes->GetScore();
					}
					mContainer.push_back(pRes);
				}

				typedef Container::const_iterator const_iterator;
				const_iterator begin() const { return mContainer.begin(); }
				const_iterator end() const { return mContainer.end(); }

				typedef Container::iterator iterator;
				iterator begin() { return mContainer.begin(); }
				iterator end() { return mContainer.end(); }

				ID GetDocId() { return mDocId; }
				void SetDocId(ID id) { mDocId = id; }

				std::string GetDocName() { return mDocName; }
				void SetDocName(std::string name) { mDocName = name; }

				Time GetStartTime() { return mStartTime; }
				Time GetEndTime() { return mEndTime; }
				Score GetScore() { return mScore; }

				void Sort();
		};
		// }}}

	public:
		typedef std::map<ID, Document*> Container;

	protected:
		Container mContainer;

	public:
		ResultsList() {}

		void AddResult(ID docId, Result* pRes)
		{
			Document* p_doc = NULL;
			Container::iterator i;
			if ((i = mContainer.find(docId)) == mContainer.end())
			{
				p_doc = new Document(docId);
				mContainer.insert(std::make_pair(docId, p_doc));
			}
			else
			{
				p_doc = i->second;
			}
			p_doc->AddResult(pRes);
		}

		typedef Container::const_iterator const_iterator;
		const_iterator begin() const { return mContainer.begin(); }
		const_iterator end() const { return mContainer.end(); }

		typedef Container::iterator iterator;
		iterator begin() { return mContainer.begin(); }
		iterator end() { return mContainer.end(); }

		unsigned int size() const { return mContainer.size(); }

		void Print(std::ostream &os, ConfigFile* config, Documents* pDocuments, float searchTime);
};

#endif

