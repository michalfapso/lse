#ifndef SEARCHPARSER_H
#define SEARCHPARSER_H

#include "SearchStringTokens.h"

class SearchStringParser
{
	public:
		void Parse(std::string qStr, SearchStringTokens* tokens);
};

#endif

