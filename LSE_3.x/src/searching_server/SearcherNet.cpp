#include "SearcherNet.h"

using namespace std;
using namespace sem;

void SearcherNet::ProcessQuery(std::ostringstream &oss, std::string qStr, ResultsList *pRes)
{
	SearchStringParser parser;
	SearchStringTokens tokens;
	parser.Parse(qStr, &tokens);
	// CREATE THE SEARCHING NET
	for(unsigned int i=0; i<tokens.size(); i++) {
//		oss << "Module:" << tokens[i].mModuleId << " Query:" << tokens[i].mQuery << endl;
		M* m = (mpModulesContainer->GetModule(tokens[i].mModuleId))->CreateChildInstance();
		m->ParseQuery(tokens[i].mQuery);
		mModulesList.push_back(m);
		DBG("Entropy[" << m->GetModuleId() << "]: "<< m->GetEntropy());
	}

	// GET THE SEARCH RESULTS
	unsigned int search_results_count = (int)mpConfig->Value("Main", "search_results");
	//while (pDocs->size() < search_results_count)
	//{
		for(list<M*>::iterator iM=mModulesList.begin(); iM!=mModulesList.end(); iM++)
		{
			(*iM)->GetNResults(search_results_count, pRes);
		}
	//}
	
	// DESTROY MODULES IN THE NET
	for(list<M*>::iterator iM=mModulesList.begin(); iM!=mModulesList.end(); iM++)
	{
		delete *iM;
	}
	mModulesList.clear();
}
