#include <boost/regex.hpp>

#include "SearchStringParser.h"
#include "dbg.h"

#include <sstream>

#define tMODULE_ID 1
#define tMODULE_QUERY 2
#define tUNKNOWN 99

using namespace std;

void SearchStringParser::Parse(string qStr, SearchStringTokens* tokens)
{
	DBG("lexer start...");
	SearchStringToken search_token;

	boost::cmatch m;
	boost::regex expression("([^\\s]*)\\s+(.*)");
	if (boost::regex_match(qStr.c_str(), m, expression))
	{
		search_token.mModuleId.assign(m[1].first, m[1].second);
		search_token.mQuery.assign(m[2].first, m[2].second);
		tokens->push_back(search_token);
	}
	else
	{
		CERR("ERROR: Unknown query '"<<qStr<<"'");
	}
}

