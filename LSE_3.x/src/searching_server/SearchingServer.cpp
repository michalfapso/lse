#include <boost/regex.hpp>
#include "SearchingServer.h"
#include "dbg.h"

#include <sstream>

#define COMPARE_SUBSTRING(str_long, str_short) \
	(strncmp(str_long, str_short, strlen(str_short)) == 0)

using namespace std;
using namespace sem;

// SearchingSession::handle_read_data() {{{
/**
 * @brief Handle client's request
 *
 * @param e Error code
 */
void SearchingSession::handle_read_data(const boost::system::error_code& e, size_t bytes_transferred)
{
	DBG("SearchingSession::handle_read_data()");
	DBG("bytes_transferred="<<bytes_transferred);
//	DBG("mInboundData:"<<mInboundData);
	if (!e)
	{
		mInboundData[bytes_transferred] = '\0';
		// Extract the data structure from the data just received.
		DBG("mInboundData:"<<mInboundData);
		//std::print(mInboundData.first(), mInboundData.last(), 0, "");
		//std::copy(mInboundData.begin(), mInboundData.end(), std::ostream_iterator<char>(std::cout, ""));

		ostringstream oss;

		if (COMPARE_SUBSTRING(mInboundData, "search"))
		{
			boost::cmatch m;
			boost::regex expression("search\\s*(.*)");
			if (boost::regex_match(mInboundData, m, expression))
			{
				string search_str(m[1].first, m[1].second);
				mpSearchingServer->Search(oss, search_str);
			}
		}
		else
		{
			CERR("ERROR: Unknown message '"<<mInboundData<<"'");
		}

		DBG("sending message '"<<oss.str()<<"'...");
		async_write(GetSocket(), boost::asio::buffer(oss.str()),
				boost::bind(&SearchingSession::handle_write, this,
					boost::asio::placeholders::error));

		if (COMPARE_SUBSTRING(mInboundData, "quit"))
		{
			oss << "quit" << endl;
			return;
		}

		// wait for other requests
		GetSocket().async_read_some(boost::asio::buffer(mInboundData, max_length),
				boost::bind(&SearchingSession::handle_read_data, this,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred));
	}
	else
	{
		if (e == boost::asio::error::eof)
		{
			DBG("end of session");
			delete this;
		}
		else
		{
			CERR("ERROR:"<<e.message());
		}
	}
}
// }}}

// SearchingSession::handle_write() {{{
void SearchingSession::handle_write(const boost::system::error_code& e)
{
	// Nothing to do here
	DBG("SearchingSession::handle_write()");
	if (e) {
		DBG("error:"<<e);
		throw boost::system::system_error(e);
	}
}
// }}}


// searchserver_service {{{
/**
  @brief function running in separate thread(s) to serve clients

  @param *arg structure for passing arguments to thread function
*/
/*
void * searchserver_service(void *arg)
{
	DBG("indexingserver_service()");
	SearchingServerParams* p_service_params = (SearchingServerParams *) arg;

	(*p_service_params->mpSocket) << "Hello!\n";

//	while (1) 
	{
		try
		{
			string data;
			(*p_service_params->mpSocket) >> data;
			ostringstream oss;

			string tmp1;

			if (parse(data.c_str(), 
				// begin grammar
				str_p("search") >> *space_p >> (+anychar_p)[assign_a(tmp1)],
				// end grammar
				space_p
				).full)
			{
				p_service_params->mpSearchingServer->Search(*p_service_params->mpSocket, tmp1);
			}
			(*p_service_params->mpSocket) << oss.str();
		}
		catch (SocketException &e)
		{
			CERR("Exception was caught:" << e.description());
//			break;
		}
	}

	delete p_service_params->mpSocket;
	CERR("thread exit");
	pthread_exit(0);
}
*/
// }}} 

void SearchingServer::Init(ConfigFile* pConfig)
{
	mpConfig = pConfig;

	assert(mpConfig);
	// open database
	string database_filename = (string)mpConfig->Value("Main", "database");
	DBG("sqlite3_open(\""<<database_filename<<"\")");
	int rc = sqlite3_open(database_filename.c_str(), &mpDb);
	if( rc ){
		fprintf(stderr, "Can't open database[%s]: %s\n", database_filename.c_str(), sqlite3_errmsg(mpDb));
		sqlite3_close(mpDb);
		exit(1);
	}

	mpDocuments = new Documents(mpDb);

	// init modules
	mpModulesContainer = new ModulesContainer(mpConfig);
	mpModulesContainer->SetSearchingServer(this);
	mpModulesContainer->InitModules(ApplicationType::ESearchingServer);
}

SearchingServer::SearchingServer (
		ConfigFile* pConfig)
: 
	mpAcceptor(NULL)
{ 
	DBG("SearchingServer::SearchingServer()"); 
	
	Init(pConfig);
}

SearchingServer::SearchingServer (
		ConfigFile* pConfig,
		boost::asio::io_service& io_service,
		const boost::asio::ip::tcp::endpoint& endpoint) 
: 
	mpAcceptor(new boost::asio::ip::tcp::acceptor(io_service, endpoint))
{ 
	DBG("SearchingServer::SearchingServer()"); 
	Init(pConfig);
	// Start an accept operation for a new connection.
	async_accept();
}

SearchingServer::~SearchingServer() 
{ 
	DBG("SearchingServer::~SearchingServer()"); 
	sqlite3_close(mpDb);
	delete mpModulesContainer;
	delete mpDocuments;
	if (mpAcceptor) delete mpAcceptor;
}

/**
 * @brief searching for a given query
 *
 * @param &oss client's output
 * @param qStr query
 */
void SearchingServer::Search(std::ostringstream &oss, string qStr)
{
	DBG("Searching for '"<<qStr<<"'");
//	oss << "SearchingServer::Search()"<< endl;
	boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
	// parse the query and create tokens for search modules
	ResultsList res;
	SearcherNet searcher_net(mpConfig, mpModulesContainer);
	searcher_net.ProcessQuery(oss, qStr, &res);
/*
	for(ResultsList::iterator iDoc = res.begin(); iDoc != res.end(); iDoc++)
	{
		oss << iDoc->second->GetDocId() << endl;
	}
*/
	boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
	boost::posix_time::time_duration td = t2 - t1;
	PrintResults(oss, &res, mpConfig, td.total_seconds());
}

void SearchingServer::PrintResults(std::ostream &os, ResultsList *results, ConfigFile* config, float searchTime) 
{
	results->Print(os, config, mpDocuments, searchTime);
}
