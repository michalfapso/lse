#ifndef SEARCHINGSERVER_H
#define SEARCHINGSERVER_H

#include <string>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include "Connection.h"

#include "dbg.h"
#include "SearchStringParser.h"
#include "ModulesContainer.h"
#include "ConfigFile.h"
#include "SearchingServer_Interface.h"
#include "ResultsList.h"
#include "SearcherNet.h"
#include "Documents.h"

#define SEARCHINGSERVER_INCOMING_BUFFER 500

void * searchserver_service(void *arg);

class SearchingServer;

class SearchingSession
{
	public:
		SearchingSession(boost::asio::io_service& io_service, SearchingServer* pSearchingServer)
			: mSocket(io_service), mpSearchingServer(pSearchingServer)
		{
			DBG("SearchingSession()");
		}

		~SearchingSession()
		{
			DBG("~SearchingSession()");
		}

		boost::asio::ip::tcp::socket& GetSocket() 
		{ 
			return mSocket; 
		}

		void start()
		{
			GetSocket().async_read_some(boost::asio::buffer(mInboundData, max_length),
					boost::bind(&SearchingSession::handle_read_data, this,
						boost::asio::placeholders::error,
						boost::asio::placeholders::bytes_transferred));
		}
		void handle_read_data(const boost::system::error_code& e, size_t bytes_transferred);

		void handle_write(const boost::system::error_code& e);
/*
		void handle_read_header(const boost::system::error_code& e);
*/
	private:
		boost::asio::ip::tcp::socket mSocket;
		SearchingServer* mpSearchingServer;

		/// Holds the inbound data.
		enum { max_length = 16384 };		
		char mInboundData[max_length+1];		
};

//typedef boost::shared_ptr<SearchingSession> SearchingSession_ptr;
typedef SearchingSession* SearchingSession_ptr;


class SearchingServer : public SearchingServer_Interface
{
	protected:
		sqlite3* mpDb;
		ModulesContainer* mpModulesContainer;
		ConfigFile* mpConfig;
		Documents* mpDocuments;

		boost::asio::ip::tcp::acceptor* mpAcceptor;
	public:
		SearchingServer(
				ConfigFile* pConfig);

		SearchingServer(
				ConfigFile* pConfig, 
				boost::asio::io_service& io_service,
				const boost::asio::ip::tcp::endpoint& endpoint);

		void handle_accept(const boost::system::error_code& e, SearchingSession_ptr session)
		{
			DBG("SearchingServer::handle_accept()...");
			if (!e)
			{
				session->start();
				async_accept();
			}
			else
			{
				// An error occurred. Log it and return. Since we are not starting a new
				// accept operation the io_service will run out of work to do and the
				// server will exit.
				CERR("ERROR: "<<e.message());
			}
			DBG("SearchingServer::handle_accept()...done");
		}


		SearchingServer(ConfigFile* pConfig, u_short port);
		~SearchingServer();

		void Search(std::ostringstream &oss, std::string qStr);


	protected:
		void Init(ConfigFile* pConfig);

		void async_accept()
		{
			// Start an accept operation for a new connection.
			SearchingSession_ptr new_session(new SearchingSession(mpAcceptor->io_service(), this));
			mpAcceptor->async_accept(new_session->GetSocket(),
					boost::bind(&SearchingServer::handle_accept, this,
						boost::asio::placeholders::error, new_session));
		}

		void PrintResults(std::ostream &os, ResultsList *results, ConfigFile* config, float searchTime);
};

#endif

