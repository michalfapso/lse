#ifndef SEARCHERNET_H
#define SEARCHERNET_H

#include <list>
#include <sstream>
#include "SearchStringParser.h"
#include "ResultsList.h"
#include "ConfigFile.h"
#include "ModulesContainer.h"
#include "../modules/M/M.h"

class SearcherNet
{
	protected:
		ConfigFile* mpConfig;
		ModulesContainer* mpModulesContainer;
		std::list<sem::M*> mModulesList;

	public:
		SearcherNet(ConfigFile* pConfig, ModulesContainer* pModulesContainer) : mpConfig(pConfig), mpModulesContainer(pModulesContainer) {}
		void ProcessQuery(std::ostringstream &oss, std::string qStr, ResultsList *pRes);

		void AddModule(sem::M* pM);
};

#endif
