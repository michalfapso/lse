#ifndef SEARCHSTRINGTOKENS_H
#define SEARCHSTRINGTOKENS_H

#include <vector>
#include <string>

class SearchStringToken
{
	public:
		std::string mModuleId;
		std::string mQuery;
};

class SearchStringTokens : public std::vector<SearchStringToken>
{
	
};

#endif
