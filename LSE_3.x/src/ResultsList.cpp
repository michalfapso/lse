#include "ResultsList.h"
#include "hsv2rgb.h"
#include <sstream>
#include <math.h>
#include <iomanip>
#include "QuickSort.h"

using namespace std;

void ResultsList::Result::Print(std::ostream &os, Document* pDocument, Results_EType eType, ConfigFile* config)
{
	Time start = this->GetStartTime() - (double)config->Value("MKws_searcher", "p_results_start_time_subtractor", 0.0);
	Time end = this->GetEndTime();
//	int h=0,m=0,s=0;

	switch(eType)
	{
		case ResultsETypeXml:
			os << "<hypothesis stream=\""<<this->GetStream()<<"\" "
				<< " record=\""<< pDocument->GetDocName() <<"\" "
				<< " score=\""<< fixed << setprecision(4) << this->GetScore() <<"\" >" << endl;
			os << this->GetLabel() << endl;
			os << "</hypothesis>" << endl;
			break;

		case ResultsETypeXmlTranscripts:
			{
				int h=0,m=0,s=0;
				s = (int)start + ((int)config->Value("MKws_searcher", "p_results_start_time_subtractor")); // to have the correct timelabel
				h = (int)(s / 3600);
				s %= 3600;
				m = (int)(s / 60);
				s %= 60;
				ostringstream bgcolor;
				float exp_score = exp(this->GetScore() / 50);
				int hue_max = 120;
				int hue_min = 0;
				int hue = (int)( hue_min + (exp_score * (hue_max - hue_min)) );
				float sat = 0.7f;
				float val = 1;
				int r=0, g=0, b=0;
				convert_hsv_to_rgb(hue,sat,val,&r,&g,&b);
				bgcolor.setf ( ios::hex, ios::basefield );
				bgcolor << "#" << (int)r << (int)g << (int)b;
				//bgcolor << "#" << hue << " " << sat << " " << val;
				//bgcolor << "#" << (int)r << " " << (int)g << " " << (int)b;
				os  << "    <record"
					<< " doc=\""<<pDocument->GetDocName()<<"\""
					<< " start=\""<< start <<"\""
					<< " end=\""<< end <<"\""
					<< " score=\""<< (int)(exp_score*100) <<"\""
					<< " bgcolor=\""<< bgcolor.str() <<"\""
					<< " timelabel=\""<< setw(2) << setfill('0') << h << ":" << setw(2) << setfill('0') << m << ":" << setw(2) << setfill('0') << s << "\""
					<< ">" << endl;
				os << this->GetLabel() << endl;
				os << "    </record>" << endl;
			}
			break;

		case ResultsETypeXml_std:
			os << "    <term file=\""<< pDocument->GetDocName() <<"\""
//				<< " channel=\""<<i->channel<<"\""
				<< " tbeg=\""<< start <<"\""
				<< " dur=\""<< end - start <<"\""
				<< " score=\""<< fixed << setprecision(4) << this->GetScore() <<"\""
				<< " decision=\""<< (this->GetScore() > (double)config->Value("MKws_searcher", "p_hard_decision_treshold", 0.0) ? "YES" : "NO") << "\""
				<< "/>" << endl;
			break;

		case ResultsETypeExp:
			os << pDocument->GetDocName()
				//<< " " << mpQueryKwdList->mTermId
				//<< " " << mOovCount
				<< " " << start
				<< " " << end
				<< endl;
			break;

		case ResultsETypeMlf:
			{
			unsigned long mlf_time_multiplier = 10000000;
			os << (unsigned long)(mlf_time_multiplier/100 * (round(start*100))) << " "
				<< (unsigned long)(mlf_time_multiplier/100 * (round(end*100))) << " "
				<< this->GetLabel() << " "
				<< this->GetScore()
				<< endl;
			}
			break;

		default:
			os << "HYPOTHESIS:" << endl;
	//		os << "STREAM: \t" << (i->first).stream << endl;
			//os << "STREAM: \t" << this->GetStream() << endl;
			os << "STREAM: LVCSR\t" << endl;
			os << "RECORD: \t" << pDocument->GetDocName() << endl;
			//os << "CHANNEL: \t" << i->channel << endl;
			os << "COUNT: \t1" << endl;
			os << "SCORE: \t" << fixed << setprecision(4) << this->GetScore() << endl;
			os << this->GetLabel() << "\tSTART " << start << "\tEND " << end << "\tCONFIDENCE " << this->GetScore() << endl;
			break;
	}
}

void ResultsList::Document::Sort()
{
	sort(mContainer.begin(), mContainer.end(), Result::CmpScore);
}

void ResultsList::Print(std::ostream &os, ConfigFile* config, Documents* pDocuments, float searchTime)
{
	DBG_FORCE("ResultsList::print() size:"<<this->size());
//	DBG_FORCE("count: "<<this->size());
	ResultsList::Results_EType p_results_output_type = ResultsList::ResultsETypeNormal;
	string p_results_output_type_str = (string)config->Value("Main", "results_output_type", "normal");
	DBG("p_results_output_type_str:"<<p_results_output_type_str);
	
	if (p_results_output_type_str == "normal")
	{
		p_results_output_type = ResultsList::ResultsETypeNormal;
	}
	else if (p_results_output_type_str == "xml")
	{
		p_results_output_type = ResultsList::ResultsETypeXml;
	}
	else if (p_results_output_type_str == "xml_std")
	{
		p_results_output_type = ResultsList::ResultsETypeXml_std;
	}
	else if (p_results_output_type_str == "exp")
	{
		p_results_output_type = ResultsList::ResultsETypeExp;
	}
	else if (p_results_output_type_str == "xml_transcripts")
	{
		p_results_output_type = ResultsList::ResultsETypeXmlTranscripts;
	}
	else if (p_results_output_type_str == "mlf")
	{
		p_results_output_type = ResultsList::ResultsETypeMlf;
	}
	else
	{
		CERR("results_output_type is not set properly");
		EXIT();
	}

	if (p_results_output_type == ResultsList::ResultsETypeXml)
	{
		os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << endl; 
	}
	else if (p_results_output_type == ResultsList::ResultsETypeXmlTranscripts)
	{
 		os << "<transcription>" << endl;
 		os << "  <data>" << endl;
	}
	else if (p_results_output_type == ResultsList::ResultsETypeXml_std)
	{
		CERR("ERROR: XML-STD result type is not supported");
		EXIT();
		/*
 		os << "  <detected_termlist termid=\""<<mpQueryKwdList->mTermId<<"\""
		    << " term_search_time=\""<<searchTime<<"\""
		    << " oov_term_count=\""<<mOovCount<<"\">" << endl;
		*/
	}
	else if (p_results_output_type == ResultsList::ResultsETypeXml_std)
	{
		os << "#!MLF!#" << endl;
	}
	// copy document-results to a vector and sort them by relevance
	vector<Document*> res_by_rel;
	res_by_rel.reserve(mContainer.size());
	for (ResultsList::const_iterator i=this->begin(); i!=this->end(); ++i) 
	{
		res_by_rel.push_back(i->second);
	}
	sort(res_by_rel.begin(), res_by_rel.end(), Document::CmpScore);

	for (vector<Document*>::const_iterator i=res_by_rel.begin(); i!=res_by_rel.end(); ++i) 
	{
		DBG("doc:"<<**i);
		ResultsList::Document* doc = *i;
		assert(doc);
		assert(pDocuments);
		doc->SetDocName(pDocuments->GetDocName(doc->GetDocId()));
		if (p_results_output_type == ResultsList::ResultsETypeMlf)
		{
			os << "\"" << doc->GetDocName() << "\"" << endl;
		}
		doc->Sort();
		for (ResultsList::Document::const_iterator j = doc->begin(); j != doc->end(); j++)
		{
			ResultsList::Result* res = *j;
			res->Print(os, doc, p_results_output_type, config);
		} // for each result in the document
		if (p_results_output_type == ResultsList::ResultsETypeMlf)
		{
			os << "." << endl;
		}
	} // for each document in the results list
	if (p_results_output_type == ResultsList::ResultsETypeXml_std)
	{
 		os << "  </detected_termlist>" << endl;
	}
	else if (p_results_output_type == ResultsList::ResultsETypeXmlTranscripts)
	{
 		os << "  </data>" << endl;
 		os << "</transcription>" << endl;
	}
	DBG_FORCE("ResultsList::print()...done");
}

