#ifndef MDOCUMENTSLIST_H
#define MDOCUMENTSLIST_H

#include <map>

class MDocumentsList
{
	protected:
		typedef std::map<ID, std::string> DocI2S;
		typedef std::map<std::string, ID> DocS2I;

		DocI2S mDocI2S;
		DocS2I mDocS2I;

	public:
		void AddDocument(ID id, std::string name)
		{
			mDocS2I.insert( make_pair(name, id) );
			mDocI2S.insert( make_pair(id, name) );
		}

		bool Contains(ID id) { return mDocI2S.find(id) != mDocI2S.end(); }

		bool Contains(std::string name) { return mDocS2I.find(name) != mDocS2I.end(); }
};

#endif
