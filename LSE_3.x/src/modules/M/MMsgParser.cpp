#include <boost/regex.hpp>
#include "MMsgParser.h"
#include "M.h"

using namespace std;
using namespace sem;

ID MMsgParser::ProcessAssignIdMsg(const std::string &msg)
{
	ID id = ID_INVALID;
	string s;

	boost::cmatch m;
	boost::regex expression("add_to_dictionary\\s*(.*)");
	if (boost::regex_match(msg.c_str(), m, expression))
	{
		string label(m[1].first, m[1].second);
		//DBG("tMMSGPARSER_ADD_TO_DICTIONARY");
		//DBG("word:"<<s);
		assert(mpM);
		assert(mpM->GetDictionary());
		id = mpM->GetDictionary()->AddUnit(label);
		//DBG("word:"<<s<<" ...added");
	}
	else
	{
		CERR("ERROR: Unknown message '"<<msg<<"'");
		EXIT();
	}
	return id;
}

