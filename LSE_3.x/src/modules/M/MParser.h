#ifndef MQUERY_H
#define MQUERY_H

#include <string>

#include "dbg.h"

namespace sem /* search engine module */
{

class MQuery
{
	public:
		MQuery() {}
		virtual ~MQuery() {}
		virtual Parse(std::string qStr);
};

}

#endif
