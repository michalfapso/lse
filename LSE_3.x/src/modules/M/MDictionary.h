#ifndef MDICTIONARY_H
#define MDICTIONARY_H

#include <string>
#include <map>
#include <boost/thread/mutex.hpp>

#include "dbg.h"
#include "global.h"
#include "sqlite3.h"
//#include "../Dictionary_Interface.h"

namespace sem /* search engine module */
{

class M;

int MDictionary_InitSelect_Callback(void *pDictionary, int argc, char **argv, char **azColName);

class MDictionary// : public Dictionary_Interface
{
		friend int MDictionary_InitSelect_Callback(void *pDictionary, int argc, char **argv, char **azColName);

	public:
		class Record
		{
			public:
				ID id;
				std::string word;
		};

		typedef void (*ForEachLabel_Callback_T)(void *pArg, ID labelId, const std::string &label);
		typedef void (*ForEachLabelId_Callback_T)(void *pArg, ID labelId);
		struct ForEachLabel_CallbackWithArg {
			ForEachLabel_Callback_T mpCallback;
			void* mpArg;
		};
		struct ForEachLabelId_CallbackWithArg {
			ForEachLabelId_Callback_T mpCallback;
			void* mpArg;
		};
	protected:
		M* mpM;
		std::string mTableName;
		enum Local { LOCAL_UNDEF, LOCAL_YES, LOCAL_NO };
		Local mLocal;
		boost::mutex mAddUnit_mutex;
		
		typedef std::map<ID, std::string> ContainerI2W;
		typedef std::map<std::string, ID> ContainerW2I;
		ContainerI2W mContainerI2W;
		ContainerW2I mContainerW2I;

		virtual void AddUnitLocal(ID unit_id, const std::string &label);
		virtual bool UseLocal();
/*		
		char * mpWordArray;
		unsigned long mWordArray_size;

		unsigned long * mpWordArrayIdxIDsort;
		unsigned long * mpWordArrayIdxAlphasort;
		unsigned long mWordArrayIdx_size;
*/
	public:
		MDictionary(M* pM) : mpM(pM), mLocal(LOCAL_UNDEF) {}
		virtual ~MDictionary() {}

		virtual void InitGlobal();
//		static int MDictionary_Init_Select_callback(void *mpDictionary, int argc, char **argv, char **azColName);
//		static int MDictionary_Init_Count_callback(void *mpDictionary, int argc, char **argv, char **azColName);


		/**
		 * @brief Add a new unit (word) to the dictionary
		 *
		 * @param label Unit's label (word itself)
		 *
		 * @return ID assigned to the given unit
		 */        
		virtual ID 
			AddUnit(const std::string &label);

		/**
		 * @brief Callback used for getting ID of an inserted word. Used by AddUnit()
		 */
//		static int 
//			MDictionary_GetId_Callback(void *id, int argc, char **argv, char **azColName);

		/**
		 * @brief Get ID assigned to the given unit (word)
		 *
		 * @param &label Unit's label
		 *
		 * @return Assigned ID. If there is no such word in the dictionary, return 0.
		 */
		virtual ID 
			GetUnitId(const std::string &label);

		/**
		 * @brief Get the label assigned to the given ID
		 *
		 * @param id Unit's ID
		 *
		 * @return Unit's label
		 */
		virtual std::string 
			GetUnitLabel(const ID id);

		virtual bool Exists(const ID id);

		virtual void ForEachLabel(ForEachLabel_Callback_T pCallback, void* pArg);
		virtual void ForEachLabelId(ForEachLabelId_Callback_T pCallback, void* pArg);

		std::string GetTableName() { return mTableName; }
		void SetTableName(const std::string &tableName) { mTableName = tableName; }

		unsigned int GetSize();

		void SetLocal(bool useLocal) { useLocal ? mLocal = LOCAL_YES : mLocal = LOCAL_NO; }
		Local GetLocal() { return mLocal; }

	// CALLBACKS {{{
		static int GetId_Callback(void *id, int argc, char **argv, char **azColName);
	protected:
		static int GetString_Callback(void *str, int argc, char **argv, char **azColName);
		static int ForEachLabel_Callback(void* pCwa, int argc, char **argv, char **azColName);
		static int ForEachLabelId_Callback(void* pCwa, int argc, char **argv, char **azColName);

	// }}}
};

}

#endif
