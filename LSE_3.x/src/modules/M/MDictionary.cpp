#include "MDictionary.h"
#include "M.h"
#include <sstream>
//#include <boost/algorithm/string/replace.hpp>

using namespace std;
using namespace sem;

bool MDictionary::UseLocal()
{ 
	return mLocal == LOCAL_YES;
//		mpM->GetAppType() == ApplicationType::EIndexingClient ||
//		mpM->GetAppType() == ApplicationType::ESearchingServer;
}

void MDictionary::InitGlobal()
{
	if (mLocal == LOCAL_UNDEF)
	{
		if (mpM->GetAppType() == ApplicationType::EIndexingClient ||
			mpM->GetAppType() == ApplicationType::ESearchingServer)
		{
			mLocal = LOCAL_YES;
		} else {
			mLocal = LOCAL_NO;
		}
	}

	if (UseLocal())
	{
		char *zErrMsg = 0;
		int rc;

		if (!mpM->GetDb())
		{
			CERR("Warning: no database specified, so no dictionary is loaded");
		}
		else
		{
			//DBG("Loading the dictionary...");
			/*
			ID max_id_unit;
			string sql = "SELECT MAX(id_unit), SUM(LENGTH(path)) FROM "+GetTableName();
			rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), GetId_Callback, max_id_unit, &zErrMsg);
			CHECK_SQLITE_RES_OK(rc, zErrMsg, sql);
			mContainerI2W.reserve(max_id_unit);
			*/
			string sql = "SELECT id_unit, label FROM "+GetTableName()+" ORDER BY id_unit ASC";
			rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), MDictionary_InitSelect_Callback, this, &zErrMsg);
			CHECK_SQLITE_RES_OK(rc, zErrMsg, sql);

			//DBG("Loading the dictionary...done");
		}
	}
}


int sem::MDictionary_InitSelect_Callback(void *pDictionary, int argc, char **argv, char **azColName)
{
	(static_cast<MDictionary*>(pDictionary))->AddUnitLocal(atol(argv[0]), argv[1]);
	return 0;
}

int MDictionary::GetId_Callback(void *id, int argc, char **argv, char **azColName) 
{
	*((ID*)id) = atol(argv[0]);
	return 0;
}

int MDictionary::GetString_Callback(void *str, int argc, char **argv, char **azColName)
{
	// !!! Maybe argv[0] shoud be copied to str instead of just assigning a pointer
	str = argv[0];
	return 0;
}

void MDictionary::AddUnitLocal(ID unit_id, const std::string &label) 
{
	//DBG("MDictionary::AddUnitLocal("<<unit_id<<", "<<label<<")");
	//mContainerI2W[unit_id] = label;
	//mContainerW2I[label] = unit_id;
	mContainerI2W.insert(make_pair(unit_id, label));
	mContainerW2I.insert(make_pair(label, unit_id));
}

ID MDictionary::AddUnit(const std::string &label)
{
	DBG("MDictionary::AddUnit("<<label<<")");
	ID id = ID_INVALID;
	// In case of IndexingClient, it is needed to ask server
	// to add the unit and assign an ID to it.
	
	//DBG("MDictionary::AddUnit() ApplicationType="<<ApplicationType::str(mpM->GetAppType()));
	if (mpM->GetAppType() == ApplicationType::EIndexingClient)
	{
		id = mpM->GetIndexingClient()->SendAssignIdMsg(mpM->GetModuleId(), "add_to_dictionary "+label);
		AddUnitLocal(id, label);
		return id;
	}
	else if (mpM->GetAppType() == ApplicationType::EIndexingServer)
	{
		//string label_sql = boost::algorithm::replace_all_copy(label, "'", "\\'");
		boost::mutex::scoped_lock scoped_lock(mAddUnit_mutex);
		DBG("AddUnit()...mutex in");
		string sql = "BEGIN TRANSACTION; INSERT INTO "+mTableName+"(label) VALUES(\""+label+"\"); SELECT seq FROM sqlite_sequence WHERE name='"+mTableName+"'; COMMIT;";
		DBG("sql:"<<sql);
		int rc;
		char* zErrMsg = 0;
		rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), GetId_Callback, &id, &zErrMsg);
		// if there is already a record with the same label in the table,
		// just return it's id_unit value
		if( rc==SQLITE_CONSTRAINT ) {
			rc = sqlite3_exec(mpM->GetDb(), "END;", NULL, NULL, &zErrMsg);
			string sql = "SELECT id_unit FROM "+mTableName+" WHERE label=\""+label+"\"";
			DBG("sql:"<<sql);
			rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), GetId_Callback, &id, &zErrMsg);
		} 
		DBG("GetDb():("<<(int)mpM->GetDb()<<")");
		// if any other error appears
		if( rc!=SQLITE_OK ){
			rc = sqlite3_exec(mpM->GetDb(), "END;", NULL, NULL, &zErrMsg);
			CERR("SQL error["<<rc<<"]: "<<zErrMsg);
			sqlite3_free(zErrMsg);
			EXIT();
			return ID_INVALID;
		}
		DBG("AddUnit()...mutex out");
		return id;
	}
	return ID_INVALID; 
}

ID MDictionary::GetUnitId(const std::string &label)
{
	//DBG("MDictionary::GetUnitId("<<label<<")...UseLocal():"<<UseLocal());
	if (UseLocal())
	{
		//DBG("mContainerW2I:"<<mContainerW2I[label]);
		ContainerW2I::iterator i = mContainerW2I.find(label);
		return i == mContainerW2I.end() ? ID_INVALID : i->second;
	}
	ID id = ID_INVALID;
	DBG("mTableName:"<<mTableName);
	DBG("label:"<<label);
	string sql = "SELECT id_unit FROM "+mTableName+" WHERE label=\""+label+"\"";
	DBG("sql:"<<sql);
	char* zErrMsg = 0;
	int rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), GetId_Callback, &id, &zErrMsg);
	if (rc != SQLITE_OK) {
		CERR("SQL error["<<rc<<"]: "<<zErrMsg);
		EXIT();
		sqlite3_free(zErrMsg);
		return ID_INVALID;
	}
	DBG("id="<<id);
	return id;
}

std::string MDictionary::GetUnitLabel(const ID id) 
{
	if (UseLocal())
	{
		return mContainerI2W[id];
	}
	char* label = 0;
	ostringstream ss;
	ss << "SELECT label FROM "<<mTableName<<" WHERE id_unit="<<id;
	string sql = ss.str();
//	string sql = "SELECT label FROM "+mTableName+" WHERE id_unit="+id;
	//DBG("sql:"<<sql);
	char* zErrMsg = 0;
	int rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), GetString_Callback, &label, &zErrMsg);
	if (rc != SQLITE_OK) {
		CERR("SQL error["<<rc<<"]: "<<zErrMsg);
		sqlite3_free(zErrMsg);
		return "";
	}
	return label;
}

bool MDictionary::Exists(const ID id)
{
	if (UseLocal())
	{
		return mContainerI2W.find(id) != mContainerI2W.end();
	}
	int count = 0;
	ostringstream ss;
	ss << "SELECT COUNT(*) FROM "<<mTableName<<" WHERE id_unit="<<id;
	string sql = ss.str();
//	string sql = "SELECT label FROM "+mTableName+" WHERE id_unit="+id;
	//DBG("sql:"<<sql);
	char* zErrMsg = 0;
	int rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), GetId_Callback, &count, &zErrMsg);
	if (rc != SQLITE_OK) {
		CERR("SQL error["<<rc<<"]: "<<zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}
	return count != 0;
}

int MDictionary::ForEachLabel_Callback(void* pCwa, int argc, char **argv, char **azColName)
{
	//DBG("ForEachLabel_Callback()");
	MDictionary::ForEachLabel_CallbackWithArg* p_cwa = reinterpret_cast<MDictionary::ForEachLabel_CallbackWithArg*>(pCwa);
	(*(p_cwa->mpCallback))(p_cwa->mpArg, atol(argv[0]), argv[1]);
	return 0;
}

void MDictionary::ForEachLabel(ForEachLabel_Callback_T pCallback, void* pArg)
{
	DBG("MDictionary::ForEachLabel()");
	if (UseLocal())
	{
		DBG("local");
		for (ContainerI2W::iterator i = mContainerI2W.begin(); i != mContainerI2W.end(); i++)
		{
			(*pCallback)(pArg, i->first, i->second);
		}
	}
	else
	{
		ostringstream ss;
		ss << "SELECT id_unit, label FROM "<<mTableName;
		string sql = ss.str();
		//DBG("sql:"<<sql);
		char* zErrMsg = 0;
		ForEachLabel_CallbackWithArg cwa;
		cwa.mpCallback = pCallback;
		cwa.mpArg = pArg;
		int rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), ForEachLabel_Callback, reinterpret_cast<void*>(&cwa), &zErrMsg);
		CHECK_SQLITE_RES_OK(rc, zErrMsg, sql);
	}
}

int MDictionary::ForEachLabelId_Callback(void* pCwa, int argc, char **argv, char **azColName)
{
	MDictionary::ForEachLabelId_CallbackWithArg* p_cwa = reinterpret_cast<MDictionary::ForEachLabelId_CallbackWithArg*>(pCwa);
	(*(p_cwa->mpCallback))(p_cwa->mpArg, atol(argv[0]));
	return 0;
}

void MDictionary::ForEachLabelId(ForEachLabelId_Callback_T pCallback, void* pArg)
{
	DBG("ForEachLabelId(): UseLocal="<<UseLocal());
	if (UseLocal())
	{
		for (ContainerI2W::iterator i = mContainerI2W.begin(); i != mContainerI2W.end(); i++)
		{
			(*pCallback)(pArg, i->first);
		}
	}
	else
	{
		ostringstream ss;
		ss << "SELECT id_unit FROM "<<mTableName;
		string sql = ss.str();
		//DBG("sql:"<<sql);
		char* zErrMsg = 0;
		ForEachLabelId_CallbackWithArg cwa;
		cwa.mpCallback = pCallback;
		cwa.mpArg = pArg;
		int rc = sqlite3_exec(mpM->GetDb(), sql.c_str(), MDictionary::ForEachLabelId_Callback, reinterpret_cast<void*>(&cwa), &zErrMsg);
		CHECK_SQLITE_RES_OK(rc, zErrMsg, sql);
	}
}

unsigned int MDictionary::GetSize()
{
	assert(mContainerW2I.size() == mContainerI2W.size());
	return mContainerI2W.size();
}
