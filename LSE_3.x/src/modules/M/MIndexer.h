#ifndef MINDEXER_H
#define MINDEXER_H

#include <string>

#include "dbg.h"
#include "global.h"

namespace sem /* search engine module */
{

class M;

enum IndexType
{
	EForwardIndex,
	EInvertedIndex,
};

class MIndexer
{
	protected:
		M* mpM;

	public:
		MIndexer(M* pM) : mpM(pM) {}
		virtual ~MIndexer() {}

		virtual void InitGlobal() { DBG("MIndexer::InitGlobal()"); }

	   	virtual void Index(ID docId, const std::string &filename) = 0;

		virtual void ProcessForwardIndex(const std::string &fwdIndex) = 0;

		virtual void Sort() = 0;
};

}

#endif
