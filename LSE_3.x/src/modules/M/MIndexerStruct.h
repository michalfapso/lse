/**
 * 1. The indexing client calls Index() method of a given module's indexer class.
 *    Forward index is filled and finally serialized and sent to the indexing server.
 * 2. The indexing server receives the forward index and calls the given module's 
 *    ProcessForwardIndex() method.
 * 3. The forward index is deserialized and it's method AddToInvLabelsContainer()
 *    is called. In this method, each label is created in the inverted index and
 *    it's AddDocumentHits() method is called.
 * 4. AddDocumentHits() adds the current label's hits to the inverted index
 *    and adds records in DocumentsSortedByRelevance and DocumentsSortedById indices.
 *    Hits are sorted by time implicitly, but the other two indices are not sorted.
 *    So they have to be sorted explicitely by calling ...
 */
#ifndef MINDEXERSTRUCT_H
#define MINDEXERSTRUCT_H

#include <string>
#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <memory>

#include <boost/mpl/and.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/archive/archive_exception.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/tuple/tuple.hpp>

#include <boost/thread/mutex.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>

#include <limits.h>
#include <stdint.h>

#include "dbg.h"
#include "global.h"

#include "MIndexer.h"

namespace sem /* search engine module */
{

class MIndexerStruct : public MIndexer
{
	//--------------------------------------------------
	// INNER CLASSES 
	//--------------------------------------------------
	public:
		class Hits;
		class Hits_OneDocHitList;
		class InvLabelsContainer;

		//--------------------------------------------------
		// WriteVector<T> {{{
		//--------------------------------------------------
		template <typename T, template <typename U, class = std::allocator<U> > class Container>
		static void WriteVector(Container<T>& container, std::ostream &os)
		{
			// write header (number of records in the hit list)
			assert(container.size() < (unsigned int)UINT32_MAX);
			uint32_t size = container.size();
			os.write(reinterpret_cast<const char *>(&size), sizeof(size));
			//DBG("WriteVector() size="<<size);

			typedef typename Container<T>::iterator Iterator;
			for (Iterator i=container.begin(); i!=container.end(); i++)
			{
				i->Write(os);
			}
		}
		// }}}

		//--------------------------------------------------
		// ReadVector<T> {{{
		//--------------------------------------------------
		template <typename T, template <typename U, class = std::allocator<U> > class Container>
		static void ReadVector(Container<T>& container, std::istream &is)
		{
			typedef typename Container<T>::iterator Iterator;
			// read header (number of records in the hit list)
			uint32_t old_size = container.size();
			uint32_t size;
			is.read(reinterpret_cast<char *>(&size), sizeof(size));
			size += old_size;
			container.resize(size);
			

			// read the record list
			assert (typeid(container) != typeid(std::vector<T>));
			// sequential access container
			Iterator i = container.begin();
			for (unsigned int idx=0; idx < old_size; idx++, i++) {}
			for (; i != container.end() && !is.eof(); i++)
			{
				i->Read(is);
			}
			//assert(i == size);
		}
		template <typename T>
		static void ReadVector(std::vector<T>& container, std::istream &is)
		{
			typedef typename std::vector<T>::iterator Iterator;
			// read header (number of records in the hit list)
			uint32_t old_size = container.size();
			uint32_t size;
			is.read(reinterpret_cast<char *>(&size), sizeof(size));
			size += old_size;
			container.resize(size);
			

			// read the record list
			// random access container
			for (unsigned int i = old_size; i < size && !is.eof(); i++)
			{
				container[i].Read(is);
			}
			//assert(i == size);
		}
		// }}}

		//--------------------------------------------------
		// DocumentsSortedByRelevance {{{
		//--------------------------------------------------
		class DocumentsSortedByRelevance
		{
			public:
				static std::string FILENAME_EXTENSION;
				static std::string FILENAME_EXTENSION_NOT_SORTED;
			protected:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					ar & mContainer;
				}
			public:
				class Record
				{
					protected:
						friend class boost::serialization::access;
						template<class Archive>
						void serialize(Archive & ar, const unsigned int version)
						{
							ar & mDocId;
							ar & mRelevance;
						}
					protected:
						ID mDocId;
						Score mRelevance;
					public:
						Record(ID docId, Score relevance)
							: mDocId(docId), mRelevance(relevance) {}

						Record()
							: mDocId(ID_INVALID), mRelevance(0.0) {}

						void Write(std::ostream &os) const;
						bool Read(std::istream &is);

						ID GetDocId() const { return mDocId; }
						Score GetRelevance() const { return mRelevance; }

						void SetDocId(ID docId) { mDocId = docId; }
						void SetRelevance(Score relevance) { mRelevance = relevance; }

						static bool CmpSort(const Record &r1, const Record &r2) { return r1.mRelevance > r2.mRelevance; }

						friend bool operator==(const Record& r1, const Record& r2) {
							return r1.mDocId == r2.mDocId && r1.mRelevance == r2.mRelevance;
						}

						friend std::ostream& operator<<(std::ostream& os, const Record &r) {
							return os << "d:"<<r.mDocId << "\tr:"<<r.mRelevance;
						}
				};
			protected:
				typedef std::vector<Record> Container;
				Container mContainer;
				bool mSorted;
			public:
				DocumentsSortedByRelevance() : mSorted(true) {}
				static bool Exists(const std::string &baseFilename, bool sorted);
				void AddRecord(const Record& rec);
				void AddRecord(const std::string &baseFilename, const Record& pDoc);
				void Write(std::ostream &os);
				void Read(std::istream &is, bool sorted);
				void Save(const std::string &baseFilename);
				void SaveAppend(const std::string &baseFilename);
				void Load(const std::string &baseFilename, bool sorted);
				void Sort();
				void JoinSameDocId();
				void RemoveUnsorted(const std::string &baseFilename);
				bool IsSorted() { return mSorted; }
				unsigned int GetSize() { return mContainer.size(); }
				//void SetRecordHitListPointer(unsigned int index, Hits_OneDocHitList* pHitList);
				const Record& operator[](const unsigned int i) {
					assert(i < mContainer.size());
					return mContainer[i];
				}

				friend bool operator==(const DocumentsSortedByRelevance& d1, const DocumentsSortedByRelevance& d2) {
					return d1.mContainer == d2.mContainer && d1.mSorted == d2.mSorted;
				}

				friend std::ostream& operator<<(std::ostream& os, const DocumentsSortedByRelevance &d) {
					for (unsigned int i=0; i<d.mContainer.size(); i++) {
						os << d.mContainer[i] << std::endl;
					}
					return os;
				}
		};
		// }}}

		//--------------------------------------------------
		// DocumentsSortedById {{{
		//--------------------------------------------------
		class DocumentsSortedById
		{
			public:
				static std::string FILENAME_EXTENSION;
				static std::string FILENAME_EXTENSION_NOT_SORTED;
			protected:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					ar & mContainer;
				}
			public:
				class Record
				{
					protected:
						friend class boost::serialization::access;
						template<class Archive>
						void serialize(Archive & ar, const unsigned int version)
						{
							ar & mDocId;
							ar & mHitListStartPos;
							ar & mHitListRecordsCount;
						}
					protected:
						ID mDocId;
						unsigned int mHitListStartPos; ///< index of the first record in the Hits::Record vector
						unsigned int mHitListRecordsCount; ///< count of records in the hit list
					public:
						Record(ID docId, unsigned int pHitListStartPos, unsigned int pHitListRecordsCount)
							: mDocId(docId), mHitListStartPos(pHitListStartPos), mHitListRecordsCount(pHitListRecordsCount) {}

						Record()
							: mDocId(ID_INVALID), mHitListStartPos(0), mHitListRecordsCount(0) {}

						void Write(std::ostream &os) const;
						bool Read(std::istream &is);

						ID GetDocId() const { return mDocId; }
						unsigned int GetHitListStartPos() const { return mHitListStartPos; }
						unsigned int GetHitListRecordsCount() const { return mHitListRecordsCount; }

						void SetDocId(ID docId) { mDocId = docId; }
						void SetHitListStartPos(unsigned int hitListStartPos) { mHitListStartPos = hitListStartPos; }
						void AddHitListStartPos(unsigned int addVal) { mHitListStartPos += addVal; }
						void SetHitListRecordsCount(unsigned int hitListRecordsCount) { mHitListRecordsCount = hitListRecordsCount; }

						static bool CmpSort(const Record &r1, const Record &r2) { return r1.mDocId < r2.mDocId; }

						friend bool operator==(const Record& r1, const Record& r2) {
							return r1.mDocId == r2.mDocId;
						}

						friend std::ostream& operator<<(std::ostream& os, const Record &r) {
							return os << "d:"<<r.mDocId << "\ts:"<<r.mHitListStartPos << "\tc:"<<r.mHitListRecordsCount;
						}
				};
			protected:
				typedef std::vector<Record> Container;
				Container mContainer;
				bool mSorted;
			public:
				DocumentsSortedById() : mSorted(true) {}
				static bool Exists(const std::string &baseFilename, bool sorted);
				void AddRecord(const Record& rec);
				void AddRecord(const std::string &baseFilename, const Record& rec);
				void Write(std::ostream &os);
				void Read(std::istream &is, bool sorted, unsigned int oldHitsSize = 0);
				void Save(const std::string &baseFilename);
				void SaveAppend(const std::string &baseFilename);
				void Load(const std::string &baseFilename, bool sorted, unsigned int oldHitsSize = 0);
				void Sort();
				void JoinSameDocId();
				void RemoveUnsorted(const std::string &baseFilename);
				bool IsSorted() { return mSorted; }
				unsigned int GetSize() const { return mContainer.size(); }
				//void SetRecordHitListPointer(unsigned int index, Hits_OneDocHitList* pHitList);
				const Record& operator[](const unsigned int i) {
					assert(i < mContainer.size());
					return mContainer[i];
				}
				const Record& GetRecordByDocId(ID docId);

				void ShiftHitListStart(unsigned int shift) {
					for(Container::iterator i=mContainer.begin(); i!=mContainer.end(); i++)
					{
						i->AddHitListStartPos(shift);
					}
				}

				friend bool operator==(const DocumentsSortedById& d1, const DocumentsSortedById& d2) {
					return d1.mContainer == d2.mContainer && d1.mSorted == d2.mSorted;
				}

				friend std::ostream& operator<<(std::ostream& os, const DocumentsSortedById &d) {
					for (unsigned int i=0; i<d.mContainer.size(); i++) {
						os << d.mContainer[i] << std::endl;
					}
					return os;
				}
		};
		// }}}

		//--------------------------------------------------
		// Hits_OneDocHitList {{{
		//--------------------------------------------------
		class Hits_OneDocHitList
		{
			private:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					ar & mContainer;
				}
			public:
				class Record
				{
					private:
						friend class boost::serialization::access;
						template<class Archive>
						void serialize(Archive & ar, const unsigned int version)
						{
							ar & mScore;
							ar & mStartTime;
							ar & mEndTime;
						}
					protected:
						friend bool CmpScore(const Record &r1, const Record &r2);
						Score mScore;
						Time mStartTime;
						Time mEndTime;
					public:
						Record()
							: mScore(0.0), mStartTime(0.0), mEndTime(0.0) {}
						Record(Score score, Time startTime, Time endTime)
							: mScore(score), mStartTime(startTime), mEndTime(endTime) {}

						void Write(std::ostream &os);
						bool Read(std::istream &is);

						void Join(const Record& with);

						Score GetScore() const { return mScore; }
						Time GetStartTime() const { return mStartTime; }
						Time GetEndTime() const { return mEndTime; }

						friend bool operator==(const Record& r1, const Record& r2) {
							return r1.mScore == r2.mScore && r1.mStartTime == r2.mStartTime && r1.mEndTime == r2.mEndTime;
						}

						friend std::ostream& operator<<(std::ostream& os, const Record &r) {
							return os << "s:"<<r.mScore << "\tt:"<<r.mStartTime<<".."<<r.mEndTime;
						}

						bool IsOverlappingWith(const Record &rec2) const
						{
							return !(mEndTime < rec2.mStartTime || mStartTime > rec2.mEndTime);
						}
						static bool CmpScore(const Record &r1, const Record &r2) { return r1.mScore < r2.mScore; }
						static bool CmpTime(const Record &r1, const Record &r2) { return r1.mStartTime < r2.mStartTime; }
				};
			protected:
				typedef std::list<Record> Container;
				Container mContainer;
			public:
				void AddRecord(const Record &rec, bool checkOverlapping);
				Score GetBestScore() { return (*max_element(mContainer.begin(), mContainer.end(), Record::CmpScore)).GetScore(); }
				unsigned int GetSize() { return mContainer.size(); }
				void Write(std::ostream &os);
				void Read(std::istream &is);
				unsigned int GetUsedMemoryBytes() { return mContainer.size() * sizeof(Hits_OneDocHitList::Record); }

				typedef Container::iterator iterator;
				typedef Container::const_iterator const_iterator;
				typedef Container::reverse_iterator reverse_iterator;
				typedef Container::const_reverse_iterator const_reverse_iterator;

				iterator begin() { return mContainer.begin(); }
				const_iterator begin() const { return mContainer.begin(); }
				iterator end() { return mContainer.end(); }
				const_iterator end() const { return mContainer.end(); }
				reverse_iterator rbegin() { return mContainer.rbegin(); }
				const_reverse_iterator rbegin() const { return mContainer.rbegin(); }
				reverse_iterator rend() { return mContainer.rend(); }
				const_reverse_iterator rend() const { return mContainer.rend(); }

				iterator erase(iterator i) { return mContainer.erase(i); }
				/*
				Record& operator[](const int i) {
					return mContainer[i];
				}
				*/
				friend bool operator==(const Hits_OneDocHitList& d1, const Hits_OneDocHitList& d2) {
					return d1.mContainer == d2.mContainer;
				}

				friend std::ostream& operator<<(std::ostream& os, const Hits_OneDocHitList &d) {
					for (const_iterator i=d.begin(); i!=d.end(); i++) {
						os << *i << std::endl;
					}
					return os;
				}
		};
		// }}}

		//--------------------------------------------------
		// Hits {{{
		//--------------------------------------------------
		class Hits
		{
			public:
				static std::string FILENAME_EXTENSION;
				static std::string FILENAME_EXTENSION_NOT_SORTED;
				typedef Hits_OneDocHitList::Record Record;
			protected:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					ar & mContainer;
				}

				typedef std::vector<Record> Container;
				Container mContainer;
				bool mSorted;
				struct CmpSortArg
				{
					Container* mpHits;
					std::vector<ID>* mpHitsDoc;
				};

				//Hits_OneDocHitList* GetLastHitList() { return *(mContainer.rbegin()); }
			public:
				Hits() { mSorted = false; /* to prevent overwriting of a sorted index */ }
				
				static bool SortCmp(int aIdx, int bIdx, CmpSortArg* pArg);
				static void SortSwap(int aIdx, int bIdx, CmpSortArg* pArg);

				static bool Exists(const std::string &baseFilename, bool sorted);
				unsigned int UpdateRecordsCount_Add(std::iostream &ios, unsigned int i);
				//void AddHits(Hits* pHits);
				//void AddOneDocHitList(Hits_OneDocHitList* pHits);
				unsigned int AddHits (std::ostream &os, Hits_OneDocHitList* pHits);
				unsigned int AddHits (const std::string &baseFilename, Hits_OneDocHitList* pHits);
				unsigned int AddHits (Hits_OneDocHitList* pHits);
				//void Save(const std::string &baseFilename);
				void Read(std::istream &is, bool sorted, unsigned int size = 0);
				void Load(const std::string &baseFilename, bool sorted);
				void Save(const std::string &baseFilename);
				unsigned int SaveAppend(const std::string &baseFilename);
				void RemoveUnsorted(const std::string &baseFilename);
				void Write(std::ostream &os);
				void Sort(DocumentsSortedById* pDocsById);
				unsigned int GetSize() { return mContainer.size(); }
				//Hits_OneDocHitList* GetHitListPointer(unsigned int index) { assert(index < mContainer.size()); return mContainer[index]; }
				unsigned int GetUsedMemoryBytes() { return mContainer.size() * sizeof(Hits_OneDocHitList::Record); }

				Record& operator[](const int i) {
					assert((unsigned int )i < mContainer.size());
					return mContainer[i];
				}

				friend bool operator==(const Hits& h1, const Hits& h2) {
					return h1.mContainer == h2.mContainer;
				}

				friend std::ostream& operator<<(std::ostream& os, const Hits &h) {
					for (unsigned int i=0; i<h.mContainer.size(); i++) {
						os << h.mContainer[i] << std::endl;
					}
					return os;
				}
		};
		// }}}

		//--------------------------------------------------
		// Label {{{
		//--------------------------------------------------
		class Label
		{
			protected:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					//ar & mLabelId;
				}
			protected:
				//ID mLabelId;
			public:
				Label() {}
		};
		// }}}

		//--------------------------------------------------
		// FwdLabel {{{
		//--------------------------------------------------
		class FwdLabel : public Label
		{
			protected:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					ar & boost::serialization::base_object<Label>(*this);
					ar & mpHits;
				}
			protected:
				Hits_OneDocHitList* mpHits;
			public:
				FwdLabel() 
					: Label(), mpHits(new Hits_OneDocHitList) {}

				~FwdLabel() { delete mpHits; }

				void AddHit(const Hits_OneDocHitList::Record &rec, bool checkOverlapping);

				Hits_OneDocHitList* GetHits() { return mpHits; }

				Score GetRelevance() { return mpHits->GetBestScore(); }

				friend bool operator==(const FwdLabel& l1, const FwdLabel& l2) {
					return *l1.mpHits == *l2.mpHits;
				}

				friend std::ostream& operator<<(std::ostream& os, const FwdLabel &l) {
					return os << *l.mpHits;
				}
		};
		// }}}

		//--------------------------------------------------
		// InvLabel {{{
		//--------------------------------------------------
		class InvLabel : public Label
		{
			protected:
				std::string mPath;
				Hits* mpHits;
				DocumentsSortedByRelevance* mpDocsByRelevance;
				DocumentsSortedById* mpDocsById;
				InvLabelsContainer* mpParentInvLabelsContainer;
			public:
				InvLabel(const std::string &path, InvLabelsContainer* parentContainer = NULL);
				~InvLabel()
				{
					if(mpDocsById) { delete mpDocsById; mpDocsById=NULL; }
					if(mpDocsByRelevance) { delete mpDocsByRelevance; mpDocsByRelevance=NULL; }
					if(mpHits) { delete mpHits; mpHits=NULL; }
				}

				static bool Exists(const std::string path, bool sorted);
				bool Exists(bool sorted);
				void AddDocumentHits(ID docId, Score relevance, Hits_OneDocHitList* pHits);
				void AddDocumentHits_NoCache(ID docId, Score relevance, Hits_OneDocHitList* pHits);
				void Load(bool sorted);
				void Save();
				void SaveAppend();
				void Sort();
				void RemoveUnsorted();

				std::string GetPath() { return mPath; }
				DocumentsSortedByRelevance* GetDocsByRelevance() { return mpDocsByRelevance; }
				DocumentsSortedById* GetDocsById() { return mpDocsById; }
				Hits* GetHits() { return mpHits; }

				friend bool operator==(const InvLabel& l1, const InvLabel& l2) {
					return l1.mPath == l2.mPath && (*l1.mpHits) == (*l2.mpHits) && (*l1.mpDocsByRelevance) == (*l2.mpDocsByRelevance) && (*l1.mpDocsById) == (*l2.mpDocsById);
				}

				friend std::ostream& operator<<(std::ostream& os, const InvLabel &l) {
					os << "path: "<<l.mPath << std::endl;
					os << "hits: "<<std::endl << *l.mpHits << std::endl;
					os << "docs by relevance: "<<std::endl << *l.mpDocsByRelevance << std::endl;
					os << "docs by id: "<<std::endl << *l.mpDocsById << std::endl;
					return os;
				}
		};
		// }}}

		//--------------------------------------------------
		// FwdLabelsContainer {{{
		//--------------------------------------------------
		class FwdLabelsContainer
		{
			private:
				friend class boost::serialization::access;
				template<class Archive>
				void serialize(Archive & ar, const unsigned int version)
				{
					ar & mDocId;
					ar & mContainer;
				}
			protected:
				ID mDocId;
				typedef std::map<ID,FwdLabel*> Container;
				Container mContainer;
				bool mCheckOverlapping;
			public:
				FwdLabelsContainer() : mDocId(ID_INVALID) {}
				FwdLabelsContainer(ID docId) : mDocId(docId) {}
				~FwdLabelsContainer();

				typedef Container::iterator iterator;
				iterator begin() { return mContainer.begin(); }
				iterator end() { return mContainer.end(); }
				unsigned int GetSize() { return mContainer.size(); }

				void AddLabel(ID labelId, FwdLabel* pLabel);
				FwdLabel* GetLabel(ID labelId);
				void AddHit(ID labelId, const Hits_OneDocHitList::Record &rec);

				void AddToInvLabelsContainer(InvLabelsContainer* pInvLabelsContainer);

				void SetCheckOverlapping(bool val) { mCheckOverlapping = val; }
				ID GetDocId() { return mDocId; }
		};
		// }}}

		//--------------------------------------------------
		// InvLabelsContainer {{{
		//--------------------------------------------------
		class InvLabelsContainer
		{
			protected:
				typedef std::map<ID,InvLabel*> Container;
				Container mContainer;
				std::string mPath;
				unsigned int mUsedMemoryBytes;
				unsigned int mMaxUsedMemoryBytes;
			public:
				InvLabelsContainer() : mUsedMemoryBytes(0), mMaxUsedMemoryBytes(0) {}
				~InvLabelsContainer();

				InvLabel* AddLabel(ID labelId);
				InvLabel* GetLabel(ID labelId);

				void FlushToDisk();
				void SetMaxUsedMemoryBytes(unsigned int bytes) { mMaxUsedMemoryBytes = bytes; }
				void IncreaseUsedMemoryBytes(unsigned int bytes);
				void SetPath(const std::string &path) { mPath = path; }
				std::string GetInvLabelPath(ID labelId, bool createIfNotExists = false);
		};
		// }}}

		//--------------------------------------------------
		// FwdRecord {{{
		//--------------------------------------------------
		struct FwdRecord
		{
			ID mLabelId;
			Score mScore;
			Time mStartTime;
			Time mEndTime;
			friend std::ostream& operator<< (std::ostream &os, const FwdRecord &r) {
				return os << "id:"<<r.mLabelId << " score:"<<r.mScore<<" start:"<<r.mStartTime<<" end:"<<r.mEndTime;
			}
		};
		// }}}

	//--------------------------------------------------


	protected:
		ID mDocId;
		FwdLabelsContainer* mpFwdLabelsContainer;
		InvLabelsContainer* mpInvLabelsContainer;
		boost::mutex mProcessForwardIndex_mutex;

	public:
		MIndexerStruct(M* pM);

		virtual ~MIndexerStruct() {}

		virtual void InitGlobal();

	   	virtual void Index(ID docId, std::string filename);
		virtual void SendForwardIndexToServer();
		virtual void ReleaseForwardIndex() { DBG("ReleaseForwardIndex()"); delete mpFwdLabelsContainer; }
		virtual void ProcessForwardIndex(const std::string &fwdIndex);
		virtual void Sort();

		virtual void AddFwdIndexRecord(const FwdRecord &rec);
		virtual void AddFwdIndexRecord(ID labelId, Score score, Time startTime, Time endTime);
		virtual void AddFwdLabelToInvertedIndex(ID docId, ID labelId, FwdLabel *pFwdLabel);

		virtual InvLabel* GetOccurrences(ID labelId);

		virtual FwdLabelsContainer* GetFwdLabelsContainer() { return mpFwdLabelsContainer; }
	protected:
		static void Sort_Callback(void* pArg, ID labelId, const std::string &label);
};

}

#endif

