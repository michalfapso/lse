#include "MIndexerStruct.h"
#include "M.h"
#include <sstream>
#include <fstream>
#include <algorithm>
#include "QuickSort.h"

#define MAX_FILES_PER_DIR 1000

using namespace std;
using namespace sem;
//using namespace boost::filesystem;

string MIndexerStruct::DocumentsSortedByRelevance::FILENAME_EXTENSION = ".idxrel";
string MIndexerStruct::DocumentsSortedByRelevance::FILENAME_EXTENSION_NOT_SORTED = ".idxrel_notsorted";

string MIndexerStruct::DocumentsSortedById::FILENAME_EXTENSION = ".idxid";
string MIndexerStruct::DocumentsSortedById::FILENAME_EXTENSION_NOT_SORTED = ".idxid_notsorted";

string MIndexerStruct::Hits::FILENAME_EXTENSION = ".hits";
string MIndexerStruct::Hits::FILENAME_EXTENSION_NOT_SORTED = ".hits_notsorted";

//**************************************************
// MIndexerStruct::DocumentsSortedByRelevance {{{
//**************************************************

void MIndexerStruct::DocumentsSortedByRelevance::AddRecord(const Record& rec) 
{
	mContainer.push_back(rec);
	if (mSorted) mSorted = false;
}

void MIndexerStruct::DocumentsSortedByRelevance::Sort()
{
	sort(mContainer.begin(), mContainer.end(), Record::CmpSort);
	mSorted = true;
}

void MIndexerStruct::DocumentsSortedByRelevance::JoinSameDocId()
{
	int i_chunk_start = 0;
	list<int> list_removed;
	for(unsigned int i=1; i<mContainer.size(); i++)
	{
		if(mContainer[i].GetDocId() == mContainer[i_chunk_start].GetDocId())
		{
			// only the highest relevance remains
			mContainer[i_chunk_start].SetRelevance(max(mContainer[i_chunk_start].GetRelevance(), mContainer[i].GetRelevance()));
			mContainer[i].SetDocId(ID_INVALID); // invalidate the duplicate record
		}
		else
		{
			i_chunk_start = i;
		}
	}
	Container c_joined;
	for(unsigned int i=0; i<mContainer.size(); i++)
	{
		if (mContainer[i].GetDocId() != ID_INVALID)
		{
			c_joined.push_back(mContainer[i]);
		}
	}
	mContainer = c_joined;
}

/*
void MIndexerStruct::DocumentsSortedByRelevance::SetRecordHitListPointer(unsigned int index, Hits_OneDocHitList* pHitList) 
{ 
	assert(index < mContainer.size()); 
	mContainer[index].mpHitList = pHitList; 
}
*/
// }}}

//**************************************************
// MIndexerStruct::DocumentsSortedById {{{
//**************************************************
void MIndexerStruct::DocumentsSortedById::AddRecord(const Record& rec) 
{
	mContainer.push_back(rec);
	if (mSorted) mSorted = false;
}

void MIndexerStruct::DocumentsSortedById::Sort()
{
	sort(mContainer.begin(), mContainer.end(), Record::CmpSort);
	mSorted = true;
}

void MIndexerStruct::DocumentsSortedById::JoinSameDocId()
{
	int i_chunk_start = 0;
	list<int> list_removed;
	for(unsigned int i=1; i<mContainer.size(); i++)
	{
		if(mContainer[i].GetDocId() == mContainer[i_chunk_start].GetDocId())
		{
			// only the highest relevance remains
			mContainer[i_chunk_start].SetHitListStartPos(min(mContainer[i_chunk_start].GetHitListStartPos(), mContainer[i].GetHitListStartPos()));
			mContainer[i_chunk_start].SetHitListRecordsCount(mContainer[i_chunk_start].GetHitListRecordsCount() + mContainer[i].GetHitListRecordsCount());
			mContainer[i].SetDocId(ID_INVALID); // invalidate the duplicate record
		}
		else
		{
			i_chunk_start = i;
		}
	}
	Container c_joined;
	for(unsigned int i=0; i<mContainer.size(); i++)
	{
		if (mContainer[i].GetDocId() != ID_INVALID)
		{
			c_joined.push_back(mContainer[i]);
		}
	}
	mContainer = c_joined;
}
/*
void MIndexerStruct::DocumentsSortedById::SetRecordHitListPointer(unsigned int index, Hits_OneDocHitList* pHitList) 
{ 
	assert(index < mContainer.size()); 
	mContainer[index]->mpHitList = pHitList; 
}
*/
const MIndexerStruct::DocumentsSortedById::Record& MIndexerStruct::DocumentsSortedById::GetRecordByDocId(ID docId)
{
	// binary search
	int low = 0, high = 0, i = 0;

	for ( low=-1, high=GetSize(); high-low > 1;) {
		i = (high + low) / 2;
		
		//DBG("current mContainer["<<i<<"]:"<<mContainer[i]);
			
		if (docId <= mContainer[i].GetDocId()) 
			high = i;
		else
			low = i;

		//DBG(low<<".."<<high<<" -> "<<mContainer[low]<<".."<<mContainer[high]);
	}
	//DBG("current mContainer["<<i<<"]:"<<mContainer[i]);
	if (docId == mContainer[high].GetDocId()) 
		return mContainer[high];
	else {
		CERR("ERROR: DocumentsSortedById::GetRecordByDocId("<<docId<<") not found");
		EXIT();
	}
}
// }}}

//**************************************************
// MIndexerStruct::Hits_OneDocHitList {{{
//**************************************************
void MIndexerStruct::Hits_OneDocHitList::Record::Join(const MIndexerStruct::Hits_OneDocHitList::Record &with)
{
	mStartTime = min(mStartTime, with.mStartTime);
	mEndTime   = max(mEndTime  , with.mEndTime  );
	if (mScore < with.mScore)
	{
		mScore = with.mScore;
		mStartTime = with.mStartTime;
		mEndTime = with.mEndTime;
	}
}

void MIndexerStruct::Hits_OneDocHitList::AddRecord(const Record &rec, bool checkOverlapping)
{
	bool bInsert = true; // will the given record be inserted?

	//DBG("AddRecord(): "<<rec);
	// if there are overlapping hypotheses with the same word in the meeting,
	// we will have only the best candidate (it's confidence and nodeID) in the index, 
	// but it will have time boundaries of the whole overlapping group.
	if (checkOverlapping)
	{
		Hits_OneDocHitList::reverse_iterator iOverlapping = mContainer.rend();
		Hits_OneDocHitList::reverse_iterator i = mContainer.rbegin();
		//DBG("GetSize():"<<mContainer.size());
		//DBG("cond:"<<(mContainer.rbegin() != mContainer.rend()));
		//DBG("mContainer ("<<(int)&mContainer<<"): ");
		/*
		unsigned int j=0;
		for (Hits_OneDocHitList::iterator jj=mContainer.begin(); jj!=mContainer.end(); j++, jj++)
		{
			DBG("mContainer["<<j<<"]("<<(int)&*jj<<"): "<<*jj);
		}
		*/
		while (i != mContainer.rend())
		{
			//DBG("i="<<*i);
			// assume that the meetingID is the same
			if(i->IsOverlappingWith(rec))
			{
				if (iOverlapping == mContainer.rend())
				{
					i->Join(rec);
					iOverlapping = i;
				}
				else
				{
					i->Join(*iOverlapping);
					//DBG("iOverlapping("<<(int)&*iOverlapping<<"):"<<*iOverlapping<<" i("<<(int)&*i<<"):"<<*i);
					/*
					j=0;
					for (Hits_OneDocHitList::iterator jj=mContainer.begin(); jj!=mContainer.end(); j++, jj++)
					{
						DBG("mContainer["<<j<<"]("<<(int)&*jj<<"): "<<*jj);
					}
					*/
					if (iOverlapping != i)
					{
						// the reverse_iterator has to be converted to iterator which is supported by list::erase()
						Hits_OneDocHitList::reverse_iterator i_to_erase = iOverlapping;
						i_to_erase++;

						//DBG("i_to_erase("<<(int)&*i_to_erase.base()<<"):"<<*i_to_erase.base());
						//DBG("i("<<(int)&*i<<"):"<<*i);

						Hits_OneDocHitList::iterator i_erased;
						// list::erase() returns list::iterator which has to be converted back to list::reverse_iterator
						i_erased = mContainer.erase(i_to_erase.base());
						i = Hits_OneDocHitList::reverse_iterator(i_erased);
						/*
						i_erased--;
						DBG("i_erased("<<(int)&*i_erased<<"):"<<*i_erased);
						DBG("i("<<(int)&*i<<"):"<<*i);
						DBG("mContainer.size():"<<mContainer.size());
						j=0;
						for (Hits_OneDocHitList::iterator jj=mContainer.begin(); jj!=mContainer.end(); j++, jj++)
						{
							DBG("mContainer["<<j<<"]("<<(int)&*jj<<"): "<<*jj);
						}
						*/
					}
					//DBG("i("<<(int)&*i<<"):"<<*i);
					iOverlapping = i;
					//DBG("iOverlappingNew:"<<*iOverlapping);
				}
				bInsert = false;
			}
			if (i != mContainer.rend())
			{
				//DBG("mContainer.size():"<<mContainer.size());
				//DBG("i:"<<*i);
				i++;
			}
		}
	}
	//		DBG_FORCE("LOOP FINISHED");
	if (bInsert)
	{
		mContainer.push_back(rec);
	}
}
//}}}

//**************************************************
// MIndexerStruct::Hits {{{
//**************************************************
unsigned int MIndexerStruct::Hits::AddHits (MIndexerStruct::Hits_OneDocHitList* pHits)
{
	mSorted = false;
	unsigned int start_pos = mContainer.size();
	for (Hits_OneDocHitList::iterator i=pHits->begin(); i!=pHits->end(); i++)
	{
		mContainer.push_back(*i);
	}
	return start_pos;
}

bool MIndexerStruct::Hits::SortCmp(int aIdx, int bIdx, CmpSortArg* pArg)
{
	//DBG("SortCmp: doc("<<(*pArg->mpHitsDoc)[aIdx]<<","<<(*pArg->mpHitsDoc)[bIdx]<<") hit("<<(*pArg->mpHits)[aIdx]<<","<<(*pArg->mpHits)[bIdx]<<")");
	//DBG("         res = " << ((*pArg->mpHitsDoc)[aIdx] == (*pArg->mpHitsDoc)[bIdx] ? Record::CmpTime((*pArg->mpHits)[aIdx],(*pArg->mpHits)[bIdx]) : (*pArg->mpHitsDoc)[aIdx] < (*pArg->mpHitsDoc)[bIdx]));
	return (*pArg->mpHitsDoc)[aIdx] == (*pArg->mpHitsDoc)[bIdx] ? Record::CmpTime((*pArg->mpHits)[aIdx],(*pArg->mpHits)[bIdx]) : (*pArg->mpHitsDoc)[aIdx] < (*pArg->mpHitsDoc)[bIdx];
}

void MIndexerStruct::Hits::SortSwap(int aIdx, int bIdx, CmpSortArg* pArg)
{
	ID tmp = (*pArg->mpHitsDoc)[aIdx];
	(*pArg->mpHitsDoc)[aIdx] = (*pArg->mpHitsDoc)[bIdx];
	(*pArg->mpHitsDoc)[bIdx] = tmp;
}

void MIndexerStruct::Hits::Sort(DocumentsSortedById* pDocsById)
{
	// pDocsById has to be sorted befor calling this method
	
	// Create a temporary vector with document ID for each record in the hit list {{{
	// This vector will be sorted together with the hit list so that the indices
	// will remain well mapped.
	vector<ID> hitsDoc;
	hitsDoc.resize(mContainer.size());

	unsigned hit_idx = 0;
	for (unsigned int i=0; i<pDocsById->GetSize(); i++)
	{
		const DocumentsSortedById::Record *p_docid_rec = &(*pDocsById)[i];
		for (hit_idx = p_docid_rec->GetHitListStartPos(); 
			hit_idx < p_docid_rec->GetHitListStartPos() + p_docid_rec->GetHitListRecordsCount(); 
			hit_idx++)
		{
			hitsDoc[hit_idx] = p_docid_rec->GetDocId();
		}
	}
	// }}}
	
	/*
	DBG("unsorted hits: "<<*this);
	for (int i=0; i<hitsDoc.size(); i++)
	{
		DBG("hit doc: "<<hitsDoc[i]);
	}
	*/
//	sort(tmp_container.begin(), tmp_container.end(), SortTmp::Cmp);
	CmpSortArg arg;
	arg.mpHits = &mContainer;
	arg.mpHitsDoc = &hitsDoc;
	QuickSort_CmpIdx(&mContainer[0], 0, (int)mContainer.size()-1, SortCmp, &arg, SortSwap, &arg);

	/*
	DBG("sorted hits: "<<*this);
	for (int i=0; i<hitsDoc.size(); i++)
	{
		DBG("hit doc: "<<hitsDoc[i]);
	}
	*/
	mSorted = true;
}

/**
 * @brief Save all hits of the current label to disk
 *
 * @param &baseFilename
 */
/*
void MIndexerStruct::Hits::Save(const std::string &baseFilename)
{
	ofstream f((baseFilename + FILENAME_EXTENSION).c_str(), ofstream::binary | ofstream::out | ofstream::app);
	boost::archive::text_oarchive oah(f);
	const Hits &h = *this;
	oah << h;
	f.close();
}
*/
// }}}

//**************************************************
// MIndexerStruct::FwdLabel {{{
//**************************************************
void MIndexerStruct::FwdLabel::AddHit(const MIndexerStruct::Hits_OneDocHitList::Record &rec, bool checkOverlapping)
{
	assert(mpHits);
	mpHits->AddRecord(rec, checkOverlapping);
}
// }}}

//**************************************************
// MIndexerStruct::InvLabel {{{
//**************************************************
MIndexerStruct::InvLabel::InvLabel(const std::string &path, InvLabelsContainer* parentContainer) 
	: 
		Label(), 
		mPath(path), 
		mpHits(new Hits), 
		mpDocsByRelevance(new DocumentsSortedByRelevance), 
		mpDocsById(new DocumentsSortedById),
		mpParentInvLabelsContainer(parentContainer)
{
}

bool MIndexerStruct::InvLabel::Exists(const std::string path, bool sorted)
{
	return Hits::Exists(path, sorted) && DocumentsSortedByRelevance::Exists(path, sorted) && DocumentsSortedById::Exists(path, sorted);
}

bool MIndexerStruct::InvLabel::Exists(bool sorted)
{
	return Exists(GetPath(), sorted);
}

void MIndexerStruct::InvLabel::Load(bool sorted)
{
	unsigned int old_hits_size = mpHits->GetSize();

	mpHits->Load(GetPath(), sorted);
	
	mpDocsByRelevance->Load(GetPath(), sorted);

	mpDocsById->Load(GetPath(), sorted, old_hits_size);
}

void MIndexerStruct::InvLabel::Save()
{
	mpHits->Save(GetPath());

	mpDocsByRelevance->Save(GetPath());

	mpDocsById->Save(GetPath());
}

void MIndexerStruct::InvLabel::SaveAppend()
{
	/*
	// if the index does not exist yet, save it normally. Appending to a non-existing file causes an abort.
	if (!mpHits->Exists(GetPath(), false))
	{
		Save();
	}
	else
	*/
	{
		unsigned int hits_start_pos = mpHits->SaveAppend(GetPath());

		// correct mHitListStartPos values to reflect the shift caused by size of hits we are appending to
		mpDocsById->ShiftHitListStart(hits_start_pos);

		mpDocsByRelevance->SaveAppend(GetPath());

		mpDocsById->SaveAppend(GetPath());

		// set the values back
		mpDocsById->ShiftHitListStart(-hits_start_pos);
	}
}

void MIndexerStruct::InvLabel::Sort()
{
	mpDocsByRelevance->Sort();

	mpDocsById->Sort();

	mpHits->Sort(mpDocsById);

	// join documents with the same id in mpDocsByRelevance mpDocsById
	mpDocsByRelevance->JoinSameDocId();
	mpDocsById->JoinSameDocId();
}

/**
 * @brief After the sorting of inverted label and storing it on disk, 
 *        the unsorted label's indices are no longer needed. 
 *        So they should be removed using this method.
 */
void MIndexerStruct::InvLabel::RemoveUnsorted()
{
	mpHits->RemoveUnsorted(GetPath());
	mpDocsByRelevance->RemoveUnsorted(GetPath());
	mpDocsById->RemoveUnsorted(GetPath());
}

void MIndexerStruct::InvLabel::AddDocumentHits(ID docId, Score relevance, MIndexerStruct::Hits_OneDocHitList* pHits)
{
	if (mpParentInvLabelsContainer)
	{
		mpParentInvLabelsContainer->IncreaseUsedMemoryBytes(pHits->GetUsedMemoryBytes() + sizeof(DocumentsSortedByRelevance::Record) + sizeof(DocumentsSortedById::Record));
	}

	unsigned int hit_list_start_pos = mpHits->AddHits(pHits);
	//DBG("hit_list_start_pos="<<hit_list_start_pos);
	mpDocsByRelevance->AddRecord(DocumentsSortedByRelevance::Record(docId, relevance));
	mpDocsById->AddRecord(DocumentsSortedById::Record(docId, hit_list_start_pos, pHits->GetSize()));
}

void MIndexerStruct::InvLabel::AddDocumentHits_NoCache(ID docId, Score relevance, MIndexerStruct::Hits_OneDocHitList* pHits)
{
	unsigned int hit_list_start_pos = mpHits->AddHits(GetPath(), pHits);
	//DBG("hit_list_start_pos="<<hit_list_start_pos);
	mpDocsByRelevance->AddRecord(GetPath(), DocumentsSortedByRelevance::Record(docId, relevance));
	mpDocsById->AddRecord(GetPath(), DocumentsSortedById::Record(docId, hit_list_start_pos, pHits->GetSize()));
}
// }}}

//**************************************************
// MIndexerStruct::FwdLabelsContainer {{{
//**************************************************
MIndexerStruct::FwdLabelsContainer::~FwdLabelsContainer()
{
	DBG("FwdLabelsContainer::~FwdLabelsContainer()");
	for (Container::iterator i=mContainer.begin(); i!=mContainer.end(); i++)
	{
		DBG("~FwdLabelsContainer: "<<(unsigned int)i->second);
		delete i->second;
	}
}

void MIndexerStruct::FwdLabelsContainer::AddLabel(ID labelId, MIndexerStruct::FwdLabel* pLabel)
{
	mContainer.insert(make_pair(labelId, pLabel));
}

MIndexerStruct::FwdLabel* MIndexerStruct::FwdLabelsContainer::GetLabel(ID labelId)
{
	Container::iterator i = mContainer.find(labelId);
	return i == mContainer.end() ? NULL : i->second;
}

void MIndexerStruct::FwdLabelsContainer::AddHit(ID labelId, const Hits_OneDocHitList::Record &rec)
{
	FwdLabel* l = GetLabel(labelId);
	if (!l) {
		l = new FwdLabel;
		AddLabel(labelId, l);
	}
	assert(l);
	l->AddHit(rec, mCheckOverlapping);

//		this->of.write(reinterpret_cast<const char *>(&latRec), sizeof(latRec));

}

/**
 * @brief Add the forward index to the given inverted index
 *
 * @param pInvLabelsContainer Inverted index
 */
void MIndexerStruct::FwdLabelsContainer::AddToInvLabelsContainer(InvLabelsContainer* pInvLabelsContainer)
{
	for (Container::iterator i=mContainer.begin(); i!=mContainer.end(); i++)
	{
		ID fwd_label_id = i->first;
		// get a label from the inverted index
		InvLabel* p_label_inv = pInvLabelsContainer->GetLabel(fwd_label_id);
		if (!p_label_inv)
		{
			p_label_inv = pInvLabelsContainer->AddLabel(fwd_label_id);
		}

		// add hits from forward label to the inverted label
		FwdLabel *p_label_fwd = i->second;
		assert(p_label_fwd);
		assert(p_label_inv);
		p_label_inv->AddDocumentHits(mDocId, p_label_fwd->GetRelevance(), p_label_fwd->GetHits());
	}
}
// }}}

//**************************************************
// MIndexerStruct::FwdRecord {{{
//**************************************************
std::ostream& operator<< (std::ostream &os, const MIndexerStruct::FwdRecord &r) {
	return os << "id:"<<r.mLabelId << " score:"<<r.mScore<<" start:"<<r.mStartTime<<" end:"<<r.mEndTime;
}
// }}}

//**************************************************
// MIndexerStruct::InvLabelsContainer {{{
//**************************************************

MIndexerStruct::InvLabelsContainer::~InvLabelsContainer()
{
	for (Container::iterator i=mContainer.begin(); i!=mContainer.end(); i++)
	{
		delete i->second;
	}
}

void MIndexerStruct::InvLabelsContainer::IncreaseUsedMemoryBytes(unsigned int bytes)
{
	if (mUsedMemoryBytes + bytes > mMaxUsedMemoryBytes)
	{
		FlushToDisk();
		mUsedMemoryBytes = 0;
	}
	mUsedMemoryBytes += bytes;
}

/**
 * @brief Only create an empty label
 *
 * @param labelId Label's ID
 *
 * @return Created inverted label
 */
MIndexerStruct::InvLabel* MIndexerStruct::InvLabelsContainer::AddLabel(ID labelId)
{
	InvLabel* l = new InvLabel(GetInvLabelPath(labelId, true), this);
	mContainer.insert(make_pair(labelId, l));
	// create the label on a disk
//	ofstream f(l->GetPath().c_str(), ofstream::binary | ofstream::out | ofstream::app);
//	f.close();
	return l;
}

/**
 * @brief Get the inverted label with the given label ID.
 *
 * @param labelId Label's ID
 *
 * @return NULL, if the label does not exist. Otherwise the label is returned.
 */
MIndexerStruct::InvLabel* MIndexerStruct::InvLabelsContainer::GetLabel(ID labelId)
{
	std::string path = GetInvLabelPath(labelId);
	DBG("GetLabel(): path="<<path);
	// if it is already loaded
	Container::iterator i = mContainer.find(labelId);
	if (i != mContainer.end())
	{
		DBG("GetLabel(): already loaded");
		return i->second;
	}

	// check whether the label is stored on a disk
	if (!InvLabel::Exists(path, true))
	{
		DBG("GetLabel(): does not exist on disk");
		return NULL;
	}

	// load it
	DBG("GetLabel(): loading");
	InvLabel* l = new InvLabel(path, this);
	l->Load(true);

	// and put it into the container
	mContainer.insert(make_pair(labelId, l));

	return l;
}

/**
 * @brief Get the path where the label is stored on the disk
 *
 * @param labelId Label's ID
 *
 * @return Path
 */
std::string MIndexerStruct::InvLabelsContainer::GetInvLabelPath(ID labelId, bool createIfNotExists)
{
	std::stringstream ss;
	ss << mPath << '/' << (int)(labelId/MAX_FILES_PER_DIR);
	if (createIfNotExists)
	{
		if (!boost::filesystem::exists(boost::filesystem::path(ss.str())))
		{
			DBG("Creating direcotry '"<<boost::filesystem::path(ss.str())<<"'");
			boost::filesystem::create_directory(boost::filesystem::path(ss.str()));
		}
	}
	ss << '/' << labelId;
	return boost::filesystem::path(ss.str()).file_string();
}

void MIndexerStruct::InvLabelsContainer::FlushToDisk()
{
	for (Container::iterator i=mContainer.begin(); i!=mContainer.end(); i++)
	{
		InvLabel* l = i->second;
		l->SaveAppend();
	}
}
// }}}

//**************************************************
// MIndexerStruct {{{
//**************************************************
MIndexerStruct::MIndexerStruct(M* pM) 
	:
	MIndexer(pM), 
	mDocId(ID_INVALID)
{
}

void MIndexerStruct::InitGlobal()
{
	assert(mpM);
	DBG("MIndexerStruct::MIndexerStruct()");
	DBG("mpM->GetAppType():"<<ApplicationType::str(mpM->GetAppType()));
	if (mpM->GetAppType() == ApplicationType::EIndexingServer || 
		mpM->GetAppType() == ApplicationType::ESearchingServer ||
		mpM->GetAppType() == ApplicationType::ESorter)
	{
		mpInvLabelsContainer = new InvLabelsContainer;
		//mpInvLabelsContainer->SetPath( ... ); // this should be called in the derived classes
	}
}

/**
 * @brief Should be called at the beginning of each derived indexer class
 *
 * @param docId Assigned ID of the document
 * @param filename Path to the document
 */
void MIndexerStruct::Index(ID docId, std::string filename)
{
	mDocId = docId;
	mpFwdLabelsContainer = new FwdLabelsContainer(docId);
	mpFwdLabelsContainer->SetCheckOverlapping(true);
	DBG("mpFwdLabelsContainer="<<(int)mpFwdLabelsContainer);
}

/**
 * @brief This method is called at the end of Index() method of each derived indexer class
 */
void MIndexerStruct::SendForwardIndexToServer()
{
	std::stringstream ss;
	boost::archive::text_oarchive oa(ss);
	assert(mpFwdLabelsContainer);
	const FwdLabelsContainer &idx = *mpFwdLabelsContainer;
	oa << idx;
	//DBG("SendForwardIndexToServer(): ss.str()="<<ss.str());
	DBG("SendForwardIndexToServer(): ss.str().length()="<<ss.str().length());
	mpM->GetIndexingClient()->SendForwardIndexToServer(mpM->GetModuleId(), ss.str());
}

void MIndexerStruct::Sort_Callback(void* pArg, ID labelId, const std::string &label)
{
	//DBG("MIndexerStruct::Sort_Callback("<<labelId<<", \""<<label<<"\")");
	MIndexerStruct* idx = reinterpret_cast<MIndexerStruct*>(pArg);

	assert(idx);
	assert(idx->mpInvLabelsContainer);
	InvLabel* l = new InvLabel(idx->mpInvLabelsContainer->GetInvLabelPath(labelId), idx->mpInvLabelsContainer);
	//DBG("GetInvLabelPath():"<<idx->mpInvLabelsContainer->GetInvLabelPath(labelId));
	if (l->Exists(false))
	{
		if (l->Exists(true))
		{
			l->Load(true); // load the sorted index
		}
		l->Load(false); // load the unsorted index
		//DBG("DocsByRelevance: " << endl << *(l->GetDocsByRelevance()));
		//DBG("DocsById: " << endl << *(l->GetDocsById()));
		l->Sort();
		//DBG("DocsByRelevance (sorted): " << endl << *(l->GetDocsByRelevance()));
		//DBG("DocsById (sorted): " << endl << *(l->GetDocsById()));
		l->Save();
		l->RemoveUnsorted();
	}
	delete l;
}

void MIndexerStruct::Sort()
{
	DBG("MIndexerStruct::Sort()");
	MDictionary* dict = mpM->GetDictionary();
	dict->ForEachLabel( Sort_Callback, this );
}

/**
 * @brief Called from the indexing server
 *
 * @param &fwdIndex The forward index to add to the global inverted index
 */
void MIndexerStruct::ProcessForwardIndex(const std::string &fwdIndex)
{
	FwdLabelsContainer fwd;
	// load the data
	std::stringstream ss(fwdIndex, ios_base::in);
	boost::archive::text_iarchive ia(ss);
	ia >> fwd;
	DBG("MIndexerStruct::ProcessForwardIndex() ..."<<fwd.GetSize()<<" labels");

	// add the forward index to the inverted index
	// do not allow parallel inserting of more than 1 forward index to inverted index
	boost::mutex::scoped_lock scoped_lock(mProcessForwardIndex_mutex);

	for (FwdLabelsContainer::iterator i=fwd.begin(); i!=fwd.end(); i++)
	{
		ID fwd_label_id = i->first;
		FwdLabel *p_fwd_label = i->second;

		AddFwdLabelToInvertedIndex(fwd.GetDocId(), fwd_label_id, p_fwd_label);

	}
	//fwd.AddToInvLabelsContainer(mpInvLabelsContainer);
}

void MIndexerStruct::AddFwdLabelToInvertedIndex(ID docId, ID labelId, FwdLabel *pFwdLabel)
{
	DBG("MIndexerStruct::AddFwdLabelToInvertedIndex()");
	// get a label from the inverted index
	assert(mpInvLabelsContainer);
	InvLabel* p_inv_label = mpInvLabelsContainer->GetLabel(labelId);
	if (!p_inv_label)
	{
		p_inv_label = mpInvLabelsContainer->AddLabel(labelId);
	}

	// add hits from forward label to the inverted label
	assert(pFwdLabel);
	assert(p_inv_label);
	p_inv_label->AddDocumentHits(docId, pFwdLabel->GetRelevance(), pFwdLabel->GetHits());
}

void MIndexerStruct::AddFwdIndexRecord(const FwdRecord &rec)
{
	assert(mpFwdLabelsContainer);
	mpFwdLabelsContainer->AddHit(rec.mLabelId, Hits_OneDocHitList::Record(rec.mScore, rec.mStartTime, rec.mEndTime));
}

void MIndexerStruct::AddFwdIndexRecord(ID labelId, Score score, Time startTime, Time endTime)
{
	assert(mpFwdLabelsContainer);
	mpFwdLabelsContainer->AddHit(labelId, Hits_OneDocHitList::Record(score, startTime, endTime));
}

MIndexerStruct::InvLabel* MIndexerStruct::GetOccurrences(ID labelId)
{
	assert(mpInvLabelsContainer);
	InvLabel* l = mpInvLabelsContainer->GetLabel(labelId);
	DBG("GetOccurrences(): size="<<l->GetHits()->GetSize());
	//DBG("GetOccurrences(): mContainer:"<<endl<<*l);
	return l;
}

// }}}

