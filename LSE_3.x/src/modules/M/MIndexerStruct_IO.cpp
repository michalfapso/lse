/*
 * INDEX FILES:
 *
 * extension	[record contents with number of bytes in brackets]
 *
 * .hits 		[score(4) startTime(4) endTime(4)]
 *
 * .idxrel_notsorted	[docId(4) docRelevance(4)]
 * .idxrel				the same as .idxrel_notsorted but with a header(4) at the beginning of the file
 *
 * .idxid_notsorted		[docId(4) hitListStartPos(4) hitListRecordsCount(4)]
 * .idxid				the same as .idxid_notsorted but with a header(4) at the beginning of the file
 *
 */

#include <fstream>
#include <limits.h>
#include <stdint.h>
#include <boost/filesystem/operations.hpp>
#include "MIndexerStruct.h"

using namespace sem;
using namespace std;

// DocumentsSortedByRelevance {{{
void MIndexerStruct::DocumentsSortedByRelevance::Record::Write(std::ostream &os) const
{
	os.write(reinterpret_cast<const char *>(&mDocId), sizeof(mDocId));
	os.write(reinterpret_cast<const char *>(&mRelevance), sizeof(mRelevance));
}

bool MIndexerStruct::DocumentsSortedByRelevance::Record::Read(std::istream &is)
{
	assert(is.good());
	is.read(reinterpret_cast<char *>(&mDocId), sizeof(mDocId));
	is.read(reinterpret_cast<char *>(&mRelevance), sizeof(mRelevance));
	return (!is.eof());
}

bool MIndexerStruct::DocumentsSortedByRelevance::Exists(const std::string &baseFilename, bool sorted)
{
	return boost::filesystem::exists(baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED));
}

void MIndexerStruct::DocumentsSortedByRelevance::AddRecord(const std::string &baseFilename, const Record& rec) 
{
	ofstream f((baseFilename + FILENAME_EXTENSION_NOT_SORTED).c_str(), ofstream::binary | ofstream::out | ofstream::app);
	CHECK_FILE(f, baseFilename + FILENAME_EXTENSION_NOT_SORTED);
	rec.Write(f);
	f.close();
}

void MIndexerStruct::DocumentsSortedByRelevance::Write(std::ostream &os)
{
	if (mSorted)
	{
		WriteVector(mContainer, os);
	}
	else
	{
		for (unsigned int i=0; i<mContainer.size(); i++)
		{
			mContainer[i].Write(os);
		}
	}
}

void MIndexerStruct::DocumentsSortedByRelevance::Save(const std::string &baseFilename)
{
	std::ofstream f((baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ofstream::binary);
	CHECK_FILE(f, (baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)));
	Write(f);
	f.close();
}

void MIndexerStruct::DocumentsSortedByRelevance::SaveAppend(const std::string &baseFilename)
{
	std::ofstream f((baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ofstream::binary | std::ofstream::app);
	CHECK_FILE(f, (baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)));
	Write(f);
	f.close();
}

void MIndexerStruct::DocumentsSortedByRelevance::Read(std::istream &is, bool sorted)
{
	assert(is.good());
	mSorted = sorted;
	if (sorted)
	{
		ReadVector(mContainer, is);
	}
	else
	{
		unsigned int i;
		Record rec;
		for (i = 0; rec.Read(is); i++)
		{
			mContainer.push_back(rec);
		}
	}
}

void MIndexerStruct::DocumentsSortedByRelevance::Load(const std::string &baseFilename, bool sorted)
{
	std::ifstream f((baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ifstream::binary);
	CHECK_FILE(f, baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED));
	Read(f, sorted);
	f.close();
}

void MIndexerStruct::DocumentsSortedByRelevance::RemoveUnsorted(const std::string &baseFilename)
{
	boost::filesystem::remove(baseFilename + FILENAME_EXTENSION_NOT_SORTED);
}

// }}}

// DocumentsSortedById {{{
void MIndexerStruct::DocumentsSortedById::Record::Write(std::ostream &os) const
{
	os.write(reinterpret_cast<const char *>(&mDocId), sizeof(mDocId));
	os.write(reinterpret_cast<const char *>(&mHitListStartPos), sizeof(mHitListStartPos));
	os.write(reinterpret_cast<const char *>(&mHitListRecordsCount), sizeof(mHitListRecordsCount));
}

bool MIndexerStruct::DocumentsSortedById::Record::Read(std::istream &is)
{
	assert(is.good());
	is.read(reinterpret_cast<char *>(&mDocId), sizeof(mDocId));
	is.read(reinterpret_cast<char *>(&mHitListStartPos), sizeof(mHitListStartPos));
	is.read(reinterpret_cast<char *>(&mHitListRecordsCount), sizeof(mHitListRecordsCount));
	return !is.eof();
}

bool MIndexerStruct::DocumentsSortedById::Exists(const std::string &baseFilename, bool sorted)
{
	return boost::filesystem::exists(baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED));
}

void MIndexerStruct::DocumentsSortedById::AddRecord (const std::string &baseFilename, const Record& rec)
{
	ofstream f((baseFilename + FILENAME_EXTENSION_NOT_SORTED).c_str(), ofstream::binary | ofstream::out | ofstream::app);
	if (!f)
	{
		CERR("Error opening file \""<<(baseFilename + FILENAME_EXTENSION_NOT_SORTED)<<"\"");
		EXIT();
	}
	rec.Write(f);
	f.close();
}

void MIndexerStruct::DocumentsSortedById::Write(std::ostream &os)
{
	if (mSorted)
	{
		WriteVector(mContainer, os);
	}
	else
	{
		for (unsigned int i=0; i<mContainer.size(); i++)
		{
			mContainer[i].Write(os);
		}
	}
}

void MIndexerStruct::DocumentsSortedById::Save(const std::string &baseFilename)
{
	std::ofstream f((baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ofstream::binary);
	CHECK_FILE(f, (baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)));
	Write(f);
	f.close();
}

void MIndexerStruct::DocumentsSortedById::SaveAppend(const std::string &baseFilename)
{
	std::ofstream f((baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ofstream::binary | std::ofstream::app);
	CHECK_FILE(f, (baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)));
	Write(f);
	f.close();
}


void MIndexerStruct::DocumentsSortedById::Read(std::istream &is, bool sorted, unsigned int oldHitsSize)
{
	assert(is.good());
	mSorted = sorted;
	unsigned int old_size = mContainer.size();
	if (sorted)
	{
		ReadVector(mContainer, is);
	}
	else
	{
		unsigned int i;
		Record rec;
		for (i = 0; rec.Read(is); i++)
		{
			mContainer.push_back(rec);
		}
	}
	// if the container was not empty, correct the start position of records
	if (old_size > 0)
	{
		for (unsigned int i=old_size; i<mContainer.size(); i++)
		{
			mContainer[i].SetHitListStartPos(oldHitsSize + mContainer[i].GetHitListStartPos());
		}
	}
}

void MIndexerStruct::DocumentsSortedById::Load(const std::string &baseFilename, bool sorted, unsigned int oldHitsSize)
{
	std::ifstream f((baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ifstream::binary);
	CHECK_FILE(f, baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED));
	Read(f, sorted, oldHitsSize);
	f.close();
}

void MIndexerStruct::DocumentsSortedById::RemoveUnsorted(const std::string &baseFilename)
{
	boost::filesystem::remove(baseFilename + FILENAME_EXTENSION_NOT_SORTED);
}
// }}}

// Hits_OneDocHitList {{{
void MIndexerStruct::Hits_OneDocHitList::Record::Write(std::ostream &os)
{
	os.write(reinterpret_cast<const char *>(&mScore), sizeof(mScore));
	os.write(reinterpret_cast<const char *>(&mStartTime), sizeof(mStartTime));
	os.write(reinterpret_cast<const char *>(&mEndTime), sizeof(mEndTime));
}

bool MIndexerStruct::Hits_OneDocHitList::Record::Read(std::istream &is)
{
	assert(is.good());
	is.read(reinterpret_cast<char *>(&mScore), sizeof(mScore));
	is.read(reinterpret_cast<char *>(&mStartTime), sizeof(mStartTime));
	is.read(reinterpret_cast<char *>(&mEndTime), sizeof(mEndTime));
	return !is.eof();
}

void MIndexerStruct::Hits_OneDocHitList::Write(std::ostream &os)
{
	WriteVector(mContainer, os);
}

void MIndexerStruct::Hits_OneDocHitList::Read(std::istream &is)
{
	assert(is.good());
	ReadVector(mContainer, is);
}
// }}}

// Hits {{{
bool MIndexerStruct::Hits::Exists(const std::string &baseFilename, bool sorted)
{
	return boost::filesystem::exists(baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED));
}
/*
unsigned int MIndexerStruct::Hits::UpdateRecordsCount_Add(std::iostream &ios, unsigned int i)
{
	streampos pos_g = ios.tellg();
	streampos pos_p = ios.tellp();
	ios.seekg(0, ios_base::beg);
	ios.seekp(0, ios_base::beg);
	uint32_t s = 0;
	ios.read(reinterpret_cast<char *>(&s), sizeof(s));
	DBG("read size:"<<s);
	s += i;
	ios.write(reinterpret_cast<const char *>(&s), sizeof(s));
	DBG("write size:"<<s);

	ios.seekg(0, ios_base::beg);
	ios.read(reinterpret_cast<char *>(&s), sizeof(s));
	DBG("2nd read size:"<<s);

	ios.seekg(pos_g, ios_base::beg);
	ios.seekp(pos_p, ios_base::beg);
	DBG("UpdateRecordsCount_Add return "<<s);
	return s;
}
*/
unsigned int MIndexerStruct::Hits::AddHits (std::ostream &os, MIndexerStruct::Hits_OneDocHitList* pHits)
{
	mSorted = false;
	assert(os.good());
	/*
	// make space for the header
	if (ios.tellp() == (streampos)0)
	{
		uint32_t header = 0;
		ios.write(reinterpret_cast<const char *>(&header), sizeof(header));
	}
	*/
	// start position without header / record size
	unsigned int start_pos = (int)os.tellp() / sizeof(MIndexerStruct::Hits_OneDocHitList::Record); 
	//DBG("start_pos="<<start_pos<<" tellp()="<<os.tellp());
	// write records
	for (Hits_OneDocHitList::iterator i=pHits->begin(); i!=pHits->end(); i++)
	{
		i->Write(os);
	}
	// update the header
	//start_pos = UpdateRecordsCount_Add(ios, pHits->size()) - pHits->size();
	return start_pos;
}

/**
 * @brief Append hits to disk
 *
 * @param pHits Hits
 */
unsigned int MIndexerStruct::Hits::AddHits (const std::string &baseFilename, MIndexerStruct::Hits_OneDocHitList* pHits)
{
	DBG("Appending to hits: "<<(baseFilename + FILENAME_EXTENSION_NOT_SORTED));
	//DBG("Exists("<<(baseFilename + FILENAME_EXTENSION_NOT_SORTED)<<") = "<<Exists(baseFilename));
	ofstream f((baseFilename + FILENAME_EXTENSION_NOT_SORTED).c_str(), fstream::binary | fstream::out | fstream::app);
	//DBG("filesize="<<boost::filesystem::file_size(baseFilename + FILENAME_EXTENSION_NOT_SORTED));
	if (!f)
	{
		f.close();
		DBG("Creating new file: "<<(baseFilename + FILENAME_EXTENSION_NOT_SORTED));
		f.open((baseFilename + FILENAME_EXTENSION_NOT_SORTED).c_str(), fstream::out);
		if (!f)
		{
			CERR("Error opening file \""<<(baseFilename + FILENAME_EXTENSION_NOT_SORTED)<<"\"");
			EXIT();
		}
		f.close();

		f.open((baseFilename + FILENAME_EXTENSION_NOT_SORTED).c_str(), fstream::binary | fstream::out);
		if (!f)
		{
			CERR("Error opening file \""<<(baseFilename + FILENAME_EXTENSION_NOT_SORTED)<<"\"");
			EXIT();
		}
	}
	uint32_t start_pos = AddHits(f, pHits);
	//DBG("start_pos="<<start_pos);
	f.close();

	return start_pos;
}

void MIndexerStruct::Hits::Read(std::istream &is, bool sorted, unsigned int reserveSize)
{
	if (reserveSize > 0) mContainer.reserve(mContainer.size() + reserveSize);

	assert(is.good());
	mSorted = sorted;
	if (sorted)
	{
		ReadVector(mContainer, is);
	}
	else
	{
		unsigned int i;
		Record rec;
		for (i = 0; rec.Read(is); i++)
		{
			mContainer.push_back(rec);
		}
	}
}

void MIndexerStruct::Hits::Write(std::ostream &os)
{
	if (mSorted)
	{
		WriteVector(mContainer, os);
	}
	else
	{
		for (unsigned int i=0; i<mContainer.size(); i++)
		{
			mContainer[i].Write(os);
		}
	}
}

/**
 * @brief Load all hits of the current label from disk
 *
 * @param &baseFilename
 */
void MIndexerStruct::Hits::Load(const std::string &baseFilename, bool sorted)
{
	ifstream f((baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ifstream::binary);
	CHECK_FILE(f, baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED));
	unsigned int count = (boost::filesystem::file_size(baseFilename + (sorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)) - (sorted ? sizeof(uint32_t) : 0)) / sizeof(Record);
	Read(f, sorted, count);
	f.close();
}

void MIndexerStruct::Hits::Save(const std::string &baseFilename)
{
	std::ofstream f((baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ofstream::binary);
	CHECK_FILE(f, (baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)));
	Write(f);
	f.close();
}

unsigned int MIndexerStruct::Hits::SaveAppend(const std::string &baseFilename)
{
	DBG("Hits::mSorte = "<<mSorted);
	std::ofstream f((baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)).c_str(), std::ofstream::binary | std::ofstream::app);
	CHECK_FILE(f, (baseFilename + (mSorted ? FILENAME_EXTENSION : FILENAME_EXTENSION_NOT_SORTED)));
	unsigned int start_pos = (int)f.tellp() / sizeof(MIndexerStruct::Hits_OneDocHitList::Record); 
	mSorted = false; // because even if the index we will append new records to is sorted, the result will not be sorted anymore
	Write(f);
	f.close();
	return start_pos;
}

void MIndexerStruct::Hits::RemoveUnsorted(const std::string &baseFilename)
{
	boost::filesystem::remove(baseFilename + FILENAME_EXTENSION_NOT_SORTED);
}

// }}}

