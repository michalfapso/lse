#ifndef MQUERY_H
#define MQUERY_H

#include <string>

#include "dbg.h"

namespace sem /* search engine module */
{

class M;

class MQuery
{
	protected:
		M* mpM;
		std::string mQueryStr;
	public:
		MQuery(M* pM) : mpM(pM) {}
		virtual ~MQuery() {}

		void SetQueryStr(const std::string &qStr) { mQueryStr = qStr; }
		std::string GetQueryStr() { return mQueryStr; }
};

}

#endif
