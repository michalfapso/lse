#include <boost/test/included/unit_test_framework.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>

#include "IndexingServer.h" 

using namespace boost::unit_test_framework;
using namespace sem;

//---------------------------------------------------------------------------

void start_indexing_server()
{
	int port = 30000;
	ConfigFile *config = new ConfigFile();
	config->Value("Main", "database", "/home/miso/projects/LSE/branches/LSE_3.x/test/data/idx/sem.sqlite3.db");

	IndexingServer *srv = new IndexingServer(config, port);
	srv->Start();

	delete srv;
}

void test_MIndexerStruct()
{
	BOOST_CHECK_EQUAL( 1, 3 );
} 

test_suite* init_unit_test_suite(int argc, char* argv[])
{
	boost::thread idx_server_thread(&start_indexing_server);

	test_suite* test = BOOST_TEST_SUITE( "M test" );
	test->add( BOOST_TEST_CASE( &test_MIndexerStruct ) );

	return test;
}

