#ifndef MQUERYPARSER_H
#define MQUERYPARSER_H

#include <string>
#include "global.h"

namespace sem
{

class M;

class MQueryParser
{
	protected:
		M* mpM;
	public:
		MQueryParser(M* pM) : mpM(pM) {}
		virtual ~MQueryParser() {}

		virtual void Parse(const std::string &qStr, MQuery* pQuery) { DBG("MQueryParser::Parse()"); }
};

}

#endif

