#ifndef MSEARCHER_H
#define MSEARCHER_H

#include <string>

#include "MQuery.h"
#include "MQueryParser.h"
#include "MDocumentsList.h"
#include "dbg.h"
#include "ResultsList.h"

namespace sem /* search engine module */
{

class M;

class MSearcher
{
	protected:
		M* mpM;
		MQuery* mpMQuery;
		MQueryParser* mpMQueryParser;
		MDocumentsList* mpMDocumentsList;

	public:
		MSearcher(M* pM) : mpM(pM) {}
		virtual ~MSearcher() {}

		virtual void ParseQuery(const std::string &qStr) {}
		virtual void GetNResults(int n, ResultsList * pDocs) {}
//		virtual void SearchGetResults(ResultSet * pResults) = 0;

		virtual MQuery* GetQuery() { return mpMQuery; }
		virtual MQueryParser* GetQueryParser() { return mpMQueryParser; }
		virtual MDocumentsList* GetDocumentsList() { return mpMDocumentsList; }
};

}

#endif

