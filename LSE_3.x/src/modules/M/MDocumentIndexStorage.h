#ifndef MDOCUMENTINDEXSTORAGE_H
#define MDOCUMENTINDEXSTORAGE_H

#include <string>

#include "dbg.h"

namespace sem /* search engine module */
{

class MDocumentIndexStorage
{
	public:
		class Document {
			public:
				ID id;
				std::string path;
				Time length;

				friend bool operator<(const Document& l, const Document& r) {
					return l.id < r.id;
				}
				
				friend std::ostream& operator<<(std::ostream& os, const Document& rec) {
					return os << "id:" << rec.id << "\tlength:" << rec.length << "\tpath:" << rec.path;
				}
		};

		MDocumentIndexStorage() {}
		virtual ~MDocumentIndexStorage() {}

		virtual ID 
			InsertRecord(Document doc);

		virtual const Document&
			GetRecord(ID id);

		virtual const Document&
			GetRecord(const std::string &path);
};

}

#endif

