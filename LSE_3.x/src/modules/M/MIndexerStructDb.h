/**
 * 1. The indexing client calls Index() method of a given module's indexer class.
 *    Forward index is filled and finally serialized and sent to the indexing server.
 * 2. The indexing server receives the forward index and calls the given module's 
 *    ProcessForwardIndex() method.
 * 3. The forward index is deserialized and it's method AddToInvLabelsContainer()
 *    is called. In this method, each label is created in the inverted index and
 *    it's AddDocumentHits() method is called.
 * 4. AddDocumentHits() adds the current label's hits to the inverted index
 *    and adds records in DocumentsSortedByRelevance and DocumentsSortedById indices.
 *    Hits are sorted by time implicitly, but the other two indices are not sorted.
 *    So they have to be sorted explicitely by calling ...
 */
#ifndef MINDEXERSTRUCTDB_H
#define MINDEXERSTRUCTDB_H

#include <string>
#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <memory>

#include <boost/mpl/and.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/archive/archive_exception.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/tuple/tuple.hpp>

#include <boost/thread/mutex.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>

#include <limits.h>
#include <stdint.h>

#include "dbg.h"
#include "global.h"

#include "MIndexerStruct.h"

namespace sem /* search engine module */
{

class MIndexerStructDb : public MIndexerStruct
{
	public:
		MIndexerStruct(M* pM);

		virtual ~MIndexerStruct() {}

		virtual void InitGlobal() {}

		void ProcessForwardIndex(const std::string &fwdIndex);
		virtual void Sort();

		virtual InvLabel* GetOccurrences(ID labelId) = 0;

		virtual AddHitToInvertedIndex(ID docId, ID labelId, Time startTime, Time endTime, Score score) = 0;
	protected:
};

}

#endif

