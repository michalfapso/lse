#ifndef MINDEXERDB_H
#define MINDEXERDB_H

#include <string>

#include "dbg.h"
#include "global.h"

#include "MIndexer.h"

namespace sem /* search engine module */
{

class M;

class MIndexerDb : public MIndexer
{
	protected:
		std::string mTableName;

	public:
		MIndexerDb(M* pM) : MIndexer(pM) {}
		virtual ~MIndexerDb() {}

		virtual void InitGlobal() { DBG("MIndexerDb::InitGlobal()"); }

		virtual void ProcessForwardIndex(const std::string &fwdIndex);

		virtual std::string GetTableName() { return mTableName; }
		virtual void SetTableName(const std::string tableName) { mTableName = tableName; }

		virtual void Sort() { CERR("MIndexerDb::Sort() is not implemented"); };
};

}

#endif

