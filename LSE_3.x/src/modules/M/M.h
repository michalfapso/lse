#ifndef M_H
#define M_H

#include <string>

#include "dbg.h"
#include "../../ApplicationType.h"
#include "global.h"
#include "ConfigFile.h"
#include "ResultsList.h"

#include "IndexingServer_Interface.h"
#include "IndexingClient_Interface.h"
#include "SearchingServer_Interface.h"

#include "MIndexer.h"
#include "MSearcher.h"
#include "MDictionary.h"
#include "MMsgParser.h"

namespace sem /* search engine module */
{

class M
{
	protected:
		std::string mModuleId; ///< Name as it appears in a query
		std::string mModuleName; ///< User-friendly name
		std::string mModuleDescription; ///< Description

		ConfigFile* 	mpConfig;
		M*				mpGlobalParent;
		MIndexer* 		mpIndexer;
		MSearcher* 		mpSearcher;
		MMsgParser* 	mpMsgParser;
		MDictionary*	mpDictionary;
		sqlite3*		mpDb;
		IndexingServer_Interface* 	mpIndexingServer;
		IndexingClient_Interface* 	mpIndexingClient;
		SearchingServer_Interface* 	mpSearchingServer;
		Documents*		mpDocuments;

		ApplicationType::EApplicationType mAppType;

		static int SqliteBusyHandler_Callback(void *pArg, int counter);
	public:

		M(ConfigFile* pConfig, M* pGlobalParent = NULL) : 
			mpConfig(pConfig), 
			mpIndexer(NULL), 
			mpSearcher(NULL), 
			mpMsgParser(NULL), 
			mpDictionary(NULL), 
			mpDb(NULL), 
			mpIndexingServer(NULL),
			mpIndexingClient(NULL),
			mpSearchingServer(NULL),
			mpDocuments(NULL)
		{
			// in case of creating a global instance, the module is it's own parent
			mpGlobalParent = pGlobalParent != NULL ? pGlobalParent : this;
		}

		/**
		 * @brief Create a new instance of the module dependent on the given
		 *        instance in the global memory space. As a searching server
		 *        starts, a "global" instance of each module is created and
		 *        all the data that can be preloaded are stored there. Then
		 *        as a new query arrives to be processed, a new thread is 
		 *        created and new module instances using this method are created.
		 *        These instances have access to the corresponding global instance 
		 *        to save the memory and the query processing time.
		 *        Each particular module should create an instance of it's own class type.
		 *
		 * @return New "child" instance of the module
		 */
		virtual M* CreateChildInstance();

		virtual ~M() 
		{
			if(mpDocuments) { delete mpDocuments; mpDocuments=NULL; }
			sqlite3_close(mpDb);
		}

		/**
		 * @brief Init global data
		 *
		 * @param appType Application type which uses the module
		 *        (indexing server, indexing client, search server
		 *        and possibly other types)
		 */
		virtual void InitGlobal(ApplicationType::EApplicationType appType);

		// INDEXING METHODS {{{
		/**
		 * @brief Index the given file with assigned document id.
		 *
		 * @param docId Document id
		 * @param filename File to index (in case of speech 
		 *        document it can be a sound file, in case of
		 *        text document it can be a text file)
		 */
		virtual void Index(ID docId, const std::string &filename);

		/**
		 * @brief Indexing server uses this method for processing 
		 *        "insert record" messages sent by indexing clients. 
		 *
		 * @param msg Message (example: "add_to_dictionary blahblah")
		 *
		 * @return ID of the inserted record.
		 */
		virtual ID ProcessAssignIdMsg(const std::string &msg);

		/**
		 * @brief Indexing server uses this method for processing 
		 *        the forward index sent by indexing client
		 *        to store it in the inverted index.
		 *
		 * @param fwdIndex Forward index
		 */
		virtual void ProcessForwardIndex(const std::string &fwdIndex);

		/**
		 * @brief Sort the index to make it searchable and store it.
		 */
		virtual void SortIndex();
		// }}} INDEXING METHODS

		// SEARCHING METHODS {{{
		/**
		 * @brief Parse the given search string
		 *
		 * @param qStr Search string (query)
		 */
		virtual void ParseQuery(const std::string &qStr);

		/**
		 * @brief Get an estimated entropy for the given query.
		 *        Should be called after ParseQuery() so that the
		 *        module can estimate the entropy based on the query.
		 *        Entropy value is used for building the searching net
		 *        to put the most discriminative modules at the beginning
		 *        of the search process.
		 *
		 * @return Entropy, values from 0.0 to 1.0 (1.0 means the highest possible discriminability)
		 */
		virtual float GetEntropy();

		/**
		 * @brief Get N documents matching the query and add them
		 *        to the documents list.
		 *
		 * @param pDocs Returned documents
		 */
		virtual void GetNResults(int n, ResultsList* pRes);
		
//		virtual void CheckResults();
		// }}} SEARCHING METHODS

		// GET and SET methods {{{
		std::string GetModuleId() { return mModuleId; }
		std::string GetModuleName() { return mModuleName; }
		std::string GetModuleDescription() { return mModuleDescription; }

		virtual ConfigFile* GetConfig() { return mpConfig; }
		virtual M* GetGlobalInstance() { return mpGlobalParent; }
		virtual MIndexer* GetIndexer() { return mpIndexer; }
		virtual MSearcher* GetSearcher() { return mpSearcher; }
		virtual MDictionary* GetDictionary() { DBG("M::GetDictionary()"); return mpDictionary; }
		virtual sqlite3* GetDb() { return mpDb; }
		virtual ApplicationType::EApplicationType GetAppType() { return mAppType; }
		virtual Documents* GetDocuments() { return mpDocuments; }

		void SetIndexingServer(IndexingServer_Interface* pIndexingServer) { mpIndexingServer = pIndexingServer; }
		IndexingServer_Interface* GetIndexingServer() { return mpIndexingServer; }

		void SetIndexingClient(IndexingClient_Interface* pIndexingClient) { mpIndexingClient = pIndexingClient; }
		IndexingClient_Interface* GetIndexingClient() { return mpIndexingClient; }

		void SetSearchingServer(SearchingServer_Interface* pSearchingServer) { mpSearchingServer = pSearchingServer; }
		SearchingServer_Interface* GetSearchingServer() { return mpSearchingServer; }

		bool IsGlobal() { return mpGlobalParent == this; }
		// }}}

};

}

#endif

