#include "M.h"

using namespace std;
using namespace sem;

void M::InitGlobal(ApplicationType::EApplicationType appType)
{
	DBG("M::Init("<<ApplicationType::str(appType)<<")");
	mAppType = appType;
	// open the database
	string database_filename = (string)mpConfig->Value("Main", "database", "");
	if (database_filename == "")
	{
		DBG("Warning: no database filename specified");
		mpDb = NULL;
	}
	else
	{
		DBG("sqlite3_open(\""<<database_filename<<"\")");
		int rc = sqlite3_open(database_filename.c_str(), &mpDb);
		if( rc ){
			fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(mpDb));
			sqlite3_close(mpDb);
			EXIT();
		}
		DBG("sqlite3_busy_handler("<<(int)mpDb<<")");
		sqlite3_busy_handler(mpDb, M::SqliteBusyHandler_Callback, this);
	}
	mpDocuments = new Documents(mpDb);
	DBG("M::mpDocuments="<<(int)mpDocuments);
}

M* M::CreateChildInstance()
{
	assert(!IsGlobal());
	M* m = new M(mpConfig, this);
	return m;
}

void M::Index(ID docId, const std::string &filename)
{
	DBG("M::Index("<<docId<<", "<<filename<<")");
}

ID M::ProcessAssignIdMsg(const std::string &msg)
{
	assert(mpMsgParser);
	return mpMsgParser->ProcessAssignIdMsg(msg);
}

void M::ProcessForwardIndex(const std::string &fwdIndex)
{
	assert(mpIndexer);
	mpIndexer->ProcessForwardIndex(fwdIndex);
}

void M::SortIndex()
{
	assert(mpIndexer);
	mpIndexer->Sort();
}

void M::ParseQuery(const std::string &qStr)
{
	DBG("M::ParseQuery("<<qStr<<")");
}

float M::GetEntropy()
{
	DBG("M::GetEntropy()");
	return 0.0;
}

void M::GetNResults(int n, ResultsList* pRes)
{
	DBG("M::GetNResults()");
}

/*
void M::CheckResults()
{
	DBG("M::CheckResults()");
}
*/

int M::SqliteBusyHandler_Callback(void *pArg, int counter)
{
//	IndexingServer* p_srv = reinterpret_cast<IndexingServer*>(pArg);
	CERR("M::SqliteBusyHandler_Callback("<<counter<<")");
	return 1;
}

