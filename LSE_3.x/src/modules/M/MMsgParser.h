#ifndef MMSGPARSER_H
#define MMSGPARSER_H

#include <string>
#include "global.h"

namespace sem
{

class M;

class MMsgParser
{
		M* mpM;
	public:
		MMsgParser(M* pM) : mpM(pM) {}
		virtual ~MMsgParser() {}

		ID ProcessAssignIdMsg(const std::string &msg);
};

}

#endif

