#include "MIndexerStructDb.h"
#include "M.h"
#include <sstream>
#include <fstream>
#include <algorithm>
#include "QuickSort.h"

using namespace std;
using namespace sem;

//**************************************************
// MIndexerStructDb {{{
//**************************************************
MIndexerStructDb::MIndexerStructDb(M* pM) 
	:
	MIndexerStructDb(pM)
{
}

void MIndexerStructDb::Sort()
{
	CERR("WARNING: MIndexerStructDb::Sort()...not implemented");
}

/**
 * @brief Called from the indexing server
 *
 * @param &fwdIndex The forward index to add to the global inverted index
 */
void MIndexerStructDb::ProcessForwardIndex(const std::string &fwdIndex)
{
	FwdLabelsContainer fwd;
	// load the data
	std::stringstream ss(fwdIndex, ios_base::in);
	boost::archive::text_iarchive ia(ss);
	ia >> fwd;

	ID doc_id = fwd.GetDocId();

	// add the forward index to the inverted index
	// do not allow parallel inserting of more than 1 forward index to inverted index
	boost::mutex::scoped_lock scoped_lock(mProcessForwardIndex_mutex);
	for (FwdLabelsContainer::iterator i_label = fwd.begin(); i_label!=fwd.end(); i_label++)
	{
		ID label_id = i_label->first;
		FwdLabel * fwd_label = i_label->second;
		Hits_OneDocHitList * fwd_hits = fwd_label->GetHits();
		for (Hits_OneDocHitList::iterator i_hit = fwd_hits->begin(); i_hit!=fwd_hits->end(); i_hit++)
		{
			const Hits_OneDocHitList::Record & rec = *i_hit;
			AddHitToInvertedIndex(doc_id, label_id, i_hit->GetStartTime(), i_hit->GetEndTime(), i_hit->GetScore());
		}
	}
}


/*
MIndexerStructDb::InvLabel* MIndexerStructDb::GetOccurrences(ID labelId)
{
	assert(mpInvLabelsContainer);
	InvLabel* l = mpInvLabelsContainer->GetLabel(labelId);
	DBG("GetOccurrences(): size="<<l->GetHits()->GetSize());
	//DBG("GetOccurrences(): mContainer:"<<endl<<*l);
	return l;
}
*/	

// }}}

