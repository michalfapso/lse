#include "ModulesContainer.h"

using namespace std;
using namespace sem;

ModulesContainer::ModulesContainer(ConfigFile* pConfig)
	: mpConfig(pConfig),
	mpIndexingServer(NULL),
	mpIndexingClient(NULL)
{
	DBG("ModulesContainer::ModulesContainer()");

	M* m;

	m = new MKws(mpConfig);
	mContainer.insert(make_pair(m->GetModuleId(), m));

	m = new MTxt(mpConfig);
	mContainer.insert(make_pair(m->GetModuleId(), m));

	//DBG("ModulesContainer::ModulesContainer() ... done");
}

ModulesContainer::~ModulesContainer() 
{ 
	DBG("ModulesContainer::~ModulesContainer()"); 
	for (Container::iterator i = mContainer.begin(); i != mContainer.end(); i++)
	{
		M* m = i->second;
		delete m;
	}
}

void ModulesContainer::InitModules(ApplicationType::EApplicationType appType)
{
	DBG("ModulesContainer::InitModules("<<ApplicationType::str(appType)<<")");
	for (Container::iterator i = mContainer.begin(); i != mContainer.end(); i++)
	{
		M* m = i->second;
		m->InitGlobal(appType);
	}
}

M* ModulesContainer::GetModule(const std::string &moduleId)
{
	Container::iterator i = mContainer.find(moduleId);
	return i == mContainer.end() ? NULL : i->second;
}

void ModulesContainer::SetIndexingClient(IndexingClient_Interface* pIndexingClient)
{
	mpIndexingClient = pIndexingClient;
	for (Container::iterator i = mContainer.begin(); i != mContainer.end(); i++)
	{
		M* m = i->second;
		m->SetIndexingClient(pIndexingClient);
	}
}

void ModulesContainer::SetIndexingServer(IndexingServer_Interface* pIndexingServer)
{
	mpIndexingServer = pIndexingServer;
	for (Container::iterator i = mContainer.begin(); i != mContainer.end(); i++)
	{
		M* m = i->second;
		m->SetIndexingServer(pIndexingServer);
	}
}

void ModulesContainer::SetSearchingServer(SearchingServer_Interface* pSearchingServer)
{
	mpSearchingServer = pSearchingServer;
	for (Container::iterator i = mContainer.begin(); i != mContainer.end(); i++)
	{
		M* m = i->second;
		m->SetSearchingServer(pSearchingServer);
	}
}

void ModulesContainer::ForEachModule(ForEachModule_Callback_T pCallback, void* pArg)
{
	for (Container::iterator i = mContainer.begin(); i != mContainer.end(); i++)
	{
		(*pCallback)(pArg, i->first, i->second);
	}
}
