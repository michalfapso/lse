#ifndef MODULESCONTAINER_H
#define MODULESCONTAINER_H

#include <map>
#include <string>
#include "dbg.h"
#include "../ApplicationType.h"
#include "ConfigFile.h"
#include "M/M.h"
#include "MKws/MKws.h"
#include "MTxt/MTxt.h"
//#include "M_Global.h"

class ModulesContainer
{
	public:
		typedef void (*ForEachModule_Callback_T)(void *pArg, const std::string &moduleId, sem::M* pM);

	private:
		typedef std::map<std::string, sem::M*> Container;
		Container mContainer;

		ConfigFile* mpConfig;
		IndexingServer_Interface* mpIndexingServer;
		IndexingClient_Interface* mpIndexingClient;
		SearchingServer_Interface* mpSearchingServer;

	public:
		ModulesContainer(ConfigFile* pConfig);
		~ModulesContainer();
		void InitModules(ApplicationType::EApplicationType appType);
		sem::M* GetModule(const std::string &moduleId);

		void ForEachModule(ForEachModule_Callback_T pCallback, void* pArg);

		void SetIndexingServer(IndexingServer_Interface* pIndexingServer);
		void SetIndexingClient(IndexingClient_Interface* pIndexingClient);
		void SetSearchingServer(SearchingServer_Interface* pSearchingServer);
};

#endif
