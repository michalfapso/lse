#ifndef INDEXINGCLIENT_INTERFACE_H
#define INDEXINGCLIENT_INTERFACE_H

class IndexingClient_Interface
{
	public:
		virtual ~IndexingClient_Interface() {}
		virtual ID SendAssignIdMsg(const std::string &moduleId, const std::string &msg) = 0;
		virtual void SendForwardIndexToServer(const std::string &moduleId, const std::string &fwdIndex) = 0;
};

#endif
