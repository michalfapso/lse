#ifndef MKWSDICTIONARY_H
#define MKWSDICTIONARY_H

#include <string>
#include <vector>

#include "../M/MDictionary.h"

namespace sem /* search engine module */
{

class MKws;

class MKwsDictionary : public MDictionary
{
	protected:
		MKws* mpMKws;
		std::map<ID,bool> mMetaWordsList;

	public:
		MKwsDictionary(MKws* pMKws) : MDictionary((M*)pMKws), mpMKws(pMKws) 
		{ 
		}

		virtual ~MKwsDictionary() {}

		virtual void InitGlobal();

		virtual bool IsMetaWord(ID wordId);

		virtual bool CanAddWiPen(const ID wordId);

		virtual bool CanAddToFwdIndex(const ID wordId);

		virtual void SetMetaWordsList(std::vector<std::string> wordList);
		virtual void SetMetaWordsList(std::string filenameWordsList);
/*
		virtual bool 
			AddUnit(const std::string &label, UnitId* assignedId = NULL);

		virtual ID 
			GetUnitId(const std::string &label);

		virtual std::string 
			GetUnitLabel(const UnitId id);
*/
};

}

#endif
