#ifndef MKWSDICTIONARYMEM_H
#define MKWSDICTIONARYMEM_H

#include <string>
#include <vector>

#include "MKwsDictionary.h"

namespace sem /* search engine module */
{

class MKwsDictionary;

class MKwsDictionaryMem : public MKwsDictionary
{
	protected:
		ID mLastUnitId;

	public:
		MKwsDictionaryMem(MKws* pMKws) : MKwsDictionary(pMKws), mLastUnitId(0)
		{ 
		}

		virtual ~MKwsDictionaryMem() {}

		virtual void InitGlobal() {}

		bool UseLocal() { return true; }

		ID AddUnit(const std::string &label) 
		{ 
			DBG("MKwsDictionaryMem::AddUnit('"<<label<<"')");
			ID unit_id = ++mLastUnitId;
			AddUnitLocal(unit_id, label); 
			return unit_id;
		}
/*
		virtual bool 
			AddUnit(const std::string &label, UnitId* assignedId = NULL);

		virtual ID 
			GetUnitId(const std::string &label);

		virtual std::string 
			GetUnitLabel(const UnitId id);
*/
};

}

#endif
