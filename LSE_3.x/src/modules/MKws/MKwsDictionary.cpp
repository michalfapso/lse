#include <fstream>
#include "MKwsDictionary.h"

using namespace std;
using namespace sem;

void MKwsDictionary::InitGlobal()
{
	MDictionary::InitGlobal();
}

bool MKwsDictionary::IsMetaWord(ID wordId)
{
	assert(wordId != ID_INVALID);
	if (mMetaWordsList.size() > 0)
	{
		map<ID,bool>::iterator iter = mMetaWordsList.find(wordId);
		return iter != mMetaWordsList.end();
	}
	else
	{
		return (wordId != ID_INVALID) && ((wordId == GetUnitId("!NULL") || wordId == GetUnitId("<s>") || wordId == GetUnitId("</s>")));
	}
}

bool MKwsDictionary::CanAddWiPen(const ID wordId)
{
	return (wordId != ID_INVALID) && wordId != GetUnitId("!NULL");
}

bool MKwsDictionary::CanAddToFwdIndex(const ID wordId)
{
	return !IsMetaWord(wordId);
//	return (wordId != ID_INVALID) && wordId != GetUnitId("!NULL") && wordId != GetUnitId("<s>") && wordId != GetUnitId("</s>");
}

void MKwsDictionary::SetMetaWordsList(std::vector<std::string> wordList)
{
	for (std::vector<std::string>::const_iterator i=wordList.begin(); i!=wordList.end(); i++)
	{
		ID id = GetUnitId(*i);
		if (id == ID_INVALID)
		{
			id = AddUnit(*i);
			DBG("AddUnit("<<*i<<") = "<<id);
			if (id == ID_INVALID)
			{
				CERR("ERROR: There is an unknown word in the meta words list: '"<<*i<<"'");
				EXIT();
			}
		}
		mMetaWordsList[id] = true;
	}
}

void MKwsDictionary::SetMetaWordsList(std::string filenameWordsList)
{
	ifstream f(filenameWordsList.c_str(), ifstream::in);
	if (!f.good())
	{
		CERR("ERROR: unable to open file '"<<filenameWordsList<<"'");
		EXIT();
	}
	vector<string> vStr;
	string str;
	while(f.good())
	{
		f >> str;
		vStr.push_back(str);
	}
	SetMetaWordsList(vStr);
}

