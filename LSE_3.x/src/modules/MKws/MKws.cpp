#include "MKws.h"

using namespace std;
using namespace sem;

MKws::MKws(ConfigFile* pConfig, M* pGlobalParent) : M(pConfig, pGlobalParent)
{
	DBG("MKws::MKws()");
	mModuleId = "KWS";
	mModuleName = "Speech search";
	mModuleDescription = "Search in output of automatic speech recognizer";

	mpIndexerLvcsr = NULL;
	mpDictionaryLvcsr = NULL;
	if ((bool)GetConfig()->Value("MKws", "work_with_lvcsr", false))
	{
		DBG("MKws::MKws() work_with_lvcsr");
		mpDictionaryLvcsr = new MKwsDictionary(this);
		mpDictionaryLvcsr->SetTableName("MKwsDictionaryLvcsr");
		string storage_type = (string)GetConfig()->Value("MKws_indexer", "storage", "disk_struct");
		if (storage_type == "disk_struct")
		{
			mpIndexerLvcsr = new MKwsIndexer(this, GetDictionaryLvcsr());
		}
/*		else if (storage_type == "mysql")
		{
			mpIndexerLvcsr = new MKwsIndexerDbMysql(this, GetDictionaryLvcsr());
		}
*/		else if (storage_type == "sqlite")
		{
			mpIndexerLvcsr = new MKwsIndexerDbSqlite(this, GetDictionaryLvcsr());
		}
	}

	mpIndexerPhn = NULL;
	mpDictionaryPhn = NULL;
	if ((bool)GetConfig()->Value("MKws", "work_with_phn", false))
	{
		DBG("MKws::MKws() work_with_phn");
		mpDictionaryPhn = new MKwsDictionary(this);
		mpDictionaryPhn->SetTableName("MKwsDictionaryPhn");
//		mpIndexerPhn = new MKwsIndexerDbSqlite(this, mpDictionaryPhn);
		mpIndexerPhn = new MKwsIndexer(this, mpDictionaryPhn);
	}

	mpDictionary = NULL;
	mpSearcher = new MKwsSearcher(this);
	mpMsgParser = new MMsgParser(this);
}

M* MKws::CreateChildInstance()
{
	// allow to create child instances only of global modules
	assert(IsGlobal());
	M* m = new MKws(mpConfig, this);
	return m;
}

MKws::~MKws()
{
	delete mpIndexer;
	delete mpIndexerLvcsr;
	delete mpIndexerPhn;
	delete mpSearcher;
	delete mpDictionaryLvcsr;
	delete mpDictionaryPhn;
	delete mpMsgParser;
}

void MKws::InitGlobal(ApplicationType::EApplicationType appType)
{
	//DBG("MKws::InitGlobal("<<ApplicationType::str(appType)<<")");
	M::InitGlobal(appType);
	
	if (appType != ApplicationType::EIndexingClient)
	{
		if (mpIndexerLvcsr && (string)GetConfig()->Value("MKws_indexer", "idx_dir_lvcsr", "") == "") 
		{
			CERR("ERROR: config value for 'idx_dir_lvcsr' has to be set");
			EXIT();
		}
		if (mpIndexerPhn && (string)GetConfig()->Value("MKws_indexer", "idx_dir_phn", "") == "") 
		{
			CERR("ERROR: config value for 'idx_dir_phn' has to be set");
			EXIT();
		}
	}

	if (mpIndexerLvcsr)	mpIndexerLvcsr->InitGlobal((string)GetConfig()->Value("MKws_indexer", "idx_dir_lvcsr", ""));
	if (mpIndexerPhn) mpIndexerPhn->InitGlobal((string)GetConfig()->Value("MKws_indexer", "idx_dir_phn", ""));
	
	if (GetDictionaryLvcsr()) GetDictionaryLvcsr()->InitGlobal();
	if (GetDictionaryPhn()) GetDictionaryPhn()->InitGlobal();

	string mwl_filename = (string)GetConfig()->Value("MKws_indexer", "meta_words_list", "");
	if (mwl_filename != "")
	{
		DBG("SetMetaWordsList("<<mwl_filename<<")");
		if (GetDictionaryLvcsr()) GetDictionaryLvcsr()->SetMetaWordsList(mwl_filename);
		if (GetDictionaryPhn()) GetDictionaryPhn()->SetMetaWordsList(mwl_filename);
	}
}

void MKws::Index(ID docId, const std::string &filename)
{
	DBG("MKws::Index("<<docId<<", "<<filename<<")");
	assert((mpIndexerLvcsr == NULL) ^ (mpIndexerPhn == NULL)); // exactly one of them has to be active
	if(mpIndexerLvcsr) 
	{
		mpIndexerLvcsr->Index(docId, filename);
		mpIndexerLvcsr->SendForwardIndexToServer();
		mpIndexerLvcsr->ReleaseForwardIndex();
	}
	if(mpIndexerPhn) 
	{
		mpIndexerPhn->Index(docId, filename);
		mpIndexerPhn->SendForwardIndexToServer();
		mpIndexerPhn->ReleaseForwardIndex();
	}
}

void MKws::ParseQuery(const std::string &qStr)
{
	DBG("MKws::ParseQuery("<<qStr<<")");
	mpSearcher->ParseQuery(qStr);
}

void MKws::GetNResults(int n, ResultsList* pRes)
{
	DBG("MKws::GetNResults()");
	assert(GetSearcher());
	GetSearcher()->GetNResults(n, pRes);
}

void MKws::SortIndex()
{
	assert(mpIndexerLvcsr || mpIndexerPhn);
	if (mpIndexerLvcsr)	mpIndexerLvcsr->Sort();
	if (mpIndexerPhn)	mpIndexerPhn->Sort();
}

void MKws::ProcessForwardIndex(const std::string &fwdIndex)
{
	assert(mpIndexerLvcsr || mpIndexerPhn);
	if (mpIndexerLvcsr)	mpIndexerLvcsr->ProcessForwardIndex(fwdIndex);
	if (mpIndexerPhn)	mpIndexerPhn->ProcessForwardIndex(fwdIndex);
}
