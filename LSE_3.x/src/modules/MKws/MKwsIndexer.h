#ifndef MKWSINDEXER_H
#define MKWSINDEXER_H

#include "../M/MIndexerStruct.h"

#include "liblse/latbinfile.h"
#include "liblse/latmlffile.h"
#include "liblse/lat.h"
#include "liblse/lattypes.h"
#include "liblse/strindexer.h"
#include "liblse/latindexer.h"
#include "liblse/indexer.h"
#include "liblse/ngram.h"
#include "search/querykwdlist.h"

namespace sem
{

class MKws;

class MKwsIndexer : public MIndexerStruct
{
	protected:
		MKws* mpMKws;
		MKwsDictionary* mpDictionary;

		static const std::string mExt_dot;
		static const std::string mExt_bin;
		static const std::string mExt_copy;
		static const std::string mExt_htk;
		static const std::string mExt_alphabetavectors;

		Timer mDbgTimer;

	public:
		MKwsIndexer(MKws* pMKws, MKwsDictionary* pDictionary) : MIndexerStruct((M*)pMKws), mpMKws(pMKws), mpDictionary(pDictionary) { DBG("MKwsIndexer::MKwsIndexer()"); }
		virtual ~MKwsIndexer() {}

		virtual void InitGlobal(const std::string &invIdxPath);

	   	virtual void Index(ID docId, const std::string &filename);

		virtual void Sort();

		virtual void Lattice2FwdIndex(lse::Lattice* pLat);
		virtual void Lattice2NgramsFwdIndex(lse::Lattice* pLat, int n);

		virtual MKws* GetModule() { return mpMKws; }

		virtual void GetOccurrences(ID labelId, lse::QueryKwd::InvIdxRecords* results, std::vector<ID> &docList);

		virtual void OutputLatticeStatistics(std::ostream &os, const lse::Lattice &lat);
		virtual void OnLatticeLoaded(lse::Lattice * pLat) {}
};

}

#endif
