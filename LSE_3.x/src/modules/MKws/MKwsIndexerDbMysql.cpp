#include "MKwsIndexerDbMysql.h"
#include "MKws.h"

using namespace sem;
using namespace std;
using namespace mysqlpp;

void MKwsIndexerDbMysql::AddFwdLabelToInvertedIndex(ID docId, ID labelId, FwdLabel *pFwdLabel)
{
	//DBG("MKwsIndexerDbMysql::AddFwdLabelToInvertedIndex()");

	Hits_OneDocHitList * fwd_hits = pFwdLabel->GetHits();
	//CERR("fwd_hits->GetSize():"<<fwd_hits->GetSize());
	Query query = mpCon->query();
	for (Hits_OneDocHitList::iterator i = fwd_hits->begin(); i!=fwd_hits->end(); i++)
	{
		try
		{
			query 
				<< "INSERT INTO " << mTableName
				<< " (id_doc, id_label, start_time, end_time, confidence)"
				<< " VALUES("
				<< "\""<<docId<<"\","
				<< "\""<<labelId<<"\","
				<< "\""<<i->GetStartTime()<<"\","
				<< "\""<<i->GetEndTime()<<"\","
				<< "\""<<i->GetScore()<<"\""
				<< ");";

			SimpleResult res = query.execute();
			query.reset();
		}
		catch (const Exception& er) {
			CERR("ERROR: " << er.what());
			EXIT();
		}
	}
}

