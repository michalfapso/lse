#ifndef MKWSSEARCHER_H
#define MKWSSEARCHER_H

#include "../M/MSearcher.h"
#include "g2p.h"
#include "querykwdlist.h"
#include "results.h"

namespace sem
{

class MKws;

class MKwsSearcher : public MSearcher
{
	protected:
		MKws* mpMKws;
		lse::QueryKwdList* mpQueryKwdList;
		lse::G2P* mpG2P;

		void SearchKeyword(
				lse::QueryKwdList 	*queryKwdList, 
				lse::Results 		*pResults);

		void InvertedIndexLookup_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void PrintMatchedKeywords(lse::QueryKwdList* pQueryKwdList);
		void InvertedIndexLookupResultsClustering_Phn(lse::QueryKwdList* pQueryKwdList);
		void PrintInvertedIndexLookupResultsClusters_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void FillInvIdxRecordsClusters_Phn(lse::QueryKwdList* pQueryKwdList);
		void SortInvIdxRecordsClusters_Phn(lse::QueryKwdList* pQueryKwdList);
		void PrintInvIdxRecordsClusters_Phn(lse::QueryKwdList* pQueryKwdList);
		void CombineInvertedIndexLookupResults_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void InvertedIndexLookupResultsClustering_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void FillInvIdxRecordsClusters_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void SortInvIdxRecordsClusters_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void PrintInvIdxRecordsClusters_LvcsrPhn(lse::QueryKwdList* pQueryKwdList);
		void InvIdxRecordsClusters2Results(lse::QueryKwdList *pQueryKwdList, lse::Results *pResults);

	public:
		MKwsSearcher(MKws* pMKws);
		virtual ~MKwsSearcher() {}

		virtual void ParseQuery(const std::string &qStr);
		virtual void GetNResults(int n, ResultsList * pRes);
};

}

#endif
