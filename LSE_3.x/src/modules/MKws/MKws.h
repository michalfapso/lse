#ifndef MKWS_H
#define MKWS_H

#include "ConfigFile.h"

#include "../M/M.h"
#include "MKwsIndexer.h"
#include "MKwsIndexerDbSqlite.h"
//#include "MKwsIndexerDbMysql.h"
#include "MKwsSearcher.h"
#include "MKwsDictionary.h"

namespace sem 
{

class MKws : public M
{
	protected:
		MKwsDictionary*	mpDictionaryLvcsr;
		MKwsDictionary*	mpDictionaryPhn;
		MKwsIndexer*	mpIndexerLvcsr;
		MKwsIndexer*	mpIndexerPhn;
		std::string		mQueryStr;
	public:
		MKws(ConfigFile* pConfig, M* pGlobalParent = NULL);
		virtual M* CreateChildInstance();
		virtual ~MKws();

		virtual void InitGlobal(ApplicationType::EApplicationType appType);
		virtual void Index(ID docId, const std::string &filename);
		virtual void ProcessForwardIndex(const std::string &fwdIndex);
		virtual void SortIndex();
		virtual void ParseQuery(const std::string &qStr);
		virtual void GetNResults(int n, ResultsList* pRes);

		virtual void SetDictionaryLvcsr() { mpDictionary = mpDictionaryLvcsr; }
		virtual void SetDictionaryPhn() { mpDictionary = mpDictionaryPhn; }

		virtual MKwsDictionary* GetDictionary() { return GetDictionaryLvcsr() ? GetDictionaryLvcsr() : GetDictionaryPhn();/*static_cast<MKwsDictionary*>(mpDictionary);*/ }
		virtual MKwsDictionary* GetDictionaryLvcsr() { return static_cast<MKwsDictionary*>(GetGlobalInstance()->mpDictionaryLvcsr); }
		virtual MKwsDictionary* GetDictionaryPhn() { return static_cast<MKwsDictionary*>(GetGlobalInstance()->mpDictionaryPhn); }
		virtual MKwsIndexer* GetIndexerLvcsr() { return static_cast<MKwsIndexer*>(mpIndexerLvcsr); }
		virtual MKwsIndexer* GetIndexerPhn() { return static_cast<MKwsIndexer*>(mpIndexerPhn); }
		virtual MKwsSearcher* GetSearcher() { return static_cast<MKwsSearcher*>(mpSearcher); }
		virtual MKws* GetGlobalInstance() { return static_cast<MKws*>(mpGlobalParent); }
};

}

#endif

