#ifndef MKWSINDEXERDBMYSQL_H
#define MKWSINDEXERDBMYSQL_H

#include "MKwsIndexer.h"
#include <mysql++/mysql++.h>

namespace sem
{
class MKwsIndexerDbMysql : public MKwsIndexer
{
		mysqlpp::Connection* mpCon;
		std::string mTableName;
	public:
		MKwsIndexerDbMysql(MKws* pMKws, MKwsDictionary* pDictionary) 
			: 
				MKwsIndexer(pMKws, pDictionary), 
				mTableName("MKwsInvIndexLvcsr") 
		{
			mpCon = new mysqlpp::Connection("test", "localhost", "karkulka", "karkulka753");
			if (!mpCon->connected())
			{
				CERR("ERROR: Connection unsuccessful");
				EXIT();
			}

			DBG("MKwsIndexerDbMysql::MKwsIndexerDbMysql()"); 
		}

		virtual void AddFwdLabelToInvertedIndex(ID docId, ID labelId, FwdLabel *pFwdLabel);

};
} // namespace sem
#endif

