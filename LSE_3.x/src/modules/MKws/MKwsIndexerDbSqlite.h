#ifndef MKWSINDEXERDBSQLITE_H
#define MKWSINDEXERDBSQLITE_H

#include "MKwsIndexer.h"

namespace sem
{

class MKwsIndexerDbSqlite : public MKwsIndexer
{
	protected:
		std::string mTableName;
		sqlite3 * mpIdxDb;
	public:
		MKwsIndexerDbSqlite(MKws* pMKws, MKwsDictionary* pDictionary) 
			: 
				MKwsIndexer(pMKws, pDictionary), 
				mTableName("MKwsInvIndexLvcsr") 
		{ 
			DBG("MKwsIndexerDbSqlite::MKwsIndexerDbSqlite()"); 
			/*
			int rc = sqlite3_open("idx/invidx.db", &mpIdxDb);
			if( rc ){
				fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(mpIdxDb));
				sqlite3_close(mpIdxDb);
				EXIT();
			}
			*/
		}

		virtual ~MKwsIndexerDbSqlite()
		{
			sqlite3_close(mpIdxDb);
		}

		virtual void AddFwdLabelToInvertedIndex(ID docId, ID labelId, FwdLabel *pFwdLabel);
//		virtual MIndexerStruct::InvLabel* GetOccurrences(ID labelId);

};
} // namespace sem
#endif
