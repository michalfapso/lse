// Viterbi - precomputation of FW BW probas
#include "latviterbifwbw.h"
#include <cmath>

using namespace std;
using namespace lse;
/*
double lse::logAdd(double a, double b, float treshold) {
//	return (a>b) ? a : b;
	if (a < b) {
		double tmp = a;
		a = b;
		b = tmp;
	}
	double diff = b - a;
	if (-diff > treshold) {
		return a;
	}
	return log(1.0 + exp(diff)) + a;
//	return log(exp(a) + exp(b));
}
*/

//--------------------------------------------------------------------------------
// constructor - has to alloc mem for all...
LatViterbiFwBw::LatViterbiFwBw (Lattice *inlat, float wip, float p_amscale, float p_lmscale, float p_amscale_kwd, float p_lmscale_kwd, bool p_baum_welsh, float p_logAdd_treshold) {
	unsigned int ii;

	this->amscale = p_amscale;
	this->lmscale = p_lmscale;
	this->amscale_kwd = p_amscale_kwd;
	this->lmscale_kwd = p_lmscale_kwd;
	this->baum_welsh = p_baum_welsh;
	this->logAdd_treshold = p_logAdd_treshold;

	// set the lattice to my own
	this -> lat = inlat;
	// get the no of nodes.
	NN = lat->nodes.size();
	// and allocate for all possible vars.
	fwds = new double[NN];
	bwds = new double[NN];
	fwlens = new int[NN];
	bwlens = new int[NN];
	// set all probas to -INF
	for (ii=0; ii<NN; ii++) {
		fwds[ii] = bwds[ii] = -INF;
		fwlens[ii] = bwlens[ii] = -1;
	}

	// set the WI proba.
	wipen = wip;
//	cout << "allocated for " << NN << " nodes"<< endl;
}


//--------------------------------------------------------------------------------
// destructor
LatViterbiFwBw::~LatViterbiFwBw () {
	delete [] fwds;
	delete [] bwds;
	delete [] fwlens;
	delete [] bwlens;
//	cout << "freed..." << endl;

}


//--------------------------------------------------------------------------------
// doing the job - FW
void LatViterbiFwBw::computeFwViterbi() {
//	cout << "at the moment nothing... " << endl;

	ID_t nodeid, S, E;     // starting and ending node id.
	unsigned int ii,jj;
	vector <ID_t> flinks;  // forward links
	double cfwd,nfwd;      // current and new forward Viterbi proba
	int    cfwlen,nfwlen;  // current and new length of path.
	float  a,l;	       // acoustic and LM probas.


// hoping that the nodes are sorted in time - otherwise we're fucked up :-)
	// get the 1st node and the last ...
	nodeid = lat->lastNode().id;
//	cout <<"FORWARD VITERBI ... ";
	DBGFWBW("got last node id=" << nodeid << " might be a nonsense"<<endl);
	nodeid = lat->firstNode().id;
	DBGFWBW("got 1st node id=" << nodeid << endl);
	// set the proba to zero, length to zero
	fwds[nodeid] = 0.0;
	fwlens[nodeid] = 0;
	for (ii=0; ii<NN-1; ii++) {

		// get the current fwd proba and path  length
		cfwd = fwds[ii];
		cfwlen = fwlens[ii];
		//check the node...
		DBGFWBW("=== ii=" << ii << " node id=" << lat->nodes[ii].id);
		DBGFWBW(" t=" << lat->nodes[ii].t << " W="<<lat->nodes[ii].W <<endl);
		// get the fwd links
		flinks = lat->fwdLinks[ii];
		// go through them, each time:
		for (jj=0; jj < flinks.size(); jj++) {
			// - get where we are going from and to + probas
			// cout << lat->links[jj]<<endl;
			S=lat->links[flinks[jj]].S;
			E=lat->links[flinks[jj]].E;
			a=lat->links[flinks[jj]].a;
			l=lat->links[flinks[jj]].l;
			DBGFWBW("S E a l: " << S << " " <<E<<" "<<a<<" "<<l<<endl);
			
//			DBG("nodesQ.push(): "<<E);
//			nodesQ.push(E);
			
			// new proba and new path length
			// add acoustic, language model and penalty
			nfwd = cfwd + a*amscale + l*lmscale;
                        if (lat->nodes[E].W != "!NULL")
                                nfwd += wipen;                        
//			if (lat->lexicon->CanAddWiPen(lat->nodes[E].W_id))
			nfwlen = cfwlen + 1;
			// OK, if better than proba at the target, push
			// it there...
			if (baum_welsh) {
				fwds[E] = logAdd(fwds[E], nfwd, logAdd_treshold);
			} else if (nfwd > fwds[E]) {
				DBGFWBW("updating node "<< E<< " from fwd "<<fwds[E]<<" len "<<fwlens[E]);
				DBGFWBW(" to fwd " << nfwd << " len " << nfwlen<<endl);
				fwds[E] = nfwd;
				fwlens[E] = nfwlen;
			}
		}
	}
	// at this point, should get the final Viterbi proba at node NN
//	cout<<"LAST NODE: Viterbi at "<<NN-1;
	ii = NN-1;
	DBGFWBW("*** =" << ii << " node id=" << lat->nodes[ii].id);
	DBGFWBW(" t=" << lat->nodes[ii].t << " W="<<lat->nodes[ii].W <<endl);

	// write fwds into nodes
	for (Lattice::NodeMap::iterator n=lat->nodes.begin(); n!=lat->nodes.end(); ++n) {
		(n->second).alpha = fwds[(n->second).id];
	}
	
	for (unsigned int i=0; i<NN; i++) DBGFWBW("fwds["<<i<<"]: "<< setprecision(13) << fwds[i]);
	
//	cout << " fwd "<<fwds[NN-1]<<" len "<<fwlens[NN-1]<<endl;

}

//--------------------------------------------------------------------------------
// doing the job - BW
void LatViterbiFwBw::computeBwViterbi() {
//	cout << "at the moment nothing... " << endl;

	ID_t nodeid, S, E;     // starting and ending node id.
	unsigned int ii,jj;
	vector <ID_t> blinks;  // forward links
	double cbwd,nbwd;      // current and new backward Viterbi proba
	int    cbwlen,nbwlen;  // current and new length of path.
	float  a,l;	       // acoustic and LM probas.
	
	
// hoping that the nodes are sorted in time - otherwise we're fucked up :-)
	// get the 1st node and the last ...
	nodeid = lat->lastNode().id;
//	cout << "BACKWARD VITERBI ... ";
	DBGFWBW("got last node id=" << nodeid << " might be a nonsense"<<endl);
	nodeid = lat->firstNode().id;
	DBGFWBW("got 1st node id=" << nodeid << endl);
	// set the proba to zero, length to zero
	bwds[NN-1] = 0.0;
	bwlens[NN-1] = 0;
	for (ii=NN-1; ii>0; ii--) {  // 0 will be handled separately ...

		// get the current fwd proba and path  length
		cbwd = bwds[ii];
		cbwlen = bwlens[ii];
		//check the node...
		DBGFWBW("=== ii=" << ii << " node id=" << lat->nodes[ii].id);
		DBGFWBW(" t=" << lat->nodes[ii].t << " W="<<lat->nodes[ii].W <<endl);
		// get the bwd links
		blinks = lat->bckLinks[ii];
		// go through them, each time:
//		DBG("--------------------------------------------------------------------------------");
//		DBG("node ID: " << ii);
		for (jj=0; jj < blinks.size(); jj++) {
			// - get where we are going from and to + probas
			// cout << lat->links[jj]<<endl;
			S=lat->links[blinks[jj]].S;
			E=lat->links[blinks[jj]].E;
			a=lat->links[blinks[jj]].a;
			l=lat->links[blinks[jj]].l;
			DBGFWBW("S E a l: " << S << " " <<E<<" "<<a<<" "<<l<<endl);
			// new proba and new path length
			// add acoustic, language model and penalty
			nbwd = cbwd + a*amscale + l*lmscale;
			if (lat->nodes[E].W != "!NULL")
				nbwd += wipen;
			nbwlen = cbwlen + 1;
//			DBG("nbwd: "<<nbwd<<" = "<<cbwd<<" + "<<a<<" + "<<l<<" + "<<wipen);
			// OK, if better than proba at the target, push
			// it there...

			if (baum_welsh) {
				bwds[S] = logAdd(bwds[S], nbwd, logAdd_treshold);
			} else if (nbwd > bwds[S]) { // this must be START !
//				DBG("nbwd: "<<nbwd<<" = "<<cbwd<<" + "<<a<<" + "<<l<<" + "<<wipen);
//				DBG("*********** nbwd:" << nbwd << " bwds["<<S<<"]:" << bwds[S]);
				DBGFWBW("updating node "<< S<< " from bwd "<<bwds[S]<<" len "<<bwlens[S]);
				DBGFWBW(" to fwd " << nbwd << " len " << nbwlen<<endl);
				bwds[S] = nbwd;
				bwlens[S] = nbwlen;
			}
		}
	}
	// at this point, should get the final Viterbi proba at node NN
//	cout<<"LAST NODE: Viterbi at "<<0;
	ii = 0;
	DBGFWBW("*** =" << ii << " node id=" << lat->nodes[ii].id);
	DBGFWBW(" t=" << lat->nodes[ii].t << " W="<<lat->nodes[ii].W <<endl);
//	cout << " bwd "<<bwds[ii]<<" len "<<bwlens[ii]<<endl;

//	for (unsigned int i=0; i<NN; i++) DBG("bwds["<<i<<"]: "<<bwds[i]);
	
	// write fwds into nodes
	for (Lattice::NodeMap::iterator n=lat->nodes.begin(); n!=lat->nodes.end(); ++n) {
		(n->second).beta = bwds[(n->second).id];
	}
}


//--------------------------------------------------------------------------------
// print it out to file;
void LatViterbiFwBw::writeViterbiFwBw(const string filename) {
	DBG("writeViterbiFwBw("<<filename<<")");
//	cout << "when ready, this will print ..." << endl;
	ofstream out(filename.c_str());
	// first thing - out the WI penalty and no of nodes
	out << wipen<<"\t"<<NN<<endl;
	// just go node by node and print ... ID, fwd, fwdlen, bwd, bwdlen
	unsigned int ii;

	for (ii=0; ii<NN; ii++) {
		out <<setprecision(13) << ii<<"\t"<<fwds[ii]<<"\t"<<fwlens[ii]<<"\t"<<bwds[ii]<<"\t"<<bwlens[ii]<<endl;
	}
	out.close();
	DBG("writeViterbiFwBw()...done");
}
//--------------------------------------------------------------------------------
// load it from file;
int LatViterbiFwBw::loadViterbiFwBw(const string filename) {
//	cout << "when ready, this will print ..." << endl;
	// this will be a classical reading - no iostreams ...
	FILE *fp;
	unsigned int myNN;
	float mywipen;
	unsigned int ii;
	// values to be read
	double fwd,bwd;
	unsigned int fwlen,bwlen,iir;

	fp = fopen(filename.c_str(),"r");
	if (fp == NULL) {
		cout << "can't open ViterbiFwBw file"<<filename<<endl;
		return(1);
	}
	// check no of nodes and wi proba - if don't match, return nonzero
	fscanf(fp,"%f",&mywipen);
	fscanf(fp,"%d",&myNN);
	if (mywipen != wipen) {
		cout << "read wi penalty "<<mywipen<<" not match with set "<<wipen;
		return(2);
	}
	if (myNN != NN) {
		cout << "read no of nodes "<<myNN<<" not match with "<<NN;
		return(3);
	}
	// here, everything should be OK, and all should be allocated.
	for (ii=0; ii<NN; ii++) {
		if (fscanf(fp,"%d",&iir) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		if (iir != ii) {
			cout <<"strange index of node"; return(5);
		}
		if (fscanf(fp,"%lf",&fwd) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		fwds[ii] = fwd;
		if (fscanf(fp,"%d",&fwlen) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		fwlens[ii] = fwlen;
		if (fscanf(fp,"%lf",&bwd) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		bwds[ii] = bwd;
		if (fscanf(fp,"%d",&bwlen) != 1) {
			cout <<filename<<" corrupted";  return(4);
		}
		bwlens[ii] = bwlen;
	}
	fclose(fp);
	return(0);
}


TLatViterbiLikelihood LatViterbiFwBw::getLinkConfidence(lse::ID_t linkID, bool scaleOnKeyword) {
//	ID_t S = lat->links[linkID].S;
//	ID_t E = lat->links[linkID].E;
	double alpha = fwds[ lat->links[linkID].S ];
	double beta = bwds[ lat->links[linkID].E ];
/*	if (linkID < 5) {
		DBG("links["<<linkID<<"]: (" << alpha << " + " << lat->links[linkID].a << " + " << lat->links[linkID].l << " + " << beta << ") - " << fwds[NN-1] << " = "<< (alpha + lat->links[linkID].a         + lat->links[linkID].l         + beta) - fwds[NN-1] <<" ...amscale:"<<amscale<<" lmscale:"<<lmscale);
	}
*/
	
	double link_wipen = (lat->lexicon->CanAddWiPen(lat->nodes[lat->links[linkID].E].W_id) ? wipen : 0);
		
	if (scaleOnKeyword) {
		LatTime time_length = lat->nodes[lat->links[linkID].E].t - lat->nodes[lat->links[linkID].S].t;
		float kwd_scale;
		if (time_length > 0)
		{
			kwd_scale = 
				(
					lat->links[linkID].a*amscale*amscale_kwd 
					+ lat->links[linkID].l*lmscale*lmscale_kwd
				) / time_length;
		}
		else
		{
			kwd_scale = 
				(
					lat->links[linkID].a*amscale*amscale_kwd 
					+ lat->links[linkID].l*lmscale*lmscale_kwd
				);
		}
		return (
			alpha 
			+ lat->links[linkID].a*amscale + lat->links[linkID].l*lmscale 
			+ beta 
			+ link_wipen 
			+ kwd_scale	
		) - fwds[NN-1];	// with scaling
	} else {
		return (alpha + lat->links[linkID].a*amscale + lat->links[linkID].l*lmscale + beta + link_wipen) - fwds[NN-1];	// without scaling
	}
}


/**
  @brief Compute each link's confidence and set Lattice::Node's parameters:
         bestLikelihood, bestLinkID, tStart
*/
void LatViterbiFwBw::computeLinksLikelihood(LatTime startTime) {
	// values for first node in lattice have to be set manually
	Lattice::NodeMap::iterator firstNode = lat->nodes.begin();
	lat->nodes[firstNode->first].tStart = startTime;
	lat->nodes[firstNode->first].bestLikelihood = 0; // log(1) = 0
	lat->nodes[firstNode->first].bestLinkID = -1;
	lat->nodes[firstNode->first].bestBckLinkID = -1;
	TLatViterbiLikelihood arr_bestLikelihood_scaleOnKeyword[lat->nodes.size()]; // array of best likelihoods with scaling on keyword
	TLatViterbiLikelihood arr_bestFwdLikelihood[lat->nodes.size()];				// array of best outgoing link's confidence
	TLatViterbiLikelihood arr_bestBckLikelihood[lat->nodes.size()];				// array of best ingoing link's confidence

	for (unsigned int i=0; i<lat->nodes.size(); i++) {
		arr_bestLikelihood_scaleOnKeyword[i] = -INF;
		arr_bestBckLikelihood[i] = -INF;
		arr_bestFwdLikelihood[i] = -INF;
	}
		 
	// now compute all confidences
	for (Lattice::LinkMap::iterator l=lat->links.begin(); l!=lat->links.end(); ++l) 
	{
		//DBG("l: "<<l->second);
		(l->second).likelihood					= (l->second).a*amscale + (l->second).l*lmscale;
//		DBG("link #"<<(l->second).id<<" likelihood:"<<(l->second).likelihood);
		(l->second).confidence					= getLinkConfidence(l->first, true);
		(l->second).confidence_noScaleOnKeyword	= getLinkConfidence(l->first, false);
		// set the best confidence for link's startNode 
		if (arr_bestFwdLikelihood[(l->second).S] < (l->second).confidence) {
			arr_bestFwdLikelihood[(l->second).S] = (l->second).confidence; // set the best forward confidence
			lat->nodes[(l->second).S].bestFwdLinkID = l->first; // set the best forward-link ID
		}
		// set the best confidence for link's endNode 
		if (arr_bestBckLikelihood[(l->second).E] < (l->second).confidence) {
			arr_bestBckLikelihood[(l->second).E] = (l->second).confidence;
			lat->nodes[(l->second).S].bestBckLinkID = l->first;
		}

		// nodes[...]->bestLinkID,tStart: with scaling on kwd
		if (lat->nodes[(l->second).S].W == "!NULL")
		{
			// for each link comming to the skipped node
			for (vector<ID_t>::iterator i_link=lat->bckLinks[(l->second).S].begin(); 
				i_link!=lat->bckLinks[(l->second).S].end(); 
				i_link++)
			{
				// i_link: the one that comes to the skipped node
				// l:      comes from the skipped node to the currently processed one
				double time_length = lat->nodes[l->second.E].t - lat->nodes[lat->links[*i_link].S].t;

				double link_wipen = lat->lexicon->CanAddWiPen(lat->nodes[lat->links[*i_link].S].W_id) ? wipen : 0;

				double conf_sum = 
					fwds[ lat->links[*i_link].S ] // alpha
					+ (
						+ lat->links[*i_link].a * amscale * amscale_kwd // acoustic likelihood with scaling on keyword
						+ lat->links[*i_link].l * lmscale * lmscale_kwd // language likelihood with scaling on keyword
						+ l->second.a * amscale * amscale_kwd // acoustic likelihood with scaling on keyword
						+ l->second.l * lmscale * lmscale_kwd // language likelihood with scaling on keyword
					  ) / (time_length > 0 ? time_length : 1)
					+ lat->links[*i_link].a * amscale // acoustic likelihood
					+ lat->links[*i_link].l * lmscale // language likelihood
					+ link_wipen // insertion penalty is not added for skipped node
					+ l->second.a * amscale // acoustic likelihood
					+ l->second.l * lmscale // language likelihood
					+ bwds[ l->second.E ]
					- fwds[ NN-1 ]; // normalize by the best path
	
				if (arr_bestLikelihood_scaleOnKeyword[l->second.E] < conf_sum)
				{
					arr_bestLikelihood_scaleOnKeyword[l->second.E] = conf_sum;
					lat->nodes[(l->second).E].bestLinkID = l->first;					// id of node's ingoing link with best confidence
					lat->nodes[(l->second).E].tStart = lat->nodes[lat->links[*i_link].S].t;		// node's start time = end time of link's start node
				}
			}
		}
		else 
		if (arr_bestLikelihood_scaleOnKeyword[(l->second).E] < (l->second).confidence) {
			arr_bestLikelihood_scaleOnKeyword[(l->second).E] = (l->second).confidence;
			lat->nodes[(l->second).E].bestLinkID = l->first;					// id of node's ingoing link with best confidence
			lat->nodes[(l->second).E].tStart = lat->nodes[(l->second).S].t;		// node's start time = end time of link's start node
		}

		// nodes[...]->bestLikelihood: with scaling on kwd
		if (lat->nodes[(l->second).E].bestLikelihood < (l->second).confidence) {
			lat->nodes[(l->second).E].bestLikelihood = (l->second).confidence;
		}
	}
}


