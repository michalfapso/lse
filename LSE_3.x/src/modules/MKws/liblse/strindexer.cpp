#include "strindexer.h"

using namespace std;
using namespace lse;

void StrIndexer::writeStruct(ofstream &out, const ID_t id, const string val) {
	
	char terminator = 0;
	
	out.write(reinterpret_cast<const char *>(&id), sizeof(id));
	out << (val);
	out.write(reinterpret_cast<const char *>(&terminator), sizeof(terminator)); // string terminator
}


int StrIndexer::readStruct(ifstream &in, ID_t &id, string &val) {
	
	in.read(reinterpret_cast<char *>(&id), sizeof(id));

	if(in.eof())
		return 2;
	
	// read a str terminated by 0x00
	val.clear();
	char c;
	in.get(c);
	
	while(c != 0) {				
		val += c;
		in.get(c);
	}
	return 0;	
}

