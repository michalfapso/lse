#ifndef INDEXER_H
#define INDEXER_H

#include "lattypes.h"
#include "latindexer.h"
#include "strindexer.h"
#include "latmeetingindexer.h"

namespace lse {

class Indexer {
	
		
	public:
		
		LatMeetingIndexer meetings;
		LatIndexer lattices;
};

} // namespace lse

#endif
