#ifndef languagemodel_H
#define languagemodel_H

#include <vector>
#include <fstream>
#include <zlib.h>
#include "lattypes.h"
#include "latmeetingindexer.h"
#include "common.h"
#include "dbg.h"

namespace lse {


//--------------------------------------------------
//	LanguageModelRec
//
class LanguageModelRec
{
public:
	double 		mP;
	std::vector<std::string> mWords;
	double		mBackoff;

	void Reset()
	{
		mP = 0;
		mWords.clear();
		mBackoff = 0;
	}

	friend std::ostream& operator<<(std::ostream& os, const LanguageModelRec &n) {
		os << n.mP<<" ";
		for (std::vector<std::string>::const_iterator i = n.mWords.begin(); i != n.mWords.end(); ++i)
		{
			os << *i << " ";
		}
		os << n.mBackoff;
		return os;
	}
};

//--------------------------------------------------
//	LanguageModelFile
//
class LanguageModelFile
{
	std::ifstream mFile;
	gzFile mFileGz;
	bool mGzipped;
	
	int		mState;
	int		mLastNgramIndex;
	bool 	mEpsilon;


	inline bool IsEol(const char c);
	inline bool IsDelimiter(const char c);
	bool ReadHeader();

public:

	std::vector<int> mNgramsCount;

	LanguageModelFile(const std::string filename = "") 
	{ 
		if (filename != "") 
		{
			Open(filename);
		}
	}

	void Init()
	{
		mFileGz = NULL;
		mGzipped = false;
		mState = 0;
		mLastNgramIndex = 0;
		mEpsilon = false;
	}

	void Open(const std::string filename);
	void Close();
	bool Next(LanguageModelRec *rec);
};



//--------------------------------------------------
//	LanguageModel
//
class LanguageModel
{
public:

	typedef float mtP;
	typedef float mtBackoff;
	typedef int mtChildrenCount;
	class Node;

	class Array
	{
	protected:
		unsigned char *mpArray;
		int mSize;
		int mCount;
		int mItemSize;
		unsigned char *mpEmptyPos;
		friend class LanguageModel;
	public:
		virtual ~Array() { delete[] mpArray; }
		ID_t GetNodeWordId(unsigned char *pNode);
		mtP GetNodeP(unsigned char *pNode);
		virtual Node operator[](unsigned char *pNode);
		virtual int GetItemSize() { return 0; };
	};

	class NodeArray : public Array
	{
	public:

		NodeArray(int Size)
		{
			DBG("NodeArray("<<Size<<")");
			mCount = Size;
			mItemSize = GetItemSize();
			mSize = mItemSize * Size;
			mpArray = new unsigned char[mSize];
			mpEmptyPos = mpArray;
//			DBG("new [] ...0x"<<std::hex<<(int)mpArray<<"...0x"<<(int)(mpArray+mSize));
			DBG("new [] ..."<<(int)mpArray<<"..."<<(int)(mpArray+mSize));
		}

		int GetItemSize()
		{
			// probability + word id + backoff + pointer to children + children count
			return sizeof(mtP) + sizeof(ID_t) + sizeof(mtBackoff) + sizeof(unsigned char *) + sizeof(mtChildrenCount);
		}

		void Add(mtP p, ID_t wordId, mtBackoff b);

		mtChildrenCount GetNodeChildrenCount(unsigned char *pNode);
		mtBackoff GetNodeBackoff(unsigned char *pNode);
		unsigned char* GetNodeChildren(unsigned char *pNode);

		void SetNodeChildrenCount(unsigned char *pNode, mtChildrenCount count);
		void SetNodeChildren(unsigned char *pNode, unsigned char *pChildren);

		Node operator[](unsigned char *pNode);
	};

	class LeafArray : public Array
	{
	public:
		LeafArray(int Size)
		{
			DBG("LeafArray("<<Size<<")");
			mCount = Size;
			mItemSize = GetItemSize();
			mSize = mItemSize * Size;
			mpArray = new unsigned char[mSize];
			mpEmptyPos = mpArray;
//			DBG("new [] ...0x"<<std::hex<<(int)mpArray<<"...0x"<<(int)(mpArray+mSize));
			DBG("new [] ..."<<(int)mpArray<<"..."<<(int)(mpArray+mSize));
		}

		int GetItemSize()
		{
			// probability + word id
			return sizeof(mtP) + sizeof(ID_t);
		}

		void Add(mtP p, ID_t wordId);
	};


	//--------------------------------------------------
	//	Node
	//
	class Node
	{
		unsigned char *mpSelf;

	public:

	//	ID_t		 	mMeetingID;

		mtP			mP;
		ID_t		mWordId;
		mtBackoff	mBackoff;
		unsigned char *mpChildren;
		mtChildrenCount	mChildrenCount;

		friend std::ostream& operator<<(std::ostream& os, const Node &n) {
			return os << "p="<<n.mP<<" wordID="<<n.mWordId<<" backoff="<<n.mBackoff<<" children:"<<n.mChildrenCount;
		}
	};










	Lexicon mLexicon;

	LanguageModel() : mLexicon(Lexicon::readonly) 
	{
//		mpRootNode = new Node;
	}

	void Load(const std::string filename);
	unsigned char* SearchChildren(int n, unsigned char *parentNode, ID_t wordID);
        float GetWordNGramProb(std::string WordString); //have to be length(WordString) <= max(n) of ngram
	float GetSentenceNGramProb(std::string WordString, int ngram); //sentence is split to overlaped words length of max ngram and sum up
        bool  IsWordInDict(std::string Word);
        
private:
	Node *mpRootNode;

	Array **mpNodeArrays; // array of pointers to the beginnings of each ngram level nodes array
	
};



}

#endif
