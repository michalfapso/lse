#include "lexicon.h"

using namespace std;
using namespace lse;



bool lse::order_by_word(const Lexicon::Record &l, const Lexicon::Record &r) {
	return l.word < r.word;
}

bool lse::order_by_id(const Lexicon::Record &l, const Lexicon::Record &r) {
	return l.id < r.id;
}

void Lexicon::writeStruct(ofstream &out, const ID_t id, const string val) {
	
	char terminator = 0;
	
	out.write(reinterpret_cast<const char *>(&id), sizeof(id));
	out << (val);
	out.write(reinterpret_cast<const char *>(&terminator), sizeof(terminator)); // string terminator
}


int Lexicon::readStruct(ifstream &in, ID_t &id, string &val) {
	
	in.read(reinterpret_cast<char *>(&id), sizeof(id));

	if(in.eof())
		return 2;
	
	// read a str terminated by 0x00
	val.clear();
	char c;
	in.get(c);
	
	while(c != 0) {				
		val += c;
		in.get(c);
	}
	return 0;	
}


//--------------------------------------------------------------------------------
// addRecord()
// 
// Add pair ID -> Val to indexer

void Lexicon::addRecord(const ID_t id, const string val) {
	static bool nullFound = false;
	if (!nullFound) if (val=="!NULL") {
		nullFound = true;
		mNullWordID = id;
	}
		
	val2idMap.insert(make_pair(val, id));
	id2valMap.insert(make_pair(id, val));

//	DBG("Lexicon::addRecord("<<id<<", "<<val<<")");
}


//--------------------------------------------------------------------------------
// getValID()
//
// If there is no occurence of given val in the lexicon, 
// new record is created in both val->id and id->val maps
// and valID of inserted val is returned,
// 
// Return value: ID of given val

ID_t Lexicon::getValID(const string val, bool add_if_not_found) {
	if (this->isReadonly)
	{
		return word2id_readonly(val);
	}

	// try to find given val in val2idMap
	map<string, ID_t>::iterator pos = val2idMap.find(val);
//	Val2ID_iter pos = val2idMap.find(val);
	
	if (pos != val2idMap.end()) {
		// ID of given val is returned
		return pos->second;		
	} 
	else if (add_if_not_found) {
		// new record is created and it's ID is returned
		ID_t newID = getNewID();
		addRecord(newID, val);
//		val2idMap.insert(make_pair(val, newID));
//		id2valMap.insert(make_pair(newID, val));	
		return newID;
	} else {
		// value has not been found, so ERROR is returned
		return GI_NOT_FOUND;
	}
}


//--------------------------------------------------------------------------------
// getVal()
// 
// If there is a val with given ID in the lexicon, it's ID is returned,
// otherwise NULL is returned
//
// Return value: val with given ID (or NULL)



string Lexicon::getVal(const ID_t id) {
	if (this->isReadonly)
	{
		return (string)id2word_readonly(id);
	}

	map<ID_t, string>::iterator pos = id2valMap.find(id);
//	ID2val_iter pos = id2valMap.find(id);
	assert(pos != id2valMap.end());

	if (pos != id2valMap.end()) {
		// val with given ID is returned
		return pos->second;
	} 
	else {
		// ERROR: there is no such val in the lexicon
		return NULL;
	}
}



//--------------------------------------------------------------------------------
// getNewID
// 
// Return value - next possible valID

ID_t Lexicon::getNewID() {

	// Sequence search for max key (ID) value
	ID_t max = GI_START_INDEX - 1; // this value will be increased (return max+1)
	for (map<ID_t, string>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		if (max < i->first) {
			max = i->first;
		}
	}
	return max+1;
}



//--------------------------------------------------------------------------------
// LoadFromFile

int Lexicon::loadFromFile(const string filename) {
/*	
	FILE *f_inWords = fopen(filename.c_str(), "rb");
	if (f_inWords == NULL) {
		return -1;
	}

	string str = filename + LEXICON_ORDBYID_EXT;
	FILE *f_inWordsIdxID = fopen(str.c_str(), "rb");
	if (f_inWordsIdxID == NULL) {
		return -1;
	}

	string str = filename + LEXICON_ORDBYALPHA_EXT;
	FILE *f_inWordsIdxAlpha = fopen(str.c_str(), "rb");
	if (f_inWordsIdxAlpha == NULL) {
		return -1;
	}

	struct stat fileStat;
	// get the size of lexicon (in bytes)
	if(!stat(filename.c_str(), &fileStat)) {
		this->wordsArray_size = fileStat.st_size;
	} else {
		cerr << "Cannot determine the size of NodeLinksIndex file" << endl << flush;
		return(-1);
	}
	// get the size of lexicon index (number of words)
	if(!stat(filename.c_str(), &fileStat)) {
		this->wordsArrayPointers_size = (int)(fileStat.st_size / sizeof(long));
	} else {
		cerr << "Cannot determine the size of NodeLinksIndex file" << endl << flush;
		return(-1);
	}

	// load wordsArray
	this->wordsArray = new char[this->wordsArray_size];
	fread(this->wordsArray, sizeof(char), this->wordsArray_size, f_inWords);
	fclose(f_inWords);
	
	// load wordsArrayPointers (sorted by ID)
	this->wordsArrayPointersIDsort = new char[this->wordsArrayPointers_size];
	fread(this->wordsArrayPointersIDsort, sizeof(long), this->wordsArrayPointers_size, f_inWordsIdxID);
	fclose(f_inWordsIdxID);

	// load wordsArrayPointers (sorted alphabetically)
	this->wordsArrayPointersAlphasort = new char[this->wordsArrayPointers_size];
	fread(this->wordsArrayPointersAlphasort, sizeof(long), this->wordsArrayPointers_size, f_inWordsIdxAlpha);
	fclose(f_inWordsIdxAlpha);
*/
	
	
	// clear both maps
	id2valMap.clear();
	val2idMap.clear();

	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
		return (2);
	}
	
	ID_t valID;
	string val;
	
	// read all nodes and convert strID into str using lexicon
	while(!in.eof()) {
		
		if (readStruct(in, valID, val) != 0) {
			break;
		}
		if (in.eof()) break;
		// add record to both maps
		addRecord(valID, val);
	}
	
	in.close();
	
	return 0;
}


int Lexicon::createIndexFile(const string filename) {

	string str = filename + LEXICON_ORDBYID_EXT;
	FILE *fout = fopen(str.c_str(), "wb");
	if (fout == NULL) {
		return LSE_ERROR_IO;
	}

	ifstream in(filename.c_str(), ios::binary);
	if (!in.good()) {
		return (2);
	}

	vector<Lexicon::Record> allWords;
	Lexicon::Record tmpRecord;
	long pos = 0;
//	ID_t valID;
	string val;
	int counter = 0;
	// ORDERED BY ID
	fwrite(&counter, sizeof(counter), 1, fout);
	while(!in.eof()) {
		pos = in.tellg();
		tmpRecord.pos = pos;
		if (readStruct(in, tmpRecord.id, tmpRecord.word) != 0) {
			break;
		}
		if (in.eof()) break;
		allWords.push_back(tmpRecord);
		fwrite(&pos, sizeof(pos), 1, fout);
		counter++;
	}

	fseek(fout, 0, SEEK_SET); 					// jump to the first record with given wordID
	fwrite(&counter, sizeof(counter), 1, fout);	// how many records are stored in this file - will be set later
	
	// ORDERED BY WORD
	str = filename + LEXICON_ORDBYALPHA_EXT;
	fout = fopen(str.c_str(), "wb");
	if (fout == NULL) {
		return LSE_ERROR_IO;
	}
	
	fwrite(&counter, sizeof(counter), 1, fout);
	sort(allWords.begin(), allWords.end(), order_by_word);
	for (vector<Lexicon::Record>::iterator i=allWords.begin(); i!=allWords.end(); ++i) {
		fwrite(&(i->pos), sizeof(i->pos), 1, fout);
	}

	fclose(fout);
	in.close();
	return 0;
}

int Lexicon::loadFromFile_readonly(const string filename) {

	DBG("Lexicon::loadFromFile_readonly()");
	
	struct stat fileStat;
	if(!stat(filename.c_str(), &fileStat)) {
		this->wordArray_size = fileStat.st_size;
	} else {
		cerr << "Lexicon: Cannot determine the size of recordsArrayPointers file: " << filename << endl << flush;
		return (-1);
	}

	FILE *fin = fopen(filename.c_str(), "rb");
	if (fin == NULL) {
		return LSE_ERROR_IO;		
	}

	DBG("wordArray_size: "<<this->wordArray_size);
	this->wordArray = new char[this->wordArray_size];
	fread(this->wordArray, sizeof(char), this->wordArray_size, fin);
	fclose(fin);


	// INDEX ORDERED BY ID
	string filename_idxid = filename + LEXICON_ORDBYID_EXT;
	fin = fopen(filename_idxid.c_str(), "rb");
	if (fin == NULL) {
		return LSE_ERROR_IO;		
	}

	DBG("reading .idxid");
	fread(&this->wordArrayIdx_size, sizeof(this->wordArrayIdx_size), 1, fin);
	DBG("  size:"<<this->wordArrayIdx_size);
	this->wordArrayIdxIDsort = new long [this->wordArrayIdx_size];
	fread(this->wordArrayIdxIDsort, sizeof(long), this->wordArrayIdx_size, fin);
	fclose(fin);
	DBG("  done");

	
	// INDEX ORDERED ALPHABETICALLY
	string filename_idxalpha = filename + LEXICON_ORDBYALPHA_EXT;
	fin = fopen(filename_idxalpha.c_str(), "rb");
	if (fin == NULL) {
		return LSE_ERROR_IO;		
	}

	DBG("reading .idxalpha");
	fread(&this->wordArrayIdx_size, sizeof(this->wordArrayIdx_size), 1, fin);
	DBG("  size:"<<this->wordArrayIdx_size);
	this->wordArrayIdxAlphasort = new long[this->wordArrayIdx_size];
	fread(this->wordArrayIdxAlphasort, sizeof(long), this->wordArrayIdx_size, fin);
	fclose(fin);
	DBG("  done");
	
	DBG("lexicon: size="<<this->wordArray_size<<" idx_size="<<this->wordArrayIdx_size);

	// set the nullWordID
	mNullWordID = word2id_readonly("!NULL");
	
	return 0;

}


const char * Lexicon::id2word_readonly(ID_t id) 
{
	DBG("id2word_readonly():");
	DBG("  id:"<<id<<" wordArrayIdxIDsort[id-1]:"<<this->wordArrayIdxIDsort[id-1]);
	return this->wordArray + ( this->wordArrayIdxIDsort[id-1] + sizeof(ID_t) );
}


ID_t Lexicon::word2id_readonly(const string &w, bool exitWhenWordNotFound) 
{
	DBG("word2id_readonly():");
	DBG("  this->wordArrayIdx_size:"<<this->wordArrayIdx_size);
	int low = 0, high = 0, i = 0;
	char * ptr = NULL;

	if (this->wordArrayIdx_size == 0) {
		cerr << "ERROR in Lexicon::word2id_readonly()" << endl << "ERROR: Lexicon is empty" << endl << flush;
		EXIT();
	}
	for ( low=-1, high=this->wordArrayIdx_size; high-low > 1;) {
		i = (high + low) / 2;
		
		ptr = this->wordArray + this->wordArrayIdxAlphasort[i] + sizeof(ID_t);
		DBG("curWord["<<i<<"]:"<<ptr);
			
		if (strcmp(w.c_str(), ptr) <= 0) 
			high = i;
		else
			low = i;

		DBG(low<<".."<<high<<" -> "<<this->wordArrayIdxAlphasort[low]<<".."<<this->wordArrayIdxAlphasort[high]<<" -> "<<this->wordArray + this->wordArrayIdxAlphasort[low] + sizeof(ID_t)<<".."<<this->wordArray + this->wordArrayIdxAlphasort[high] + sizeof(ID_t));
	}
	DBG("curWord["<<i<<"]:"<<ptr);
	if (strcmp(w.c_str(), this->wordArray + this->wordArrayIdxAlphasort[high] + sizeof(ID_t)) == 0) 
		return *(reinterpret_cast<long*>(this->wordArray + this->wordArrayIdxAlphasort[high]));
	else {
		CERR("ERROR: word2id_readonly('"<<w<<"') not found");
		if (exitWhenWordNotFound) 
		{
			exit(-1);
		}
		return -1;
	}
}


bool Lexicon::WordExists_readonly(ID_t id)
{
	return (1 <= id) && (id <= wordArrayIdx_size);
}

bool Lexicon::WordExists_readonly(const string &word)
{
	return word2id_readonly(word) < 0;
}

//--------------------------------------------------------------------------------
// saveToFile

int Lexicon::saveToFile(const string filename) {
/*		
	FILE *f_outWords = fopen(filename.c_str(), "wb");
	if (f_outWords == NULL) {
		return -1;
	}

	string str = filename + LEXICON_ORDBYID_EXT;
	FILE *f_outWordsIdxID = fopen(str.c_str(), "wb");
	if (f_outWordsIdxID == NULL) {
		return -1;
	}
	
	char terminator = 0;
	for (int i=0; i<this->wordsCount; i++) {
//		long wordPos = ftell(f_outWords);
		long wordPos = wordArrayPointersIDsort[i];
		fwrite(&wordsArray[wordPos], strlen(wordsArray[wordPos]), 1, f_outWords);
		fwrite(&terminator, sizeof(terminator), 1, f_outWords);
		// ID index
		fwrite(&wordPos, sizeof(wordPos), 1, f_outWordsIdxID);
	}

	close(f_outWordsIdxID);
	close(f_outWords);
*/	

	ofstream out(filename.c_str(), ios::binary);
	if (out.bad()) {
		return (2);
	}

	// it is not important which map to save to file, because they are identical
	for(map<ID_t, string>::iterator i=id2valMap.begin(); i!=id2valMap.end(); ++i) {
		writeStruct(out, i->first, i->second);
	}
	out.close();
	
	return 0;
}


//--------------------------------------------------------------------------------
// print()

void Lexicon::print() 
{
	for (map<ID_t, string>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		cout << i->first << '\t' << i->second << endl;
	}
}


bool Lexicon::isMetaWord(const ID_t id) 
{
	return (id == this->getNullWordID()) || isMetaWord(this->isReadonly ? id2word_readonly(id) : getVal(id));
}

bool Lexicon::isMetaWord(const std::string word) 
{
	return (word[0] == '<');
//	return (word[0] == '<' || word == "sil");
}

bool Lexicon::isSil(const ID_t id)
{
	return isMetaWord(this->isReadonly ? id2word_readonly(id) : getVal(id));
}

bool Lexicon::isSil(const std::string word)
{
	return (word == "<sil>" || word == "sil");
}


bool Lexicon::CanAddWiPen(const ID_t wordID)
{
	return wordID != getNullWordID();
}

int Lexicon::size()
{
	return this->isReadonly ? this->wordArrayIdx_size : id2valMap.size();
}
