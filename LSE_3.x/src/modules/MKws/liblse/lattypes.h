#ifndef LATTYPES_H
#define LATTYPES_H

#include <list>
#include <vector>
#include <iomanip>
#include <iostream>

namespace lse {

enum DirectionType
{
	FWD,
	BCK
};


struct SourceType 
{
	enum Enum
	{
		Unknown,
		LVCSR,
		PHN,
		PHN_CLUSTER,
		LVCSR_PHN
	};
	static std::string ToStr(SourceType::Enum t)
	{
		if (t == Unknown) {
			return "Unknown";
		} else if (t == LVCSR) {
			return "LVCSR";
		} else if (t == PHN) {
			return "PHN";
		} else if (t == PHN_CLUSTER) {
			return "PHN_CLUSTER";
		} else if (t == LVCSR_PHN) {
			return "LVCSR_PHN";
		}
		return "Unknown";
	}
	friend std::ostream& operator<<(std::ostream& os, SourceType::Enum t)
	{
		return os << ToStr(t);
	}
};

typedef int ID_t;
#define ID_UNDEF	-1

typedef std::list< ID_t > IDList;
typedef std::vector< ID_t > IDVector;
typedef float LatTime;
typedef float LatticeLink_a;
typedef float LatticeLink_l;
typedef float Confidence;

typedef double TLatViterbiLikelihood;
typedef double TLatViterbiFwBw;
static const TLatViterbiFwBw LATVITERBIFWBW_INFINITY = 1.0e34;
static const TLatViterbiFwBw INF = 1.0e34;

//#define DBG(str)
// #define DBG(str)	cout << __FILE__ << ':' << __LINE__ << "> " << str << endl << flush
//#define DBG(str) std::cerr << std::dec << std::setprecision(13) << std::setw(20) << std::left << __FILE__ << ':' << std::setw(5) << std::right << __LINE__ << "> " << str << std::endl << std::flush
//#define DBG(str)

//#define COUT(str) std::cout << __FILE__ << ':' << std::setw(5) << __LINE__ << "> " << str << std::endl << std::flush
//#define CERR(str) std::cerr << __FILE__ << ':' << std::setw(5) << __LINE__ << "> " << str << std::endl << std::flush
//#define EXIT() CERR("EXIT...aborting application"); exit(1);
//#define CINGET() CERR("Press ENTER to continue..."); std::cin.get();

#define LATBINFILE_EXT_NODELINKSINDEX	".nlidx"
#define LATBINFILE_EXT_NODES			".nodes"
#define LATBINFILE_EXT_BESTNODES		".bestnodes"
#define LATBINFILE_EXT_FWDLINKS			".fwdlinks"
#define LATBINFILE_EXT_BCKLINKS			".bcklinks"
#define LATBINFILE_EXT_TIME				".time"
#define LATBINFILE_EXT_ALPHALAST		".alpha"
#define LATCONCATWORD					"<sil>"

#define FRAME_LENGTH 10 // in miliseconds
#define NGRAM_LENGTH 3
#define NGRAMS_HASH_SIZE 30000


#if defined(_WIN32) || defined(MINGW)
	#define PATH_SLASH						'\\'
#else
	#define PATH_SLASH						'/'
#endif

#define LSE_ERROR_IO					-1
}

#endif
