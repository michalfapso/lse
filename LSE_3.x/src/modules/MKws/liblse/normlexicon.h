#ifndef NORMLEXICON_H
#define NORMLEXICON_H

#include "lexicon.h"
#include "lattypes.h"

#define NORM_LABEL_PHN "_PHN_"
#define NORM_LABEL_FRM "_FRM_"
#define NORM_LABEL_GLOB "_GLOB_"
#define NORM_LABEL_GLOB_OOV "_GLOB_OOV_"

namespace lse {

//--------------------------------------------------
//	NormLexicon
//--------------------------------------------------
class NormLexicon
{
private:
	//--------------------------------------------------
	//	NormIndexRecord
	//--------------------------------------------------
	class NormIndexRecord 
	{
	public:
		unsigned char mCount; ///< pronounciation variants count
		int mNormPos; ///< relative position of a list of pronounciation variants from the beginning of the pronounciation array
	};
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

private:

	Lexicon *mpWordLexicon;

	float 	*mpNormArray;
	int		mNormArraySize;

public:
	float mPhnNorm;
	float mFrmNorm;
	float mGlobNorm;
	float mGlobOovNorm;

	NormLexicon(Lexicon * pWordLexicon = NULL, std::string normLexiconFilename = "");

	bool IsActive();

	void SetWordLexicon(Lexicon * pWordLexicon);
	void LoadFromNormLexiconFile(std::string filename);

	float operator[] (const ID_t wordId);
	float GetWordNormConst(const std::string &word);
};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

} // namespace lse

#endif
