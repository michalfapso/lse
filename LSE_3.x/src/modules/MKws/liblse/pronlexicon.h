#ifndef PRONLEXICON_H
#define PRONLEXICON_H

#include "lexicon.h"
#include "lattypes.h"

namespace lse {

//--------------------------------------------------
//	PronLexicon
//--------------------------------------------------
class PronLexicon
{
private:
	//--------------------------------------------------
	//	PronIndexRecord
	//--------------------------------------------------
	class PronIndexRecord 
	{
	public:
		unsigned char mCount; ///< pronounciation variants count
		int mPronPos; ///< relative position of a list of pronounciation variants from the beginning of the pronounciation array
	};
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	
public:
	//--------------------------------------------------
	//	PronVariants
	//--------------------------------------------------
	class PronVariants
	{
	public:
		//--------------------------------------------------
		//	const_iterator
		//--------------------------------------------------
		class const_iterator
		{
			const char *mpBegin;
			const char *mpCurrentPos;
			int mCount;
			int mCurrentItem;
			
		public:
			const_iterator(int count, const char* startPos, int curItem = 0);

			// Get the data element at this position
			const char* operator*() const;

			// Move position forward 1 place
			const_iterator& operator++();

			// Comparison operators
			bool operator== (const const_iterator& i) const;
			bool operator!= (const const_iterator& i) const;
		};
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private:
		const char *mpBegin;
		int mCount;

	public:
		PronVariants(int count, const char* startPos);

		PronVariants::const_iterator begin();
		PronVariants::const_iterator end();
	};
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


private:

	Lexicon *mpWordLexicon;

	char 	*mpPronArray;
	int		mPronArraySize;

	PronIndexRecord		*mpPronArrayIndex;
	int					mPronArrayIndexSize;

public:

	PronLexicon(Lexicon * pWordLexicon = NULL);
	void SetWordLexicon(Lexicon * pWordLexicon);

	void LoadFromDct(std::string filename);

	PronVariants operator[] (const ID_t wordId);
};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

} // namespace lse

#endif
