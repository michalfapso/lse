#include "latbinfile_io.h"
#include "latbinfile.h"

using namespace std;
using namespace lse;

//--------------------------------------------------------------------------------
//	Cursor
//--------------------------------------------------------------------------------

bool LatBinFile::LatNodeLinksArray::Cursor::gotoNodeID(ID_t nodeID, DirectionType dir) 
{
//	DBG("gotoNodeID("<<nodeID<<")");
	
	if (nodeID < latNodeLinksArray->firstID || 
		nodeID > latNodeLinksArray->lastID)
	{
		cerr << "ERROR: LatNodeLinksArray::Cursor::gotoNodeID("<<nodeID<<") ... nodeID out of borders ["<<latNodeLinksArray->firstID<<".."<<latNodeLinksArray->lastID<<"]"<<endl;
		exit(1);
	}
	ptr = latNodeLinksArray->array + latNodeLinksArray->arrayPointers[nodeID] - latNodeLinksArray->nodeLinksStartPos; // offset to this->array
	idx = 0;
	
//	DBG("Cursor::gotoNodeID("<<nodeID<<"): array + "<<latNodeLinksArray->arrayPointers[nodeID]<<" - "<<latNodeLinksArray->nodeLinksStartPos<<"   arraySize:"<<latNodeLinksArray->arraySize);

	LatBinFile::NodeLinks nl;
	nl.readHeaderFrom(&(ptr)); 
	if (dir == FWD) {
		linksCount = nl.fwdLinksCount;
	} else {
		linksCount = nl.bckLinksCount;
		// jump over all forward links
		ptr += nl.fwdLinksCount * sizeof(LatBinFile::NodeLinks::Link);
	}

	return true;
}

bool LatBinFile::LatNodeLinksArray::Cursor::next() {
	ptr += sizeof(LatBinFile::NodeLinks::Link);
	return (++idx < linksCount);
}

bool LatBinFile::LatNodeLinksArray::Cursor::eol() {
	DBG("eol: "<<idx<<" >= "<<linksCount);
	return (idx >= linksCount);
}

LatBinFile::NodeLinks::Link LatBinFile::LatNodeLinksArray::Cursor::getLink() {
	static LatBinFile::NodeLinks::Link tmpLink;
	tmpLink.readFrom(ptr);
	return tmpLink;
}
	
//--------------------------------------------------------------------------------
//	LatNodeLinksArray
//--------------------------------------------------------------------------------

/**
  @brief Process all forward links and get the one with the highest likelihood

  @return Outgoing link with highest likelihood
*/
ID_t LatBinFile::LatNodeLinksArray::bestFollowingNode(ID_t nodeID, DirectionType dir, TLatViterbiLikelihood * bestLikelihood) {
	ID_t bestNodeID = -1;
	*bestLikelihood = -INF;

	LatBinFile::LatNodeLinksArray::Cursor cur(this);
	cur.gotoNodeID(nodeID, dir);
	while (!cur.eol()) {
		LatBinFile::NodeLinks::Link l = cur.getLink();
		if (*bestLikelihood < l.conf) {
			*bestLikelihood = l.conf;
			bestNodeID = l.nodeID;
		}
		cur.next();
	}
	
	return bestNodeID;
}



int LatBinFile::LatNodeLinksArray::load(const string filename, ID_t startNodeID, ID_t endNodeID) {
	
	DBG("LatBinFile::LatNodeLinksArray::load("<<filename<<", "<<startNodeID<<", "<<endNodeID<<")");
	
	FILE *fin = fopen(filename.c_str(), "rb");
	if (fin == NULL) {
		return (LAT_ERROR_IO);		
	}

	string filenameIndex = filename + LATBINFILE_EXT_NODELINKSINDEX;
	FILE *finIndex = fopen(filenameIndex.c_str(), "rb");
	if (finIndex == NULL) {
		return LAT_ERROR_IO;		
	}

	// ***** INDEX FILE *****
	// get the size of the pointers file
	if ( (this->arrayPointersSize = (int)(file_size(filenameIndex.c_str()) / sizeof(long))) < 0 ) {
		cerr << "Cannot determine the size of NodeLinksIndex file" << endl << flush;
		exit(1);
	}
	DBG("arrayPointersSize:"<<this->arrayPointersSize);
	
	// load the pointers file into memory
	if (endNodeID == -1)
	{
		endNodeID = arrayPointersSize-1;
	}
	this->arrayPointers = new long[this->arrayPointersSize];
	fread(this->arrayPointers, sizeof(long), this->arrayPointersSize, finIndex);
	fclose(finIndex);
	DBG("nodeLinksArray.arrayPointersSize: "<<this->arrayPointersSize);
	DBG("endNodeID: "<<endNodeID);
	long startPos = this->arrayPointers[startNodeID];
	long endPos = 0;
	DBG("startPos:"<<startPos<<" endPos:"<<endPos);
	// if endNodeID == last nodeID in lattice, then don't load the last node
	if (endNodeID == (ID_t)arrayPointersSize-1 || endNodeID == -1) {
		long fsize;
		if ( (fsize = file_size(filename.c_str())) < 0 ) {
			cerr << "Cannot determine the size of NodeLinks file" << endl << flush;
			exit(1);
		}
		endPos = fsize;
		DBG("endPos = fsize = "<<fsize);
	} else {
		endPos = this->arrayPointers[endNodeID+1];
//		endPos = this->arrayPointers[endNodeID];
		DBG("endPos = "<<endPos);
	}
	this->arraySize = endPos - startPos;
	this->array = new unsigned char[this->arraySize];
	this->nodeLinksStartPos = startPos;
	DBG("nodeLinksArray.size: "<<this->arraySize);

	// ***** NODE LINKS FILE *****
	DBG("Seeking to startPos:"<<startPos);
	fseek(fin, startPos, SEEK_SET); 			// jump to the first nodelinks record which will be read
	fread(this->array, (this->arraySize), 1, fin);	// read all node links from startPos to endPos into this->array


	// Getting center node ID
	// Binary searching in pointer array
/*	int left = 0;
	int right = this->arrayPointersSize - 1;

	while (left <= right) {
		int middle = (left + right) / 2;

		if (centerNodePosition > this->arrayPointers[middle]) {
			left = middle + 1;
		} else if (centerNodePosition < this->arrayPointers[middle]) {
			right = middle - 1;
		} else {
			*centerNodeID = middle;
			break;
		}
	}
*/

	fclose(fin);

//	this->firstID = *(reinterpret_cast<ID_t*>(this->array));
//	this->lastID = this->firstID + nodesCount - 1;
	this->firstID = startNodeID;
	this->lastID = endNodeID;

	DBG("firstID: "<<this->firstID<<"   lastID: "<<this->lastID);
	
	return 0;
}


/*
long LatBinFile::LatNodeLinksArray::getNodePositionInNodesFile(ID_t nodeID) {

	return nodeID * nodeRecordWidth;
}
*/

bool LatBinFile::LatNodeLinksArray::containsID(ID_t nodeID) {

	if (nodeID < this->firstID || nodeID > this->lastID) { // if we are going to jump out of read lattice
		return false;
	} else {
		return true;
	}
}

void LatBinFile::LatNodeLinksArray::printLinks(std::ostream &out, ID_t nodeID, DirectionType direction) {
		
	LatBinFile::LatNodeLinksArray::Cursor cur(this);
	cur.gotoNodeID(nodeID, direction);
	
	while (!cur.eol()) {
		LatBinFile::NodeLinks::Link l = cur.getLink();
		out << "  " << (direction==FWD ? "E=" : "S=") << l.nodeID << " p=" << l.conf << endl;
		cur.next();
	}
}

int LatBinFile::LatNodeLinksArray::getFwdLinksCount(ID_t nodeID) {
	unsigned char *ptr = this->array + this->arrayPointers[nodeID] - this->nodeLinksStartPos; // offset to this->array
	LatBinFile::NodeLinks nl;
	nl.readHeaderFrom(&ptr);
	return nl.fwdLinksCount;
}

int LatBinFile::LatNodeLinksArray::getBckLinksCount(ID_t nodeID) {
	unsigned char *ptr = this->array + this->arrayPointers[nodeID] - this->nodeLinksStartPos; // offset to this->array
	LatBinFile::NodeLinks nl;
	nl.readHeaderFrom(&ptr);
	return nl.bckLinksCount;
}

void LatBinFile::LatNodeLinksArray::free() 
{
	if (this->array != NULL) {
		delete[] this->array;
		this->array = NULL;
	}
	if (this->arrayPointers != NULL) {
		delete[] this->arrayPointers;
		this->arrayPointers = NULL;
	}
	this->arraySize = 0;
	this->arrayPointersSize = 0;
	this->firstID = 0;
	this->lastID = 0;
}

long LatBinFile::LatNodeLinksArray::size() 
{
	return this->arraySize;
}


long LatBinFile::LatNodeLinksArray::linksCount()
{
//--------------------------------------------------
//	WARNING: not well tested
//--------------------------------------------------

//	DBG("(("<<this->arraySize<<" - ("<<this->arrayPointersSize<<"-1) * "<<LatBinFile::NodeLinks::getHeaderSize()<<") / "<<sizeof(LatBinFile::NodeLinks::Link)<<" / 2"); // division by 2 because of we have forward and backward links
	return (long)((this->arraySize - (this->arrayPointersSize-1) * LatBinFile::NodeLinks::getHeaderSize()) / sizeof(LatBinFile::NodeLinks::Link) / 2); // division by 2 because of we have forward and backward links
}
