
#include "mymath.h"
#include <cmath>

using namespace lse;
using namespace std;


double lse::logAdd(double a, double b, float treshold) {
//	return (a>b) ? a : b;
	if (a < b) {
		double tmp = a;
		a = b;
		b = tmp;
	}
	double diff = b - a;
	if (-diff > treshold) {
		return a;
	}
        return log(1.0 + exp(diff)) + a;
//      return log(exp(a) + exp(b));
}
