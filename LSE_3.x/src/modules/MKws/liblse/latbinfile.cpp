#include "latbinfile.h"

using namespace std;
using namespace lse;





//static const linkFileRecordWidth = sizeof(ID_t) + sizeof(ID_t) + sizeof(LatticeLink_a) + sizeof(LatticeLink_l);



/**
  @brief Converts LatBinFile::Link to Lattice::Link

  @param startNodeID Link's start node id 
  @param dir Direction (forward, backward)

  @return Converted link
*/
/*
Lattice::Link LatBinFile::Link::toLatticeLink(ID_t startNodeID, Direction dir) {
	Lattice::Link tmpLatLink;
	if (dir == forward) {
		tmpLatLink.S = startNodeID;
		tmpLatLink.E = nodeID;
	} else {
		tmpLatLink.E = startNodeID;
		tmpLatLink.S = nodeID;
	}
	tmpLatLink.id = id;
	tmpLatLink.likelihood = likelihood;

	return tmpLatLink;
}
*/


/**
  @brief Copy constructor

  @param &nl Instance to copy from

  @return 
*/
LatBinFile::NodeLinks::NodeLinks(const NodeLinks &nl) {
	id = nl.id;
	fwdLinksCount = nl.fwdLinksCount;
	bckLinksCount = nl.bckLinksCount;
	for(vector<LatBinFile::Link>::const_iterator i=nl.fwdLinks.begin(); i!=nl.fwdLinks.end(); ++i) {
		fwdLinks.push_back(*i);
	}
	for(vector<LatBinFile::Link>::const_iterator i=nl.bckLinks.begin(); i!=nl.bckLinks.end(); ++i) {
		bckLinks.push_back(*i);
	}

/*		int memSize = linkArrayRecordWidth * (fwdLinksCount + bckLinksCount);
	links = new unsigned char[memSize];
	DBG("MEMSIZE:" << memSize << " LINKS:" << (long)links << " COPYLINKS:" << (long)nl.links << " fwd:" << fwdLinksCount << " bck:" << bckLinksCount);
	memcpy(links, nl.links, memSize);
*/		
}

/*
void LatBinFile::NodeLinks::free()
{
	if (fwdLinks != NULL) {
		delete fwdLinks;
		fwdLinks = NULL;
	}
	
	if (bckLinks != NULL) {
		delete bckLinks;
		bckLinks = NULL;
	}
}
*/

/*
void LatBinFile::writeNodeLinks(LatBinFile::NodeLinks &nl, ofstream &out)
{
	// id
	out.write(reinterpret_cast<const char *>(&nl.id), sizeof(nl.id));
	// fwdLinks count
	int count = nl.fwdLinks->size();
	out.write(reinterpret_cast<const char *>(&count), sizeof(count));
	// bckLinks count
	count = nl.isFirstNode ? 1 : nl.bckLinks->size();
	out.write(reinterpret_cast<const char *>(&count), sizeof(count));
	

	for (vector<ID_t>::iterator j=nl.fwdLinks->begin(); j!=nl.fwdLinks->end(); ++j) 
	{
		Lattice::Link link = lat->links[*j];
		ID_t E_transposed = link.E + this->lastInsertedNodeID + 1; // +1 because current lattice's nodes are starting from lastInsertedNodeID + 1
		fwrite(&E_transposed, sizeof(E_transposed), 1, f_outNodeLinks);
//			fwrite(&(link.likelihood), sizeof(link.likelihood), 1, f_outNodeLinks);
		fwrite(&(link.likelihood_noScaleOnKeyword), sizeof(link.likelihood_noScaleOnKeyword), 1, f_outNodeLinks);
	}
	// backward links IDs list ... if we are on the first node which has no back link, we have to add one to the preceding lattice's end node
	if (i == lat->nodes.begin()) 
	{			
		TLatViterbiLikelihood lik = 0.0;					// this is the only possible link (path)
		ID_t S_transposed = this->lastInsertedNodeID; 
		fwrite(&S_transposed, sizeof(S_transposed), 1, f_outNodeLinks);
		fwrite(&lik, sizeof(lik), 1, f_outNodeLinks);
	} 
	// backward links (like forward)
	for (vector<ID_t>::iterator j=lat->bckLinks[nodeID].begin(); j!=lat->bckLinks[nodeID].end(); ++j) 
	{			
		Lattice::Link link = lat->links[*j];
		ID_t S_transposed = link.S + this->lastInsertedNodeID + 1; // +1 because current lattice's nodes are starting from lastInsertedNodeID + 1
		fwrite(&(S_transposed), sizeof(S_transposed), 1, f_outNodeLinks);
//			fwrite(&(link.likelihood), sizeof(link.likelihood), 1, f_outNodeLinks);
		fwrite(&(link.likelihood_noScaleOnKeyword), sizeof(link.likelihood_noScaleOnKeyword), 1, f_outNodeLinks);
	}


	
	out.write(reinterpret_cast<const char *>(&), sizeof());
	out.write(reinterpret_cast<const char *>(&), sizeof());
	out.write(reinterpret_cast<const char *>(&), sizeof());
	out.write(reinterpret_cast<const char *>(&), sizeof());
	out.write(reinterpret_cast<const char *>(&), sizeof());
}
*/
/**
  @brief Process all forward links and get the one with the highest likelihood

  @return Outgoing link with highest likelihood
*/
LatBinFile::Link* LatBinFile::NodeLinks::bestFwdLink() {
	LatBinFile::Link* iBestLink = NULL;
	TLatViterbiLikelihood bestLikelihood = -INF;

	if (fwdLinks.size() == 0) {
//		DBG("fwdLinks.size() == 0 ... bestFwdLink() return value = NULL");
		return NULL;
	}

	for(LatBinFile::FwdLinks::iterator i=this->fwdLinks.begin(); i!=this->fwdLinks.end(); ++i) {
		if (i->likelihood > bestLikelihood) {
			bestLikelihood = i->likelihood;
			iBestLink = &(*i);
		}
	}
	return iBestLink;
}



/**
  @brief Process all backward links and get the one with the highest likelihood

  @return ingoing link with highest likelihood
*/
LatBinFile::Link* LatBinFile::NodeLinks::bestBckLink() {
	LatBinFile::Link* iBestLink = NULL;
	TLatViterbiLikelihood bestLikelihood = -INF;

	if (bckLinks.size() == 0)
		return NULL;
	
	for(LatBinFile::BckLinks::iterator i=bckLinks.begin(); i!=bckLinks.end(); ++i) {
		if (i->likelihood > bestLikelihood) {
			bestLikelihood = i->likelihood;
			iBestLink = &(*i);
		}
	}
	return iBestLink;
}
/**
  @brief Get node with specific ID from array LatBinFile::nodes. 
  			In nodes is part of all nodes in given time interval

  @param startNodeID First node's ID in nodes
  @param id Which node we want to get

  @return Node with given ID
**********************************************************************************/
/*
   Lattice::Node LatBinFile::getNodeFromNodesArray(ID_t startNodeID, ID_t id) {

	unsigned char * pos = nodes + ((id - startNodeID) * NodesArray::nodeRecordWidth);
//	DBG("startid: " << startNodeID << "pos: " << (long)(pos-nodes) << "\tnodeID: " << id);
	
	Lattice::Node res;
	res.id = *((ID_t*)pos);
	pos += sizeof(ID_t);
	
//	DBG("pos:" << (long)(pos-nodes) << " W:" << (int)(*(pos)) << (int)(*(pos+1)) << (int)(*(pos+2)) << (int)(*(pos+3)));
	ID_t res.W_id = *((ID_t*)pos);
//	DBG("wordID:" << wordID);
	res.W = mpLexicon->getVal( W_id );
//	DBG("WordID: " << (ID_t)(*((ID_t*)pos))); // - (long)nodes);
	pos += sizeof(ID_t);
		
	res.t = *((LatTime*)pos);
	pos += sizeof(LatTime);
	
	res.v = *((int*)pos);
	pos += sizeof(int);

	res.p = *((float*)pos);
	pos += sizeof(float);
				
	
//	DBG("getNodeFromNodesArray> " << res);
//	DBG("--------------------------------------------------");
	return res;
	
}
*/



/*
void LatBinFile::readNodesRecord(FILE *fin, LatBinFile::Node &node) {

	// node ID	
	fread(&(node.id), sizeof(node.id), 1, fin);
	// word ID
	fread(&(node.W_id), sizeof(node.W_id), 1, fin);
	// start time
	fread(&(node.tStart), sizeof(node.t), 1, fin);
	// end time
	fread(&(node.t), sizeof(node.t), 1, fin);
	// posteriors
	fread(&(node.p), sizeof(node.p), 1, fin);
	// best backward nodeID
	fread(&(node.bestBckNodeID), sizeof(node.bestBckNodeID), 1, fin);
	// best forward nodeID
	fread(&(node.bestFwdNodeID), sizeof(node.bestFwdNodeID), 1, fin);
}


void LatBinFile::writeNodesRecord(FILE *fin, LatBinFile::Node &node) {
	// node ID	
	fwrite(&(node.id), sizeof(node.id), 1, fin);
	// word ID
	fwrite(&(node.W_id), sizeof(node.W_id), 1, fin);
	// start time
	fwrite(&(node.tStart), sizeof(node.t), 1, fin);
	// end time
	fwrite(&(node.t), sizeof(node.t), 1, fin);
	// posteriors
	fwrite(&(node.p), sizeof(node.p), 1, fin);
	// best backward nodeID
	fwrite(&(node.bestBckNodeID), sizeof(node.bestBckNodeID), 1, fin);
	// best forward nodeID
	fwrite(&(node.bestFwdNodeID), sizeof(node.bestFwdNodeID), 1, fin);
}
*/


/**
  @brief Reads link from links file and stores it into link param

  @param &gz_in Gzipped file
  @param &link Link read from file
**********************************************************************************/
void LatBinFile::readLinksRecord(FILE *fin, LatBinFile::Link &link) {
	
	// target node ID
	fread(&(link.nodeID), sizeof(link.nodeID), 1, fin);
	// likelihood
	fread(&(link.likelihood), sizeof(link.likelihood), 1, fin);
}



/**
  @brief Reads node's links record from file and stores it into nodeLinks param

  @param &gz_in File to read from
  @param &nodeLinks Read values are stored here
**********************************************************************************/
void LatBinFile::readNodeLinksRecord(FILE *fin, LatBinFile::NodeLinks *nodeLinks) {

	LatBinFile::Link tmpLink;

	// node ID
	fread(&(nodeLinks->id), sizeof(nodeLinks->id), 1, fin);
	// forward links count
	fread(&(nodeLinks->fwdLinksCount), sizeof(nodeLinks->fwdLinksCount), 1, fin);
	// backward links count
	fread(&(nodeLinks->bckLinksCount), sizeof(nodeLinks->bckLinksCount), 1, fin);
	
	nodeLinks->fwdLinks.clear();
	for (int i=0; i<nodeLinks->fwdLinksCount; i++) {
		
		readLinksRecord(fin, tmpLink);
		nodeLinks->fwdLinks.push_back(tmpLink);
	}

	nodeLinks->bckLinks.clear();
	for (int i=0; i<nodeLinks->bckLinksCount; i++) {

		readLinksRecord(fin, tmpLink);
		nodeLinks->bckLinks.push_back(tmpLink);
	}
	
	
}


/**
@brief Clean all output files. Useful for lattice concatenation, when data are 
       appended to these files. So before first writing just call this method.

@param filename Output file name (*.binlat)
*/
void LatBinFile::cleanOutputFiles(const string filename) {

	ofstream mkfile;
	mkfile.open(filename.c_str()); 	// clean .binlat
	mkfile.close();

	string str;

	str = filename + LATBINFILE_EXT_NODELINKSINDEX;
	mkfile.open(str.c_str()); 		// clean .binlat.nlidx
	mkfile.close();
	
	str = filename + LATBINFILE_EXT_NODES;
	mkfile.open(str.c_str()); 		// clean .binlat.nodes
	mkfile.close();

	str = filename + LATBINFILE_EXT_TIME;
	mkfile.open(str.c_str()); 		// clean .binlat.time
	mkfile.close();
}



/**
  @brief Read nodes from binary file and write those of them 
  		 which belong to the best path into another file.
		 This way the best path is created, but it has a similar
		 structure like complete lattice file

  @param filename Lattice with .binlat extension

  @return Zero if succeed
*/
int LatBinFile::saveAllBestPathNodes(const string filename) {

	DBG("!!! LatBinFile::saveAllBestPathNodes() IS NOT IMPLEMENTED !!!");
	exit(1);
/*	NodesArray latNodesArray;

	int nodesCount = latNodesArray.getNodesFileItemsCount(filename);
	latNodesArray.load(filename + LATBINFILE_EXT_NODES, 0, nodesCount);
	DBG("nodesCount: "<<nodesCount);

	string filename_bestpath = filename + LATBINFILE_EXT_BESTNODES;
	FILE *fout = fopen(filename_bestpath.c_str(), "wb");
	if (fout == NULL) {
		return LSE_ERROR_IO;
	}
	
	LatBinFile::Node node;
	int i = 0;
	while (i < nodesCount) {
		node = latNodesArray[i];
		DBG("node: "<<node);
		writeNodesRecord(fout, node);
		// jump out if the last node has been read
		if ((i = node.bestFwdNodeID) == 0) {
			DBG("breaking");
			break;
		}
	}
	DBG("i:"<<i<<" nodesCount:"<<nodesCount);

	fclose(fout);
	*/
	return 0;
}

/**
  @brief Save lattice attached to this class (LatBinFile) into binary format

  @param filename Name of file in which we want to store lattice

  @return LATBINFILE_ERROR_IO if any file operation fault occured
*/
int LatBinFile::save(
		const string filename, 
		const string meeting_rec, 
		bool append) 
{
	DBG("LatBinFile::save("<<filename<<", "<<meeting_rec<<")");
	ios_base::openmode filemode = (append ? ios_base::app | ios_base::binary : ios_base::out | ios_base::binary);

	ofstream f_outNodeLinks(filename.c_str(), filemode);
	if (!f_outNodeLinks.good()) {
		CERR("ERROR: Cannot open file for writing: "<<filename);
		EXIT();
	}

	string str;

	str = filename + LATBINFILE_EXT_NODELINKSINDEX;
	DBG("f_outNodeLinksIndex:"<<str);
	ofstream f_outNodeLinksIndex(str.c_str(), filemode);
	if (!f_outNodeLinksIndex.good()) {
		CERR("ERROR: Cannot open file for writing: "<<str);
		EXIT();
	}

	str = filename + LATBINFILE_EXT_NODES;
	ofstream f_outNodes(str.c_str(), filemode);
	if (!f_outNodes.good()) {
		CERR("ERROR: Cannot open file for writing: "<<str);
		EXIT();
	}

	str = filename + LATBINFILE_EXT_TIME;
	ofstream f_outTime(str.c_str(), filemode);
	if (!f_outTime.good()) {
		CERR("ERROR: Cannot open file for writing: "<<str);
		EXIT();
	}

	str = filename + LATBINFILE_EXT_ALPHALAST;
	ofstream f_outAlphaLast(str.c_str(), filemode);
	if (!f_outAlphaLast.good()) {
		CERR("ERROR: Cannot open file for writing: "<<str);
		EXIT();
	}
	
	bool write_nodelinks_dbg = true;

	ofstream f_dbg;
	if (write_nodelinks_dbg)
	{
		str = filename + ".nodelinks_dbg";
		f_dbg.open(str.c_str(), ios_base::app);
	}

	DBG("opening files...done");
	// we need to store the endNodeID, because the next block (if (append) { ... }) 
	// will add a link to it, so it will not look like the end node, which should have no forward links
	ID_t endNodeID = lat->lastNode().id; 

	// add connection link between last node in lattice, which is going to be appended now, and <sil> node between two concatenated lattices
	if (append) {
		ID_t lastLinkIndex = ((lat->links.size() > 0) ? ((--lat->links.end())->first) : (-1));
		ID_t lastNodeIndex = ((lat->nodes.size() > 0) ? ((--lat->nodes.end())->first) : (-1));
		
		Lattice::Link tmpLink;
		tmpLink.id = ++lastLinkIndex;
		tmpLink.S = endNodeID;
		tmpLink.E = lastNodeIndex + 1; // connection node's ID (connection node will be just added)
		tmpLink.a = 0.0;
		tmpLink.l = 0.0;
		tmpLink.likelihood = 0.0;
		tmpLink.confidence = 0.0;
		tmpLink.confidence_noScaleOnKeyword = 0.0;
		lat->addLink(tmpLink);
	}
	
	lat->updateFwdBckLinks();				// update forward and backward links lists

	LatIndexer::Record latIndexerRec;		// main searching index record
	LatMeetingIndexerRecord meetingRec;
	meetingRec.path = meeting_rec;
	meetingRec.length = (--lat->nodes.end())->second.t; // time of the last node in lattice
	DBG("mpIndexer:"<<mpIndexer);
	latIndexerRec.mMeetingID = mpIndexer->meetings.insertRecord(&meetingRec);// add index for this meeting to indexer->meetings
	LatTime predTime = this->lastInsertedTime;					// last processed node time other than the current one
	unsigned int nodesCountInCurTime = 0;	// how many nodes have the same time (used in TimeIndex)
	
	DBG("creating first node...");
	
	// if there is already some lattice written in the output file, then it's end node has a forward link to <sil> node. So we have to write this node
	DBG("this->outputFileEmpty:"<<outputFileEmpty<<" && append:"<<append);
	if (!this->outputFileEmpty && append) {
		ID_t nodeID_transposed = this->lastInsertedNodeID + 1; // +2 because currently inserted <sil> node has ID = lastIns... + 1, so the nex will be +2
		LatTime t = ((lat->nodes.begin())->second).t; // put <sil> node in first node's time of current lattice
		DBG("lastInsertedTime: "<<this->lastInsertedTime);
		LatTime tStart = this->lastInsertedTime;
		// TimeIndex
		nodesCountInCurTime = 1;			// the <sil> node
		TimeRecord tRec;
		tRec.t = predTime;
		tRec.nodeID = nodeID_transposed;
		tRec.writeTo(f_outTime);

		predTime = t; // nodesCountInCurTime will be written after first node in lattice (second node has some "nonzero" time value)
		// NodeLinksIndex
		long nodeLinksPos = file_size(filename.c_str());//f_outNodeLinks.tellp();
		DBG("f_outNodeLinks.tellp(): "<<nodeLinksPos);
		f_outNodeLinksIndex.write(reinterpret_cast<const char *>(&nodeLinksPos), sizeof(nodeLinksPos));
		// NodeLinks
		NodeLinks nl;
		nl.id = nodeID_transposed;
		nl.fwdLinksCount = 1;
		nl.bckLinksCount = 1;
		nl.writeHeaderTo(f_outNodeLinks);
		//   ... forward link
		NodeLinks::Link l;
		l.nodeID = this->lastInsertedNodeID + 2; // +2 because currently inserted <sil> node has ID = lastIns... + 1, so the nex will be +2
		l.conf = 0.0;
		l.writeTo(f_outNodeLinks);
		//   ... backward link
		l.nodeID = this->lastInsertedNodeID; // because it is the ID of previous lattice's end node
		l.conf = 0.0;
		l.writeTo(f_outNodeLinks);
		// Nodes
		LatBinFile::Node n;
		n.id = this->lastInsertedNodeID + 1;
		n.W_id = mpLexicon->GetUnitId(LATCONCATWORD);
		n.tStart = tStart;
		n.t = t;
		n.conf = 0.0;
		n.alpha = this->lastInsertedAlpha; // this block is executed only if the binary file is not empty, so there is already something in lastInsertedAlpha
		n.beta = 0.0; // because this node is actually the very last node of the previous lattice
		n.writeTo(f_outNodes);

		/*
		// it is not needed to add this to the forward index
		if (createFwdIndex)
		{
			latIndexerRec.mWordID = n.W_id;//getValID(LATCONCATWORD);
	//		latIndexerRec.position = ftell(f_outNodeLinks);
			latIndexerRec.mNodeID = nodeID_transposed;
			latIndexerRec.mConf = 0.0;
			latIndexerRec.mStartTime = tStart;
			latIndexerRec.mEndTime = t;
	//		DBG("latIndexerRec.nodeID:"<< latIndexerRec.nodeID);
			//mpIndexer->fwdIndex.addRecord(latIndexerRec); // add node to search index
			(*pfAddToFwdIndex)(pPtr, latIndexerRec);
		}
		*/

		if (write_nodelinks_dbg)
		{
			f_dbg << n << endl;
		}
		
		AlphaLastRecord alRec;
		alRec.nodeID = n.id;
		alRec.alphaLast = n.alpha;
		alRec.writeTo(f_outAlphaLast);

		this->lastInsertedNodeID++;
	}

//	DBG("creating first node...done");
	
	// write all nodes into MEETING and NODES files and insert records to searching index file
	for(Lattice::NodeMap::iterator i=lat->nodes.begin(); i!=lat->nodes.end(); ++i) {
		
		ID_t nodeID = (i->second).id;
		ID_t nodeID_transposed = nodeID + this->lastInsertedNodeID + 1; // +1 because current lattice's nodes are starting from lastInsertedNodeID + 1
		if (write_nodelinks_dbg)
		{
			f_dbg << i->second << endl;
		}
		// ***** TIME INDEX *****
		// WARNING: modifying anything in writing to files may cause problems in other methods of LatBinFile class
		//
		// add new time record
		if (predTime != (i->second).t && abs((i->second).t - predTime) > TIME_INDEX_RECORD_FREQUENCY) {
			predTime = (i->second).t;
			TimeRecord tRec;
			tRec.t = (i->second).t;
			tRec.nodeID = nodeID_transposed;
			tRec.writeTo(f_outTime);
		}

		nodesCountInCurTime++;		

		// ***** NODE LINKS INDEX FILE *****
		// WARNING: modifying anything in writing to files may cause problems in other methods of LatBinFile class
		// 
		long nodeLinksPos = f_outNodeLinks.tellp();
		// node ID - we don't need to write also nodeID, because they starts always from 0 and are increasing by 1
//		fwrite(f_outNodeLinksIndex, reinterpret_cast<char *>(&nodeID), sizeof(nodeID));
		// position of nodeLinks in nodeLinks data file
		f_outNodeLinksIndex.write(reinterpret_cast<const char *>(&nodeLinksPos), sizeof(nodeLinksPos));
		
		// ***** NODE LINKS FILE *****
		// WARNING: modifying anything in writing to files may cause problems in other methods of LatBinFile class
		// 
		NodeLinks nl;
		nl.id = nodeID_transposed;
		nl.fwdLinksCount = lat->fwdLinks[nodeID].size();
		nl.bckLinksCount = (i == lat->nodes.begin() && !outputFileEmpty) ? 1 : lat->bckLinks[nodeID].size();
		nl.writeHeaderTo(f_outNodeLinks);
		// forward links IDs list ... if we are on the last node which has no fwd link, we have to add one to the following lattice's start node
		for (vector<ID_t>::iterator j=lat->fwdLinks[nodeID].begin(); j!=lat->fwdLinks[nodeID].end(); ++j) 
		{
			NodeLinks::Link l;
			Lattice::Link latLink = lat->links[*j];
			l.nodeID = latLink.E + this->lastInsertedNodeID + 1; // +1 because current lattice's nodes are starting from lastInsertedNodeID + 1
			l.conf = latLink.confidence_noScaleOnKeyword;
//			l.conf = latLink.likelihood;
//			DBG("writing link... nodeID:"<<l.nodeID<<" conf:"<<l.conf);
			l.writeTo(f_outNodeLinks);
		}
		// backward links IDs list ... if we are on the first node which has no back link, we have to add one to the preceding lattice's end node
		if (i == lat->nodes.begin() && !this->outputFileEmpty) 
		{			
			NodeLinks::Link l;
			l.nodeID = this->lastInsertedNodeID; 
			l.conf = 0.0;
			l.writeTo(f_outNodeLinks);
		} 
		// backward links (like forward)
		for (vector<ID_t>::iterator j=lat->bckLinks[nodeID].begin(); j!=lat->bckLinks[nodeID].end(); ++j) 
		{	
			NodeLinks::Link l;
			Lattice::Link latLink = lat->links[*j];
			l.nodeID = latLink.S + this->lastInsertedNodeID + 1; // +1 because current lattice's nodes are starting from lastInsertedNodeID + 1
			l.conf = latLink.confidence_noScaleOnKeyword;
//			l.conf = latLink.likelihood;
//			DBG("writing link... nodeID:"<<l.nodeID<<" conf:"<<l.conf);
			l.writeTo(f_outNodeLinks);
		}
	
		
		// ***** NODES FILE *****
		// WARNING: modifying anything in writing to files may cause problems in other methods of LatBinFile class
		// 
		// node ID
		LatBinFile::Node n;
		n.id = nodeID_transposed;
		n.W_id = mpLexicon->GetUnitId((i->second).W);
		n.tStart = (i->second).tStart;
		n.t = (i->second).t;
		n.conf = (i->second).bestLikelihood;
		n.alpha = (i->second).alpha;
		n.beta = (i->second).beta;
		n.writeTo(f_outNodes);
		
		/*
		if(createFwdIndex)
		{
			//add word to lexicon if there isn't this word already
			latIndexerRec.mWordID = n.W_id;//getValID((i->second).W);
	//		latIndexerRec.position = ftell(f_outNodeLinks);
			latIndexerRec.mNodeID = nodeID_transposed;
			latIndexerRec.mConf = (i->second).bestLikelihood; 
			latIndexerRec.mStartTime = (i->second).tStart;
			latIndexerRec.mEndTime = (i->second).t;
			//if node != "!NULL" => add node to time index and search index
			if (latIndexerRec.mWordID != mpLexicon->GetUnitId("!NULL")) { //getValID("!NULL")) {
				(*pfAddToFwdIndex)(pPtr, latIndexerRec);
//				mpIndexer->fwdIndex.addRecord(latIndexerRec); // add node to search index
			}
		}
		*/
		i->second.W_id = n.W_id;
	}
	this->lastInsertedAlpha = lat->nodes[endNodeID].alpha;
	this->lastInsertedTime = lat->nodes[endNodeID].t;
	this->lastInsertedNodeID += lat->nodes.size();
	
	DBG("lastInsertedNodeID = "<<lastInsertedNodeID<<" + "<<lat->nodes.size());
	
	f_outNodeLinks.close();
	f_outNodeLinksIndex.close();
	f_outNodes.close();
	f_outTime.close();
	f_outAlphaLast.close();
	if (write_nodelinks_dbg)
	{
		f_dbg.close();
	}
	this->outputFileEmpty = false;
	return 0;
}


int LatBinFile::closeConcatenatedLatticeOutput(const string filename, const string meeting_rec, bool createFwdIndex) {
		
	ios_base::openmode filemode = ios_base::app | ios_base::binary;
	
	ofstream f_outNodeLinks(filename.c_str(), filemode);
	if (!f_outNodeLinks.good()) {
		return LATBINFILE_ERROR_IO;		
	}

	string str;

	str = filename + LATBINFILE_EXT_NODELINKSINDEX;
	ofstream f_outNodeLinksIndex(str.c_str(), filemode);
	if (!f_outNodeLinksIndex.good()) {
		return LATBINFILE_ERROR_IO;
	}

	str = filename + LATBINFILE_EXT_NODES;
	ofstream f_outNodes(str.c_str(), filemode);
	if (!f_outNodes.good()) {
		return LATBINFILE_ERROR_IO;		
	}

	str = filename + LATBINFILE_EXT_TIME;
	ofstream f_outTime(str.c_str(), filemode);
	if (!f_outTime.good()) {
		return LATBINFILE_ERROR_IO;		
	}

	str = filename + LATBINFILE_EXT_ALPHALAST;
	ofstream f_outAlphaLast(str.c_str(), filemode);
	if (!f_outAlphaLast.good()) {
		return LATBINFILE_ERROR_IO;		
	}

	LatIndexer::Record latIndexerRec;		// main searching index record
	LatMeetingIndexerRecord meetingRec;
	meetingRec.path = meeting_rec;
	meetingRec.length = this->lastInsertedTime;
	latIndexerRec.mMeetingID = mpIndexer->meetings.insertRecord(&meetingRec);// add index for this meeting to indexer->meetings
	LatTime predTime = this->lastInsertedTime;	// last processed node time other than the current one
	unsigned int nodesCountInCurTime = 0;	// how many nodes have the same time (used in TimeIndex)

	ID_t nodeIDtransposed = this->lastInsertedNodeID + 1; 
	LatTime t = this->lastInsertedTime + 0.01; 
	LatTime tStart = this->lastInsertedTime;
	// TimeIndex
	nodesCountInCurTime = 1;			// the <sil> node
	TimeRecord tRec;
	tRec.t = predTime;
	tRec.nodeID = nodeIDtransposed;
	tRec.writeTo(f_outTime);

	predTime = t; // nodesCountInCurTime will be written after first node in lattice (second node has some "nonzero" time value)
	// NodeLinksIndex
	long nodeLinksPos = f_outNodeLinks.tellp();
	f_outNodeLinksIndex.write(reinterpret_cast<const char *>(&nodeLinksPos), sizeof(nodeLinksPos));
	// NodeLinks
	NodeLinks nl;
	nl.id = nodeIDtransposed;
	nl.fwdLinksCount = 0;
	nl.bckLinksCount = 1;
	nl.writeHeaderTo(f_outNodeLinks);
	//   ... forward link ... none
	//   ... backward link
	NodeLinks::Link l;
	l.nodeID = this->lastInsertedNodeID;
	l.conf = 0.0;
	l.writeTo(f_outNodeLinks);
	// Nodes
	LatBinFile::Node n;
	n.id = this->lastInsertedNodeID + 1;
	n.W_id = mpLexicon->GetUnitId(LATCONCATWORD);
	n.tStart = tStart;
	n.t = t;
	n.conf = 0.0;
	n.alpha = this->lastInsertedAlpha;
	n.beta = 0.0; ///< ???
	n.writeTo(f_outNodes);

	AlphaLastRecord alRec;
	alRec.nodeID = n.id;
	alRec.alphaLast = n.alpha;
	alRec.writeTo(f_outAlphaLast);

	/*
	// it is not needed to add this to the forward index
	if (createFwdIndex)
	{
		// LatIndexer
		latIndexerRec.mWordID = n.W_id;
		latIndexerRec.mNodeID = nodeIDtransposed;
		latIndexerRec.mConf = 0.0;
		latIndexerRec.mStartTime = tStart;
		latIndexerRec.mEndTime = t;
		DBG("latIndexerRec.nodeID:"<< latIndexerRec.mNodeID);
		mpIndexer->fwdIndex.addMeeting(1);
		mpIndexer->fwdIndex.addRecord(latIndexerRec); // add node to search index
		mpIndexer->fwdIndex.closeMeeting();
	}
	*/
	this->lastInsertedNodeID++;

	return 0;
}



int LatBinFile::convertToTextFile(const string filename) {
	
	DBG("LatBinFile::convertToTextFile()");
	LatNodeLinksArray nodeLinksArray_tmp;
	NodesArray nodesArray_tmp;
	// ***** READ NODE LINKS *****
	// 
	// seek to node at time startTime
	ID_t startNodeID = 0;
	ID_t endNodeID = -1; // == infinity == end node in binary lattice
/*	DBG("getNodeLinksTimeBorders()");
	if (getNodeLinksTimeBorders(filename + LATBINFILE_EXT_TIME, startTime, endTime, &nodesCount, &startNodeID, &endNodeID) == LATBINFILE_ERROR_IO) 
		return LATBINFILE_ERROR_IO;
	DBG("getNodeLinksTimeBorders()...done");
*/	
	//DBG("nodesCount:"<<nodesCount);

	// read all records until endTime
	DBG("nodeLinksArray_tmp.load("<<filename<<", "<<startNodeID<<", "<<endNodeID<<")");
	nodeLinksArray_tmp.load(filename, startNodeID, endNodeID);
	DBG("nodeLinksArray_tmp.size(): "<<nodeLinksArray_tmp.size());
	DBG("Getting first and last node ID");
	DBG("ID_first:" << startNodeID << " ID_last:" << endNodeID);

	// ***** READ NODES *****
	//
	DBG("Reading nodes...");
	DBG("nodesArray_tmp.load("<<filename + LATBINFILE_EXT_NODES<<", "<<startNodeID<<", "<<endNodeID<<")");
	if (nodesArray_tmp.load(filename + LATBINFILE_EXT_NODES, startNodeID, endNodeID) != 0) {
		return LATBINFILE_ERROR_IO;
	}
	DBG("nodesArray_tmp.load...done");
	DBG("nodesArray_tmp.size:"<<nodesArray_tmp.size());

	string filename_out;
	
	// SAVE NODES
	filename_out = filename + LATBINFILE_EXT_NODES + ".txt";
	ofstream outNodes(filename_out.c_str());
	
	// SAVE NODELINKS
	filename_out = filename + ".txt";
	ofstream out(filename_out.c_str());
	// write header info (N,L)
	out << "N=" << endNodeID - startNodeID + 1 << endl;
	// write all nodes (with wordID instead of word)
	
//	startNodeID = 6229;
//	endNodeID = 6229;
	for(int i=nodesArray_tmp.firstID; i<=nodesArray_tmp.lastID; ++i) {
		LatBinFile::Node n = nodesArray_tmp[i];
		outNodes << n << endl;
//		cout << n << endl;
		out << "I="	<< n.id << "\tbckLinks=" << nodeLinksArray_tmp.getBckLinksCount(i) << "\tfwdLinks=" << nodeLinksArray_tmp.getFwdLinksCount(i) << "\ttStart=" << n.tStart << "\tt=" << n.t << "\tW_id=" << n.W_id << "\tp=" << n.conf << endl << flush;
		nodeLinksArray_tmp.printLinks(out, i, BCK);
		nodeLinksArray_tmp.printLinks(out, i, FWD);
	}
	
	out.close();
	outNodes.close();
	
	return 0;
}

/**
  @brief Load part of lattice from binary file

  @param filename 			Lattice nodelinks file's name
  @param centerNodePosition Position of center node in nodelinks file
  @param centerNodeID 		In this parameter is stored center node's id (center node is given by centerNodePosition)
  @param startTime 			Start time of part of lattice we want to load
  @param endTime 			End time of part of lattice we want to load
  @param fwdNodesCount 		How far from center node (forward direction) will be the last loaded node
  @param bckNodesCount 		How far from center node (backward direction) will be the last loaded node

  @return LATBINFILE_ERROR_IO if any file operation fault occured
*/
int LatBinFile::load(const string filename, LatTime startTime, LatTime endTime) {
	
	DBG("LatBinFile::load("<<filename<<")");
	if (startTime<0) startTime = 0;
	
	string str;
	
/*	str = filename + LATBINFILE_EXT_NODES;
	FILE *f_inNodes = fopen(str.c_str(), "rb");
	if (f_inNodes == NULL) {
		return (LATBINFILE_ERROR_IO);
	}
*/
	// ***** READ NODE LINKS *****
	// 
	// seek to node at time startTime
	DBG("getNodeLinksTimeBorders()");

//	print_TimeIndex(filename + LATBINFILE_EXT_TIME);
	
	dbgTimer.start();
	if (getNodeLinksTimeBorders(filename + LATBINFILE_EXT_TIME, startTime, endTime, &this->firstNodeID, &this->lastNodeID) == LATBINFILE_ERROR_IO) 
		return LATBINFILE_ERROR_IO;
	dbgTimer.end();
	cumulTime_load_time += dbgTimer.val();
	
	DBG("getNodeLinksTimeBorders()...done");
	
	if (endTime == -1) 
		lastNodeID = -1;
	
	// read all records until endTime
	dbgTimer.start();
	nodeLinks.load(filename, firstNodeID, lastNodeID);
	dbgTimer.end();
	cumulTime_load_nodelinks += dbgTimer.val();

	DBG("Getting first and last node ID");
//	this->firstNodeID = nodeLinks.firstID;
//	this->lastNodeID = nodeLinks.lastID;
	DBG("ID_first:" << this->firstNodeID << " ID_last:" << this->lastNodeID);


/*	DBG("NodeLinks read count:" << nodeLinksMap.size());
	for (LatBinFile::NodeLinksMap::iterator i=nodeLinksMap.begin(); i!=nodeLinksMap.end(); ++i) {
		DBG("NodeLinks: " << (i->second));
	}
*/	


	// ***** READ NODES *****
	//
	DBG("Reading nodes...");
	DBG("this->nodes.load("<<filename + LATBINFILE_EXT_NODES<<", "<<firstNodeID<<", "<<lastNodeID<<")");

	dbgTimer.start();
	if (this->nodes.load(filename + LATBINFILE_EXT_NODES, this->firstNodeID, this->lastNodeID) != 0) {
		return LATBINFILE_ERROR_IO;
	}
	dbgTimer.end();
	cumulTime_load_nodes += dbgTimer.val();

	DBG("this->nodes.load...done");

	DBG("nodes.size:"<<this->nodes.size());

	if (this->alphaLastArray.load(filename + LATBINFILE_EXT_ALPHALAST) != 0) {
		return LATBINFILE_ERROR_IO;
	}
/*	
	DBG("Nodes array size:" << nodesArraySize);
	for (int i=firstNodeID; i<=lastNodeID; i++) {
		DBG("nodeArray[" << i << "]: " << getNodeFromNodesArray(firstNodeID, i));
	}
*/

//	We are not working with Lattice::*, so lat->updateFwdBckLinks(); is not needed
	
	return 0;
}


/**
  @brief Get context of the keyword with given ID

  @param centerNodeID	Found keyword
  @param fwdNodesCount	How many words should be get in forward pass
  @param bckNodesCount	How many words should be get in backward pass
  @param hyp			Result (hypotheses) are stored here
*/
/*
void LatBinFile::getKeywordContext(ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, Hypothesis *hyp, QueryKwdList *queryKwdList, QueryKwdList::iterator iQueryKwd) {

	DBG("FORWARD links processing");
	// *** FORWARD LINKS ***
	Hypothesis::Word curHypWord;

	DBG("centerNodeID:"<<centerNodeID);
	LatTime predNodeEndTime = this->nodes[centerNodeID].t;
	ID_t curNodeID = centerNodeID;
//	LatBinFile::Link *bestLink;
	ID_t bestNodeID;
	TLatViterbiLikelihood bestLinkLikelihood;
	int count=0;
	while (count < fwdNodesCount) 
	{
		// if the current kwd is a part of a phrase, 
		// then it is needed to find the path in lattice which fits the phrase
//		if (iQueryKwd->nb_next == 0) 
//		{
			// we need to look into the nodeLinks because we need to check also other paths than only the best one
//			if((bestNodeID = nodeLinks.bestFwdNode(curNodeID, &bestLinkLikelihood)) == -1) { // if end node found (which has no forward links), then break the loop
			
//		} else {
			// if end node found (which has no forward links), then break the loop

			if ((bestNodeID = nodeLinks.bestFwdNode(curNodeID, &bestLinkLikelihood)) == -1) 
			{ 
				DBG("No forward links... forward processing done");
				break;
			}
//		}

		curNodeID = bestNodeID;
		// if we are going to jump out of read lattice
		if (!this->nodes.containsID(curNodeID)) 
		{ 
			DBG("Jumping out of borders <"<<firstNodeID<<".."<<lastNodeID<<">... forward processing done (curNodeID:"<<curNodeID<<")");
			break;
		}

		// add hypothesis
		DBG("curNodeID:"<<curNodeID<<" W_id:"<<nodes[curNodeID].W_id);
		if (!mpLexicon->isMetaWord(this->nodes[curNodeID].W_id)) 
		{
			DBG("getVal("<<this->nodes[curNodeID]<<")");
			curHypWord.str = mpLexicon->id2word_readonly(this->nodes[curNodeID].W_id);
			curHypWord.start = predNodeEndTime;
			curHypWord.end = this->nodes[curNodeID].t;
			curHypWord.conf = bestLinkLikelihood; // Likelihood of best link coming into current node
			hyp->push_back_word(curHypWord);
			if (queryKwdList->contains(this->nodes[curNodeID].W_id))
				hyp->addKeyword(curHypWord);
			DBG("curNodeID:"<<curNodeID<<" word:"<<curHypWord.str<<" W_id:"<<nodes[curNodeID].W_id);
			count++;
		} else {
			DBG("curNodeID:"<<curNodeID<<" word:<META> W_id:"<<nodes[curNodeID].W_id);
		}
		
		predNodeEndTime = this->nodes[curNodeID].t;
	}
	DBG("FORWARD hypothesis count: "<< count);

	DBG("BACKWARD links processing");
	// *** BACKWARD LINKS ***
	ID_t nextNodeID;
	curNodeID = centerNodeID;
	predNodeEndTime = this->nodes[centerNodeID].t;
	count = 0;
	while (count < bckNodesCount) {
//		if ((bestNodeID = nodeLinks.bestBckNode(curNodeID, &bestLinkLikelihood)) == -1) { // if first node found (which has no backward links), then break the loop
		if ((bestNodeID = nodeLinks.bestBckNode(curNodeID, &bestLinkLikelihood)) == -1) { // if first node found (which has no backward links), then break the loop
			DBG("nodeID:"<<curNodeID<<" ...first node found -> backward processing termination");
			break;
		}

		nextNodeID = bestNodeID;
		if (!this->nodes.containsID(nextNodeID)) { // if we are going to jump out of read lattice
			DBG("nextNodeID:"<<nextNodeID<<" ...going to jump out of read lattice (IDs: "<<firstNodeID<<"..."<<lastNodeID<<") -> backward processing termination");
			break;
		}

		DBG("curNodeID:"<<curNodeID<<" nextNodeID:"<<nextNodeID);

		// add hypothesis
		if (!mpLexicon->isMetaWord(this->nodes[curNodeID].W_id)) {
			curHypWord.str = mpLexicon->id2word_readonly(this->nodes[curNodeID].W_id);
			curHypWord.start = this->nodes[nextNodeID].t;
			curHypWord.end = this->nodes[curNodeID].t;
			curHypWord.conf = bestLinkLikelihood; // Likelihood of best link coming into current node
			if (queryKwdList->contains(this->nodes[curNodeID].W_id))
				hyp->addKeyword(curHypWord);
			hyp->push_front_word(curHypWord);
			DBG("curNodeID:"<<curNodeID<<" word:"<<curHypWord.str);
			count++;
		} else {
			DBG("curNodeID:"<<curNodeID<<" word:!NULL");
		}

		curNodeID = nextNodeID; // go to the next node
	}
	DBG("BACKWARD hypothesis count: "<< count);
}
*/

/**
  @brief find the way through lattice that corresponds the given phrase (queryKwdList)

  @param centerNodeID
  @param queryKwdList

  @return 
*/
/*
bool LatBinFile::matchPhraseContext_recursive(ID_t nodeID, TLatViterbiLikelihood likelihood, QueryKwdList::iterator iQueryKwd, QueryKwdList *queryKwdList, IDWithLikelihoodList *res) {
	LatNodeLinksArray::Cursor linkCursor(&this->nodeLinks);
	QueryKwdList::iterator iQueryKwd_cur = iQueryKwd++;
	IDWithLikelihood resRecord;	
	resRecord.id = nodeID;
	resRecord.likelihood = likelihood;
	
	DBG("matchPhraseContext_recursive("<<this->nodes[nodeID]<<", "<<iQueryKwd_cur->W<<")");
	
	if (iQueryKwd_cur->nb_next != 0 || iQueryKwd == queryKwdList->end()) { // if we have processed all keywords, it means that this is the right path
		DBG("  GOOD PATH: "<<this->nodes[nodeID]);
		DBG("  queryKwdList END");
		res->push_front(resRecord);
		return true;
	} 	
	
	linkCursor.gotoNodeID(nodeID, LatNodeLinksArray::fwd);
	while (!linkCursor.eol()) {
		LatBinFile::NodeLinks::Link curLink = linkCursor.getLink();
		DBG("  link's endNode:"<<this->nodes[curLink.nodeID]);
		
		if (!this->nodes.containsID(curLink.nodeID)) { // if we are going to jump out of read lattice
			DBG("  OUT OF LATTICE");
			return false;
		}
		
		if (this->nodes[curLink.nodeID].W_id == iQueryKwd->W_id) { // if the current link points to the next word in phrase
			DBG("  match word: "<<this->nodes[curLink.nodeID]);
			if (matchPhraseContext_recursive(curLink.nodeID, curLink.conf, iQueryKwd, queryKwdList, res)) {
				DBG("  GOOD PATH: "<<this->nodes[nodeID]);
				res->push_front(resRecord);
				return true; // if we found the matching path
			}
		}
		
		linkCursor.next();
	}
	return false;
}
*/

/*
bool LatBinFile::matchPhraseContext_breadthFirst(ID_t centerNodeID, TLatViterbiLikelihood likelihood, QueryKwdList::iterator iQueryKwd, QueryKwdList *queryKwdList, IDWithLikelihoodList *res) 
{
	// create scores array
	float* score = new float[this->nodes.size()];
	// ...and fill it with zeros
	memset(score, 0, sizeof(float) * this->nodes.size());


	typedef queue < ID_t > IDQueue;
	IDQueue idQ;
	idQ.push(centerNodeID);

	LatNodeLinksArray::Cursor linkCursor(&this->nodeLinks);
	// while there are some nodes left in the forward direction
	while (!idQ.empty()) 
	{
		// get the first ID from queue
		ID_t curNodeID = idQ.first();
		float curNodeScore = score[curNodeID - nodes.firstNodeID];
		// remove it from the queue
		idQ.pop();
		
		linkCursor.gotoNodeID(curNodeID, LatNodeLinksArray::fwd);
		while (!linkCursor.eol()) 
		{
			LatNodeLinksArray::Link curLink = linkCursor.getLink();
			
			// if the fwd node is out of the part of lattice in memory, skip it
			if (!this->nodes.containsID(curLink.endNodeID)) 
			{ 
				DBG("  OUT OF LATTICE:"<<curLink.endNodeID);
				continue;
			}

			// set the score of the current fwd node
			if (score[curLink.endNodeID - nodes.firstNodeID] > curNodeScore + 1) 
			{
				score[curLink.endNodeID - nodes.firstNodeID] = curNodeScore + 1;
			}

			if (nodes[curLink.endNodeID].W_id == iQueryKwd->W_id) 
			{
				
			}

			linkCursor.next();
		}

	}
	
	// clean up
	delete[] score;

	return false;
}
*/	

/*
bool LatBinFile::getPhraseKeywordContext(ID_t centerNodeID, int fwdNodesCount, int bckNodesCount, QueryKwdList::iterator curKwd, QueryKwdList *queryKwdList, Hypothesis *hyp) {

	IDWithLikelihoodList idList;
	DBG("FORWARD links processing");
	// *** FORWARD LINKS ***
	Hypothesis::Word curHypWord;

	DBG("centerNodeID:"<<centerNodeID);
	LatTime predNodeEndTime = this->nodes[centerNodeID].t;
	ID_t curNodeID = centerNodeID;
//	LatBinFile::Link *bestLink;
	ID_t bestNodeID;
	TLatViterbiLikelihood bestLinkLikelihood;
	LatNodeLinksArray::Cursor linkCursor(&this->nodeLinks);
	int count=0;
	bool inPhrase = false;
	while (count < fwdNodesCount) {
		if (curKwd != queryKwdList->end()) {
			DBG("curKwd:"<<*curKwd);
			// if the current kwd is the first in a phrase
			if (curKwd->nb_next == 0 && inPhrase == false) {
				DBG("matchPhraseContext_recursive()");
				if (! matchPhraseContext_recursive(centerNodeID, (TLatViterbiLikelihood)0, curKwd, queryKwdList, &idList)) {
					return false; // if the phrase does not match, just return false
				}
				for (IDWithLikelihoodList::iterator i = idList.begin(); i != idList.end(); ++i) {
					DBG("idList: "<<*i);
				}
				if ((idList.begin())->id == centerNodeID) {
					idList.erase(idList.begin());
				}
				inPhrase = true;
			} 
		}
		if (idList.size() == 0) {
			DBG("idList.size() == 0");
			inPhrase = false;
		}

		if (inPhrase) {
			DBG("inPhrase == true");
			bestNodeID = (idList.begin())->id;					// load nodeID
			bestLinkLikelihood = (idList.begin())->likelihood;	// and it's likelihood
			idList.erase(idList.begin());	// erase loaded nodeID from idList
		} else {
			DBG("inPhrase == false");
			if((bestNodeID = nodeLinks.bestFwdNode(curNodeID, &bestLinkLikelihood)) == -1) { // if end node found (which has no forward links), then break the loop
				DBG("No forward links... forward processing done");
				break;
			}
		}
		
		DBG("bestNodeID = "<<bestNodeID);
		curNodeID = bestNodeID;
		if (!this->nodes.containsID(curNodeID)) { // if we are going to jump out of read lattice
			DBG("Jumping out of borders <"<<firstNodeID<<".."<<lastNodeID<<">... forward processing done (curNodeID:"<<curNodeID<<")");
			break;
		}
		DBG("");
		// add hypothesis
		DBG("curNodeID:"<<curNodeID<<" W_id:"<<nodes[curNodeID].W_id);
		if (this->nodes[curNodeID].W_id != mpLexicon->nullWordID) {
			DBG("getVal("<<this->nodes[curNodeID]<<")");
			curHypWord.str = mpLexicon->id2word_readonly(this->nodes[curNodeID].W_id);
			curHypWord.start = predNodeEndTime;
			curHypWord.end = this->nodes[curNodeID].t;
			curHypWord.conf = bestLinkLikelihood; // Likelihood of best link coming into current node
			hyp->push_back_word(curHypWord);
			DBG("curNodeID:"<<curNodeID<<" word:"<<curHypWord.str);
			count++;
		} else {
			DBG("curNodeID:"<<curNodeID<<" word:!NULL");
		}
		
		predNodeEndTime = this->nodes[curNodeID].t;
		if (curKwd != queryKwdList->end()) { // if some keyword remains in the list, then move the pointer on this keyword
			curKwd++;
		}
	}
	DBG("FORWARD hypothesis count: "<< count);

	DBG("BACKWARD links processing");
	// *** BACKWARD LINKS ***
	bool keywordAdded = false;
	ID_t nextNodeID;
	curNodeID = centerNodeID;
	predNodeEndTime = this->nodes[centerNodeID].t;
	count = 0;
	while (count < bckNodesCount) {
		if ((bestNodeID = nodeLinks.bestBckNode(curNodeID, &bestLinkLikelihood)) == -1) { // if first node found (which has no backward links), then break the loop
			DBG("nodeID:"<<curNodeID<<" ...first node found -> backward processing termination");
			break;
		}

		nextNodeID = bestNodeID;
		if (!this->nodes.containsID(nextNodeID)) { // if we are going to jump out of read lattice
			DBG("nextNodeID:"<<nextNodeID<<" ...going to jump out of read lattice (IDs: "<<firstNodeID<<"..."<<lastNodeID<<") -> backward processing termination");
			break;
		}

		DBG("curNodeID:"<<curNodeID<<" nextNodeID:"<<nextNodeID);

		// add hypothesis
		if (this->nodes[curNodeID].W_id != mpLexicon->nullWordID) {
			curHypWord.str = mpLexicon->id2word_readonly(this->nodes[curNodeID].W_id);
			curHypWord.start = this->nodes[nextNodeID].t;
			curHypWord.end = this->nodes[curNodeID].t;
			curHypWord.conf = bestLinkLikelihood; // Likelihood of best link coming into current node
			if (!keywordAdded) {
				hyp->addKeyword(curHypWord);
				keywordAdded = true;
			}
			hyp->push_front_word(curHypWord);
			DBG("curNodeID:"<<curNodeID<<" word:"<<curHypWord.str);
			count++;
		} else {
			DBG("curNodeID:"<<curNodeID<<" word:!NULL");
		}

		curNodeID = nextNodeID; // go to the next node
	}
	DBG("BACKWARD hypothesis count: "<< count);

	
	return true;
}
*/


/**
  @brief Computes value for fseek() which represents position of node
  with given ID
 
  Nodes ID's domain is 0..infinity
*/
long LatBinFile::getNodePositionInNodesFile(ID_t nodeID) 
{
	return nodeID * NodesArray::nodeRecordWidth;
}


long LatBinFile::getNodePositionInNodeArray(ID_t nodeID) 
{
	return (nodeID - firstNodeID) * NodesArray::nodeRecordWidth;	
}



/**
  @brief Returns position of node links at given times

  @param filename
  @param startTime
  @param endTime
  @param startPos Return value - position of first node links with time startTime in node links file
  @param endPos Return value - position of first node links with time endTime in node links file
*/
int LatBinFile::getNodeLinksTimeBorders(const string filename, LatTime startTime, LatTime endTime, ID_t * startNodeID, ID_t * endNodeID) {
	
	DBG("getNodeLinksTimeBorders("<<filename<<", "<<startTime<<".."<<endTime<<")");
	
	ifstream f_inTime(filename.c_str(), ios_base::binary);
	if (!f_inTime.good()) {
		return LATBINFILE_ERROR_IO;		
	}

	bool startNodeFound = false;
	
	DBG("startTime: "<<startTime<<" endTime: "<<endTime);

	TimeRecord tRec;
	ID_t tRecPredNodeID = -1;
	while (!f_inTime.eof()) {
		tRec.readFrom(f_inTime);
		if (f_inTime.eof()) break;

		DBG("t:" << tRec.t << " nodeID:" << tRec.nodeID);
		// if time t is found, stop reading file and return position
		if (tRec.t > endTime && startNodeFound && endTime != -1) {
			if (endNodeID != NULL) *endNodeID = tRec.nodeID;
			break;
		}

		if (tRec.t >= startTime) {
			if (!startNodeFound) {
				if (startNodeID != NULL) *startNodeID = (tRecPredNodeID==-1) ? tRec.nodeID : tRecPredNodeID;
				startNodeFound = true;
			}
//			DBG("  startTime:"<<tRec.t<<"\tnodeID:"<<tRec.nodeID);
			if (endTime == -1)
				break;
		} 
		tRecPredNodeID = tRec.nodeID;
	}
	f_inTime.close();
	
	if (endTime == -1) {
		if (endNodeID != NULL) *endNodeID = -1;
	}

	DBG("  startNodeID:"<<*startNodeID<<" endNodeID:"<<*endNodeID);
	DBG("getNodeLinksTimeBorders: done");
	return 0;
}



int LatBinFile::print_TimeIndex(const string filename) {

	ifstream f_inTime(filename.c_str(), ios_base::binary);
	if (!f_inTime.good()) {
		return LATBINFILE_ERROR_IO;		
	}

	TimeRecord tRec;
	//DBG("startTime: "<<startTime<<" endTime: "<<endTime);

	while (!f_inTime.eof()) {
		tRec.readFrom(f_inTime);
		if (f_inTime.eof()) break;
		cout << tRec << endl;
		// if time t is found, stop reading file and return position
	}
	f_inTime.close();

	return 0;
}

void LatBinFile::stat() {
	DBG("cumulTime_load_time: " << cumulTime_load_time << " s");
	DBG("cumulTime_load_nodes: " << cumulTime_load_nodes << " s");
	DBG("cumulTime_load_nodelinks: " << cumulTime_load_nodelinks << " s");
}

void LatBinFile::free() 
{
	nodes.free();
	nodeLinks.free();
}
