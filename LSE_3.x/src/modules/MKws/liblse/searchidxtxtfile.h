#ifndef SEARCHIDXTXTFILE_H
#define SEARCHIDXTXTFILE_H

#include <fstream>
#include "lattypes.h"
#include "latindexer.h"

namespace lse {

//--------------------------------------------------
//	SearchIdxTxtRecord
//--------------------------------------------------
class SearchIdxTxtRecord : public LatIndexer::Record
{
	public:
		LatIndexer::RecordIdx mRecordIdx;
		friend std::ostream& operator<<(std::ostream& os, SearchIdxTxtRecord &rec)
		{
			return os << "meetingID:" << rec.mMeetingID << "\twordID:" << rec.mWordID << "\tconf:" << rec.mConf << "\ttStart:" << rec.mStartTime << "\ttEnd:" << rec.mEndTime << "\trecordIdx:" << rec.mRecordIdx;
		}
};

//--------------------------------------------------
//	SearchIdxTxtFile
//--------------------------------------------------
class SearchIdxTxtFile
{
		std::ifstream 	mFile;
		int				mState;
		bool 			mEpsilon;

		bool IsEol(const char c);
		bool IsDelimiter(const char c);

		void Init()
		{
			mState = 0;
			mEpsilon = false;
		}

	public:

		SearchIdxTxtFile();
		SearchIdxTxtFile(const std::string &filename);

		void Open(const std::string &filename);
		bool Next(SearchIdxTxtRecord *rec);
		void Close();
};

} // namespace lse

#endif
