/*********************************************************************************
 *	TODO: 
 *
 *	Add checksum hash table for each word in map str2idMap to speed up searching
 *
 *	getNewWordID(): there is some useable STL function (hope so)
 *	                store last index in variable
 *
 *	getWordID(): for parallel processing - implementation of semaphor is needed
 *
 */

#ifndef GENERICINDEXER_H
#define GENERICINDEXER_H

#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include "lattypes.h"

namespace lse {

#define GI_NOT_FOUND	0
#define GI_START_INDEX		1


//--------------------------------------------------------------------------------
// GenericIndexer class

template< class T > 
class GenericIndexer {
	
	std::map<ID_t, T> id2valMap;
	std::map<T, ID_t> val2idMap;
	
	ID_t getNewID();
	
	
public:

	virtual ~GenericIndexer() {};
	
	void addRecord(const ID_t id, const T val);
	
	ID_t getValID(const T val, bool add_if_not_found = true); 
	T getVal(const ID_t id);
	
	int saveToFile(const std::string filename);
	int loadFromFile(const std::string filename);

	virtual void writeStruct(std::ofstream &out, const ID_t id, const T val) = 0;
	virtual int readStruct(std::ifstream &in, ID_t &id, T &val) = 0;
	
	void print();

	
};

}

//--------------------------------------------------------------------------------
// addRecord()
// 
// Add pair ID -> Val to indexer

template<class T>
void lse::GenericIndexer<T>::addRecord(const lse::ID_t id, const T val) {
	val2idMap.insert(make_pair(val, id));
	id2valMap.insert(make_pair(id, val));
}


//--------------------------------------------------------------------------------
// getValID()
//
// If there is no occurence of given val in the lexicon, 
// new record is created in both val->id and id->val maps
// and valID of inserted val is returned,
// 
// Return value: ID of given val

template<class T> 
lse::ID_t lse::GenericIndexer<T>::getValID(const T val, bool add_if_not_found) {

	// try to find given val in val2idMap
	typename std::map<T, lse::ID_t>::iterator pos = val2idMap.find(val);
//	Val2ID_iter pos = val2idMap.find(val);
	
	if (pos != val2idMap.end()) {
		// ID of given val is returned
		return pos->second;		
	} 
	else if (add_if_not_found) {
		// new record is created and it's ID is returned
		lse::ID_t newID = getNewID();
		val2idMap.insert(make_pair(val, newID));
		id2valMap.insert(make_pair(newID, val));	
		return newID;
	} else {
		// value has not been found, so ERROR is returned
		return GI_NOT_FOUND;
	}
}


//--------------------------------------------------------------------------------
// getVal()
// 
// If there is a val with given ID in the lexicon, it's ID is returned,
// otherwise NULL is returned
//
// Return value: val with given ID (or NULL)



template<class T> 
T lse::GenericIndexer<T>::getVal(const lse::ID_t id) {
	
	typename std::map<lse::ID_t, T>::iterator pos = id2valMap.find(id);
//	ID2val_iter pos = id2valMap.find(id);

	if (pos != id2valMap.end()) {
		// val with given ID is returned
		return pos->second;
	} 
	else {
		// ERROR: there is no such val in the lexicon
		return NULL;
	}
}



//--------------------------------------------------------------------------------
// getNewID
// 
// Return value - next possible valID

template<class T> 
lse::ID_t lse::GenericIndexer<T>::getNewID() {

	// Sequence search for max key (ID) value
	lse::ID_t max = GI_START_INDEX - 1; // index starts from GI_START_INDEX...max is going to be max+1

	for (typename std::map<lse::ID_t, T>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		if (max < i->first) {
			max = i->first;
		}
	}
	return max+1;
}



//--------------------------------------------------------------------------------
// LoadFromFile

template<class T> 
int lse::GenericIndexer<T>::loadFromFile(const std::string filename) {
	
	// clear both maps
	id2valMap.clear();
	val2idMap.clear();

	std::ifstream in(filename.c_str(), std::ios::binary);
	if (!in.good()) {
		return (2);
	}
	
	lse::ID_t valID;
	T val;
	
	// read all nodes and convert strID into str using lexicon
	while(!in.eof()) {
		
		if (readStruct(in, valID, val) != 0) {
			break;
		}
		// add record to both maps
		val2idMap.insert(make_pair(val, valID));
		id2valMap.insert(make_pair(valID, val));	
	}
	
	in.close();
	
	return 0;
}



//--------------------------------------------------------------------------------
// saveToFile

template<class T> 
int lse::GenericIndexer<T>::saveToFile(const std::string filename) {
		
	std::ofstream out(filename.c_str(), std::ios::binary);
	if (out.bad()) {
		return (2);
	}

	// it is not important which map to save to file, because they are identical
	for(typename std::map<lse::ID_t, T>::iterator i=id2valMap.begin(); i!=id2valMap.end(); ++i) {
		writeStruct(out, i->first, i->second);
	}
	out.close();
	return 0;
}


//--------------------------------------------------------------------------------
// print()

template< class T > 
void lse::GenericIndexer< T >::print() {
	for (typename std::map<lse::ID_t, T>::iterator i = id2valMap.begin(); i != id2valMap.end(); i++) {
		std::cout << "ID=" << i->first << '\t' << i->second << std::endl;
	}

}
#endif
