/*********************************************************************************
 *	TODO: 
 *
 *	Add checksum hash table for each word in map str2idMap to speed up searching
 *
 *	getNewWordID(): there is some useable STL function (hope so)
 *	                store last index in variable
 *
 *	getWordID(): for parallel processing - implementation of semaphor is needed
 *
 */

#ifndef LEXICON_H
#define LEXICON_H

#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <cassert>
#include <sys/stat.h>
#include "lattypes.h"
#include "genericindexer.h"
#include "dbg.h"

#define LEXICON_ORDBYID_EXT ".idxid"
#define LEXICON_ORDBYALPHA_EXT ".idxalpha"

namespace lse {

class Lexicon {
	
public:
	enum AccessMethod {
		readonly,
		readwrite
	};

/*	enum Format {
		bin,
		txt
	};
*/	
private:
	char * wordArray;
	long wordArray_size;

	long * wordArrayIdxIDsort;
	long * wordArrayIdxAlphasort;
	long wordArrayIdx_size;

//	bool isBinary;
	
	std::map<ID_t, std::string> id2valMap;
	std::map<std::string, ID_t> val2idMap;

	int mNullWordID;


/*	char * wordsArray;
	long * wordsArrayPointersIDsort;
	long * wordsArrayPointersAlphasort;
	unsigned long wordsArray_size;
	unsigned long wordsCount;
*/	ID_t getNewID();
	void addRecord(const ID_t id, const std::string val);
	
public:


	class Record {
	public:
		ID_t id;
		std::string word;
		long pos;
		
		friend bool order_by_word(const Lexicon::Record &l, const Lexicon::Record &r);
		friend bool order_by_id(const Lexicon::Record &l, const Lexicon::Record &r);
	};

	bool isReadonly;

	Lexicon(AccessMethod accessMethod/*, Format format = bin*/) : 
		wordArray(NULL), 
		wordArray_size(0),
		wordArrayIdxIDsort(NULL),
		wordArrayIdxAlphasort(NULL), 
		wordArrayIdx_size(0)
	{
		this->isReadonly = (accessMethod == readonly);			
//		this->isBinary = (format == bin);			

//		addRecord(0, "!NULL"); // !NULL word has always id=0
	}

	~Lexicon() {
		// free arrays
		if (wordArray != NULL)
			delete[] wordArray;

		if (wordArrayIdxIDsort != NULL)
			delete[] wordArrayIdxIDsort;
		
		if (wordArrayIdxAlphasort != NULL)
			delete[] wordArrayIdxAlphasort;
	}
	
	int size();

	ID_t getNullWordID() { return mNullWordID; }
	ID_t getValID(const std::string val, bool add_if_not_found = true); 
	std::string getVal(const ID_t id);
	
	int saveToFile(const std::string filename);
	int loadFromFile(const std::string filename);

	int loadFromFile_readonly(const std::string filename);
	const char * id2word_readonly(ID_t id);
	ID_t word2id_readonly(const std::string &w, bool exitWhenWordNotFound = true);
	bool WordExists_readonly(ID_t id);
	bool WordExists_readonly(const std::string &w);

	
	void writeStruct(std::ofstream &out, const ID_t id, const std::string val);
	int readStruct(std::ifstream &in, ID_t &id, std::string &val);
	
	int createIndexFile(const std::string filename);

	bool isMetaWord(const ID_t id);
	bool isMetaWord(const std::string word);
	bool isSil(const ID_t id);
	bool isSil(const std::string word);
	
	bool CanAddWiPen(const ID_t wordID);
		
	void print();
};

bool order_by_word(const Lexicon::Record &l, const Lexicon::Record &r);
bool order_by_id(const Lexicon::Record &l, const Lexicon::Record &r);
} // namespace lse

#endif
