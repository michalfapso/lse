#include "ngram.h"
#include <math.h>

using namespace lse;
using namespace std;

Ngram::Ngram()
{
	DBG(this<<" Ngram::Ngram()");
	mpItems = NULL;
}

Ngram::Ngram(int n)
{
	DBG(this<<" Ngram::Ngram(n)");
	mN = n;
	mTStart = INF;
	mTEnd = 0.0;
	mpItems = new ID_t[mN];
	mConf = -INF;
	mHash = 0;
}

Ngram::Ngram(const Ngram &ngram_parent)
{
	CopyParamsFrom(ngram_parent);
}


void Ngram::CopyParamsFrom(const Ngram &ngram_parent)
{
	DBG(this<<" Ngram::CopyParamsFrom("<<&ngram_parent<<")");
	mN = ngram_parent.mN;
	mTStart = ngram_parent.mTStart;
	mTEnd = ngram_parent.mTEnd;
	mConf = ngram_parent.mConf;
	mHash = ngram_parent.mHash;
	mNodePath.assign(ngram_parent.mNodePath.begin(), ngram_parent.mNodePath.end());
//	DBG("creating mpItems");
	if (mpItems == NULL)
	{
		mpItems = new ID_t[mN];
	}
//	DBG("copying mpItems ... "<<ngram_parent.mpItems<<" -> "<<mpItems );
	memcpy(mpItems, ngram_parent.mpItems, sizeof(ID_t)*mN);
//	DBG("copying mpItems done");
}

Ngram::~Ngram()
{
	delete[] mpItems;
}



void Ngram::SetItem(int n, LatTime tStart, LatTime tEnd, ID_t W_id)
{
	DBG(this<<" Ngram::SetItem("<<n<<")");
	assert(n < mN);
	mpItems[n] = W_id;
	mHash += 128*n*W_id;
	if (mTStart > tStart) 
	{
		DBG(this<<" mTStart > tStart ... "<<mTStart<<" > "<<tStart);
		mTStart = tStart;
	}
	if (mTEnd < tEnd)
	{
		DBG(this<<" mTEnd < tEnd ... "<<mTEnd<<" < "<<tEnd);
		mTEnd = tEnd;
	}
}


void Ngrams::Insert(Ngram* pNgram)
{
	int i = GetIndex(pNgram);
	mpHash[i].push_back(pNgram);
}

NgramsHashItemList* Ngrams::GetHashItemList(const Ngram* pNgram)
{
	return &(mpHash[GetIndex(pNgram)]);
}

int* Ngrams::GetHashItemListSize(const Ngram* pNgram)
{
	return &(mpHashListSizes[GetIndex(pNgram)]);
}

Ngram* NgramsCursor::NextBegin()
{
	DBG("mHashItem:"<<mHashItem);
	while (miNgram == mpNgrams->mpHash[mHashItem].end())
	{
		mHashItem++;
		if (mHashItem >= mpNgrams->mHashSize)
		{
			return NULL;
		}
		//DBG("mHashItem:"<<mHashItem);
		miNgram = mpNgrams->mpHash[mHashItem].begin();
	}
	return *miNgram;
}

Ngram* NgramsCursor::Begin()
{
	mHashItem = 0;
	return NextBegin();
}

Ngram* NgramsCursor::Next()
{
	miNgram++;
	if (miNgram == mpNgrams->mpHash[mHashItem].end())
	{
		return NextBegin();
	} 
	else
	{
		return *miNgram;
	}
}

