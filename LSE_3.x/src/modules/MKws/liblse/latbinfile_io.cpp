#include "latbinfile.h"

using namespace std;
using namespace lse;


//--------------------------------------------------------------------------------
// NodeLinks
//--------------------------------------------------------------------------------
int LatBinFile::NodeLinks::getHeaderSize()
{
	return sizeof(ID_t)
		 + sizeof(int)
		 + sizeof(int);
}

void LatBinFile::NodeLinks::writeHeaderTo(std::ofstream &out)
{
	out.write(reinterpret_cast<const char *>(&id), sizeof(id));
	out.write(reinterpret_cast<const char *>(&fwdLinksCount), sizeof(fwdLinksCount));
	out.write(reinterpret_cast<const char *>(&bckLinksCount), sizeof(bckLinksCount));
}

void LatBinFile::NodeLinks::readHeaderFrom(std::ifstream &in)
{
	in.read(reinterpret_cast<char *>(&id), sizeof(id));
	in.read(reinterpret_cast<char *>(&fwdLinksCount), sizeof(fwdLinksCount));
	in.read(reinterpret_cast<char *>(&bckLinksCount), sizeof(bckLinksCount));
}

void LatBinFile::NodeLinks::readHeaderFrom(unsigned char ** a)
{
	id = *(reinterpret_cast<ID_t*>(*a));
	*a += sizeof(ID_t);
	
	fwdLinksCount = *(reinterpret_cast<int*>(*a));
	*a += sizeof(int);

	bckLinksCount = *(reinterpret_cast<int*>(*a));
	*a += sizeof(int);

	DBG("fwdLinksCount: "<<fwdLinksCount<<"   bckLinksCount: "<<bckLinksCount);
}

//--------------------------------------------------------------------------------
// NodeLinks::Link
//--------------------------------------------------------------------------------
void LatBinFile::NodeLinks::Link::writeTo(ofstream &out)
{
//	DBG("Link::writeTo() nodeID="<<nodeID<<" conf="<<conf);
	out.write(reinterpret_cast<const char *>(&nodeID), sizeof(nodeID));
	out.write(reinterpret_cast<const char *>(&conf), sizeof(conf));
}


void LatBinFile::NodeLinks::Link::readFrom(ifstream &in)
{
	in.read(reinterpret_cast<char *>(&nodeID), sizeof(nodeID));
	in.read(reinterpret_cast<char *>(&conf), sizeof(conf));
}

void LatBinFile::NodeLinks::Link::readFrom(unsigned char * a)
{
	nodeID = *(reinterpret_cast<ID_t*>(a));
	a += sizeof(ID_t);

	conf = *(reinterpret_cast<float*>(a));
	a += sizeof(float);
}

//--------------------------------------------------------------------------------
// Node
//--------------------------------------------------------------------------------
void LatBinFile::Node::writeTo(ofstream &out)
{
	out.write(reinterpret_cast<const char *>(&id), sizeof(id));
	out.write(reinterpret_cast<const char *>(&W_id), sizeof(W_id));
	out.write(reinterpret_cast<const char *>(&tStart), sizeof(tStart));
	out.write(reinterpret_cast<const char *>(&t), sizeof(t));
	out.write(reinterpret_cast<const char *>(&conf), sizeof(conf));
	out.write(reinterpret_cast<const char *>(&alpha), sizeof(alpha));
	out.write(reinterpret_cast<const char *>(&beta), sizeof(beta));
}

void LatBinFile::Node::readFrom(ifstream &in)
{
	in.read(reinterpret_cast<char *>(&id), sizeof(id));
	in.read(reinterpret_cast<char *>(&W_id), sizeof(W_id));
	in.read(reinterpret_cast<char *>(&tStart), sizeof(tStart));
	in.read(reinterpret_cast<char *>(&t), sizeof(t));
	in.read(reinterpret_cast<char *>(&conf), sizeof(conf));
	in.read(reinterpret_cast<char *>(&alpha), sizeof(alpha));
	in.read(reinterpret_cast<char *>(&beta), sizeof(beta));
}


//--------------------------------------------------------------------------------
// TimeRecord
//--------------------------------------------------------------------------------
void LatBinFile::TimeRecord::writeTo(ofstream &out)
{
	out.write(reinterpret_cast<const char *>(&t), sizeof(t));
	out.write(reinterpret_cast<const char *>(&nodeID), sizeof(nodeID));
	DBG("TimeRecord::write("<<t<<", "<<nodeID<<")");
}

void LatBinFile::TimeRecord::readFrom(ifstream &in)
{
	in.read(reinterpret_cast<char *>(&t), sizeof(t));
	in.read(reinterpret_cast<char *>(&nodeID), sizeof(nodeID));
}


//--------------------------------------------------------------------------------
// AlphaLastRecord
//--------------------------------------------------------------------------------
void LatBinFile::AlphaLastRecord::writeTo(std::ofstream &out)
{
	out.write(reinterpret_cast<const char *>(&nodeID), sizeof(nodeID));
	out.write(reinterpret_cast<const char *>(&alphaLast), sizeof(alphaLast));
	DBG("AlphaLastRecord::write("<<nodeID<<","<<alphaLast<<")");
}

void LatBinFile::AlphaLastRecord::readFrom(std::ifstream &in)
{
	in.read(reinterpret_cast<char *>(&nodeID), sizeof(nodeID));
	in.read(reinterpret_cast<char *>(&alphaLast), sizeof(alphaLast));
	DBG("AlphaLastRecord::read("<<nodeID<<","<<alphaLast<<")");
}

void LatBinFile::AlphaLastRecord::readFrom(unsigned char ** a)
{
	nodeID = *(reinterpret_cast<ID_t*>(*a));
	*a += sizeof(nodeID);
	
	alphaLast = *(reinterpret_cast<float*>(*a));
	*a += sizeof(alphaLast);
	DBG("AlphaLastRecord::read("<<nodeID<<","<<alphaLast<<")");
}
