/*********************************************************************************
 *	TODO: 
 *
 *	Add checksum hash table for each word in map str2idMap to speed up searching
 *
 *	getNewWordID(): there is some useable STL function (hope so)
 *	                store last index in variable
 *
 *	getWordID(): for parallel processing - implementation of semaphor is needed
 *
 */

#ifndef LATMEETINGINDEXER_H
#define LATMEETINGINDEXER_H

#include <iostream>
#include <fstream>
#include <map>
#include "lexicon.h"
#include "lattypes.h"
#include "common.h"


//--------------------------------------------------------------------------------
// LatMeetingIndexer class

namespace lse {

class LatMeetingIndexerRecord {
	
public:

	ID_t id;
	std::string path;
	LatTime length;
	
	friend bool operator<(const LatMeetingIndexerRecord& l, const LatMeetingIndexerRecord& r) {
		return l.id < r.id;
	}
	
	friend std::ostream& operator<<(std::ostream& os, const LatMeetingIndexerRecord& rec) {
		return os << "id:" << rec.id << "\tlength:" << rec.length << "\tpath:" << rec.path;
	}
};

class LatMeetingIndexer {

	ID_t getNewID();

public:
	
	typedef std::map<ID_t, LatMeetingIndexerRecord> Id2valMap;
	typedef std::map<std::string, ID_t> Val2idMap;

	Id2valMap id2valMap;
	Val2idMap val2idMap;

	void addRecord(const LatMeetingIndexerRecord val);
	
	ID_t insertRecord(LatMeetingIndexerRecord *val);
	ID_t getRecID(const std::string &path);
	LatMeetingIndexerRecord getVal(const ID_t id);
	
	int saveToFile(const std::string filename);
	int loadFromFile(const std::string filename);

	void writeStruct(std::ofstream &out, const ID_t id, const LatMeetingIndexerRecord val);
	int readStruct(std::ifstream &in, ID_t &id, LatMeetingIndexerRecord &val);
	
	void print();
};

} // namespace lse


#endif
