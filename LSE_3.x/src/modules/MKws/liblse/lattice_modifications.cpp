#include <queue>
#include "lat.h"
#include "lattice_modifications.h"

using namespace std;
using namespace lse;


// bool  C_Lattice_Modifications::cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_NodeVector &from, T_NodeVector &to) {{{
bool  C_Lattice_Modifications::cutOffPieceOfLattice(
                LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo,
                T_NodeVector &from, T_NodeVector &to)
{
  std::map<lse::ID_t,int> LinksToDelete, NodesToDelete;
  bool IsNodeInBEG=false, IsNodeInEND=false;
  std::vector<ID_t>::iterator DeleteMe;

  from.clear();
  to.clear();

  float pom;
  if( FromTimeTo > ToTimeFrom ){
    pom = (FromTimeTo - ToTimeFrom)/2;
    FromTimeTo = ToTimeFrom+pom;
    ToTimeFrom = ToTimeFrom+pom;
  }//if
   
	if( lattice->TimePrun  ){
		if( FromTimeFrom < lattice->nodes[lattice->firstNodeID].t_from){
			FromTimeFrom = lattice->nodes[lattice->firstNodeID].t_from + 0.01;
		};
	}else{
		if( FromTimeFrom < lattice->nodes[lattice->firstNodeID].t){
			FromTimeFrom = lattice->nodes[lattice->firstNodeID].t + 0.01;
		};	
	}	

	if( ToTimeTo > lattice->nodes[lattice->lastNodeID].t ){
		ToTimeTo = lattice->nodes[lattice->lastNodeID].t-0.01;
  }

  for (std::map<ID_t, Lattice::Node>::iterator iNode = lattice->nodes.begin(); iNode != lattice->nodes.end(); ++iNode)
  {
    if( ((iNode->second).t >= FromTimeFrom) && ((iNode->second).t <= FromTimeTo) )
    {
      from.push_back((iNode->second).id);
      IsNodeInBEG=true;
      //std::cout << "Found node in BEG REG, ID: " <<  (iNode->second).id << std::endl;
    }
    if( ((iNode->second).t >= ToTimeFrom) && ((iNode->second).t <= ToTimeTo) )
    {
      to.push_back((iNode->second).id);
      IsNodeInEND=true;
      //std::cout << "Found node in END REG, ID: " <<  (iNode->second).id << std::endl;
    }
    if( ((iNode->second).t > FromTimeTo) && ((iNode->second).t < ToTimeFrom) )
    {
      NodesToDelete[(iNode->second).id]=0;
//      std::cout << "Found node in INNER REG, ID: " <<  (iNode->second).id << std::endl;
    }
  }//for (std::map<ID_t, Lattice::Node>::iterator iNode = nodes.begin(); iNode != nodes.end(); ++iNode)

  if( not (IsNodeInBEG && IsNodeInEND) ){
    return false;
  }
    
  for (std::map<ID_t, Lattice::Link>::iterator iLink = lattice->links.begin(); iLink != lattice->links.end(); ++iLink)
  {
    if( (lattice->nodes[(iLink->second).S].t <= FromTimeTo) && (lattice->nodes[(iLink->second).E].t > FromTimeTo) )
    {
      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, over BEG REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
    }
    
    if( (lattice->nodes[(iLink->second).S].t < ToTimeFrom) && (lattice->nodes[(iLink->second).E].t >= ToTimeFrom) )
    {
      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, over END REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
    }

//    if( (nodes[(iLink->second).S].t >= FromTimeFrom) && (nodes[(iLink->second).E].t <= ToTimeTo) )
//    {
//      LinksToDelete[(iLink->second).id]=0;
//      std::cout << "Found link to be deleted, in OUTER REG, ID: " <<  (iLink->second).id << " from " << (iLink->second).S << " to " << (iLink->second).E << std::endl;
//    }
  }//for links

  
  //std::cout << "Erasing Link ";
  for (std::map<lse::ID_t,int>::iterator iLinkID = LinksToDelete.begin(); iLinkID != LinksToDelete.end(); ++iLinkID)
  {
    //std::cout << (*iLinkID) << " ";
    lattice->deleteLink((iLinkID->first));
  }//delete links
//  std::cout << std::endl;
  
//  std::cout << "Erasing Node ";
  for (std::map<lse::ID_t,int>::iterator iNodeID = NodesToDelete.begin(); iNodeID != NodesToDelete.end(); ++iNodeID)
  {
    //std::cout << (*iNodeID) << " ";
    lattice->deleteNode((iNodeID->first));
  }//delete nodes
//  std::cout << std::endl;

  
  return true;
}// bool  C_Lattice_Modifications::cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_NodeVector &from, T_NodeVector &to) {{{

// void C_Lattice_Modifications::insert1WordToLattice(T_NodeVector &from, T_NodeVector &to, std::string Label) {{{
void C_Lattice_Modifications::insert1WordToLattice(T_NodeVector &from, T_NodeVector &to, std::string Label)
{

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( lattice->bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = lattice->getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = lattice->nodes[*iNodeToID];
      tmpNode.id = LastNode;

      lattice->addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = lattice->fwdLinks[*iNodeToID].begin(); ifwdlink != lattice->fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = lattice->getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = lattice->links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        lattice->addLink(tmpLink);

        lattice->fwdLinks[LastNode].push_back(LastLink);
        lattice->bckLinks[lattice->links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if  

  }//for

  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
    {
    
        ID_t LastLink = lattice->getLastLinkID()+1;
        Lattice::Link tmpLink;

        tmpLink.init();
        tmpLink.id = LastLink;
        tmpLink.S  = *iNodeFromID;
        tmpLink.E  = *iNodeToID;
        tmpLink.a  = 0;
        tmpLink.l  = 0;

        lattice->addLink(tmpLink);

        lattice->fwdLinks[*iNodeFromID].push_back(LastLink);
        lattice->bckLinks[*iNodeToID].push_back(LastLink);
        
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;
      
    }//for nodes from nowhere
  }//for nodes to nowhere

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    lattice->nodes[*iNodeToID].W = Label;
    lattice->nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (lattice->fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (lattice->fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      lattice->links[*iFwdLinkID].l = 0;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere

}// void C_Lattice_Modifications::insert1WordToLattice(T_NodeVector &from, T_NodeVector &to, std::string Label) {{{

//void C_Lattice_Modifications::insertMoreWordToLattice(T_NodeVector &from, T_NodeVector &to, T_StringVector Labels) {{{
void C_Lattice_Modifications::insertMoreWordsToLattice(T_NodeVector &from, T_NodeVector &to, T_StringVector &Labels)
{

  float min_time=0, max_time=INFTY;
  
  for(T_NodeVector::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    if( min_time < lattice->nodes[*iNodeFromID].t){
      min_time = lattice->nodes[*iNodeFromID].t;
    }
  }
  for(T_NodeVector::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    if( max_time > lattice->nodes[*iNodeToID].t){
      max_time = lattice->nodes[*iNodeToID].t;
    }
  }

  std::vector<lse::ID_t> TermNodes;
  TermNodes.reserve(Labels.size());
  for(int iTerm = 0; iTerm < (int)(Labels.size()-1); iTerm++ )
  {
    ID_t LastNode = lattice->getLastNodeID()+1;
    Lattice::Node tmpNode;
    tmpNode.id = LastNode;
    tmpNode.W = Labels[iTerm];
    tmpNode.t_from = min_time;
    tmpNode.t = max_time;
		tmpNode.v = 1;
    lattice->addNode(tmpNode);
    TermNodes.push_back(LastNode);
    std::cerr << "Added node " << tmpNode.id << " " << tmpNode.W << std::endl;
  }//for
  
  
  //beginning of term
  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    
    ID_t LastLink = lattice->getLastLinkID()+1;
    Lattice::Link tmpLink;

    tmpLink.init();
    tmpLink.id = LastLink;
    tmpLink.S  = *iNodeFromID;
    tmpLink.E  = TermNodes[0];
    tmpLink.a  = 0;
    tmpLink.l  = 0;

    lattice->addLink(tmpLink);

    lattice->fwdLinks[*iNodeFromID].push_back(LastLink);
    lattice->bckLinks[TermNodes[0]].push_back(LastLink);
    std::cerr << "Link added between " << *iNodeFromID << " and " << TermNodes[0] << std::endl;

  }//for nodes to nowhere

  //end of term
  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( lattice->bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = lattice->getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = lattice->nodes[*iNodeToID];
      tmpNode.id = LastNode;

      lattice->addNode(tmpNode);

      std::cerr << "Node " << *iNodeToID << " has backlink from Node " << lattice->links[*(lattice->bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = lattice->fwdLinks[*iNodeToID].begin(); ifwdlink != lattice->fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = lattice->getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = lattice->links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        lattice->addLink(tmpLink);

        lattice->fwdLinks[LastNode].push_back(LastLink);
        lattice->bckLinks[lattice->links[*ifwdlink].E].push_back(LastLink);

        std::cerr << "Adding link " << LastLink << " from " << LastNode << " to " << lattice->links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if
  }
  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
    ID_t LastLink = lattice->getLastLinkID()+1;
    Lattice::Link tmpLink;

    tmpLink.init();
    tmpLink.id = LastLink;
    tmpLink.S  = TermNodes[Labels.size()-2];
    tmpLink.E  = *iNodeToID;
    tmpLink.a  = 0;
    tmpLink.l  = 0;

    lattice->addLink(tmpLink);

    lattice->fwdLinks[TermNodes[Labels.size()-2]].push_back(LastLink);
    lattice->bckLinks[*iNodeToID].push_back(LastLink);

    lattice->nodes[*iNodeToID].W = Labels[Labels.size()-1];
    lattice->nodes[*iNodeToID].v = 1;
    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (lattice->fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (lattice->fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      lattice->links[*iFwdLinkID].l = 0;
      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere
  
  //for nodes inside the term
  for(int iNodeID = 1; iNodeID <= (int)(TermNodes.size()-1); iNodeID++ ){
    std::cerr << TermNodes.size() << " " << iNodeID << std::endl;
    ID_t LastLink = lattice->getLastLinkID()+1;
    Lattice::Link tmpLink;

    tmpLink.init();
    tmpLink.id = LastLink;
    tmpLink.S  = TermNodes[iNodeID-1];
    tmpLink.E  = TermNodes[iNodeID];
    tmpLink.a  = 0;
    tmpLink.l  = 0;

    lattice->addLink(tmpLink);

    lattice->fwdLinks[TermNodes[iNodeID-1]].push_back(LastLink);
    lattice->bckLinks[TermNodes[iNodeID]].push_back(LastLink);
    std::cerr << "Adding link inside term " << tmpLink.id << " from " << tmpLink.S << " to " << tmpLink.E << std::endl;
  }//for
}// void C_Lattice_Modifications::insertMoreWordToLattice(T_NodeVector &from, T_NodeVector &to, T_StringVector Labels) }}}



// void C_Lattice_Modifications::insert1WordToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label, std::string WordInLm, LanguageModel &LM) {{{
void C_Lattice_Modifications::insert1WordToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label, std::string WordInLm)
{                 

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( lattice->bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = lattice->getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = lattice->nodes[*iNodeToID];
      tmpNode.id = LastNode;

      lattice->addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = lattice->fwdLinks[*iNodeToID].begin(); ifwdlink != lattice->fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = lattice->getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = lattice->links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        lattice->addLink(tmpLink);

        lattice->fwdLinks[LastNode].push_back(LastLink);
        lattice->bckLinks[lattice->links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if  

  }//for

  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
    {
    
        ID_t LastLink = lattice->getLastLinkID()+1;
        Lattice::Link tmpLink;

        tmpLink.init();
        tmpLink.id = LastLink;
        tmpLink.S  = *iNodeFromID;
        tmpLink.E  = *iNodeToID;
        tmpLink.a  = 0;
        tmpLink.l  = LM->GetWordNGramProb(DeleteBackslash((lattice->nodes[*iNodeFromID].W)+" "+WordInLm))*2.3026;
        //std::cout << DeleteBackslash(nodes[*iNodeFromID].W) << " " << DeleteBackslash(WordInLm) << " " << (tmpLink.l) << std::endl;

        lattice->addLink(tmpLink);

        lattice->fwdLinks[*iNodeFromID].push_back(LastLink);
        lattice->bckLinks[*iNodeToID].push_back(LastLink);
        
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;
      
    }//for nodes from nowhere
  }//for nodes to nowhere

  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    lattice->nodes[*iNodeToID].W = Label;
    lattice->nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (lattice->fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (lattice->fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      lattice->links[*iFwdLinkID].l = LM->GetWordNGramProb(DeleteBackslash(WordInLm+" "+(lattice->nodes[lattice->links[*iFwdLinkID].E].W)))*2.3026;
      //std::cout << DeleteBackslash(WordInLm) << " " << DeleteBackslash(nodes[links[*iFwdLinkID].E].W) << " " << (links[*iFwdLinkID].l) << std::endl;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere

}//void C_Lattice_Modifications::insert1WordToLatticeWithLM(std::vector<ID_t> &from, std::vector<ID_t> &to, std::string Label, std::string WordInLm, LanguageModel &LM) }}}


//void C_Lattice_Modifications::insertMoreWordsToLatticeWithLM(T_NodeVector &from, T_NodeVector &to, T_StringVector &Labels, T_StringVector WordsInLm, LanguageModel &LM) {{{
void C_Lattice_Modifications::insertMoreWordsToLatticeWithLM(T_NodeVector &from, T_NodeVector &to, T_StringVector &Labels, T_StringVector &WordsInLm)
{

  float min_time=0, max_time=INFTY;
  
  for(T_NodeVector::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    if( min_time < lattice->nodes[*iNodeFromID].t){
      min_time = lattice->nodes[*iNodeFromID].t;
    }
  }
  for(T_NodeVector::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID )
  {
    if( max_time > lattice->nodes[*iNodeToID].t){
      max_time = lattice->nodes[*iNodeToID].t;
    }
  }

  std::vector<lse::ID_t> TermNodes;
  TermNodes.reserve(Labels.size());
  for(int iTerm = 0; iTerm < (int)(Labels.size()-1); iTerm++ )
  {
    ID_t LastNode = lattice->getLastNodeID()+1;
    Lattice::Node tmpNode;
    tmpNode.id = LastNode;
    tmpNode.W = Labels[iTerm];
    tmpNode.t_from = min_time;
    tmpNode.t = max_time;
		tmpNode.v = 1;
    lattice->addNode(tmpNode);
    TermNodes.push_back(LastNode);
  }//for
  
  
  //beginning of term
  for(std::vector<ID_t>::iterator iNodeFromID = from.begin(); iNodeFromID != from.end(); ++iNodeFromID )
  {
    
    ID_t LastLink = lattice->getLastLinkID()+1;
    Lattice::Link tmpLink;

    tmpLink.init();
    tmpLink.id = LastLink;
    tmpLink.S  = *iNodeFromID;
    tmpLink.E  = TermNodes[0];
    tmpLink.a  = 0;
    tmpLink.l  = LM->GetWordNGramProb(DeleteBackslash((lattice->nodes[*iNodeFromID].W)+" "+WordsInLm[0]))*2.3026;;

    lattice->addLink(tmpLink);

    lattice->fwdLinks[*iNodeFromID].push_back(LastLink);
    lattice->bckLinks[TermNodes[0]].push_back(LastLink);
 //     std::cout << "Link added between " << *iNodeFromID << " and " << *iNodeToID << std::endl;

  }//for nodes to nowhere

  //end of term
  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
      
    if( lattice->bckLinks[*iNodeToID].size() != 0 ){
      ID_t LastNode = lattice->getLastNodeID()+1;
      Lattice::Node tmpNode;
      tmpNode = lattice->nodes[*iNodeToID];
      tmpNode.id = LastNode;

      lattice->addNode(tmpNode);

 //     std::cout << "Node " << *iNodeToID << " has backlink from Node " << links[*(bckLinks[*iNodeToID].begin())].S << " copying this node to new one " << LastNode << std::endl;
      
      for(std::vector<ID_t>::iterator ifwdlink = lattice->fwdLinks[*iNodeToID].begin(); ifwdlink != lattice->fwdLinks[*iNodeToID].end(); ++ifwdlink ){

        ID_t LastLink = lattice->getLastLinkID()+1;
        Lattice::Link tmpLink;
        tmpLink = lattice->links[*ifwdlink];
        
        tmpLink.id = LastLink;
        tmpLink.S  = LastNode;
        tmpLink.l  = 0;

        lattice->addLink(tmpLink);

        lattice->fwdLinks[LastNode].push_back(LastLink);
        lattice->bckLinks[lattice->links[*ifwdlink].E].push_back(LastLink);

 //       std::cout << "Adding link " << LastLink << " from " << LastNode << " to " << links[*ifwdlink].E << std::endl;
        
      }//for

      if( iNodeToID != to.begin()){
        std::vector<ID_t>::iterator delme;
        delme=iNodeToID;
        iNodeToID--;
        to.erase(delme);
      }else{
        to.erase(iNodeToID);
        iNodeToID=to.begin();
      }
      to.push_back(LastNode);
    }//if
  }
  for(std::vector<ID_t>::iterator iNodeToID = to.begin(); iNodeToID != to.end(); ++iNodeToID ){
    ID_t LastLink = lattice->getLastLinkID()+1;
    Lattice::Link tmpLink;

    tmpLink.init();
    tmpLink.id = LastLink;
    tmpLink.S  = TermNodes[Labels.size()-2];
    tmpLink.E  = *iNodeToID;
    tmpLink.a  = 0;
    tmpLink.l  = LM->GetWordNGramProb(DeleteBackslash(WordsInLm[Labels.size()-2]+" "+WordsInLm[Labels.size()-1]))*2.3026;

    lattice->addLink(tmpLink);

    lattice->fwdLinks[TermNodes[Labels.size()-2]].push_back(LastLink);
    lattice->bckLinks[*iNodeToID].push_back(LastLink);

    lattice->nodes[*iNodeToID].W = Labels[Labels.size()-1];
    lattice->nodes[*iNodeToID].v = 1;
//    std::cout << "Label correction " << *iNodeToID << std::endl;
    for(std::vector<ID_t>::iterator iFwdLinkID = (lattice->fwdLinks[*iNodeToID]).begin(); iFwdLinkID != (lattice->fwdLinks[*iNodeToID]).end(); ++iFwdLinkID ){
      lattice->links[*iFwdLinkID].l = LM->GetWordNGramProb(DeleteBackslash(WordsInLm[Labels.size()-1]+" "+(lattice->nodes[lattice->links[*iFwdLinkID].E].W)))*2.3026;
//      std::cout << "Language score deleting " << *iFwdLinkID << std::endl;
    }  
  }//for nodes from nowhere
  
  //for nodes inside the term
  for(int iNodeID = 1; iNodeID <= (int)(TermNodes.size()-1); iNodeID++ ){
    ID_t LastLink = lattice->getLastLinkID()+1;
    Lattice::Link tmpLink;

    tmpLink.init();
    tmpLink.id = LastLink;
    tmpLink.S  = TermNodes[iNodeID-1];
    tmpLink.E  = TermNodes[iNodeID];
    tmpLink.a  = 0;
    tmpLink.l  = LM->GetWordNGramProb(DeleteBackslash(WordsInLm[iNodeID-1]+" "+WordsInLm[iNodeID]))*2.3026;

    lattice->addLink(tmpLink);

    lattice->fwdLinks[TermNodes[iNodeID-1]].push_back(LastLink);
    lattice->bckLinks[TermNodes[iNodeID]].push_back(LastLink);
  }//for
}// void C_Lattice_Modifications::insertMoreWordToLattice(T_NodeVector &from, T_NodeVector &to, T_StringVector Labels) }}}

// void C_Lattice_Modifications::insertTermToLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels) {{{
bool C_Lattice_Modifications::insertTermToLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels)
{
  T_NodeVector from, to;
  if( cutOffPieceOfLattice(FromTimeFrom-outer_bound, FromTimeTo+inner_bound, ToTimeFrom-inner_bound, ToTimeTo+outer_bound, from, to) ){
    if( Labels.size() == 1){
      insert1WordToLattice(from, to, Labels[0]);
    }else if(  Labels.size() > 1){
      insertMoreWordsToLattice(from, to, Labels);
    }//if
    lattice->cleanLattice();
		return true;
	}else{
	  return false;
	}	
}// void C_Lattice_Modifications::insertTermToLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels) }}}


// void C_Lattice_Modifications::insertTermToLatticeWithLM(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels, T_StringVector &WordsInLm){{{
bool C_Lattice_Modifications::insertTermToLatticeWithLM(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels, T_StringVector &WordsInLm)
{
  T_NodeVector from, to;
  if( LM != NULL){
    if( cutOffPieceOfLattice(FromTimeFrom-outer_bound, FromTimeTo+inner_bound, ToTimeFrom-inner_bound, ToTimeTo+outer_bound, from, to) ){
      if( Labels.size() == 1 ){
        insert1WordToLatticeWithLM(from, to, Labels[0], WordsInLm[0]);
      }else if( Labels.size() > 1 ){
        insertMoreWordsToLatticeWithLM(from, to, Labels, WordsInLm);
      }//if
      lattice->cleanLattice();
			return true;
		}else{
		  return false;
		}	
  }else{
	  return false;
	}
}//void C_Lattice_Modifications::insertTermToLatticeWithLM(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels, T_StringVector &WordsInLm)}}}

// void C_Lattice_Modifications::relaxLatticeNodesInInterval(LatTime FromTime, LatTime ToTime) {{{
void C_Lattice_Modifications::relaxLatticeNodesInInterval(LatTime FromTime, LatTime ToTime)
{
  for (std::map<ID_t, Lattice::Link>::iterator iLink = lattice->links.begin(); iLink != lattice->links.end(); ++iLink){
	  if( ( lattice->nodes[(iLink->second).S].t <= ToTime )  && ( lattice->nodes[(iLink->second).E].t >= FromTime )){
			  
			if(  lattice->nodes[(iLink->second).E].t < ToTime ){
				lattice->nodes[(iLink->second).E].t = ToTime;
			}
				
				
		  if(  lattice->nodes[(iLink->second).S].t >= FromTime ){
//			  lattice->nodes[(iLink->second).S].t_from = FromTime;
				lattice->nodes[(iLink->second).S].t = ToTime;
				lattice->nodes[(iLink->second).E].t_from = FromTime;
			}
			
			
		}	
	}//for
}// void C_Lattice_Modifications::relaxLatticeNodesInInterval(LatTime FromTime, LatTime ToTime) }}}


