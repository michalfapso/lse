#ifndef LATTICE_MODIFICATIONS_H
#define LATTICE_MODIFICATIONS_H

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>
#include "lat.h"
#include "languagemodel.h"
 
#define LAT_ERROR_IO	2
#define INFTY 9.99e99

namespace lse {

class C_Lattice_Modifications {
    Lattice * lattice;
    LanguageModel * LM;

  public:
    typedef std::vector<lse::ID_t> T_NodeVector; //term
    typedef std::vector<std::string> T_StringVector; //term

    float outer_bound;
    float inner_bound;

    
    C_Lattice_Modifications(){ lattice=NULL; LM=NULL; outer_bound=0; inner_bound=0;};
    ~C_Lattice_Modifications(){};
  
    void SetLattice(Lattice * in_lattice){lattice=in_lattice;};
    Lattice * GetLattice(){return lattice;};
    void SetLM(LanguageModel * in_LM){LM=in_LM;};
    LanguageModel * GetLM(){return LM;};

    void SetOuterBoundary(float in_outer){outer_bound = in_outer;};
    void SetInnerBoundary(float in_inner){inner_bound = in_inner;};
  
  
    bool insertTermToLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels);
    bool insertTermToLatticeWithLM(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, T_StringVector &Labels, T_StringVector &WordsInLm);

    bool cutOffPieceOfLattice(LatTime FromTimeFrom, LatTime FromTimeTo, LatTime ToTimeFrom, LatTime ToTimeTo, std::vector<ID_t> &from, std::vector<ID_t> &to);
    
    void insert1WordToLattice(T_NodeVector &from, T_NodeVector &to, std::string Label);
    void insertMoreWordsToLattice(T_NodeVector &from, T_NodeVector &to, T_StringVector &Labels);
    
    void insert1WordToLatticeWithLM(T_NodeVector &from, T_NodeVector &to, std::string Label, std::string WordInLm);
    void insertMoreWordsToLatticeWithLM(T_NodeVector &from, T_NodeVector &to, T_StringVector &Labels, T_StringVector &WordsInLm);

    void relaxLatticeNodesInInterval(LatTime FromTime, LatTime ToTime);

};



} // namespace lse

#endif
