#!/bin/bash

cd data

cat << EOF | sqlite3 lse.db
delete from docs;
delete from MKwsDictionaryLvcsr;
delete from sqlite_sequence;
EOF

#cat ../../../../../scripts/create_sqlite3_db.sql | sqlite3 lse.db

rm -rf output
mkdir output
mkdir output/idx

cd ..

#gdb \
#valgrind --leak-check=full \
./test_MKws
