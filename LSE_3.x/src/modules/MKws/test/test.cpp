#include <boost/test/included/unit_test_framework.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include <boost/asio.hpp>

#include "IndexingServer.h" 
#include "IndexingClient.h" 
#include "ConfigFile.h"
#include "MKwsDictionaryMem.h"

#include "sqlite3.h"

#define MODULE_ID "KWS"
#define HOSTNAME "localhost"
#define PORT "30000"
#define PORTint 30000

#define SERVER_OUT_REDIRECT "indexing_server.out"

#define ROOT_DIR "/home/miso/projects/LSE/branches/LSE_3.x"

using namespace boost::unit_test_framework;
using namespace sem;
using namespace std;
namespace fs = boost::filesystem;

// get_config() {{{
static ConfigFile* get_config()
{
	ConfigFile *config = new ConfigFile();
	config->Value("Main", "database", ROOT_DIR "/data/lse.db");
	config->Value("MKws_indexer", "p_search_index", (fs::initial_path() / "data/output/idx").string());
	config->Value("MKws_indexer", "p_compute_links_likelihood", true);
	config->Value("MKws_indexer", "p_htk_remove_backslash", true);
	config->Value("MKws_indexer", "idx_dir_lvcsr", (fs::initial_path() / "data/output/idx").string());
	config->Value("MKws_indexer", "p_data_type", "LVCSR"); // we will index LVCSR data
	config->Value("MKws", "work_with_lvcsr", "true");
	config->Value("MKws", "work_with_phn", "false");
	return config;
}
// }}}

// Test_IndexingClient {{{
class Test_IndexingClient : public IndexingClient
{
	public:
		Test_IndexingClient(ConfigFile* pConfig, std::string hostname, std::string port)
			:
			IndexingClient(pConfig, hostname, port) {}

		ID SendAssignIdMsg(const std::string &moduleId, const std::string &msg)
		{
			DBG("Test_IndexingClient::SendAssignIdMsg("<<moduleId<<", \""<<msg<<"\")");
			ID id = this->IndexingClient::SendAssignIdMsg(moduleId, msg);
			return id;
		}
		void SendForwardIndexToServer(const std::string &moduleId, const std::string &fwdIndex)
		{
			DBG("Test_IndexingClient::SendForwardIndexToServer("<<moduleId<<", \""<<fwdIndex<<"\")");
			this->IndexingClient::SendForwardIndexToServer(moduleId, fwdIndex);
		}
};
// }}}

//---------------------------------------------------------------------------

// start_indexing_server() {{{
void start_indexing_server()
{
	ConfigFile *config = get_config();

    boost::asio::io_service io_service;
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), PORTint);
	IndexingServer *srv = new IndexingServer(config, io_service, endpoint);
	io_service.run();

	DBG("IndexingServer FINISHED");
	delete srv;
}
// }}}

// test_MKwsDictionary() {{{
void test_MKwsDictionary()
{
	DBG("--------------------------------------------------");
	DBG("test_MKwsDictionary");
	DBG("--------------------------------------------------");
	ID id = ID_INVALID;
	
	// create indexing client with modules container
	ConfigFile *config = get_config();
	Test_IndexingClient *cli = new Test_IndexingClient(config, HOSTNAME, PORT);
	ModulesContainer *modules = cli->GetModules();

	// get the tested module and it's dictionary
	MKws* m = dynamic_cast<MKws*>(modules->GetModule(MODULE_ID));
	assert(m);
	MKwsDictionary* dict = m->GetDictionaryLvcsr();
	assert(dict);

	BOOST_CHECK_EQUAL(dict->GetUnitId("blabla"), ID_INVALID);

	id = dict->AddUnit("blabla");
	BOOST_CHECK_EQUAL(dict->GetUnitId("blabla"), id);

	id = dict->AddUnit("blablaa");
	BOOST_CHECK_EQUAL(dict->GetUnitId("blablaa"), id);
	BOOST_CHECK(dict->GetUnitId("blabla") != dict->GetUnitId("blablaa"));

	id = dict->AddUnit("blabla");
	DBG("id_blabla="<<id);

	delete cli;
	delete config;
}
// }}}

// test_MKwsIndexer() {{{
void test_MKwsIndexer()
{
	DBG("--------------------------------------------------");
	DBG("test_MKwsIndexer");
	DBG("--------------------------------------------------");
	ID id = ID_INVALID;
	
	ConfigFile *config = get_config();

	Test_IndexingClient *cli = new Test_IndexingClient(config, HOSTNAME, PORT);
	ModulesContainer *modules = cli->GetModules();
//	cli->Start();
	MKws* m = dynamic_cast<MKws*>(modules->GetModule(MODULE_ID));
	assert(m);

	// fill the index
	id = cli->GetDocId("document1");
	m->Index(id, "data/input/document1.lat");

	delete cli;
	delete config;
}
// }}}

// test_MIndexerStruct() {{{
void test_MIndexerStruct()
{
	DBG("--------------------------------------------------");
	DBG("test_MIndexerStruct");
	DBG("--------------------------------------------------");
	
	ConfigFile *config = get_config();

	Test_IndexingClient *cli = new Test_IndexingClient(config, HOSTNAME, PORT);
	ModulesContainer *modules = cli->GetModules();
	MKws* m = dynamic_cast<MKws*>(modules->GetModule(MODULE_ID));
	assert(m);
	//MKwsIndexer* idx = m->GetIndexer();

	// MIndexerStruct::DocumentsSortedByRelevance::Record {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		// Read / Write
		MIndexerStruct::DocumentsSortedByRelevance::Record r1(3, 3.3);
		MIndexerStruct::DocumentsSortedByRelevance::Record r2;
		r1.Write(ss);
		r2.Read(ss);
		BOOST_CHECK_EQUAL(r1, r2);

		// CmpSort
		MIndexerStruct::DocumentsSortedByRelevance::Record r3(3, 2.3);
		BOOST_CHECK_EQUAL(MIndexerStruct::DocumentsSortedByRelevance::Record::CmpSort(r1, r3), true);
	}
	// }}}
	
	// MIndexerStruct::DocumentsSortedById::Record {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		// Read / Write
		MIndexerStruct::DocumentsSortedById::Record r1(2, 0, 3);
		MIndexerStruct::DocumentsSortedById::Record r2;
		r1.Write(ss);
		r2.Read(ss);
		BOOST_CHECK_EQUAL(r1, r2);

		// CmpSort
		MIndexerStruct::DocumentsSortedById::Record r3(3, 3, 2);
		BOOST_CHECK_EQUAL(MIndexerStruct::DocumentsSortedById::Record::CmpSort(r1, r3), true);
	}
	// }}}
	
	// MIndexerStruct::Hits_OneDocHitList::Record {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		// Read / Write
		MIndexerStruct::Hits_OneDocHitList::Record r1(0.8, 13.2, 13.8);
		MIndexerStruct::Hits_OneDocHitList::Record r2;
		r1.Write(ss);
		r2.Read(ss);
		BOOST_CHECK_EQUAL(r1, r2);

		// CmpSort
		MIndexerStruct::Hits_OneDocHitList::Record r3(0.9, 13.2, 13.8);
		BOOST_CHECK_EQUAL(MIndexerStruct::Hits_OneDocHitList::Record::CmpScore(r1, r3), true);
	}
	// }}}
	
	// MIndexerStruct::DocumentsSortedByRelevance {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		MIndexerStruct::DocumentsSortedByRelevance d;

		MIndexerStruct::DocumentsSortedByRelevance::Record r1(3, 4.0);
		MIndexerStruct::DocumentsSortedByRelevance::Record r2(2, 3.0);
		MIndexerStruct::DocumentsSortedByRelevance::Record r3(5, 5.0);
		d.AddRecord(r1);
		d.AddRecord(r2);
		d.AddRecord(r3);
		BOOST_CHECK_EQUAL(d.IsSorted(), false);

		// Write / Read not sorted
		d.Write(ss);
		MIndexerStruct::DocumentsSortedByRelevance d2;
		d2.Read(ss, false); // not sorted
		BOOST_CHECK_EQUAL(d.IsSorted(), d2.IsSorted());
		BOOST_CHECK_EQUAL(d, d2);

		// Sorting
		d.Sort();
		BOOST_CHECK_EQUAL(d.IsSorted(), true);
		BOOST_CHECK_EQUAL(d[0], r3);
		BOOST_CHECK_EQUAL(d[1], r1);
		BOOST_CHECK_EQUAL(d[2], r2);

		// Write / Read sorted
		stringstream ss2(stringstream::in | stringstream::out);
		d.Write(ss2);
		MIndexerStruct::DocumentsSortedByRelevance d3;
		d3.Read(ss2, true);
		BOOST_CHECK_EQUAL(d, d3);
	}
	// }}}

	// MIndexerStruct::DocumentsSortedById {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		MIndexerStruct::DocumentsSortedById d;

		MIndexerStruct::DocumentsSortedById::Record r1(3, 2, 2);
		MIndexerStruct::DocumentsSortedById::Record r2(2, 0, 2);
		MIndexerStruct::DocumentsSortedById::Record r3(5, 5, 2);
		d.AddRecord(r1);
		d.AddRecord(r2);
		d.AddRecord(r3);
		BOOST_CHECK_EQUAL(d.IsSorted(), false);

		// Write / Read not sorted
		d.Write(ss);
		MIndexerStruct::DocumentsSortedById d2;
		d2.Read(ss, false); // not sorted
		BOOST_CHECK_EQUAL(d.IsSorted(), d2.IsSorted());
		BOOST_CHECK_EQUAL(d, d2);

		// Sorting
		d.Sort();
		BOOST_CHECK_EQUAL(d.IsSorted(), true);
		BOOST_CHECK_EQUAL(d[0], r2);
		BOOST_CHECK_EQUAL(d[1], r1);
		BOOST_CHECK_EQUAL(d[2], r3);

		// Write / Read sorted
		stringstream ss2(stringstream::in | stringstream::out);
		d.Write(ss2);
		MIndexerStruct::DocumentsSortedById d3;
		d3.Read(ss2, true);
		BOOST_CHECK_EQUAL(d, d3);
	}
	// }}}
	
	// MIndexerStruct::Hits_OneDocHitList {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		{
			MIndexerStruct::Hits_OneDocHitList d;

			MIndexerStruct::Hits_OneDocHitList::Record r1(3.6, 0, 0.07);
			MIndexerStruct::Hits_OneDocHitList::Record r2(5.4, 0.11, 0.13);
			MIndexerStruct::Hits_OneDocHitList::Record r3(3.2, 0, 0.11);
			d.AddRecord(r1, true);
			d.AddRecord(r2, true);
			d.AddRecord(r3, true);
		}

		{
			MIndexerStruct::Hits_OneDocHitList d;

			MIndexerStruct::Hits_OneDocHitList::Record r1(-3.014466047287, 0, 0);
			MIndexerStruct::Hits_OneDocHitList::Record r2(-13.27176856995, 0.02999999932945, 0.09000000357628);
			MIndexerStruct::Hits_OneDocHitList::Record r3(-67.71875	, 0.2000000029802, 0.2000000029802);
			MIndexerStruct::Hits_OneDocHitList::Record r4(-3.758707523346, 0.2300000041723, 0.2300000041723);
			MIndexerStruct::Hits_OneDocHitList::Record r5(-117.7342376709, 0.2099999934435, 0.2099999934435);
			MIndexerStruct::Hits_OneDocHitList::Record r6(-88.03457641602, 0.2199999988079, 0.2199999988079);
			MIndexerStruct::Hits_OneDocHitList::Record r7(-65.94648742676, 0.3799999952316, 0.3799999952316);
			MIndexerStruct::Hits_OneDocHitList::Record r8(-66.93514251709, 0.2800000011921, 0.2800000011921);
			MIndexerStruct::Hits_OneDocHitList::Record r9(-2.534952640533, 0.3499999940395, 0.3499999940395);
			MIndexerStruct::Hits_OneDocHitList::Record r10(-6.575932025909, 0.589999973774, 0.589999973774);
			MIndexerStruct::Hits_OneDocHitList::Record r11(-125.3705062866, 0.6000000238419, 0.6000000238419);
			MIndexerStruct::Hits_OneDocHitList::Record r12(-106.6968460083, 0.589999973774, 0.6399999856949);
			d.AddRecord(r1, true);
			d.AddRecord(r2, true);
			d.AddRecord(r3, true);
			d.AddRecord(r4, true);
			d.AddRecord(r5, true);
			d.AddRecord(r6, true);
			d.AddRecord(r7, true);
			d.AddRecord(r8, true);
			d.AddRecord(r9, true);
			d.AddRecord(r10, true);
			d.AddRecord(r11, true);
			d.AddRecord(r12, true);
		}

		{
			// Indexing of overlapped hits
			MIndexerStruct::Hits_OneDocHitList d;

			MIndexerStruct::Hits_OneDocHitList::Record r1(3.6, 12.5, 12.9);
			MIndexerStruct::Hits_OneDocHitList::Record r2(5.4, 2.2, 2.5);
			MIndexerStruct::Hits_OneDocHitList::Record r3(3.2, 2.4, 2.7);
			MIndexerStruct::Hits_OneDocHitList::Record r4(5.1, 5.8, 6.3);
			MIndexerStruct::Hits_OneDocHitList::Record r5(3.2, 2.6, 2.9);
			d.AddRecord(r1, true);
			d.AddRecord(r2, true);
			d.AddRecord(r3, true);
			d.AddRecord(r4, true);
			d.AddRecord(r5, true);
			// Only 3 hits should remain in the index after filtering the overlapped ones
			BOOST_CHECK_EQUAL(d.GetSize(), 3u);
			// The best score has one of the overlapping hits
			BOOST_CHECK_CLOSE(d.GetBestScore(), (Score)5.4, 0.0001);

			cout << d << endl;

			// Write / Read
			d.Write(ss);
			MIndexerStruct::Hits_OneDocHitList d2;
			d2.Read(ss);
			BOOST_CHECK_EQUAL(d, d2);
		}
	}
	// }}}
	
	// MIndexerStruct::Hits {{{
	{
		stringstream ss(stringstream::in | stringstream::out);

		MIndexerStruct::Hits_OneDocHitList ho1;

		MIndexerStruct::Hits_OneDocHitList::Record r1(3.6, 12.5, 12.9);
		MIndexerStruct::Hits_OneDocHitList::Record r2(2.4, 2.4, 2.7);
		MIndexerStruct::Hits_OneDocHitList::Record r3(5.1, 5.8, 6.3);
		ho1.AddRecord(r1, true);
		ho1.AddRecord(r2, true);
		ho1.AddRecord(r3, true);

		MIndexerStruct::Hits_OneDocHitList ho2;

		MIndexerStruct::Hits_OneDocHitList::Record r4(4.7, 13.6, 13.10);
		MIndexerStruct::Hits_OneDocHitList::Record r5(3.5, 3.5, 3.8);
		MIndexerStruct::Hits_OneDocHitList::Record r6(6.2, 6.9, 7.4);
		ho2.AddRecord(r4, true);
		ho2.AddRecord(r5, true);
		ho2.AddRecord(r6, true);

		MIndexerStruct::Hits h;

		// Write / Read stringstream
		int start_pos = 0;

		start_pos = h.AddHits(ss, &ho1); // writes only to stream
		BOOST_CHECK_EQUAL(start_pos, 0);
		start_pos = h.AddHits(ss, &ho2);
		BOOST_CHECK_EQUAL(start_pos, 3);

		start_pos = h.AddHits(&ho1); // writes only to memory
		BOOST_CHECK_EQUAL(start_pos, 0);
		start_pos = h.AddHits(&ho2);
		BOOST_CHECK_EQUAL(start_pos, 3);

		MIndexerStruct::Hits h2;
		h2.Read(ss, false); // not sorted
		BOOST_CHECK_EQUAL(h, h2);

		// Write / Read file on disk
		fs::path dir_hits = fs::initial_path() / "data/output/m_hits";
		if (fs::exists(dir_hits))
		{
			CERR("ERROR: Remove the directory \""<<dir_hits<<"\"");
			EXIT();
		}
		fs::create_directory(dir_hits);

		start_pos = h.AddHits((dir_hits / "1").string(), &ho1);
		BOOST_CHECK_EQUAL(start_pos, 0);
		start_pos = h.AddHits((dir_hits / "1").string(), &ho2);
		BOOST_CHECK_EQUAL(start_pos, 3);
	}
	// }}}
	
	{
		// MIndexerStruct::FwdLabelsContainer {{{
		stringstream ss(stringstream::in | stringstream::out);

		MIndexerStruct::FwdLabel* fl1 = new MIndexerStruct::FwdLabel;
		MIndexerStruct::Hits_OneDocHitList::Record r1(3.6, 12.5, 12.9);
		MIndexerStruct::Hits_OneDocHitList::Record r2(2.4, 2.4, 2.7);
		MIndexerStruct::Hits_OneDocHitList::Record r3(5.1, 5.8, 6.3);
		fl1->AddHit(r1, true);
		fl1->AddHit(r2, true);
		fl1->AddHit(r3, true);

		MIndexerStruct::FwdLabel* fl2 = new MIndexerStruct::FwdLabel;
		MIndexerStruct::Hits_OneDocHitList::Record r4(4.7, 13.6, 13.10);
		MIndexerStruct::Hits_OneDocHitList::Record r5(3.5, 3.5, 3.8);
		MIndexerStruct::Hits_OneDocHitList::Record r6(6.2, 6.9, 7.4);
		fl2->AddHit(r4, true);
		fl2->AddHit(r5, true);
		fl2->AddHit(r6, true);

		MIndexerStruct::FwdLabel* fl3 = new MIndexerStruct::FwdLabel;
		MIndexerStruct::Hits_OneDocHitList::Record r7(5.7, 14.6, 14.10);
		MIndexerStruct::Hits_OneDocHitList::Record r8(4.5, 4.5, 4.8);
		MIndexerStruct::Hits_OneDocHitList::Record r9(7.2, 7.9, 8.4);
		fl3->AddHit(r7, true);
		fl3->AddHit(r8, true);
		fl3->AddHit(r9, true);

		MIndexerStruct::FwdLabelsContainer flc(1); // docId
		flc.AddLabel(1/*docId*/, fl1);
		flc.AddHit(2/*docId*/, r4);
		flc.AddHit(2/*docId*/, r5);
		flc.AddHit(2/*docId*/, r6);

		BOOST_CHECK_EQUAL(*flc.GetLabel(1), *fl1);
		BOOST_CHECK_EQUAL(*flc.GetLabel(2), *fl2);
		// }}}
	

		// write the indexed hits directly to disk WITHOUT CACHING
		// MIndexerStruct::InvLabel {{{
		{
			// there has to be "/0" in the dir_invlabel because it is the subdir that
			// would be assigned to this label id by InvLabelsContainer
			fs::path dir_invlabel = fs::initial_path() / "data/output/m_invlabel_nocaching/0"; 
			if (fs::exists(dir_invlabel))
			{
				CERR("ERROR: Remove the directory \""<<dir_invlabel<<"\"");
				EXIT()
			}
			fs::create_directories(dir_invlabel);

			// write the not-sorted InvLabel to disk
			{
				MIndexerStruct::InvLabel il1((dir_invlabel / "1").string());
				BOOST_CHECK_EQUAL(il1.Exists(false), false);
				BOOST_CHECK_EQUAL(il1.Exists(true), false);
				il1.AddDocumentHits_NoCache(1, fl1->GetRelevance(), fl1->GetHits());
				il1.AddDocumentHits_NoCache(2, fl2->GetRelevance(), fl2->GetHits());
			}

			// sort the InvLabel on disk
			{
				MIndexerStruct::InvLabel il2((dir_invlabel / "1").string());
				BOOST_CHECK_EQUAL(il2.Exists(false), true);
				il2.Load(false);
				il2.Sort();
				il2.Save();
				il2.RemoveUnsorted();
				BOOST_CHECK_EQUAL(il2.Exists(false), false);
			}

			// load the sorted InvLabel from disk
			{
				MIndexerStruct::InvLabel il3((dir_invlabel / "1").string());
				BOOST_CHECK_EQUAL(il3.Exists(true), true);
				il3.Load(true);
				
				MIndexerStruct::DocumentsSortedByRelevance* dr = il3.GetDocsByRelevance();
				MIndexerStruct::DocumentsSortedById* di = il3.GetDocsById();
				MIndexerStruct::Hits* h = il3.GetHits();

				BOOST_CHECK_EQUAL(dr->GetSize(), 2u);
				BOOST_CHECK_EQUAL((*dr)[0].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*dr)[1].GetDocId(), 1);
				BOOST_CHECK((*dr)[0].GetRelevance() >= (*dr)[1].GetRelevance());

				BOOST_CHECK_EQUAL(di->GetSize(), 2u);
				BOOST_CHECK_EQUAL((*di)[0].GetDocId(), 1);
				BOOST_CHECK_EQUAL((*di)[1].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListStartPos(), 0u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListStartPos(), 3u);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListRecordsCount(), 3u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListRecordsCount(), 3u);

				BOOST_CHECK_EQUAL(h->GetSize(), fl1->GetHits()->GetSize() + fl2->GetHits()->GetSize());
				BOOST_CHECK_EQUAL((*h)[0], r2);
				BOOST_CHECK_EQUAL((*h)[3], r5);
			}

			// add more unsorted hits to the inverted label
			{
				MIndexerStruct::InvLabel il4((dir_invlabel / "1").string());
				il4.AddDocumentHits_NoCache(3, fl3->GetRelevance(), fl3->GetHits());
			}

			// combine the sorted and unsorted hits and resort them
			{
				MIndexerStruct::InvLabel il5((dir_invlabel / "1").string());
				il5.Load(true);
				il5.Load(false);
				BOOST_CHECK_EQUAL(il5.GetHits()->GetSize(), fl1->GetHits()->GetSize() + fl2->GetHits()->GetSize() + fl3->GetHits()->GetSize());
				il5.Sort();
				il5.Save();
				il5.RemoveUnsorted();
			}

			// load the sorted label and check it
			MIndexerStruct::InvLabel il6((dir_invlabel / "1").string());
			{
				BOOST_CHECK_EQUAL(il6.Exists(true), true);
				il6.Load(true);

				MIndexerStruct::DocumentsSortedByRelevance* dr = il6.GetDocsByRelevance();
				MIndexerStruct::DocumentsSortedById* di = il6.GetDocsById();
				MIndexerStruct::Hits* h = il6.GetHits();

				BOOST_CHECK_EQUAL(dr->GetSize(), 3u);
				BOOST_CHECK_EQUAL((*dr)[0].GetDocId(), 3);
				BOOST_CHECK_EQUAL((*dr)[1].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*dr)[2].GetDocId(), 1);
				BOOST_CHECK((*dr)[0].GetRelevance() >= (*dr)[1].GetRelevance());
				BOOST_CHECK((*dr)[1].GetRelevance() >= (*dr)[2].GetRelevance());

				DBG("di[2]: "<<(*di)[2]);
				BOOST_CHECK_EQUAL(di->GetSize(), 3u);
				BOOST_CHECK_EQUAL((*di)[0].GetDocId(), 1);
				BOOST_CHECK_EQUAL((*di)[1].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*di)[2].GetDocId(), 3);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListStartPos(), 0u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListStartPos(), 3u);
				BOOST_CHECK_EQUAL((*di)[2].GetHitListStartPos(), 6u);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListRecordsCount(), 3u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListRecordsCount(), 3u);
				BOOST_CHECK_EQUAL((*di)[2].GetHitListRecordsCount(), 3u);

				BOOST_CHECK_EQUAL(h->GetSize(), fl1->GetHits()->GetSize() + fl2->GetHits()->GetSize() + fl3->GetHits()->GetSize());
				BOOST_CHECK_EQUAL((*h)[0], r2);
				BOOST_CHECK_EQUAL((*h)[3], r5);
				BOOST_CHECK_EQUAL((*h)[6], r8);
			}

			// MIndexerStruct::InvLabelsContainer {{{
			MIndexerStruct::InvLabelsContainer ilc;
			DBG("remove_leaf:"<<(dir_invlabel.remove_leaf()).string());
			ilc.SetPath((dir_invlabel.remove_leaf()).string());
			MIndexerStruct::InvLabel* il7 = ilc.GetLabel(1);
			BOOST_CHECK_EQUAL(il6, *il7);
			BOOST_CHECK_EQUAL(ilc.GetLabel(1234), (MIndexerStruct::InvLabel*)NULL);
			BOOST_CHECK_EQUAL(ilc.GetLabel(1), il7);
			BOOST_CHECK(ilc.AddLabel(2) != (MIndexerStruct::InvLabel*)NULL);
			BOOST_CHECK_EQUAL(ilc.GetInvLabelPath(1), (dir_invlabel / "0/1").string());
			// }}}
		}
		// }}}

		// write the indexed hits directly to disk WITH CACHING
		// MIndexerStruct::InvLabel {{{
		{
			// there has to be "/0" in the dir_invlabel because it is the subdir that
			// would be assigned to this label id by InvLabelsContainer
			fs::path dir_invlabel = fs::initial_path() / "data/output/m_invlabel_caching/0"; 
			if (fs::exists(dir_invlabel))
			{
				CERR("ERROR: Remove the directory \""<<dir_invlabel<<"\"");
				EXIT()
			}
			fs::create_directories(dir_invlabel);

			// add hits to InvLabel and sort them in memory
			{
				MIndexerStruct::InvLabel il1((dir_invlabel / "1").string());
				BOOST_CHECK_EQUAL(il1.Exists(false), false);
				BOOST_CHECK_EQUAL(il1.Exists(true), false);
				il1.AddDocumentHits(1, fl1->GetRelevance(), fl1->GetHits());
				il1.AddDocumentHits(2, fl2->GetRelevance(), fl2->GetHits());

				// sort the InvLabel in memory
				il1.Sort();
				il1.Save();
				//il2.RemoveUnsorted(); // this is not needed here, since the unsorted index was only stored in memory.
				BOOST_CHECK_EQUAL(il1.Exists(false), false);
			}

			// load the sorted InvLabel from disk
			{
				MIndexerStruct::InvLabel il3((dir_invlabel / "1").string());
				BOOST_CHECK_EQUAL(il3.Exists(true), true);
				il3.Load(true);
				
				MIndexerStruct::DocumentsSortedByRelevance* dr = il3.GetDocsByRelevance();
				MIndexerStruct::DocumentsSortedById* di = il3.GetDocsById();
				MIndexerStruct::Hits* h = il3.GetHits();

				BOOST_CHECK_EQUAL(dr->GetSize(), 2u);
				BOOST_CHECK_EQUAL((*dr)[0].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*dr)[1].GetDocId(), 1);
				BOOST_CHECK((*dr)[0].GetRelevance() >= (*dr)[1].GetRelevance());

				BOOST_CHECK_EQUAL(di->GetSize(), 2u);
				BOOST_CHECK_EQUAL((*di)[0].GetDocId(), 1);
				BOOST_CHECK_EQUAL((*di)[1].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListStartPos(), 0u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListStartPos(), 3u);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListRecordsCount(), 3u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListRecordsCount(), 3u);

				BOOST_CHECK_EQUAL(h->GetSize(), fl1->GetHits()->GetSize() + fl2->GetHits()->GetSize());
				BOOST_CHECK_EQUAL((*h)[0], r2);
				BOOST_CHECK_EQUAL((*h)[3], r5);
			}

			// add more unsorted hits to the inverted label
			{
				MIndexerStruct::InvLabel il4((dir_invlabel / "1").string());
				il4.AddDocumentHits(3, fl3->GetRelevance(), fl3->GetHits());
				il4.SaveAppend();
			}

			// combine the sorted and unsorted hits and resort them
			{
				MIndexerStruct::InvLabel il5((dir_invlabel / "1").string());
				il5.Load(true);
				il5.Load(false);
				BOOST_CHECK_EQUAL(il5.GetHits()->GetSize(), fl1->GetHits()->GetSize() + fl2->GetHits()->GetSize() + fl3->GetHits()->GetSize());
				il5.Sort();
				il5.Save();
				il5.RemoveUnsorted();
			}

			// load the sorted label and check it
			MIndexerStruct::InvLabel il6((dir_invlabel / "1").string());
			{
				BOOST_CHECK_EQUAL(il6.Exists(true), true);
				il6.Load(true);

				MIndexerStruct::DocumentsSortedByRelevance* dr = il6.GetDocsByRelevance();
				MIndexerStruct::DocumentsSortedById* di = il6.GetDocsById();
				MIndexerStruct::Hits* h = il6.GetHits();

				BOOST_CHECK_EQUAL(dr->GetSize(), 3u);
				BOOST_CHECK_EQUAL((*dr)[0].GetDocId(), 3);
				BOOST_CHECK_EQUAL((*dr)[1].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*dr)[2].GetDocId(), 1);
				BOOST_CHECK((*dr)[0].GetRelevance() >= (*dr)[1].GetRelevance());
				BOOST_CHECK((*dr)[1].GetRelevance() >= (*dr)[2].GetRelevance());

				DBG("di[2]: "<<(*di)[2]);
				BOOST_CHECK_EQUAL(di->GetSize(), 3u);
				BOOST_CHECK_EQUAL((*di)[0].GetDocId(), 1);
				BOOST_CHECK_EQUAL((*di)[1].GetDocId(), 2);
				BOOST_CHECK_EQUAL((*di)[2].GetDocId(), 3);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListStartPos(), 0u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListStartPos(), 3u);
				BOOST_CHECK_EQUAL((*di)[2].GetHitListStartPos(), 6u);
				BOOST_CHECK_EQUAL((*di)[0].GetHitListRecordsCount(), 3u);
				BOOST_CHECK_EQUAL((*di)[1].GetHitListRecordsCount(), 3u);
				BOOST_CHECK_EQUAL((*di)[2].GetHitListRecordsCount(), 3u);

				BOOST_CHECK_EQUAL(h->GetSize(), fl1->GetHits()->GetSize() + fl2->GetHits()->GetSize() + fl3->GetHits()->GetSize());
				BOOST_CHECK_EQUAL((*h)[0], r2);
				BOOST_CHECK_EQUAL((*h)[3], r5);
				BOOST_CHECK_EQUAL((*h)[6], r8);
			}
		}
		// }}}

		// fl2 and fl3 have to be released manually, fl1 is taken care of by the flc object.
		delete fl2;
		delete fl3;
	}

	delete cli;
	delete config;
}
// }}}
 
// test_liblse() {{{
void test_liblse()
{
	ConfigFile config;
	MKws* pMKws = new MKws(&config);
	pMKws->InitGlobal(ApplicationType::EIndexingClient);
	MKwsDictionaryMem* pDictionary = new MKwsDictionaryMem(pMKws);
	pDictionary->SetLocal(true);
	lse::Lattice lat(pDictionary);
	
	DBG("pDictionary->GetLocal():"<<pDictionary->GetLocal());


	string fname = "data/input/document_NULLnodes.lat";
	float latStartTime = 0.0;
	int p_lat_time_multiplier = 1;
	bool p_only_add_words_to_lexicon = false;
	bool p_htk_remove_backslash = true;
	if(lat.loadFromHTKFile(fname, latStartTime, p_lat_time_multiplier, p_only_add_words_to_lexicon, p_htk_remove_backslash) != 0) {
		cerr << "Error occured while reading lattice file " << fname << endl << flush;
		exit (1);
	}
	lat.sortLattice();

	float p_wi_penalty = 0.0;
	float p_amscale = 1;
	float p_amscale_kwd = 0.0;
	float p_lmscale = 1;
	float p_lmscale_kwd = 0.0;
	bool p_use_baumwelsh = false;
	float p_logAdd_treshold = 7;
	lse::LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_amscale_kwd, p_lmscale_kwd, p_use_baumwelsh, p_logAdd_treshold);
	vit.computeFwViterbi();
	vit.computeBwViterbi();
	vit.computeLinksLikelihood(latStartTime);

	lat.printNodes();
}
// }}}

test_suite* init_unit_test_suite(int argc, char* argv[])
{
//	boost::thread idx_server_thread(&start_indexing_server);
//	sleep(1);

	test_suite* test = BOOST_TEST_SUITE( "MKws test" );

	test->add( BOOST_TEST_CASE( &test_MKwsDictionary ) );
	test->add( BOOST_TEST_CASE( &test_MKwsIndexer ) );
	test->add( BOOST_TEST_CASE( &test_MIndexerStruct ) );
//	test->add( BOOST_TEST_CASE( &test_liblse ) );

	// if check_is_small is not used, a warning pops up
	boost::test_tools::check_is_small(0.1, 0.2);

//	idx_server_thread.join();
	return test;
}

