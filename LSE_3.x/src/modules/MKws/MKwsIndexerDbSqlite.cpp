#include "MKwsIndexerDbSqlite.h"
#include "MKws.h"

using namespace sem;
using namespace std;

void MKwsIndexerDbSqlite::AddFwdLabelToInvertedIndex(ID docId, ID labelId, FwdLabel *pFwdLabel)
{
	DBG("MKwsIndexerDbSqlite::AddFwdLabelToInvertedIndex()");
	sqlite3* db = mpMKws->GetDb();
	//sqlite3* db = mpIdxDb;

	Hits_OneDocHitList * fwd_hits = pFwdLabel->GetHits();
	//CERR("fwd_hits->GetSize():"<<fwd_hits->GetSize());
	ostringstream sql;
	for (Hits_OneDocHitList::iterator i = fwd_hits->begin(); i!=fwd_hits->end(); i++)
	{
		sql << "INSERT INTO " << mTableName
			<< " (id_doc, id_label, start_time, end_time, confidence)"
			<< " VALUES("
			<< "\""<<docId<<"\","
			<< "\""<<labelId<<"\","
			<< "\""<<i->GetStartTime()<<"\","
			<< "\""<<i->GetEndTime()<<"\","
			<< "\""<<i->GetScore()<<"\""
			<< "); " << endl;
		//DBG("sql:"<<sql.str());
		DBG("SQL:"<<sql.str());
		int rc;
		char* zErrMsg = 0;
		rc = sqlite3_exec(db, sql.str().c_str(), NULL, NULL, &zErrMsg);
		// if any error appears
		CHECK_SQLITE_RES_OK(rc, zErrMsg, sql.str().c_str());
	}
}

/*
int MKwsIndexerDbSqlite::SelectHits_Callback(void *ptr, int argc, char **argv, char **azColName)
{
	Hits* p_hits = static_cast<Hits*>(ptr);
	p_hits->AddHits()
	// !!! Maybe argv[0] shoud be copied to str instead of just assigning a pointer
	str = argv[0];
	return 0;
}

MIndexerStruct::InvLabel* MKwsIndexerDbSqlite::GetOccurrences(ID labelId)
{
	assert(mpInvLabelsContainer);
	InvLabel* l = new MIndexerStruct::InvLabel("");
	Hits* h = l->GetHits();
	DocumentsSortedByRelevance* dr = l->GetDocsByRelevance();
	DocumentsSortedById* di = GetDocsById();

	ostringstream sql;
	sql << "SELECT id_doc, start_time, end_time, confidence FROM "<<mTableName<<" WHERE id_label="<<labelId<<" ORDER BY id_doc, start_time ASC";
	rc = sqlite3_exec(mpM->GetDb(), sql.str().c_str(), SelectHits_Callback, h, &zErrMsg);
	CHECK_SQLITE_RES_OK(rc, zErrMsg, sql.str());

	DBG("GetOccurrences(): size="<<l->GetHits()->GetSize());
	//DBG("GetOccurrences(): mContainer:"<<endl<<*l);
	return l;
}
*/
