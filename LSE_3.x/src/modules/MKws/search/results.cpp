#include <algorithm> // sort()
#include "results.h"

using namespace std;
using namespace lse;


void Results::SetQueryKwdList(QueryKwdList *pQueryKwdList)
{
	mpQueryKwdList = pQueryKwdList;
}

void Results::postProcess(NormLexicon *pNormLexiconLvcsr, NormLexicon *pNormLexiconPhn) 
{
	DBG_FORCE("Results::postProcess()");
	this->NormalizeResults(pNormLexiconLvcsr, pNormLexiconPhn);

//	this->sortResults();
	this->joinOverlappingResults();
}

void Results::joinOverlappingResults()
{
		volatile bool overlapping = true;
		while (overlapping) 
		{
			overlapping = false;
			// compare results only within 1 RecordStream
//			for (vector<Hypothesis>::iterator iHyp1=(iRS->second).begin(); !overlapping, iHyp1!=(iRS->second).end(); ++iHyp1)
//			for (vector<Hypothesis>::iterator iHyp1=iRS->begin(); !overlapping, iHyp1!=iRS->end(); ++iHyp1)
			for (ResultsBaseType::iterator iHyp1=this->begin(); !overlapping, iHyp1!=this->end(); ++iHyp1)
			{
//				for (vector<Hypothesis>::iterator iHyp2=(iRS->second).begin(); !overlapping, iHyp2!=(iRS->second).end(); ++iHyp2) 
//				for (vector<Hypothesis>::iterator iHyp2=iRS->begin(); !overlapping, iHyp2!=iRS->end(); ++iHyp2) 
				for (ResultsBaseType::iterator iHyp2=this->begin(); !overlapping, iHyp2!=this->end(); ++iHyp2) 
				{
					// do not compare hypotheses from different records
					if ( !(iHyp1->documentId == iHyp2->documentId && iHyp1->channel == iHyp2->channel) ) continue;

					// don't compare the same results
					if (iHyp1 == iHyp2) continue; 

					LatTime iHyp1_start = iHyp1->words.begin()->start;
					LatTime iHyp1_end   = (--iHyp1->words.end())->end;
					LatTime iHyp2_start = iHyp2->words.begin()->start;
					LatTime iHyp2_end   = (--iHyp2->words.end())->end;
/*
					DBG_FORCE("iHyp1_start: " << iHyp1_start << " iHyp1_end: " << iHyp1_end);
					DBG_FORCE("iHyp2_start: " << iHyp2_start << " iHyp2_end: " << iHyp2_end);
					DBG_FORCE("iHyp1_score: " << iHyp1->score);
					DBG_FORCE("iHyp2_score: " << iHyp2->score);
*/						
					// overlapping detection
					if ((iHyp1_start >= iHyp2_start && iHyp1_start <  iHyp2_end) ||
						(iHyp1_end   >  iHyp2_start && iHyp1_end   <= iHyp2_end) ||
						(iHyp1_start <  iHyp2_start && iHyp1_end   >  iHyp2_end))
					{
						DBG_FORCE("overlapping results:" << endl << *iHyp1 << *iHyp2);

						bool iHyp1_is_better = iHyp1->keywords.size() > iHyp2->keywords.size();
						if (iHyp1->keywords.size() == iHyp2->keywords.size()) {
							iHyp1_is_better = iHyp1->score > iHyp2->score;
						}
						
						// remove the worse one
						if (iHyp1_is_better) {
//							cout << "iHyp1 is better" << endl << flush;
//							(iRS->second).erase(iHyp2);
							this->erase(iHyp2);
						} else {
//							cout << "iHyp2 is better" << endl << flush;
//							(iRS->second).erase(iHyp1);
							this->erase(iHyp1);
						}
						DBG_FORCE("Results::joinOverlappingResults() ... 1 result erased");
//						cout << "erased" << endl << flush;
						overlapping = true;
						break;
					}
				} // for iHyp2
				if (overlapping) break;
			} // for iHyp1
		} // while (overlapping)
}

void Results::sortResults()
{
	// sort all results by score
	sort(this->begin(), this->end());
}

/*
void Results::print(ostream *os, ConfigFile* config, float searchTime) 
{
	DBG_FORCE("Results::print()");
	DBG_FORCE("count: "<<this->size());
	Results_EType p_results_output_type = ResultsETypeNormal;
	string p_results_output_type_str = (string)config->Value("MKws_searcher", "p_results_output_type");
	
	if (p_results_output_type_str == "normal")
	{
		p_results_output_type = ResultsETypeNormal;
	}
	else if (p_results_output_type_str == "xml")
	{
		p_results_output_type = ResultsETypeXml;
	}
	else if (p_results_output_type_str == "xml_std")
	{
		p_results_output_type = ResultsETypeXml_std;
	}
	else if (p_results_output_type_str == "exp")
	{
		p_results_output_type = ResultsETypeExp;
	}
	else if (p_results_output_type_str == "xml_transcripts")
	{
		p_results_output_type = ResultsETypeXmlTranscripts;
	}

	if (p_results_output_type == ResultsETypeXml)
	{
		*os << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << endl; 
	}
	else if (p_results_output_type == ResultsETypeXmlTranscripts)
	{
 		*os << "<transcription>" << endl;
 		*os << "  <data>" << endl;
	}
	else if (p_results_output_type == ResultsETypeXml_std)
	{
 		*os << "  <detected_termlist termid=\""<<mpQueryKwdList->mTermId<<"\""
		    << " term_search_time=\""<<searchTime<<"\""
		    << " oov_term_count=\""<<mOovCount<<"\">" << endl;
	}
	for (ResultsBaseType::iterator i=this->begin(); i!=this->end(); ++i) 
	{		
		LatTime start = (i->keywords.begin())->start - (double)config->Value("MKws_searcher", "p_results_start_time_subtractor");
		LatTime end = (i->keywords.rbegin())->end;

		if ((bool)config->Value("MKws_searcher", "p_results_convert_to_local_time"))
		{
			string latname_nopath = i->record;
			string::size_type strPos = latname_nopath.rfind(PATH_SLASH);
			latname_nopath.erase(0, strPos+1);

			LatTime latStartTime = 0;
			string sStartTime = latname_nopath;
			string::size_type pos_1st = 0;
			for (int field_idx=1; field_idx < (int)config->Value("MKws_searcher", "p_lat_filename_start_time_field"); field_idx++) {
				pos_1st = sStartTime.find (((string)config->Value("MKws_searcher", "p_record_delim"))[0], pos_1st+1); // find first _ in latname
				DBG("pos_1st:"<<pos_1st);
			}
			if (pos_1st != string::npos) {
				string::size_type pos_2nd = latname_nopath.find (((string)config->Value("MKws_searcher", "p_record_delim"))[0], pos_1st+1); // find second _ in latname
				sStartTime = sStartTime.substr(pos_1st+1, pos_2nd - pos_1st - 1);
				latStartTime = atof(sStartTime.c_str()) / (int)config->Value("MKws_searcher", "p_latname_time_multiplier");
				DBG("Getting start time from filename...sStartTime:"<<sStartTime<<" latStartTime:"<<latStartTime<<endl);
			} else {
				CERR("Error in lattice's name: Expecting _ in \"" << latname_nopath << "\" ... setting start time to 0.0");
			}
			start -= latStartTime;
			end -= latStartTime;
		}
		if (p_results_output_type == ResultsETypeXml)
		{
			*os << "<hypothesis stream=\""<<i->stream<<"\" "
				<< " record=\""<<i->record<<"\" "
				<< " score=\""<< fixed << setprecision(4) << i->score <<"\" >" << endl;
			i->printXml(os);
			*os << "</hypothesis>" << endl;
		}
		else if (p_results_output_type == ResultsETypeXmlTranscripts)
		{
			int h=0,m=0,s=0;
			s = (int)start + ((int)config->Value("MKws_searcher", "p_results_start_time_subtractor")); // to have the correct timelabel
			h = (int)(s / 3600);
			s %= 3600;
			m = (int)(s / 60);
			s %= 60;
			*os << "    <record channel=\""<<i->channel<<"\""
				<< " start=\""<< start <<"\""
				<< " end=\""<< end <<"\""
				<< " bgcolor=\"#FFFFFF\""
				<< " timelabel=\""<< h << "h:" << m << "m:" << s << "s\""
				<< ">" << endl;
			i->printText(os); *os << endl;
			*os << "    </record>" << endl;
		}
		else if (p_results_output_type == ResultsETypeXml_std)
		{
			*os << "    <term file=\""<< i->record <<"\""
				<< " channel=\""<<i->channel<<"\""
				<< " tbeg=\""<< start <<"\""
				<< " dur=\""<< end - start <<"\""
				<< " score=\""<< fixed << setprecision(4) << i->score <<"\""
				<< " decision=\""<< (i->decision ? "YES" : "NO") << "\""
//				<< " pron=\""<< (i->pron) << "\""
				<< "/>" << endl;
		}
		else if (p_results_output_type == ResultsETypeExp)
		{
			*os << i->record
				<< " " << mpQueryKwdList->mTermId
				<< " " << mOovCount
				<< " " << start
				<< " " << end
				<< endl;
		}
		else
		{
			*os << "HYPOTHESIS:" << endl;
	//		*os << "STREAM: \t" << (i->first).stream << endl;
	//		*os << "STREAM: \t" << i->stream << endl;
			*os << "STREAM: LVCSR\t" << endl;
			*os << "RECORD: \t" << i->record << endl;
//			*os << "CHANNEL: \t" << i->channel << endl;
			*os << "COUNT: \t1" << endl;
			*os << "SCORE: \t" << fixed << setprecision(4) << i->score<< endl;
			*os << *i << endl;
		}
	}
	if (p_results_output_type == ResultsETypeXml_std)
	{
 		*os << "  </detected_termlist>" << endl;
	}
	else if (p_results_output_type == ResultsETypeXmlTranscripts)
	{
 		*os << "  </data>" << endl;
 		*os << "</transcription>" << endl;
	}
	DBG_FORCE("Results::print()...done");
}
*/

void Results::push_back(lse::Hypothesis hyp)
{
	// check if there is some limit on number of hypotheses and
	// if the hypothesis is not empty
	// 
	// TODO it will not work if &ALLSUCCESSFULTOKENS is set because
	// the first rescored hypothesis may return even more results than
	// is set in &RES=... so no other rescored hypothesis will be pushed
	// into results collection
	if (((int)this->size() < mMaxResultsCount || mMaxResultsCount == -1) &&
		(hyp.keywordsCount > 0))
	{
		((ResultsBaseType*)this)->push_back(hyp);
	}
	else
	{
		CERR("ERROR: Throwing away hypothesis");
		EXIT();
	}
}

void Results::SetMaxHypothesesCount(int max)
{
	mMaxResultsCount = max;
}
		
/*
std::ostream& operator<<(std::ostream& os, const Results& res)
{
	for (Results::const_iterator i=res.begin(); i!=res.end(); ++i) 
	{		
		os << "HYPOTHESIS:" << endl;
		os << "STREAM: \t" << (i->first).stream << endl;
		os << "RECORD: \t" << (i->first).record << endl;
		os << "COUNT: \t" << (i->second).size() << endl;
		for (vector<Hypothesis>::const_iterator j=(i->second).begin(); j!=(i->second).end(); ++j) {
			os << *j << endl;
		}
	}
	return os;
}
*/

void Results::NormalizeResults(NormLexicon *pNormLexiconLvcsr, NormLexicon *pNormLexiconPhn)
{
	DBG_FORCE("Results::NormalizeResults()");
	
	if (pNormLexiconLvcsr->IsActive())
	{
		for (Results::iterator iRes=this->begin(); iRes!=this->end(); ++iRes)
		{
			bool phn = false;

			DBG_FORCE("result score:"<<iRes->score);
			DBG_FORCE("result:"<<*iRes);
			for (Hypothesis::Words::iterator iWord = iRes->keywords.begin();
				 iWord != iRes->keywords.end();
				 iWord ++)
			{
				DBG_FORCE("iWord:"<<iWord->str<<" ... "<<iWord->pron);
				if (pNormLexiconPhn->IsActive() && iWord->pron != "")
				{
					phn = true;
					vector<string> phonemes;
					string_split(trim_outer_spaces(iWord->pron), " ", phonemes);
					
					// for each unigram in current iKwd
					for (vector<string>::iterator iPhn = phonemes.begin(); iPhn != phonemes.end(); ++iPhn)
					{
						iRes->score -= pNormLexiconPhn->GetWordNormConst(*iPhn);
					}

					int frames = (int)((iWord->end - iWord->start) * (1000/FRAME_LENGTH));
					DBG_FORCE("frames:"<<frames);
					iRes->score -= frames*pNormLexiconPhn->mFrmNorm;
				}
				else
				{
					vector<string> words;
					string_split(trim_outer_spaces(iWord->str), " ", words);
					// for each word in current iWord
					for (vector<string>::iterator iWordItem = words.begin(); iWordItem != words.end(); ++iWordItem)
					{
						iRes->score -= pNormLexiconLvcsr->GetWordNormConst(*iWordItem);
						DBG_FORCE("pNormLexiconLvcsr->GetWordNormConst(iWord->str) ... "<<pNormLexiconLvcsr->GetWordNormConst(*iWordItem));
					}
					int frames = (int)((iWord->end - iWord->start) * (1000/FRAME_LENGTH));
					DBG_FORCE("frames:"<<frames<<" mFrmNorm:"<<pNormLexiconLvcsr->mFrmNorm);
					iRes->score -= frames*pNormLexiconLvcsr->mFrmNorm;
				}
			}
			if (phn)
			{
				iRes->score -= pNormLexiconPhn->mGlobOovNorm;
			}
			else
			{
				DBG_FORCE("mGlobNorm:"<<pNormLexiconLvcsr->mGlobNorm);
				iRes->score -= pNormLexiconLvcsr->mGlobNorm;
			}

			iRes->decision = iRes->score > mHardDecisionTreshold;
			DBG_FORCE("result score NORMALIZED:"<<iRes->score);
			DBG_FORCE("result NORMALIZED:"<<*iRes);
		}
	}
	else if (pNormLexiconPhn->IsActive())
	{
		for (Results::iterator iRes=this->begin(); iRes!=this->end(); ++iRes)
		{
			DBG_FORCE("result score:"<<iRes->score);
			DBG_FORCE("result:"<<*iRes);
			// for each word within the hypothesis
			for (Hypothesis::Words::iterator iWord = iRes->words.begin();
				 iWord != iRes->words.end();
				 iWord ++)
			{
				vector<string> phonemes;
				string_split(trim_outer_spaces(iWord->pron), " ", phonemes);
				
				// for each unigram in current iKwd
				for (vector<string>::iterator iPhn = phonemes.begin(); iPhn != phonemes.end(); ++iPhn)
				{
					iRes->score -= pNormLexiconPhn->GetWordNormConst(*iPhn);
				}
				/*
				int frames = (int)((iWord->end - iWord->start) * (1000/FRAME_LENGTH));
				DBG_FORCE("frames:"<<frames);
				iRes->score -= frames*pNormLexiconPhn->mFrmNorm;
				*/
			}
			int frames = (int)((iRes->keywords.rbegin()->end - iRes->keywords.begin()->start) * (1000/FRAME_LENGTH));
			DBG_FORCE("frames:"<<frames);
			iRes->score -= frames*pNormLexiconPhn->mFrmNorm;

			iRes->score -= pNormLexiconPhn->mGlobNorm;
			iRes->decision = iRes->score > mHardDecisionTreshold;
			DBG_FORCE("result score NORMALIZED:"<<iRes->score);
			DBG_FORCE("result NORMALIZED:"<<*iRes);
		}
	}
}

