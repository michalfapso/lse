#include "querykwdlist.h"
#include "MKwsDictionary.h"

using namespace std;
using namespace lse;
using namespace sem;

void QueryKwd::SetPronList(list<gpt::PhnTrans::pt_entry> *pPronList, QueryKwdList *parentKwdList, MKwsDictionary *pLexiconPhn)
{
	// for each pronounciation variant (phoneme string)
	for (list<gpt::PhnTrans::pt_entry>::iterator iPron = pPronList->begin();
		 iPron != pPronList->end();
		 iPron ++)
	{
		iPron->trans = trim_outer_spaces(iPron->trans);
		PronVariant pv;
		pv.mPtEntry = *iPron;
		GeneratePronQueryKwdList(&pv, pLexiconPhn, parentKwdList);
		
		mPronList.push_back(pv);
	}
}

void QueryKwd::GeneratePronQueryKwdList(PronVariant *pv, MKwsDictionary *pLexiconPhn, QueryKwdList* pParentQueryKwdList)
{
	pv->mpQueryKwdListPhn = new QueryKwdList;
	pv->mpQueryKwdListNgram = new QueryKwdList;

	pv->mpQueryKwdListNgram->mExactMatch = pParentQueryKwdList->mExactMatch;

	// phonemes (mQueryKwdListPhn)
	vector<string> phonemes_str;
	string_split(pv->mPtEntry.trans, " ", phonemes_str);
	DBG_FORCE("TRANS: "<<pv->mPtEntry.trans);
	for (vector<string>::iterator i = phonemes_str.begin(); i != phonemes_str.end(); i++)
	{
		QueryKwd kwd;

		DBG_FORCE("PHONEME: "<<*i);
		kwd.W						= *i;
		kwd.W_id 					= pLexiconPhn==NULL ? -1 : pLexiconPhn->GetUnitId(kwd.W);
		kwd.isNextInPhrase			= true;
		kwd.confidence_min_isset	= false;
		kwd.confidence_max_isset	= false;
		kwd.time_min				= this->time_min;
		kwd.time_max				= this->time_max;
		kwd.time_min_type			= this->time_min_type;
		kwd.time_max_type			= this->time_max_type;
		kwd.lessThan				= false;
		kwd.greaterThan				= false;
		kwd.mIsNgram				= false;

		pv->mpQueryKwdListPhn->push_back(kwd);
	}
	pv->mpQueryKwdListPhn->rbegin()->isNextInPhrase = false; // last phoneme
	
	// ngrams (mpQueryKwdListNgram)
	vector<string> ngram_list;
	phn2ngram(pv->mPtEntry.trans, &ngram_list, 3);
	for (vector<string>::iterator i = ngram_list.begin(); i != ngram_list.end(); i++)
	{
		QueryKwd kwd;

		kwd.W						= *i;
		kwd.W_id 					= pLexiconPhn==NULL ? -1 : pLexiconPhn->GetUnitId(kwd.W);
		kwd.isNextInPhrase			= false;
		kwd.confidence_min_isset	= false;
		kwd.confidence_max_isset	= false;
		kwd.time_min				= this->time_min;
		kwd.time_max				= this->time_max;
		kwd.time_min_type			= this->time_min_type;
		kwd.time_max_type			= this->time_max_type;
		kwd.lessThan				= false;
		kwd.greaterThan				= false;
		kwd.mIsNgram				= true;

		pv->mpQueryKwdListNgram->push_back(kwd);
	}
	pv->mpQueryKwdListNgram->mOverlapping = true;
}
	
void QueryKwdList::push_back(QueryKwd &rQueryKwd)
{
	DBG_FORCE("QueryKwdList::push_back("<<rQueryKwd<<")");
	if (rQueryKwd.W.find('_') != string::npos) 
	{ 
		rQueryKwd.mIsNgram = true;
		DBG_FORCE("mNgram2Phn="<<mNgram2Phn);
		if (mNgram2Phn)
		{
			DBG_FORCE("StringSplit()");
			vector<string> phonemes_str;
			string_split(rQueryKwd.W, "_", phonemes_str);
			for (vector<string>::iterator i=phonemes_str.begin(); i!=phonemes_str.end(); ++i)
			{
				rQueryKwd.phonemes.push_back(mpLexicon->GetUnitId(*i));
				mPhonemesCnt++;
			}
			DBG_FORCE("StringSplit()...done " << rQueryKwd.phonemes.size());
		}
	}

	bool found = false;
	for (QueryKwdList::iterator iKwd=begin(); iKwd!=end(); ++iKwd)
	{
		if (iKwd->W == rQueryKwd.W)
		{
			found = true;
		}
	}
	if (!found)
	{
		mUniqSize++;
	}
	((vector< QueryKwd >*)this)->push_back(rQueryKwd);
}

void QueryKwdList::AddWords(QueryKwdList* pQueryKwdList_From)
{
	for (QueryKwdList::iterator iKwd = pQueryKwdList_From->begin();
		iKwd != pQueryKwdList_From->end();
		iKwd ++)
	{
		((vector<QueryKwd>*)this)->push_back(*iKwd);
	}
}

int QueryKwdList::getKwdIndex(ID_t W_id)
{
	int kwdIndex = 0;
	for (QueryKwdList::iterator iKwd = this->begin(); iKwd != this->end(); ++iKwd)
	{
		if (iKwd->W_id == W_id) {
			return kwdIndex;
		}
		kwdIndex++;
	}
	kwdIndex = -1; // not found
	return kwdIndex;
}

int QueryKwdList::getKwdIndex(QueryKwdList::const_iterator iKwd)
{
	int kwdIndex = 0;
	for (QueryKwdList::iterator i = this->begin(); i != this->end(); ++i)
	{
		if (iKwd == i) {
			return kwdIndex;
		}
		kwdIndex++;
	}
	kwdIndex = -1; // not found
	return kwdIndex;
}

bool QueryKwdList::contains(ID_t W_id)
{
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
	{
		if (i->W_id == W_id)
		{
			return true;
		}
		else if (mNgram2Phn)
		{
			for (vector<ID_t>::iterator iPhn=i->phonemes.begin(); iPhn!=i->phonemes.end(); ++iPhn)
			{
				if (*iPhn == W_id)
				{
					return true;
				}
			}
		}
	}
	return false;
}

bool QueryKwdList::contains(std::string W) 
{
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
		if (i->W == W)
			return true;
	return false;
}

QueryKwdList::iterator QueryKwdList::find(std::string W) 
{
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
		if (i->W == W)
			return i;
	return this->end();
}

std::ostream& lse::operator<<(std::ostream& os, const QueryKwd& kwd) 
{
	char buffer[20];
	sprintf(buffer, "%.2f", kwd.time_min);
	std::string time_min_str = kwd.time_min_type == null ? "-inf" : buffer;
	sprintf(buffer, "%.2f", kwd.time_max);
	std::string time_max_str = kwd.time_max_type == null ? "inf" : buffer;

	if (kwd.time_min_type == lse::time) 
		time_min_str += "s";
	else if (kwd.time_min_type == lse::percents)
		time_min_str += " * 100%";

	if (kwd.time_max_type == lse::time) 
		time_max_str += "s";
	else if (kwd.time_max_type == lse::percents)
		time_max_str += " * 100%";
	
	os << kwd.W << ":" << kwd.W_id 
	   << "\tnb_prev:" << kwd.nb_prev 
	   << "\tnb_next:" << kwd.nb_next 
	   << (kwd.isNextInPhrase ? "\tnextKwdInPhrase" : "")
	   << "\tconf:<" << (kwd.confidence_min_isset ? kwd.confidence_min : -INF) << ".." << (kwd.confidence_max_isset ? kwd.confidence_max : INF) <<">"
	   << "\ttime:<" << time_min_str << ".." << time_max_str << ">"
	   << (kwd.lessThan ? "\tlessThan" : "")
	   << (kwd.greaterThan ? "\tgreaterThan" : "");

	os << "\tProns:";
	for (PronVariants::const_iterator i=kwd.mPronList.begin(); i!=kwd.mPronList.end(); i++)
	{
		os << i->mPtEntry.trans << ", ";
	}
	/*
	os << " phonemes:"<<kwd.phonemes.size() << " ";
	for (vector<ID_t>::const_iterator iPhn=kwd.phonemes.begin(); iPhn!=kwd.phonemes.end(); ++iPhn)
	{
		os << *iPhn << " ";
	}
	*/
	return os;
}

std::ostream& lse::operator<<(std::ostream& os, const lse::PronVariant& pron)
{
	os << *(pron.mpQueryKwdListPhn);
	return os;
}

std::ostream& lse::operator<<(std::ostream& os, const lse::QueryKwdList& kwdlist)
{
	for (std::vector< QueryKwd >::const_iterator i = kwdlist.begin(); i!=kwdlist.end(); ++i)
	{
		os << *i << endl;
	}
	return os;
}

std::ostream& lse::operator<<(std::ostream& os, const InvIdxRecordsCluster& c) 
{
	os << "meetingID:"<<c.mMeetingID<<" conf:"<<c.mConf<<" kwds:"<<c.mValidKeywordsCount<<" t["<<c.mStartTime<<" .. "<<c.mEndTime<<"] source:" << c.mSourceType << " subclusters:"<< c.size() << std::endl;

	// each keyword (group[keyword])
	for (InvIdxRecordsCluster::const_iterator iCluster=c.begin(); iCluster!=c.end(); ++iCluster)
	{
		os << "  kwd: " << iCluster->first << std::endl;
		// each cluster (group[keyword]->clusters)
		for (int i=0; i<iCluster->second.size(); i++)
		{
			LatIndexer::Occurrence* pOccurrence = iCluster->second.GetOccurrence(i);
			os << "     [meetingID:" << pOccurrence->mMeetingID << "] " 
				<< pOccurrence->mStartTime << " ... " << pOccurrence->mEndTime
				<< " (conf:" << pOccurrence->mConf << ")"
				<< " sourceType:" << iCluster->second.GetSourceType(i)
				<< std::endl;
			if (iCluster->second.GetSourceType(i) == SourceType::PHN_CLUSTER)
			{
				os << "    --------------------------------------------------" << endl;
				os << "    SUBCLUSTER:" << endl << *((InvIdxRecordsCluster*)pOccurrence) << endl;
				os << "    --------------------------------------------------" << endl;
			}
		}
	}
	return os;
}

void QueryKwdList::print()
{
	DBG_FORCE("--------------------------------------------------");
	for (std::vector< QueryKwd >::iterator i = this->begin(); i!=this->end(); ++i)
	{
		DBG_FORCE(*i);
	}
	DBG_FORCE("mUniqSize:"<<mUniqSize);
	DBG_FORCE("--------------------------------------------------");
}

void QueryKwd::InvIdxRecords::push_back(const LatIndexer::Record &rec)
{
	QueryKwd::InvIdxRecord recInvIdxRecord;
	recInvIdxRecord.mWordID 	= rec.mWordID;
	recInvIdxRecord.mMeetingID 	= rec.mMeetingID;
	recInvIdxRecord.mNodeID 	= rec.mNodeID;
	recInvIdxRecord.mConf 		= rec.mConf;		// confidence
	recInvIdxRecord.mStartTime 	= rec.mStartTime;	// word's start time
	recInvIdxRecord.mEndTime 	= rec.mEndTime;		// word's end time
	mvByConfidence.push_back(recInvIdxRecord);
}

unsigned int QueryKwd::InvIdxRecords::size()
{
	return mvByConfidence.size();
}

QueryKwd::InvIdxRecord* QueryKwd::InvIdxRecords::FindOccurrence(const LatIndexer::Occurrence& where)
{
	DBG("FindOccurrence()");
	if (size() == 0)
	{
		return NULL;
	}
	if (!mpByTime)
	{
		CERR("ERROR: the InvIdxRecords list is not sorted by time");
		EXIT();
	}
	//--------------------------------------------------
	// BINARY SEARCH
	//
	bool found = false;
	int mid = 0;
	int left = 0;
	int right = this->size() - 1;
	DBG("size:"<<this->size());
	while (left <= right)
	{
		mid = (int)floor((right-left)/2)+left;

		DBG("this->size():"<<this->size());
		DBG("mid:"<<mid);

		DBG("left:"<<left<<" right:"<<right<<" mid:"<<mid);
		DBG("where.mMeetingID:"<<where.mMeetingID<<" (*this)[mid].mMeetingID:"<<mvByConfidence[mpByTime[mid]].mMeetingID);
		DBG("where.mStartTime:"<<where.mStartTime<<" (*this)[mid].mStartTime:"<<mvByConfidence[mpByTime[mid]].mStartTime);
		DBG("where.mEndTime:"<<where.mEndTime<<" (*this)[mid].mEndTime:"<<mvByConfidence[mpByTime[mid]].mEndTime);
		if (where.mMeetingID > mvByConfidence[mpByTime[mid]].mMeetingID)
		{
			left  = mid+1;
		}
		else if (where.mMeetingID < mvByConfidence[mpByTime[mid]].mMeetingID)
		{
			right = mid-1;
		}
		// now meetingIDs are the same, let us look at the time
		else if (where.mStartTime > mvByConfidence[mpByTime[mid]].mEndTime)
		{
			left  = mid+1;
		}
		else if (where.mEndTime < mvByConfidence[mpByTime[mid]].mStartTime)
		{
			right = mid-1;
		}
		else
		{
			found = true;
			break;
		}
	}

	return !found ? NULL : &(mvByConfidence[mpByTime[mid]]);
	//--------------------------------------------------
	
}

void QueryKwdList::QueryKwdClustersClustering()
{
	// IF THE REVERSE INDEX IS SORTED BY WORDID AND TIME, IT IS POSSIBLE TO USE BINARY SEARCH,
	// OTHERWISE (IE. WHEN IT IS SORTED BY CONFIDENCE), IT IS NEEDED TO DO IT SEQUENTIALLY

	// FOLLOWING LOOPS ARE CHOOSING THE FORWARD AND BACKWARD CLUSTERS FOR EACH QUERY-KEYWORD'S CLUSTER
	// for each keyword from query
	QueryKwdList::iterator kwdPrev = this->end();
	for (QueryKwdList::iterator kwdCur = this->begin(); kwdCur != this->end(); ++kwdCur) 
	{
		if (kwdPrev != this->end()) 
		{
			DBG(kwdPrev->W << " -> " << kwdCur->W);

			// for each cluster from kwdPrev
			for (int i_rec_prev = 0; i_rec_prev < (int)kwdPrev->mpClusters->size(); i_rec_prev++)
			{
//			for (vector<Search::InvIdxRecord>::iterator iRecPrev = kwdPrev->mpClusters->mvByTime.begin(); 
//				 iRecPrev != kwdPrev->recClusters.end(); 
//				 ++iRecPrev) 

				DBG("i_rec_prev:"<<i_rec_prev);
				DBG("mpByTime:"<<kwdPrev->mpClusters->mpSortedByTime);
				InvIdxRecordsCluster *pClusterPrev = kwdPrev->mpClusters->mpSortedByTime[i_rec_prev];
				DBG("kwdPrev->mpClusters->size():"<<kwdPrev->mpClusters->size());
				DBG("recPrev: "<<pClusterPrev);
				
				pClusterPrev->mBestFwdClusterFound = false;
				
				int mid = 0;
				//--------------------------------------------------
				// BINARY SEARCH
				//
				bool found = false;
				int left = 0;
				int right = kwdCur->mpClusters->size() - 1;
				while (left <= right)
				{
					mid = (int)floor((right-left)/2)+left;

					DBG("kwdPrev->mpClusters->mpByTime:"<<kwdPrev->mpClusters->mpSortedByTime);
					DBG("kwdPrev->mpClusters->size():"<<kwdPrev->mpClusters->size());
					DBG("mid:"<<mid);
					DBG("kwdPrev->mpClusters->mpByTime[mid]:"<<kwdPrev->mpClusters->mpSortedByTime[mid]);

					DBG("left:"<<left<<" right:"<<right<<" mid:"<<mid);
					DBG("pClusterPrev->tStart > kwdCur->recClusters[mid].tEnd ... "<<pClusterPrev->mStartTime<<" > "<<kwdCur->mpClusters->mpSortedByTime[mid]->mEndTime);
					DBG("pClusterPrev->tEnd < kwdCur->recClusters[mid].tStart ... "<<pClusterPrev->mEndTime<<" < "<<kwdCur->mpClusters->mpSortedByTime[mid]->mStartTime);
					if (pClusterPrev->mMeetingID > kwdCur->mpClusters->mpSortedByTime[mid]->mMeetingID)
					{
						left  = mid+1;
					}
					else if (pClusterPrev->mMeetingID < kwdCur->mpClusters->mpSortedByTime[mid]->mMeetingID)
					{
						right = mid-1;
					}
					// now meetingIDs are the same, let us look at the time
					else if (this->mOverlapping)
					{
						if (pClusterPrev->mStartTime > kwdCur->mpClusters->mpSortedByTime[mid]->mEndTime)
						{
							left  = mid+1;
						}
						else if (pClusterPrev->mEndTime < kwdCur->mpClusters->mpSortedByTime[mid]->mStartTime)
						{
							right = mid-1;
						}
						else
						{
							found = true;
							break;
						}
					}
					else
					{
						if (pClusterPrev->mStartTime - kwdPrev->nb_next > kwdCur->mpClusters->mpSortedByTime[mid]->mEndTime)
						{
							left  = mid+1;
						}
						else if (pClusterPrev->mEndTime + kwdPrev->nb_next < kwdCur->mpClusters->mpSortedByTime[mid]->mStartTime)
						{
							right = mid-1;
						}
						else
						{
							found = true;
							break;
						}

					}
				}

				if (!found) 
				{
					continue;
				}
				//--------------------------------------------------

				int mid_search = mid;
				int sign = 1;
//				DBG_FORCE("kwdCur->recClusters.size():"<<kwdCur->recClusters.size());
				while (1)
				{
					InvIdxRecordsCluster *pClusterCur = kwdCur->mpClusters->mpSortedByTime[mid_search];
//					DBG_FORCE("mid_search:"<<mid_search);
//					DBG_FORCE("pClusterCur:"<<*pClusterCur);

					if (
						(pClusterCur->mMeetingID != pClusterPrev->mMeetingID)
						||
						(this->mOverlapping &&
						 !is_overlapping(pClusterCur->mStartTime, pClusterCur->mEndTime,
										pClusterPrev->mStartTime, pClusterPrev->mEndTime)
						)
						||
						(!this->mOverlapping &&
						 (
							(pClusterPrev->mStartTime - kwdPrev->nb_next > pClusterCur->mEndTime)
							||
							(pClusterPrev->mEndTime + kwdPrev->nb_next < pClusterCur->mStartTime)
						 )
						)
					   )
					{
						DBG("NOT OVERLAPPING");
						if (sign == -1)
						{
							break;
						}
						else
						{
							sign *= -1;
							mid_search = mid - 1; // because mid was already processed
							if (mid_search > 0)
							{
								continue; // if there are some hypotheses on the other side of 'mid' index, process them
							}
							else
							{
								break; // if there is nothing left on the other side of 'mid' index, we are done
							}
						}
					}

					DBG("  clusterCur: "<<*pClusterCur);

					// If the distance between the kwdCur's cluster and kwdPrev's cluster fits the distance specified by search query
					// For free kwds, just select the one with best posterior
					if (kwdPrev->nb_next == -1 ||
						// operator < (lessThan)
						(
						  (
							(kwdPrev->lessThan && pClusterPrev->mEndTime < pClusterCur->mStartTime) ||
							(kwdPrev->greaterThan && pClusterCur->mEndTime < pClusterPrev->mStartTime) ||
							(!kwdPrev->lessThan && !kwdPrev->greaterThan)
						  )
						  && abs((int)(pClusterCur->mStartTime - pClusterPrev->mEndTime)) <= kwdPrev->nb_next)
						)
					{
						DBG("pClusterPrev: meetingID="<<pClusterPrev->mMeetingID<<" t["<<pClusterPrev->mStartTime<<".."<<pClusterPrev->mEndTime<<"]");
						DBG("pClusterCur: meetingID="<<pClusterCur->mMeetingID<<" t["<<pClusterCur->mStartTime<<".."<<pClusterCur->mEndTime<<"]");
						// add the current cluster to the previous cluster's forward list / and vice versa
						// clusters are already sorted by confidence
						pClusterPrev->mFwdClusters.push_back(pClusterCur);
						pClusterCur->mBckClusters.push_back(pClusterPrev);

//						!!! THE FOLLOWING CODE SHOULD BE REPLACED BY SORTING FORWARD CLUSTERS (see the line above) !!!
						
						// THE NEXT LINE SHOULD BE MAYBE REPLACED BY SOMETHING LIKE LIKELIHOOD OF ALL WORDS 
						// FROM THE CURRENT CLUSTER (NOT ONLY ONE OF THEM) AND ALSO THE DISTANCE BETWEEN KWDS (THE NEARER THE BETTER)
						// select the best cluster which fits the distance between keywords
						
						if (!pClusterPrev->mBestFwdClusterFound) 
						{
							pClusterPrev->mpBestFwdCluster = pClusterCur;
							pClusterPrev->mBestFwdClusterFound = true;
						} 
						else if (pClusterPrev->mpBestFwdCluster->mConf < pClusterCur->mConf) 
						{
							pClusterPrev->mpBestFwdCluster = pClusterCur;
							pClusterPrev->mBestFwdClusterFound = true;
						}
					}

					mid_search += sign;
					if (sign == 1)
					{
						if (mid_search >= (int)kwdCur->mpClusters->size())
						{
							sign *= -1;
							mid_search = mid - 1; // because mid was already processed
							if (mid_search > 0)
							{
								continue; // if there are some hypotheses on the other side of 'mid' index, process them
							}
							else
							{
								break; // if there is nothing left on the other side of 'mid' index, we are done
							}
						}
					}
					else
					{
						if (mid_search < 0)
						{
							break; // if there is nothing left on the left side of 'mid' index, we are done
						}
					}

				} // while(bidir_search)
			} // for each cluster from kwdPrev
		} // if kwdPrev is OK
		kwdPrev = kwdCur;
	} // for each keyword from query
}

void QueryKwdList::FillQueryClustersFwd_recursive(
		InvIdxRecordsCluster* pParentCluster, 
		InvIdxRecordsCluster* pCurCluster, 
		QueryKwdList::iterator curKwd)
{
	// FORWARD
	//DBG("QueryKwdList::FillInvIdxRecordsClustersFwd_recursive()");
	QueryKwdList::iterator iFwdKwd = curKwd;
	iFwdKwd++;

	if (pCurCluster->mFwdClusters.size() == 0)
	{
		pParentCluster->mEndTime = pCurCluster->mEndTime;
	}
	
	// for all forward clusters of the current cluster
	for (InvIdxRecordsClustersPList::iterator ipCluster = pCurCluster->mFwdClusters.begin();
		 ipCluster != pCurCluster->mFwdClusters.end();
		 ++ipCluster)
	{
		// We need to check if the current cluster is not already in the keyword's cluster list
		// because the current cluster (ipCluster) could have been already added to the pParentCluster
		// for the forward keyword (iFwdKwd) 
		// (it may happen because of a longer chain of keywords in the query, 
		// so that i.e. the previous keyword has 2 clusters, which are not overlapped,
		// but they have the same forward clusters associated)
		if (!pParentCluster->ContainsCluster(*iFwdKwd, *ipCluster)) 
		{
			pParentCluster->Insert(*iFwdKwd, *ipCluster, (*ipCluster)->GetSourceType());
		} 
		// let's dive one more step into recursion
		if (iFwdKwd != --(this->end()))
		{
			FillQueryClustersFwd_recursive(pParentCluster, *ipCluster, iFwdKwd);
		}
	}
	//DBG("QueryKwdList::FillInvIdxRecordsClustersFwd_recursive()...done");
}


void QueryKwdList::FillQueryClustersBck_recursive(
		InvIdxRecordsCluster* pParentCluster, 
		InvIdxRecordsCluster* pCurCluster, 
		QueryKwdList::iterator curKwd)
{
	// BACKWARD
	QueryKwdList::iterator iBckKwd = curKwd;
	iBckKwd--;

	if (pCurCluster->mBckClusters.size() == 0)
	{
		pParentCluster->mStartTime = pCurCluster->mStartTime;
	}
		
	// for all backward clusters of the current cluster
	for (InvIdxRecordsClustersPList::iterator ipCluster = pCurCluster->mBckClusters.begin();
		 ipCluster != pCurCluster->mBckClusters.end();
		 ++ipCluster)
	{
		// add the backward cluster to the pParentCluster for the corresponding keyword
		// to see the reason of calling ContainsCluster see the method FillInvIdxRecordsClustersFwd_recursive
		if (!pParentCluster->ContainsCluster(*iBckKwd, *ipCluster))
		{
			pParentCluster->Insert(*iBckKwd, *ipCluster, (*ipCluster)->GetSourceType());
		}
		// if we are on the first kwd, the recursion is terminated
		if (iBckKwd != this->begin())
		{
			FillQueryClustersBck_recursive(pParentCluster, *ipCluster, iBckKwd);
		}
	}
}
		


void QueryKwdList::FillQueryClusters()
{
	DBG("QueryKwdList::FillQueryClusters()");
	QueryKwdList::iterator iBestKwd = GetLeastQueryClustersKwd();
	DBG("  iBestKwd:"<< iBestKwd->W_id);
	
	// for all clusters of iBestKwd
	for (InvIdxRecordsClusters::iterator iBestKwdCluster = iBestKwd->mpClusters->begin();
		 iBestKwdCluster != iBestKwd->mpClusters->end();
		 iBestKwdCluster ++)
	{
		InvIdxRecordsCluster tmpCluster;
		tmpCluster.mMeetingID = iBestKwdCluster->mMeetingID;
		// add the current cluster into the tmpCluster
		tmpCluster.Insert(iBestKwd->W_id, &(*iBestKwdCluster), iBestKwdCluster->GetSourceType());

		FillQueryClustersFwd_recursive(&tmpCluster, &(*iBestKwdCluster), iBestKwd);
		FillQueryClustersBck_recursive(&tmpCluster, &(*iBestKwdCluster), iBestKwd);

		tmpCluster.mValidKeywordsCount = tmpCluster.size();

		// add the group to the list of cluster groups
		bool add = false;
		if (mExactMatch)
		{
			vector<ID_t> unique_kwds;
			for (QueryKwdList::iterator iKwd=begin(); iKwd!=end(); iKwd++)
			{
				bool found = false;
				for (vector<ID_t>::iterator i=unique_kwds.begin(); i!=unique_kwds.end(); i++)
				{
					if (*i == iKwd->W_id)
					{
						found = true;
					}
				}
				if (!found)
				{
					unique_kwds.push_back(iKwd->W_id);
				}
			}
			if (tmpCluster.mValidKeywordsCount >= (int)unique_kwds.size())
			{
				add = true;
			}
		}
		else
		{
			add = true;
		}
		if (add)
		{
			// COMUTE THE ESTIMATED CONFIDENCE OF THE WHOLE CLUSTER
			TLatViterbiLikelihood minClusterConf = INF;//(*( tmpCluster.begin()->second.begin() ))->mConf;
			// for all keywords (in current group) find the one with the lowest confidence
			for (InvIdxRecordsCluster::iterator iKwd2OccurrenceList=tmpCluster.begin(); iKwd2OccurrenceList!=tmpCluster.end(); ++iKwd2OccurrenceList)
			{
				TLatViterbiLikelihood maxKwdConf = -INF;
				// for all occurrences of the keyword find the one with the highest confidence
				for (int i=0; i<iKwd2OccurrenceList->second.size(); i++)
				{
					LatIndexer::Occurrence* pOccurrence = iKwd2OccurrenceList->second.GetOccurrence(i);
					if (maxKwdConf < pOccurrence->mConf) {
						maxKwdConf = pOccurrence->mConf;
					}
				}
				// select the minimal confidence for the current group (of all keywords in group)
				if (minClusterConf > maxKwdConf ) {
					minClusterConf = maxKwdConf;
				}
			}
			tmpCluster.mConf = minClusterConf;
			//DBG("FillQueryClusters: adding cluster "<<tmpCluster);

			this->mInvIdxRecordsClusters.push_back(tmpCluster);
		}
	}
	DBG("QueryKwdList::FillQueryClusters()...done");
}

void QueryKwdList::InvertedIndexLookupResultsClustering()
{
	// IF THE REVERSE INDEX IS SORTED BY WORDID AND TIME, IT IS POSSIBLE TO USE BINARY SEARCH,
	// OTHERWISE (IE. WHEN IT IS SORTED BY CONFIDENCE), IT IS NEEDED TO DO IT SEQUENTIALLY

	// FOLLOWING LOOPS ARE CHOOSING THE FORWARD AND BACKWARD CLUSTERS FOR EACH QUERY-KEYWORD'S CLUSTER
	// for each keyword from query
	QueryKwdList::iterator kwdPrev = this->end();
	for (QueryKwdList::iterator kwdCur = this->begin(); kwdCur != this->end(); ++kwdCur) 
	{
		if (kwdPrev != this->end()) 
		{
			DBG(kwdPrev->W << " -> " << kwdCur->W);

			// for each cluster from kwdPrev
			for (int i_rec_prev = 0; i_rec_prev < (int)kwdPrev->searchResults.size(); i_rec_prev++)
			{
//			for (vector<Search::InvIdxRecord>::iterator iRecPrev = kwdPrev->searchResults.mvByTime.begin(); 
//				 iRecPrev != kwdPrev->recClusters.end(); 
//				 ++iRecPrev) 

				DBG("i_rec_prev:"<<i_rec_prev);
				DBG("mpByTime:"<<kwdPrev->searchResults.mpByTime);
				int recPrevIdx = kwdPrev->searchResults.mpByTime[i_rec_prev];
				QueryKwd::InvIdxRecord *pRecPrev = &(kwdPrev->searchResults.mvByConfidence[recPrevIdx]);
				DBG("kwdPrev->searchResults.mvByConfidence.size():"<<kwdPrev->searchResults.mvByConfidence.size());
				DBG("recPrevIdx:"<<recPrevIdx);
				/*
				bool dbg = pRecPrev->mMeetingID == 19 && pRecPrev->mStartTime > 145 && pRecPrev->mStartTime < 146;
				if (dbg) DBG_FORCE("recPrev: "<<*pRecPrev);
				if (dbg)
				{
					for (int i=0; i<kwdCur->searchResults.size(); i++)
					{
						int recMidSearchIdx = kwdCur->searchResults.mpByTime[i];
						QueryKwd::InvIdxRecord *pRecCur = &(kwdCur->searchResults.mvByConfidence[recMidSearchIdx]);
						DBG_FORCE("res: "<<*pRecCur);
					}
				}
				*/
				pRecPrev->mBestFwdKwdClusterFound = false;
				
				int mid = 0;
				//--------------------------------------------------
				// BINARY SEARCH
				//
				bool found = false;
				int left = 0;
				int right = kwdCur->searchResults.size() - 1;
				while (left <= right)
				{
					mid = (int)floor((right-left)/2)+left;

					DBG("kwdPrev->searchResults.mpByTime:"<<kwdPrev->searchResults.mpByTime);
					DBG("kwdPrev->searchResults.size():"<<kwdPrev->searchResults.size());
					DBG("mid:"<<mid);
					DBG("kwdPrev->searchResults.mpByTime[mid]:"<<kwdPrev->searchResults.mpByTime[mid]);
					int recMidIdx = kwdCur->searchResults.mpByTime[mid];
//					int recLeftIdx = kwdCur->searchResults.mpByTime[left];
//					int recRightIdx = kwdCur->searchResults.mpByTime[right];

					DBG("left:"<<left<<" right:"<<right<<" mid:"<<mid);
					DBG("pRecPrev->tStart > kwdCur->recClusters[mid].tEnd ... "<<pRecPrev->mStartTime<<" > "<<kwdCur->searchResults.mvByConfidence[recMidIdx].mEndTime);
					DBG("pRecPrev->tEnd < kwdCur->recClusters[mid].tStart ... "<<pRecPrev->mEndTime<<" < "<<kwdCur->searchResults.mvByConfidence[recMidIdx].mStartTime);
					//if (dbg) DBG_FORCE("mid: "<<kwdCur->searchResults.mvByConfidence[recMidIdx]);
					if (pRecPrev->mMeetingID > kwdCur->searchResults.mvByConfidence[recMidIdx].mMeetingID)
					{
						left  = mid+1;
					}
					else if (pRecPrev->mMeetingID < kwdCur->searchResults.mvByConfidence[recMidIdx].mMeetingID)
					{
						right = mid-1;
					}
					// now meetingIDs are the same, let us look at the time
					else if (this->mOverlapping && pRecPrev->mStartTime > kwdCur->searchResults.mvByConfidence[recMidIdx].mEndTime)
					{
						left  = mid+1;
					}
					else if (this->mOverlapping && pRecPrev->mEndTime < kwdCur->searchResults.mvByConfidence[recMidIdx].mStartTime)
					{
						right = mid-1;
					}
					else
					{
						found = true;
						break;
					}
				}

				if (!found) 
				{
					continue;
				}
				//--------------------------------------------------

				int mid_search = mid;
				int sign = 1;
//				DBG_FORCE("kwdCur->recClusters.size():"<<kwdCur->recClusters.size());
				while (1)
				{
					int recMidSearchIdx = kwdCur->searchResults.mpByTime[mid_search];
					QueryKwd::InvIdxRecord *pRecCur = &(kwdCur->searchResults.mvByConfidence[recMidSearchIdx]);
					//if (dbg) DBG_FORCE("mid_search:"<<mid_search);
					//if (dbg) DBG_FORCE("pRecCur:"<<*pRecCur);

					if (
						(pRecCur->mMeetingID != pRecPrev->mMeetingID)
						||
						(this->mOverlapping &&
						 !is_overlapping(pRecCur->mStartTime, pRecCur->mEndTime,
										pRecPrev->mStartTime, pRecPrev->mEndTime)
						)
					   )
					{
						DBG("NOT OVERLAPPING");
						if (sign == -1)
						{
							break;
						}
						else
						{
							sign *= -1;
							mid_search = mid - 1; // because mid was already processed
							if (mid_search > 0)
							{
								continue; // if there are some hypotheses on the other side of 'mid' index, process them
							}
							else
							{
								break; // if there is nothing left on the other side of 'mid' index, we are done
							}
						}
					}

					DBG("  clusterCur: "<<*pRecCur);

					// If the distance between the kwdCur's cluster and kwdPrev's cluster fits the distance specified by search query
					// For free kwds, just select the one with best posterior
					if (kwdPrev->nb_next == -1 ||
						// operator < (lessThan)
						(
						  (
							(kwdPrev->lessThan && pRecPrev->mEndTime < pRecCur->mStartTime) ||
							(kwdPrev->greaterThan && pRecCur->mEndTime < pRecPrev->mStartTime) ||
							(!kwdPrev->lessThan && !kwdPrev->greaterThan)
						  )
						  && abs((int)(pRecCur->mStartTime - pRecPrev->mEndTime)) <= kwdPrev->nb_next)
						)
					{
						DBG("pRecPrev: meetingID="<<pRecPrev->mMeetingID<<" t["<<pRecPrev->mStartTime<<".."<<pRecPrev->mEndTime<<"]");
						DBG("pRecCur: meetingID="<<pRecCur->mMeetingID<<" t["<<pRecCur->mStartTime<<".."<<pRecCur->mEndTime<<"]");
						// add the current cluster to the previous cluster's forward list / and vice versa
						// clusters are already sorted by confidence
						pRecPrev->mFwdClusters.push_back(pRecCur);
						pRecCur->mBckClusters.push_back(pRecPrev);

//						!!! THE FOLLOWING CODE SHOULD BE REPLACED BY SORTING FORWARD CLUSTERS (see the line above) !!!
						
						// THE NEXT LINE SHOULD BE MAYBE REPLACED BY SOMETHING LIKE LIKELIHOOD OF ALL WORDS 
						// FROM THE CURRENT CLUSTER (NOT ONLY ONE OF THEM) AND ALSO THE DISTANCE BETWEEN KWDS (THE NEARER THE BETTER)
						// select the best cluster which fits the distance between keywords
						
						if (!pRecPrev->mBestFwdKwdClusterFound) 
						{
							pRecPrev->mpBestFwdKwdCluster = pRecCur;
							pRecPrev->mBestFwdKwdClusterFound = true;
						} 
						else if (pRecPrev->mpBestFwdKwdCluster->mConf < pRecCur->mConf) 
						{
							pRecPrev->mpBestFwdKwdCluster = pRecCur;
							pRecPrev->mBestFwdKwdClusterFound = true;
						}
					}

					mid_search += sign;
					if (sign == 1)
					{
						if (mid_search >= (int)kwdCur->searchResults.size())
						{
							sign *= -1;
							mid_search = mid - 1; // because mid was already processed
							if (mid_search > 0)
							{
								continue; // if there are some hypotheses on the other side of 'mid' index, process them
							}
							else
							{
								break; // if there is nothing left on the other side of 'mid' index, we are done
							}
						}
					}
					else
					{
						if (mid_search < 0)
						{
							break; // if there is nothing left on the left side of 'mid' index, we are done
						}
					}

				} // while(bidir_search)
			} // for each cluster from kwdPrev
		} // if kwdPrev is OK
		kwdPrev = kwdCur;
	} // for each keyword from query
}

void QueryKwdList::PrintInvertedIndexLookupResultsClusters()
{
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("Keywords' Clusters");
	for (QueryKwdList::iterator iKwd = this->begin(); iKwd != this->end(); ++iKwd) 
	{
		DBG_FORCE("Kwd: "<<iKwd->W<<" W_id:"<<iKwd->W_id);
		for (vector<QueryKwd::InvIdxRecord>::iterator iInvRec = iKwd->searchResults.mvByConfidence.begin(); 
			 iInvRec != iKwd->searchResults.mvByConfidence.end(); 
			 iInvRec ++) 
		{
			DBG_FORCE("  meetingID:"<<iInvRec->mMeetingID<<" t:"<<iInvRec->mStartTime<<".."<<iInvRec->mEndTime<<" conf:"<<iInvRec->mConf);
			for (QueryKwd::InvIdxRecord::InvIdxRecordList::iterator iFwdCluster=iInvRec->mFwdClusters.begin();
				 iFwdCluster != iInvRec->mFwdClusters.end();
				 iFwdCluster ++)
			{
				DBG_FORCE("    fwdCluster: W_id:"<<(*iFwdCluster)->mWordID<<" t:"<<(*iFwdCluster)->mStartTime<<".."<<(*iFwdCluster)->mEndTime<<" conf:"<<(*iFwdCluster)->mConf);
			}
			for (QueryKwd::InvIdxRecord::InvIdxRecordList::iterator iBckCluster=iInvRec->mBckClusters.begin();
				 iBckCluster != iInvRec->mBckClusters.end();
				 iBckCluster ++)
			{
				DBG_FORCE("    bckCluster: W_id:"<<(*iBckCluster)->mWordID<<" t:"<<(*iBckCluster)->mStartTime<<".."<<(*iBckCluster)->mEndTime<<" conf:"<<(*iBckCluster)->mConf);
			}
		}

	}
}

void QueryKwdList::FillInvIdxRecordsClustersFwd_recursive(
		InvIdxRecordsCluster *group, 
		QueryKwd::InvIdxRecord* curInvIdxRecord, 
		QueryKwdList::iterator curKwd,
		SourceType::Enum sourceType)
{
	// FORWARD
	DBG("QueryKwdList::FillInvIdxRecordsClustersFwd_recursive()");
	QueryKwdList::iterator iFwdKwd = curKwd;
	iFwdKwd++;

	if (curInvIdxRecord->mFwdClusters.size() == 0)
	{
		group->mEndTime = curInvIdxRecord->mEndTime;
	}
	
	// for all forward clusters of the current cluster
	for (QueryKwd::InvIdxRecord::InvIdxRecordList::iterator iInvRec = curInvIdxRecord->mFwdClusters.begin();
		 iInvRec != curInvIdxRecord->mFwdClusters.end();
		 ++iInvRec)
	{
		// We need to check if the current cluster is not already in the keyword's cluster list
		// because the current cluster (iInvRec) could have been already added to the group
		// for the forward keyword (iFwdKwd) 
		// (it may happen because of a longer chain of keywords in the query, 
		// so that i.e. the previous keyword has 2 clusters, which are not overlapped,
		// but they have the same forward clusters associated)
		if (!group->ContainsCluster(*iFwdKwd, *iInvRec)) 
		{
			group->Insert(*iFwdKwd, *iInvRec, sourceType);
		} 
		// let's dive one more step into recursion
		if (iFwdKwd != --(this->end()))
		{
			FillInvIdxRecordsClustersFwd_recursive(group, *iInvRec, iFwdKwd, sourceType);
		}
	}
	DBG("QueryKwdList::FillInvIdxRecordsClustersFwd_recursive()...done");
}


void QueryKwdList::FillInvIdxRecordsClustersBck_recursive(
		InvIdxRecordsCluster *group, 
		QueryKwd::InvIdxRecord* curInvIdxRecord, 
		QueryKwdList::iterator curKwd,
		SourceType::Enum sourceType)
{
	// BACKWARD
	QueryKwdList::iterator iBckKwd = curKwd;
	iBckKwd--;

	if (curInvIdxRecord->mBckClusters.size() == 0)
	{
		group->mStartTime = curInvIdxRecord->mStartTime;
	}
		
	// for all backward clusters of the current cluster
	for (QueryKwd::InvIdxRecord::InvIdxRecordList::iterator iInvRec = curInvIdxRecord->mBckClusters.begin();
		 iInvRec != curInvIdxRecord->mBckClusters.end();
		 ++iInvRec)
	{
		// add the backward cluster to the group for the corresponding keyword
		// to see the reason of calling ContainsCluster see the method FillInvIdxRecordsClustersFwd_recursive
		if (!group->ContainsCluster(*iBckKwd, *iInvRec))
		{
			group->Insert(*iBckKwd, *iInvRec, sourceType);
		}
		// if we are on the first kwd, the recursion is terminated
		if (iBckKwd != this->begin())
		{
			FillInvIdxRecordsClustersBck_recursive(group, *iInvRec, iBckKwd, sourceType);
		}
	}
}
		


void QueryKwdList::FillInvIdxRecordsClusters(SourceType::Enum sourceType)
{
	DBG("QueryKwdList::fillClusterGroups()");
	QueryKwdList::iterator iBestKwd = GetLeastInvIdxRecordsKwd();
	//mInvIdxRecordsClusters.iBestKwd = iBestKwd;
	DBG("  iBestKwd:"<< iBestKwd->W_id);
	
	// for all clusters of iBestKwd
	for (vector<QueryKwd::InvIdxRecord>::iterator iBestKwdRec = iBestKwd->searchResults.mvByConfidence.begin();
		 iBestKwdRec != iBestKwd->searchResults.mvByConfidence.end();
		 iBestKwdRec ++)
	{
		InvIdxRecordsCluster tmpCluster;
		tmpCluster.mSourceType = sourceType;
		tmpCluster.mMeetingID = iBestKwdRec->mMeetingID;
		// add the current cluster into the tmpCluster
		tmpCluster.Insert(iBestKwd->W_id, &(*iBestKwdRec), sourceType);

		FillInvIdxRecordsClustersFwd_recursive(&tmpCluster, &(*iBestKwdRec), iBestKwd, sourceType);
		FillInvIdxRecordsClustersBck_recursive(&tmpCluster, &(*iBestKwdRec), iBestKwd, sourceType);

		tmpCluster.mValidKeywordsCount = tmpCluster.size();

		// add the group to the list of cluster groups
		bool add = false;
		if (mExactMatch)
		{
			if (tmpCluster.mValidKeywordsCount == (int)this->size())
			{
				add = true;
			}
		}
		else
		{
			add = true;
		}
		if (add)
		{
			// COMUTE THE ESTIMATED CONFIDENCE OF THE WHOLE CLUSTER
			TLatViterbiLikelihood minClusterConf = INF;//(*( tmpCluster.begin()->second.begin() ))->mConf;
			// for all keywords (in current group) find the one with the lowest confidence
			for (InvIdxRecordsCluster::iterator iKwd2OccurrenceList=tmpCluster.begin(); 
				iKwd2OccurrenceList != tmpCluster.end(); 
				iKwd2OccurrenceList ++)
			{
				TLatViterbiLikelihood maxKwdConf = -INF;
				// for all occurrences of the keyword find the one with the highest confidence
				for (int i=0; i<iKwd2OccurrenceList->second.size(); i++)
				{
					LatIndexer::Occurrence* pOccurrence = iKwd2OccurrenceList->second.GetOccurrence(i);
					if (maxKwdConf < pOccurrence->mConf) {
						maxKwdConf = pOccurrence->mConf;
					}
				}
				// select the minimal confidence for the current group (of all keywords in group)
				if (minClusterConf > maxKwdConf ) {
					minClusterConf = maxKwdConf;
				}
			}
			tmpCluster.mConf = minClusterConf;
			DBG("FillInvIdxRecordsClusters: adding cluster "<<tmpCluster);

			this->mInvIdxRecordsClusters.push_back(tmpCluster);
		}
	}
	DBG("QueryKwdList::fillClusterGroups()...done");
}


void QueryKwdList::SortInvIdxRecordsClusters(const InvIdxRecordsClusters::SortType::Enum sortType)
{
	mInvIdxRecordsClusters.SortClusters(sortType);
}


InvIdxRecordsCluster* InvIdxRecordsClusters::FindCluster(const LatIndexer::Occurrence &where)
{
	DBG("FindCluster()");
	if (size() == 0)
	{
		return NULL;
	}
	if (!mpSortedByTime)
	{
		CERR("ERROR: the cluster list is not sorted by time");
		EXIT();
	}
	//--------------------------------------------------
	// BINARY SEARCH
	//
	bool found = false;
	int mid = 0;
	int left = 0;
	int right = this->size() - 1;
	DBG_FORCE("size:"<<this->size());
	while (left <= right)
	{
		mid = (int)floor((right-left)/2)+left;

		DBG("this->size():"<<this->size());
		DBG("mid:"<<mid);

		DBG("left:"<<left<<" right:"<<right<<" mid:"<<mid);
		DBG("where.mMeetingID:"<<where.mMeetingID<<" (*this)[mid].mMeetingID:"<<mpSortedByTime[mid]->mMeetingID);
		DBG("where.mStartTime:"<<where.mStartTime<<" (*this)[mid].mStartTime:"<<mpSortedByTime[mid]->mStartTime);
		DBG("where.mEndTime:"<<where.mEndTime<<" (*this)[mid].mEndTime:"<<mpSortedByTime[mid]->mEndTime);
		if (where.mMeetingID > mpSortedByTime[mid]->mMeetingID)
		{
			left  = mid+1;
		}
		else if (where.mMeetingID < mpSortedByTime[mid]->mMeetingID)
		{
			right = mid-1;
		}
		// now meetingIDs are the same, let us look at the time
		else if (where.mStartTime > mpSortedByTime[mid]->mEndTime)
		{
			left  = mid+1;
		}
		else if (where.mEndTime < mpSortedByTime[mid]->mStartTime)
		{
			right = mid-1;
		}
		else
		{
			found = true;
			break;
		}
	}

	return !found ? NULL : mpSortedByTime[mid];
	//--------------------------------------------------
}


void QueryKwdList::PrintInvIdxRecordsClusters()
{
	int cluster_counter = 0;
	for(InvIdxRecordsClusters::iterator iCluster = mInvIdxRecordsClusters.begin();
		iCluster != mInvIdxRecordsClusters.end();
		iCluster ++)
	{
		// each group
		CERR("--------------------------------------------------------------------------------");
		CERR("Cluster #" << ++cluster_counter << " " << endl << *iCluster);
	}
	CERR("--------------------------------------------------------------------------------");
	CERR("Search::PrintInvIdxRecordsClusters()...done");
}


/**
  @brief comparison function for sorting cluster groups (sorted first by confidence and then by the number of valid keywords)
*/
bool InvIdxRecordsClusters::cmp_InvIdxRecordsCluster(const InvIdxRecordsCluster& lv, const InvIdxRecordsCluster& rv)
{
	if (lv.mValidKeywordsCount == rv.mValidKeywordsCount)
	{
		return lv.mConf > rv.mConf;
	} else {
		return lv.mValidKeywordsCount > rv.mValidKeywordsCount;
	}
}


//bool InvIdxRecordsClusters::cmp_PCluster(const LatIndexer::PCluster &lv, const LatIndexer::PCluster &rv)
bool InvIdxRecordsClusters::cmp_PRevIdxRecord(const QueryKwd::InvIdxRecord* lv, const QueryKwd::InvIdxRecord* rv)
{
	return lv->mConf > rv->mConf;
}

bool InvIdxRecordsClusters::cmp_InvIdxRecordsCluster_ByTime(const InvIdxRecordsCluster_Pointer& lv, const InvIdxRecordsCluster_Pointer& rv)
{
	if (lv->mMeetingID == rv->mMeetingID)
	{
		return lv->mEndTime < rv->mEndTime;
	}
	else
	{
		return lv->mMeetingID < rv->mMeetingID;
	}
}

void InvIdxRecordsClusters::SortClusters(InvIdxRecordsClusters::SortType::Enum sortType) 
{
	if (sortType == InvIdxRecordsClusters::SortType::ByConf || sortType == InvIdxRecordsClusters::SortType::ByTimeAndConf)
	{
		//--------------------------------------------------
		// SORT BY CONFIDENCE
		//--------------------------------------------------
		sort( this->begin(), this->end(), cmp_InvIdxRecordsCluster );
	}
	
	if (sortType == SortType::ByTime || sortType == SortType::ByTimeAndConf)
	{
		//--------------------------------------------------
		// SORT BY TIME
		//--------------------------------------------------
		DBG_FORCE("InvIdxRecordsClusters.size():"<<this->size());
		mpSortedByTime = new InvIdxRecordsCluster_Pointer [this->size()];
		for (unsigned int i=0; i<this->size(); i++)
		{
			mpSortedByTime[i] = &((*this)[i]);
		}
		/*
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("NOT SORTED:");
		for (unsigned int i=0; i<this->size(); i++)
		{
			DBG_FORCE("mpSortedByTime["<<i<<"]: "<<*mpSortedByTime[i]);
		}
		*/
		QuickSort(mpSortedByTime, (int)0, (int)this->size()-1, InvIdxRecordsClusters::cmp_InvIdxRecordsCluster_ByTime);
		/*
		DBG_FORCE("--------------------------------------------------");
		DBG_FORCE("SORTED:");
		for (unsigned int i=0; i<this->size(); i++)
		{
			DBG_FORCE("mpSortedByTime["<<i<<"]: "<<*mpSortedByTime[i]);
		}
		EXIT();
		*/
		DBG_FORCE("InvIdxRecordsClusters.size():"<<this->size());
	}
}

void InvIdxRecordsClusters::Print()
{
	DBG_FORCE("--------------------------------------------------------------------------------");
	DBG_FORCE("Clusters:");
	for (iterator iCluster = begin(); iCluster != end(); iCluster++)
	{
		DBG_FORCE(*iCluster);
	}
	DBG_FORCE("--------------------------------------------------------------------------------");
}


void OccurrenceWithSourceList::Insert(LatIndexer::Occurrence* pOccurrence, SourceType::Enum sourceType)
{
	mOccurrencePList.push_back(pOccurrence);
	mSourceTypeList.push_back(sourceType);

	// set the meeting of the occurrence list
	mMeetingID = pOccurrence->mMeetingID;

	// set the end time of the occurrence list
	if (mEndTime < pOccurrence->mEndTime)
	{
		mEndTime = pOccurrence->mEndTime;
	}

	// set the start time of the occurrence list
	if (mStartTime > pOccurrence->mStartTime)
	{
		mStartTime = pOccurrence->mStartTime;
	}

	// set the confidence of the occurrence list
	if (mConf > pOccurrence->mConf)
	{
		mConf = pOccurrence->mConf;
	}
}


void InvIdxRecordsCluster::Insert(ID_t W_id, LatIndexer::Occurrence* pOccurrence, SourceType::Enum sourceType)
{
	// select min confidence of all keyword's clusters
//	if (this->conf > pOccurrence->conf) {
//		this->conf = pOccurrence->conf;
//	}
	// add the given record to the clusters of the given keyword in the current cluster
	(*this)[W_id].Insert(pOccurrence, sourceType);
	
	// if the meetingID is not set yet, then assign the pOccurrence->meetingID to it
	if (mMeetingID == -1)
	{
		mMeetingID = pOccurrence->mMeetingID;
	}
	
	// set the end time of the cluster
	if (mEndTime < pOccurrence->mEndTime)
	{
		mEndTime = pOccurrence->mEndTime;
	}

	// set the start time of the cluster
	if (mStartTime > pOccurrence->mStartTime)
	{
		mStartTime = pOccurrence->mStartTime;
	}
}

bool InvIdxRecordsCluster::ContainsCluster(const QueryKwd& kwd, LatIndexer::Occurrence* pCmpOccurrence)
{
	// compare with all clusters of the given keyword in the group
	for (int i=0; i<(*this)[kwd.W_id].size(); i++)
	{
		LatIndexer::Occurrence* pOccurrence = (*this)[kwd.W_id].GetOccurrence(i);
		if (*pOccurrence == *pCmpOccurrence)
		{
			return true;
		}
	}
	return false;
}



QueryKwdList::iterator QueryKwdList::GetLeastInvIdxRecordsKwd()
{
	// select the keyword with the minimal number of clusters = the most discriminative keyword
	QueryKwdList::iterator iBestKwd = this->begin();
	for (QueryKwdList::iterator iQueryKwd = ++this->begin(); iQueryKwd != this->end(); ++iQueryKwd) 
	{
		if (iQueryKwd->searchResults.size() < iBestKwd->searchResults.size())
		{
			iBestKwd = iQueryKwd;
		}
	}
	return iBestKwd;
}

QueryKwdList::iterator QueryKwdList::GetLeastQueryClustersKwd()
{
	// select the keyword with the minimal number of clusters = the most discriminative keyword
	QueryKwdList::iterator iBestKwd = this->begin();
	for (QueryKwdList::iterator iQueryKwd = ++this->begin(); iQueryKwd != this->end(); ++iQueryKwd) 
	{
		if (iQueryKwd->mpClusters->size() < iBestKwd->mpClusters->size())
		{
			iBestKwd = iQueryKwd;
		}
	}
	return iBestKwd;
}

