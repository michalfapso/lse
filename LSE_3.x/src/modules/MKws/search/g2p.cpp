#include <iostream>
#include "g2p.h"
#include "lattypes.h"
#include "global.h"

using namespace lse;
using namespace std;
using namespace gpt;

int G2P::GetTranscs(std::string word, std::list<gpt::PhnTrans::pt_entry> *list)
{
	return PT.GetTranscs(word.c_str(), list);
}


bool G2P::Load(string filenameRules, string filenameSymbols, string filenameLexicon, int maxVariants, bool scaleProb, float probTresh)
{
	int ret;
	
	// load rules
	ret = GPT.LoadRules(filenameRules.c_str());
	if(ret != GPT_OK)
	{
		char msg[1024];
		sprintf(msg, "Can not open grapheme to phoneme conversion rules: %s\n", filenameRules.c_str());
		CERR(msg);
		return false;
	}

	// load symbols
	ret = GPT.LoadSymbols(filenameSymbols.c_str());
	if(ret != GPT_OK)
	{
		char msg[1024];
		sprintf(msg, "Can not open symbols for grapheme to phoneme conversion rules: %s\n", filenameSymbols.c_str());
		CERR(msg);
		return false;
	}

	// set GPT parameters
	GPT.SetMaxProns(maxVariants);
	GPT.SetScalePronProb(scaleProb);
	GPT.SetLowestPronProb(probTresh);

	// configure phoneme transcription (grapheme to phoneme transc)
	PT.SetGPTrans(&GPT);


	// load lexicon
	Lex.Clear();
		
	char errWord[256];
	bool save_bin = true;
	ret = Lex.Load(filenameLexicon.c_str(), 0, save_bin, errWord);
	if(ret != LEX_OK)
	{
		char msg[1024];
		switch(ret)
		{
			case LEX_INPUTFILE:
				sprintf(msg, "Can not open lexicon %s\n", filenameLexicon.c_str());
				break;
			case LEX_TRANSC:
				sprintf(msg, "Invalid word transcription '%s', file %s\n", errWord, filenameLexicon.c_str());
				break;
			case LEX_ALREDYEXISTS:
				sprintf(msg, "Word transctiption alredy exists '%s', file %s\n", errWord, filenameLexicon.c_str());
				break;
			case LEX_SYNTAX:
				sprintf(msg, "Missing transcription for word '%s', file %s\n", errWord, filenameLexicon.c_str());
				break;
		}
		CERR(msg);
		return false;
	}

	// configure phoneme transcription (lexicon merger)
	PT.SetLexicon(&Lex);
	return true;
}

