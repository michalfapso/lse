#ifndef SEARCHCONFIG_H
#define SEARCHCONFIG_H

#include "lattypes.h"
#include "g2p.h"
#include "lat.h"
#include "lexicon.h"
#include "normlexicon.h"

namespace lse {

typedef std::map<ID_t, int> MeetingsList;

enum Results_EType {
	ResultsETypeNormal,		// text format
	ResultsETypeXml,		// XML format for MBrowser
	ResultsETypeXml_std,	// XML format for STD evals
	ResultsETypeExp,		// Suitable for experiments, easy to parse
	ResultsETypeXmlTranscripts,	// XML format for MBrowser transcripts
};



class SearchConfig
{
	public:

		Lexicon *lexiconLvcsr; // stores records of type: wordID -> word  and  word -> wordID
		Lexicon *lexiconPhn; // stores records of type: wordID -> word  and  word -> wordID
		Lattice::Indexer *indexerLvcsr;
		Lattice::Indexer *indexerPhn;
		G2P *g2p;
//		Dct *dct;
		NormLexicon normLexicon_lvcsr;
		NormLexicon normLexicon_phn;
		MeetingsList mValidMeetings;

		//application parameters/*{{{*/
		std::string p_lexicon_lvcsr			;
		std::string p_lexicon_phn			;
		std::string p_meeting 				;
		std::string p_searchindex_lvcsr		;
		std::string p_searchindex_phn		;
		std::string p_phoneme_string 		;
		std::string p_query 				;
		std::string p_data_dir_path_phn		;
		std::string p_data_dir_path_lvcsr	;
		unsigned int p_hyps_per_stream 		;
		int p_clusters_count 				;
		float p_time_delta 					;
		int p_fwd_nodes_count 				;
		int p_bck_nodes_count 				;
		bool p_show_all_matched_keywords 	;
		bool p_show_all_clusters 			;
		bool p_show_reverse_index_lookup_results ;
		bool p_time_index_print				;
		bool p_dbg 							;
		std::string p_meeting_list 			;
		float p_phrase_kwd_neiborhood 		;
		bool p_word_clusters 				;
		int p_max_results 					;
		int p_port 							;
		bool p_only_print_candidates 		;


		// g2p
//		std::string p_g2p_dictionary		;
		std::string p_g2p_lexicon			;
		std::string p_g2p_rules				;
		std::string p_g2p_symbols			;
		int p_g2p_max_variants				;
		bool p_g2p_scale_prob				;
		float p_g2p_prob_tresh				;


		bool p_only_print_candidate_trigrams;
		Results_EType p_results_output_type;
		char p_record_delim					;
		int p_record_cutoff_column			;
		int p_record_channel_column			;
		std::string p_normalization_lexicon_lvcsr;
		std::string p_normalization_lexicon_phn	;
		float p_hard_decision_treshold		;
//		float p_wipen						;
//		float p_pipen						;
		bool p_phn_query_multiword_to_singleword;
		bool p_verify_candidates			;
		bool p_results_convert_to_local_time;
		int p_lat_filename_start_time_field ;
		float p_latname_time_multiplier		;
		float p_results_start_time_subtractor;
		/*}}}*/


		SearchConfig() :
			lexiconLvcsr ( NULL ), // stores records of type: wordID -> word  and  word -> wordID
			lexiconPhn ( NULL ), // stores records of type: wordID -> word  and  word -> wordID
			indexerLvcsr ( NULL ),
			indexerPhn ( NULL ),
			g2p ( NULL ),
//			dct ( NULL ),
			p_lexicon_lvcsr( "" ),
			p_lexicon_phn( "" ),
			p_meeting( "" ),
			p_searchindex_lvcsr( "" ),
			p_searchindex_phn( "" ),
			p_phoneme_string( "" ),
			p_query( "" ),
			p_data_dir_path_phn( "" ),
			p_data_dir_path_lvcsr( "" ),
			p_hyps_per_stream( 2 ),
			p_clusters_count( -1 ),
			p_time_delta( 0 ),
			p_fwd_nodes_count( 5 ),
			p_bck_nodes_count( 5 ),
			p_show_all_matched_keywords( false ),
			p_show_all_clusters( false ),
			p_show_reverse_index_lookup_results( false ),
			p_time_index_print( false ),
			p_dbg( false ),
			p_meeting_list( "" ),
			p_phrase_kwd_neiborhood( 0.0 ),
			p_word_clusters( false ),
			p_max_results( 10 ),
			p_port( 0 ),
			p_only_print_candidates( false ),
//			p_g2p_dictionary( "" ),
			p_g2p_lexicon( "" ),
			p_g2p_rules( "" ),
			p_g2p_symbols( "" ),
			p_g2p_max_variants( -1 ),
			p_g2p_scale_prob( false ),
			p_g2p_prob_tresh( -1.0f ),
			p_only_print_candidate_trigrams( false ),
			p_results_output_type( ResultsETypeNormal ),
			p_record_delim( '_' ),
			p_record_cutoff_column( 0 ),
			p_record_channel_column( 0 ),
			p_normalization_lexicon_lvcsr( "" ),
			p_normalization_lexicon_phn( "" ),
			p_hard_decision_treshold( 0.0 ),
//			p_wipen( 0.0 ),
//			p_pipen( 0.0 ),
			p_phn_query_multiword_to_singleword( false ),
			p_verify_candidates( true ),
			p_results_convert_to_local_time( false ),
			p_lat_filename_start_time_field( 0 ),
			p_latname_time_multiplier( 1 ),
			p_results_start_time_subtractor( 0.0 )
		{
		}

		void ParseCommandLineArguments(int argc, char** &argv);

		LatMeetingIndexer* GetMeetingIndexer();
};

} // namespace lse {

#endif
