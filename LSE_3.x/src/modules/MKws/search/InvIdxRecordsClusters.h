#ifndef SEARCHCLUSTERGROUPS_H
#define SEARCHCLUSTERGROUPS_H

#include <vector>
#include <map>
#include "lattypes.h"
#include "querykwdlist.h"


namespace lse {

//================================================================================
//	InvIdxRecordsCluster
//================================================================================
// kwd.W_id -> InvIdxRecordList
class InvIdxRecordsCluster : public std::map<ID_t, QueryKwd::InvIdxRecordList>
{
	public:
		ID_t					mMeetingID;
		TLatViterbiLikelihood 	conf; // min confidence of all clusters
		int 					validKeywordsCount;				
		LatTime					mStartTime;
		LatTime					mEndTime;

		InvIdxRecordsCluster() : mMeetingID(-1), conf(0), validKeywordsCount(0), mStartTime(INF), mEndTime(-INF) {}

		friend std::ostream& operator<<(std::ostream& os, const InvIdxRecordsCluster& g);
		
		void Insert(ID_t W_id, QueryKwd::RevIdxRecord* pRevIdxRecord);
		void Insert(QueryKwdList::iterator iKwd, QueryKwd::RevIdxRecord* pRevIdxRecord) { Insert(iKwd->W_id, pRevIdxRecord); }
		bool containsCluster(QueryKwdList::iterator iKwd, const QueryKwd::RevIdxRecord &cmpRevIdxRecord);
		LatTime GetStartTime() { return mStartTime; }
		LatTime GetEndTime()   { return mEndTime; }
};

//================================================================================
//	InvIdxRecordsClusters
//================================================================================
// list of InvIdxRecordsCluster items
class InvIdxRecordsClusters : public std::vector< InvIdxRecordsCluster > 
{
	public:
		QueryKwdList::iterator iBestKwd; ///< the keyword with the least number of hits in the reverse index
		
		static bool cmp_SearchClusterGroup(const InvIdxRecordsCluster& lv, const InvIdxRecordsCluster& rv);
		static bool cmp_PRevIdxRecord(const QueryKwd::RevIdxRecord* lv, const QueryKwd::RevIdxRecord* rv);
		void sortGroups();
};


} // namespace lse

#endif
