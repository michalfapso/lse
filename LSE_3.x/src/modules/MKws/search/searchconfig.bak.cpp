#include "searchconfig.h"
#include "latbinfile.h"

using namespace lse;
using namespace std;

void SearchConfig::ParseCommandLineArguments(int argc, char** &argv)
{
//	DBG_FORCE("ParseCommandLineArguments("<<argv<<")");
	int j=1;
	while (j<argc) {

		DBG_FORCE("argv["<<j<<"]:" << argv[j]);
			
		// break cycle at the end of parameter list
		if (argv[j][0] != '-') {
			break;
		
		} else if (strcmp(argv[j], "-lexicon-idx") == 0) {

			CERR("ERROR: instead of -lexicon-idx use -lexicon-idx-[lvcsr|phn]");
			EXIT();
			
		} else if (strcmp(argv[j], "-lexicon-idx-lvcsr") == 0) {
			
			j++;
			DBG_FORCE("argv[j]:"<<argv[j]);
			p_lexicon_lvcsr = argv[j];
			DBG("Loading lexicon " << p_lexicon_lvcsr << "...");
			lexiconLvcsr = new Lexicon(Lexicon::readonly);
			if (lexiconLvcsr->loadFromFile_readonly(p_lexicon_lvcsr) != 0) {
				CERR("ERROR: Lexicon input file " << p_lexicon_lvcsr << " not found");
				EXIT();
			}
			normLexicon_lvcsr.SetWordLexicon(lexiconLvcsr);
			DBG("done");
			
		} else if (strcmp(argv[j], "-lexicon-idx-phn") == 0) {
			
			j++;
			p_lexicon_phn = argv[j];
			DBG("Loading lexicon " << p_lexicon_phn << "...");
			lexiconPhn = new Lexicon(Lexicon::readonly);
			if (lexiconPhn->loadFromFile_readonly(p_lexicon_phn) != 0) {
				CERR("ERROR: Lexicon input file " << p_lexicon_phn << " not found");
				EXIT();
			}
			normLexicon_phn.SetWordLexicon(lexiconPhn);
			DBG("done");
			
		} else if (strcmp(argv[j], "-search-idx") == 0) {

			CERR("ERROR: instead of -search-idx use -search-idx-[lvcsr|phn]");
			EXIT();
			
		} else if (strcmp(argv[j], "-search-idx-lvcsr") == 0) {

			j++;
			p_searchindex_lvcsr = argv[j];

		} else if (strcmp(argv[j], "-search-idx-phn") == 0) {

			j++;
			p_searchindex_phn = argv[j];
			
		} else if (strcmp(argv[j], "-meetings-idx") == 0) {

			CERR("ERROR: instead of -meeting-idx use -meeting-idx-[lvcsr|phn]");
			EXIT();
			
		} else if (strcmp(argv[j], "-meetings-idx-lvcsr") == 0) {

			j++;
			p_meeting = argv[j];
			DBG("Loading meeting indices " << p_meeting << "...");
			if (indexerLvcsr == NULL)
			{
				indexerLvcsr = new Lattice::Indexer;
			}
			if (indexerLvcsr->meetings.loadFromFile(p_meeting) != 0) {
				CERR("ERROR: Meetings indexer input file " << p_meeting << " not found ... using empty indexer");
				EXIT();
			}
			DBG("done");
		
		} else if (strcmp(argv[j], "-meetings-idx-phn") == 0) {

			j++;
			p_meeting = argv[j];
			DBG("Loading meeting indices " << p_meeting << "...");
			if (indexerPhn == NULL)
			{
				indexerPhn = new Lattice::Indexer;
			}
			if (indexerPhn->meetings.loadFromFile(p_meeting) != 0) {
				CERR("ERROR: Meetings indexer input file " << p_meeting << " not found ... using empty indexer");
				EXIT();
			}
			DBG("done");
		


			
		} else if (strcmp(argv[j], "-phoneme-string") == 0) {

			j++;
			p_phoneme_string = argv[j];
			
		} else if (strcmp(argv[j], "-q") == 0) {

			j++;
			p_query = argv[j];			
			DBG_FORCE("p_query: "<<p_query);
			
		} else if (strcmp(argv[j], "-hyps-per-stream") == 0) {

			j++;
			p_hyps_per_stream = atoi(argv[j]);

		} else if (strcmp(argv[j], "-clusters-count") == 0) {
			
			j++;
			p_clusters_count = atoi(argv[j]);

		} else if (strcmp(argv[j], "-time-delta") == 0) {

			j++;
			p_time_delta = atof(argv[j]);

		} else if (strcmp(argv[j], "-fwd-nodes-count") == 0) {

			j++;
			p_fwd_nodes_count = atoi(argv[j]);

		} else if (strcmp(argv[j], "-bck-nodes-count") == 0) {

			j++;
			p_bck_nodes_count = atoi(argv[j]);
			
		} else if (strcmp(argv[j], "-data-dir-path") == 0) {
			CERR("ERROR: -data-dir-path switch does not exist. Use -data-dir-path-phn or -data-dir-path-lvcsr instead.");
			EXIT();
		
		} else if (strcmp(argv[j], "-data-dir-path-phn") == 0) {
			j++;
			p_data_dir_path_phn = argv[j];
			if (p_data_dir_path_phn[p_data_dir_path_phn.length()-1] == PATH_SLASH)
			{
				p_data_dir_path_phn.erase(p_data_dir_path_phn.length()-1); // remove the last char (slash)
			}
		
		} else if (strcmp(argv[j], "-data-dir-path-lvcsr") == 0) {
			j++;
			p_data_dir_path_lvcsr = argv[j];
			if (p_data_dir_path_lvcsr[p_data_dir_path_lvcsr.length()-1] == PATH_SLASH)
			{
				p_data_dir_path_lvcsr.erase(p_data_dir_path_lvcsr.length()-1); // remove the last char (slash)
			}
			
		} else if (strcmp(argv[j], "-show-all-matched-keywords") == 0) {
			
			p_show_all_matched_keywords = true;
			
		} else if (strcmp(argv[j], "-show-all-clusters") == 0) {

			p_show_all_clusters = true;
			
		} else if (strcmp(argv[j], "-show-reverse-index-lookup-results") == 0) {

			p_show_reverse_index_lookup_results = true;
			
		} else if (strcmp(argv[j], "-time-index-print") == 0) {
			
			p_time_index_print = true;
			j++;
			LatBinFile latBinFile_tmp;
			latBinFile_tmp.print_TimeIndex(argv[j]);

		} else if (strcmp(argv[j], "-dbg") == 0) {
			
			p_dbg = true;
			
		} else if (strcmp(argv[j], "-phrase-kwd-neighborhood") == 0) {
			
			j++;
			p_phrase_kwd_neiborhood = atof(argv[j]);
				
		} else if (strcmp(argv[j], "-meeting-list") == 0) {

			j++;
			p_meeting_list = argv[j];

		} else if (strcmp(argv[j], "-word-clusters") == 0) {

			p_word_clusters = true;

		} else if (strcmp(argv[j], "-max-results") == 0) {

			j++;
			p_max_results = atoi(argv[j]);
			
		} else if (strcmp(argv[j], "-port") == 0) {

			j++;
			p_port = atoi(argv[j]);

		} else if (strcmp(argv[j], "-only-print-candidates") == 0) {

			p_only_print_candidates = true;
	
		} else if (strcmp(argv[j], "-only-print-candidate-trigrams") == 0) {

			p_only_print_candidate_trigrams = true;
	
		} else if (strcmp(argv[j], "-xml-output") == 0) {

			p_results_output_type = ResultsETypeXml;
	
		} else if (strcmp(argv[j], "-results-output-type") == 0) {

			j++;
			if (strcmp(argv[j], "normal") == 0)
			{
				p_results_output_type = ResultsETypeNormal;
			}
			else if (strcmp(argv[j], "xml") == 0)
			{
				p_results_output_type = ResultsETypeXml;
			}
			else if (strcmp(argv[j], "xml_std") == 0)
			{
				p_results_output_type = ResultsETypeXml_std;
			}
			else if (strcmp(argv[j], "exp") == 0)
			{
				p_results_output_type = ResultsETypeExp;
			}
			else if (strcmp(argv[j], "xml_transcripts") == 0)
			{
				p_results_output_type = ResultsETypeXmlTranscripts;
			}
	
		} else if (strcmp(argv[j], "-record-delim") == 0) {

			j++;
			p_record_delim = argv[j][0];

		} else if (strcmp(argv[j], "-record-cutoff-column") == 0) {

			j++;
			p_record_cutoff_column = atoi(argv[j]);

		} else if (strcmp(argv[j], "-record-channel-column") == 0) {

			j++;
			p_record_channel_column = atoi(argv[j]);

		} else if (strcmp(argv[j], "-normalization-lexicon-lvcsr") == 0) {

			j++;
			p_normalization_lexicon_lvcsr = argv[j];
			normLexicon_lvcsr.LoadFromNormLexiconFile(p_normalization_lexicon_lvcsr);

		} else if (strcmp(argv[j], "-normalization-lexicon-phn") == 0) {

			j++;
			p_normalization_lexicon_phn = argv[j];
			normLexicon_phn.LoadFromNormLexiconFile(p_normalization_lexicon_phn);

		} else if (strcmp(argv[j], "-hard-decision-treshold") == 0) {

			j++;
			p_hard_decision_treshold = atof(argv[j]);
/*
		} else if (strcmp(argv[j], "-g2p-dictionary") == 0) {

			j++;
			p_g2p_dictionary = argv[j];
*/
		} else if (strcmp(argv[j], "-g2p-lexicon") == 0) {

			j++;
			p_g2p_lexicon = argv[j];

		} else if (strcmp(argv[j], "-g2p-rules") == 0) {

			j++;
			p_g2p_rules = argv[j];

		} else if (strcmp(argv[j], "-g2p-symbols") == 0) {

			j++;
			p_g2p_symbols = argv[j];

		} else if (strcmp(argv[j], "-g2p-max-variants") == 0) {

			j++;
			p_g2p_max_variants = atoi(argv[j]);

		} else if (strcmp(argv[j], "-g2p-scale-prob") == 0) {

			j++;
			p_g2p_scale_prob = true;

		} else if (strcmp(argv[j], "-g2p-prob-tresh") == 0) {

			j++;
			p_g2p_prob_tresh = atof(argv[j]);
/*
		} else if (strcmp(argv[j], "-wipen") == 0) {

			j++;
			p_wipen = atof(argv[j]);

		} else if (strcmp(argv[j], "-pipen") == 0) {

			j++;
			p_pipen = atof(argv[j]);
*/
		} else if (strcmp(argv[j], "-phn-query-multiword-to-singleword") == 0) {

			p_phn_query_multiword_to_singleword = true;

		} else if (strcmp(argv[j], "-phn-query-multiword") == 0) {

			p_phn_query_multiword_to_singleword = false;

		} else if (strcmp(argv[j], "-dont-verify-candidates") == 0) {

			p_verify_candidates = false;

		} else if (strcmp(argv[j], "-results-convert-to-local-time") == 0) {

			p_results_convert_to_local_time = true;

		} else if (strcmp(argv[j], "-lat-filename-start-time-field") == 0) {

			j++;
			p_lat_filename_start_time_field = atoi(argv[j]);

		} else if (strcmp(argv[j], "-latname-time-multiplier") == 0) {

			j++;
			p_latname_time_multiplier = atof(argv[j]);

		} else if (strcmp(argv[j], "-results-start-time-subtractor") == 0) {

			j++;
			p_results_start_time_subtractor = atof(argv[j]);

		} else {
			// Unknown switch detection
			if (argv[j][0] == '-') {
				CERR("Unknown switch: " << argv[j]);
				EXIT();
			}
		}
		
		j++;
	}

	DBG_FORCE("g2p->Load("<<p_g2p_rules<<", "<<p_g2p_symbols<<", "<<p_g2p_lexicon<<", "<<p_g2p_max_variants<<", "<<p_g2p_scale_prob<<", "<<p_g2p_prob_tresh<<")");
	if (p_g2p_lexicon != "")
	{
		g2p = new G2P;
		g2p->Load(p_g2p_rules, p_g2p_symbols, p_g2p_lexicon, p_g2p_max_variants, p_g2p_scale_prob, p_g2p_prob_tresh);
	}
/*
	if (p_g2p_dictionary != "")
	{
		dct = new Dct(lexiconLvcsr);
		DctFile dct_file(p_g2p_dictionary);
		DctFileRec dct_rec;
		while(dct_file.Next(&dct_rec))
		{
			dct.Add(dct_rec);
		}
	}
*/
}

LatMeetingIndexer* SearchConfig::GetMeetingIndexer()
{
	if (indexerLvcsr)
	{
		return &(indexerLvcsr->meetings);
	}
	else if (indexerPhn)
	{
		return &(indexerPhn->meetings);
	}
	else
	{
		CERR("ERROR: No indexer is set");
		EXIT();
	}
}

