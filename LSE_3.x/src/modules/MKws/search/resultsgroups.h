#ifndef RESULTSGROUPS_H
#define RESULTSGROUPS_H

#include <string>
#include <iostream>
#include <map>
#include <vector>
#include "hypothesis.h"
#include "querykwdlist.h"
#include "normlexicon.h"

namespace lse {

class RecordStream;

typedef std::vector< lse::Hypothesis > ResultsBaseType;

//class Results : public std::map< RecordStream, std::vector< lse::Hypothesis > > // map (record+stream -> hypothesis list)
class Results : public ResultsBaseType // map (record+stream -> hypothesis list)
{
		int mMaxResultsCount;
		QueryKwdList *mpQueryKwdList;

		void joinOverlappingResults();
		void sortResults();
		
	public:

		int mOovCount;

		enum EType {
			ETypeNormal,	// text format
			ETypeXml,		// XML format for MBrowser
			ETypeXml_std	// XML format for STD evals
		};

		Results() : 
			ResultsBaseType(), 
			mMaxResultsCount(-1),
			mOovCount(0)
		{};
		
		class ResultsGroup : public std::vector < Results::iterator > {
			public:
				std::string record;
				ResultsGroup(std::string rec = "") : record(rec) {};
		};
		typedef std::vector < ResultsGroup > ResultsGroups;
		
		void SetQueryKwdList(QueryKwdList *pQueryKwdList);
		void push_back(lse::Hypothesis hyp);
		void postProcess(NormLexicon *pNormLexicon);
		void print(std::ostream *os = &std::cout, EType type=ETypeNormal, float searchTime = 0.0);
		void SetMaxHypothesesCount(int max);
		void NormalizeResults(NormLexicon *pNormLexicon);
		
	private:	
		ResultsGroups resultsGroups;
	public:		
		Results::ResultsGroups::iterator findResultsGroup(Results::iterator iRes);
//		friend std::ostream& operator<<(std::ostream& os, const Results& res);
};


class RecordStream {

	public:	
		std::string record;
		std::string stream;

		RecordStream(std::string record, std::string stream) {
			this->record = record;
			this->stream = stream;
		}
		
		friend bool operator<(const RecordStream& l, const RecordStream& r) {
			if (l.record == r.record) {
				return l.stream < r.stream;
			} else {
				return l.record < r.record;
			}
		}
};

} // namespace lse

#endif
