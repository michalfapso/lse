#ifndef HYPOTHESIS_H
#define HYPOTHESIS_H

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <iomanip>
#include "lattypes.h"
#include "latindexer.h"
#include "global.h"


namespace lse {


/// list of keywords and list of context words
class Hypothesis {
		
public:

	class Word {
	public:
		std::string str;
		LatTime start;
		LatTime end;
		Confidence conf;		
		std::string pron; ///< pronounciation variant
		
		friend std::ostream& operator<<(std::ostream& os, const Hypothesis::Word& w) {
			return os << w.str << "\tSTART " << w.start << "\tEND " << w.end;// << "\tCONFIDENCE " << w.conf;
		}

	};


	std::string stream;
	ID documentId;
//	std::string record;
	std::string channel;
	bool decision;
	float score;
	
	typedef std::list<Hypothesis::Word> Keywords;
	typedef std::list<Hypothesis::Word> Words;

	Words words;
	Keywords keywords;
	int keywordsCount;


	Hypothesis() 
	{
		stream = "";
		documentId = ID_INVALID;
		//record = "";
		channel = "";
		decision = false;
		score = 0.0;
		keywordsCount = 0;
	}
	
	// copy constructor:
	Hypothesis(const Hypothesis &h)
	{
		stream = h.stream;
		//record = h.record;
		documentId = h.documentId;
		channel = h.channel;
		decision = h.decision;
		score = h.score;
		keywordsCount = h.keywordsCount;
		for(Keywords::const_iterator i=h.keywords.begin(); i!=h.keywords.end(); ++i) {
			keywords.push_back(*i);
		}
		for(Words::const_iterator i=h.words.begin(); i!=h.words.end(); ++i) {
			words.push_back(*i);
		}
	}

	// ostream operator
	friend std::ostream& operator<<(std::ostream& os, const Hypothesis& hyp) {
		os << "stream:" << hyp.stream
			<< " documentId:" << hyp.documentId
			<< " channel:" << hyp.channel
			<< " decision:" << hyp.decision
			<< " score:" << hyp.score;

		os << " keywords(" << hyp.keywords.size() << "):";
		for (Keywords::const_iterator i=hyp.keywords.begin(); i!=hyp.keywords.end(); ++i) {
			os << *i << " ";
		}
		os << " words(" << hyp.words.size() << "):";
		for (Words::const_iterator i=hyp.words.begin(); i!=hyp.words.end(); ++i) {
			os << *i << " ";
		}
		
		return os;
	}

	void printXml(std::ostream *os);
	void printText(std::ostream *os);

	// comparator
	friend bool operator<(const Hypothesis& l, const Hypothesis& r);
	
	void push_back_word(std::string str, LatTime start, LatTime end, Confidence conf);
	void push_back_word(Hypothesis::Word w); 
	
	void push_front_word(std::string str, LatTime start, LatTime end, Confidence conf);
	void push_front_word(Hypothesis::Word w);
	
	void addKeyword(std::string str, LatTime start, LatTime end, Confidence conf);
	void addKeyword(Hypothesis::Word w);
	void addKeywordBack(Hypothesis::Word w);
	void addKeywordFront(Hypothesis::Word w);
	
	void updateKeywordCounter(std::string str);
		
	void setFrontWordStartTime(LatTime t);

};

} // namespace lse

#endif

