/*********************************************************************************
 *
 *	Application for searching in binary lattices filesystem
 *
 *
 *
 *
 *
 */



#include <iostream>
#include <fstream>
#include <string>
#include "lat.h"
#include "lattypes.h"
#include "strindexer.h"
#include "latindexer.h"
#include "indexer.h"
#include "latbinfile.h"
#include "hypothesis.h"
#include "querykwdlist.h"
#include "lexicon.h"
#include "wordclusters.h"
#include "timer.h"
#include "common.h"
#include "results.h"
#include "search.h"
#include "g2p.h"
#include "normlexicon.h"
#include "searchconfig.h"


// server include files
#ifndef _WIN32
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <pthread.h>
#endif

using namespace std;
using namespace lse;


#define DBG_MAIN(str) if (config->p_dbg) cerr << __FILE__ << ':' << setw(5) << __LINE__ << "> " << str << endl << flush;

#define TIMER_START	dbgTimer.start();
#define TIMER_END(msg)	dbgTimer.end();	DBG_FORCE("TIMER["<<msg<<"]: "<<dbgTimer.val());

// sockets imeplementation constants
static const int BUFFER = 2048;
static const int QUEUE = 5; 	// length of the queue of waiting connections

// files extensions
static const string ext_dot = ".dot";
static const string ext_bin = ".bin";
static const string ext_copy = ".lat.copy";
static const string ext_bingz = ".bin.gz";
static const string ext_htk = ".lat";

Timer dbgTimer;
double cumulTime_load;
double cumulTime_load_time;
double cumulTime_load_nodes;
double cumulTime_load_nodelinks;
double cumulTime_getContext;

//application parameters/*{{{*/
/*
string p_lexicon_lvcsr				= "";
string p_lexicon_phn				= "";
string p_meeting 					= "";
string p_searchindex_lvcsr			= "";
string p_searchindex_phn			= "";
string p_phoneme_string 			= "";
string p_query 						= "";
string p_data_dir_path_phn			= "";
string p_data_dir_path_lvcsr		= "";
unsigned int p_hyps_per_stream 		= 2; // set default value
int p_clusters_count 				= -1; // undefined
float p_time_delta 					= 0;
int p_fwd_nodes_count 				= 5;
int p_bck_nodes_count 				= 5;
bool p_show_all_matched_keywords 	= false;
bool p_show_all_clusters 			= false;
bool p_show_reverse_index_lookup_results = false;
bool p_time_index_print				= false;
bool p_dbg 							= false;
string p_meeting_list 				= "";
float p_phrase_kwd_neiborhood 		= 0.0;
bool p_word_clusters 				= false;
int p_max_results 					= 10;
int p_port 							= 0;
bool p_only_print_candidates 		= false;


// g2p
string p_g2p_lexicon				= "";
string p_g2p_rules					= "";
string p_g2p_symbols				= "";
int p_g2p_max_variants				= -1;
bool p_g2p_scale_prob				= false;
float p_g2p_prob_tresh				= -1.0f;


bool p_only_print_candidate_trigrams= false;
Results::EType p_results_output_type= Results::ETypeNormal;
char p_record_delim					= '_';
int p_record_cutoff_column			= 0;
int p_record_channel_column			= 0;
string p_normalization_lexicon_lvcsr= "";
string p_normalization_lexicon_phn	= "";
float p_hard_decision_treshold		= 0.0;
*/
/*}}}*/
// structure used for passing arguments to the service (thread) function
class ServiceParams {
public:
	int sd;
	SearchConfig *config;
};

/*
enum SearchSource {
	lattice,
	best_path,
	title,
	summary,
	kws		
};
*/



/* forward declarations {{{ */
void searchKeyword(
		SearchConfig *config,
		QueryKwdList *queryKwdList, 
		Results *pResults
	);
bool isKwdSeparator(char c);
LatTime str2LatTime(std::string str);
bool isANY(string kwd);
//void processANYkwd(Lexicon *lexicon, Lattice::Indexer *indexer, SearchSource src, QueryKwd kwd, MeetingsList *validMeetings);
int parseQuery(
		const string p_query, 
		SearchConfig *config,
		QueryKwdList *queryKwdList, 
		bool *processANY
	);

void processQuery(
		const string p_query, 
		SearchConfig *config,
		ostringstream *output
	);
/*
bool isValidPath(QueryKwd::RevIdxRecord *pRevIdxRecord, 
				 QueryKwdList::iterator iKwdCur, 
				 QueryKwdList* kwdList);
*/
/* }}} */

#ifndef _WIN32
void * service(void *arg);
#endif

//--------------------------------------------------------------------------------

/* isQueryVar {{{ */
bool isQueryVar(string q, int kwdStartPos, const string &identifier) 
{
	return q.substr(kwdStartPos, identifier.length()) == identifier;
}
/* }}} */

/* getQueryVarValue {{{ */
const string getQueryVarValue(string q, int kwdStartPos, int kwdSpacePos, const string identifier) 
{
	return q.substr(kwdStartPos + identifier.length(), kwdSpacePos - kwdStartPos - identifier.length());
}
/* }}} */

/* main() {{{ */
int main(int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Here should be some HELP (TODO!!!)";
		cerr << "Hello" << endl;
		return 1;
	}

	// print arguments
	for (int i=0; i<argc; i++)
	{
		DBG_FORCE("APPLICATION ARGUMENTS: "<<argv[i]);
	}

/*
	Lexicon *lexiconLvcsr = NULL; // stores records of type: wordID -> word  and  word -> wordID
	Lexicon *lexiconPhn = NULL; // stores records of type: wordID -> word  and  word -> wordID
	Lattice::Indexer *indexerLvcsr = NULL;
	Lattice::Indexer *indexerPhn = NULL;

	G2P g2p;
*/
	SearchConfig* config = new SearchConfig;
/*	
	config->lexiconLvcsr = lexiconLvcsr;
	config->lexiconPhn = lexiconLvcsr;
	config->indexerLvcsr = indexerLvcsr;
	config->indexerPhn = indexerPhn;
	config->g2p = g2p;
*/
	config->ParseCommandLineArguments(argc, argv);

/*	Lattice globalLat(lexicon, indexer); // concatenation of lattices given by application arguments
	globalLat.N = 1;
	globalLat.L = 0;
	LatticeNode firstNode;
	firstNode.id=0;
	firstNode.t=0.0;
	firstNode.W="!NULL";
	firstNode.v=0;
	globalLat.addNode(firstNode); // add empty node
*/
		


/*
	// get list of transcriptions - pronounciation variants
	list<gpt::PhnTrans::pt_entry> pron_list;
	DBG_FORCE("g2p->GetTranscs(FAPSO)");
	g2p.GetTranscs("fapso", &pron_list);
	DBG_FORCE("pron_list.size():"<<pron_list.size());
	for (list<gpt::PhnTrans::pt_entry>::iterator iPron=pron_list.begin(); iPron!=pron_list.end(); ++iPron)
	{
		DBG_FORCE("PRON: "<<iPron->trans);
	}
	exit(1);
*/
	
/*
	validMeetings is no more a global variable because of the use of threads
	   
	if (p_meeting_list != "") {
		LatMeetingIndexerRecord meetingRec;
		ifstream in(p_meeting_list.c_str());
		while (!in.eof()) {
			getline(in, meetingRec.path);
			validMeetings.insert(make_pair(indexer.meetings.getValID(&meetingRec), 0));
		}
	}
*/

	if (config->p_query != "") {
	//--------------------------------------------------------------------------------
	//                                 STANDALONE
	//--------------------------------------------------------------------------------
//		cout << "STANDALONE" << endl << flush;
		ostringstream output;
		processQuery(config->p_query, config, &output);
		cout << output.str();

	} 
#ifndef _WIN32	
	else {
	//--------------------------------------------------------------------------------
	//                                 SERVER
	//--------------------------------------------------------------------------------
		struct   protoent  *ptrp;     /* pointer to a protocol table entry */
		struct   sockaddr_in sad;     /* structure to hold server's address */
		struct   sockaddr_in cad;     /* structure to hold client's address */
		int      sd;                  /* socket descriptors */
		int      alen;                /* length of address */
		ServiceParams serviceParams;  /* arguments for service function */
		pthread_t  tid;               /* variable to hold thread ID */

		memset((char  *)&sad,0,sizeof(sad)); /* clear sockaddr structure   */
		sad.sin_family = AF_INET;            /* set family to Internet     */
		sad.sin_addr.s_addr = INADDR_ANY;    /* set the local IP address */

		if (config->p_port > 0)                          /* test for illegal value    */
			sad.sin_port = htons((u_short)(config->p_port));
		else {                                /* print error message and exit */
			CERR("bad port number " << config->p_port);
			exit (1);
		}

		/* Map TCP transport protocol name to protocol number */

		if ( ((int)(ptrp = getprotobyname("tcp"))) == 0)  {
			CERR("cannot map \"tcp\" to protocol number");
			exit (1);
		}

		/* Create a socket */
		sd = socket (PF_INET, SOCK_STREAM, ptrp->p_proto);
		if (sd < 0) {
			CERR("socket creation failed");
			exit(1);
		}

		/* Bind a local address to the socket */
		if (bind(sd, (struct sockaddr *)&sad, sizeof (sad)) < 0) {
			CERR("bind failed");
			exit(1);
		}

		/* Specify a size of request queue */
		if (listen(sd, QUEUE) < 0) {
			CERR("listen failed");
			exit(1);
		}

		alen = sizeof(cad);

		/* Main server loop - accept and handle requests */
		CERR("Server up and running.");
		serviceParams.config = config;
		while (1) 
		{
			cout << "SERVER: Waiting for contact ..." << endl;

			if (  (serviceParams.sd = accept(sd, (struct sockaddr *)&cad, (socklen_t*)&alen)) < 0) 
			{
				CERR("accept failed");
				exit (1);
			}
			DBG_FORCE("creating thread");
			pthread_create(&tid, NULL, service, (void *) &serviceParams );
			DBG_FORCE("thread created");
		}
		close(sd);

	} // SERVER or STANDALONE
#endif
	delete config;

	return 0;
}
/* }}} */

#ifndef _WIN32
/* Server network communication routines {{{ */
//--------------------------------------------------------------------------------
//	sendall
//--------------------------------------------------------------------------------
/**
  @brief Send all data from buffer buf to socket s

  @param s socke+t
  @param *buf buffer
  @param *len length of buffer

  @return -1 on failure, 0 on success
*/
int sendall(int s, const char *buf, int *len)
{
	int total = 0;        // how many bytes we've sent
	int bytesleft = *len; // how many we have left to send
	int n = 0;

	while(total < *len) {
		n = send(s, buf+total, bytesleft, 0);
		if (n == -1) { break; }
		total += n;
		bytesleft -= n;
	}

	*len = total; // return number actually sent here

	return n==-1?-1:0; // return -1 on failure, 0 on success
} 


//--------------------------------------------------------------------------------
//	service
//--------------------------------------------------------------------------------
/**
  @brief function running in separate thread(s) to serve clients

  @param *arg structure for passing arguments to thread function
*/
void * service(void *arg)
{
	ServiceParams *serviceParams;
	int n, r;
	int sd;
	char buf[BUFFER];

	serviceParams = (ServiceParams *) arg;
	sd = serviceParams->sd;	// we have to store the socket file descriptor, because the next service() thread will rewrite this value

	if ((n = read(sd, buf, BUFFER)) > 0) {

		buf[n] = 0;			// put the string-terminator at the end of received string		
		chomp(buf,n);		// remove newline character(s) from the end of the string
		cout << "incoming query: " << buf << endl;
		
		// query -> search results (text output)
		ostringstream output;
		processQuery(buf, serviceParams->config, &output);
		
		r = output.str().length();
		if (sendall(sd, output.str().c_str(), &r) == -1) 
		{
			CERR("write(): Buffer written just partially");
		} else {
			CERR("client served");
		}
	}
	if (n == -1)
		DBG("read()");

	close(sd);
	pthread_exit(0);

/*
   
	return (0);
*/
}
/* }}} */
#endif

//--------------------------------------------------------------------------------
//	processQuery
//--------------------------------------------------------------------------------
/**
  @brief process the given query (parse it and generate search results)

  @param p_query given query
  @param *lexicon pointer to lexicon
  @param *indexer pointer to indexer
  @param *output pointer to output stream
*/
/* processQuery {{{ */
void processQuery(
		const string p_query, 
		SearchConfig *config,
		ostringstream *output) 
{
//	QueryKwdList queryKwdList(lexicon);
	QueryKwdList queryKwdList;
	//SearchSource searchSource = lattice; // let's search in lattice as default 
	bool processANY;
	Timer search_timer;
	search_timer.start();

	queryKwdList.hyps_per_stream = config->p_hyps_per_stream;
	queryKwdList.time_delta = config->p_time_delta;
	queryKwdList.fwd_nodes_count = config->p_fwd_nodes_count;
	queryKwdList.bck_nodes_count = config->p_bck_nodes_count;
	queryKwdList.phrase_kwd_neighborhood = config->p_phrase_kwd_neiborhood;
	queryKwdList.max_results = config->p_max_results;

	parseQuery(p_query, config, &queryKwdList, &processANY);

	if (processANY) {
		CERR("ERROR: _any_ keywords are not supported");
		exit(1);
//		processANYkwd(lexicon, indexer, searchSource, *(queryKwdList.begin()), &validMeetings);
	} else {
		Results results; // map (record+stream -> hypothesis list)
		results.SetQueryKwdList(&queryKwdList);
		results.SetMaxHypothesesCount(queryKwdList.max_results);
		results.SetHardDecisionTreshold(config->p_hard_decision_treshold);

		for (MeetingsList::iterator i=config->mValidMeetings.begin(); i!=config->mValidMeetings.end(); ++i) {
			DBG_FORCE("MeetingsList: " << i->first);
		}

		searchKeyword(config, &queryKwdList, &results);
		DBG_FORCE("before postprocessing ... results.size():"<<results.size());
//		results.postProcess();
		results.postProcess(&config->normLexicon_lvcsr, &config->normLexicon_phn);
		DBG_FORCE("after postprocessing ... results.size():"<<results.size());
		search_timer.end();
		results.print(output, config, search_timer.val());
	}
}
/* }}} */

//--------------------------------------------------------------------------------
//	parseQuery
//--------------------------------------------------------------------------------
/**
  @brief parse the given query and store it into queryKwdList structure

  @param p_query given query
  @param *config configuration parameters
  @param *searchSource 
  @param *processANY query consists of '_any_' keyword

  @return zero on success
*/
/* parseQuery {{{ */
int parseQuery(
		const string p_query, 
		SearchConfig *config,
		QueryKwdList *queryKwdList, 
		bool *processANY) 
{
	DBG_FORCE("Searching for '"<<p_query<<"'");
	if (p_query != "") {
		float q_nb = -1;
		string::size_type kwdStartPos = 0;
		string::size_type kwdSpacePos = 0;
		QueryKwd kwd;
		QueryKwd *pKwdTimeSet = NULL;
		bool firstPhraseKwd = false;
		bool lastPhraseKwd = false;

		int state = 0;
		bool epsilon = false; // epsilon transition (without moving to the next word)

		while (kwdStartPos < p_query.length()) {
			DBG("kwdStartPos:"<<kwdStartPos<<" p_query.length():"<<p_query.length());
			if (!epsilon) {
				for (;isKwdSeparator(p_query[kwdStartPos]); kwdStartPos++) {} // jump over white space
				
				if ((kwdSpacePos = p_query.find(' ', kwdStartPos)) == string::npos) { // find end of current keyword
					kwdSpacePos = p_query.length();
				}
				
				kwd.W = p_query.substr(kwdStartPos, kwdSpacePos - kwdStartPos);
				kwd.W = upcase(kwd.W);
				kwd.W = nonAsciiToOctal(kwd.W);
				DBG_MAIN("kwd.W:"<<kwd.W);
				kwd.greaterThan = false;
				kwd.lessThan = false;
				kwd.confidence_min_isset = false;
				kwd.confidence_max_isset = false;
				kwd.time_min_type = null;
				kwd.time_max_type = null;
				kwd.nb_prev = q_nb;
				kwd.nb_next = q_nb;
				kwd.isNextInPhrase = false;
			} else {
				epsilon = false; // reset epsilon
			}

			DBG("switch ("<<state<<")");
			switch (state) {

				// beginning of the keyword
				case 0:
					
					// c(...) < x, c(...) > y, ...
					if (kwd.W.substr(0, 2) == "c(") {
						
						DBG("  trimming");
						kwd.W.erase(0,2);					// trim "c(" from beginning 
						kwd.W.erase(kwd.W.length()-1,1); 	// and ")" from the end
						DBG("  trimmed kwd: "<<kwd.W);
						// if the word is in the word list, load it's parameters. Otherwise add it to the word list
						QueryKwdList::iterator iKwd;
						if ((iKwd = queryKwdList->find(kwd.W)) != queryKwdList->end()) {
							kwd = *iKwd;
						} else {
							queryKwdList->push_back(kwd);
						}
						state = 1;
						
					} else

					// phrase: "KWD1 KWD2"
					if (p_query[kwdStartPos] == '"') {
						DBG_MAIN("  Phrase start found");
						kwd.W.erase(0,1); 					// erase " from kwd
						kwdStartPos ++; 					// jump over "
						state = 2;
						firstPhraseKwd = true;
						epsilon = true; 					// we need to know whether this is the last word in phrase (ending with ")
						
					} else

					// neighborhood: nb=<float>
					if (isQueryVar(p_query, kwdStartPos, "nb=")) 
					{						
						q_nb = atof(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "nb=").c_str());
						DBG_MAIN("  q_nb.float = "<<q_nb);

					} else

					// -hyps-per-stream
					if (isQueryVar(p_query, kwdStartPos, "&HPS=")) 
					{
						queryKwdList->hyps_per_stream = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&HPS=").c_str());
						DBG_MAIN("  hyps_per_stream = "<<queryKwdList->hyps_per_stream);

					} else

					// -time-delta
					if (isQueryVar(p_query, kwdStartPos, "&TD=")) 
					{
						queryKwdList->time_delta = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&TD=").c_str());
						DBG_MAIN("  time_delta = "<<queryKwdList->time_delta);

					} else
						
					// -fwd-nodes-count
					if (isQueryVar(p_query, kwdStartPos, "&FNC=")) 
					{
						queryKwdList->fwd_nodes_count = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&FNC=").c_str());
						DBG_MAIN("  fwd_nodes_count = "<<queryKwdList->fwd_nodes_count);

					} else

					// -bck-nodes-count
					if (isQueryVar(p_query, kwdStartPos, "&BNC=")) 
					{
						queryKwdList->bck_nodes_count = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&BNC=").c_str());
						DBG_MAIN("  bck_nodes_count = "<<queryKwdList->bck_nodes_count);

					} else
						
					// -phrase-kwd-neighborhood
					if (isQueryVar(p_query, kwdStartPos, "&PNB=")) 
					{
						queryKwdList->phrase_kwd_neighborhood = atof(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&PNB=").c_str());
						DBG_MAIN("  phrase_kwd_neighborhood = "<<queryKwdList->phrase_kwd_neighborhood);

					} else

					// -max-results
					if (isQueryVar(p_query, kwdStartPos, "&RES=")) 
					{
						queryKwdList->max_results = atoi(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&RES=").c_str());
						DBG_MAIN("  max_results = "<<queryKwdList->max_results);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&SRC=")) 
					{
//						CERR("ERROR: specifying a source (&SRC=...) is not supported");
//						EXIT();
						LatMeetingIndexerRecord meetingRec;
						meetingRec.path = getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&RES=");
						config->mValidMeetings.insert(make_pair(config->indexerLvcsr->meetings.getRecID(meetingRec.path), 0));
						DBG_MAIN("  SRC = "<<meetingRec.path);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&EM=")) 
					{
						queryKwdList->mExactMatch = (getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&EM=") == "true" ? true : false);
						DBG_MAIN("  EM = "<<queryKwdList->mExactMatch);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&OVERLAP")) 
					{
						queryKwdList->mOverlapping = true;
						DBG_MAIN("  OVERLAP = "<<queryKwdList->mOverlapping);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&EM")) 
					{
						queryKwdList->mExactMatch = true;
						DBG_MAIN("  EM = true");

					} else

					if (isQueryVar(p_query, kwdStartPos, "&NGRAM2PHN")) 
					{
						queryKwdList->mNgram2Phn = true;
						DBG_MAIN("  NGRAM2PHN = true");

					} else
						
					if (isQueryVar(p_query, kwdStartPos, "&PHN2NGRAM")) 
					{
						queryKwdList->mPhn2Ngram = true;
						DBG_MAIN("  PHN2NGRAM = true");

					} else
						
					if (isQueryVar(p_query, kwdStartPos, "&ALLSUCCESSFULTOKENS")) 
					{
						queryKwdList->mReturnAllSuccessfulTokens = true;
						DBG_MAIN("  ALLSUCCESSFULTOKENS = true");

					} else
						
					if (isQueryVar(p_query, kwdStartPos, "&ID=")) 
					{
						queryKwdList->mTermId = getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&ID=");
						DBG_MAIN("  mTermId = "<<queryKwdList->mTermId);

					} else

					if (isQueryVar(p_query, kwdStartPos, "&SEARCHSOURCETYPE=")) 
					{
						queryKwdList->mSearchSource = SearchSource::ParseString(getQueryVarValue(p_query, kwdStartPos, kwdSpacePos, "&SEARCHSOURCETYPE=").c_str());
						DBG_MAIN("  SEARCHSOURCE = "<<queryKwdList->mSearchSource);

					} else
						
					// words order operators
					if (kwd.W == "<") {
						(--queryKwdList->end())->lessThan = true;
					} else 
						
					if (kwd.W == ">") {
						(--queryKwdList->end())->greaterThan = true;
					} else
						
					// time operator
					if (kwd.W.substr(0, 2) == "t(") {
						
						DBG("  trimming");
						kwd.W.erase(0,2);					// trim "c(" from beginning 
						kwd.W.erase(kwd.W.length()-1,1); 	// and ")" from the end
						DBG("  trimmed kwd: "<<kwd.W);
						// if the word is in the word list, load it's parameters. Otherwise add it to the word list
						QueryKwdList::iterator iKwd;
						if ((iKwd = queryKwdList->find(kwd.W)) != queryKwdList->end()) {
							kwd = *iKwd;
							pKwdTimeSet = &(*iKwd);
						} else {
							queryKwdList->push_back(kwd);
							pKwdTimeSet = &(*queryKwdList->rbegin());
						}
						DBG_MAIN("setting time for kwd:"<<kwd);
						state = 5;
						
					} else 
					
					// other words
					{
						kwd.nb_prev = q_nb;
						kwd.nb_next = q_nb;
						queryKwdList->push_back(kwd);
						DBG("  normal keyword:"<<kwd.W);
					}
					break;

				case 1:

					// confidence operators
					if (kwd.W == "<") {
						state = 3;
					} else if (kwd.W == ">") {
						state = 4;
					}

					break;

				case 2:
					
					// within the phrase
					if (p_query[kwdSpacePos-1] == '"') {
						DBG_MAIN("  Phrase end found");
						kwd.W.erase(kwd.W.length()-1,1); // erase " from kwd
						kwdSpacePos --; // cut " off
						state = 0;
						lastPhraseKwd = true;
					}
					DBG_MAIN("  p_query[kwdSpacePos-1]:"<<p_query[kwdSpacePos-1]);

					kwd.nb_prev = firstPhraseKwd ? q_nb : queryKwdList->phrase_kwd_neighborhood;
					kwd.nb_next = lastPhraseKwd ? q_nb : queryKwdList->phrase_kwd_neighborhood;
					DBG_MAIN("  inPhrase...kwd:"<<kwd.W);
					
					if (firstPhraseKwd) {
						firstPhraseKwd = false;
					} else {
						if (queryKwdList->size() > 0)
							(--(queryKwdList->end())) -> isNextInPhrase = true;
					}
					if (lastPhraseKwd) lastPhraseKwd = false; 

					queryKwdList->push_back(kwd);
					
					break;
				
				case 3:
					// c(...) < x
					(--queryKwdList->end())->confidence_max = atof(kwd.W.c_str());
					(--queryKwdList->end())->confidence_max_isset = true;
					state = 0;
					break;

				case 4:
					// c(...) > x
					(--queryKwdList->end())->confidence_min = atof(kwd.W.c_str());
					(--queryKwdList->end())->confidence_min_isset = true;
					state = 0;
					break;
					
				case 5:
					// timing operators
					if (kwd.W == "<") {
						state = 6;
					} else if (kwd.W == ">") {
						state = 7;
					}
					break;

				case 6:
					// t(...) < x
					pKwdTimeSet->time_max = str2LatTime(kwd.W.c_str());
					if (pKwdTimeSet->time_max >= 0) {
						pKwdTimeSet->time_max_type = lse::time;
					} else {
						pKwdTimeSet->time_max_type = lse::percents;
						pKwdTimeSet->time_max *= -1; // convert it back to positive value
					}
					
					DBG_MAIN("time_max:"<<pKwdTimeSet->time_max);
					state = 0;
					break;

				case 7:
					// t(...) > x
					pKwdTimeSet->time_min = str2LatTime(kwd.W.c_str());
					if (pKwdTimeSet->time_min >= 0) {
						pKwdTimeSet->time_min_type = lse::time;
					} else {
						pKwdTimeSet->time_min_type = lse::percents;
						pKwdTimeSet->time_min *= -1; // convert it back to positive value
					}
					
					DBG_MAIN("time_min:"<<pKwdTimeSet->time_min);
					state = 0;
					break;
					

					
			} // switch (state)		
			if (!epsilon)
			{
				kwdStartPos = kwdSpacePos + 1;
			}
		} // while (kwdStartPos < p_query.length())

		*processANY = false; // are we going to process the keyword "_any_" or the ordinary query
		for (QueryKwdList::iterator kwd = queryKwdList->begin(); kwd != queryKwdList->end(); ++kwd) {
			if (isANY(kwd->W)) {
				if (queryKwdList->size() > 1) {
					CERR("The keyword '_any_' can be used only without other words");
					return 1;
				} else {
					*processANY = true;
					break;
				}
			}					
		}
			
	} 
	return 0;
}
/* }}} */

//--------------------------------------------------------------------------------
//	isValidPath
//--------------------------------------------------------------------------------
/**
  @brief check if words from query are bound (with the neighborhood operator)

  @param iCluster current cluster
  @param iKwdCur current keyword
  @param kwdList list of all keywords from query

  @return true if there are words in clusters which fits all bound keywords
*/
/* isValidPath {{{ */
/*
bool isValidPath(QueryKwd::RevIdxRecord *pRevIdxRecord, 
				 QueryKwdList::iterator iKwdCur, 
				 QueryKwdList* kwdList)
{

	// if we are at the last kwd in query 
	// or the current kwd is not binded with the next kwd from query, 
	// then this path is valid
	if (iKwdCur == kwdList->end() || iKwdCur->nb_next == -1) {
		return true;
	}
	
	// if current cluster did not set any following cluster, we are on the wrong path
	if (!pRevIdxRecord->mBestFwdKwdClusterFound) {
		return false;
	}	

	return isValidPath(pRevIdxRecord->mpBestFwdKwdCluster, ++iKwdCur, kwdList);
	
}
*/
/* }}} */

//--------------------------------------------------------------------------------
//	searchKeyword
//--------------------------------------------------------------------------------
/**
  @brief search for given parsed query using indexer and lexicon and write the results to output stream os

  @param *lexicon
  @param *indexer
  @param *queryKwdList parsed query
  @param *os output stream
*/
/* searchKeyword {{{ */
void searchKeyword(
		SearchConfig		*config,
		QueryKwdList 		*queryKwdList, 
		Results 			*pResults) 
{
	DBG("searchKeyword()");
	
	DBG_FORCE("queryKwdList.size():"<<queryKwdList->size());

	/* get wordID for keywords {{{ */
	for (QueryKwdList::iterator i = queryKwdList->begin(); i != queryKwdList->end(); ++i) 
	{
		DBG_FORCE("WORD: "<<i->W);
		// if the word is not in the lvcsr lexicon
		bool is_oov = true;
		if (config->lexiconLvcsr)
		{
			is_oov = (i->W_id = config->lexiconLvcsr->word2id_readonly(i->W, false)) < 0;
		}
		if ( is_oov ) {
			cerr << "Word " << i->W << " has not been found in LVCSR lexicon... trying to convert to phonemes" << endl;
			pResults->mOovCount++;
			i->W_id = -pResults->mOovCount; // assign a unique W_id for each OOV
		}
		if ((queryKwdList->mSearchSource == SearchSource::LVCSR_OOVPHN && is_oov) ||
			 queryKwdList->mSearchSource == SearchSource::LVCSR_PHN ||
			 queryKwdList->mSearchSource == SearchSource::PHN)
		{
			if (config->g2p != NULL)
			{
				// get list of transcriptions - pronounciation variants
				list<gpt::PhnTrans::pt_entry> pron_list;
				DBG_FORCE("g2p->GetTranscs("<<i->W<<")");
				config->g2p->GetTranscs(lowcase(i->W), &pron_list);
				//config->g2p->GetTranscs(i->W, &pron_list);
				DBG_FORCE("pron_list.size():"<<pron_list.size());
				for (list<gpt::PhnTrans::pt_entry>::iterator iPron=pron_list.begin(); iPron!=pron_list.end(); ++iPron)
				{
					DBG_FORCE("PRON: "<<iPron->trans);
				}
				// create phn and ngram QueryKwdList for the current keyword i
				i->SetPronList(&pron_list, queryKwdList, config->lexiconPhn); 
			}
		}
	}
	/* }}} */

	DBG_FORCE("queryKwdList before postprocessing:"); queryKwdList->print();
//	EXIT();
	/* convert multiword phoneme query to a single-word one {{{ */
	if (config->p_phn_query_multiword_to_singleword && queryKwdList->size() > 1)
	{
		vector<string> pron_list_str;

		QueryKwdList::iterator iKwdPrev = queryKwdList->begin();
		QueryKwdList::iterator iKwdCur = queryKwdList->begin(); iKwdCur++;
		// for the first keyword simply copy the pronounciation variants
		for (PronVariants::iterator iPronPrev = iKwdPrev->mPronList.begin(); 
			iPronPrev != iKwdPrev->mPronList.end();
			iPronPrev ++)
		{
			pron_list_str.push_back(trim_outer_spaces(iPronPrev->mPtEntry.trans));
		}
		// for all other keywords expand the pronounciation variants with "sil" and without "sil"
		while (iKwdCur != queryKwdList->end()) 
		{
			int pron_list_prev_size = pron_list_str.size();
			for (int iPronPrev = 0; iPronPrev < pron_list_prev_size; iPronPrev++)
			{
				for (PronVariants::iterator iPronCur = iKwdCur->mPronList.begin(); 
					iPronCur != iKwdCur->mPronList.end();
					iPronCur ++)
				{
					string new_pron;
					new_pron = pron_list_str[iPronPrev] + " " + trim_outer_spaces(iPronCur->mPtEntry.trans);
					pron_list_str.push_back(new_pron);

					new_pron = pron_list_str[iPronPrev] + " sil " + trim_outer_spaces(iPronCur->mPtEntry.trans);
					pron_list_str.push_back(new_pron);
				}
			}
			for (int iPronPrev = 0; iPronPrev < pron_list_prev_size; iPronPrev++)
			{
				pron_list_str.erase(pron_list_str.begin());
			}
			iKwdCur++;
		}
		list<gpt::PhnTrans::pt_entry> pron_list;
		for(vector<string>::iterator i=pron_list_str.begin(); i!=pron_list_str.end(); i++) 
		{ 
			DBG_FORCE("pron_list_str[]:"<<*i);	
			gpt::PhnTrans::pt_entry pron;
			pron.trans = *i;
			pron_list.push_back(pron);
		}

		string all_keywords = "";
		for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); iKwd++)
		{
			all_keywords += (all_keywords.length() > 0 ? " " : "") + iKwd->W;
		}
		QueryKwd kwd_all_keywords;

		QueryKwdList* p_query_singleword = new QueryKwdList;
		p_query_singleword->CopyParams(*queryKwdList); // copy query parameters
		p_query_singleword->push_back(kwd_all_keywords);
		p_query_singleword->begin()->SetPronList(&pron_list, p_query_singleword, config->lexiconPhn);
		p_query_singleword->print();
		queryKwdList = p_query_singleword;
	}
	/* }}} */

	/* allow only pronounciation variants longer then N phonemes (N=3) {{{ */
	for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); iKwd++)
	{
		bool changed = true;
		while (changed)
		{
			changed = false;
			for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
				iPron != iKwd->mPronList.end();
				iPron ++)
			{
				int phn_count = 0;
				for(string::size_type pos = 0; pos != string::npos; pos = iPron->mPtEntry.trans.find(" ", pos+1))
				{
					phn_count++;
				}
				if (phn_count < NGRAM_LENGTH)
				{
					DBG_FORCE("removing pronounciation variant:"<<iPron->mPtEntry.trans<<" phn_count:"<<phn_count);
					iKwd->mPronList.erase(iPron);
					changed = true;
					break;
				}
			}
		}
	} 
	/* }}} */

	DBG_FORCE("queryKwdList after postprocessing:"); queryKwdList->print();

	Search search(queryKwdList);

	search.SetLexiconPhn(config->lexiconPhn);
	search.SetLexiconLvcsr(config->lexiconLvcsr);
	search.SetIndexerPhn(config->indexerPhn);
	search.SetIndexerLvcsr(config->indexerLvcsr);
//	search.SetValidMeetings(validMeetings);

	search.SetRecordChannelColumn(config->p_record_channel_column);
	search.SetRecordNameCutoffColumn(config->p_record_cutoff_column);
	search.SetRecordDelim(config->p_record_delim);

	
	// create list of matching index entries (from the reverse index)
TIMER_START;
	search.InvertedIndexLookup_LvcsrPhn(queryKwdList, config->p_searchindex_lvcsr, config->p_searchindex_phn);
TIMER_END("reverse-index-lookup");



	// show matched keywords
	if (config->p_show_all_matched_keywords) {
		search.PrintMatchedKeywords(queryKwdList);
	}

/* create clusters for each keyword from query !!! COMMENTED OUT !!! {{{ */
/*
TIMER_START;

	DBG_MAIN("Clustering...");
	LatIndexer::Record tmpRec;
	for (QueryKwdList::iterator iQueryKwd = queryKwdList->begin(); iQueryKwd != queryKwdList->end(); ++iQueryKwd) 
	{
		DBG_MAIN("Kwd " << iQueryKwd->W << ": searchResults.size():" << iQueryKwd->searchResults.size());
		for (vector<QueryKwd::RevIdxRecord>::iterator i=iQueryKwd->searchResults.mvByConfidence.begin(); i!=iQueryKwd->searchResults.mvByConfidence.end(); ++i) 
		{
			tmpRec = *i;
			// if the meeting is not valid, then skip it
			if (validMeetings->size() > 0 && validMeetings->find(tmpRec.meetingID) == validMeetings->end()) 
			{
//				DBG_MAIN("meetingID:" << tmpRec.meetingID << " skipped");
				continue;
			}
//			iQueryKwd->recClusters.add(tmpRec);
//			continue;

			// translate percentual time value into the real time of current cluster's meeting
			LatTime time_min_translated = 0.0;
			LatTime time_max_translated = 0.0;
			
			// min time
			if (iQueryKwd->time_min_type == lse::time) 
			{
				time_min_translated = iQueryKwd->time_min;						
			}
			else if (iQueryKwd->time_min_type == lse::percents)
			{
				time_min_translated = indexer->meetings.getVal(tmpRec.meetingID).length * iQueryKwd->time_min;
			}
			
			// max time
			if (iQueryKwd->time_max_type == lse::time) 
			{
				time_max_translated = iQueryKwd->time_max;
			}
			else if (iQueryKwd->time_max_type == lse::percents)
			{
				time_max_translated = indexer->meetings.getVal(tmpRec.meetingID).length * iQueryKwd->time_max;
			}
				
			// remove the mess and accept the good ones
			if ( (iQueryKwd->confidence_min_isset && tmpRec.conf < iQueryKwd->confidence_min) ||
				 (iQueryKwd->confidence_max_isset && tmpRec.conf > iQueryKwd->confidence_max) ||
				
				 (iQueryKwd->time_min_type != null && tmpRec.t < time_min_translated) ||
				 (iQueryKwd->time_max_type != null && tmpRec.t > time_max_translated) ) 
			{
				DBG("    Deleting...");
				i->wordID = -1; // SHOULD BE REALLY DELETED ?!?
			} 
			else 
			{
//				iQueryKwd->recClusters.add(tmpRec, false); // the second argument 'false' means that the overlapping hypotheses in the reverse index are already clustered (only one hypothesis)
			}
		}
	}
	DBG_MAIN("  done");

TIMER_END("create clusters for each keyword");	
*/
/* }}} */


	/* show inverted index lookup results {{{ */
	if (config->p_show_reverse_index_lookup_results) {
		for (QueryKwdList::iterator iQueryKwd = queryKwdList->begin(); iQueryKwd != queryKwdList->end(); ++iQueryKwd) {
			DBG_FORCE("CLUSTERS LIST FOR KWD: "<<iQueryKwd->W);
			DBG_FORCE("searchResults.size(): "<<iQueryKwd->searchResults.size());
//			iQueryKwd->recClusters.sort();
//			iQueryKwd->recClusters.print();
		}
	}
	/* }}} */


	/* create PHN clusters {{{ */
TIMER_START;
	search.InvertedIndexLookupResultsClustering_Phn(queryKwdList);
TIMER_END("computing neighbourhood hypotheses (PHN)");

	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("PrintInvertedIndexLookupResultsClusters_LvcsrPhn()");
	search.PrintInvertedIndexLookupResultsClusters_LvcsrPhn(queryKwdList);
	DBG_FORCE("PrintInvertedIndexLookupResultsClusters_LvcsrPhn()...done");
	DBG_FORCE("--------------------------------------------------");

TIMER_START;
	search.FillInvIdxRecordsClusters_Phn(queryKwdList);
TIMER_END("creating clusters of neighbourhood hypotheses (PHN)");

TIMER_START;
	search.SortInvIdxRecordsClusters_Phn(queryKwdList);
TIMER_END("sorting clusters of neighbourhood hypotheses (PHN)");
	/* }}} */

	// show clusters
	if (config->p_show_all_clusters) 
	{
		search.PrintInvIdxRecordsClusters_Phn(queryKwdList);
	}

	/* create LVCSR + PHN clusters {{{ */
	search.CombineInvertedIndexLookupResults_LvcsrPhn(queryKwdList);

TIMER_START;
	search.InvertedIndexLookupResultsClustering_LvcsrPhn(queryKwdList);
TIMER_END("computing neighbourhood hypotheses (LVCSR + PHN)");

//	search.PrintInvertedIndexLookupResultsClusters_LvcsrPhn(queryKwdList);

TIMER_START;
	search.FillInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
TIMER_END("creating clusters of neighbourhood hypotheses (LVCSR + PHN)");

TIMER_START;
	search.SortInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
TIMER_END("sorting clusters of neighbourhood hypotheses (LVCSR + PHN)");
	/* }}} */

	// show clusters
	if (config->p_show_all_clusters) 
	{
		search.PrintInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
	}

	if (config->p_verify_candidates)
	{
		//search.VerifyCandidates(config, queryKwdList, pResults);
	}
	else
	{
		search.InvIdxRecordsClusters2Results(config, queryKwdList, pResults);
	}
}
/* }}} */

//--------------------------------------------------------------------------------
//	processANYkwd
//--------------------------------------------------------------------------------
/**
  @brief process the special case of query consisting of the keyword '_any_'

  @param *lexicon
  @param *indexer
  @param src search source (lattice, best path, ... )
  @param kwd parsed query (keyword search constraints)
*/
/* processANYkwd {{{ */
/*
void processANYkwd(Lexicon *lexicon, Lattice::Indexer *indexer, SearchSource src, QueryKwd kwd, MeetingsList *validMeetings) 
{
	string filename;
	LatBinFile latBinFile;
	LatBinFile::Node tmpNode;
	Hypothesis::Word tmpWord;
	LatIndexer::RecordClusters recClusters;
	LatIndexer::Record latIdxRecord;
	
	DBG_FORCE("KWD: "<<kwd);
	
	for (LatMeetingIndexer::Id2valMap::iterator iMeeting = indexer->meetings.id2valMap.begin(); iMeeting != indexer->meetings.id2valMap.end(); ++iMeeting) {

		// check if the current meeting is valid for searching
		if (validMeetings->size() > 0 && validMeetings->find(iMeeting->second.id) == validMeetings->end()) {
			DBG_FORCE("meetingID:" << iMeeting->second.id << " skipped");
			continue;
		}
			
		string filename = iMeeting->second.path;
		if (src == lattice)
			filename += LATBINFILE_EXT_NODES;
		else if (src == best_path)
			filename += LATBINFILE_EXT_BESTNODES;
		
		if (p_data_dir_path[p_data_dir_path.length()-1] != PATH_SLASH && filename[0] != PATH_SLASH)
			p_data_dir_path += PATH_SLASH;
		
		string meeting_full_path = p_data_dir_path + filename;
		
		ifstream f_inNodes(meeting_full_path.c_str(), ios::binary);
		if (!f_inNodes.good()) {
			CERR("ERROR: can not open file " << meeting_full_path);
			exit(1);
		}

		DBG_FORCE("Looking for words in meeting: "<<meeting_full_path);
		
		while (!f_inNodes.eof()) {
			tmpNode.readFrom(f_inNodes);
			if (f_inNodes.eof()) break;
			// translate percentual time value into the real time of current cluster's meeting
			LatTime time_min_translated = 0.0;
			LatTime time_max_translated = 0.0;
			
			// min time
			if (kwd.time_min_type == lse::time) 
				time_min_translated = kwd.time_min;						
			else if (kwd.time_min_type == lse::percents)
				time_min_translated = indexer->meetings.getVal(iMeeting->second.id).length * kwd.time_min;
			
			// max time
			if (kwd.time_max_type == lse::time) 
				time_max_translated = kwd.time_max;						
			else if (kwd.time_max_type == lse::percents)
				time_max_translated = indexer->meetings.getVal(iMeeting->second.id).length * kwd.time_max;

			if ( (kwd.confidence_min_isset && tmpNode.conf < kwd.confidence_min) ||
				 (kwd.confidence_max_isset && tmpNode.conf > kwd.confidence_max) ||

				 (kwd.time_min_type != null && tmpNode.t < time_min_translated) ||
				 (kwd.time_max_type != null && tmpNode.t > time_max_translated) ) 
			{
				// this kwd does not match the query
			} else {
				if (p_word_clusters) {
					// add hypothesis to clusters
					latIdxRecord.wordID = tmpNode.W_id;
					latIdxRecord.meetingID = iMeeting->first;
					//latIdxRecord.position = ;
					// META-INFO
					latIdxRecord.conf = tmpNode.conf;		// confidence
					latIdxRecord.tStart = tmpNode.tStart;	// word's start time
					latIdxRecord.t = tmpNode.t;		// word's end time
					
					recClusters.add(latIdxRecord);
				} else {
					// just print the hypothesis
					tmpWord.str = lexicon->id2word_readonly(tmpNode.W_id);
					tmpWord.start = tmpNode.tStart;
					tmpWord.end = tmpNode.t;
					tmpWord.conf = tmpNode.conf;

					cout << "WORD\t" << tmpWord << endl;
				}
			}
		} // while !eof
		f_inNodes.close();
	}

	if (p_word_clusters) {
		recClusters.sort();
		for (LatIndexer::RecordClusters::iterator iCluster = recClusters.begin(); iCluster != recClusters.end(); ++iCluster) {
			tmpWord.str = lexicon->id2word_readonly(iCluster->begin()->wordID);
			tmpWord.start = iCluster->begin()->tStart;
			tmpWord.end = iCluster->begin()->t;
			tmpWord.conf = iCluster->begin()->conf;
			cout << "WORD\t" << tmpWord << endl;
		}
	}
}
*/
/* }}} */

/* isKwdSeparator {{{*/
bool isKwdSeparator(char c) 
{
	return (c==' ' || c=='\t' || c=='\n');
}
/*}}}*/

/* isANY {{{*/
bool isANY(string kwd) 
{
	return kwd == "_any_";
}
/*}}}*/

//--------------------------------------------------------------------------------
//	str2LatTime
//--------------------------------------------------------------------------------
/**
  @brief converts the input string to seconds

  @param str input string

  @return time in seconds
*/
/* str2LatTime {{{ */
LatTime str2LatTime(std::string str) 
{
	LatTime res = 0;
	string strTmp = "";
	float multiplier = 0;
	for (unsigned int i=0; i<str.length(); i++) {
		switch (str[i]) {
			
			case 'h':
//				DBG_FORCE("hours: <"<<strTmp<<">");
				multiplier = 3600; 	// 1 hour = 3600 sec
				break;

			case 'm':
//				DBG_FORCE("minutes: <"<<strTmp<<">");
				multiplier = 60; 	// 1 min = 60 sec
				break;

			case 's':
//				DBG_FORCE("seconds: <"<<strTmp<<">");
				multiplier = 1; 	// 1 sec = 1 sec :o)
				break;

			case '%':
				DBG("percents: <"<<strTmp<<">");
				multiplier = -0.01;
				break;

			default:
				strTmp += str[i];

		}

		if (multiplier != 0) {
			res += atof(strTmp.c_str()) * multiplier;
			multiplier = 0;
			strTmp.clear();
		}
	}
	DBG("res: "<<res);
	return res;
}
/* }}} */

