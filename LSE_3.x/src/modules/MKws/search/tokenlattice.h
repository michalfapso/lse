#ifndef TOKENLATTICE_H
#define TOKENLATTICE_H

#include <vector>
#include <list>
#include <map>
#include "lattypes.h"
#include "latbinfile.h"
#include "latindexer.h"
#include "querykwdlist.h"
//#include "SearchClusterGroups.h"

namespace lse {


//#define DBG_SEARCH(str) DBG_FORCE(str)
#define DBG_SEARCH(str)
//#define DBG_TOKENS(str) DBG_FORCE(str)
#define DBG_TOKENS(str)


#define GROUP_TIME_OFFSET	0.0 // time in seconds
#define KWD_SCALE_MULTIPLIER 0.00001

static int WLRcounter = 0;
static int globalWlrID = 0;
	
//================================================================================
//	WLR
//================================================================================
class WLR 
{
	public:

		//========================================================================
		//	ID
		//========================================================================
		class ID 
		{
				unsigned char*	mData;
				int 			mSize;
				unsigned char 	kwdsCount;

				void AddKwd(int queryKwdIndex);
				friend class WLR;
			public:
				ID() : mData(NULL), mSize(0), kwdsCount(0) { 
//					DBG_SEARCH("ID::ID()"); 
				}
				ID(const int &p_size) { 
					this->Init(p_size); 
//					DBG_SEARCH("ID::ID(mSize)"); 
				}
				ID(const ID &id) {
//					DBG_SEARCH("ID::ID(&id)");
					mSize = 0;
					mData = NULL;
					Init(id.mSize);
					memcpy(mData, id.mData, mSize);
					kwdsCount = id.kwdsCount;
				}
				~ID() { 
					if (mData!=NULL) {
//						DBG_SEARCH("ID::~ID()...deleting mData");
						delete[] mData; 
					} else {
//						DBG_SEARCH("ID::~ID()...mData == NULL");
					}
				}

				void Init(int p_size) {
					mSize = p_size;
					if (mData!=NULL) delete[] mData;
					mData = new unsigned char[mSize];
					memset(mData, 0, mSize*sizeof(unsigned char));
					kwdsCount = 0;
				}

				friend bool operator==(const ID& l, const ID& r); 
				friend bool operator!=(const ID& l, const ID& r) { return !(l==r); }
				ID operator+(int kwdIndex) {
					if (kwdIndex < 0) 
						return *this;
					ID idTmp;
					idTmp = *this;
					idTmp.AddKwd(kwdIndex);
					return idTmp;
				}
				ID& operator=(const ID& r) {
					if (mData == NULL)
						Init(r.mSize);
					memcpy(mData, r.mData, mSize);
					kwdsCount = r.kwdsCount;
					return *this;
				}
				friend std::ostream& operator<<(std::ostream& os, const ID& id) {
					for (int i=0; i<id.mSize; i++)
						if (id.mData[i] > 0)
							os << i << " ";
					return os;
				}
				int GetKwdsCount() const { return this->kwdsCount; }
				bool IsEmpty() { return this->GetKwdsCount() == 0; }
		};
		
		

//		IDList nodeHistory;
//		WLR* wlr;
		
		
		int 	mObjID;
		WLR::ID mID; ///< paths with the same id can be merged together (only the one with the best score will survive)
		double 	mConf;
		double	mPathLikelihood;
		double	mAlpha;
		ID_t 	mNodeID;
		WLR* 	mNext;
		int 	refs;
		bool 	isKwd;
		int 	fwdContextLen;
		
		QueryKwdList::iterator miKwd; ///< if this WLR is on a keyword, then miKwd is an iterator to queryKwdList representing this keyword

		// CONSTRUCTOR
		WLR(ID_t p_nodeID, int p_queryKwdListSize) : 
			mObjID			(globalWlrID++), 
			mConf			(-INF), 
			mPathLikelihood	(-INF),
			mAlpha			(-INF),
			mNodeID			(p_nodeID), 
			mNext			(NULL), 
			refs			(0), 
			isKwd			(false), 
			fwdContextLen	(0)
		{ 
//			DBG_FORCE("WLR::WLR() mObjID:"<<mObjID);
			mID.Init(p_queryKwdListSize); 
			WLRcounter++;
		}
		
		// COPY CONSTRUCTOR
		WLR(const WLR &wlr) {
//			nodeHistory = path.nodeHistory;
//			wlr = path.wlr;
//			wlr->refs++;
//			DBG_FORCE("WLR::WLR(&wlr) mObjID:"<<mObjID);
			WLRcounter++;
			mObjID 			= globalWlrID++; 
			mID 			= wlr.mID;
			mConf 			= wlr.mConf;
			mPathLikelihood = wlr.mPathLikelihood;
			mAlpha 			= wlr.mAlpha;
			mNodeID 		= wlr.mNodeID;
			mNext 			= wlr.mNext;
			(mNext->refs)++;
			isKwd 			= wlr.isKwd;
			fwdContextLen 	= wlr.fwdContextLen;
		}

		~WLR() {
//			DBG_FORCE("~WLR(mObjID:"<<this->mObjID<<" mNodeID:"<<this->mNodeID<<")");
			WLRcounter--;
		}
		
		void free() {
//			DBG_FORCE("WLR::free("<<*this<<") mObjID:"<<mObjID);
			--(this->refs);
			assert(refs >= 0);

			if(refs == 0) {
				if(mNext != NULL) {
//					DBG_SEARCH("trying to free a 'mNext' pointer");
					mNext->free(); // recursively free all wlrs
				} else {
//					DBG_SEARCH("mNext pointer is NULL");
				}
//				DBG_SEARCH("deleting WLR at mNodeID "<<this->mNodeID);
				delete this;
			} else {
//				DBG_SEARCH("cannot delete WLR at mNodeID "<<this->mNodeID<<" ... refs:"<<refs);
			}
		}

//				~WLR() {
//					if(wlr != NULL) delete wlr;
//				}
		
//		void addNode(ID_t node, int queryKwdListSize, int queryKwdIndex = -1);
		double confidenceWithNode(ID_t nodeID, float linkConf, LatBinFile *binlat);
		void AddKwd(int queryKwdIndex);

		void setNext(WLR* nextWlr) {
			mNext = nextWlr;                                                    // get the pointer
			if (mNext != NULL) {
//				DBG_SEARCH("WLR::setNext() increasing refs on WLR->mObjID="<<mNext->mObjID<<": "<<mNext->refs<<"->"<<mNext->refs + 1);
				(mNext->refs)++;
			}
		}

		friend std::ostream& operator<<(std::ostream& os, const WLR& wlr) {
			os << "mObjID:"<<wlr.mObjID<<" mID:"<<wlr.mID<<" refs:"<<wlr.refs<<" mNext:"<<wlr.mNext<<" mAlpha:"<<wlr.mAlpha<<" mPathLikelihood:"<<wlr.mPathLikelihood<<" mConf:"<<wlr.mConf<<"|";

			const WLR* iWlr = &wlr;
			while (iWlr != NULL) {
				os << iWlr->mNodeID << " ";
				iWlr = iWlr->mNext;
			}
			return os;
		}
		
		friend bool operator<(const WLR& l, const WLR& r) {
			// better is the one with more keywords on the path, but if there are paths with the same keywords count, then compare path confidences
			if(l.mID.GetKwdsCount() == r.mID.GetKwdsCount()) {
				return l.mConf < r.mConf;
			} else {
				return l.mID.GetKwdsCount() < r.mID.GetKwdsCount();
			}
		}

};

//================================================================================
//	TokenPathList
//================================================================================
class TokenPathList : public std::list<WLR*> // list can do erase() in constant time, whereas vector only in linear time
{
	public:
		WLR* getBestTokenPath();
		TokenPathList::iterator getBestTokenPathIterator();

		WLR* getBestPath() {
			WLR* bestWLR = *(this->begin());
			for (TokenPathList::iterator iPath=this->begin(); iPath!=this->end(); ++iPath) {
				if (bestWLR->mConf < (*iPath)->mConf) {
					bestWLR = *iPath;
				}
			}
			return bestWLR;
		}
		
		void addPath(WLR* wlr) {
			wlr->refs++;
			this->push_back(wlr);
		}
		void print() {
			for (TokenPathList::iterator iPath=this->begin(); iPath!=this->end(); ++iPath) {
				std::cerr << (**iPath) << std::endl;
			}
		}
		void freePath(std::list<WLR*>::iterator iWlr) {
			(*iWlr)->free();
			this->erase(iWlr);
		}
};

//================================================================================
//	TokenLattice
//================================================================================
class TokenLattice 
{
	public:
		class Node
		{
			public:
				TokenPathList tokenPathList;
				ID_t mID;

				void startNewToken(LatBinFile::Node* binNode, QueryKwdList* queryKwdList);
				friend std::ostream& operator<<(std::ostream& os, const Node& n) {
					os << "[nodeID:"<<n.mID<<"]: ";
					for (TokenPathList::const_iterator i=n.tokenPathList.begin(); i!=n.tokenPathList.end(); ++i)
						os << "("<<**i<<") ";
					return os;
				}
		};
	private:
		int firstID;
		int lastID;
		int nodesCount;
		TokenLattice::Node * nodes;
		LatBinFile *binlat;
		Lexicon* mpLexicon;
		QueryKwdList *queryKwdList;
	
	public:
		TokenPathList resultPathList; ///< a list of best token paths
		
		/**
		 * @brief Constructor for TokenLattice
		 *
		 * @param p_startNodeID first nodeID in the part of lattice
		 * @param p_endNodeID last nodeID in the part of lattice
		 * @param *p_binlat the lattice loaded into the memory
		 */
		TokenLattice(
				LatBinFile *pBinlat, 
				Lexicon *pLexicon,
				QueryKwdList *pQueryKwdList) 
		: 	
			binlat				(pBinlat), 
			mpLexicon			(pLexicon),
			queryKwdList		(pQueryKwdList)
		{
			firstID 	= binlat->nodes.firstID;
			lastID  	= binlat->nodes.lastID;
			nodesCount 	= lastID - firstID + 1;
			nodes 		= new TokenLattice::Node[nodesCount];
			// set tokenlattice nodes' mID
			for (int i=0; i<nodesCount; i++) nodes[i].mID=i+firstID;	
		}

		/**
		 * @brief Destructor for the TokenLattice
		 */
		~TokenLattice() { if (nodes!=NULL) delete[] nodes; }
	
		/**
		 * @brief Operator for getting the TokenLattice::Node structure by associated lattice's nodeID
		 *
		 * @param nodeID
		 *
		 * @return 
		 */
		TokenLattice::Node& operator[](int nodeID) { return nodes[nodeID - firstID]; }
		
		void ForwardPass(InvIdxRecordsCluster* pCluster, QueryKwdList* pQueryKwdList);

		
		/**
		 * @brief Check if the given sequence of nodes fits the query
		 *
		 * @param fromPath the preceding path
		 * @param toNodeID new node on the path (which is going to be added to this path)
		 * @param piCurKwd if toNodeID represents any keyword from the query, then in piCurKwd will be written the appropriate queryKwdList iterator
		 *
		 * @return true, if the new node (toNodeID) fits the given query
		 */
		bool FitsQuery(WLR* fromPath, ID_t toNodeID, QueryKwdList::iterator* piCurKwd);
		
		bool SendToken(ID_t fromNodeID, ID_t toNodeID, float linkConf);
		bool CanStartTokenOnKwd(ID_t W_id, int *kwdIndex);
		void FreeTokens();
//		void print();
};

}// namespace

#endif
