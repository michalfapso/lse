#include <algorithm>
#include "search.h"

using namespace std;
using namespace lse;

void Search::InvertedIndexLookupResultsClustering_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN search resutls
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->InvertedIndexLookupResultsClustering();
		}
	}
}

void Search::CombineInvertedIndexLookupResults_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	DBG_FORCE("CombineInvertedIndexLookupResults_LvcsrPhn()");
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		iKwd->mpClusters = new InvIdxRecordsClusters;

		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			// sort the clusters by time since we want to use binary search
			DBG_FORCE("mpClusters->SortClusters()");
			iKwd->mpClusters->SortClusters(InvIdxRecordsClusters::SortType::ByTime);
			DBG_FORCE("mpClusters->SortClusters()...done");
			InvIdxRecordsClusters new_clusters;
			// for each cluster of the variant
			for (InvIdxRecordsClusters::iterator iCluster = iPron->mpQueryKwdListNgram->mInvIdxRecordsClusters.begin();
				iCluster != iPron->mpQueryKwdListNgram->mInvIdxRecordsClusters.end();
				iCluster ++)
			{
				// if there is no such occurrence in the LVCSR inverted index lookup results
				// and no previous PronVariant has added it yet
				DBG("searchResults.FindOccurrence()");
				if (!iKwd->searchResults.FindOccurrence(*iCluster) && !iKwd->mpClusters->FindCluster(*iCluster))
				{
					DBG("adding new cluster: "<<*iCluster);
					iCluster->SetSourceType(SourceType::PHN_CLUSTER);
					iCluster->SetPronVariant(&(*iPron));
					new_clusters.push_back(*iCluster);
				}
			}
			// add all the new_clusters to the query keyword's clusters list
			for (InvIdxRecordsClusters::iterator iCluster = new_clusters.begin();
				iCluster != new_clusters.end();
				iCluster ++)
			{
				iKwd->mpClusters->push_back(*iCluster);
			}
//			DBG_FORCE("iKwd->mpClusters->Print()"); iKwd->mpClusters->Print();
		}

		// at last add all LVCSR occurrences to the clusters list
		for (unsigned int i=0; i<iKwd->searchResults.size(); i++)
		{
			InvIdxRecordsCluster cluster = iKwd->searchResults.mvByConfidence[i];
			cluster.SetSourceType(SourceType::LVCSR);
			iKwd->mpClusters->push_back(cluster);
		}

		// now sort the complete word's clusters list by time again
		iKwd->mpClusters->SortClusters(InvIdxRecordsClusters::SortType::ByTime);
//		DBG_FORCE("iKwd->mpClusters->Print()"); iKwd->mpClusters->Print();
	}
}

void Search::InvertedIndexLookupResultsClustering_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR search results
	pQueryKwdList->QueryKwdClustersClustering();
}

void Search::PrintInvertedIndexLookupResultsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR search results
	pQueryKwdList->PrintInvertedIndexLookupResultsClusters();

	// for PHN search resutls
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->PrintInvertedIndexLookupResultsClusters();
		}
	}
}

void Search::FillInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN inversed index records
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->FillInvIdxRecordsClusters(SourceType::PHN);
		}
	}
}

void Search::FillInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR inversed index records
	pQueryKwdList->FillQueryClusters();
}

void Search::SortInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN inversed index records
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->SortInvIdxRecordsClusters(InvIdxRecordsClusters::SortType::ByTimeAndConf);
		}
	}
}

void Search::SortInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR inversed index records
	pQueryKwdList->SortInvIdxRecordsClusters(InvIdxRecordsClusters::SortType::ByTimeAndConf); // sort LVCSR by time and by confidence
}

void Search::PrintInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN inversed index records
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->PrintInvIdxRecordsClusters();
		}
	}
}

void Search::PrintInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR inversed index records
	pQueryKwdList->PrintInvIdxRecordsClusters();
}

void Search::ExpandPhonemes(const QueryKwdList* src, QueryKwdList* dest)
{
	int 			length = 3;
	float			nb_prev = 1;
	float			nb_next = 1;
	HistContainer	hist;

	dest->mUniqSize = 0;

	for (QueryKwdList::const_iterator iKwd = src->begin(); iKwd != src->end(); ++iKwd)
	{
		HistRecord histRec;
		histRec.W_id = iKwd->W_id;
		histRec.W	 = iKwd->W;
		hist.push_back(histRec);

		if ((int)hist.size() == length)
		{
			QueryKwd kwd;
			kwd.W 						= "";
			for (HistContainer::iterator iHistRec=hist.begin(); iHistRec!=hist.end(); ++iHistRec)
			{
				kwd.phonemes.push_back(iHistRec->W_id);
				kwd.W 				   += (kwd.W=="" ? "" : "_") + iHistRec->W;
			}
			kwd.W_id 					= mpLexiconPhn->word2id_readonly(kwd.W);
			kwd.nb_prev 				= nb_prev;
			kwd.nb_next					= nb_next;
			kwd.isNextInPhrase			= false;
			kwd.confidence_min_isset	= false;
			kwd.confidence_max_isset	= false;
			kwd.time_min				= iKwd->time_min;
			kwd.time_max				= iKwd->time_max;
			kwd.time_min_type			= iKwd->time_min_type;
			kwd.time_max_type			= iKwd->time_max_type;
			kwd.lessThan				= false;
			kwd.greaterThan				= false;
			kwd.mIsNgram				= true;
			
			dest->push_back(kwd);
			hist.pop_front();
		}
	}

	if (dest->size() > 0)
	{
		(dest->begin())->nb_prev = -1;
		(--dest->end())->nb_next = -1;
	}
}

void Search::NgramsCluster2PhnCluster(InvIdxRecordsCluster* pClusterNgrams, InvIdxRecordsCluster* pClusterPhonemes, Lexicon* pLexicon)
{
	// for each item in the group (couple iKwd -> ClustersList)
	// for LVCSR single-word-query there is only 1 item
	// for PHN single-word-query there might be more phoneme ngrams
	pClusterPhonemes->mMeetingID = pClusterNgrams->mMeetingID;
	pClusterPhonemes->mStartTime = min(pClusterNgrams->mStartTime, pClusterPhonemes->mStartTime);
	pClusterPhonemes->mEndTime   = max(pClusterNgrams->mEndTime, pClusterPhonemes->mEndTime);
	pClusterPhonemes->mConf      = min(pClusterNgrams->mConf, pClusterPhonemes->mConf);
	pClusterPhonemes->mSourceType= SourceType::PHN;

	for (InvIdxRecordsCluster::iterator iClusterItem = pClusterNgrams->begin(); 
		 iClusterItem != pClusterNgrams->end(); 
		 iClusterItem ++)
	{
		ID_t kwd_W_id = iClusterItem->first;
		OccurrenceWithSourceList* pOCL = &(iClusterItem->second);

		vector<string> unigrams;
		string_split(pLexicon->id2word_readonly(kwd_W_id), "_", unigrams);

		// for each unigram in current iKwd
		for (vector<string>::iterator iUnigram = unigrams.begin(); iUnigram != unigrams.end(); ++iUnigram)
		{
			ID_t unigram_W_id			= pLexicon->word2id_readonly(*iUnigram);

			// for each occurrence of the current ngram
			for (int i=0; i<pOCL->size(); i++)
			{
				LatIndexer::Occurrence* pNgramRecord = pOCL->GetOccurrence(i);
				LatIndexer::Occurrence* pNewRecord = new QueryKwd::InvIdxRecord;
				//pNewRecord->mWordID 	= unigram_W_id;
				pNewRecord->mMeetingID 	= pNgramRecord->mMeetingID;
				pNewRecord->mStartTime	= pNgramRecord->mStartTime;
				pNewRecord->mEndTime	= pNgramRecord->mEndTime;
				pNewRecord->mConf		= pNgramRecord->mConf; // confidence is actually not relevant in the verification process, because it is recomputed in the verification step

				pClusterPhonemes->Insert(unigram_W_id, pNewRecord, SourceType::PHN);

				DBG_FORCE("pClusterPhonemes->Insert("<<unigram_W_id<<", "<<*pNewRecord<<")");
				//							DBG_FORCE("phn: "<<*iUnigram<<" "<<*pNewRecord);
			}
		}
	}
}

void Search::addToHypothesis(ID_t curNodeID, LatTime tStart, float conf, Hypothesis *hyp, Lexicon *pLexicon, QueryKwdList* pQueryKwdList, EFrontBack frontback)
{
//	DBG_FORCE("Search::addToHypothesis()");
	if (!pLexicon->isMetaWord(binlat->nodes[curNodeID].W_id)) 
	{
		Hypothesis::Word curHypWord;
		curHypWord.str = pLexicon->id2word_readonly(binlat->nodes[curNodeID].W_id);
		curHypWord.start = tStart;
		curHypWord.end = binlat->nodes[curNodeID].t;
		curHypWord.conf = conf; // Likelihood of best link coming into current node
		if (hyp->stream == "PHN" || hyp->stream == "PHN_CLUSTER")
		{
			DBG_FORCE("curHypWord.str:"<<curHypWord.str);
			curHypWord.pron = curHypWord.str;
		}
		if (frontback == back) {
			hyp->push_back_word(curHypWord);
		} else 
		if (frontback == front) {
			hyp->push_front_word(curHypWord);
		}
		if (pQueryKwdList->contains(binlat->nodes[curNodeID].W_id)) {
			if (frontback == front) {
				hyp->addKeywordFront(curHypWord);
			} else {
				hyp->addKeywordBack(curHypWord);
			}
		}
//		DBG_SEARCH("addToHypothesis() curNodeID:"<<curNodeID<<"\tW_id:"<<binlat->nodes[curNodeID].W_id<<"\tconf:"<<conf<<"\tword:"<<curHypWord.str);
	} else {
//		DBG_SEARCH("addToHypothesis() curNodeID:"<<curNodeID<<"\t W_id:"<<binlat->nodes[curNodeID].W_id<<"\tconf:"<<conf<<"\tword:<META>");
	}
//	DBG_FORCE("Search::addToHypothesis() ... done");
}
	

void Search::GetNodesContext(Lexicon *pLexicon, ID_t nodeID, DirectionType dir, Hypothesis *hyp, QueryKwdList* pQueryKwdList)
{
	// *** BACKWARD LINKS ***
//	DBG_FORCE("Search::GetNodesContext("<<nodeID<<", "<<(dir==FWD?"FWD":"BCK")<<")");
	ID_t nextNodeID;
	ID_t curNodeID = nodeID;
	ID_t bestNodeID;
	TLatViterbiLikelihood bestLinkLikelihood;
	int count = 0;
	int count_max = (dir==FWD ? pQueryKwdList->fwd_nodes_count : pQueryKwdList->bck_nodes_count);
//	DBG_FORCE("count_max:"<<count_max);
	while (count < count_max) 
	{
//		DBG_FORCE("curNodeID:"<<curNodeID);
		if ((bestNodeID = binlat->nodeLinks.bestFollowingNode(curNodeID, dir, &bestLinkLikelihood)) == -1) { // if first node found (which has no backward links), then break the loop
//			DBG_FORCE("nodeID:"<<curNodeID<<" ...first node found -> backward processing termination");
			break;
		}

		nextNodeID = bestNodeID;
		if (!binlat->nodes.containsID(nextNodeID)) { // if we are going to jump out of read lattice
//			DBG_FORCE("nextNodeID:"<<nextNodeID<<" ...going to jump out of read lattice -> backward processing termination");
			break;
		}

//		DBG_FORCE("curNodeID:"<<curNodeID<<" nextNodeID:"<<nextNodeID);
		// add hypothesis (the first kwd on the best path has been added to the hypothesis already in rescoreHits())
		if (dir == BCK && count > 0) 
		{
			addToHypothesis(curNodeID, binlat->nodes[nextNodeID].t, bestLinkLikelihood, hyp, pLexicon, pQueryKwdList, (dir==FWD ? back : front));
		}
		else if (dir == FWD)
		{
			addToHypothesis(curNodeID, binlat->nodes[curNodeID].tStart, bestLinkLikelihood, hyp, pLexicon, pQueryKwdList, (dir==FWD ? back : front));
		}
		count++;
		curNodeID = nextNodeID; // go to the next node
	}
//	DBG_FORCE("Search::GetNodesContext() ... done");
}



//================================================================================
//	AddPathToHypothesis
//================================================================================
void Search::AddPathToHypothesis(WLR* pBestPath, Hypothesis* pHyp, ID_t* pFirstNodeID, Lexicon *pLexicon, QueryKwdList* pQueryKwdList)
{
	if (pBestPath == NULL) {
		DBG_FORCE("!!! bestPath->nodeHistory.size() == 0 !!!");
		return;
	}
	
	if (pBestPath != NULL) *pFirstNodeID = pBestPath->mNodeID;
	LatBinFile::LatNodeLinksArray::Cursor cur(&(binlat->nodeLinks));
	WLR* iWlr = pBestPath;
	while (iWlr->mNext != NULL)
	{
//		DBG_SEARCH("================================================================================");
//		DBG_SEARCH("iNodeID:"<<iWlr->mNodeID);
//			tokenLattice[*iNodeID].tokenPathList.print();
		cur.gotoNodeID(iWlr->mNodeID, BCK);
		while (!cur.eol()) 
		{
			LatBinFile::NodeLinks::Link l = cur.getLink();
			if (l.nodeID == iWlr->mNext->mNodeID) 
			{
				addToHypothesis(iWlr->mNodeID, binlat->nodes[iWlr->mNext->mNodeID].t, l.conf, pHyp, pLexicon, pQueryKwdList, front);
			}
			cur.next();
		}
		iWlr = iWlr->mNext;
		*pFirstNodeID = iWlr->mNodeID;
	}

	TLatViterbiLikelihood predLinkLikelihood;
	ID_t predNodeID = binlat->nodeLinks.bestFollowingNode(iWlr->mNodeID, BCK, &predLinkLikelihood); // if first node found (which has no backward links), then break the loop
	DBG_FORCE("predNodeID = "<<predNodeID<<"   conf = "<<predLinkLikelihood);
	
	// now we need to add the first node (with a proper start time)
	addToHypothesis(iWlr->mNodeID, binlat->nodes[iWlr->mNodeID].tStart, binlat->nodes[iWlr->mNodeID].conf, pHyp, pLexicon, pQueryKwdList, front);
}



void Search::PrintCandidates(InvIdxRecordsCluster* pGroup, QueryKwdList* pQueryKwdList, LatMeetingIndexerRecord &rMeetingRec)
{
	cout << "\"" << rMeetingRec.path << "\"" << endl;
	cout << setprecision(13) << pGroup->GetStartTime() * 10000000 << " " << pGroup->GetEndTime() * 10000000;
	for (QueryKwdList::iterator iKwd=pQueryKwdList->begin(); iKwd!=pQueryKwdList->end(); ++iKwd)
	{
		cout << " " << iKwd->W;
	}
//				cout << " " << pGroup->conf;
	cout << endl;
	cout << "." << endl;
}

void Search::PrintCandidateTrigrams(Lexicon *pLexicon, InvIdxRecordsCluster* pGroup, QueryKwdList* pQueryKwdList, LatMeetingIndexerRecord &rMeetingRec)
{
	cout << "\"" << rMeetingRec.path << "\"" << endl;

	string phn_str = "";
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); iKwd != pQueryKwdList->end(); ++iKwd)
	{
		phn_str += (phn_str=="" ? "" : "_") + iKwd->W;
	}

	cout << "@" << setprecision(13) << (int)(pGroup->mStartTime * 10000000) << " " << (int)(pGroup->mEndTime * 10000000) << " " << phn_str << endl;
	for (InvIdxRecordsCluster::iterator iClusterList = pGroup->begin(); iClusterList != pGroup->end(); ++iClusterList)
	{
		string ngram = pLexicon->id2word_readonly(iClusterList->first);

		for (int i=0; i<iClusterList->second.size(); i++)
		{
			LatIndexer::Occurrence* pInvRec = iClusterList->second.GetOccurrence(i);
			double normconf = pInvRec->mConf / (pInvRec->mEndTime * 100 - pInvRec->mStartTime * 100);
			cout << setprecision(13) << (int)(pInvRec->mStartTime * 10000000) << " " << (int)(pInvRec->mEndTime * 10000000) << " " << ngram << " " << normconf << endl;
		}
	}
	cout << "." << endl;
}

/*
string Search::GetMeetingChannelFromMeetingName(const string &meetingName, SearchConfig* pConfig)
{
	// get record's channel
	if (pConfig->p_record_channel_column > 0)
	{
		string::size_type pos = 0;
		for (int field_idx=1; field_idx < pConfig->p_record_channel_column; field_idx++) {
			pos = meetingName.find (pConfig->p_record_delim, pos+1); // find first _ in latname
			DBG("pos:"<<pos);
		}
		if (pos != string::npos) {
			string::size_type pos_2nd = meetingName.find (pConfig->p_record_delim, pos+1); // find second _ in latname
			if (pos_2nd == string::npos)
			{
				pos_2nd = meetingName.find ('.', pos+1);
			}
			return meetingName.substr(pos+1, pos_2nd - pos - 1);
		}
	}
	return "";
}
*/

/*
string Search::CutoffMeetingName(const std::string &meetingName, SearchConfig* pConfig)
{
	// cut off additional info from record's name
	if (pConfig->p_record_cutoff_column > 0)
	{
		string::size_type pos = 0;
		for (int field_idx=1; field_idx < pConfig->p_record_cutoff_column; field_idx++) {
			pos = meetingName.find (pConfig->p_record_delim, pos+1); // find first _ in latname
			DBG("pos:"<<pos);
		}
		if (pos != string::npos) {
			return meetingName.substr(0, pos);
		} else {
			CERR("ERROR in record's name: Expecting '"<<pConfig->p_record_delim<<"' in '" << meetingName << "' ...using full record's path");
		}
	}
	return meetingName;
}
*/

void Search::PrintMatchedKeywords(QueryKwdList* pQueryKwdList)
{
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); iKwd != pQueryKwdList->end(); ++iKwd) {
		DBG_FORCE("FOUND KEYWORD: " << *iKwd);
		DBG_FORCE("foundLatRecords.size(): " << iKwd->searchResults.size());
		for (vector<QueryKwd::InvIdxRecord>::iterator iRec = iKwd->searchResults.mvByConfidence.begin(); iRec != iKwd->searchResults.mvByConfidence.end(); ++iRec) {
			DBG_FORCE("[LVCSR]"<<*iRec);
		}
		DBG_FORCE("================================================================================");
		for (PronVariants::iterator iPronVariant = iKwd->mPronList.begin(); 
			 iPronVariant != iKwd->mPronList.end(); 
			 iPronVariant ++)
		{
			DBG_FORCE("================================================================================");
			DBG_FORCE("PronVariant: "<<endl<<*(iPronVariant->mpQueryKwdListPhn));
			DBG_FORCE("================================================================================");
			for (QueryKwdList::iterator iPronVariantKwd = iPronVariant->mpQueryKwdListNgram->begin(); 
				 iPronVariantKwd != iPronVariant->mpQueryKwdListNgram->end(); 
				 iPronVariantKwd ++) 
			{
				for (vector<QueryKwd::InvIdxRecord>::iterator iRec = iPronVariantKwd->searchResults.mvByConfidence.begin();
					 iRec != iPronVariantKwd->searchResults.mvByConfidence.end(); 
					 iRec ++) 
				{
					DBG_FORCE("[PHN]"<<*iRec);
				}
				DBG_FORCE("================================================================================");
			}
		}
	}
}

#if 0
bool Search::RescoreHits(
		Lexicon *pLexicon, 
		InvIdxRecordsCluster* pCluster, 
		QueryKwdList* pQueryKwdList, 
		LatMeetingIndexerRecord meetingRec, 
		SearchConfig* pConfig, 
		Hypothesis &hypSettings, 
		Results *results) 
{
	// create token lattice
	TokenLattice tokenLattice(binlat, pLexicon, pQueryKwdList);
	
	// do the forward pass on binlat with pQueryKwdList and add all results into tokenLattice.resultPathList
	tokenLattice.ForwardPass(pCluster, pQueryKwdList);

	DBG_FORCE("tokenLattice.resultPathList.size(): "<<tokenLattice.resultPathList.size());
	

	bool found = (tokenLattice.resultPathList.size() != 0);
	// if we have not found any result, return
	if (tokenLattice.resultPathList.size() != 0)
	{
		TokenPathList::iterator iTokenPath = tokenLattice.resultPathList.begin();

		if (!pQueryKwdList->mReturnAllSuccessfulTokens)
		{
			iTokenPath = tokenLattice.resultPathList.getBestTokenPathIterator();
		}

		DBG_FORCE("tokenLattice.resultPathList.print():"); tokenLattice.resultPathList.print();
			
		while(iTokenPath != tokenLattice.resultPathList.end())
		{
			Hypothesis hyp(hypSettings);

			WLR* pTokenPath = *iTokenPath;
			if (pQueryKwdList->mReturnAllSuccessfulTokens)
			{
				iTokenPath++;
			}
			DBG("returned path:"<<*pTokenPath);
			
			ID_t path_first_node_id = -1; // will be set within AddPathToHypothesis()
			AddPathToHypothesis(pTokenPath, &hyp, &path_first_node_id, pLexicon, pQueryKwdList); // add all words from the best path to hyp and set path_first_node_id
			hyp.score = pTokenPath->mConf; // set the score of the hypothesis as the sore of the best path
			// now start the backward links processing from the first node on the best path to get the context before it
			if (pQueryKwdList->bck_nodes_count > 0)
			{
			DBG("GetNodesContext("<<path_first_node_id<<", BCK)");
				GetNodesContext(pLexicon, path_first_node_id, BCK, &hyp, pQueryKwdList);
			}

			DBG_FORCE("HYPOTHESIS:"<<hyp);
			// don't add empty results
			results->push_back(hyp);
//			DBG_FORCE("results->push_back(hyp): "<<hyp);
			// if we want to return only the best path, then it's done
			if (!pQueryKwdList->mReturnAllSuccessfulTokens)
			{
				break;
			}
		}
	}

	// LET'S FREE ALL WLRs
	DBG_SEARCH("DELETING WLRs (resultPathList.size(): "<<tokenLattice.resultPathList.size()<<")");
	while (tokenLattice.resultPathList.size() > 0)
	{
		tokenLattice.resultPathList.freePath(tokenLattice.resultPathList.begin());
	}
	tokenLattice.FreeTokens();
	
	DBG_SEARCH("Search::rescoreHits() done");
	return found;
}

void Search::VerifyCandidates_HybridSources(
		SearchConfig* pConfig,
		QueryKwdList* pQueryKwdList,
		InvIdxRecordsCluster* pCluster,
		std::vector<LocalResults*>* pLocalResultsPList,
		Results* pGlobalResults)
{
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("VerifyCandidates_HybridSources()");
	DBG_FORCE("LOCAL RESULTS:");
	Hypothesis hyp;
	hyp.stream = "LVCSR_PHN";
	hyp.score  = INF;
	// get record info from the first result hypothesis of the first LocalResult*
	//hyp.channel = (*pLocalResultsPList->begin())->mpResults->begin()->channel;
	//hyp.record = (*pLocalResultsPList->begin())->mpResults->begin()->record;
	hyp.documentId = (*pLocalResultsPList->begin())->mpResults->begin()->documentId;

	for (vector<LocalResults*>::iterator ipLocalResults = pLocalResultsPList->begin();
		ipLocalResults != pLocalResultsPList->end();
		ipLocalResults ++)
	{
		LocalResults* pLocalResults = *ipLocalResults;
		//DBG_FORCE("LOCAL RESULTS.print()"); pLocalResults->mpResults->print();
		DBG_FORCE("LOCAL RESULTS STREAM:"<<pLocalResults->mpResults->begin()->stream);
		Hypothesis::Word word;
		for (QueryKwdList::iterator iKwd = pLocalResults->miKwdBegin;
			iKwd != pLocalResults->miKwdEnd;
			iKwd ++)
		{
			word.str += (word.str.length() ? " " : "") + iKwd->W;
		}

		if (pLocalResults->mpResults->begin()->stream == "PHN")
		{
			for (Hypothesis::Keywords::iterator i = pLocalResults->mpResults->begin()->keywords.begin();
				i != pLocalResults->mpResults->begin()->keywords.end();
				i ++)
			{
				word.pron += (word.pron.length() ? " " : "") + i->str;
			}
		}

		DBG_FORCE("WORD PRON:"<<word.pron);
		word.start 	=  INF;
		word.end 	= -INF;
		word.conf 	= -INF;
		for (ResultsBaseType::iterator iHypothesis = pLocalResults->mpResults->begin();
			iHypothesis != pLocalResults->mpResults->end();
			iHypothesis ++)
		{
			word.start = min(word.start, iHypothesis->keywords.begin()->start);
			word.end = max(word.end, iHypothesis->keywords.rbegin()->end);
			DBG_FORCE("iHypothesis->score:"<<iHypothesis->score);
			word.conf = max(word.conf, iHypothesis->score);
		}
		hyp.score = min(hyp.score, word.conf);
		hyp.push_back_word(word);
		hyp.addKeyword(word);
	}
	pGlobalResults->push_back(hyp);
	//DBG_FORCE("RESULTS.print()"); pGlobalResults->print();
}

bool Search::VerifyCandidates_SingleSource(
		SearchConfig* pConfig,
		QueryKwdList* pQueryKwdList,
		InvIdxRecordsCluster* pCluster,
		Lattice::Indexer* pIndexer,
		Lexicon* pLexicon,
		string* pDataDirPath,
		Results* pResults,
		SourceType::Enum sourceType)
{
	DBG_FORCE("VerifyCandidates_SingleSource()");
	/* time and path data handling {{{ */
	// read matching lattices
	LatMeetingIndexerRecord meetingRec = pIndexer->meetings.getVal(pCluster->mMeetingID); // could be also iRec->meetingID
	if (meetingRec.path == "") 
	{
		CERR("ERROR: lattice with id " << pCluster->mMeetingID << " not found");
		EXIT();
	} 
	DBG("Searching in: " << meetingRec.path);
	DBG("time: " << pCluster->mStartTime << ".." << pCluster->mEndTime);
	LatTime startTime = (pCluster->mStartTime) - pQueryKwdList->time_delta;
	if (startTime < 0) {
		startTime = 0;
	}
	LatTime endTime = (pCluster->mEndTime) + pQueryKwdList->time_delta;
	//		DBG_FORCE("endTime = "<<(iRecord->t)<<" + "<<pQueryKwdList->time_delta);
	if (endTime > meetingRec.length) {
		endTime = -1;
	}

	DBG("startTime:" << startTime << " endTime:" << endTime);
	//					latBinFile.load(meeting, iRecord->position, &centerNodeID);

	string meeting_full_path = *pDataDirPath + PATH_SLASH + meetingRec.path;
//		DBG_FORCE(string2hex(*p_data_dir_path));
//		DBG_FORCE(string2hex(meetingRec.path));
	
	DBG("meeting fullpath: " << meeting_full_path);
	/* }}} */

	/* FILL THE HYPOTHESIS {{{ */
	Hypothesis hyp;
	hyp.stream = SourceType::ToStr(pCluster->GetSourceType());
	hyp.score  = pCluster->GetConfidence();
	// get record's channel
	//hyp.channel = GetMeetingChannelFromMeetingName(meetingRec.path, pConfig);
	// cut off additional info from record's name
	//hyp.record = CutoffMeetingName(meetingRec.path, pConfig);

	//hyp.record = meetingRec.path;
	/* }}} */
	DBG_FORCE("VerifyCandidates_SingleSource() ...Hypothesis:"<<hyp.stream);

/*
	float wipen;
	if (sourceType == SourceType::LVCSR)
	{
		wipen = pConfig->p_wipen;
	}
	else if (sourceType == SourceType::PHN_CLUSTER)
	{
		wipen = pConfig->p_pipen;
	}
	else
	{
		CERR ("ERROR: Unsupported source type");
		EXIT();
	}
*/
	Timer dbgTimer;
	LatBinFile latBinFile;
//	latBinFile.SetWipen(wipen);
	latBinFile.SetIndexer(pIndexer);
	latBinFile.SetLexicon(pLexicon);

	DBG_FORCE("latBinFile.load("<<meeting_full_path<<", "<<startTime<<", "<<endTime<<")");
	dbgTimer.start();
	latBinFile.load(meeting_full_path, startTime, endTime);
	dbgTimer.end();
	DBG_FORCE("TIMER[load lattice]: "<<dbgTimer.val());
	//cumulTime_load += dbgTimer.val();
	DBG("latBinFile.load()...done");

	DBG_FORCE("RescoreHits()");

	DBG_FORCE("QUERY:"<<*pQueryKwdList);
	DBG_FORCE("CLUSTER:"<<*pCluster);

	dbgTimer.start();
	SetBinlat(latBinFile);
	bool found = RescoreHits(pLexicon, pCluster, pQueryKwdList, meetingRec, pConfig, hyp, pResults);
	//pResults->print();
	dbgTimer.end();
	DBG_FORCE("TIMER[multiple hypotheses rescoring]: "<<dbgTimer.val());
//			search.GetNodesContext(iRecord->nodeID, FWD, &hyp);
//			search.GetNodesContext(iRecord->nodeID, BCK, &hyp);
//			dbgTimer.end();
//			DBG_FORCE("TIMER[one hypothesis get context]: "<<dbgTimer.val());
	//cumulTime_getContext += dbgTimer.val();
	DBG_FORCE("RescoreHits()...done...found="<<found);
	//				(*results)[rs].push_back(hyp);

	latBinFile.free(); // free memory
	return found;
}

bool Search::VerifyCandidates_SubQuery_SelectSource(
		SearchConfig* pConfig, 
		QueryKwdList* pQueryKwdList, 
		QueryKwdList::iterator iKwdBegin,
		QueryKwdList::iterator iKwdEnd,
		InvIdxRecordsCluster* pCluster,
		SourceType::Enum sourceType,
		Results* pResults)
{
	DBG_FORCE("VerifyCandidates_SubQuery_SelectSource()");
	DBG_FORCE("sourceType:"<<sourceType);
	DBG_FORCE("CLUSTER:"<<*pCluster);
	Lattice::Indexer* p_indexer = NULL;
	Lexicon* p_lexicon = NULL;
	string* p_data_dir_path = NULL;
	QueryKwdList* p_query_kwd_list = NULL;

	QueryKwdList::iterator iKwdBeginNext = iKwdBegin; iKwdBeginNext++;

	if (sourceType == SourceType::LVCSR)
	{
		if (iKwdBeginNext == iKwdEnd)
		{
			OccurrenceWithSourceList& ocl = (*pCluster)[iKwdBegin->W_id];
			// for each ngram
			for (int i=0; i<ocl.size(); i++)
			{
				LatIndexer::Occurrence* pLvcsrWordOccurrence = ocl.GetOccurrence(i);
				// create a hypothesis and push it to the results
				Hypothesis hyp;
				hyp.stream = "LVCSR";
				hyp.score  = pLvcsrWordOccurrence->GetConfidence();
				// get record's channel
				LatMeetingIndexerRecord meetingRec = pConfig->indexerLvcsr->meetings.getVal(pLvcsrWordOccurrence->mMeetingID); // could be also iRec->meetingID
				hyp.channel = GetMeetingChannelFromMeetingName(meetingRec.path, pConfig);
				// cut off additional info from record's name
				hyp.record = CutoffMeetingName(meetingRec.path, pConfig);
				//hyp.record = meetingRec.path;
				DBG_FORCE("Assigning LVCSR hyp.score="<<hyp.score);

				Hypothesis::Word word;
				word.str 	= pConfig->lexiconLvcsr->id2word_readonly(iKwdBegin->W_id);
				word.start 	= pLvcsrWordOccurrence->GetStartTime();
				word.end 	= pLvcsrWordOccurrence->GetEndTime();
				word.conf 	= pLvcsrWordOccurrence->GetConfidence();
				hyp.push_back_word(word);
				hyp.addKeyword(word);
				pResults->push_back(hyp);
				//DBG_FORCE("RESULTS:"); pResults->print();
				return true;
			}
		}
		else
		{
			p_indexer = pConfig->indexerLvcsr;
			p_lexicon = pConfig->lexiconLvcsr;
			p_data_dir_path = &(pConfig->p_data_dir_path_lvcsr);
			p_query_kwd_list = new QueryKwdList();
			p_query_kwd_list->CopyParams(*pQueryKwdList); // copy query parameters
//			p_query_kwd_list->reserve(pQueryKwdList->size()); // reserve some space for copy operation ( should be exactly (iKwdEnd - iKwdBegin).size() )
//			copy(iKwdBegin, iKwdEnd, p_query_kwd_list->begin()); // copy query words
			for (QueryKwdList::iterator iKwd = iKwdBegin; iKwd != iKwdEnd; iKwd++)
			{
				((vector<QueryKwd>*)p_query_kwd_list)->push_back(*iKwd);
			}
			bool found = VerifyCandidates_SingleSource(pConfig, p_query_kwd_list, pCluster, p_indexer, p_lexicon, p_data_dir_path, pResults, sourceType);
			delete p_query_kwd_list;
			return found;
		}
	}
	else if (sourceType == SourceType::PHN_CLUSTER)
	{
		p_indexer = pConfig->indexerPhn;
		p_lexicon = pConfig->lexiconPhn;
		p_data_dir_path = &(pConfig->p_data_dir_path_phn);

		// if we have to verify only 1 keyword, let's do it
		// but if there are more keywords to be verified in PHN lattice,
		// let's concatenate their phoneme strings and verify them all at once
		if (iKwdBeginNext == iKwdEnd) // if only 1 keyword
		{ 	
			QueryKwdList::iterator iKwd = iKwdBegin;
			OccurrenceWithSourceList& ocl = (*pCluster)[iKwd->W_id];
			// for each ngram
			for (int i=0; i<ocl.size(); i++)
			{
				InvIdxRecordsCluster* pNgramCluster = ((InvIdxRecordsCluster*)ocl.GetOccurrence(i));
				assert(pNgramCluster->mpPronVariant != NULL);
				p_query_kwd_list = pNgramCluster->mpPronVariant->mpQueryKwdListPhn;
				
				// we also need to create another cluster with phonemes
				InvIdxRecordsCluster* pClusterPhonemes = new InvIdxRecordsCluster;
				NgramsCluster2PhnCluster(pNgramCluster, pClusterPhonemes, p_lexicon);

				DBG_FORCE("PHN CLUSTER:"<<*pClusterPhonemes);
				return VerifyCandidates_SingleSource(pConfig, p_query_kwd_list, pClusterPhonemes, p_indexer, p_lexicon, p_data_dir_path, pResults, sourceType);
			}
		}
		else // if more keywords
		{	
			p_query_kwd_list = new QueryKwdList();
			InvIdxRecordsCluster* pClusterPhonemes = new InvIdxRecordsCluster;
			// for each keyword (cluster of overlapping ngrams)
			for (QueryKwdList::iterator iKwd = iKwdBegin; iKwd != iKwdEnd; iKwd++)
			{
				OccurrenceWithSourceList& ocl = (*pCluster)[iKwd->W_id];
				// for each ngram
				for (int i=0; i<ocl.size(); i++)
				{
					InvIdxRecordsCluster* pNgramCluster = ((InvIdxRecordsCluster*)ocl.GetOccurrence(i));
					DBG("pNgramCluster:"<<*pNgramCluster);
					DBG("pNgramCluster->mpPronVariant:"); //pNgramCluster->mpPronVariant->mpQueryKwdListPhn->print();
					assert(pNgramCluster->mpPronVariant != NULL);
					if (i == 0)
					{
						int prev_size = p_query_kwd_list->size();
						if (iKwd == iKwdBegin)
						{
							p_query_kwd_list->CopyParams(*(pNgramCluster->mpPronVariant->mpQueryKwdListPhn));
						}
						else
						{
							(*p_query_kwd_list)[prev_size-1].isNextInPhrase = true;
							(*p_query_kwd_list)[prev_size-1].nb_next = pConfig->p_phrase_kwd_neiborhood;
						}
						p_query_kwd_list->AddWords(pNgramCluster->mpPronVariant->mpQueryKwdListPhn);
						if (iKwd != iKwdBegin)
						{
							DBG("iKwd != iKwdBegin");
							//DBG_FORCE("iKwdLast_inPrevQueryKwdList:"<<(++iKwdLast_inPrevQueryKwdList)->W_id);
							(*p_query_kwd_list)[prev_size].nb_prev = pConfig->p_phrase_kwd_neiborhood;
						}
					}
					NgramsCluster2PhnCluster(pNgramCluster, pClusterPhonemes, p_lexicon);
				}
			}
			DBG_FORCE("PHN CLUSTER:"<<*pClusterPhonemes);
			DBG_FORCE("PHN QUERY:"<<*p_query_kwd_list);
			bool found = VerifyCandidates_SingleSource(pConfig, p_query_kwd_list, pClusterPhonemes, p_indexer, p_lexicon, p_data_dir_path, pResults, sourceType);
			delete p_query_kwd_list;
			return found;
		} // if more than 1 keyword
	}
	else
	{
		CERR("ERROR: Source type "<<sourceType<<" is not implemented in Search::VerifyCandidates_SubQuery_SelectSource()");
		EXIT();
	}
	return false;
}


/* VerifyCandidates() {{{ */
void Search::VerifyCandidates(
		SearchConfig *pConfig,
		QueryKwdList *pQueryKwdList,
		Results *pResults)
{
	for(InvIdxRecordsClusters::iterator iCluster = pQueryKwdList->mInvIdxRecordsClusters.begin();
		iCluster != pQueryKwdList->mInvIdxRecordsClusters.end();
		iCluster ++)
	{
		InvIdxRecordsCluster* pCluster = &(*iCluster);
		Hypothesis hyp;

		DBG_FORCE("VERIFYING CLUSTER:"<<*pCluster);

		// for each word in the query
		QueryKwdList::iterator iKwdPrev = pQueryKwdList->begin();
		SourceType::Enum previous_source_type = (*pCluster)[iKwdPrev->W_id].GetSourceType(0);

		vector<LocalResults*> local_results_plist;
		bool hybrid = false;
		bool found = false;
		for (QueryKwdList::iterator iKwd = pQueryKwdList->begin();
			iKwd != pQueryKwdList->end();
			iKwd ++)
		{
			OccurrenceWithSourceList& osl = (*pCluster)[iKwd->W_id];

			SourceType::Enum current_source_type = osl.GetSourceType(0);
			if (previous_source_type != current_source_type)
			{
				DBG_FORCE("HYBRID: SourceType="<<previous_source_type);
				hybrid = true;
				LocalResults* p_local_results = new LocalResults;
				p_local_results->mpResults = new Results;
				p_local_results->mpQueryKwdList = pQueryKwdList;
				p_local_results->miKwdBegin = iKwdPrev;
				p_local_results->miKwdEnd = iKwd;
				found = VerifyCandidates_SubQuery_SelectSource(pConfig, pQueryKwdList, iKwdPrev, iKwd, pCluster, previous_source_type, p_local_results->mpResults);
				if (!found) {
					break;
				}
				local_results_plist.push_back(p_local_results);
				previous_source_type = current_source_type;
				iKwdPrev = iKwd;
				//DBG_FORCE("RESULTS:"); p_local_results->mpResults->print();
			}
		}
		if (hybrid)
		{
			if (found)
			{
				LocalResults* p_local_results = new LocalResults;
				p_local_results->mpResults = new Results;
				p_local_results->mpQueryKwdList = pQueryKwdList;
				p_local_results->miKwdBegin = iKwdPrev;
				p_local_results->miKwdEnd = pQueryKwdList->end();
				found = VerifyCandidates_SubQuery_SelectSource(pConfig, pQueryKwdList, iKwdPrev, pQueryKwdList->end(), pCluster, previous_source_type, p_local_results->mpResults);
				if (found)
				{
					local_results_plist.push_back(p_local_results);
					//DBG_FORCE("RESULTS:"); p_local_results->mpResults->print();
		//			DBG_FORCE("Local Results:"); local_results.print();
					VerifyCandidates_HybridSources(pConfig, pQueryKwdList, pCluster, &local_results_plist, pResults);
					//DBG_FORCE("Global Results:"); pResults->print();
				}
			}
		}
		else
		{
			DBG_FORCE("HYBRID = FALSE");
			found = VerifyCandidates_SubQuery_SelectSource(pConfig, pQueryKwdList, pQueryKwdList->begin(), pQueryKwdList->end(), pCluster, previous_source_type, pResults);
			//DBG_FORCE("Results:"); pResults->print();
		}
		if (!found)
		{
			DBG_FORCE("VerifyCandidates() Cluster not found in lattice");
		}
	}
}
/* }}} */
#endif


void Search::InvIdxRecordsClusters2Results(
		QueryKwdList *pQueryKwdList,
		Results *pResults)
{
	for(InvIdxRecordsClusters::iterator iCluster = pQueryKwdList->mInvIdxRecordsClusters.begin();
		iCluster != pQueryKwdList->mInvIdxRecordsClusters.end();
		iCluster ++)
	{
		InvIdxRecordsCluster* pCluster = &(*iCluster);
		Hypothesis hyp;
		hyp.stream = SourceType::ToStr(pCluster->GetSourceType());

		//LatMeetingIndexerRecord meetingRec = pConfig->GetMeetingIndexer()->getVal(pCluster->mMeetingID); // could be also iRec->meetingID
		//hyp.channel = GetMeetingChannelFromMeetingName(meetingRec.path, pConfig);
		//hyp.record = CutoffMeetingName(meetingRec.path, pConfig);
		hyp.documentId = pCluster->mMeetingID;
		hyp.score = pCluster->mConf;

		for (QueryKwdList::iterator iKwd = pQueryKwdList->begin();
			iKwd != pQueryKwdList->end();
			iKwd ++)
		{
			OccurrenceWithSourceList* pOsl = &((*pCluster)[iKwd->W_id]);

			Hypothesis::Word word;
			word.str 	= iKwd->W;
			word.start 	= pOsl->mStartTime;
			word.end 	= pOsl->mEndTime;
			word.conf 	= pOsl->mConf;
			if (iKwd->W_id < 0)
			{
				for (QueryKwdList::iterator iPhn = iKwd->mPronList[0].mpQueryKwdListPhn->begin();
					iPhn != iKwd->mPronList[0].mpQueryKwdListPhn->end();
					iPhn ++)
				{
					word.pron += (word.pron.length() ? " " : "") + iPhn->W;
				}
			}
			hyp.push_back_word(word);
			hyp.addKeyword(word);
		}
		pResults->push_back(hyp);
	}
}

#if 0
/* VerifyCandidates_Singleword() {{{ */	
void Search::VerifyCandidates_Singleword(
		SearchConfig *pConfig,
		QueryKwdList *pQueryKwdList,
		Results *pResults)
{
	int results_counter = 0;
	Timer dbgTimer;
	QueryKwdList::iterator iQueryKwd = pQueryKwdList->begin();

	DBG("Processing kwd: "<< (iQueryKwd->W));

	/* create cluster cursors {{{ */
	// one cursor for LVCSR + one cursor for each PHN pronounciation variant
	int clusters_cursors_size = 1 + iQueryKwd->mPronList.size();
	InvIdxRecordsClustersCursor clusters_cursors[clusters_cursors_size];
	// for LVCSR
	clusters_cursors[0].SetInvIdxRecordsClusters(pQueryKwdList->mInvIdxRecordsClusters);
	// for each PHN pronounciation variant
	for (int i = 0; i < (int)iQueryKwd->mPronList.size(); i++)
	{
		clusters_cursors[1+i].SetInvIdxRecordsClusters(iQueryKwd->mPronList[i].mpQueryKwdListNgram->mInvIdxRecordsClusters);
	}
	/* }}} */

	bool verifying = true;
	while(verifying)
	{
		Lattice::Indexer* p_indexer = NULL;
		Lexicon* p_lexicon = NULL;
		string* p_data_dir_path = NULL;
		QueryKwdList* p_query_kwd_list = NULL;
		/* select the best cluster {{{ */
		// let's move to the first PHN cluster (in each pronounciation variant) 
		// which is not in the LVCSR clusters list
		//
		// for each pronounciation variant
		for (int i=1; i<clusters_cursors_size; i++)
		{
			DBG("clusters_cursors:"<<i);
			DBG("Eol():"<<clusters_cursors[i].Eol());
			DBG("FindCluster():"<<clusters_cursors[0].GetClustersList()->FindCluster(clusters_cursors[i].Get()));
			// while there is such occurrence in LVCSR, move the cursor to another PHN cluster
			while (
					!clusters_cursors[i].Eol()
					&&
					clusters_cursors[0].GetClustersList()->FindCluster(clusters_cursors[i].Get()) != NULL
			      )
			{
				DBG_FORCE("Omitting PHN cluster: "<<*clusters_cursors[i].Get());
				clusters_cursors[i].Next();
			}
		}
		// actually first we need to select the first non-empty cursor
		int best_cluster_idx = 0;
		while (clusters_cursors[best_cluster_idx].Eol())
		{
			best_cluster_idx++;
			if (best_cluster_idx >= clusters_cursors_size)
			{
				verifying = false;
				break;
			}
		}
		if (!verifying)
		{
			break;
		}

		// now select the best cursor
		int i = 0;
		while (i<clusters_cursors_size)
		{
			// jump over empty cursors
			while (i==best_cluster_idx || clusters_cursors[i].Eol())
			{
				i++;
			}
			// just check whether we are still within the corect cursor array range
			if (i >= clusters_cursors_size)
			{
				break;
			}

			DBG_FORCE("best_cluster_idx:"<<best_cluster_idx<<" i:"<<i);
			DBG_FORCE("cursor["<<i<<"]: "<<clusters_cursors[i].Get()->GetConfidence()<< " ... cursor["<<best_cluster_idx<<"]: "<<clusters_cursors[best_cluster_idx].Get()->GetConfidence());
			if (clusters_cursors[i].Get()->GetConfidence() > clusters_cursors[best_cluster_idx].Get()->GetConfidence())
			{
				best_cluster_idx = i;
			}
			i++;
		}

		Hypothesis hyp;

		if (best_cluster_idx == 0)
		{
			DBG_FORCE("LVCSR Cluster");
			hyp.stream = "LVCSR";
			p_indexer = pConfig->indexerLvcsr;
			p_lexicon = pConfig->lexiconLvcsr;
			p_data_dir_path = &(pConfig->p_data_dir_path_lvcsr);
			p_query_kwd_list = pQueryKwdList;
		}
		else
		{
			DBG_FORCE("PHN Cluster");
			hyp.stream = "PHN";
			p_indexer = pConfig->indexerPhn;
			p_lexicon = pConfig->lexiconPhn;
			p_data_dir_path = &(pConfig->p_data_dir_path_phn);
			p_query_kwd_list = iQueryKwd->mPronList[best_cluster_idx-1].mpQueryKwdListPhn;
		}
		InvIdxRecordsCluster* pCluster = clusters_cursors[best_cluster_idx].Get();
		clusters_cursors[best_cluster_idx].Next();
		/* }}} */

		DBG("--------------------------------------------------------------------------------");
		DBG("  Cluster: "<<*pCluster);
		DBG("--------------------------------------------------------------------------------");

		/* time and path data handling {{{ */
		// read matching lattices
		LatMeetingIndexerRecord meetingRec = p_indexer->meetings.getVal(pCluster->mMeetingID); // could be also iRec->meetingID
		if (meetingRec.path == "") 
		{
			CERR("ERROR: lattice with id " << pCluster->mMeetingID << " not found");
			EXIT();
		} 
		DBG("Searching in: " << meetingRec.path);
		DBG("time: " << pCluster->mStartTime << ".." << pCluster->mEndTime);
		LatTime startTime = (pCluster->mStartTime) - pQueryKwdList->time_delta;
		if (startTime < 0) {
			startTime = 0;
		}
		LatTime endTime = (pCluster->mEndTime) + pQueryKwdList->time_delta;
		//		DBG_FORCE("endTime = "<<(iRecord->t)<<" + "<<pQueryKwdList->time_delta);
		if (endTime > meetingRec.length) {
			endTime = -1;
		}

		DBG("startTime:" << startTime << " endTime:" << endTime);
		//					latBinFile.load(meeting, iRecord->position, &centerNodeID);

		string meeting_full_path = *p_data_dir_path + PATH_SLASH + meetingRec.path;
//		DBG_FORCE(string2hex(*p_data_dir_path));
//		DBG_FORCE(string2hex(meetingRec.path));
		
		DBG("meeting fullpath: " << meeting_full_path);
		/* }}} */

		/* FILL THE HYPOTHESIS {{{ */
		hyp.score  = pCluster->GetConfidence();
		// get record's channel
		hyp.channel = GetMeetingChannelFromMeetingName(meetingRec.path, pConfig);
		// cut off additional info from record's name
		hyp.record = CutoffMeetingName(meetingRec.path, pConfig);
		//hyp.record = meetingRec.path;
		/* }}} */

		// if we have only one keyword in the cluster and we do not need to get a forward or backward context, then it is not needed to load the lattice
		if (pCluster->size() == 1 && pQueryKwdList->fwd_nodes_count == 0 && pQueryKwdList->bck_nodes_count == 0)
		{
			Hypothesis::Word word;
			word.str 	= p_lexicon->id2word_readonly(pCluster->begin()->first);
			word.start 	= pCluster->GetStartTime();
			word.end 	= pCluster->GetEndTime();
			word.conf 	= pCluster->GetConfidence();
			hyp.push_back_word(word);
			hyp.addKeyword(word);
		}
		else
		// if there are more keywords in the cluster, we need to load the lattice 
		// and verify that the given query is really there 
		// (if there is a path in the lattice satisfying the query). 
		//
		// if we want to get some context, it is also needed to traverse 
		// through the best path in forward and backward direction 
		{
			// if we have ngrams in the selected cluster, we need to expand them 
			// to unigrams to be able to find them in the lattice 
			// (since there are unigrams on nodes in lattices)
			//
			// we also need to create another iGroup with phonemes
			InvIdxRecordsCluster* pClusterPhonemes = new InvIdxRecordsCluster;
			// for each item in the group (couple iKwd -> ClustersList)
			// for LVCSR single-word-query there is only 1 item
			// for PHN single-word-query there might be more phoneme ngrams
			for (InvIdxRecordsCluster::iterator iClusterItem = pCluster->begin(); 
				 iClusterItem != pCluster->end(); 
				 iClusterItem ++)
			{
				ID_t							kwd_W_id		=   iClusterItem->first;
				LatIndexer::OccurrencePList*	pInvRecList 	= &(iClusterItem->second);

				vector<string> unigrams;
				string_split(p_lexicon->id2word_readonly(kwd_W_id), "_", unigrams);

				// for each unigram in current iKwd
				for (vector<string>::iterator iUnigram = unigrams.begin(); iUnigram != unigrams.end(); ++iUnigram)
				{
					ID_t unigram_W_id			= p_lexicon->word2id_readonly(*iUnigram);

					// for each record in the current keyword's inversed index record list
					for (QueryKwd::InvIdxRecordList::iterator ipInvIdxRecord = pInvRecList->begin(); 
						 ipInvIdxRecord != pInvRecList->end(); 
						 ipInvIdxRecord ++)
					{
						QueryKwd::InvIdxRecord* pNgramRecord = *ipInvIdxRecord;
						QueryKwd::InvIdxRecord* pNewRecord;
						pNewRecord				= new QueryKwd::InvIdxRecord;
						pNewRecord->mWordID 	= unigram_W_id;
						pNewRecord->mMeetingID 	= pNgramRecord->mMeetingID;
						pNewRecord->mStartTime	= pNgramRecord->mStartTime;
						pNewRecord->mEndTime	= pNgramRecord->mEndTime;
						pNewRecord->mConf		= pNgramRecord->mConf; // confidence is actually not relevant in the verification process, because it is recomputed there

						pClusterPhonemes->Insert(unigram_W_id, pNewRecord);

						DBG_FORCE("pClusterPhonemes->Insert("<<unigram_W_id<<", "<<*pNewRecord<<")");
						//							DBG_FORCE("phn: "<<*iUnigram<<" "<<*pNewRecord);
					}
				}
			}
			pCluster = pClusterPhonemes;
			DBG_FORCE("Search cluster: "<<*pCluster);

			SetQueryKwdList(p_query_kwd_list);







			LatBinFile latBinFile;

			DBG_FORCE("latBinFile.load("<<meeting_full_path<<", "<<startTime<<", "<<endTime<<")");
			dbgTimer.start();
			latBinFile.load(meeting_full_path, startTime, endTime);
			dbgTimer.end();
			DBG_FORCE("TIMER[load lattice]: "<<dbgTimer.val());
			//cumulTime_load += dbgTimer.val();
			DBG("latBinFile.load()...done");

			DBG_FORCE("RescoreHits()");
			dbgTimer.start();
			SetBinlat(latBinFile);
			RescoreHits(p_lexicon, pCluster, p_query_kwd_list, meetingRec, pConfig, hyp, pResults);
			//pResults->print();
			dbgTimer.end();
			DBG_FORCE("TIMER[multiple hypotheses rescoring]: "<<dbgTimer.val());
//			search.GetNodesContext(iRecord->nodeID, FWD, &hyp);
//			search.GetNodesContext(iRecord->nodeID, BCK, &hyp);
//			dbgTimer.end();
//			DBG_FORCE("TIMER[one hypothesis get context]: "<<dbgTimer.val());
			//cumulTime_getContext += dbgTimer.val();
			DBG_FORCE("RescoreHits()...done");
			//				(*results)[rs].push_back(hyp);

			latBinFile.free(); // free memory
		}

		pResults->push_back(hyp);

		if(++results_counter >= pQueryKwdList->max_results) {
			DBG_FORCE("BREAK: results_counter="<<results_counter);
			break; // if the phrase does match, goto stop processing the current cluster and go to the next one
		}

	} // while (verifying)
}
/* }}} */
#endif


/* VerifyCandidates_Multiword() {{{ */
/*
void Search::VerifyCandidates_Multiword(Results *pResults)
{

	// if there is more than 1 kwd in the query 
	// or if we have ngrams in the reverse index and phonemes in lattices
	// we have to do the forward pass

	search.FillInvIdxRecordsClusters();
	//	DBG_FORCE("NOT SORTED CLUSTER GROUPS:");
	//	search.printClusterGroups();

	search.SortClusterGroups();
	DBG_FORCE("SORTED CLUSTER GROUPS:");
	search.PrintClusterGroups();

	int results_counter = 0;
	//--------------------------------------------------------------------------------
	// CONFIDENCE REESTIMATION
	//--------------------------------------------------------------------------------

	// for each group
	for (InvIdxRecordsClusters::iterator iGroup=search.clusterGroups.begin(); iGroup!=search.clusterGroups.end(); ++iGroup)
	{
		DBG_FORCE("--------------------------------------------------------------------------------");

		DBG_FORCE("EM="<<queryKwdList->mExactMatch);
		DBG_FORCE("validKeywordsCount: "<<iGroup->validKeywordsCount);
		DBG_FORCE("queryKwdListNgrams.GetUniqSize()"<<queryKwdListNgrams.GetUniqSize());
		DBG_FORCE("iGroup:"<<endl<<*iGroup);

		if (queryKwdList->mExactMatch && iGroup->validKeywordsCount < (int)queryKwdListNgrams.GetUniqSize()) {
			DBG_FORCE("EM=true && "<<iGroup->validKeywordsCount<<" < "<<(int)queryKwdListNgrams.GetUniqSize());
			continue;
		}

		// iBestKwd is the keyword with the least number of hits in the reverse index
		// in each group, the iBestKwd has only 1 cluster in the cluster list
		QueryKwd::InvIdxRecordList::iterator iInvRecord = (*iGroup)[search.clusterGroups.iBestKwd->W_id].begin();
		QueryKwd::InvIdxRecord *pInvRecord = *iInvRecord;
		//			QueryKwd::InvIdxRecord::iterator pInvRecord = (*iInvIdxRecordList) -> begin();
		LatMeetingIndexerRecord meetingRec = indexer->meetings.getVal(pInvRecord->meetingID); // could be also iCluster->meetingID
		if (meetingRec.path == "") {
			CERR("ERROR: lattice with id " << pInvRecord->meetingID << " not found ... jumping over this hypotheses cluster");
			exit(1);
		} 
		//		DBG("Searching in: " << meetingRec.path);
		//		DBG("time: " << pInvRecord->t);
		LatTime startTime = (pInvRecord->t) - queryKwdList->time_delta;
		if (startTime < 0) {
			startTime = 0;
		}
		LatTime endTime = (pInvRecord->t) + queryKwdList->time_delta;
		//		DBG_FORCE("endTime = "<<(pInvRecord->t)<<" + "<<queryKwdList->time_delta);
		if (endTime > meetingRec.length) {
			endTime = -1;
		}
		//		DBG("startTime:" << startTime << " endTime:" << endTime);

		if (p_data_dir_path[p_data_dir_path.length()-1] != PATH_SLASH && meetingRec.path[0] != PATH_SLASH)
			p_data_dir_path += PATH_SLASH;

		string meeting_full_path = p_data_dir_path + meetingRec.path;
		//		DBG("meeting fullpath: " << meeting_full_path);






		//		DBG_FORCE("latBinFile.nodes.firstID:"<<latBinFile.nodes.firstID);


		DBG("Searching in: " << meetingRec.path);

		InvIdxRecordsCluster* pGroup = &(*iGroup);

		if (queryKwdList->mPhn2Ngram)
		{
			// we also need to create another iGroup with phonemes
			InvIdxRecordsCluster* pGroupPhonemes = new InvIdxRecordsCluster;
			// for each item in the group (couple iKwd -> ClustersList)
			for (InvIdxRecordsCluster::iterator iGroupItem = pGroup->begin(); iGroupItem != pGroup->end(); ++iGroupItem)
			{
				ID_t						kwd_W_id		=   iGroupItem->first;
				QueryKwd::InvIdxRecordList*	pInvRecList 	= &(iGroupItem->second);
				// for each cluster in current ClustersList
				for (QueryKwd::InvIdxRecordList::iterator ipInvIdxRecord = pInvRecList->begin(); ipInvIdxRecord != pInvRecList->end(); ++ipInvIdxRecord)
				{
					QueryKwd::InvIdxRecord* pNgramRecord = *ipInvIdxRecord;
					// for each phoneme in current iKwd
					vector<string> phonemes;
					string_split(lexicon->id2word_readonly(kwd_W_id), "_", phonemes);

					for (vector<string>::iterator iPhn = phonemes.begin(); iPhn != phonemes.end(); ++iPhn)
					{
						ID_t phn_W_id			= lexicon->word2id_readonly(*iPhn);

						QueryKwd::InvIdxRecord* pNewRecord;
						pNewRecord				= new QueryKwd::InvIdxRecord;
						pNewRecord->wordID 		= phn_W_id;
						pNewRecord->meetingID  	= pNgramRecord->meetingID;
						pNewRecord->tStart		= pNgramRecord->tStart;
						pNewRecord->t			= pNgramRecord->t;
						pNewRecord->conf		= pNgramRecord->conf;

						pGroupPhonemes->Insert(phn_W_id, pNewRecord);

						DBG_FORCE("pGroupPhonemes->Insert("<<phn_W_id<<", "<<*pNewRecord<<")");
						//							DBG_FORCE("phn: "<<*iPhn<<" "<<*pNewRecord);
					}
				}
			}
			pGroup = pGroupPhonemes;
			DBG_FORCE("Search cluster group: "<<*pGroup);

			search.SetQueryKwdList(queryKwdListPhonemes);
		}

		if (p_only_print_candidates)
		{
			search.PrintCandidates(pGroup, meetingRec);
		}
		else if (p_only_print_candidate_trigrams)
		{
			search.PrintCandidateTrigrams(&(*iGroup), meetingRec);
		}
		else
		{
			DBG_FORCE("latBinFile.load("<<meeting_full_path<<", "<<startTime<<", "<<endTime<<")");
			dbgTimer.start();
			latBinFile.load(meeting_full_path, startTime, endTime);
			dbgTimer.end();
			DBG_FORCE("TIMER[load-lattice]: "<<dbgTimer.val());
			cumulTime_load += dbgTimer.val();
			DBG_FORCE("latBinFile.load()...done");			

			DBG_FORCE("search.rescoreHits()");
			dbgTimer.start();
			search.SetBinlat(latBinFile);
			search.RescoreHits(pGroup, meetingRec, pResults);
			//				results->print();
			dbgTimer.end();
			DBG_FORCE("TIMER[multiple hypotheses rescoring]: "<<dbgTimer.val());
			cumulTime_getContext += dbgTimer.val();
			DBG_FORCE("search.rescoreHits()...done");

			latBinFile.free(); // free memory
		}

		if (queryKwdList->mPhn2Ngram)
		{
			search.SetQueryKwdList(&queryKwdListNgrams);

			delete pGroup;
		}
		if(++results_counter >= queryKwdList->max_results) {
			break; // if the phrase does match, goto stop processing the current cluster and go to the next one
		}

	}

	DBG_FORCE("TIMER[cumulTime_load]: " << cumulTime_load << " s");
	DBG_FORCE("TIMER[cumulTime_getContext]: " << cumulTime_getContext << " s");
	latBinFile.stat();
}
*/
/* }}} */	


