#include "InvIdxRecordsClusters.h"

using namespace lse;
using namespace std;

/**
  @brief comparison function for sorting cluster groups (sorted first by confidence and then by the number of valid keywords)

*/
bool InvIdxRecordsClusters::cmp_SearchClusterGroup(const InvIdxRecordsCluster& lv, const InvIdxRecordsCluster& rv)
{
	if (lv.validKeywordsCount == rv.validKeywordsCount)
	{
		return lv.conf > rv.conf;
	} else {
		return lv.validKeywordsCount > rv.validKeywordsCount;
	}
}


//bool InvIdxRecordsClusters::cmp_PCluster(const LatIndexer::PCluster &lv, const LatIndexer::PCluster &rv)
bool InvIdxRecordsClusters::cmp_PRevIdxRecord(const QueryKwd::RevIdxRecord* lv, const QueryKwd::RevIdxRecord* rv)
{
	return lv->conf > rv->conf;
}


void InvIdxRecordsClusters::sortGroups() 
{
	// for all groups
	for (InvIdxRecordsClusters::iterator iGroup=this->begin(); iGroup!=this->end(); ++iGroup)
	{
		// first set the init value for minKwdConf:
		// group -> first keyword in map -> it's cluster list -> first cluster (it is pointer, so we need to do *... ->conf) ;o)
		InvIdxRecordsCluster::iterator iKwd2RevIdxRecordList = iGroup->begin();
//		QueryKwd::RevIdxRecordList clustersList = iGroup->begin()->second;
//		QueryKwd::RevIdxRecordList::iterator iCluster = iGroup->begin()->second.begin();
		TLatViterbiLikelihood minKwdConf = (*( iGroup->begin()->second.begin() ))->conf;
		// for all keywords (in current group)
		iGroup->validKeywordsCount = iGroup->size();
		for (InvIdxRecordsCluster::iterator iKwd2RevIdxRecordList=iGroup->begin(); iKwd2RevIdxRecordList!=iGroup->end(); ++iKwd2RevIdxRecordList)
		{
			sort( iKwd2RevIdxRecordList->second.begin(), iKwd2RevIdxRecordList->second.end(), cmp_PRevIdxRecord );
			// select the minimal confidence for the current group (of all keywords in group)
			if (minKwdConf > (*(iKwd2RevIdxRecordList->second.begin()))->conf ) {
				minKwdConf = (*(iKwd2RevIdxRecordList->second.begin()))->conf;
			}
		}
		iGroup->conf = minKwdConf;
	}
	
	sort( this->begin(), this->end(), cmp_SearchClusterGroup );
}


void InvIdxRecordsCluster::Insert(ID_t W_id, QueryKwd::RevIdxRecord* pRevIdxRecord)
{
	// select min confidence of all keyword's clusters
//	if (this->conf > pRevIdxRecord->conf) {
//		this->conf = pRevIdxRecord->conf;
//	}
	// add the given record to the clusters of the given keyword in the current group
	(*this)[W_id].push_back(pRevIdxRecord);
	
	// if the meetingID is not set yet, then assign the pRevIdxRecord->meetingID to it
	if (mMeetingID == -1)
	{
		mMeetingID = pRevIdxRecord->meetingID;
	}
	
	// set the end time of the group
	if (mEndTime < pRevIdxRecord->t)
	{
		mEndTime = pRevIdxRecord->t;
	}

	// set the start time of the group is associated
	if (mStartTime > pRevIdxRecord->tStart)
	{
		mStartTime = pRevIdxRecord->tStart;
	}

}

bool InvIdxRecordsCluster::containsCluster(QueryKwdList::iterator iKwd, const QueryKwd::RevIdxRecord &cmpRevIdxRecord)
{
	// compare with all clusters of the given keyword in the group
	for (QueryKwd::RevIdxRecordList::iterator iRevRec=(*this)[iKwd->W_id].begin(); iRevRec!=(*this)[iKwd->W_id].end(); ++iRevRec)
	{
		if ((**iRevRec) == cmpRevIdxRecord) // iRevRec is a pointer to iterator (therefore **iRevRec)
		{
			return true;
		}
	}
	return false;
}



std::ostream& operator<<(std::ostream& os, const InvIdxRecordsCluster& g) 
{
	os << "meetingID:"<<g.mMeetingID<<" conf:"<<g.conf<<" kwds:"<<g.validKeywordsCount<<" t["<<g.mStartTime<<" .. "<<g.mEndTime<<"]" << std::endl;

	// each keyword (group[keyword])
	for (InvIdxRecordsCluster::const_iterator iGroupItem=g.begin(); iGroupItem!=g.end(); ++iGroupItem)
	{
		os << "  kwd: " << iGroupItem->first << std::endl;
		// each cluster (group[keyword]->clusters)
		for (QueryKwd::RevIdxRecordList::const_iterator iRevRec=iGroupItem->second.begin(); iRevRec!=iGroupItem->second.end(); ++iRevRec)
		{
			os << "     [" << (*iRevRec)->meetingID << "] " 
				<< (*iRevRec)->tStart << " ... " << (*iRevRec)->t
				<< " (conf:" << (*iRevRec)->conf << ")"
				<< std::endl;
		}
	}
	return os;
}
