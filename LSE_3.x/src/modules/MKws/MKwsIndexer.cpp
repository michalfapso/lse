#include "MKwsIndexer.h"
#include "MKws.h"
#include "liblse/latindexer.h"

#define MKWSINDEXER_LOAD_ARG(t,id,def) t id = (t)(mpMKws->GetConfig()->Value("MKws_indexer", #id, def))

using namespace std;
using namespace sem;
using namespace lse;

const std::string MKwsIndexer::mExt_dot = ".dot";
const std::string MKwsIndexer::mExt_bin = ".binlat";
const std::string MKwsIndexer::mExt_copy = ".lat.copy";
const std::string MKwsIndexer::mExt_htk = ".lat";
const std::string MKwsIndexer::mExt_alphabetavectors = ".abvec";

// forward declarations {{{
bool MKwsIndexer_JumpOverCallback(void* ptr, ID_t wordId);
//void MKwsIndexer_AddToFwdIndex(void* pPtr, LatIndexer::Record &rec);
// }}}

void MKwsIndexer::InitGlobal(const std::string &invIdxPath)
{
	MIndexerStruct::InitGlobal();
	if (mpM->GetAppType() == ApplicationType::EIndexingServer ||
		mpM->GetAppType() == ApplicationType::ESearchingServer ||
		mpM->GetAppType() == ApplicationType::ESorter)
	{
		DBG("mpInvLabelsContainer->SetPath(\""<< invIdxPath <<"\")");
		if (!boost::filesystem::exists(invIdxPath))
		{
			CERR("ERROR: Index path '"<<invIdxPath<<"' does not exist");
			EXIT();
		}
		assert(mpInvLabelsContainer);
		mpInvLabelsContainer->SetPath(invIdxPath);
		//mpInvLabelsContainer->SetPath(mpMKws->GetConfig()->Value("MKws_indexer", "idx_dir") + suffix);
	}
}

void MKwsIndexer::OutputLatticeStatistics(std::ostream &os, const Lattice &lat)
{
	os << "AllNodes: " << lat.nodes.size();
	unsigned int count = 0;
	for (Lattice::NodeMap::const_iterator i = lat.nodes.begin(); i!=lat.nodes.end(); i++)
	{
		if (i->second.W == "!NULL")
		{
			count++;
		}
	}
	os << "NullNodes: " << count;
	os << "Links: " << lat.links.size();
}

void MKwsIndexer::Index(ID docId, const std::string &filename)
{
	MIndexerStruct::Index(docId, filename);
	DBG("MKwsIndexer::Index("<<docId<<", "<<filename<<")");

	Lattice globalLat(mpDictionary); // concatenation of lattices given by application arguments
	LatBinFile latBinFile;
	LatBinFile latBinFileConcat;
	globalLat.N = 1;
	globalLat.L = 0;
	Lattice::Node firstNode;
	firstNode.id=0;
	firstNode.t=0.0;
	firstNode.W="!NULL";
	firstNode.v=0;
	firstNode.bestLikelihood=0.0;
	globalLat.addNode(firstNode); // add empty node

	//application parameters
	//MKWSINDEXER_LOAD_ARG(bool, p_read_binary, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_write_binary, false);
	MKWSINDEXER_LOAD_ARG(bool, p_write_binary_gzipped, false);
	MKWSINDEXER_LOAD_ARG(bool, p_write_dot, false);
	MKWSINDEXER_LOAD_ARG(bool, p_write_htk, false);
	MKWSINDEXER_LOAD_ARG(bool, p_sort_lattice, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_compute_fwbw, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_make_one_end_node, false);
	MKWSINDEXER_LOAD_ARG(string, p_lattice_dir_out, ".");
	MKWSINDEXER_LOAD_ARG(string, p_lexicon_out, "");
	MKWSINDEXER_LOAD_ARG(string, p_lexicon_in, "");
	MKWSINDEXER_LOAD_ARG(string, p_concatlattice_out, "");
//	MKWSINDEXER_LOAD_ARG(string, p_search_index, "");
	MKWSINDEXER_LOAD_ARG(string, p_search_indexer_in, "");
	MKWSINDEXER_LOAD_ARG(string, p_data_dir_prefix, "");
	MKWSINDEXER_LOAD_ARG(double, p_wi_penalty, 0.0);
	MKWSINDEXER_LOAD_ARG(string, p_mlf_out, "");
	MKWSINDEXER_LOAD_ARG(string, p_mlf_kwdlist, "");
	MKWSINDEXER_LOAD_ARG(double, p_mlf_likelihood_treshold, -1); // no treshold is set
	MKWSINDEXER_LOAD_ARG(double, p_lmscale, 1);
	MKWSINDEXER_LOAD_ARG(double, p_amscale, 1);
	MKWSINDEXER_LOAD_ARG(double, p_amscale_kwd, 0.0);
	MKWSINDEXER_LOAD_ARG(double, p_lmscale_kwd, 0.0);
	MKWSINDEXER_LOAD_ARG(bool, p_viterbi_baumwelsh, false);
	MKWSINDEXER_LOAD_ARG(bool, p_compute_links_likelihood, false);
	MKWSINDEXER_LOAD_ARG(double, p_logAdd_treshold, 7);
	//MKWSINDEXER_LOAD_ARG(double, p_trim_t_start, 0);
	//MKWSINDEXER_LOAD_ARG(double, p_trim_t_end, 0);
	MKWSINDEXER_LOAD_ARG(bool, p_compute_overlapped_words_likelihood, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_likelihood_no_scale_on_keyword, false);
	MKWSINDEXER_LOAD_ARG(bool, p_check_node_sorting, false);
	//MKWSINDEXER_LOAD_ARG(int, p_debug_level, 0);
	MKWSINDEXER_LOAD_ARG(bool, p_htk_posteriors_output, false);
	MKWSINDEXER_LOAD_ARG(bool, p_read_time_from_filename, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_bin2txt, false);
	MKWSINDEXER_LOAD_ARG(string, p_generate_lexicon_index, "");
	MKWSINDEXER_LOAD_ARG(double, p_pruning_posterior_treshold, -INF);
	MKWSINDEXER_LOAD_ARG(bool, p_do_pruning_posterior_treshold, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_generate_best_path_nodes_from_nodes_file, false);
	MKWSINDEXER_LOAD_ARG(bool, p_write_viterbi, false);
	MKWSINDEXER_LOAD_ARG(bool, p_only_add_words_to_lexicon, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_readonly_lexicon, false);
	MKWSINDEXER_LOAD_ARG(int, p_lat_time_multiplier, 1);
	MKWSINDEXER_LOAD_ARG(string, p_lat_filename_delimiter, "_");
	MKWSINDEXER_LOAD_ARG(int, p_lat_filename_start_time_field, 0);
	MKWSINDEXER_LOAD_ARG(int, p_latname_time_multiplier, 100); // frames
	MKWSINDEXER_LOAD_ARG(bool, p_get_best_path, false);
	//MKWSINDEXER_LOAD_ARG(bool, p_output_alpha_beta_vectors, false);
	MKWSINDEXER_LOAD_ARG(string, p_write_ngrams, "");
	MKWSINDEXER_LOAD_ARG(int, p_ngrams_size, 3);
	MKWSINDEXER_LOAD_ARG(bool, p_generate_ngram_index, false);
	MKWSINDEXER_LOAD_ARG(string, p_indexed_meeting, "");
	MKWSINDEXER_LOAD_ARG(string, p_log, "");
	MKWSINDEXER_LOAD_ARG(bool, p_htk_remove_backslash, false);
	MKWSINDEXER_LOAD_ARG(string, p_data_type, ""); // "LVCSR" | "PHN"
	MKWSINDEXER_LOAD_ARG(bool, p_output_lattice_statistics, false); // "LVCSR" | "PHN"

	if (p_concatlattice_out != "")
	{
		latBinFileConcat.cleanOutputFiles(trim_file_extension(p_concatlattice_out) + mExt_bin);
	}
/*	if (p_search_index != "")
	{
		if (indexer.fwdIndex.create(p_search_index) != 0) {
			CERR("ERROR: Creating forward index file: " << p_search_index);
			exit(1);
		}
	}
*/	if (p_mlf_out != "")
	{
		ofstream mkfile(p_mlf_out.c_str()); // create MLF file with header !!! overwrite if exists
		mkfile << "#!MLF!#" << endl;
		mkfile.close();
	}
	if (p_get_best_path)
	{
		p_compute_links_likelihood = true;
	}

	mDbgTimer.start();

	// is the given filename a list of lattices?
	bool is_latlist;
	string fname = filename;
	if (fname.substr(0,5) == "LIST:")
	{
		is_latlist = true;
		fname.erase(0,5);
	} else {
		is_latlist = false;
	}
	
	if (!file_exists(fname.c_str()))
	{
		CERR("ERROR: file does not exist: "<<fname);
		EXIT();
	}

	//==================================================
	// OPEN LATTICE LIST FILE
	//==================================================
	ifstream in_latlist;
	if (is_latlist) {
		in_latlist.open(fname.c_str());
		if (in_latlist.fail()) {
			CERR("Error: latlist open failed ... " << fname);
			EXIT();
		}
	}

	if (p_data_type == "")
	{
		CERR("ERROR: p_data_type has to be set");
		EXIT();
	}
	else if (p_data_type == "PHN")
	{
		mpMKws->SetDictionaryPhn();
	}
	else if (p_data_type == "LVCSR")
	{
		mpMKws->SetDictionaryLvcsr();
	}

	// process the lattice or in case of list of lattices 
	// process all lattices in the list
	for (int processed_files_counter = 0; 
		(is_latlist) ? (getline(in_latlist, fname)) : processed_files_counter == 0; 
		processed_files_counter++)
	{
		DBG("is_latlist="<<is_latlist);

		//==================================================
		// PROCESS LATTICE FILENAME
		//==================================================
		string latname = fname;
		// erase file extension
		string::size_type strPos = latname.rfind (PATH_SLASH);
		if (strPos != string::npos) {
			string::size_type pos2 = latname.find ('.', strPos);
			latname.erase(pos2);
		}
		// erase path (only fname without extension remains)
		string latname_nopath = latname;
		strPos = latname_nopath.rfind(PATH_SLASH);
		latname_nopath.erase(0, strPos+1);


		//==================================================
		// GET LATTICE'S START TIME
		//==================================================
		LatTime latStartTime = 0;
		if (p_read_time_from_filename) {
			string sStartTime = latname_nopath;
			string::size_type pos_1st = 0;
			for (int field_idx=1; field_idx < p_lat_filename_start_time_field; field_idx++) {
				pos_1st = sStartTime.find (p_lat_filename_delimiter, pos_1st+1); // find first _ in latname
				DBG("pos_1st:"<<pos_1st);
			}
			if (pos_1st != string::npos) {
				string::size_type pos_2nd = latname_nopath.find (p_lat_filename_delimiter, pos_1st+1); // find second _ in latname
				sStartTime = sStartTime.substr(pos_1st+1, pos_2nd - pos_1st - 1);
				latStartTime = atof(sStartTime.c_str()) / p_latname_time_multiplier;
				DBG("Getting start time from fname...sStartTime:"<<sStartTime<<" latStartTime:"<<latStartTime<<endl);
			} else {
				CERR("Error in lattice's name: Expecting '"<<p_lat_filename_delimiter<<"' in \"" << latname_nopath << "\" ... setting start time to 0.0");
				EXIT();
			}
		}

		Lattice lat(mpDictionary);
		lat.amscale = p_amscale;
		lat.lmscale = p_lmscale;

		//==================================================
		// READ HTK LATTICE
		//==================================================
		DBG( "reading..." << flush);

		if(lat.loadFromHTKFile(fname, latStartTime, p_lat_time_multiplier, p_only_add_words_to_lexicon, p_htk_remove_backslash) != 0) {
			cerr << "Error occured while reading lattice file " << fname << endl << flush;
			exit (1);
		}
		DBG( "done\t" << flush);

		OnLatticeLoaded(&lat);

		//==================================================
		// SORT LATTICE
		//==================================================
		if (p_sort_lattice)
		{
			DBG("sorting lattice");
			lat.sortLattice();
			DBG("sorting lattice...done");
		}
		
		//==================================================
		// CHECK NODE SORTING
		//==================================================
		if (p_check_node_sorting) {
			DBG("lattice link test");
			for (Lattice::LinkMap::iterator l=lat.links.begin(); l!=lat.links.end(); ++l) {
				if ((l->second).S > (l->second).E) {
					CERR("Error in \"" << fname << "\" ...linkID:"<<l->first<<" "<<(l->second).S<<" -> "<<(l->second).E);
					exit(1);
				}
			}
			DBG("lattice link test: done");
		}

		//==================================================
		// PRUNE LATTICE
		//==================================================
		if (p_do_pruning_posterior_treshold) {
			lat.updateFwdBckLinks();
			lat.pruneLattice(p_pruning_posterior_treshold);
		}

		//==================================================
		// COMPUTE LINKS LIKELIHOOD (POSTERIORS)
		//==================================================
		if (p_compute_links_likelihood) {
			DBG("compute links likelihood..." << flush);

			LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
			vit.computeFwViterbi();
			vit.computeBwViterbi();
			vit.computeLinksLikelihood(latStartTime);
			if (p_write_viterbi) 
				vit.writeViterbiFwBw(p_lattice_dir_out + PATH_SLASH + latname_nopath + ".fwbw");

			DBG("done   ");
		}
		
		//==================================================
		// OUTPUT LATTICE STATISTICS
		//==================================================
		if (p_output_lattice_statistics)
		{
			OutputLatticeStatistics(cout, lat);
		}

		//==================================================
		// NGRAMS
		//==================================================
		if (p_write_ngrams != "")
		{
			lat.OutputNgrams(p_write_ngrams, p_ngrams_size, this, MKwsIndexer_JumpOverCallback);
		}

		//==================================================
		// HTK OUTPUT
		//==================================================
		if (p_write_htk) {
			DBG( "htk-export (" << p_lattice_dir_out + PATH_SLASH + latname_nopath + mExt_copy << ")..." << flush);
			lat.saveToHTKFile(p_lattice_dir_out + PATH_SLASH + latname_nopath + mExt_copy, p_htk_posteriors_output, true); // COPY OF LATTICE		
			DBG( "done\t" << flush);
		}

		//==================================================
		// MLF OUTPUT
		//==================================================
		if (p_mlf_out != "") {
			DBG( "mlf-export..." << flush);
			//			lexicon.print();
			LatViterbiFwBw vit(&lat, p_wi_penalty, p_amscale, p_lmscale, p_amscale_kwd, p_lmscale_kwd, p_viterbi_baumwelsh, p_logAdd_treshold);
			vit.computeFwViterbi();
			vit.computeBwViterbi();
			vit.computeLinksLikelihood(latStartTime);
			//			vit.writeViterbiFwBw(fname + ".fwbw");

			//			DBG("fwds[lastNode]="<<vit.fwds[vit.NN-1]<<" bwds[firstNode]="<<vit.bwds[0]);
			/*			DBG("Checking fwds");
						for (ID_t n=0; n<vit.NN; n++) {
						for (vector<ID_t>::iterator l=lat.bckLinks[n].begin(); l!=lat.bckLinks[n].end(); ++l) {
						if (vit.fwds[lat.links[*l].S] > vit.fwds[n]) {
						DBG("ERROR: "<<lat.links[*l].S<<" -> "<<n<<" ... fwds: "<<vit.fwds[lat.links[*l].S]<<" > "<<vit.fwds[n]);
						}
						}
						}
						DBG("Checking fwds: done");*/
			LatMlfFile latmlffile(&lat, &vit, p_mlf_likelihood_treshold, p_compute_overlapped_words_likelihood);
			if (latmlffile.loadKeywordList(p_mlf_kwdlist) != 0) {
				cout << "Error: processing kwdlist: " << p_mlf_kwdlist << endl << flush;
				exit(1);
			}
			latmlffile.save(p_mlf_out, lat.record);
			DBG( "done\t" << flush);
		}

		//==================================================
		// LATTICE CONCATENATION
		//==================================================
		if (p_concatlattice_out != "") {
			DBG( "adding-to-concatlattice..." << flush);

			latBinFileConcat.setLattice(lat);
			string meeting_filename = trim_file_extension(p_concatlattice_out) + mExt_bin;
			DBG("meeting_filename: "<<meeting_filename);

			string meeting_rec = trim_file_extension(trim_path(p_concatlattice_out, p_data_dir_prefix)) + mExt_bin;
			DBG("meeting_rec: "<<meeting_rec);

/*
			if (p_search_index != "")
			{
				if (!p_generate_ngram_index)
				{
					indexer.fwdIndex.addMeeting(lat.nodes.size());
				}
			}
*/			latBinFileConcat.save(meeting_filename, meeting_rec, true);

			//			globalLat.addLattice(lat);
			DBG( "done\t" << flush);
		}

		//==================================================
		// FORWARD INDEX
		//==================================================
//		if (p_search_index != "")
		{
			DBG("Generate forward index...");
			if (p_generate_ngram_index)
			{
				Lattice2NgramsFwdIndex(&lat, p_ngrams_size);
			} else {
				Lattice2FwdIndex(&lat);
			}
			DBG("Generate forward index...done");
		}

		//==================================================
		// CLOSE FORWARD INDEX
		//==================================================
/*		if (p_search_index != "")
		{
			indexer.fwdIndex.closeMeeting();
		}
*/
		//==================================================
		// DOT OUTPUT
		//==================================================
		if (p_write_dot) {
			DBG( "dot-export..." << flush);
			lat.saveToDotFile(p_lattice_dir_out + PATH_SLASH + latname_nopath + mExt_dot);
			DBG( "done\t" << flush);
		}
	} // for each lattice

	//==================================================
	// AFTER PROCESSING LATTICES
	//==================================================

	
	if (is_latlist) in_latlist.close();
	
	//==================================================
	// SAVE CONCATENATED LATTICE
	//==================================================
	if (p_concatlattice_out != "") {
		DBG( "saving-concatlattice..." << flush);
/*		if (p_write_binary) {
			DBG( "binary..." << flush);
			globalLat.saveToBinaryFile(p_concatlattice_out + mExt_bin);
		} 
*/
		
		//==================================================
		// HTK OUTPUT
		//==================================================
		if (p_write_htk) {
			DBG("WARNING: Saving concatenated lattice to HTK file format is not implemented");
//			globalLat.saveToHTKFile(p_concatlattice_out + mExt_htk);				// without posteriors
//			globalLat.saveToHTKFile(p_concatlattice_out + mExt_htk + ".lik", true, true);	// with posteriors
		}

		//==================================================
		// BINARY OUTPUT
		//==================================================
		if (p_write_binary_gzipped) {
			DBG( "binary-gzipped..." << flush);
//			latBinFile.setLattice(globalLat);

			string meeting_filename = trim_file_extension(p_concatlattice_out) + mExt_bin;
			string meeting_rec = trim_file_extension(trim_path(p_concatlattice_out, p_data_dir_prefix)) + mExt_bin;
			
//			latBinFile.save(meeting_filename, meeting_rec);


			latBinFileConcat.closeConcatenatedLatticeOutput(meeting_filename, meeting_rec, !p_generate_ngram_index);
//			DBG("latBinFileConcat.closeConcatenatedLatticeOutput("<<meeting_filename<<", "<<meeting_rec<<")");
//			globalLat.saveToBinaryFile(p_concatlattice_out + mExt_bingz, Lattice::gzip);
		}

		//==================================================
		// DOT OUTPUT
		//==================================================
		if (p_write_dot) {	
			DBG( "dot...");
			globalLat.saveToDotFile(p_concatlattice_out + mExt_dot);
		}
		DBG( "done" << endl);
	}

	//==================================================
	// SAVE SEARCH INDEX
	//==================================================
/*	if (p_search_index != "") {
		DBG( "closing forward index file...");
		indexer.fwdIndex.close();
	}
*/
	mDbgTimer.end();
	if (p_log != "")
	{
		DBG("LOG: "<<p_log);
		ofstream log(p_log.c_str(), (file_exists(p_log.c_str()) ? ios::out | ios::app : ios::out));
		log << "indexing_time: " << mDbgTimer.val() << endl;
		log.close();
	}
}

bool MKwsIndexer_JumpOverCallback(void* pPtr, ID_t wordId) 
{
	MKwsIndexer* pIndexer = reinterpret_cast<MKwsIndexer*>(pPtr);
	// jump over meta-words
	return pIndexer->GetModule()->GetDictionary()->IsMetaWord(wordId);
}

void MKwsIndexer::Lattice2FwdIndex(Lattice* pLat)
{
	FwdRecord fwdRec;
	for(Lattice::NodeMap::iterator i=pLat->nodes.begin(); i!=pLat->nodes.end(); ++i) 
	{
		fwdRec.mLabelId = mpMKws->GetDictionary()->GetUnitId((i->second).W);
		fwdRec.mScore = (i->second).bestLikelihood; 
		fwdRec.mStartTime = (i->second).tStart;
		fwdRec.mEndTime = (i->second).t;
		//if node != "!NULL" => add node to time index and search index
		//DBG("fwdRec:"<<fwdRec);
		//DBG("CanAddToFwdIndex:"<<mpMKws->GetDictionary()->CanAddToFwdIndex(fwdRec.mLabelId));
		if (mpMKws->GetDictionary()->CanAddToFwdIndex(fwdRec.mLabelId)) 
		{
			AddFwdIndexRecord(fwdRec);
		}
	}
}

void MKwsIndexer::Lattice2NgramsFwdIndex(Lattice* pLat, int n)
{
	Ngrams ngrams(NGRAMS_HASH_SIZE);
	DBG("Generating ngrams");
	TLatViterbiLikelihood alpha_last = pLat->nodes.rbegin()->second.alpha;
	// assign word id to each node
	for (Lattice::NodeMap::iterator iNode = pLat->nodes.begin(); iNode != pLat->nodes.end(); ++iNode)
	{
		if ((iNode->second.W_id = mpMKws->GetDictionary()->GetUnitId(iNode->second.W)) == 0)
		{
			iNode->second.W_id = mpMKws->GetDictionary()->AddUnit(iNode->second.W);
		}
	}
	// send tokens from each node to generate ngrams
	for (Lattice::NodeMap::iterator iNode = pLat->nodes.begin(); iNode != pLat->nodes.end(); ++iNode)
	{
		DBG("--------------------------------------------------");
		DBG("Sending tokens from node: "<<iNode->second);
		pLat->GenerateNgrams_recursive(ngrams, iNode->second.id, 0, n, NULL, alpha_last, this, MKwsIndexer_JumpOverCallback);
	}
	DBG("Generating ngrams...done");

	FwdRecord fwdRec;

	DBG("Ngrams size:"<<ngrams.Size());
	if (ngrams.Size() > 0)
	{
//		AllocateFwdIndex(ngrams.size());

		NgramsCursor cur(ngrams);
		for (Ngram *pNgram = cur.Begin(); pNgram != NULL; pNgram = cur.Next())
		{
			string ngram_str = "";
			for (int i=n-1; i>=0; i--)
			{
				ngram_str += pLat->lexicon->GetUnitLabel(pNgram->GetItemWordId(i)) + (i>0 ? "_" : "");
			}

			if ((fwdRec.mLabelId = pLat->lexicon->GetUnitId(ngram_str)) == 0)
				fwdRec.mLabelId = pLat->lexicon->AddUnit(ngram_str);
			fwdRec.mScore = pNgram->GetConfidence();
			fwdRec.mStartTime = pNgram->GetStartTime();
			fwdRec.mEndTime = pNgram->GetEndTime();

			AddFwdIndexRecord(fwdRec);
		}
	}
}
/*
void MKwsIndexer_AddToFwdIndex(void* pPtr, LatIndexer::Record &rec)
{
	MKwsIndexer* pIndexer = reinterpret_cast<MKwsIndexer*>(pPtr);
	pIndexer->AddFwdIndexRecord((ID)(rec.mWordID), (Score)(rec.mConf), (Time)(rec.mStartTime), (Time)(rec.mEndTime));
}
*/

void MKwsIndexer::GetOccurrences(ID labelId, lse::QueryKwd::InvIdxRecords* results, std::vector<ID> &docList)
{
	InvLabel* l = MIndexerStruct::GetOccurrences(labelId);
	if (l == NULL)
	{
		results = NULL;
		CERR("ERROR: label id "<<labelId<<" does not exist in the index");
		EXIT();
	}
	DocumentsSortedByRelevance* dr = l->GetDocsByRelevance();
	DocumentsSortedById* di = l->GetDocsById();
	Hits* h = l->GetHits();

	DBG("dr->GetSize():"<<dr->GetSize());
	DBG("di->GetSize():"<<di->GetSize());
	DBG("h->GetSize():"<<h->GetSize());

	// sorted by time
	if (results->mpByTime != NULL)
	{
		delete[] results->mpByTime;
	}
	results->mpByTime = new int[h->GetSize()];
	DBG("--------------------------------------------------");
	DBG("docList:");
	for (vector<ID>::iterator iDoc=docList.begin(); iDoc!=docList.end(); iDoc++)
	{
		DBG(*iDoc);
	}
	DBG("--------------------------------------------------");

	// sorted by confidence / relevance
	LatIndexer::Record latRec;
	latRec.mWordID = labelId;
	int results_idx = 0;
	for (unsigned int i=0; i<dr->GetSize(); i++)
	{
		DocumentsSortedByRelevance::Record rr = (*dr)[i];
		//DBG("rr: "<<rr);
		if (docList.size() > 0)
		{
			bool found = false;
			for (vector<ID>::iterator iDoc=docList.begin(); iDoc!=docList.end(); iDoc++)
			{
				if (*iDoc == rr.GetDocId())
				{
					DBG("found");
					found = true;
					break;
				}
			}
			if (!found)
			{
				continue;
			}
		}
		latRec.mMeetingID = rr.GetDocId();
		//DBG("latRec.mMeetingID="<<latRec.mMeetingID);
		DocumentsSortedById::Record ri = di->GetRecordByDocId(rr.GetDocId());
		//DBG("ri: "<<ri);
		for (unsigned int j=0; j<ri.GetHitListRecordsCount(); j++)
		{
			Hits_OneDocHitList::Record rh = (*h)[j + ri.GetHitListStartPos()];
			latRec.mConf = rh.GetScore();
			latRec.mStartTime = rh.GetStartTime();
			latRec.mEndTime = rh.GetEndTime();
			//DBG("latRec: "<<latRec);
			results->push_back(latRec);

			results->mpByTime[ri.GetHitListStartPos() + j] = results_idx;
			results_idx++;
		}
	}
}

void MKwsIndexer::Sort()
{
	DBG("MKwsIndexer::Sort()");
	mpDictionary->ForEachLabel( Sort_Callback, this );
}
