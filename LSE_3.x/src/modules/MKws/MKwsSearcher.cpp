#include "MKwsSearcher.h"
#include "MKws.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "search/hypothesis.h"
#include "ResultsList.h"

#define MKWSSEARCHER_LOAD_ARG(t,id,def) t id = (t)(mpMKws->GetConfig()->Value("MKws_searcher", #id, def))
#define MKWSSEARCHER_GET_ARG(t,id,def) ((t)(mpMKws->GetConfig()->Value("MKws_searcher", #id, def)))

#define TIMER_START \
	t1 = boost::posix_time::microsec_clock::local_time()

#define TIMER_END(msg) \
	t2 = boost::posix_time::microsec_clock::local_time(); \
	td = t2 - t1; \
	DBG("TIMER: " << msg << " td=" << td.total_microseconds() << " microseconds (" << td << ")");

using namespace std;
using namespace sem;
using namespace lse;


/* isKwdSeparator {{{*/
bool isKwdSeparator(char c) 
{
	return (c==' ' || c=='\t' || c=='\n');
}
/*}}}*/

/* isQueryVar {{{ */
bool isQueryVar(string q, int kwdStartPos, const string &identifier) 
{
	return q.substr(kwdStartPos, identifier.length()) == identifier;
}
/* }}} */

/* getQueryVarValue {{{ */
const string getQueryVarValue(string q, int kwdStartPos, int kwdSpacePos, const string identifier) 
{
	return q.substr(kwdStartPos + identifier.length(), kwdSpacePos - kwdStartPos - identifier.length());
}
/* }}} */

/* isANY {{{*/
bool isANY(string kwd) 
{
	return kwd == "_any_";
}
/*}}}*/

//	str2LatTime {{{
/**
  @brief converts the input string to seconds

  @param str input string

  @return time in seconds
*/
LatTime str2LatTime(std::string str) 
{
	LatTime res = 0;
	string strTmp = "";
	float multiplier = 0;
	for (unsigned int i=0; i<str.length(); i++) {
		switch (str[i]) {
			
			case 'h':
//				DBG_FORCE("hours: <"<<strTmp<<">");
				multiplier = 3600; 	// 1 hour = 3600 sec
				break;

			case 'm':
//				DBG_FORCE("minutes: <"<<strTmp<<">");
				multiplier = 60; 	// 1 min = 60 sec
				break;

			case 's':
//				DBG_FORCE("seconds: <"<<strTmp<<">");
				multiplier = 1; 	// 1 sec = 1 sec :o)
				break;

			case '%':
				DBG("percents: <"<<strTmp<<">");
				multiplier = -0.01;
				break;

			default:
				strTmp += str[i];

		}

		if (multiplier != 0) {
			res += atof(strTmp.c_str()) * multiplier;
			multiplier = 0;
			strTmp.clear();
		}
	}
	DBG("res: "<<res);
	return res;
}
// }}} 

// MKwsSearcher::MKwsSearcher() {{{
MKwsSearcher::MKwsSearcher(MKws* pMKws) : MSearcher((M*)pMKws), mpMKws(pMKws) 
{
	MKWSSEARCHER_LOAD_ARG(string, p_g2p_lexicon, "");
	MKWSSEARCHER_LOAD_ARG(string, p_g2p_rules, "");
	MKWSSEARCHER_LOAD_ARG(string, p_g2p_symbols, "");
	MKWSSEARCHER_LOAD_ARG(int, p_g2p_max_variants, 0);
	MKWSSEARCHER_LOAD_ARG(bool, p_g2p_scale_prob, true);
	MKWSSEARCHER_LOAD_ARG(double, p_g2p_prob_tresh, 0.0);

	if (p_g2p_lexicon != "")
	{
		DBG_FORCE("mpG2P->Load("<<p_g2p_rules<<", "<<p_g2p_symbols<<", "<<p_g2p_lexicon<<", "<<p_g2p_max_variants<<", "<<p_g2p_scale_prob<<", "<<p_g2p_prob_tresh<<")");
		mpG2P = new G2P;
		mpG2P->Load(p_g2p_rules, p_g2p_symbols, p_g2p_lexicon, p_g2p_max_variants, p_g2p_scale_prob, p_g2p_prob_tresh);
	}
	else
	{
		mpG2P = NULL;
	}
}
// }}}

// ParseQuery() {{{
void MKwsSearcher::ParseQuery(const std::string &qStr)
{
//	QueryKwdList mpQueryKwdList(lexicon);
	//SearchSource searchSource = lattice; // let's search in lattice as default 
	//Timer search_timer;
	//search_timer.start();

	DBG_FORCE("qStr:"<<qStr);

	mpQueryKwdList = new QueryKwdList;

	assert(mpMKws);
	assert(mpMKws->GetConfig());
	assert(mpQueryKwdList);

	mpQueryKwdList->hyps_per_stream = (int)(mpMKws->GetConfig()->Value("MKws_indexer", "p_hyps_per_stream", (int)0));
	mpQueryKwdList->time_delta = (double)(mpMKws->GetConfig()->Value("MKws_indexer", "p_time_delta", 0.0));
	mpQueryKwdList->fwd_nodes_count = (int)(mpMKws->GetConfig()->Value("MKws_indexer", "p_fwd_nodes_count", 0));
	mpQueryKwdList->bck_nodes_count = (int)(mpMKws->GetConfig()->Value("MKws_indexer", "p_bck_nodes_count", 0));
	mpQueryKwdList->phrase_kwd_neighborhood = (double)(mpMKws->GetConfig()->Value("MKws_indexer", "p_phrase_kwd_neiborhood", 0.0));
	mpQueryKwdList->max_results = (int)(mpMKws->GetConfig()->Value("MKws_indexer", "p_max_results", -1));

	bool processANY = false;

	DBG_FORCE("Searching for '"<<qStr<<"'");
	if (qStr != "") {
		float q_nb = -1;
		string::size_type kwdStartPos = 0;
		string::size_type kwdSpacePos = 0;
		QueryKwd kwd;
		QueryKwd *pKwdTimeSet = NULL;
		bool firstPhraseKwd = false;
		bool lastPhraseKwd = false;

		int state = 0;
		bool epsilon = false; // epsilon transition (without moving to the next word)

		while (kwdStartPos < qStr.length()) {
			DBG("kwdStartPos:"<<kwdStartPos<<" qStr.length():"<<qStr.length());
			if (!epsilon) {
				for (;isKwdSeparator(qStr[kwdStartPos]); kwdStartPos++) {} // jump over white space
				
				if ((kwdSpacePos = qStr.find(' ', kwdStartPos)) == string::npos) { // find end of current keyword
					kwdSpacePos = qStr.length();
				}
				
				kwd.W = qStr.substr(kwdStartPos, kwdSpacePos - kwdStartPos);
				//kwd.W = upcase(kwd.W);
				kwd.W = nonAsciiToOctal(kwd.W);
				DBG("kwd.W:"<<(kwd.W));
				kwd.greaterThan = false;
				kwd.lessThan = false;
				kwd.confidence_min_isset = false;
				kwd.confidence_max_isset = false;
				kwd.time_min_type = null;
				kwd.time_max_type = null;
				kwd.nb_prev = q_nb;
				kwd.nb_next = q_nb;
				kwd.isNextInPhrase = false;
			} else {
				epsilon = false; // reset epsilon
			}

			DBG("switch ("<<state<<")");
			switch (state) {

				// beginning of the keyword
				case 0:
					
					// c(...) < x, c(...) > y, ...
					if (kwd.W.substr(0, 2) == "c(") {
						
						DBG("  trimming");
						kwd.W.erase(0,2);					// trim "c(" from beginning 
						kwd.W.erase(kwd.W.length()-1,1); 	// and ")" from the end
						DBG("  trimmed kwd: "<<kwd.W);
						// if the word is in the word list, load it's parameters. Otherwise add it to the word list
						QueryKwdList::iterator iKwd;
						if ((iKwd = mpQueryKwdList->find(kwd.W)) != mpQueryKwdList->end()) {
							kwd = *iKwd;
						} else {
							mpQueryKwdList->push_back(kwd);
						}
						state = 1;
						
					} else

					// phrase: "KWD1 KWD2"
					if (qStr[kwdStartPos] == '"') {
						DBG("  Phrase start found");
						kwd.W.erase(0,1); 					// erase " from kwd
						kwdStartPos ++; 					// jump over "
						state = 2;
						firstPhraseKwd = true;
						epsilon = true; 					// we need to know whether this is the last word in phrase (ending with ")
						
					} else

					// neighborhood: nb=<float>
					if (isQueryVar(qStr, kwdStartPos, "nb=")) 
					{						
						q_nb = atof(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "nb=").c_str());
						DBG("  q_nb.float = "<<q_nb);

					} else

					// -hyps-per-stream
					if (isQueryVar(qStr, kwdStartPos, "&HPS=")) 
					{
						mpQueryKwdList->hyps_per_stream = atoi(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&HPS=").c_str());
						DBG("  hyps_per_stream = "<<mpQueryKwdList->hyps_per_stream);

					} else

					// -time-delta
					if (isQueryVar(qStr, kwdStartPos, "&TD=")) 
					{
						mpQueryKwdList->time_delta = atoi(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&TD=").c_str());
						DBG("  time_delta = "<<mpQueryKwdList->time_delta);

					} else
						
					// -fwd-nodes-count
					if (isQueryVar(qStr, kwdStartPos, "&FNC=")) 
					{
						mpQueryKwdList->fwd_nodes_count = atoi(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&FNC=").c_str());
						DBG("  fwd_nodes_count = "<<mpQueryKwdList->fwd_nodes_count);

					} else

					// -bck-nodes-count
					if (isQueryVar(qStr, kwdStartPos, "&BNC=")) 
					{
						mpQueryKwdList->bck_nodes_count = atoi(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&BNC=").c_str());
						DBG("  bck_nodes_count = "<<mpQueryKwdList->bck_nodes_count);

					} else
						
					// -phrase-kwd-neighborhood
					if (isQueryVar(qStr, kwdStartPos, "&PNB=")) 
					{
						mpQueryKwdList->phrase_kwd_neighborhood = atof(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&PNB=").c_str());
						DBG("  phrase_kwd_neighborhood = "<<mpQueryKwdList->phrase_kwd_neighborhood);

					} else

					// -max-results
					if (isQueryVar(qStr, kwdStartPos, "&RES=")) 
					{
						mpQueryKwdList->max_results = atoi(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&RES=").c_str());
						DBG("  max_results = "<<mpQueryKwdList->max_results);

					} else

					if (isQueryVar(qStr, kwdStartPos, "&DOC=")) 
					{
//						CERR("ERROR: specifying a source (&DOC=...) is not supported");
//						EXIT();
						//LatMeetingIndexerRecord meetingRec;
						string doc = getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&DOC=");
						assert(mpMKws->GetGlobalInstance()->GetDocuments());
						ID doc_id = mpMKws->GetGlobalInstance()->GetDocuments()->GetDocId(doc);
						DBG("doc_id["<<doc<<"]:"<<doc_id);
						mpQueryKwdList->mDocList.push_back(doc_id);
						//mpMDocumentsList.AddDocument(config->indexerLvcsr->meetings.getRecID(meetingRec.path), 0);
						//DBG("  SRC = "<<meetingRec.path);

					} else

					if (isQueryVar(qStr, kwdStartPos, "&EM=")) 
					{
						mpQueryKwdList->mExactMatch = (getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&EM=") == "true" ? true : false);
						DBG("  EM = "<<mpQueryKwdList->mExactMatch);

					} else

					if (isQueryVar(qStr, kwdStartPos, "&OVERLAP")) 
					{
						mpQueryKwdList->mOverlapping = true;
						DBG("  OVERLAP = "<<mpQueryKwdList->mOverlapping);

					} else

					if (isQueryVar(qStr, kwdStartPos, "&EM")) 
					{
						mpQueryKwdList->mExactMatch = true;
						DBG("  EM = true");

					} else

					if (isQueryVar(qStr, kwdStartPos, "&NGRAM2PHN")) 
					{
						mpQueryKwdList->mNgram2Phn = true;
						DBG("  NGRAM2PHN = true");

					} else
						
					if (isQueryVar(qStr, kwdStartPos, "&PHN2NGRAM")) 
					{
						mpQueryKwdList->mPhn2Ngram = true;
						DBG("  PHN2NGRAM = true");

					} else
						
					if (isQueryVar(qStr, kwdStartPos, "&ALLSUCCESSFULTOKENS")) 
					{
						mpQueryKwdList->mReturnAllSuccessfulTokens = true;
						DBG("  ALLSUCCESSFULTOKENS = true");

					} else
						
					if (isQueryVar(qStr, kwdStartPos, "&ID=")) 
					{
						mpQueryKwdList->mTermId = getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&ID=");
						DBG("  mTermId = "<<mpQueryKwdList->mTermId);

					} else

					if (isQueryVar(qStr, kwdStartPos, "&SEARCHSOURCETYPE=")) 
					{
						mpQueryKwdList->mSearchSource = SearchSource::ParseString(getQueryVarValue(qStr, kwdStartPos, kwdSpacePos, "&SEARCHSOURCETYPE=").c_str());
						DBG("  SEARCHSOURCE = "<<mpQueryKwdList->mSearchSource);

					} else
						
					// words order operators
					if (kwd.W == "<") {
						(--mpQueryKwdList->end())->lessThan = true;
					} else 
						
					if (kwd.W == ">") {
						(--mpQueryKwdList->end())->greaterThan = true;
					} else
						
					// time operator
					if (kwd.W.substr(0, 2) == "t(") {
						
						DBG("  trimming");
						kwd.W.erase(0,2);					// trim "c(" from beginning 
						kwd.W.erase(kwd.W.length()-1,1); 	// and ")" from the end
						DBG("  trimmed kwd: "<<kwd.W);
						// if the word is in the word list, load it's parameters. Otherwise add it to the word list
						QueryKwdList::iterator iKwd;
						if ((iKwd = mpQueryKwdList->find(kwd.W)) != mpQueryKwdList->end()) {
							kwd = *iKwd;
							pKwdTimeSet = &(*iKwd);
						} else {
							mpQueryKwdList->push_back(kwd);
							pKwdTimeSet = &(*mpQueryKwdList->rbegin());
						}
						DBG("setting time for kwd:"<<kwd);
						state = 5;
						
					} else 
					
					// other words
					{
						kwd.nb_prev = q_nb;
						kwd.nb_next = q_nb;
						mpQueryKwdList->push_back(kwd);
						DBG("  normal keyword:"<<kwd.W);
					}
					break;

				case 1:

					// confidence operators
					if (kwd.W == "<") {
						state = 3;
					} else if (kwd.W == ">") {
						state = 4;
					}

					break;

				case 2:
					
					// within the phrase
					if (qStr[kwdSpacePos-1] == '"') {
						DBG("  Phrase end found");
						kwd.W.erase(kwd.W.length()-1,1); // erase " from kwd
						kwdSpacePos --; // cut " off
						state = 0;
						lastPhraseKwd = true;
					}
					DBG("  qStr[kwdSpacePos-1]:"<<qStr[kwdSpacePos-1]);

					kwd.nb_prev = firstPhraseKwd ? q_nb : mpQueryKwdList->phrase_kwd_neighborhood;
					kwd.nb_next = lastPhraseKwd ? q_nb : mpQueryKwdList->phrase_kwd_neighborhood;
					DBG("  inPhrase...kwd:"<<kwd.W);
					
					if (firstPhraseKwd) {
						firstPhraseKwd = false;
					} else {
						if (mpQueryKwdList->size() > 0)
							(--(mpQueryKwdList->end())) -> isNextInPhrase = true;
					}
					if (lastPhraseKwd) lastPhraseKwd = false; 

					mpQueryKwdList->push_back(kwd);
					
					break;
				
				case 3:
					// c(...) < x
					(--mpQueryKwdList->end())->confidence_max = atof(kwd.W.c_str());
					(--mpQueryKwdList->end())->confidence_max_isset = true;
					state = 0;
					break;

				case 4:
					// c(...) > x
					(--mpQueryKwdList->end())->confidence_min = atof(kwd.W.c_str());
					(--mpQueryKwdList->end())->confidence_min_isset = true;
					state = 0;
					break;
					
				case 5:
					// timing operators
					if (kwd.W == "<") {
						state = 6;
					} else if (kwd.W == ">") {
						state = 7;
					}
					break;

				case 6:
					// t(...) < x
					pKwdTimeSet->time_max = str2LatTime(kwd.W.c_str());
					if (pKwdTimeSet->time_max >= 0) {
						pKwdTimeSet->time_max_type = lse::time;
					} else {
						pKwdTimeSet->time_max_type = lse::percents;
						pKwdTimeSet->time_max *= -1; // convert it back to positive value
					}
					
					DBG("time_max:"<<pKwdTimeSet->time_max);
					state = 0;
					break;

				case 7:
					// t(...) > x
					pKwdTimeSet->time_min = str2LatTime(kwd.W.c_str());
					if (pKwdTimeSet->time_min >= 0) {
						pKwdTimeSet->time_min_type = lse::time;
					} else {
						pKwdTimeSet->time_min_type = lse::percents;
						pKwdTimeSet->time_min *= -1; // convert it back to positive value
					}
					
					DBG("time_min:"<<pKwdTimeSet->time_min);
					state = 0;
					break;
					

					
			} // switch (state)		
			if (!epsilon)
			{
				kwdStartPos = kwdSpacePos + 1;
			}
		} // while (kwdStartPos < qStr.length())

		processANY = false; // are we going to process the keyword "_any_" or the ordinary query
		for (QueryKwdList::iterator kwd = mpQueryKwdList->begin(); kwd != mpQueryKwdList->end(); ++kwd) {
			if (isANY(kwd->W)) {
				if (mpQueryKwdList->size() > 1) {
					CERR("ERROR: The keyword '_any_' can be used only without other words");
					EXIT();
				} else {
					processANY = true;
					break;
				}
			}					
		}
			
	} 

	if (processANY) {
		CERR("ERROR: _any_ keywords are not supported");
		EXIT();
	}
}
// }}}

// GetNResults() {{{
void MKwsSearcher::GetNResults(int n, ResultsList * pRes) 
{
	Results results; // map (record+stream -> hypothesis list)
	results.SetQueryKwdList(mpQueryKwdList);
	results.SetMaxHypothesesCount(mpQueryKwdList->max_results);
	results.SetHardDecisionTreshold(MKWSSEARCHER_GET_ARG(double, p_hard_decision_treshold, 0));
/*
	for (MeetingsList::iterator i=config->mValidMeetings.begin(); i!=config->mValidMeetings.end(); ++i) {
		DBG_FORCE("MeetingsList: " << i->first);
	}
*/
	SearchKeyword(mpQueryKwdList, &results);
	DBG_FORCE("before postprocessing ... results.size():"<<results.size());
	//results.postProcess();
	//results.postProcess(&config->normLexicon_lvcsr, &config->normLexicon_phn);
	DBG_FORCE("after postprocessing ... results.size():"<<results.size());
	//search_timer.end();
//	results.print(output, mpMKws->GetConfig(), 0/*search_timer.val()*/);
	for (ResultsBaseType::iterator iHyp = results.begin(); iHyp != results.end(); iHyp++)
	{
		ResultsList::Result* res = new ResultsList::Result;
		res->SetStartTime(iHyp->keywords.begin()->start);
		res->SetEndTime(iHyp->keywords.rbegin()->end);
		res->SetScore(iHyp->score);
		res->SetStream(iHyp->stream);
		ostringstream label;
		bool put_separator = false;
		for (Hypothesis::Keywords::const_iterator iKwd=iHyp->keywords.begin(); iKwd!=iHyp->keywords.end(); ++iKwd) {
			label << (put_separator ? " " : "") << iKwd->str;
			if (!put_separator) put_separator = true;
		}
		res->SetLabel(label.str());
		pRes->AddResult(iHyp->documentId, res);
	}
}
// }}}

//	SearchKeyword() {{{
/**
  @brief search for given parsed query using indexer and lexicon and write the results to output stream os

  @param *queryKwdList parsed query
  @param *pResults results
*/
void MKwsSearcher::SearchKeyword(
		QueryKwdList 		*queryKwdList, 
		Results 			*pResults) 
{
	boost::posix_time::ptime t1;
	boost::posix_time::ptime t2;
	boost::posix_time::time_duration td;
	DBG("SearchKeyword()");
	
	DBG_FORCE("queryKwdList.size():"<<queryKwdList->size());

	/* get wordID for keywords {{{ */
	for (QueryKwdList::iterator i = queryKwdList->begin(); i != queryKwdList->end(); ++i) 
	{
		DBG_FORCE("WORD: "<<i->W);
		// if the word is not in the lvcsr lexicon
		bool is_oov = true;

		DBG("mpDictionaryLvcsr->GetSize() = "<<mpMKws->GetDictionaryLvcsr()->GetSize());
		if (mpMKws->GetDictionaryLvcsr())
		{
			is_oov = (i->W_id = mpMKws->GetDictionaryLvcsr()->GetUnitId(i->W)) == ID_INVALID;
		}
		if ( is_oov ) {
			DBG("Word " << i->W << " has not been found in LVCSR lexicon... trying to convert to phonemes");
			pResults->mOovCount++;
			i->W_id = -pResults->mOovCount; // assign a unique W_id for each OOV
		}
		if ((queryKwdList->mSearchSource == SearchSource::LVCSR_OOVPHN && is_oov) ||
			 queryKwdList->mSearchSource == SearchSource::LVCSR_PHN ||
			 queryKwdList->mSearchSource == SearchSource::PHN)
		{
			if (mpG2P != NULL)
			{
				// get list of transcriptions - pronounciation variants
				list<gpt::PhnTrans::pt_entry> pron_list;
				DBG_FORCE("g2p->GetTranscs("<<i->W<<")");
				mpG2P->GetTranscs(lowcase(i->W), &pron_list);
				DBG_FORCE("pron_list.size():"<<pron_list.size());
				for (list<gpt::PhnTrans::pt_entry>::iterator iPron=pron_list.begin(); iPron!=pron_list.end(); ++iPron)
				{
					DBG_FORCE("PRON: "<<iPron->trans);
				}
				// create phn and ngram QueryKwdList for the current keyword i
				i->SetPronList(&pron_list, queryKwdList, mpMKws->GetDictionaryPhn()); 
			}
			else
			{
				CERR("grapheme2phoneme tool is not set properly");
				EXIT();
			}
		}
		else
		{
			if (is_oov)
			{
				CERR("WARNING: Word "<<i->W<<" has not been found in LVCSR lexicon and the phoneme search is not enabled");
				Hypothesis hyp;
				hyp.score = -INF;
				hyp.addKeyword("Word "+i->W+" has not been found in LVCSR lexicon", 0, 0, -INF);
				pResults->push_back(hyp);
//				EXIT();
			}
		}
	}
	/* }}} */

	DBG_FORCE("queryKwdList before postprocessing:"); queryKwdList->print();
//	EXIT();
	/* convert multiword phoneme query to a single-word one {{{ */
	if (MKWSSEARCHER_GET_ARG(bool, p_phn_query_multiword_to_singleword, false) && queryKwdList->size() > 1)
	{
		vector<string> pron_list_str;

		QueryKwdList::iterator iKwdPrev = queryKwdList->begin();
		QueryKwdList::iterator iKwdCur = queryKwdList->begin(); iKwdCur++;
		// for the first keyword simply copy the pronounciation variants
		for (PronVariants::iterator iPronPrev = iKwdPrev->mPronList.begin(); 
			iPronPrev != iKwdPrev->mPronList.end();
			iPronPrev ++)
		{
			pron_list_str.push_back(trim_outer_spaces(iPronPrev->mPtEntry.trans));
		}
		// for all other keywords expand the pronounciation variants with "sil" and without "sil"
		while (iKwdCur != queryKwdList->end()) 
		{
			int pron_list_prev_size = pron_list_str.size();
			for (int iPronPrev = 0; iPronPrev < pron_list_prev_size; iPronPrev++)
			{
				for (PronVariants::iterator iPronCur = iKwdCur->mPronList.begin(); 
					iPronCur != iKwdCur->mPronList.end();
					iPronCur ++)
				{
					string new_pron;
					new_pron = pron_list_str[iPronPrev] + " " + trim_outer_spaces(iPronCur->mPtEntry.trans);
					pron_list_str.push_back(new_pron);

					new_pron = pron_list_str[iPronPrev] + " sil " + trim_outer_spaces(iPronCur->mPtEntry.trans);
					pron_list_str.push_back(new_pron);
				}
			}
			for (int iPronPrev = 0; iPronPrev < pron_list_prev_size; iPronPrev++)
			{
				pron_list_str.erase(pron_list_str.begin());
			}
			iKwdCur++;
		}
		list<gpt::PhnTrans::pt_entry> pron_list;
		for(vector<string>::iterator i=pron_list_str.begin(); i!=pron_list_str.end(); i++) 
		{ 
			DBG_FORCE("pron_list_str[]:"<<*i);	
			gpt::PhnTrans::pt_entry pron;
			pron.trans = *i;
			pron_list.push_back(pron);
		}

		string all_keywords = "";
		for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); iKwd++)
		{
			all_keywords += (all_keywords.length() > 0 ? " " : "") + iKwd->W;
		}
		QueryKwd kwd_all_keywords;

		QueryKwdList* p_query_singleword = new QueryKwdList;
		p_query_singleword->CopyParams(*queryKwdList); // copy query parameters
		p_query_singleword->push_back(kwd_all_keywords);
		p_query_singleword->begin()->SetPronList(&pron_list, p_query_singleword, mpMKws->GetDictionaryPhn());
		p_query_singleword->print();
		queryKwdList = p_query_singleword;
	}
	/* }}} */

	/* allow only pronounciation variants longer then N phonemes (N=3) {{{ */
	for (QueryKwdList::iterator iKwd = queryKwdList->begin(); iKwd != queryKwdList->end(); iKwd++)
	{
		bool changed = true;
		while (changed)
		{
			changed = false;
			for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
				iPron != iKwd->mPronList.end();
				iPron ++)
			{
				int phn_count = 0;
				for(string::size_type pos = 0; pos != string::npos; pos = iPron->mPtEntry.trans.find(" ", pos+1))
				{
					phn_count++;
				}
				if (phn_count < NGRAM_LENGTH)
				{
					DBG_FORCE("removing pronounciation variant:"<<iPron->mPtEntry.trans<<" phn_count:"<<phn_count);
					iKwd->mPronList.erase(iPron);
					changed = true;
					break;
				}
			}
		}
	} 
	/* }}} */

	DBG_FORCE("queryKwdList after postprocessing:"); queryKwdList->print();

/*	
	Search search(queryKwdList);

	search.SetLexiconPhn(mpMKws->GetDictionaryPhn());
	search.SetLexiconLvcsr(mpMKws->GetDictionaryLvcsr());
	search.SetIndexerPhn(mpMKws->GetIndexerPhn());
	search.SetIndexerLvcsr(mpMKws->GetIndexerLvcsr());
//	search.SetValidMeetings(validMeetings);

	search.SetRecordChannelColumn(MKWSSEARCHER_GET_ARG(int, p_record_channel_column, 0));
	search.SetRecordNameCutoffColumn(MKWSSEARCHER_GET_ARG(int, p_record_cutoff_column, 0));
	search.SetRecordDelim(MKWSSEARCHER_GET_ARG(string, p_record_delim, "_")[0]);
*/
	
	// create list of matching index entries (from the reverse index)
//TIMER_START;
	InvertedIndexLookup_LvcsrPhn(queryKwdList);
//TIMER_END("reverse-index-lookup");

	// show matched keywords
	if (MKWSSEARCHER_GET_ARG(bool, p_show_all_matched_keywords, false)) {
		PrintMatchedKeywords(queryKwdList);
	}

/* create clusters for each keyword from query !!! COMMENTED OUT !!! {{{ */
/*
TIMER_START;

	DBG("Clustering...");
	LatIndexer::Record tmpRec;
	for (QueryKwdList::iterator iQueryKwd = queryKwdList->begin(); iQueryKwd != queryKwdList->end(); ++iQueryKwd) 
	{
		DBG("Kwd " << iQueryKwd->W << ": searchResults.size():" << iQueryKwd->searchResults.size());
		for (vector<QueryKwd::RevIdxRecord>::iterator i=iQueryKwd->searchResults.mvByConfidence.begin(); i!=iQueryKwd->searchResults.mvByConfidence.end(); ++i) 
		{
			tmpRec = *i;
			// if the meeting is not valid, then skip it
			if (validMeetings->size() > 0 && validMeetings->find(tmpRec.meetingID) == validMeetings->end()) 
			{
//				DBG("meetingID:" << tmpRec.meetingID << " skipped");
				continue;
			}
//			iQueryKwd->recClusters.add(tmpRec);
//			continue;

			// translate percentual time value into the real time of current cluster's meeting
			LatTime time_min_translated = 0.0;
			LatTime time_max_translated = 0.0;
			
			// min time
			if (iQueryKwd->time_min_type == lse::time) 
			{
				time_min_translated = iQueryKwd->time_min;						
			}
			else if (iQueryKwd->time_min_type == lse::percents)
			{
				time_min_translated = indexer->meetings.getVal(tmpRec.meetingID).length * iQueryKwd->time_min;
			}
			
			// max time
			if (iQueryKwd->time_max_type == lse::time) 
			{
				time_max_translated = iQueryKwd->time_max;
			}
			else if (iQueryKwd->time_max_type == lse::percents)
			{
				time_max_translated = indexer->meetings.getVal(tmpRec.meetingID).length * iQueryKwd->time_max;
			}
				
			// remove the mess and accept the good ones
			if ( (iQueryKwd->confidence_min_isset && tmpRec.conf < iQueryKwd->confidence_min) ||
				 (iQueryKwd->confidence_max_isset && tmpRec.conf > iQueryKwd->confidence_max) ||
				
				 (iQueryKwd->time_min_type != null && tmpRec.t < time_min_translated) ||
				 (iQueryKwd->time_max_type != null && tmpRec.t > time_max_translated) ) 
			{
				DBG("    Deleting...");
				i->wordID = -1; // SHOULD BE REALLY DELETED ?!?
			} 
			else 
			{
//				iQueryKwd->recClusters.add(tmpRec, false); // the second argument 'false' means that the overlapping hypotheses in the reverse index are already clustered (only one hypothesis)
			}
		}
	}
	DBG("  done");

TIMER_END("create clusters for each keyword");	
*/
/* }}} */


	/* show inverted index lookup results {{{ */
	if (MKWSSEARCHER_GET_ARG(bool, p_show_reverse_index_lookup_results, false)) {
		for (QueryKwdList::iterator iQueryKwd = queryKwdList->begin(); iQueryKwd != queryKwdList->end(); ++iQueryKwd) {
			DBG_FORCE("CLUSTERS LIST FOR KWD: "<<iQueryKwd->W);
			DBG_FORCE("searchResults.size(): "<<iQueryKwd->searchResults.size());
//			iQueryKwd->recClusters.sort();
//			iQueryKwd->recClusters.print();
		}
	}
	/* }}} */


	/* create PHN clusters {{{ */
TIMER_START;
	InvertedIndexLookupResultsClustering_Phn(queryKwdList);
TIMER_END("computing neighbourhood hypotheses (PHN)");

/*
	DBG_FORCE("--------------------------------------------------");
	DBG_FORCE("PrintInvertedIndexLookupResultsClusters_LvcsrPhn()");
	PrintInvertedIndexLookupResultsClusters_LvcsrPhn(queryKwdList);
	DBG_FORCE("PrintInvertedIndexLookupResultsClusters_LvcsrPhn()...done");
	DBG_FORCE("--------------------------------------------------");
*/
TIMER_START;
	FillInvIdxRecordsClusters_Phn(queryKwdList);
TIMER_END("creating clusters of neighbourhood hypotheses (PHN)");

TIMER_START;
	SortInvIdxRecordsClusters_Phn(queryKwdList);
TIMER_END("sorting clusters of neighbourhood hypotheses (PHN)");
	/* }}} */

	// show clusters
	if (MKWSSEARCHER_GET_ARG(bool, p_show_all_clusters, false)) 
	{
		PrintInvIdxRecordsClusters_Phn(queryKwdList);
	}

	/* create LVCSR + PHN clusters {{{ */
	CombineInvertedIndexLookupResults_LvcsrPhn(queryKwdList);

TIMER_START;
	InvertedIndexLookupResultsClustering_LvcsrPhn(queryKwdList);
TIMER_END("computing neighbourhood hypotheses (LVCSR + PHN)");

//	search.PrintInvertedIndexLookupResultsClusters_LvcsrPhn(queryKwdList);

TIMER_START;
	FillInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
TIMER_END("creating clusters of neighbourhood hypotheses (LVCSR + PHN)");

TIMER_START;
	SortInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
TIMER_END("sorting clusters of neighbourhood hypotheses (LVCSR + PHN)");
	/* }}} */

	// show clusters
	if (MKWSSEARCHER_GET_ARG(bool, p_show_all_clusters, false)) 
	{
		PrintInvIdxRecordsClusters_LvcsrPhn(queryKwdList);
	}

	DBG("queryKwdList.size():"<<queryKwdList->size());
	if (MKWSSEARCHER_GET_ARG(bool, p_verify_candidates, false))
	{
		CERR("ERROR: Verification of candidates in lattices is not implemented");
		EXIT();
		//search.VerifyCandidates(config, queryKwdList, pResults);
	}
	else
	{
		InvIdxRecordsClusters2Results(queryKwdList, pResults);
		DBG("queryKwdList.size():"<<queryKwdList->size());
		DBG("pResults.size():"<<pResults->size());
	}
}
// }}}

// InvertedIndexLookup_LvcsrPhn() {{{
/**
 * @brief Get all hits of keywords (LVCSR) in pQueryKwdList and also for pronounciation variants if there are any (PHN)
 *
 * @param pQueryKwdList query
 */
void MKwsSearcher::InvertedIndexLookup_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	DBG_FORCE("looking into search index");
	// for each keyword in query
	for (QueryKwdList::iterator i = pQueryKwdList->begin(); i != pQueryKwdList->end(); ++i) 
	{
		// if the keyword is in LVCSR vocabulary
		if (mpMKws->GetDictionaryLvcsr())
		{
			if (mpMKws->GetDictionaryLvcsr()->Exists(i->W_id) &&
				(pQueryKwdList->mSearchSource == SearchSource::LVCSR || 
				 pQueryKwdList->mSearchSource == SearchSource::LVCSR_PHN ||
				 pQueryKwdList->mSearchSource == SearchSource::LVCSR_OOVPHN)
			   )
			{
				// get word's occurences in a reverse index
				mpMKws->GetGlobalInstance()->GetIndexerLvcsr()->GetOccurrences(i->W_id, &i->searchResults, pQueryKwdList->mDocList);
				//i->SetInvertedIndex(filenameSearchindexLvcsr);
				DBG_FORCE("searchResults (LVCSR): "<<i->searchResults.size());
			}
		}

		// for each pronounciation variant
		if (mpMKws->GetDictionaryPhn())
		{
			for (PronVariants::iterator iPron = i->mPronList.begin(); iPron != i->mPronList.end(); ++iPron)
			{
				// for each ngram
				for (QueryKwdList::iterator iNgram = iPron->mpQueryKwdListNgram->begin(); 
					 iNgram != iPron->mpQueryKwdListNgram->end();
					 iNgram ++)
				{
					// get ngram's occurences in a reverse index
					mpMKws->GetGlobalInstance()->GetIndexerPhn()->GetOccurrences(iNgram->W_id, &iNgram->searchResults, pQueryKwdList->mDocList);
					//iNgram->SetInvertedIndex(filenameSearchindexPhn);

					DBG_FORCE("searchResults (PHN): "<<iNgram->searchResults.size());
				}
			}
		}
	}
	DBG("  done");
}
// }}}

// PrintMatchedKeywords() {{{
void MKwsSearcher::PrintMatchedKeywords(QueryKwdList* pQueryKwdList)
{
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); iKwd != pQueryKwdList->end(); ++iKwd) {
		DBG_FORCE("FOUND KEYWORD: " << *iKwd);
		DBG_FORCE("foundLatRecords.size(): " << iKwd->searchResults.size());
		for (vector<QueryKwd::InvIdxRecord>::iterator iRec = iKwd->searchResults.mvByConfidence.begin(); iRec != iKwd->searchResults.mvByConfidence.end(); ++iRec) {
			DBG_FORCE("[LVCSR]"<<*iRec);
		}
		DBG_FORCE("================================================================================");
		for (PronVariants::iterator iPronVariant = iKwd->mPronList.begin(); 
			 iPronVariant != iKwd->mPronList.end(); 
			 iPronVariant ++)
		{
			DBG_FORCE("================================================================================");
			DBG_FORCE("PronVariant: "<<endl<<*(iPronVariant->mpQueryKwdListPhn));
			DBG_FORCE("================================================================================");
			for (QueryKwdList::iterator iPronVariantKwd = iPronVariant->mpQueryKwdListNgram->begin(); 
				 iPronVariantKwd != iPronVariant->mpQueryKwdListNgram->end(); 
				 iPronVariantKwd ++) 
			{
				for (vector<QueryKwd::InvIdxRecord>::iterator iRec = iPronVariantKwd->searchResults.mvByConfidence.begin();
					 iRec != iPronVariantKwd->searchResults.mvByConfidence.end(); 
					 iRec ++) 
				{
					DBG_FORCE("[PHN]"<<*iRec);
				}
				DBG_FORCE("================================================================================");
			}
		}
	}
}
// }}}

// InvertedIndexLookupResultsClustering_Phn() {{{
void MKwsSearcher::InvertedIndexLookupResultsClustering_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN search resutls
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->InvertedIndexLookupResultsClustering();
		}
	}
}
// }}}

// PrintInvertedIndexLookupResultsClusters_LvcsrPhn() {{{
void MKwsSearcher::PrintInvertedIndexLookupResultsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR search results
	pQueryKwdList->PrintInvertedIndexLookupResultsClusters();

	// for PHN search resutls
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->PrintInvertedIndexLookupResultsClusters();
		}
	}
}
// }}}

// FillInvIdxRecordsClusters_Phn() {{{
void MKwsSearcher::FillInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN inversed index records
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->FillInvIdxRecordsClusters(SourceType::PHN);
		}
	}
}
// }}}

// SortInvIdxRecordsClusters_Phn() {{{
void MKwsSearcher::SortInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN inversed index records
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->SortInvIdxRecordsClusters(InvIdxRecordsClusters::SortType::ByTimeAndConf);
		}
	}
}
// }}}

// PrintInvIdxRecordsClusters_Phn() {{{
void MKwsSearcher::PrintInvIdxRecordsClusters_Phn(QueryKwdList* pQueryKwdList)
{
	// for PHN inversed index records
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			iPron->mpQueryKwdListNgram->PrintInvIdxRecordsClusters();
		}
	}
}
// }}}

// CombineInvertedIndexLookupResults_LvcsrPhn() {{{
void MKwsSearcher::CombineInvertedIndexLookupResults_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	DBG_FORCE("CombineInvertedIndexLookupResults_LvcsrPhn()");
	for (QueryKwdList::iterator iKwd = pQueryKwdList->begin(); 
		 iKwd != pQueryKwdList->end(); 
		 iKwd ++) 
	{
		iKwd->mpClusters = new InvIdxRecordsClusters;

		// for each pronounciation variant
		for (PronVariants::iterator iPron = iKwd->mPronList.begin(); 
			 iPron != iKwd->mPronList.end(); 
			 iPron ++)
		{
			// sort the clusters by time since we want to use binary search
			DBG_FORCE("mpClusters->SortClusters()");
			iKwd->mpClusters->SortClusters(InvIdxRecordsClusters::SortType::ByTime);
			DBG_FORCE("mpClusters->SortClusters()...done");
			InvIdxRecordsClusters new_clusters;
			// for each cluster of the variant
			for (InvIdxRecordsClusters::iterator iCluster = iPron->mpQueryKwdListNgram->mInvIdxRecordsClusters.begin();
				iCluster != iPron->mpQueryKwdListNgram->mInvIdxRecordsClusters.end();
				iCluster ++)
			{
				// if there is no such occurrence in the LVCSR inverted index lookup results
				// and no previous PronVariant has added it yet
				DBG("searchResults.FindOccurrence()");
				if (!iKwd->searchResults.FindOccurrence(*iCluster) && !iKwd->mpClusters->FindCluster(*iCluster))
				{
					DBG("adding new cluster: "<<*iCluster);
					iCluster->SetSourceType(SourceType::PHN_CLUSTER);
					iCluster->SetPronVariant(&(*iPron));
					new_clusters.push_back(*iCluster);
				}
			}
			// add all the new_clusters to the query keyword's clusters list
			for (InvIdxRecordsClusters::iterator iCluster = new_clusters.begin();
				iCluster != new_clusters.end();
				iCluster ++)
			{
				iKwd->mpClusters->push_back(*iCluster);
			}
//			DBG_FORCE("iKwd->mpClusters->Print()"); iKwd->mpClusters->Print();
		}

		// at last add all LVCSR occurrences to the clusters list
		for (unsigned int i=0; i<iKwd->searchResults.size(); i++)
		{
			InvIdxRecordsCluster cluster = iKwd->searchResults.mvByConfidence[i];
			cluster.SetSourceType(SourceType::LVCSR);
			iKwd->mpClusters->push_back(cluster);
		}

		// now sort the complete word's clusters list by time again
		iKwd->mpClusters->SortClusters(InvIdxRecordsClusters::SortType::ByTime);
//		DBG_FORCE("iKwd->mpClusters->Print()"); iKwd->mpClusters->Print();
	}
}
// }}}

// InvertedIndexLookupResultsClustering_LvcsrPhn() {{{
void MKwsSearcher::InvertedIndexLookupResultsClustering_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR search results
	pQueryKwdList->QueryKwdClustersClustering();
}
// }}}

// FillInvIdxRecordsClusters_LvcsrPhn() {{{
void MKwsSearcher::FillInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR inversed index records
	pQueryKwdList->FillQueryClusters();
}
// }}}

// SortInvIdxRecordsClusters_LvcsrPhn() {{{
void MKwsSearcher::SortInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR inversed index records
	pQueryKwdList->SortInvIdxRecordsClusters(InvIdxRecordsClusters::SortType::ByTimeAndConf); // sort LVCSR by time and by confidence
}
// }}}

// PrintInvIdxRecordsClusters_LvcsrPhn() {{{
void MKwsSearcher::PrintInvIdxRecordsClusters_LvcsrPhn(QueryKwdList* pQueryKwdList)
{
	// for LVCSR inversed index records
	pQueryKwdList->PrintInvIdxRecordsClusters();
}
// }}}

// InvIdxRecordsClusters2Results() {{{
void MKwsSearcher::InvIdxRecordsClusters2Results(QueryKwdList *pQueryKwdList, Results *pResults)
{
	for(InvIdxRecordsClusters::iterator iCluster = pQueryKwdList->mInvIdxRecordsClusters.begin();
		iCluster != pQueryKwdList->mInvIdxRecordsClusters.end();
		iCluster ++)
	{
		InvIdxRecordsCluster* pCluster = &(*iCluster);
		Hypothesis hyp;
		hyp.stream = SourceType::ToStr(pCluster->GetSourceType());

		//LatMeetingIndexerRecord meetingRec = pConfig->GetMeetingIndexer()->getVal(pCluster->mMeetingID); // could be also iRec->meetingID
		//hyp.channel = GetMeetingChannelFromMeetingName(meetingRec.path, pConfig);
		//hyp.record = CutoffMeetingName(meetingRec.path, pConfig);
		hyp.documentId = pCluster->mMeetingID;
		hyp.score = pCluster->mConf;

		for (QueryKwdList::iterator iKwd = pQueryKwdList->begin();
			iKwd != pQueryKwdList->end();
			iKwd ++)
		{
			OccurrenceWithSourceList* pOsl = &((*pCluster)[iKwd->W_id]);

			Hypothesis::Word word;
			word.str 	= iKwd->W;
			word.start 	= pOsl->mStartTime;
			word.end 	= pOsl->mEndTime;
			word.conf 	= pOsl->mConf;
			if (iKwd->W_id < 0)
			{
				for (QueryKwdList::iterator iPhn = iKwd->mPronList[0].mpQueryKwdListPhn->begin();
					iPhn != iKwd->mPronList[0].mpQueryKwdListPhn->end();
					iPhn ++)
				{
					word.pron += (word.pron.length() ? " " : "") + iPhn->W;
				}
			}
			hyp.push_back_word(word);
			hyp.addKeyword(word);
		}
		//DBG("pResults->push_back(): "<<hyp);
		pResults->push_back(hyp);
	}
	//DBG("pResults->size():"<<pResults->size());
}
// }}}

