#ifndef INDEXINGSERVER_INTERFACE_H
#define INDEXINGSERVER_INTERFACE_H

#include "global.h"

class IndexingServer_Interface
{
	public:
		virtual ~IndexingServer_Interface() {}
//		virtual void Start() = 0;
		virtual ID ProcessAssignIdMsg(const std::string &moduleId, const std::string &msg) = 0;
};

#endif
