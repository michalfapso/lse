#ifndef MTXTMSGPARSER_H
#define MTXTMSGPARSER_H

#include <string>

#include "../M/MMsgParser.h"

namespace sem /* search engine module */
{

class MTxt;

class MTxtMsgParser : public MMsgParser
{
	protected:
		MTxt* mpMTxt;

	public:
		MTxtMsgParser(MTxt* pMTxt) : MMsgParser((M*)pMTxt), mpMTxt(pMTxt) {}
		virtual ~MTxtMsgParser() {}
};

}

#endif
