#ifndef MTXTSEARCHER_H
#define MTXTSEARCHER_H

#include "../M/MSearcher.h"
#include "MTxtQuery.h"
#include "MTxtQueryParser.h"

namespace sem
{

class MTxt;

class MTxtSearcher : public MSearcher
{
	protected:
		MTxt* mpMTxt;
		MTxtQuery* mpQuery;
		MTxtQueryParser* mpQueryParser;

	public:
		MTxtSearcher(MTxt* pMTxt);
		virtual ~MTxtSearcher();

		virtual void ParseQuery(const std::string &qStr);
		virtual void GetNResults(int n, ResultsList * pRes);
};

}

#endif
