#ifndef MTXTDICTIONARY_H
#define MTXTDICTIONARY_H

#include <string>

#include "../M/MDictionary.h"

namespace sem /* search engine module */
{

class MTxt;

class MTxtDictionary : public MDictionary
{
	protected:
		MTxt* mpMTxt;

	public:
		MTxtDictionary(MTxt* pMTxt) : MDictionary((M*)pMTxt), mpMTxt(pMTxt) 
		{ 
			SetTableName("MTxtDictionary"); 
		}

		virtual ~MTxtDictionary() {}

		virtual void InitGlobal();
/*
		virtual bool 
			AddUnit(const std::string &label, UnitId* assignedId = NULL);

		virtual ID 
			GetUnitId(const std::string &label);

		virtual std::string 
			GetUnitLabel(const UnitId id);
*/
};

}

#endif
