#include "MTxt.h"

using namespace std;
using namespace sem;

MTxt::MTxt(ConfigFile* pConfig, M* pGlobalParent) : M(pConfig, pGlobalParent)
{
	mModuleId = "SUM";
	mModuleName = "Summary";
	mModuleDescription = "Search in document summary";

	mpIndexer = new MTxtIndexer(this);
	mpSearcher = new MTxtSearcher(this);
	mpDictionary = new MTxtDictionary(this);
	mpMsgParser = new MMsgParser(this);
}

M* MTxt::CreateChildInstance()
{
	assert(!IsGlobal());
	M* m = new MTxt(mpConfig, this);
	return m;
}

MTxt::~MTxt()
{
	delete mpIndexer;
	delete mpSearcher;
	delete mpDictionary;
	delete mpMsgParser;
}

void MTxt::InitGlobal(ApplicationType::EApplicationType appType)
{
	M::InitGlobal(appType);
	//DBG("MTxt::Init("<<ApplicationType::str(appType)<<")");
	mpDictionary->InitGlobal();
	mpIndexer->InitGlobal();
}

void MTxt::Index(ID docId, const std::string &filename)
{
	DBG("MTxt::Index("<<docId<<", "<<filename<<")");
	mpIndexer->Index(docId, filename);
}

void MTxt::ParseQuery(const std::string &qStr)
{
	DBG("MTxt::ParseQuery("<<qStr<<")");
	mpSearcher->ParseQuery(qStr);
}

void MTxt::GetNResults(int n, ResultsList* pRes)
{
	DBG("MTxt::GetNResults()");
	assert(GetSearcher());
	GetSearcher()->GetNResults(n, pRes);
}

