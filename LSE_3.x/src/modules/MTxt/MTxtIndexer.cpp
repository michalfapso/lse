#include "MTxtIndexer.h"
#include "MTxt.h"

#include <fstream>
#include <sstream>

using namespace std;
using namespace sem;

void MTxtIndexer::InitGlobal()
{
	MIndexerDb::InitGlobal();
}

void MTxtIndexer::Index(ID docId, const std::string &filename)
{
	DBG("MTxtIndexer::Index("<<docId<<", "<<filename<<")");
	ifstream fin(filename.c_str());
	if( !fin ) {
		CERR("Error opening " << filename << " for input");
		EXIT();
	}
	string word;
	unsigned int pos = 1;
	while( fin >> word )
	{
		DBG("word: '"<<word<<"'");
		ID word_id = ID_INVALID;
		assert(mpMTxt);
		assert(mpMTxt->GetDictionary());
		// if the word is not included in the dictionary, add it
		if ( (word_id = mpMTxt->GetDictionary()->GetUnitId(word)) == ID_INVALID )
		{
			word_id = mpMTxt->GetDictionary()->AddUnit(word);
		}
		DBG("id: "<<word_id);
		AddRecord(docId, word_id, pos);
		pos++;
	}
}

void MTxtIndexer::AddRecord(ID docId, ID wordId, unsigned int pos)
{
	ostringstream ss;
	ss << "INSERT INTO "<<mTableName<<"(id_doc,id_label,pos) VALUES("<<docId<<","<<wordId<<","<<pos<<")";
	DBG("sql:"<<ss.str());
	char *zErrMsg = 0;
	int rc = sqlite3_exec(mpM->GetDb(), ss.str().c_str(), NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK) {
		CERR("SQL error["<<rc<<"]: "<<zErrMsg);
		sqlite3_free(zErrMsg);
		EXIT();
	}
}
