#include "MTxtSearcher.h"
#include "MTxt.h"

#include <sstream>

using namespace std;
using namespace sem;

MTxtSearcher::MTxtSearcher(MTxt* pMTxt) : MSearcher((M*)pMTxt), mpMTxt(pMTxt) 
{
	mpQuery = new MTxtQuery(mpMTxt);
	mpQueryParser = new MTxtQueryParser(mpMTxt);
}

MTxtSearcher::~MTxtSearcher()
{
	delete mpQuery;
	delete mpQueryParser;
}

void MTxtSearcher::ParseQuery(const std::string &qStr)
{
	mpQueryParser->Parse(qStr, mpQuery);
}

static int MTxtSearcher_GetNResults_Callback(void *p, int argc, char **argv, char **azColName)
{
	// in this case there is only 1 result in each document (see GetNResults() method below)
	ResultsList* pRes = (ResultsList*)p;

	ID doc_id = atol(argv[0]);
	pRes->AddResult(doc_id, new ResultsList::Result(atof(argv[1]), 0, 0));
	return 0;
}

void MTxtSearcher::GetNResults(int n, ResultsList * pRes)
{
	DBG("MTxtSearcher::GetNResults()");
	string word = mpQuery->GetQueryStr();
	ID word_id = mpMTxt->GetDictionary()->GetUnitId(word);

	ostringstream ss;
	ss << "SELECT id_doc, count(id_doc) as score FROM "<<mpMTxt->GetIndexer()->GetTableName();
	ss << " WHERE id_label = "<<word_id;
	ss << " GROUP BY id_doc";
	ss << " ORDER BY count(id_doc) desc";
	ss << " LIMIT 0,"<<n;
	DBG("sql:"<<ss.str());
	char *zErrMsg = 0;
	int rc = sqlite3_exec(mpMTxt->GetDb(), ss.str().c_str(), MTxtSearcher_GetNResults_Callback, pRes, &zErrMsg);
	if (rc != SQLITE_OK) {
		CERR("SQL error["<<rc<<"]: "<<zErrMsg);
		sqlite3_free(zErrMsg);
		EXIT();
	}
}

