#ifndef MTXTQUERY_H
#define MTXTQUERY_H

#include <string>

#include "../M/MQuery.h"

namespace sem /* search engine module */
{

class MTxt;

class MTxtQuery : public MQuery
{
	protected:
		MTxt* mpMTxt;

	public:
		MTxtQuery(MTxt* pMTxt) : MQuery((M*)pMTxt), mpMTxt(pMTxt) {}
		virtual ~MTxtQuery() {}
};

}

#endif
