#ifndef MTXTINDEXER_H
#define MTXTINDEXER_H

#include "../M/MIndexerDb.h"

namespace sem
{

class MTxt;

class MTxtIndexer : public MIndexerDb
{
	protected:
		MTxt* mpMTxt;

	public:
		MTxtIndexer(MTxt* pMTxt) : MIndexerDb((M*)pMTxt), mpMTxt(pMTxt) { SetTableName("MTxtIndex"); }
		virtual ~MTxtIndexer() {}

		virtual void InitGlobal();

	   	virtual void Index(ID docId, const std::string &filename);

		virtual void AddRecord(ID docId, ID wordId, unsigned int pos);
};

}

#endif
