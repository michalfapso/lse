#ifndef MTXTQUERYPARSER_H
#define MTXTQUERYPARSER_H

#include <string>
#include "global.h"

namespace sem
{

class M;

class MTxtQueryParser : public MQueryParser
{
	protected:
		MTxt* mpMTxt;
	public:
		MTxtQueryParser(MTxt* pMTxt) : MQueryParser((M*)pMTxt), mpMTxt(pMTxt) {}
		virtual ~MTxtQueryParser() {}

		virtual void Parse(const std::string &qStr, MTxtQuery* pQuery) 
		{ 
			DBG("MTxtQueryParser::Parse()"); 
			pQuery->SetQueryStr(qStr);
		}
};

}

#endif

