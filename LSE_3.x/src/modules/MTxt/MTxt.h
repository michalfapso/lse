#ifndef MTXT_H
#define MTXT_H

#include "../M/M.h"
#include "MTxtIndexer.h"
#include "MTxtSearcher.h"
#include "MTxtDictionary.h"

namespace sem 
{

class MTxt : public M
{
	public:
		MTxt(ConfigFile* pConfig, M* pGlobalParent = NULL);
		virtual M* CreateChildInstance();
		virtual ~MTxt();

		virtual void InitGlobal(ApplicationType::EApplicationType appType);
		virtual void Index(ID docId, const std::string &filename);
		virtual void ParseQuery(const std::string &qStr);
		virtual void GetNResults(int n, ResultsList* pRes);

		virtual MTxtIndexer* GetIndexer() { return dynamic_cast<MTxtIndexer*>(mpIndexer); }
		virtual MTxtDictionary* GetDictionary() { return dynamic_cast<MTxtDictionary*>(mpDictionary); }
};

}

#endif

