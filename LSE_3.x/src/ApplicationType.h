#ifndef APPLICATIONTYPE_H
#define APPLICATIONTYPE_H

#include <string>

class ApplicationType
{
	public:
		enum EApplicationType
		{
			EIndexingClient,
			EIndexingServer,
			ESorter,
			ESearchingServer,
		};

		static std::string str(EApplicationType t) 
		{
			switch (t)
			{
				case EIndexingClient: return "EIndexingClient"; break;
				case EIndexingServer: return "EIndexingServer"; break;
				case ESorter: return "ESorter"; break;
				case ESearchingServer: return "ESearchingServer"; break;
			}
			return "UNKNOWN_APP_TYPE";
		}
};

#endif

